package com.phonethics.shoplocal;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils.TruncateAt;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.BadTokenException;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.flurry.android.Constants;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.networkcall.AddCustomerProfile;
import com.phonethics.networkcall.AddCustomerProfileReceiver;
import com.phonethics.networkcall.AddCustomerProfileReceiver.CustomerProfileReceiver;
import com.phonethics.networkcall.GetDateCategory;
import com.phonethics.networkcall.GetDateCategoryReceiver;
import com.phonethics.networkcall.GetDateCategoryReceiver.GetDate;
import com.phonethics.networkcall.GetUserProfileResultReceiver;
import com.phonethics.networkcall.GetUserProfileResultReceiver.GetUserProfileInterface;
import com.phonethics.networkcall.GetUserProfileService;


public class CustomerProfile extends SherlockActivity implements CustomerProfileReceiver,GetUserProfileInterface, GetDate{

	ActionBar actionBar;
	ImageView profileThumb;

	EditText customerName;
	TextView customerDob;
	EditText customerEmail;
	EditText customerCity;
	EditText customerState;
	Button btnCreateProfile,btnLoginFb;

	RadioGroup radioGroup;

	RadioButton genMale;
	RadioButton genFemale;
	String gender="";

	Activity context;
	private static final List<String> PERMISSIONS = Arrays.asList("email","user_birthday","user_location");

	private boolean pendingPublishReauthorization = false;
	String 			fbUserid="";
	String 			fbAccessToken="";
	ProgressBar		pBar;

	Uri mImageCaptureUri;
	private File      mFileTemp;

	LinearLayout dateViewParent;

	//Activity request
	public static final int REQUEST_CODE_GALLERY      = 0x1;
	public static final int REQUEST_CODE_TAKE_PICTURE = 0x4;
	public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;

	private final int REQ_CODE_CAMERA = 5;
	private final int REQ_CODE_GALLERY = 6;
	private final int REQ_CODE_GALLERYCROP = 7;

	String FILE_PATH="";
	String FILE_NAME="";
	String FILE_TYPE="";

	Dialog dialog,remoteDialog;

	boolean isImageBroadCast;

	int age = 0;

	//Api Urls
	static String API_HEADER;
	static String API_VALUE;
	static String PATH;
	static String URL;

	//Session Manger Class
	SessionManager session;

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";


	static String USER_ID="";
	static String AUTH_ID="";

	boolean isLoggedInCustomer, loginThroughFB = false;

	AddCustomerProfileReceiver recieverObj;
	GetUserProfileResultReceiver mGetProfile;
	GetDateCategoryReceiver getDateCat;

	Calendar dateTime=Calendar.getInstance();

	String profilePicUrl, userIdFB, accessTokenFB;

	String nowAsString ;

	//Image loader
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	NetworkCheck network;
	Bitmap bitmapImage=null;
	String encodedpath="";

	//Photo base url
	String PHOTO_URL, GET_CATEGORIES;

	int i=0;

	ArrayList<String> name;

	ArrayList<String> ids;

	ArrayList<LinearLayout> allLayouts;

	TextView[]  forDatePicker;

	TextView[]  forDateTypePicker;

	//TextView[]  forAddMoreButton;

	LinearLayout[] childLayout;

	RelativeLayout[] addMoreBtnLayout;

	ImageView[] deleteImg;

	int compareIndexes = 0;

	boolean isBelowDateClicked = false;

	int positionOfDateText;

	TextView AddMore;

	ArrayList<String> dateToPass;

	ArrayList<String> idToPass;

	boolean isError = false;

	boolean isdeleted = false;

	int statusAddDelete[];

	String[] idToPasArray;

	String[] dateToPassArray;

	int chkCount = 0;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();


	private static long back_pressed;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;


	ExpandableListView drawerList;

	byte userGender;
	String drawer_item="";


	DrawerExpandibleAdapter drawerAdapter;
	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	DrawerClass drawerClass;


	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	LocalNotification localNotification;

	byte[] profilepic_byte;

	Button btnProfChooseImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_profile);


		Date alsoNow = Calendar.getInstance().getTime();

		nowAsString = new SimpleDateFormat("yyyy-MM-dd").format(alsoNow);

		//Log.d("NOWDATE","NOWDATE" +  nowAsString);

		context=this;

		localNotification=new LocalNotification(getApplicationContext());

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		drawerClass=new DrawerClass(context);

		PHOTO_URL=getResources().getString(R.string.photo_url);

		session = new SessionManager(context);
		network=new NetworkCheck(context);

		imageLoader=ImageLoader.getInstance();

		moreData=drawerClass.isShowMore();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCustomerCache");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}


		//Log.d("COUNTONCREATE","COUNTONCREATE" + chkCount);

		if(cacheDir.exists())
		{
			cacheDir.delete();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}else if(network.isNetworkAvailable())
		{

			/*DeleteRecursive(cacheDir);*/
		}

		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		imageLoader.clearDiscCache();
		imageLoader.clearMemoryCache();


		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		// Defining url and path

		URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api)+getResources().getString(R.string.register);

		GET_CATEGORIES = getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api)+getResources().getString(R.string.dateCat);

		//PATH=getResources().getString(R.string.login);

		recieverObj=new AddCustomerProfileReceiver(new Handler());
		recieverObj.setReceiver(this);

		mGetProfile=new GetUserProfileResultReceiver(new Handler());
		mGetProfile.setReceiver(this);


		getDateCat = new GetDateCategoryReceiver(new Handler());
		getDateCat.setReciver(this);

		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
		isLoggedInCustomer=session.isLoggedInCustomer();

		pBar =(ProgressBar)findViewById(R.id.pBar);
		pBar.setVisibility(View.INVISIBLE);
		actionBar=getSupportActionBar();
		actionBar.setTitle(getResources().getString(R.string.itemMyProfile));
		actionBar.setHomeButtonEnabled(true);
		actionBar.setIcon(R.drawable.ic_drawer);
		actionBar.show();


		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);





		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		btnProfChooseImage=(Button)findViewById(R.id.btnProfChooseImage);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});

		//Creating Drawer
		createDrawer();


		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getResources().getString(R.string.itemMyProfile));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle("Menu");
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);



		radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
		genMale=(RadioButton)findViewById(R.id.genMale);
		genFemale=(RadioButton)findViewById(R.id.genFemale);

		profileThumb=(ImageView)findViewById(R.id.profileThumb);

		customerName=(EditText)findViewById(R.id.customerName);
		customerDob=(TextView)findViewById(R.id.customerDob);
		customerEmail=(EditText)findViewById(R.id.customerEmail);
		customerCity=(EditText)findViewById(R.id.customerCity);
		customerState=(EditText)findViewById(R.id.customerState);

		btnCreateProfile=(Button)findViewById(R.id.btnCreateProfile);
		btnLoginFb		= (Button)findViewById(R.id.btnCreateProfileFb);

		dateViewParent = (LinearLayout) findViewById(R.id.dateViewParent);
		allLayouts = new ArrayList<LinearLayout>();

		dateToPass = new ArrayList<String>();

		idToPass = new ArrayList<String>();

		mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mImageCaptureUri = Uri.fromFile(mFileTemp);

		AddMore = (TextView) findViewById(R.id.AddMore);

		dialog=new Dialog(context);
		remoteDialog=new Dialog(context);

		getDateProfile();
		//		if(isLoggedInCustomer)
		//		{
		//			//			getDateProfile();
		//			loadProfile();
		//		}
		//		else
		//		{
		//			showToast("Please login to update profile.");
		//			Intent intent=new Intent(context,LoginSignUpCustomer.class);
		//			context.startActivityForResult(intent,2);
		//			overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
		//		}




		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(checkedId==genMale.getId())
				{
					gender=genMale.getText().toString();
					//showToast(gender);
				}
				else if(checkedId==genFemale.getId())
				{
					gender=genFemale.getText().toString();
					//showToast(gender);
				}
			}
		});

		btnLoginFb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				loginThroughFB = true;

				try {
					PackageInfo info = getPackageManager().getPackageInfo(
							"com.phonethics.shoplocal", 
							PackageManager.GET_SIGNATURES);
					for (Signature signature : info.signatures) {
						MessageDigest md = MessageDigest.getInstance("SHA");
						md.update(signature.toByteArray());
						//Log.d("MY KEY HASH:", "MY KEY HASH:" +Base64.encodeToString(md.digest(), Base64.DEFAULT));
					}
				} catch (NameNotFoundException e) {

				} catch (NoSuchAlgorithmException e) {

				}
				if(network.isNetworkAvailable())
				{
					login_facebook();
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		btnProfChooseImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				choosePicture();
			}
		});

		profileThumb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				choosePicture();
			}
		});

		btnCreateProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(network.isNetworkAvailable())
				{
					idToPass.clear();
					dateToPass.clear();

					for(int i=0;i<idToPasArray.length;i++){



						if(!(idToPasArray[i].equalsIgnoreCase("0")) && !(dateToPassArray[i].equalsIgnoreCase("0"))){

							//Log.d("PLACESTOFILL","PLACESTOFILL " + i);

							idToPass.add(idToPasArray[i]);
							dateToPass.add(dateToPassArray[i]);

						}
						//					else{
						//						
						//						idToPass.clear();
						//						dateToPass.clear();
						//					}
					}


					if(!isLoggedInCustomer)
					{

						showToast(getResources().getString(R.string.customerLoginAlert));
						Intent intent=new Intent(context,LoginSignUpCustomer.class);
						context.startActivityForResult(intent,2);
						overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
					}
					else{

						//					if(customerName.length() == 0 && customerDob.length() == 0 && customerEmail.length() == 0 && customerCity.length() == 0 && customerState.length() == 0){
						//
						//						showToast("Please enter details to create profile");
						//					}
						//					else 
						if(customerEmail.getText().length()>0 && !isValidEmail(customerEmail.getText()))
						{
							showToast(getResources().getString(R.string.emailIdAlert));
						}

						else{

							for(int i=0; i<forDatePicker.length;i++){

								if (forDatePicker[i].getVisibility() == View.VISIBLE){




									//								if(forDatePicker[i].getText().toString().trim().length() == 0){
									//
									//									if(forDateTypePicker[i].getText().toString().length()!=0){
									//
									//										isError = true;
									//										break;
									//									}
									//								}
									//								else 
									if(forDateTypePicker[i].getText().toString().trim().length() == 0){

										if(forDatePicker[i].getText().toString().length()!=0){

											isError = true;
											break;
										}
									}
									else{

										isError = false;
									}

								}

							}

							//Log.d("ISERROR", "ISERROR" + isError);

							if(isError){

								showToast(getResources().getString(R.string.dateTypeAlert));
							}
							else{



								for(int i=0;i<forDatePicker.length;i++){

									if(forDatePicker[i].getVisibility()==View.VISIBLE){

										if((forDatePicker[i].getText().toString().length())>0){

											chkCount = 1;
											//Log.d("COUNT","COUNT" + chkCount + " " + forDatePicker[i].getText().toString());
											break;
										}
									}
								}

								if(chkCount!=1){

									// show alert/prompt to fill date

									AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
									alertDialog.setIcon(R.drawable.ic_launcher);
									alertDialog.setTitle("Shoplocal");
									alertDialog.setMessage("Entering important dates allows merchants to send you special offers around those dates.");
									alertDialog.setIcon(R.drawable.ic_launcher);
									alertDialog.setCancelable(true);
									alertDialog.setPositiveButton("Skip", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {

											addProfile();
										}
									});
									alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {


										}
									});
									AlertDialog alert = alertDialog.create();
									alert.show();
								}
								else{

									addProfile();
								}

							}


						}

					}



				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}

			}
		});


		customerDob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isBelowDateClicked = false;
				chooseDate();
			}
		});

		EventTracker.startLocalyticsSession(getApplicationContext());

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}


		}

		/** Anirudh Custom dialog share */
		dialogShare = new Dialog(CustomerProfile.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));


	}//on create Ends here

	void choosePicture()
	{
		AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setTitle("Shoplocal");
		alertDialog.setMessage("Choose Image From :");
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setCancelable(true);
		alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				encodedpath="";
				loginThroughFB = false;
				openGallery();
			}
		});
		alertDialog.setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				encodedpath="";
				loginThroughFB = false;
				takePicture();
			}
		});
		AlertDialog alert = alertDialog.create();
		alert.show();
	}


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		uiHelper.onStop();
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	void loginAlert(String msg)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				context.startActivityForResult(intent,2);
				overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();
			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();

		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();
			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}


			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}


			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}*/


			showDialogTab();

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{

				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 11);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{

				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}


	void showDialogTab(){

		dialogShare.show();

		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(CustomerProfile.this)
					.setLink("http://shoplocal.co.in")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}

				dismissDialog();

			}
		});
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}



	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();

		try
		{
			Session.openActiveSession(this, false, new Session.StatusCallback() {


				// callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {

					/*if(session.isOpened())
				{	*/
					session.closeAndClearTokenInformation();
					/*exception.printStackTrace();
					}*/
					//					Log.i("Clearing", "Token");
				}
			});
			//FlurryAgent.endTimedEvent("Gallery_Opened_Event");
		}catch(NullPointerException npx)
		{
			npx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Validating Email id.
	public final  boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}   

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}



	//	@Override
	//	public void onBackPressed() {
	//		// TODO Auto-generated method stub
	//		super.onBackPressed();
	//
	//		
	//		this.finish();
	//		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	//
	//
	//	}


	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.AddCustomerProfileReceiver.CustomerProfileReceiver#onReceiveCustomerProfile(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveCustomerProfile(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			pBar.setVisibility(View.GONE);

			String prof_msg = resultData.getString("prof_msg");

			String prof_status= resultData.getString("prof_status");

			if(prof_status.equalsIgnoreCase("true")){

				//showToast("Profile created successfully");

				AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setTitle("Shoplocal");
				alertDialog.setMessage(getResources().getString(R.string.profileUpdate));
				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						//					finish();
					}
				});

				AlertDialog alert = alertDialog.create();
				alert.show();
			}
			else if(prof_status.equalsIgnoreCase("false"))
			{

				showToast(prof_msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
					createDrawer();
				}
				//			if(prof_msg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				session.logoutCustomer();
				//				createDrawer();
				//			}
			}else if(prof_status.equalsIgnoreCase("error"))
			{
				showToast(prof_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	int getCurrentAge(String date){

		Calendar cal = Calendar.getInstance();

		date = date.substring(0, 4);

		Log.i("Year","Current year "+date);

		int age = Integer.parseInt(date) - cal.get(Calendar.YEAR);

		return Math.abs(age);
	}

	byte getGender(String gender){
		if(gender.equalsIgnoreCase("male")){
			return Constants.MALE;
		}
		else {
			return Constants.FEMALE;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.ProfileExistResultReceiver.IsProfileExists#onReceiveProfileStatus(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveProfileStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		pBar.setVisibility(View.GONE);

		try
		{

			String profile_status=resultData.getString("prof_status");
			String prof_msg=resultData.getString("prof_msg");

			//Log.d("PASSEDATES","PASSEDATES" +  passedDates.size());

			if(profile_status.equalsIgnoreCase("true"))
			{

				String	ID=resultData.getString("ID");
				String	NAME=resultData.getString("NAME");
				String	MOBILE=resultData.getString("MOBILE");
				String	EMAIL=resultData.getString("EMAIL");
				//		String	DOB=resultData.getString("DOB");
				String	GENDER=resultData.getString("GENDER");
				String	CITY=resultData.getString("CITY");
				String	STATE=resultData.getString("STATE");
				String	IMAGE_URL=resultData.getString("IMAGE_URL");

				String FACEBOOK_USER_ID = resultData.getString("FACEBOOK_USER_ID");

				Log.i("FUSERID","FUSERID "+FACEBOOK_USER_ID);

				// check whether facebook user_id is present or not

				if(FACEBOOK_USER_ID.equalsIgnoreCase("0")){

					btnLoginFb.setText("Connect with facebook");

				}
				else{

					btnLoginFb.setText("Sync profile from facebook");
				}


				ArrayList<String> passedDates = resultData.getStringArrayList("datesList");

				Log.i("GENDER","GENDER "+GENDER);

				userGender = getGender(GENDER);

				ArrayList<String> passedDateIds = resultData.getStringArrayList("categoryList");

				setProfile(NAME, "", EMAIL, CITY, STATE, GENDER, IMAGE_URL);


				if(passedDates.size() == 1 && passedDates.get(0).equalsIgnoreCase("0000-00-00")){

					//childLayout[0].setVisibility(View.GONE);

					//				forDatePicker[0].setText("");
					//				forDateTypePicker[0].setText("");

					for(int i=0;i<name.size();i++){

						if(i == 3){
							break;
						}
						else{

							childLayout[i].setVisibility(View.VISIBLE);
							forDatePicker[i].setText("");
							forDateTypePicker[i].setText(name.get(i));

							idToPasArray[i] = ids.get(i);
						}
					}

				}
				else{

					for(int i=0;i<passedDates.size();i++){

						childLayout[i].setVisibility(View.VISIBLE);

						forDatePicker[i].setText(passedDates.get(i));



						chkCount = 1;

						//dateToPassArray[i] = passedDates.get(i);

						for(int j=0;j< name.size();j++){

							if(passedDateIds.get(i).equalsIgnoreCase(ids.get(j))){

								forDateTypePicker[i].setText(name.get(j));

								//idToPasArray[i] = name.get(i);

								//Log.d("IDTOPASS","IDTOPASSS " + name.get(j) + " " + passedDates.get(i));

								dateToPassArray[i] = passedDates.get(i);

								idToPasArray[i] = ids.get(j);

								if(name.get(j).equalsIgnoreCase("birthday")){
									age = getCurrentAge(passedDates.get(i));
									Log.i("Birthday","My birthday "+age);
								}

								break;

							}



						}



					}


					for(int i=0;i<dateToPassArray.length;i++){

						//Log.d("IDTOPASS","IDTOPASSS " + dateToPassArray[i] + " " + idToPasArray[i]);
					}
				}


				//showToast(prof_msg);
			}
			else if(profile_status.equalsIgnoreCase("false"))
			{
				showToast(prof_msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
					createDrawer();
				}

				//			if(prof_msg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				session.logoutCustomer();
				//				createDrawer();
				//			}

			}
			else if(profile_status.equalsIgnoreCase("error"))
			{
				showToast(prof_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void login_facebook(){


		try
		{
			pBar.setVisibility(View.VISIBLE);
			//showToast("Please Wait");

			//List<String> permissions = session.getPermissions();


			Session.openActiveSession(context, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();

						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						//						Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {


						Request request = Request.newMeRequest(session,new Request.GraphUserCallback() {
							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									try{
										//Log.d("", "Fb responcse == "+response.toString());
										//Log.d("", "FbUser >> "+ user.toString());
										fbUserid=user.getId();
										fbAccessToken=session.getAccessToken();
										//										Log.i("User Name ", "FbName : "+user.getName());
										//										Log.i("User Id ", "FbId : "+user.getId());
										//										Log.i("User Id ", "FbLocation : "+user.getLocation().toString());
										//
										//										Log.i("User Id ", "FbEmailId : "+user.getLink());
										//										Log.i("User Id ", "FbDob : "+user.getBirthday());
										//
										//										Log.i("User Id ", "FbGender : "+user.asMap().get("gender").toString());
										//										Log.i("User Id ", "FbEmail : "+user.getProperty("email").toString());
										//Log.i("User Id ", "FbCity : "+user.getLocation().getCity().toString());
										//Log.i("User Id ", "FbState : "+user.getLocation().getState().toString());
										//										Log.i("User Access Token ", "FbAccess Token : "+session.getAccessToken());
										pBar.setVisibility(View.INVISIBLE);

										String name = user.getName();
										String dob = user.getBirthday();
										Log.i("User Birthday","birthday "+dob);
										gender = user.asMap().get("gender").toString();

										if(gender.equalsIgnoreCase("male")){

											genMale.setChecked(true);
											//genFemale.setSelected(false);
											//Toast.makeText(context, "male", 0).show();
										}
										else{

											//genMale.setSelected(false);
											genFemale.setChecked(true);
											//Toast.makeText(context, "female", 0).show();

										}

										String email =user.getProperty("email").toString();
										String city = user.getLocation().getProperty("name").toString();
										String url="http://graph.facebook.com/"+user.getId()+"/picture?type=large";
										setText(name, dob, email, city, "","",url);

										userIdFB = user.getId();

										accessTokenFB = session.getAccessToken();

									}catch(Exception ex){
										ex.printStackTrace();
									}


								}

								else{
									pBar.setVisibility(View.INVISIBLE);
									//									Log.i("ELSE", "ELSE");
								}
							}


						});
						request.executeAsync();
						//session.getAccessToken();
						//Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();
					}


					if(session.isClosed()){
						pBar.setVisibility(View.VISIBLE);
						EventTracker.logEvent("CSignup_LoginFailedFB", false);
						//Toast.makeText(context, "Closed", 0).show();

						try {

							if ((exception instanceof FacebookOperationCanceledException ||
									exception instanceof FacebookAuthorizationException)) {
								new AlertDialog.Builder(CustomerProfile.this)
								.setTitle("Login Failed")
								.setMessage(exception.getMessage().toString())
								.setPositiveButton("OK", null)
								.show();

							}

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						Log.d("EXCEPTION","EXCEPTION " + exception);

					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		try
		{
			if(requestCode==2)
			{

				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				isLoggedInCustomer=session.isLoggedInCustomer();

				//				getDateProfile();
				loadProfile();

				createDrawer();

				/*addProfile();*/
			}

			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);

			if(requestCode==11)
			{
				if(session.isLoggedIn())
				{

					SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
					Editor 	editor=prefs.edit();
					editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
					editor.commit();

					boolean isAddStore=data.getBooleanExtra("isAddStore", false);
					if(isAddStore)
					{
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						context.finish();
					}
					else
					{
						Intent intent=new Intent(context,MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			}
		}catch(IllegalStateException ilgx)
		{
			ilgx.printStackTrace();
		}
		catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}


		if (resultCode != RESULT_OK) {

			return;
		}

		try
		{


			Bitmap bitmap;

			switch (requestCode) {
			// CAMERA - A	
			case REQ_CODE_CAMERA:
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				bitmap = null;
				CameraImageSave cameraImageSave = new CameraImageSave();
				String filePath = cameraImageSave.getImagePath();
				try{
					bitmap = cameraImageSave.getBitmapFromFile(480);

					Uri uri = Uri.fromFile(new File(filePath));

					FileInputStream in = new FileInputStream(filePath);
					BufferedInputStream buf = new BufferedInputStream(in);
					bitmap = BitmapFactory.decodeStream(buf);


					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
					profilepic_byte = stream.toByteArray();

					cameraImageSave.deleteFromFile();
					bitmap = null;
					cameraImageSave = null;

					setPhoto(BitmapFactory.decodeByteArray(profilepic_byte , 0, profilepic_byte.length));
				}
				catch(Exception e){
					Log.e("Exception","Cusomer profile class");
					e.printStackTrace();
				}
				break;

				// GET IMAGE FROM GALLERY - A
			case REQ_CODE_GALLERY:
				if(resultCode == RESULT_OK){
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(CustomerProfile.this, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQ_CODE_GALLERYCROP);
				}
				break;

			case REQ_CODE_GALLERYCROP:
				if(resultCode == RESULT_OK){
					String filePath1 = Environment.getExternalStorageDirectory()
							+ File.separator + "temp_photo_crop.jpg";
					try{
						FileInputStream in = new FileInputStream(filePath1);
						BufferedInputStream buf = new BufferedInputStream(in);
						bitmap = BitmapFactory.decodeStream(buf);

						bitmap = Bitmap.createScaledBitmap(bitmap, 480, 480, true);

						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						profilepic_byte = stream.toByteArray();
						bitmap = null;

						setPhoto(BitmapFactory.decodeByteArray(profilepic_byte , 0, profilepic_byte.length));
					}
					catch(Exception e){}
				}
				break;
			}

		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	//Down scaling bitmap to get Thumbnai
	public  Bitmap getThumbnail(Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;//optional
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;//optional
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional


		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);

		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}



	public void setText(String Name,String DOB, String emailId, String city, String state,String gender,String imgeUrl){

		customerName.setText(Name);
		DateFormat  dt=new SimpleDateFormat("yyyy-mm-dd");


		DateFormat df = new SimpleDateFormat("mm/dd/yyyy"); 
		Date startDate;
		String newDateString = null ;
		try {
			startDate = df.parse(DOB);
			newDateString = dt.format(startDate);
			System.out.println(newDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}


		for(int i=0;i<name.size();i++){


			if(i!=0 && forDateTypePicker[i].getText().toString().equalsIgnoreCase(name.get(0))){


			}
			else{
				childLayout[0].setVisibility(View.VISIBLE);
				forDatePicker[0].setText(newDateString);
				forDateTypePicker[0].setText(name.get(0));

				idToPasArray[0] = "1";
				dateToPassArray[0] = newDateString;
				break;

			}
		}



		//	customerDob.setText(newDateString);
		customerEmail.setText(emailId);
		customerCity.setText(city);
		customerState.setText(state);
		/*ImageLoader imageLoader = new ImageLoader(context);
		imageLoader.DisplayImage(imgeUrl, profileThumb);*/
		imageLoader.displayImage(imgeUrl, profileThumb, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				// TODO Auto-generated method stub
				pBar.setVisibility(View.GONE);
			}
		});
		btnCreateProfile.setText("Update Profile");

		profilePicUrl = imgeUrl;
	}
	public void setProfile(String Name,String DOB, String emailId, String city, String state,String gender,String imgeUrl){

		try
		{
			Log.d("GENDER","GENDER" + gender);

			customerName.setText(Name);
			//			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			//
			//
			//			DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
			//			Date startDate;
			//			String newDateString = null ;
			//			try {
			//				startDate = df.parse(DOB);
			//				newDateString = dt.format(startDate);
			//				System.out.println(newDateString);
			//			} catch (ParseException e) {
			//				e.printStackTrace();
			//			}



			//			customerDob.setText(newDateString);
			customerEmail.setText(emailId);
			customerCity.setText(city);
			customerState.setText(state);

			if(gender.equalsIgnoreCase("Male")){

				genMale.setChecked(true);
			}
			else{

				genFemale.setChecked(true);
			}

			/*ImageLoader imageLoader = new ImageLoader(context);
		imageLoader.DisplayImage(imgeUrl, profileThumb);*/
			imageLoader.displayImage(PHOTO_URL+imgeUrl, profileThumb, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					pBar.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					// TODO Auto-generated method stub
					pBar.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					// TODO Auto-generated method stub
					pBar.setVisibility(View.GONE);
					bitmapImage=((BitmapDrawable)profileThumb.getDrawable()).getBitmap();
					encodedpath=encodeTobase64(bitmapImage);
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
				}

				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					pBar.setVisibility(View.GONE);
				}
			});
			btnCreateProfile.setText("Update Profile");

			profilePicUrl = imgeUrl;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public static String encodeTobase64(Bitmap image)
	{
		Bitmap immagex=image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

		Log.e("LOOK", imageEncoded);
		return imageEncoded;
	}
	public static Bitmap decodeBase64(String input) 
	{
		byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}




	private void openGallery() {

		/*Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);*/

		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQ_CODE_GALLERY);
	}


	private void takePicture() {

		Intent intent = new Intent(CustomerProfile.this, CameraView.class);
		startActivityForResult(intent, REQ_CODE_CAMERA);
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}


	private void startCropImage() {

		//Intent intent = new Intent(this, ZoomCroppingActivity.class);
		/*intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 3);*/

		Intent intent = new Intent(this, ZoomCroppingActivity.class);

		intent.putExtra("comingfrom", "gallery");
		intent.putExtra("imagepath", mFileTemp.getPath());

		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}


	void setPhoto(Bitmap bitmap)
	{

		/*ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			user_file= stream.toByteArray();*/

		profileThumb.setImageBitmap(bitmap);
		profileThumb.setScaleType(ScaleType.FIT_CENTER);
		//		reviewImage.setImageBitmap(bitmap);
		//		reviewImage.setScaleType(ScaleType.FIT_CENTER);
		//		deleteImage.setVisibility(View.VISIBLE);
		//		imgThumbPreview.setVisibility(View.VISIBLE);
		//		reviewImage.setVisibility(View.VISIBLE);
	}

	private void addProfile() {

		/** Flurry set age and gender */

		Log.d("AGEOFUSER","AGEOFUSER " + age);

		if(age!=0)
			EventTracker.setUserAge(CustomerProfile.this, age);
		EventTracker.setUserGender(userGender);

		if(encodedpath==null || encodedpath.equals("")){
			/*if(!FILE_PATH.equals("") && FILE_PATH.length()!=0)
			{
				byte [] b=imageTobyteArray(FILE_PATH,150,150);
				if(b!=null)
				{
					encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
					if(encodedpath==null || encodedpath.equals("")){
						encodedpath="";
					}
				}
			}*/

			if(profilepic_byte!=null)
			{
				encodedpath=Base64.encodeToString(profilepic_byte, Base64.DEFAULT);
				Log.i("Encode ", "Details : "+encodedpath);
			}
			else
			{
				encodedpath="undefined";
			}
		}



		if(loginThroughFB){

			Intent intent=new Intent(context, AddCustomerProfile.class);
			intent.putExtra("addCustomerProfile", recieverObj);
			intent.putExtra("URL", URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);

			intent.putExtra("name", customerName.getText().toString());
			intent.putExtra("email", customerEmail.getText().toString());
			intent.putExtra("dob", customerDob.getText().toString());
			intent.putExtra("city", customerCity.getText().toString());
			intent.putExtra("state", customerState.getText().toString());


			intent.putExtra("gender", gender.toLowerCase());
			Log.d("GENDER","GENDER FB " + gender.toLowerCase());

			intent.putExtra("image_url", profilePicUrl);
			intent.putExtra("facebook_user_id", userIdFB);
			intent.putExtra("facebook_access_token", accessTokenFB);

			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("type", "facebook");

			intent.putStringArrayListExtra("allIds", idToPass);
			intent.putStringArrayListExtra("allDates", dateToPass);

			context.startService(intent);
			pBar.setVisibility(View.VISIBLE);


		}
		else{

			Intent intent=new Intent(context, AddCustomerProfile.class);
			intent.putExtra("addCustomerProfile", recieverObj);
			intent.putExtra("URL", URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);

			intent.putExtra("name", customerName.getText().toString());
			intent.putExtra("email", customerEmail.getText().toString());
			intent.putExtra("dob", customerDob.getText().toString());
			intent.putExtra("city", customerCity.getText().toString());
			intent.putExtra("state", customerState.getText().toString());


			intent.putExtra("userfile", encodedpath);
			intent.putExtra("filename", FILE_NAME);
			intent.putExtra("filetype", FILE_TYPE);

			if(genMale.isChecked()){

				intent.putExtra("gender", genMale.getText().toString().toLowerCase());
				Log.d("GENDER","GENDER MANUAL " + genMale.getText().toString().toLowerCase());
			}
			else{

				intent.putExtra("gender", genFemale.getText().toString().toLowerCase());
				Log.d("GENDER","GENDER MANUAL " + genFemale.getText().toString().toLowerCase());


			}

			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("type", "manual");

			intent.putStringArrayListExtra("allIds", idToPass);
			intent.putStringArrayListExtra("allDates", dateToPass);

			context.startService(intent);
			pBar.setVisibility(View.VISIBLE);

		}




		//		intent.putExtra("user_id", USER_ID);
		//		intent.putExtra("auth_id", AUTH_ID);
		//		context.startService(intent);

	}

	private void loadProfile()
	{
		Intent intent=new Intent(context, GetUserProfileService.class);
		intent.putExtra("getUserProfile", mGetProfile);
		intent.putExtra("URL", URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		context.startService(intent);
		pBar.setVisibility(View.VISIBLE);


	}

	//Convert image into byte array
	static 	byte[] imageTobyteArray(String path,int width,int height)
	{	byte[] b = null ;
	if(!path.equals("") || path.length()!=0)
	{


		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inDither=true;//optional
		options.inPreferredConfig=Bitmap.Config.RGB_565;//optional

		Bitmap bm = BitmapFactory.decodeFile(path,options);

		options.inSampleSize = calculateInSampleSize(options, width, height);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		bm=BitmapFactory.decodeFile(path,options);


		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
		b= baos.toByteArray();
	}
	return b;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}


	public void chooseDate(){
		/*	dateTime.add(Calendar.DAY_OF_MONTH, 1);*/
		new DatePickerDialog(context, d, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
	}

	public void chooseDate(int pos){
		/*	dateTime.add(Calendar.DAY_OF_MONTH, 1);*/

		positionOfDateText  = pos;
		new DatePickerDialog(context, d, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
	}



	DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");


			String ct = DateFormat.getDateInstance().format(new Date());

			String setDate = dt.format(dateTime.getTime());

			Log.d("SETDATE","SETDATE" +  setDate);

			Log.d("SETDATE","SETDATE" +  setDate);

			//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			//			try {
			//				
			//				Date date1 = sdf.parse(ct);
			//				Date date2 = sdf.parse(setDate);
			//				
			if(setDate.compareTo(nowAsString)>0){

				//				Toast.makeText(context, "Unable to set future date", 0).show();
				showToast(getResources().getString(R.string.dateAlert));

				if(isBelowDateClicked){

					//Toast.makeText(context, "belowclickinif", 0).show();

					for(int i=0; i<name.size();i++){

						if(i==positionOfDateText)
							forDatePicker[i].setText("");
					}

				}else{

					//	customerDob.setText("");
				}

			}
			else{

				if(isBelowDateClicked){

					//Toast.makeText(context, "belowclickinelse", 0).show();

					for(int i=0; i<name.size();i++){

						if(i==positionOfDateText){

							forDatePicker[i].setText(dt.format(dateTime.getTime()));

							//						dateToPass.add(dt.format(dateTime.getTime()));

							dateToPassArray[i] = dt.format(dateTime.getTime());

						}

					}


				}else{

					//customerDob.setText(dt.format(dateTime.getTime()));
				}


				//Toast.makeText(context, "Cant set future date", 0).show();
			}

		}
	};

	@Override
	public void onGetDateReceive(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		pBar.setVisibility(View.GONE);

		try {

			String status = resultData.getString("date_status");

			String message = resultData.getString("prof_msg");

			ids = resultData.getStringArrayList("dataIds");

			name = resultData.getStringArrayList("dataName");

			if(status.equalsIgnoreCase("true")){

				if(isLoggedInCustomer)
				{
					//			getDateProfile();
					loadProfile();
				}
				else
				{
					loginAlert(getResources().getString(R.string.customerLoginAlert));

				}

				int size = name.size();

				forDatePicker = new TextView[size];

				forDateTypePicker = new TextView[size];

				//forAddMoreButton = new TextView[size];

				childLayout = new LinearLayout[size];

				addMoreBtnLayout = new RelativeLayout[size];

				deleteImg = new ImageView[size];

				statusAddDelete = new int[size];

				idToPasArray = new String[size];

				dateToPassArray = new String[size];


				// FOR INITIAL STATUS OF IDTOPASS AND DATETOPASS ARRAYLIST


				for(int indx=0; indx < idToPasArray.length; indx++){

					idToPasArray[indx] = "0";
					dateToPassArray[indx] = "0";
				}


				// FOR INITIAL STATUS OF ADD AND DELETE 

				for(int k=0;k<statusAddDelete.length;k++){

					if(k==0){

						statusAddDelete[k] = 10;
					}
					else{

						statusAddDelete[k] = 5;
					}

				}


				for(int k=0;k<size;k++){

					forDatePicker[k] = new TextView(context);
					forDateTypePicker[k] = new TextView(context);
					//forAddMoreButton[k] = new TextView(context);
					childLayout[k]  = new LinearLayout(context);
					addMoreBtnLayout[k] = new RelativeLayout(context);
					deleteImg[k] = new ImageView(context);
				}


				creatLayout(size);



				// TO SHOW ONLY ONE VIEW AT THE VERY FIRST TIME

				for(int chk=0;chk<size;chk++){

					if(chk!=0){

						childLayout[chk].setVisibility(View.GONE);
						//addMoreBtnLayout[chk].setVisibility(View.GONE);
					}
					else{

						//deleteImg[chk].setVisibility(View.INVISIBLE);
					}
				}



				// TO ADD A NEW VIEW ON CLICK OF ADD MORE BUTTON




				//				for(int add =1;add < size ;add++){
				//
				//					Log.d("ADDSIZE","ADDSIZE" +  add);

				//Log.d("ADDSIZE","ADDSIZE" +  compareIndexes);

				//					if(add == size-1){
				//
				//						//AddMore.setVisibility(View.INVISIBLE);
				//						break;
				//					}

				//					forAddMoreButton[add].setOnClickListener(new OnClickListener() {
				//
				//						@Override
				//						public void onClick(View v) {
				//							// TODO Auto-generated method stub
				//
				//							Log.d("compareIndexes","compareIndexes" +  compareIndexes);
				//
				//							//addMoreBtnLayout[compareIndexes].setVisibility(View.GONE);
				//							childLayout[++compareIndexes].setVisibility(View.VISIBLE);
				//
				//							//addMoreBtnLayout[compareIndexes].setVisibility(View.VISIBLE);
				//						}
				//					});

				//					compareIndexes = add; 

				AddMore.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub


						//Toast.makeText(context, "CLicked " + compareIndexes, 0).show();

						//							if(compareIndexes==name.size()){
						//							
						//								AddMore.setVisibility(View.INVISIBLE);
						//							}
						//							else{




						compareIndexes++;

						//						childLayout[compareIndexes].setVisibility(View.VISIBLE);

						for(int m=0;m<statusAddDelete.length;m++){

							if(statusAddDelete[m] == 5){

								statusAddDelete[m] = 10;
								childLayout[m].setVisibility(View.VISIBLE);
								break;
							}
						}


						Log.d("Value","Value" +  compareIndexes);


						if(compareIndexes==name.size()-1){

							AddMore.setVisibility(View.INVISIBLE);
						}

						//							}




					}
				});

				//				}



				// TO DELETE ANY RECORDS


				for(int j=0;j<size;j++){

					final int copy = j;


					deleteImg[j].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							if(isError){

								isError = false;
							}

							compareIndexes = compareIndexes-1;

							forDateTypePicker[copy].setText("");
							forDatePicker[copy].setText("");
							childLayout[copy].setVisibility(View.GONE);

							idToPasArray[copy] = "0";
							dateToPassArray[copy] = "0";

							statusAddDelete[copy] = 5;

							AddMore.setVisibility(View.VISIBLE);



							//							if(compareIndexes == -1){
							//
							//								compareIndexes = 0;
							//							}

							//							Log.d("Value","Value DLT" +  copy);
							//							Log.d("Value","Value DLT" +  compareIndexes);
							//Toast.makeText(context, "clicked delete " + compareIndexes, 0).show();
							//if(copy!=name.size()-1)
							//addMoreBtnLayout[copy].setVisibility(View.GONE);
						}
					});
				}



				// TO GET CLICK OF ALL DATEPICKERS 


				for( int m = 0 ; m< size ;m++){

					final int pos = m;
					forDatePicker[m].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							isBelowDateClicked = true;
							//new DatePickerDialog(context, d, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
							chooseDate(pos);
						}
					});
				}



				// TO GET CLICK OF ALL DATE TYPE PICKER AND TO SET THEM

				for(int l =0 ; l <size; l++){

					final int copy = l;
					forDateTypePicker[l].setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub



							final CharSequence[] cs = name.toArray(new CharSequence[name.size()]);

							AlertDialog.Builder builder = new AlertDialog.Builder(context);
							builder.setTitle("Make your selection");
							builder.setIcon(R.drawable.ic_launcher);
							builder.setItems(cs, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,int which) {
									// TODO Auto-generated method stub



									checkForAvailability(cs[which].toString(), copy, which);
									//forDateTypePicker[copy].setText(cs[which]);

								}




							});
							AlertDialog alert = builder.create();
							alert.show();

						}
					});
				}

			}
			else{

				showToast(message);
				//				if(message.startsWith(getResources().getString(R.string.invalidAuth)))
				//				{
				//					session.logoutCustomer();
				//					createDrawer();
				//				}

			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}



	}



	private void getDateProfile() {
		// TODO Auto-generated method stub


		Intent intent = new Intent(context, GetDateCategory.class);

		intent.putExtra("getDateCategory", getDateCat);
		intent.putExtra("URL", GET_CATEGORIES);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);

		context.startService(intent);
		pBar.setVisibility(View.VISIBLE);
	}





	private void creatLayout(int size) {
		// TODO Auto-generated method stub


		for(int index=0; index< size; index++){


			//Toast.makeText(context, "size " + name.size(), 0).show();

			// FOR LINEAR LAYOUT IN WHICH SPINNER IS PRESENT



			childLayout[index].setOrientation(LinearLayout.HORIZONTAL);

			LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp2.setMargins(10, 10, 10, 10);
			childLayout[index].setLayoutParams(lp2);



			// FOR TEXT BOX TO SELECT DATE FROM DATEPICKER


			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
			lp.setMargins(0, 10, 5, 10);


			forDatePicker[index].setLayoutParams(lp);
			forDatePicker[index].setGravity(Gravity.CENTER);
			//datePick.setPadding(5, 5, 5, 5);

			forDatePicker[index].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdownicon, 0);
			forDatePicker[index].setBackgroundResource(R.drawable.shadow_effect);
			forDatePicker[index].setHint("Select Date");

			childLayout[index].addView(forDatePicker[index]);




			// FOR TEXTBOX TO SELECT TYPE OF DATE FROM LIST


			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.9f);
			lp1.setMargins(0, 10, 5, 10);


			forDateTypePicker[index].setLayoutParams(lp1);
			//dateType.setPadding(5, 5, 5, 5);
			forDateTypePicker[index].setGravity(Gravity.CENTER);

			forDateTypePicker[index].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.dropdownicon, 0);
			forDateTypePicker[index].setBackgroundResource(R.drawable.shadow_effect);
			forDateTypePicker[index].setHint("select type of date");
			forDateTypePicker[index].setTextSize(12);
			forDateTypePicker[index].setEllipsize(TruncateAt.END);
			forDateTypePicker[index].setSingleLine(true);


			childLayout[index].addView(forDateTypePicker[index]);


			// ADD DELETE BUTTON IN LINEAR LAYOUT


			LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp3.setMargins(0, 10, 5, 10);

			deleteImg[index].setLayoutParams(lp3);
			deleteImg[index].setImageResource(R.drawable.deletebtn);
			childLayout[index].addView(deleteImg[index]);

			// FOR ADDMORE BUTTON LAYOUT AND ITS TEXT 


			RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

			param.setMargins(20, 10, 0, 0);
			//addMoreBtnLayout[index].setLayoutParams(param);



			// FOR ADDMORE BUTTON TEXTVIEW INSIDE ABOVE RELATIVE LAYOUT

			if(index!=size-1){

				//				forAddMoreButton[index].setText("Add more");
				//				forAddMoreButton[index].setTextColor(Color.parseColor("#2E9AFE"));
				//				forAddMoreButton[index].setTextSize(22);
				//				forAddMoreButton[index].setLayoutParams(param);
				//addMoreBtnLayout[index].addView(forAddMoreButton[index]);


			}

			dateViewParent.addView(childLayout[index]);

			//dateViewParent.addView(addMoreBtnLayout[index]);

		}
	}


	public void checkForAvailability(String string, int position, int textPos) {
		// TODO Auto-generated method stub

		boolean passIt = false;

		for(int chk = 0; chk< name.size(); chk++){


			if(position == chk){

			}
			else{

				if(forDateTypePicker[chk].getText().toString().equalsIgnoreCase(string)){

					passIt = false;

					showToast(getResources().getString(R.string.dateTypeAlert));
					forDateTypePicker[position].setText("");
					break;

				}
				else{


					passIt = true;

					forDateTypePicker[position].setText(string);


					//					idToPass.add(ids.get(textPos) + "");

				}
			}

		}

		if(passIt){

			//			idToPass.add(ids.get(textPos) + "");

			//			Log.d("IDARRAY","IDARRAY " + position);

			idToPasArray[position] = ids.get(textPos);


		}

	}



	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}





	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try
		{
			//			SearchView searchView = new SearchView(actionBar.getThemedContext());
			//			searchView.setQueryHint("Search");
			//			searchView.setIconified(true);
			//
			//			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
			//			.setIcon(R.drawable.abs__ic_search)
			//			.setActionView(searchView)
			//			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			//
			//			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			//
			//				@Override
			//				public boolean onQueryTextSubmit(String newText) {
			//
			//					Intent intent=new Intent(context,SearchActivity.class);
			//					intent.putExtra("genericSearch", true);
			//					intent.putExtra("search_text", newText);
			//					startActivity(intent);
			//					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
			//
			//					return true;
			//				}
			//
			//
			//
			//				@Override
			//				public boolean onQueryTextChange(String newText) {
			//
			//
			//					return true;
			//				}
			//			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		try
		{
			Session.openActiveSession(this, false, new Session.StatusCallback() {


				// callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {

					/*if(session.isOpened())
				{	*/
					session.closeAndClearTokenInformation();
					/*exception.printStackTrace();
					}*/
					//					Log.i("Clearing", "Token");
				}
			});
			//FlurryAgent.endTimedEvent("Gallery_Opened_Event");
		}catch(NullPointerException npx)
		{
			npx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();

			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}


}

