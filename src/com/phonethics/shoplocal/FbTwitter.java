package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;



public class FbTwitter extends SherlockActivity {

	WebView socailWeb;
	ActionBar actionBar;
	Activity context;
	ProgressBar webProg;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();

	private static long back_pressed;

	boolean isFb;
	boolean isMerchant;

	SessionManager session;


	String drawer_item="";

	DrawerExpandibleAdapter drawerAdapter;
	ExpandableListView drawerList;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	DrawerClass drawerClass;

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	LocalNotification localNotification;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fb_twitter);
		context=this;
		localNotification=new LocalNotification(getApplicationContext());
		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		session=new SessionManager(context);

		drawerClass=new DrawerClass(context);

		actionBar=getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);

		EventTracker.startLocalyticsSession(context);




		socailWeb=(WebView)findViewById(R.id.socailWeb);
		webProg=(ProgressBar)findViewById(R.id.webProg);

		moreData=drawerClass.isShowMore();

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);





		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});


		Bundle b=getIntent().getExtras();
		if(b!=null)
		{

			String facebook=b.getString("facebook");
			String twitter=b.getString("twitter");
			isMerchant=b.getBoolean("isMerchant");

			if(isMerchant)
			{
				mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
				actionBar.setHomeButtonEnabled(false);
			}
			if(!isMerchant)
			{
				actionBar.setIcon(R.drawable.ic_drawer);
			}
			else if(isMerchant)
			{
				actionBar.setDisplayHomeAsUpEnabled(true);
			}

			isFb=b.getBoolean("isFb");
			if(isFb)
			{
				actionBar.setTitle(getString(R.string.itemFacebook));
				socailWeb.loadUrl(getResources().getString(R.string.fb));
			}else
			{
				actionBar.setTitle(getString(R.string.itemTwitter));
				socailWeb.loadUrl(getResources().getString(R.string.twitter));
			}
		}



		if(!isMerchant)
		{
			mDrawerToggle=new ActionBarDrawerToggle(
					this,                  /* host Activity */
					mDrawerLayout,         /* DrawerLayout object */
					R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
					R.string.drawer_open,  /* "open drawer" description for accessibility */
					R.string.drawer_close  /* "close drawer" description for accessibility */
					) {
				public void onDrawerClosed(View view) {
					if(isFb)
					{
						actionBar.setTitle(getString(R.string.itemFacebook));

					}else
					{
						actionBar.setTitle(getString(R.string.itemTwitter));

					}
					//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
				}

				public void onDrawerOpened(View drawerView) {
					actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
					//                getActionBar().setTitle(mDrawerTitle);
					//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
				}
			};

			mDrawerLayout.setDrawerListener(mDrawerToggle);




			//Creating Drawer
			createDrawer();
		}

		socailWeb.setWebViewClient(new MyWebViewClient());
		socailWeb.requestFocus(View.FOCUS_DOWN);

		socailWeb.getSettings().setJavaScriptEnabled(true);
		socailWeb.getSettings().setPluginState(PluginState.ON);


		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}

		/** Anirudh custom share dialog */
		dialogShare = new Dialog(FbTwitter.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));



	}//onCreate Ends Here

	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();

			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();

		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			//			Log.i("LOG TRIM ", "LOG TRIM "+drawer_item.replaceAll(" ", ""));
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();

			return false;
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		uiHelper.onStop();
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}
	/*
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}*/

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{

			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}*/


			showDialogTab();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}


	void showDialogTab(){
		dialogShare.show();

		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(FbTwitter.this)
					.setLink("http://shoplocal.co.in")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}

				dismissDialog();

			}
		});
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}



	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {


			view.loadUrl(url);
			return true;
		}

		@Override
		public void onLoadResource(WebView  view, String  url){

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			webProg.setVisibility(View.GONE);
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			webProg.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			webProg.setVisibility(View.GONE);
			super.onReceivedError(view, errorCode, description, failingUrl);
		}


	}    

	void finishTo()
	{
		if(isMerchant)
		{
			Intent intent=new Intent(context,MerchantTalkHome.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			this.finish();
		}
		else
		{
			//			Intent intent=new Intent(context,HomeGrid.class);
			//			startActivity(intent);
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			this.finish();

		}
	}



	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}





	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try
		{
			/*SearchView searchView = new SearchView(actionBar.getThemedContext());
			searchView.setQueryHint("Search");
			searchView.setIconified(true);

			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
			.setIcon(R.drawable.abs__ic_search)
			.setActionView(searchView)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

				@Override
				public boolean onQueryTextSubmit(String newText) {

					Intent intent=new Intent(context,SearchActivity.class);
					intent.putExtra("genericSearch", true);
					intent.putExtra("search_text", newText);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
					return true;
				}



				@Override
				public boolean onQueryTextChange(String newText) {


					return true;
				}
			});*/

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(!isMerchant)
		{
			switch(item.getItemId()){
			case android.R.id.home:
				mDrawerLayout.openDrawer(Gravity.LEFT);
				if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
				{
					mDrawerLayout.closeDrawers();
				}
			}
		}
		else
		{
			finishTo();
		}

		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(!isMerchant)
		{
			if(back_pressed+2000 >System.currentTimeMillis())
			{
				localNotification.alarm();
				finishTo();
			}
			else
			{
				Toast.makeText(getBaseContext(), "Press again to exit!", Toast.LENGTH_SHORT).show();
				back_pressed=System.currentTimeMillis();
			}
		}
		else
		{
			finishTo();
		}
	}


}
