package com.phonethics.shoplocal;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MyToast
{

	public static void showToast(Activity context,String text,int type,int length)
	{
		LayoutInflater inflator=context.getLayoutInflater();

		View view =inflator.inflate(R.layout.customtoast, null);
		ImageView toastIcon=(ImageView)view.findViewById(R.id.toastIcon);
		
		TextView textView=(TextView)view.findViewById(R.id.customToastText);
		textView.setText(text);
		
		switch (type) {
		case 1:
			//connection error
			toastIcon.setImageResource(R.drawable.ic_connection_error);
			break;
		case 2:
			//warning
			//toastIcon.setImageResource(R.drawable.ic_input_field_warning);
			break;
		case 3:
			//error  
			toastIcon.setImageResource(R.drawable.ic_input_field_error);
			break;
		default:
			break;
		}
		

		//Toast.makeText(context, text, length).show();
		Toast toast=new Toast(context);
		toast.setDuration(length);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setView(view);
		toast.show();


	}
	public static void showToast(Activity context,String text,int type)
	{
		int length=Toast.LENGTH_SHORT;
		
		LayoutInflater inflator=context.getLayoutInflater();

		View view =inflator.inflate(R.layout.customtoast, null);
		ImageView toastIcon=(ImageView)view.findViewById(R.id.toastIcon);
		
		TextView textView=(TextView)view.findViewById(R.id.customToastText);
		textView.setText(text);
		
		switch (type) {
		case 1:
			//connection error
			toastIcon.setImageResource(R.drawable.ic_connection_error);
			break;
		case 2:
			//warning
			//toastIcon.setImageResource(R.drawable.ic_input_field_warning);
			break;
		case 3:
			//error  
			toastIcon.setImageResource(R.drawable.ic_input_field_error);
			break;
		default:
			break;
		}

		//Toast.makeText(context, text, length).show();
		Toast toast=new Toast(context);
		toast.setDuration(length);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setView(view);
		toast.show();
		/*Toast.makeText(context, text, length).show();*/
	}
}
