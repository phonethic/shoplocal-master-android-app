package com.phonethics.shoplocal;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.networkcall.AddBroadCastReceiver;
import com.phonethics.networkcall.AddBroadCastReceiver.AddBroadCastResult;
import com.phonethics.networkcall.AddBroadCastService;


public class CreateBroadCast extends SherlockActivity implements AddBroadCastResult {

	ActionBar actionbar;
	static String BUSINESS_ID="";
	static String BUSINESS_URL;
	static String STORE_URL;
	static String BROADCAST_URL;

	static String BUSINESS_PATH="";
	static String SOTRES_PATH="";
	static String GET_BROADCAST_URL;
	static String Add_BROADCAST_URL;

	static String USER_ID="";
	static String AUTH_ID="";


	static String API_HEADER;
	static String API_VALUE;

	static String LOGIN_PATH;
	static String LOGOUT_PATH;

	static String LOGIN_URL;

	Activity context;

	//CreateBroadcast
	EditText edtBroadTitle,edtBroadDesc,edtTags;
	Button btnReview;
	CheckBox chkIsOffer;

	TextView offerDate,offerTime;

	AddBroadCastReceiver mAddBroadCast;
	ArrayList<String> SELECTED_STORE_ID;

	boolean isSplitLogin=false,isMyBroadCast=false;

	TableRow tblSelectDate,tblselectTime;

	String is_offered="";


	Calendar dateTime=Calendar.getInstance();
	Calendar dateTimeCurrent=Calendar.getInstance();
	long miliseconds;

	//Session Manger
	SessionManager session;
	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	ImageView imgBroadCast;

	String MERCHANT_BUSINESS_ID="";

	ProgressBar prog;

	Dialog dialog,remoteDialog;
	ImageView imgThumbPreview;

	String FILE_PATH="";
	String FILE_NAME="";
	String FILE_TYPE="";

	/**************************FILE CROP***********************/
	public static final int REQUEST_CODE_GALLERY      = 6;
	public static final int REQUEST_CODE_TAKE_PICTURE = 7;
	public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;
	private static final int REQUEST_CODE_CUSTOMCAMERA = 8;
	private static final int REQUEST_CODE_POSTGALLERY = 9;
	private static final int REQUEST_CODE_POSTCROPGALLERY = 10;
	final int PIC_CROP = 3;
	private File      mFileTemp;

	Uri mImageCaptureUri;

	Button btnImageChooser;
	Button btnChooseImage;
	Button btnTakeImage;
	Button btnRemoteImage;
	Button btnSubmitUrl;


	EditText url;

	boolean isRemote=false;
	String remoteurl="";

	boolean isImageBroadCast;
	boolean isReuse=false;
	String imageType="";
	String encodedpath="";

	ScrollView makePostScroll;
	ScrollView viewPostScroll;

	Button editButton;
	Button postButton;

	TextView txtViewOfferDate;
	TextView txtViewOfferDetail;

	ImageView reviewImage;

	ImageView deleteImage;

	String title;
	String description;
	String photourl;
	String tags;

	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	String PHOTO_URL;

	Bitmap bitmapImage;

	TextView txtViewOfferTitle;

	Typeface tf;

	NetworkCheck network;
	File cacheDir;

	EditText edtImageTitle;
	ImageView imageCaptionSep;
	TableRow imageCaptionRow;

	ImageView sepratorCheck;

	String caption;

	//Event Map

	Map<String,String> eventMap=new HashMap<String, String>();

	byte[] gallery_byte = null ;

	LocalNotification localNotification;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_broad_cast);


		context=this;
		network=new NetworkCheck(context);
		localNotification=new LocalNotification(getApplicationContext());

		actionbar=getSupportActionBar();
		actionbar.setTitle(getResources().getString(R.string.createBroadCastHeader));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCache");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}else if(network.isNetworkAvailable())
		{

			/*DeleteRecursive(cacheDir);*/
		}

		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		//Setting Current Date

		Date cdt=new Date();

		dateTimeCurrent.setTime(cdt);

		dialog=new Dialog(context);
		remoteDialog=new Dialog(context);

		PHOTO_URL=getResources().getString(R.string.photo_url);

		//ScrollViews
		makePostScroll=(ScrollView)findViewById(R.id.makePostScroll);
		viewPostScroll=(ScrollView)findViewById(R.id.viewPostScroll);

		//Button
		editButton=(Button)findViewById(R.id.editButton);
		postButton=(Button)findViewById(R.id.postButton);

		sepratorCheck=(ImageView)findViewById(R.id.sepratorCheck);

		//TextView
		txtViewOfferDate=(TextView)findViewById(R.id.txtViewOfferDate);
		txtViewOfferDetail=(TextView)findViewById(R.id.txtViewOfferDetail);
		txtViewOfferTitle=(TextView)findViewById(R.id.txtViewOfferTitle);

		//Caption
		edtImageTitle=(EditText)findViewById(R.id.edtImageTitle);
		edtImageTitle.setTypeface(tf);

		//Caption Row
		imageCaptionRow=(TableRow)findViewById(R.id.imageCaptionRow);

		//Caption Row Seprator
		imageCaptionSep=(ImageView)findViewById(R.id.imageCaptionSep);

		/*txtViewOfferTitle.setTextSize(25);
		txtViewOfferDetail.setTextSize(20);*/

		editButton.setText(getString(R.string.edit));

		txtViewOfferTitle.setTypeface(tf);
		txtViewOfferDate.setTypeface(tf);

		reviewImage=(ImageView)findViewById(R.id.reviewImage);

		//Image Thumbnail
		imgThumbPreview=(ImageView)findViewById(R.id.imgThumbPreview);
		deleteImage=(ImageView)findViewById(R.id.deleteImage);

		//Dialogs
		remoteDialog.setContentView(R.layout.remoteurl);
		dialog.setContentView(R.layout.createimagebroadcastdialog);

		//Setting Titles to dialogs
		remoteDialog.setTitle("Enter url");
		dialog.setTitle("Image");

		//Setting animation to dialogs
		dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		remoteDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mImageCaptureUri = Uri.fromFile(mFileTemp);

		FILE_PATH="/sdcard/temp_photo.jpg";

		//Image Chooser button
		btnImageChooser=(Button)findViewById(R.id.btnImageChooser);

		//dialog buttons
		/*		btnChooseImage=(Button)dialog.findViewById(R.id.btnChooseImage);
		btnTakeImage=(Button)dialog.findViewById(R.id.btnCapture);*/
		btnRemoteImage=(Button)dialog.findViewById(R.id.btnRemote);
		btnSubmitUrl=(Button)remoteDialog.findViewById(R.id.btnSubmitUrl);


		btnChooseImage=(Button)findViewById(R.id.btnChooseImage);
		btnTakeImage=(Button)findViewById(R.id.btnCapture);

		btnChooseImage.setTypeface(tf);
		btnTakeImage.setTypeface(tf);


		//Dialog url
		url=(EditText)remoteDialog.findViewById(R.id.edtRemoteUrl);


		//
		//		final int[] ITEM_DRAWABLES = { R.drawable.text, R.drawable.url,
		//				R.drawable.photo, R.drawable.video};

		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		//		BROADCAST_URL=getResources().getString(R.string.server_url)+"test_api";

		/*	BROADCAST_URL="http://192.168.254.37/hyperlocallist/"+getResources().getString(R.string.broadcast_api); */

		Add_BROADCAST_URL=getResources().getString(R.string.add_broadcast);

		/*
		 * API KEY
		 */

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		mAddBroadCast=new AddBroadCastReceiver(new Handler());
		mAddBroadCast.setReceiver(this);


		Bundle b=getIntent().getExtras();

		//Session Manager
		session=new SessionManager(getApplicationContext());



		//Creating broadcast
		edtBroadTitle=(EditText)findViewById(R.id.edtBroadTitle);
		edtBroadDesc=(EditText)findViewById(R.id.edtBroadDesc);
		edtTags=(EditText)findViewById(R.id.edtTags);
		chkIsOffer=(CheckBox)findViewById(R.id.chkIsOffer);
		btnReview=(Button)findViewById(R.id.btnReview);

		prog=(ProgressBar)findViewById(R.id.prog);

		imgBroadCast=(ImageView)findViewById(R.id.imgBroadCast);

		Date current_date=new Date();
		Date current_time=new Date();

		DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss");

		offerDate=(TextView)findViewById(R.id.offerDate);
		offerTime=(TextView)findViewById(R.id.offerTime);


		//Setting Date +1 
		Calendar cal=Calendar.getInstance();

		Date tDate=new Date(cal.getTimeInMillis()+24*3600*1000);


		//		offerDate.setText(dt.format(cdt));
		offerDate.setText(dt.format(tDate));
		offerTime.setText(dt2.format(cdt.getTime()));

		tblSelectDate=(TableRow)findViewById(R.id.tblSelectDate);
		tblselectTime=(TableRow)findViewById(R.id.tblselectTime);

		tblSelectDate.setVisibility(View.GONE);
		tblselectTime.setVisibility(View.GONE);

		offerDate.setTypeface(tf);
		offerTime.setTypeface(tf);

		edtBroadTitle.setTypeface(tf);
		edtBroadDesc.setTypeface(tf);

		edtTags.setTypeface(tf);
		btnReview.setTypeface(tf);



		try
		{

			if(b!=null)
			{
				/*isSplitLogin=b.getBoolean("isSplitLogin", false);*/

				HashMap<String,String>user_details=session.getUserDetails();

				USER_ID=user_details.get(KEY_USER_ID).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID).toString();

				isSplitLogin=session.isLoggedIn();

				/*USER_ID =b.getString("USER_ID");
			AUTH_ID =b.getString("AUTH_ID");*/
				SELECTED_STORE_ID=b.getStringArrayList("SELECTED_STORE_ID");
				title=b.getString("title");
				description=b.getString("description");
				photourl=b.getString("photourl");
				tags=b.getString("tags");
				caption=b.getString("caption");

				Log.i("PASSED DATA", "PASSED DATA title "+title);
				Log.i("PASSED DATA", "PASSED DATA description "+description);
				Log.i("PASSED DATA", "PASSED DATA photourl "+photourl);
				Log.i("PASSED DATA", "PASSED DATA tags "+tags);
				Log.i("PASSED DATA", "PASSED DATA CAPTION "+caption);


				if(title!="")
				{
					edtBroadTitle.setText(title);
				}

				if(caption!="")
				{
					edtImageTitle.setText(caption);
				}

				if(description!="")
				{
					edtBroadDesc.setText(description);
				}
				if(photourl!=null && !photourl.equals(""))
				{
					try
					{


						photourl=photourl.replaceAll(" ","%20");

						if(!photourl.equalsIgnoreCase("notfound"))
						{
							//load images and display it 
							imgThumbPreview.setVisibility(View.VISIBLE);
							reviewImage.setVisibility(View.VISIBLE);
							deleteImage.setVisibility(View.VISIBLE);
							/*imageloader.DisplayImage(PHOTO_URL+photourl,imgThumbPreview);
							imageloader.DisplayImage(PHOTO_URL+photourl,reviewImage);*/

							imageLoader.displayImage(PHOTO_URL+photourl, imgThumbPreview, new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
									// TODO Auto-generated method stub
									//get Bitmap and encode it to Base64.
									bitmapImage=((BitmapDrawable)imgThumbPreview.getDrawable()).getBitmap();
									/*	encodedpath=encodeTobase64(bitmapImage);*/
									FILE_TYPE="image/jpeg";
									FILE_NAME="temp_photo.jpg";

									try
									{
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmapImage.compress(Bitmap.CompressFormat.JPEG, 80, stream);
										gallery_byte = stream.toByteArray();
									}catch(Exception ex)
									{
										ex.printStackTrace();
									}

								}

								@Override
								public void onLoadingCancelled(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}
							});
							imageLoader.displayImage(PHOTO_URL+photourl, reviewImage, new ImageLoadingListener() {

								@Override
								public void onLoadingStarted(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onLoadingCancelled(String arg0, View arg1) {
									// TODO Auto-generated method stub

								}
							});


							isImageBroadCast=true;
							isReuse=true;
							Log.i("PASSED DATA", "PASSED DATA encoded "+encodedpath);

						}
						else
						{
							reviewImage.setVisibility(View.GONE);
							imgThumbPreview.setVisibility(View.GONE);
						}

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
				if(tags!="")
				{
					edtTags.setText(tags);
				}

				/*MERCHANT_BUSINESS_ID=b.getString("MERCHANT_BUSINESS_ID");*/
			}


		}catch(Exception ex)
		{
			ex.printStackTrace();

		}


		btnChooseImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				eventMap.put("source", "Gallery");
				EventTracker.logEvent("NewPost_ImageAdded",eventMap );
				openGallery();
				isRemote=false;
			}
		});

		btnTakeImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				eventMap.put("source", "Camera");
				EventTracker.logEvent("NewPost_ImageAdded",eventMap );
				takePictureFromCustomCamera();
				isRemote=false;
			}
		});




		btnImageChooser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
				alertDialogBuilder.setTitle("City");
				alertDialogBuilder.setMessage("Photo");
				//null should be your on click listener
				alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						takePicture(context);
					}
				});
				alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						choosePict(context);
					}
				});
				alertDialogBuilder.show();*/


				dialog.show();


				btnChooseImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						openGallery();
						isRemote=false;
					}
				});

				btnTakeImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						takePictureFromCustomCamera();
						isRemote=false;
					}
				});

				btnRemoteImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						isRemote=true;
						dialog.dismiss();
						remoteDialog.show();
					}
				});

				btnSubmitUrl.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						remoteurl=url.getText().toString();
						remoteDialog.dismiss();
					}
				});

				/*Button btnLogin=(Button)dialog.findViewById(R.id.btnLoginDialogMerchant);
				Button btnCacel=(Button)dialog.findViewById(R.id.btnLoginDialogCalcel);*/


			}
		});

		chkIsOffer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(chkIsOffer.isChecked())
				{
					is_offered="1";
					tblSelectDate.setVisibility(View.VISIBLE);
					tblselectTime.setVisibility(View.VISIBLE);
					sepratorCheck.setVisibility(View.VISIBLE);
				}else
				{
					is_offered="";
					tblSelectDate.setVisibility(View.GONE);
					tblselectTime.setVisibility(View.GONE);
					sepratorCheck.setVisibility(View.GONE);
				}
			}
		});

		//Review post
		btnReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	

				editButton.setText(getString(R.string.edit));

				if(edtBroadTitle.getText().toString().length()==0)
				{
					Toast.makeText(context, getResources().getString(R.string.titleAlert), Toast.LENGTH_SHORT).show();
				}
				//				else if(edtBroadDesc.getText().toString().length()==0)
				//				{
				//					Toast.makeText(context, getResources().getString(R.string.descAlert), Toast.LENGTH_SHORT).show();
				//				}
				//If its offer
				else if(is_offered.equalsIgnoreCase("1"))
				{
					if(offerDate.getText().toString().length()==0)
					{
						Toast.makeText(context, getResources().getString(R.string.offerDateAlert), Toast.LENGTH_SHORT).show();
					}
					else if(offerDate.getText().toString().length()==0)
					{
						Toast.makeText(context, getResources().getString(R.string.offerTimeAlert), Toast.LENGTH_SHORT).show();
					}
					else
					{
						Log.i("IS OFFER", "IS OFFER "+is_offered);

						makePostScroll.setVisibility(ViewGroup.GONE);
						viewPostScroll.setVisibility(ViewGroup.VISIBLE);



						txtViewOfferDate.setText("Offer valid till "+offerDate.getText().toString()+offerTime.getText().toString());
						txtViewOfferDetail.setText(edtBroadDesc.getText().toString());
						txtViewOfferTitle.setText(edtBroadTitle.getText().toString());


						/*postBroadCast(true);*/
					}
				}
				//If its not offer
				else
				{
					Log.i("IS OFFER", "IS OFFER "+is_offered);
					makePostScroll.setVisibility(ViewGroup.GONE);
					viewPostScroll.setVisibility(ViewGroup.VISIBLE);

					txtViewOfferDate.setText("Offer valid till "+offerDate.getText().toString()+offerTime.getText().toString());
					txtViewOfferDetail.setText(edtBroadDesc.getText().toString());
					txtViewOfferTitle.setText(edtBroadTitle.getText().toString());
					makePostScroll.setVisibility(ViewGroup.GONE);
					viewPostScroll.setVisibility(ViewGroup.VISIBLE);

					/*postBroadCast(false);*/
				}

				if(viewPostScroll.isShown())
				{
					if(is_offered.equalsIgnoreCase("1"))
					{
						txtViewOfferDate.setVisibility(View.VISIBLE);
					}
					else
					{
						txtViewOfferDate.setVisibility(View.INVISIBLE);
					}
				}


			}
		});

		//Allow user to Edit Broadcast
		editButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!prog.isShown())
				{
					editButton.setText(getString(R.string.edit));
					makePostScroll.setVisibility(ViewGroup.VISIBLE);
					viewPostScroll.setVisibility(ViewGroup.GONE);
				}
			}
		});

		//Post Broadcast.
		postButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(edtBroadTitle.getText().toString().length()==0)
				{
					Toast.makeText(context, getResources().getString(R.string.titleAlert), Toast.LENGTH_SHORT).show();
				}
				//				else if(edtBroadDesc.getText().toString().length()==0)
				//				{
				//					Toast.makeText(context,  getResources().getString(R.string.descAlert), Toast.LENGTH_SHORT).show();
				//				}
				//If its offer
				else if(is_offered.equalsIgnoreCase("1"))
				{
					if(offerDate.getText().toString().length()==0)
					{
						Toast.makeText(context,  getResources().getString(R.string.offerDateAlert), Toast.LENGTH_SHORT).show();
					}
					else if(offerDate.getText().toString().length()==0)
					{
						Toast.makeText(context,  getResources().getString(R.string.offerTimeAlert), Toast.LENGTH_SHORT).show();
					}
					else
					{
						Log.i("IS OFFER", "IS OFFER "+is_offered);
						if(!prog.isShown())
						{
							postBroadCast(true);
						}
						else
						{
							Toast.makeText(context, getResources().getString(R.string.postingAlert), Toast.LENGTH_SHORT).show();
						}
					}
				}
				//If its not offer
				else
				{
					Log.i("IS OFFER", "IS OFFER "+is_offered);

					if(!prog.isShown())
					{
						postBroadCast(false);
					}
					else
					{
						Toast.makeText(context, getResources().getString(R.string.postingAlert), Toast.LENGTH_SHORT).show();
					}

				}
			}
		});

		offerDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseDate();
			}
		});
		offerTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseTime();	
			}
		});

		imgBroadCast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//				Intent intent=new Intent(context, CreateImageBroadCast.class);
				//				intent.putExtra("SELECTED_STORE_ID", SELECTED_STORE_ID);
				//				/*		intent.putExtra("MERCHANT_BUSINESS_ID",MERCHANT_BUSINESS_ID);*/
				//				startActivity(intent);
				//				finish();
			}
		});

		//delete image
		deleteImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*imgThumbPreview.setImageResource(R.drawable.placeholder);
				reviewImage.setImageResource(R.drawable.placeholder);*/
				imgThumbPreview.setVisibility(View.GONE);
				reviewImage.setVisibility(View.GONE);

				gallery_byte=null;
				FILE_PATH="";
				FILE_NAME="";
				encodedpath="";
				isImageBroadCast=false;
				deleteImage.setVisibility(View.GONE);
			}
		});
	}

	public static String encodeTobase64(Bitmap image)
	{
		Bitmap immagex=image;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] b = baos.toByteArray();
		String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

		Log.e("LOOK", imageEncoded);
		return imageEncoded;
	}
	public static Bitmap decodeBase64(String input) 
	{
		byte[] decodedByte = Base64.decode(input, 0);
		return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}

	void postBroadCast(boolean isOffer)
	{
		if(network.isNetworkAvailable())
		{

			Intent intent=new Intent(context, AddBroadCastService.class);
			intent.putExtra("addBroadCast",mAddBroadCast);
			intent.putExtra("URL", BROADCAST_URL+Add_BROADCAST_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			/*ArrayList<String>tempStore=new ArrayList<String>();
		tempStore.add(SELECTED_STORE_ID);*/
			intent.putStringArrayListExtra("store_id", SELECTED_STORE_ID);
			intent.putExtra("title", edtBroadTitle.getText().toString());
			intent.putExtra("body", edtBroadDesc.getText().toString());
			intent.putExtra("tags", edtTags.getText().toString());

			intent.putExtra("state", "published");
			intent.putExtra("format", "html");
			intent.putExtra("is_offered", is_offered);
			intent.putExtra("image_title", edtImageTitle.getText().toString());

			if(isOffer)
			{
				//If its Image Broadcast
				if(isImageBroadCast)
				{		
					//if local image
					if(isRemote==false)
					{
						imageType="photo";
						if(FILE_PATH.equals("") && FILE_PATH.length()==0)
						{
							Toast.makeText(context, "Please Select photo to upload", Toast.LENGTH_SHORT).show();
						}
						else
						{

							try
							{
								/*if(!isReuse)
							{*/
								/*if(!FILE_PATH.equals("") && FILE_PATH.length()!=0)
								{
									byte [] b=imageTobyteArray(FILE_PATH);
									if(b!=null)
										encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
									Log.i("Encode ", "Details : "+encodedpath);
								}
								else
								{
									encodedpath="undefined";
								}*/

								if(gallery_byte!=null)
								{
									encodedpath=Base64.encodeToString(gallery_byte, Base64.DEFAULT);
									Log.i("Encode ", "Details : "+encodedpath);
								}
								else
								{
									encodedpath="undefined";
								}

								/*}else
							{

							}*/
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}
						}
					}
					//if remote image
					else
					{
						imageType="url";
						FILE_NAME="";
						FILE_TYPE="";
						encodedpath="";
					}

					intent.putExtra("type", "photo");
					intent.putExtra("isImageBroadCast", isImageBroadCast);
					intent.putExtra("filename", FILE_NAME);
					intent.putExtra("filetype", FILE_TYPE);
					intent.putExtra("userfile", encodedpath);
				}
				intent.putExtra("url", remoteurl);
				intent.putExtra("offer_date_time", offerDate.getText().toString());
				intent.putExtra("offer_time", offerTime.getText().toString());


			}
			else
			{

				//If its Image Broadcast
				if(isImageBroadCast)
				{	
					//if local image
					if(isRemote==false)
					{
						imageType="photo";
						if(FILE_PATH.equals("") && FILE_PATH.length()==0)
						{
							Toast.makeText(context, "Please Select photo to upload", Toast.LENGTH_SHORT).show();
						}
						else
						{
							try
							{
								/*if(!isReuse)
							{*/
								//								if(!FILE_PATH.equals("") && FILE_PATH.length()!=0)
								//								{
								//									byte [] b=imageTobyteArray(FILE_PATH);
								//									if(b!=null)
								//										encodedpath=Base64.encodeToString(b, Base64.DEFAULT);
								//									Log.i("Encode ", "Details : "+encodedpath);
								//								}
								//								else
								//								{
								//									encodedpath="undefined";
								//								}

								if(gallery_byte!=null)
								{
									encodedpath=Base64.encodeToString(gallery_byte, Base64.DEFAULT);
									Log.i("Encode ", "Details : "+encodedpath);
								}
								else
								{
									encodedpath="undefined";
								}

								/*}
							else
							{

							}*/
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}

						}
					}
					//if remote image
					else
					{
						imageType="url";
						FILE_NAME="";
						FILE_TYPE="";
						encodedpath="";


					}
					intent.putExtra("type", "photo");
					intent.putExtra("isImageBroadCast", isImageBroadCast);
					intent.putExtra("filename", FILE_NAME);
					intent.putExtra("filetype", FILE_TYPE);
					intent.putExtra("userfile", encodedpath);
				}
				intent.putExtra("url", remoteurl);
				intent.putExtra("offer_date_time", "");
				intent.putExtra("offer_time", "");

			}

			context.startService(intent);
			prog.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public void onReceiveAddBroadCastResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			prog.setVisibility(View.GONE);
			String broadcast_status=resultData.getString("broadcast_status");
			if(broadcast_status.equalsIgnoreCase("true"))
			{
				try
				{
					DateFormat format; format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
					localNotification.clearPostPrefs();
					localNotification.checkPref();
					localNotification.setPostPref(format.format(new Date()),localNotification.isPostAlarmFired());
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				EventTracker.logEvent("NewPost_Posted", false);
				String msg=resultData.getString("broadcast_msg");
				Toast.makeText(context,  msg, Toast.LENGTH_SHORT).show();
				Intent intent=new Intent(context, ViewPost.class);
				intent.putExtra("STORE_ID",SELECTED_STORE_ID.get(0));
				startActivity(intent);
				finish();
			}
			else if(broadcast_status.equalsIgnoreCase("false"))
			{
				EventTracker.logEvent("NewPost_PostFailed", false);

				String msg=resultData.getString("broadcast_msg");
				showToast(msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					session.logoutUser();
					invalidAuthFinish();
				}
				//			if(msg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				session.logoutUser();
				//				finish();
				//			}
			}
			else if(broadcast_status.equalsIgnoreCase("error"))
			{
				String msg=resultData.getString("broadcast_msg");
				showToast(msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void invalidAuthFinish()
	{
		try
		{

			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					try
					{
						session.logoutUser();
						Intent intent=new Intent(context, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();

			/*showToast(getResources().getString(R.string.invalidAuth)+ " Try logging in again ");*/

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void chooseDate(){
		dateTime.add(Calendar.DAY_OF_MONTH, 1);
		new DatePickerDialog(context, d, dateTime.get(Calendar.YEAR),dateTime.get(Calendar.MONTH),dateTime.get(Calendar.DAY_OF_MONTH)).show();
	}
	public void chooseTime(){
		new TimePickerDialog(context, t, dateTime.get(Calendar.HOUR_OF_DAY), dateTime.get(Calendar.MINUTE), true).show();
	}

	DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			dateTime.set(Calendar.YEAR,year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDate();
		}
	};
	TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE,minute);
			updateTime();
		}
	};
	private void updateTime() {

		DateFormat  dt2=new SimpleDateFormat(" HH:mm:ss");
		miliseconds=dateTime.getTimeInMillis();
		offerTime.setText(dt2.format(dateTime.getTime()));
	}
	private void updateDate() {

		Log.i("Current Date", "Current Date "+dateTimeCurrent.get(Calendar.YEAR)+" "+dateTimeCurrent.get(Calendar.MONTH)+" "+dateTimeCurrent.get(Calendar.DAY_OF_MONTH));
		Log.i("Current Date", "Current Date changed "+dateTime.get(Calendar.YEAR)+" "+dateTime.get(Calendar.MONTH)+" "+dateTime.get(Calendar.DAY_OF_MONTH));

		Date d1=dateTimeCurrent.getTime();
		Date d2=dateTime.getTime();

		if(dateDiff(d2, d1)<0)
		{
			showToast(getResources().getString(R.string.pastDateAlert));
			//reseting calender
			dateTime=Calendar.getInstance();
			chooseDate();
		}
		else
		{
			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMMM-yyyy");
			offerDate.setText(dt.format(dateTime.getTime()));
		}
	}

	public long dateDiff(Date d1,Date d2)
	{
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		Log.i("daysBetween", "daysBetween "+datediff);
		return datediff;
	}





	//Date Differnece
	public static long daysBetween(Calendar startDate, Calendar endDate) {  
		Calendar date = (Calendar) startDate.clone();  
		long daysBetween = 0;  
		while (date.before(endDate)) {  
			date.add(Calendar.DAY_OF_MONTH, 1);  
			daysBetween++;  
		}  

		Log.i("daysBetween", "daysBetween "+daysBetween);
		return daysBetween;  
	} 


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		/*SubMenu subMenu1 = menu.addSubMenu("Extra Settings");

		if(isSplitLogin)
		{
			subMenu1.add("Logout");
		}
		MenuItem subMenu1Item = subMenu1.getItem();
		subMenu1Item.setIcon(R.drawable.action_overflow);
		subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/


		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishto();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		if(item.getTitle().toString().equalsIgnoreCase("Logout"))
		{
			session.logoutUser();
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		if(item.getTitle().toString().equalsIgnoreCase("Inorbit Malls"))
		{
			finishto();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishto();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();	
	}


	/*********************************************** Photo Section ********************************************************/

	//Convert image into byte array
	byte[] imageTobyteArray(String path)
	{	byte[] b = null ;
	if(!path.equals("") || path.length()!=0)
	{


		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inDither=true;//optional
		options.inPreferredConfig=Bitmap.Config.RGB_565;//optional

		Bitmap bm = BitmapFactory.decodeFile(path,options);

		//		options.inSampleSize = calculateInSampleSize(options, 320, 320);
		options.inSampleSize = calculateInSampleSize(options, 480, 480);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		bm=BitmapFactory.decodeFile(path,options);


		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		//		bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
		bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
		b= baos.toByteArray();
	}
	return b;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}


	private void takePicture() {

		/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/

		try {

			/*	mImageCaptureUri = Uri.fromFile(mFileTemp);
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra("return-data", false);
			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);*/
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}

	private void takePictureFromCustomCamera() {

		/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/

		try {

			/*	mImageCaptureUri = Uri.fromFile(mFileTemp);
			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra("return-data", false);
			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);*/
			Intent intent = new Intent(context,CameraView.class);
			intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
			startActivityForResult(intent, REQUEST_CODE_CUSTOMCAMERA);
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}

	private void openGallery() {

		/*	Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);*/
		/*Intent intent = new Intent(context,PhotoActivity.class);
		intent.putExtra("REQUEST_CODE_GALLERY",REQUEST_CODE_GALLERY);
		startActivityForResult(intent, REQUEST_CODE_GALLERY);*/
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_POSTGALLERY);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());

	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		try
		{
			switch (requestCode) {

			case REQUEST_CODE_CUSTOMCAMERA:
				if(resultCode == Activity.RESULT_OK){

					try{
						Bitmap bitmap = null;

						CameraImageSave cameraImageSave = new CameraImageSave();
						bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){


							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							gallery_byte = stream.toByteArray();
							cameraImageSave.deleteFromFile();

							cameraImageSave = null;
							bitmap = null;
						}

						setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0, gallery_byte.length));
					}
					catch(Exception e){ }
				}
				break;

			case REQUEST_CODE_POSTGALLERY:
				if(resultCode == Activity.RESULT_OK) {
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(CreateBroadCast.this, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQUEST_CODE_POSTCROPGALLERY);
				}

				break;

			case REQUEST_CODE_POSTCROPGALLERY:
				if(resultCode == Activity.RESULT_OK){
					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";

					CameraImageSave cameraSaveImage = new CameraImageSave();

					String filePath = cameraSaveImage.getImagePath();


					try{
						Bitmap bitmap = cameraSaveImage.getBitmapFromFile(480);

						if(bitmap!=null){
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							cameraSaveImage.deleteFromFile();
							gallery_byte = stream.toByteArray();
							bitmap = null;
							cameraSaveImage = null;
						}
						setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0,gallery_byte.length));
					}
					catch(Exception e){
						Log.e("Exception","Exception at Create broacast class");
						e.printStackTrace();
					}
				}
				break;
			}

			super.onActivityResult(requestCode, resultCode, data);
		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	//Down scaling bitmap to get Thumbnai
	public  Bitmap getThumbnail(Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;//optional
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;//optional
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional


		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);

		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	/*private void startCropImage() {

		Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 3);

		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}*/


	//Scaling down exisitng bitmap
	public static Bitmap decodeSampledBitmap(byte [] bitmap,  int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		/* BitmapFactory.decodeResource(res, resId, options);*/
		BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length, options);
		/* return BitmapFactory.decodeResource(res, resId, options);*/
	}

	//converting bitmap to byet array
	public static byte[] convertBitmapToByteArray(Bitmap bitmap) {
		if (bitmap == null) {
			return null;
		} else {
			byte[] b = null;
			try {
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.JPEG, 0, byteArrayOutputStream);
				b = byteArrayOutputStream.toByteArray();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return b;
		}
	}


	//Cropping image
	void perFormCrop(Uri picUri)
	{
		try {
			//call the standard crop action intent (the user device may not support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			//indicate image type and Uri
			cropIntent.setDataAndType(picUri, "image/*");
			//set crop properties
			cropIntent.putExtra("crop", "true");
			//indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			//indicate output X and Y
			cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);
			//retrieve data on return
			cropIntent.putExtra("return-data", true);
			//start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);

		}
		catch(ActivityNotFoundException anfe){
			//display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}


	void setPhoto(Bitmap bitmap)
	{

		/*ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			user_file= stream.toByteArray();*/
		try
		{
			FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";

			imgThumbPreview.setImageBitmap(bitmap);
			imgThumbPreview.setScaleType(ScaleType.FIT_XY);
			reviewImage.setImageBitmap(bitmap);
			reviewImage.setScaleType(ScaleType.FIT_XY);
			deleteImage.setVisibility(View.VISIBLE);
			imgThumbPreview.setVisibility(View.VISIBLE);
			reviewImage.setVisibility(View.VISIBLE);

			editButton.setText(getString(R.string.edit));
			editButton.setTextColor(Color.WHITE);

			isImageBroadCast=true;
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Image path for selected image from gallery.
	void setImagePath(Uri uri)
	{

		FILE_PATH=getRealPathFromURI(uri);
		Log.i("Image  : ", "Image Path Choosen : "+getRealPathFromURI(uri));

	}
	void setImagePath(String imagePath)
	{
		FILE_PATH=imagePath;
		Log.i("Image  : ", "Image Path Captured : "+imagePath);
	}


	//Image name for selected image from gallery.
	void setImageName(String imageName)
	{
		FILE_NAME=imageName;
		Log.i("Image  : ", "Image Name : "+imageName);
	}

	//Get absolute path of of Image File
	private String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}


	/**
	 * Get the uri of the captured file
	 * @return A Uri which path is the path of an image file, stored on the dcim folder
	 */
	private  Uri getImageUri() {


		DateFormat  dt=new SimpleDateFormat("d MMM yyyy");
		DateFormat  dt2=new SimpleDateFormat("hh:mm a");
		Date date=new Date();
		/*String CAPTURE_TITLE=dt.format(date).toString()+" "+dt2.format(date).toString()+".jpeg";*/
		String CAPTURE_TITLE="capured"+".jpeg";
		/*			String CAPTURE_TITLE="hi.jpg";*/
		File file = new File(Environment.getExternalStorageDirectory() + "/.citypics", CAPTURE_TITLE);
		Uri imgUri = Uri.fromFile(file);
		Log.i("Captured", "File Path: "+file.getAbsolutePath()+" Name : "+file.getName());
		/*		txtImagePath.setText(file.getAbsolutePath());
					txtImageName.setText(CAPTURE_TITLE);*/
		FILE_PATH=file.getAbsolutePath();
		FILE_TYPE="image/jpeg";
		FILE_NAME=CAPTURE_TITLE;

		return imgUri;
	}





	void finishto()
	{
		if(edtBroadTitle.getText().toString().length()!=0 || edtBroadDesc.getText().toString().length()==0)
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Talk to Customer");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.discardMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					Intent intent=new Intent(context, MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
				}
			})
			;

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		else if(!prog.isShown())
		{
			Intent intent=new Intent(context, MerchantTalkHome.class);
			startActivity(intent);
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		else
		{
			MyToast.showToast(context, "Please wait while posting",3);
		}
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		isImageBroadCast=false;
	}



}

