package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.AppEventsConstants;
import com.facebook.AppEventsLogger;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.MerchantLoginReceiver;
import com.phonethics.networkcall.MerchantLoginService;
import com.phonethics.networkcall.MerchantPasswordReceiver;
import com.phonethics.networkcall.MerchantPasswordService;
import com.phonethics.networkcall.MerchantRegister;
import com.phonethics.networkcall.MerchantResultReceiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.phonethics.networkcall.MerchantLoginReceiver.LoginReceiver;
import com.phonethics.networkcall.MerchantPasswordReceiver.SetPasswordReceiver;
import com.phonethics.networkcall.MerchantResultReceiver.MerchantRegisterInterface;
import com.urbanairship.push.PushManager;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SignupCustomer extends SherlockFragmentActivity implements SetPasswordReceiver, LoginReceiver,  MerchantRegisterInterface {


	ActionBar actionBar;

	ArrayList<String>pages=new ArrayList<String>();
	static Activity context;


	static String API_HEADER;
	static String API_VALUE;

	String SET_PASSWORD_PATH;
	String REGISTER_PATH;


	String LOGIN_PATH;
	String LOGIN_URL;

	ProgressBar shopProgress;
	//Session Manger Class
	SessionManager session;

	String mobileno="";

	static String STORE_URL;
	static String SOTRES_PATH;

	static String USER_ID="";
	static String AUTH_ID="";

	Button buttonEdit;
	TextView txtCustomerMobNo;
	Button signUpResendButton;
	Button signUpNext;
	static EditText  shopVerificationCode;
	TextView serverMessage;

	NetworkCheck isnetConnected;

	String REGISTER_URL;

	String password;
	String mobile_no;

	//Set Merchant Password Receiver
	public MerchantPasswordReceiver mPasswordRecevier;
	//Login Recevier
	public MerchantLoginReceiver mLoginRecevier;

	//All Stores
	GetAllStores_Of_Perticular_User_Receiver mAllStores;

	String verification_code;

	MerchantResultReceiver mRegister;

	String server_message;

	boolean isFromTour=false;

	boolean isVeriDone;
	
	DBUtil dbutil;
	String activeArea="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_customer);

		context=this;
		//Database
		dbutil=new DBUtil(context);

		session=new SessionManager(context);

		isnetConnected=new NetworkCheck(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
		actionBar.show();
		actionBar.setDisplayHomeAsUpEnabled(true);

		//		buttonEdit=(Button)findViewById(R.id.buttonEdit);
		txtCustomerMobNo=(TextView)findViewById(R.id.txtCustomerMobNo);
		signUpResendButton=(Button)findViewById(R.id.signUpResendButton);
		signUpNext=(Button)findViewById(R.id.signUpNext);
		shopVerificationCode=(EditText)findViewById(R.id.shopVerificationCode);
		serverMessage=(TextView)findViewById(R.id.serverMessage);

		mRegister=new MerchantResultReceiver(new Handler());
		mRegister.setReceiver(this);

		REGISTER_URL=getResources().getString(R.string.server_url) + getResources().getString(R.string.user_api);

		//Defining URL's
		LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api);
		LOGIN_PATH=getResources().getString(R.string.login);

		REGISTER_PATH=getResources().getString(R.string.register_new);
		SET_PASSWORD_PATH=getResources().getString(R.string.new_set_password);

		//Api Headers
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);


		shopProgress=(ProgressBar)findViewById(R.id.customerProgressBar);

		mPasswordRecevier=new MerchantPasswordReceiver(new Handler());
		mPasswordRecevier.setReceiver(this);

		mLoginRecevier=new MerchantLoginReceiver(new Handler());
		mLoginRecevier.setReceiver(this);

		Bundle b=getIntent().getExtras();

		if(b!=null)
		{
			try
			{
				mobileno=b.getString("mobileno");
				server_message=b.getString("server_message");
				String txtNum="Edit: +"+mobileno;
				isFromTour=b.getBoolean("isFromTour",false);
				//				txtCustomerMobNo.setText(mobileno);
				txtCustomerMobNo.setText(Html.fromHtml("<u>"+txtNum+"</u>"));
				serverMessage.setText(server_message);

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}


		txtCustomerMobNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				context.finish();
				context.overridePendingTransition(0,R.anim.shrink_fade_out_center);
			}
		});

		signUpNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				verification_code=shopVerificationCode.getText().toString();
				password=shopVerificationCode.getText().toString();

				Log.d("CODE","CODE " + verification_code);

				if(verification_code.length()!=0)
				{
					callSetPasswordService();	
				}
				else
				{
					showToast("Please enter verification code");
				}

			}
		});
		signUpResendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callRegistrationApi();
			}
		});
		
		activeArea=dbutil.getActiveAreaID();
		isVeriDone = false;

	}

	void callRegistrationApi()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//Calling Register Api to Register Merchant.
			Intent intent=new Intent(context, MerchantRegister.class);
			intent.putExtra("merchantRegister", mRegister);
			intent.putExtra("URL", REGISTER_URL+REGISTER_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("mobile_no",mobileno);
			context.startService(intent);
			shopProgress.setVisibility(View.VISIBLE);
		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	void loginService()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, MerchantLoginService.class);
			intent.putExtra("merchantLogin", mLoginRecevier);
			intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("contact_no", mobileno);
			intent.putExtra("password", password);
			context.startService(intent);
			shopProgress.setVisibility(View.VISIBLE);

		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}
	void callSetPasswordService()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, MerchantPasswordService.class);
			intent.putExtra("URL", REGISTER_URL+SET_PASSWORD_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("setPassword", mPasswordRecevier);
			intent.putExtra("password", password);
			intent.putExtra("mobile_no", mobileno);
			/*if(ForgetPassword==1)
			{*/
			intent.putExtra("verification_code", verification_code);
			intent.putExtra("isForgotPassword",true);
			/*}*/
			context.startService(intent);
			shopProgress.setVisibility(View.VISIBLE);
		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public void onReceiverMerchantRegister(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			shopProgress.setVisibility(View.GONE);
			String passstatus=resultData.getString("passstatus");
			final String passmsg=resultData.getString("passmsg");
			String error_code=resultData.getString("error_code");

			//Saving Number in Prefs
			session.createUser(mobileno);

			if(error_code.equalsIgnoreCase("-223")) //New user
			{
				EventTracker.logEvent("CSignUp_New", false);
			}
			else if(error_code.equalsIgnoreCase("-202")) //Returning user
			{
				EventTracker.logEvent("CSignUp_Return", false);
			}

			if(passstatus.equalsIgnoreCase("true"))
			{


				EventTracker.logEvent("CSignUp_New", false);


				/*callVerifyTelcoMobileApi();*/
			}
			else
			{


				showToast(passmsg);
				if(passmsg.startsWith(getResources().getString(R.string.connectionTimedOut)))
				{

				}
				/*if(passmsg.startsWith("Hey! It seems, You are already registered on ShopLocal. We are sending a verification code to your mobile number. Just enter it below, choose a new password and you are good to go."))*/
				else
				{

					EventTracker.logEvent("CSignUp_Return", false);


				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	@Override
	public void onReceiveLoginResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		try
		{
			shopProgress.setVisibility(View.GONE);
			String auth_id=resultData.getString("login_authCode");
			String loginstatus=resultData.getString("loginstatus");
			String loginid=resultData.getString("loginid");

			USER_ID=loginid;
			AUTH_ID=auth_id;

			if(loginstatus.equalsIgnoreCase("true"))
			{
				Log.i("isFromTour", "isFromTour "+isFromTour);
				if(isFromTour)
				{
					EventTracker.logEvent("Tour_SignUpSuccess", false);
				}
				else
				{
					EventTracker.logEvent("CSignup_ShoplocalLoginSuccess", false);
				}

				//Clear saved stae of user.
				//clearPageState();

				session.createLoginSessionCustomer(mobileno, password.toString(), USER_ID, AUTH_ID, false);

				// registration completes

				AppEventsLogger logger = AppEventsLogger.newLogger(this);

				Bundle parameters = new Bundle();
				parameters.putString("User Type", "Customer");
				parameters.putString("Login Type", "Mobile");

				logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION,parameters);

				setCustomerTag(USER_ID);
				if(activeArea.length()!=0)
				{
					setAreaTag(activeArea);
				}

				/*	Intent intent=new Intent(context, HomeGrid.class);
				intent.putExtra("isSplitLogin",true);
				intent.putExtra("choiceMode", 2);
				intent.putExtra("viewPost", 0);
				startActivity(intent);*/
				Intent intent=new Intent();
				context.setResult(2, intent);

				context.finish();
			}
			else 
			{
				EventTracker.logEvent("CSignup_ShoplocalLoginFailed", false);

				Toast.makeText(context,auth_id, Toast.LENGTH_SHORT).show();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void setCustomerTag(String userId)
	{
		try
		{
			Log.i("URBAN", "URBAN SET TAG");
			boolean isMisMatch=false;


			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			ArrayList<String>tempIdList=new ArrayList<String>();

			String temp="";
			String replace="";
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			//Traversing Tags to find out if there are any tags existing with own_id

			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains customer_id_
				if(temp.startsWith("customer_id_"))
				{
					//Replace customer_id_ with "" and compare it with ID
					replace=temp.replaceAll("customer_id_", "");
					tempIdList.add(replace);
					Log.i("TAG", "Urban TAG ID "+replace);

					if(!userId.equalsIgnoreCase(replace))
					{
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false && tempIdList.size()==0)
			{
				isMisMatch=true;
			}


			if(isMisMatch==true)
			{

				//Clear customer_id_ from Tag List
				for(int i=0;i<tempIdList.size();i++)
				{
					tags.remove("customer_id_"+tempIdList.get(i));
				}


				tags.add("customer_id_"+userId);

				Log.i("TAG", "Urban TAG AFTER "+tags.toString());

				PushManager.shared().setTags(tags);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	void setAreaTag(String area)
	{
		Log.i("URBAN", "URBAN SET TAG");
		boolean isMisMatch=false;

		//Adding Tags

		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			//Traversing Tags to find out if there are any tags existing with own_id

			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains area_
				if(temp.startsWith("area_"))
				{
					//Replace area_ with "" and compare it with ID
					replace=temp.replaceAll("area_", "");
					tempIdList.add(replace);
					Log.i("TAG", "Urban TAG ID "+replace);

					if(!area.equalsIgnoreCase(replace))
					{
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false && tempIdList.size()==0)
			{
				isMisMatch=true;
			}


			if(isMisMatch==true)
			{

				//Clear area_ from Tag List
				for(int i=0;i<tempIdList.size();i++)
				{
					tags.remove("area_"+tempIdList.get(i));
				}


				tags.add("area_"+area);

				Log.i("TAG", "Urban TAG AFTER "+tags.toString());

				PushManager.shared().setTags(tags);
			}
	}

	@Override
	public void onReceivePasswordResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		shopProgress.setVisibility(View.GONE);
		String passstatus=resultData.getString("passstatus");
		String passmsg=resultData.getString("passmsg");
		String error_code=resultData.getString("error_code");

		if(error_code.equalsIgnoreCase("-207")) //Setpassword success
		{
		}
		else if(error_code.equalsIgnoreCase("-209")) //Setpassword failure
		{
			EventTracker.logEvent("CSignUp_VerifyCodeInvalid", false);

		}

		if(passstatus.equalsIgnoreCase("true"))
		{
			//showToast("Password set successfully!");
			/*Intent intent=new Intent(context,ShopLocalMerchantInfo.class);
				startActivity(intent);
				context.finish();*/
			EventTracker.logEvent("CSignup_VerifyCodeDone", false);

			loginService();


		}
		else
		{
			showToast(passmsg);
			//				if(passmsg.startsWith("Your mobile number is not verified yet. Please press call or give a missed call on '01246758608'."))
			//				{
			//					viewCustomer.setCurrentItem(2, true);
			//				}
		}

	}


	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*Intent intent=new Intent(context,HomeGrid.class);
		startActivity(intent);*/
		finishTo();
		return true;
	}

	void finishTo()
	{
		//savedPageState();
		mobileno="";
		//		ForgetPassword=0;
		//		isRegistrationCalled=false;
		//		startTime=-1;
		//		endTime=-1;
		Intent intent=new Intent();
		context.setResult(2, intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();

	}
	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		if(isVeriDone == false){
			EventTracker.logEvent("CSignup_VerifyCode", false);
			isVeriDone = true;
		}
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}


	// Auto Fill verification code

	public static class VerificationBroadCastCustomer extends BroadcastReceiver
	{


		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String verification_code= intent.getStringExtra("verification_code");
			setVerificationCode(verification_code);
		}

	}

	static void setVerificationCode(String verification_code)
	{
		//signup page details.
		Log.i("Verification Code : ", "Verification Code customer : "+verification_code);
		//EditText tempshopVerificationCode=(EditText)viewCustomer.findViewById(R.id.shopVerificationCodeCustomer);

		shopVerificationCode.setText(verification_code);

	}

}
