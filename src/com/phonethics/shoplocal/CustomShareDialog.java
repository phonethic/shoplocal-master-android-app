package com.phonethics.shoplocal;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.SharedListViewAdapter;

public class CustomShareDialog {
	
	//Context context;
	Dialog dialogShare;
	ListView listViewShare;
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	
	/*public CustomShareDialog(Context context){
		this.context = context;
	}*/
	
	void showDialog(final Activity activity, final ArrayList<String> appName, final ArrayList<String> packageNames, ArrayList<Drawable> appIcon, final ArrayList<String> photo_url1
			, final ArrayList<String> news_title, final String PHOTO_PARENT_URL, final ArrayList<String> offer_title, 
			final ArrayList<String> offer_desc, final UiLifecycleHelper uiHelper){
		dialogShare = new Dialog(activity);
		
		dialogShare.setContentView(R.layout.sharedialog);
		
		dialogShare.setTitle("Select an action");
		
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(activity, 0, activity.getLayoutInflater(), appName, packageNames, appIcon));
		dialogShare.show();
		
		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook")){
					
					if(photo_url1.get(0).toString().length()!=0)
					{
						String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");

						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(activity)
						.setLink("http://shoplocal.co.in")
						.setName(news_title.get(position) + " - " + "Offers")
						.setPicture(PHOTO_PARENT_URL+photo_source)
						.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
						.build();
						uiHelper.trackPendingDialogCall(shareDialog.present());
						dismissDialog();
					}
					else
					{
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(activity)
						.setLink("http://shoplocal.co.in")
						.setName(news_title.get(position) + " - " + "Offers")
						.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
						.build();
						uiHelper.trackPendingDialogCall(shareDialog.present());
						dismissDialog();
					}
				}
				else{
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					activity.startActivity(i);
				}
			}
		});
	}
	
	void dismissDialog(){
		dialogShare.dismiss();
	}

}
