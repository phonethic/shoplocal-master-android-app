package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.actionbarsherlock.widget.SearchView;
import com.facebook.AppEventsConstants;
import com.facebook.AppEventsLogger;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.DrawerAdapter;
import com.phonethics.model.EntryItem;
import com.phonethics.model.Item;
import com.phonethics.model.NewsFeedModel;
import com.phonethics.networkcall.FavouritePostResultReceiver;
import com.phonethics.networkcall.FavouritePostResultReceiver.FavouritePostInterface;
import com.phonethics.networkcall.FavouritePostService;
import com.phonethics.networkcall.GetPlaceCategoryReceiver;
import com.phonethics.networkcall.GetPlaceCategoryReceiver.GetCategory;
import com.phonethics.networkcall.GetPlaceCategoryService;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;
import com.phonethics.networkcall.NewsFeedReceiver;
import com.phonethics.networkcall.NewsFeedReceiver.NewsFeed;
import com.phonethics.networkcall.NewsFeedService;
import com.phonethics.networkcall.SearchResultReceiver;
import com.phonethics.networkcall.SearchResultReceiver.SearchResult;
import com.phonethics.networkcall.SearchService;
import com.phonethics.networkcall.ShareResultReceiver;
import com.phonethics.networkcall.ShareResultReceiver.ShareResultInterface;
import com.phonethics.networkcall.ShareService;
/*import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;*/
import com.phonethics.shoplocal.MyShoplocal.NewsFeedAdapter;
import com.squareup.picasso.Picasso;




public class SearchActivity extends SherlockActivity implements SearchResult, OnClickListener, OnNavigationListener,GetCategory, OnCheckedChangeListener, Receiver, NewsFeed, FavouritePostInterface, ShareResultInterface {

	Activity context;
	ActionBar actionBar;

	static String STORE_URL;
	static String NEWS_FEED_SEARCH_URL;
	static String LOGIN_URL;
	static String API_HEADER;
	static String API_VALUE;
	static String STORE_CATEGORY;

	static String GET_SEARCH_URL;

	static String offer="";

	//search layout
	EditText edtSearch;
	ProgressBar searchListProg;
	Button btnSearchLoadMore;
	ListView listCategory;

	PullToRefreshListView listSearchResult;
	PullToRefreshListView listNewsFeedSearch;


	RelativeLayout searchListFilter;
	RelativeLayout relListResult;
	TextView txtSearchCounter;
	TextView txtSearchCounterBack;
	ImageView imgSearchButton;
	ImageView imgSearchClose;


	String TOTAL_SEARCH_PAGES="";
	String TOTAL_SEARCH_RECORDS="";
	static String PHOTO_PARENT_URL;

	//Search
	ArrayList<String> Search_ID=new ArrayList<String>();
	ArrayList<String> Search_SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> Search_BUILDING=new ArrayList<String>();
	ArrayList<String> Search_STREET=new ArrayList<String>();
	ArrayList<String> Search_LANDMARK=new ArrayList<String>();
	ArrayList<String> Search_AREA=new ArrayList<String>();
	ArrayList<String> Search_CITY=new ArrayList<String>();
	ArrayList<String> Search_STATE=new ArrayList<String>();
	ArrayList<String> Search_COUNTRY=new ArrayList<String>();
	ArrayList<String> Search_MOBILE_NO=new ArrayList<String>();
	ArrayList<String> Search_PHOTO=new ArrayList<String>();
	ArrayList<String> Search_DISTANCE=new ArrayList<String>();
	ArrayList<String> Search_DESCRIPTION=new ArrayList<String>();
	ArrayList<String>category=new ArrayList<String>();
	ArrayList<String>category_id=new ArrayList<String>();

	ArrayList<String> Search_HAS_OFFER=new ArrayList<String>();
	ArrayList<String> Search_OFFER=new ArrayList<String>();

	//Newsfeed

	ArrayList<String> NEWS_FEED_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_NAME=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_AREA_ID=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_MOBNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO3=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_TELLNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_LATITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_LONGITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DISTANCE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_POST_TYPE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_TITLE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DESCRIPTION=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_image_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_thumb_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url3=new ArrayList<String>();

	ArrayList<String> NEWS_DATE=new ArrayList<String>();
	ArrayList<String> NEWS_IS_OFFERED=new ArrayList<String>();
	ArrayList<String> NEWS_OFFER_DATE_TIME=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_place_total_like=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_place_total_share=new ArrayList<String>();


	int search_post_count=20;
	int search_page=0;

	static boolean isCheckOffers=false;
	boolean isSearchClicked=false;

	SearchResultReceiver searchReceiver;
	GetPlaceCategoryReceiver mStoreCategoryReceiver;
	LocationResultReceiver mReceiver;
	NewsFeedReceiver newsFeedReceiver;

	NetworkCheck isnetConnected;

	//Session Manger
	SessionManager session;
	boolean isSplitLogin=false;

	/*	SlidingMenu slide_menu ;*/

	//Slide menu
	TextView txtSlideMenu_Register;
	TextView txtSlideMenu_News;
	TextView txtSlideMenu_StoreList;
	TextView txtSlideMenu_Favourite;
	TextView txtSlideMenu_Refresh;
	TextView txtSlideMenu_Dashboard;
	TextView txtSlideMenu_Search;
	TextView txtSlideMenu_Setting;
	TextView txtSlideMenu_Logout;

/*	static ImageLoader imageLoaderList;*/

	String business_id="292";

	CategoryListAdapter categAdapter;

	RelativeLayout categoryLayout;

	CheckBox chkBox;
	int sposition=0;
	String locality="Lokhandwala";

	boolean isRefreshing=false;


	String latitued="";
	String longitude="";

	static boolean IS_SHOPLOCAL=false;

	ArrayList<String>dropdownItems=new ArrayList<String>();
	String JSON_STRING;
	String key="area_id";
	String value;

	int dropdown_position;

	boolean isLocationFound=false;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ListView drawerList;

	private static long back_pressed;

	String drawer_item="";

	DrawerAdapter drawerAdapter;

	boolean isGenericSearch=false;
	boolean isNewsCategory=false;
	String search_text;

	//Database
	DBUtil dButil;

	NewsFeedAdapter adapter=null;

	//Newsfeed
	static String USER_ID;
	static String AUTH_ID;

	String VIA = "Android";

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	int POST_LIKE=2;

	ShareResultReceiver shareServiceResult;
	FavouritePostResultReceiver mFav;

	String TEMP_POST_ID="";

	static String BROADCAST_URL;
	static String LIKE_API;
	static String SHARE_URL;

	SearchApiListAdapter sadapter;

	Map<String, String> eventMap;
	Map<String,String> searchMap;

	String areaName = "";

	private UiLifecycleHelper uiHelper;
	
	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;
	
	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		context=this;
		session=new SessionManager(getApplicationContext());
		isSplitLogin=session.isLoggedIn();
		dButil=new DBUtil(context);

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		eventMap=new HashMap<String, String>();  
		searchMap=new HashMap<String, String>();

		EventTracker.startLocalyticsSession(context);


		JSON_STRING=getResources().getString(R.string.dropdownjson);

		/*IS_SHOPLOCAL=getResources().getBoolean(R.bool.isShopLocal);*/
		Log.i("IS_SHOPLOCAL", "IS_SHOPLOCAL "+JSON_STRING);

		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

		mFav=new FavouritePostResultReceiver(new Handler());
		mFav.setReceiver(this);

		shareServiceResult = new ShareResultReceiver(new Handler());
		shareServiceResult.setReceiver(this);

		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		LIKE_API=getResources().getString(R.string.like);

		SHARE_URL = "/" + getResources().getString(R.string.share);



		chkBox=(CheckBox)findViewById(R.id.chkBox);
		chkBox.setOnCheckedChangeListener(this);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		btnSearchLoadMore=(Button)findViewById(R.id.btnSearchLoadMore);

		listSearchResult=(PullToRefreshListView)findViewById(R.id.listSearchResult);
		listNewsFeedSearch=(PullToRefreshListView)findViewById(R.id.listNewsFeedSearch);


		categoryLayout=(RelativeLayout)findViewById(R.id.categoryLayout);
		searchListFilter=(RelativeLayout)findViewById(R.id.searchListFilter);
		relListResult=(RelativeLayout)findViewById(R.id.relListResult);
		txtSearchCounter=(TextView)findViewById(R.id.txtSearchCounter);
		//		txtSearchCounterBack=(TextView)findViewById(R.id.txtSearchCounterBack);
		imgSearchButton=(ImageView)findViewById(R.id.imgSearchButton);
		imgSearchClose=(ImageView)findViewById(R.id.imgSearchClose);
		searchListProg=(ProgressBar)findViewById(R.id.searchListProg);

		//		txtSearchCounterBack.getBackground().setAlpha(200);

		//Search List Categories

		listCategory=(ListView)findViewById(R.id.listCategory);





		isnetConnected=new NetworkCheck(context);

		//SearchReceiver
		searchReceiver=new SearchResultReceiver(new Handler());
		searchReceiver.setReceiver(this);

		mReceiver=new LocationResultReceiver(new Handler());
		mReceiver.setReceiver(this);

		newsFeedReceiver=new NewsFeedReceiver(new Handler());
		newsFeedReceiver.setReceiver(this);

		//Category Receiver
		mStoreCategoryReceiver=new GetPlaceCategoryReceiver(new Handler());
		mStoreCategoryReceiver.setReceiver(this);

		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		NEWS_FEED_SEARCH_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		/*STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.dummy_search);*/
		GET_SEARCH_URL=getResources().getString(R.string.searchapi);

		//Store Category
		STORE_CATEGORY=getResources().getString(R.string.place_category);

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//Api Key

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		actionBar=getSupportActionBar();
		actionBar.setTitle("Categories");
		//		actionBar.setIcon(R.drawable.ic_drawer);
		//		actionBar.setHomeButtonEnabled(true);
		actionBar.show();


		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);


		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ListView)findViewById(R.id.drawerList);

		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		actionBar.setHomeButtonEnabled(false);
		
	
		//Creating Drawer
		//		createDrawer();

		sadapter=new SearchApiListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, Search_SOTRE_NAME, Search_PHOTO,Search_HAS_OFFER,Search_OFFER,Search_DESCRIPTION,Search_DISTANCE);
		listSearchResult.setAdapter(sadapter);

		adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
				NEWS_FEED_place_total_like, NEWS_FEED_place_total_share,NEWS_FEED_MOBNO1,NEWS_FEED_MOBNO2,NEWS_FEED_MOBNO3,NEWS_FEED_TELLNO1,NEWS_FEED_TELLNO2,NEWS_FEED_TELLNO3,NEWS_OFFER_DATE_TIME);


		listNewsFeedSearch.setAdapter(adapter);

		try
		{


			Bundle b=getIntent().getExtras();
			if(b!=null)
			{
				search_text=b.getString("search_text");
				isGenericSearch=b.getBoolean("genericSearch",false);
				isNewsCategory=b.getBoolean("isNewsCategory",false);

				if(isNewsCategory)
				{
					//					actionBar.setTitle("Categories");
					listNewsFeedSearch.setVisibility(View.VISIBLE);
					listSearchResult.setVisibility(View.GONE);
					Log.i("VISIBILIETY", "VISIBILIETY News Vs Search isNewsCategory"+isNewsCategory+" "+listNewsFeedSearch.isShown()+" "+listSearchResult.isShown());
				}
				else
				{
					listNewsFeedSearch.setVisibility(View.GONE);
					listSearchResult.setVisibility(View.VISIBLE);
					Log.i("VISIBILIETY", "VISIBILIETY News Vs Search isNewsCategory"+isNewsCategory+" "+listNewsFeedSearch.isShown()+" "+listSearchResult.isShown());
				}

				if(isGenericSearch)
				{
					offer="1";
					isCheckOffers=true;
					chkBox.setChecked(true);
					edtSearch.setText(search_text);

					//					EventTracker.logEvent("Search_StoreText", false);

					//					key="locality";
					//					value="Lokhandwala, Andheri West";

					//Searching

					if(isnetConnected.isNetworkAvailable())
					{
						if(edtSearch.getText().toString().length()!=0)
						{

							/*		animaSlidUp.setFillAfter(true);*/
							Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
							animaSlidDown.setFillAfter(true);

							//Temporarily disabled
							searchListFilter.startAnimation(animaSlidDown);

							categoryLayout.startAnimation(animaSlidDown);


							isSearchClicked=true;
							if(isSearchClicked==true)
							{
								clearSearchArray();
							}

							//							if(IS_SHOPLOCAL)
							//							{
							//							if(latitued.length()==0 && longitude.length()==0)
							//							{
							//								locationService();
							//							}
							//							else
							//							{
							if(isNewsCategory)
							{
								clearArray();
								loadNewsFeeds();
							}
							else
							{
								searchApi();
							}
							///							}
							//							else
							//							{
							//								searchApi();
							//							}
							categoryLayout.setVisibility(View.GONE);
							if(chkBox.isChecked())
							{
								chkBox.setChecked(false);
								offer="";
								isCheckOffers=false;
							}


							/*InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);*/

							try
							{
								//Hiding keyboard
								InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
							}catch(NullPointerException npe)
							{
								npe.printStackTrace();
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}

							relListResult.setVisibility(View.VISIBLE);

							animaSlidDown.setAnimationListener(new AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationRepeat(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									//Temporarily disabled
									searchListFilter.setVisibility(View.GONE);
									/*relListResult.startAnimation(animaSlidUp);*/
									relListResult.setVisibility(View.VISIBLE);
									//Hiding keyboard

								}
							});
						}
						else
						{

						}
					}
					else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}

				}
				else
				{
					offer="";
					isCheckOffers=false;
					chkBox.setChecked(false);	
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				//				actionBar.setTitle(getResources().getString(R.string.searchTitle));
				if(isNewsCategory)
				{
					actionBar.setTitle(getResources().getString(R.string.offerCategoryTitle));
				}
				else
				{
					actionBar.setTitle(getResources().getString(R.string.searchTitle));
				}
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);



		DrawerAdapter adapter=new DrawerAdapter(context, 0, items);

		drawerList.setAdapter(adapter);

		drawerList.setOnItemClickListener(new DrawerItemClickListener());

		//List Navigation

		//		Context acontext = getSupportActionBar().getThemedContext();
		/*ArrayAdapter<CharSequence> business_list = ArrayAdapter.createFromResource(acontext, R.array.business_name, R.layout.list_menus);
		business_list.setDropDownViewResource(R.layout.list_dropdown_item);*/

		//parse json of dropdown and set it to navigation
		//		parseJson(JSON_STRING);

		//Create Adapter
		//		CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(context, 0, 0, dropdownItems);


		//Setting Navigation Type.
		//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		//		actionBar.setListNavigationCallbacks(business_list, this);
		//		actionBar.setSelectedNavigationItem(dropdown_position);

		/*		SharedPreferences pref=context.getSharedPreferences("dropdown", 0);*/



		/*if(business_id==getResources().getString(R.string.malad_id))
		{
			sposition=0;
		}
		else if(business_id==getResources().getString(R.string.vashi_id))
		{
			sposition=1;
		}
		else if(business_id==getResources().getString(R.string.cyberabad_id))
		{
			sposition=2;
		}
		else if(business_id==getResources().getString(R.string.whitefield_id))
		{
			sposition=3;
		}


		actionbar.setSelectedNavigationItem(sposition);*/

		//Session Manager



		try
		{
			//			SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
			//			dropdown_position=pref.getInt("dropdown_position",0);
			//			Log.i("dropdown_position", "dropdown_position"+dropdown_position);
			value=dButil.getActiveAreaID();

			if(value.length()!=0){


				areaName = dButil.getAreaNameById(Integer.parseInt(value));
			}



			getLocationFromPrefs();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		category.add("Offers");
		category_id.add("-1");



		searchListFilter.setVisibility(View.VISIBLE);
		relListResult.setVisibility(View.GONE);
		categoryLayout.setVisibility(View.GONE);

		categAdapter=new CategoryListAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher,category);
		listCategory.setAdapter(categAdapter);

		if(isnetConnected.isNetworkAvailable())
		{
			loadAllCategories();
		}
		else
		{
			MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

		}

		//NewsFeed Refresh Listener

		listNewsFeedSearch.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				isRefreshing=true;
				try
				{
					if(isnetConnected.isNetworkAvailable())
					{
						if(edtSearch.getText().toString().length()!=0)
						{
							//							search_page=1;
							search_page=0;
							search_post_count=20;
							//							if(IS_SHOPLOCAL)
							//							{
							/*if(latitued.equalsIgnoreCase("not found") && longitude.equalsIgnoreCase("not found"))
								{
									locationService();
								}
								else
								{
									searchApi();
								}*/

							loadNewsFeeds();

							//							}
							//							else
							//							{
							//								searchApi();
							//							}
						}
					}else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		//Refresh Listener
		listSearchResult.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				isRefreshing=true;
				try
				{
					if(isnetConnected.isNetworkAvailable())
					{
						if(edtSearch.getText().toString().length()!=0)
						{
							//							search_page=1;
							search_page=0;
							search_post_count=20;
							//							if(IS_SHOPLOCAL)
							//							{
							/*if(latitued.equalsIgnoreCase("not found") && longitude.equalsIgnoreCase("not found"))
								{
									locationService();
								}
								else
								{
									searchApi();
								}*/

							searchApi();

							//							}
							//							else
							//							{
							//								searchApi();
							//							}
						}
					}else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}
		});


		listNewsFeedSearch.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub
				try
				{
					if(isnetConnected.isNetworkAvailable())
					{

						if(NEWS_FEED_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
						{
							Log.i("ID SIZE","ID SIZE "+NEWS_FEED_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
							btnSearchLoadMore.setVisibility(View.GONE);
						}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
						{
							if(!searchListProg.isShown())
							{

								//								search_page++;
								Log.i("Page", "Page "+search_page);
								loadNewsFeeds();;
							}
							else
							{
								Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
							}

						}
						else
						{
							Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
							btnSearchLoadMore.setVisibility(View.GONE);
						}
					}
					else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
		//On Last item visible

		listSearchResult.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub
				try
				{
					if(isnetConnected.isNetworkAvailable())
					{

						if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
						{
							Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
							btnSearchLoadMore.setVisibility(View.GONE);
						}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
						{
							if(!searchListProg.isShown())
							{

								//								search_page++;
								Log.i("Page", "Page "+search_page);
								//								if(IS_SHOPLOCAL)
								//								{
								if(latitued.equalsIgnoreCase("not found") && longitude.equalsIgnoreCase("not found"))
								{
									locationService();
								}
								else
								{
									searchApi();
								}
								//								}
								//								else
								//								{
								//									searchApi();
								//								}
								/*isSearchClicked=false;*/
							}
							else
							{
								Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
							}

						}
						else
						{
							Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
							btnSearchLoadMore.setVisibility(View.GONE);
						}
					}
					else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		btnSearchLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isnetConnected.isNetworkAvailable())
				{

					if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
					{
						Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnSearchLoadMore.setVisibility(View.GONE);
					}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
					{
						if(!searchListProg.isShown())
						{

							search_page++;
							Log.i("Page", "Page "+search_page);
							searchApi();
							/*isSearchClicked=false;*/
						}
						else
						{
							Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
						}

					}
					else
					{
						Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnSearchLoadMore.setVisibility(View.GONE);
					}
				}

			}
		});

		//imgSearchButton click 
		imgSearchButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//Animating EditText
				Animation animation = new ScaleAnimation(0, 1, 1,1, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 1);
				animation.setDuration(250);
				edtSearch.setVisibility(View.VISIBLE);
				//commented purposely
				/*imgSearchClose.setVisibility(View.VISIBLE);
				imgSearchButton.setVisibility(View.GONE);*/
				edtSearch.startAnimation(animation);
				animation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						/*	searchListFilter.setVisibility(View.VISIBLE);*/
						//Search Layout

						Animation animaSlidUp=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						/*		animaSlidUp.setFillAfter(true);*/

						//Temporarily disabled 
						searchListFilter.startAnimation(animaSlidUp);
						/*relDefault.setVisibility(View.GONE);
								if(relDefaultSearchProgress.isShown())
								{
									relDefaultSearchProgress.setVisibility(ViewGroup.GONE);
								}*/
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						//Temporarily disabled 
						searchListFilter.setVisibility(View.VISIBLE);

						// TODO Auto-generated method stub
						//Hiding keyboard
						/*InputMethodManager imm = (InputMethodManager)getSystemService(SearchApi.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
								imm.toggleSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_FORCED, 0);*/
					}
				});
			}
		});

		//Add text change listener
		edtSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				categoryLayout.setVisibility(View.VISIBLE);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.toString().length()==0)
				{
					/*relListResult.setVisibility(View.GONE);*/
					//Temporarily disabled
					if(!searchListFilter.isShown())
					{
						Animation animaSlidUp=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						searchListFilter.startAnimation(animaSlidUp);
						searchListFilter.setVisibility(View.VISIBLE);
						relListResult.setVisibility(View.GONE);

						//added 
						clearSearchArray();

					}
					Animation animaSlidOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);
					categoryLayout.startAnimation(animaSlidOut);
					categoryLayout.setVisibility(View.GONE);
				}
			}
		});
		//Searching In API.
		edtSearch.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if(isnetConnected.isNetworkAvailable())
					{
						if(edtSearch.getText().toString().length()!=0)
						{

							/*		animaSlidUp.setFillAfter(true);*/
							Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
							animaSlidDown.setFillAfter(true);

							//Temporarily disabled
							searchListFilter.startAnimation(animaSlidDown);

							categoryLayout.startAnimation(animaSlidDown);


							isSearchClicked=true;
							if(isSearchClicked==true)
							{
								clearSearchArray();
							}

							//							if(IS_SHOPLOCAL)
							//							{
							if(latitued.length()==0 && longitude.length()==0)
							{
								locationService();
							}
							else
							{
								if(isNewsCategory)
								{
									clearArray();
									loadNewsFeeds();
								}
								else
								{
									searchApi();
								}
							}
							//							}
							//							else
							//							{
							//								searchApi();
							//							}
							categoryLayout.setVisibility(View.GONE);
							if(chkBox.isChecked())
							{
								chkBox.setChecked(false);
								offer="";
								isCheckOffers=false;
							}


							/*InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);*/

							try
							{
								//Hiding keyboard
								InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
							}catch(NullPointerException npe)
							{
								npe.printStackTrace();
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}

							relListResult.setVisibility(View.VISIBLE);

							animaSlidDown.setAnimationListener(new AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationRepeat(Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub
									//Temporarily disabled
									searchListFilter.setVisibility(View.GONE);
									/*relListResult.startAnimation(animaSlidUp);*/
									relListResult.setVisibility(View.VISIBLE);
									//Hiding keyboard

								}
							});
						}
						else
						{

						}
					}
					else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

					}

					return true;
				}
				return false;
			}
		});

		imgSearchClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//Animating Edit text
				Animation animation = new ScaleAnimation(1, 0, 1,1, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 1);
				animation.setDuration(250);


				/*		animaSlidUp.setFillAfter(true);*/
				final Animation animaSlidDown=AnimationUtils.loadAnimation(context, R.anim.fade_out);
				animaSlidDown.setFillAfter(true);

				final Animation animFadeOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				final Animation animFadeIn=AnimationUtils.loadAnimation(context, R.anim.fade_in);

				edtSearch.startAnimation(animation);
				animation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
						//Temporarily disabled
						searchListFilter.startAnimation(animaSlidDown);
						/*if(relListResult.isShown())
						{
							relListResult.startAnimation(animFadeOut);
						}*/
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						imgSearchClose.setVisibility(View.GONE);
						edtSearch.setVisibility(View.GONE);


						searchListFilter.setVisibility(View.GONE);

						relListResult.setVisibility(View.GONE);

						imgSearchButton.setVisibility(View.VISIBLE);


						/*relDefault.startAnimation(animFadeIn);
						relDefault.setVisibility(View.VISIBLE);*/

						try
						{
							//Hiding keyboard
							InputMethodManager imm = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
						}catch(NullPointerException npe)
						{
							npe.printStackTrace();
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});
				animFadeOut.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub

					}
				});
				clearSearchArray();
				//	relListResult.setVisibility(View.GONE);
				

				
			}
		});
		

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		
		
		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		
		
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
		        pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
		
		
		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}
				
				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
				
			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
			
			
		}
		
		
		/** Anirudh- Custom share dialog */
		dialogShare = new Dialog(SearchActivity.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));



	}//onCreate Ends Here

	//	void createDrawer()
	//	{
	//		//Creating Drawer Menu
	//		//items.add(new DrawerSearch());
	//		try
	//		{
	//			items.clear();
	//			items.add(new SectionItem(getResources().getString(R.string.sectionShopLocal)));
	//			items.add(new EntryItem(getResources().getString(R.string.itemOffers),"",R.drawable.offers));
	//			items.add(new EntryItem(getResources().getString(R.string.itemOfferCategories),"",R.drawable.services));
	//			items.add(new EntryItem(getResources().getString(R.string.itemAllStores),"",R.drawable.stores));
	//			items.add(new EntryItem(getResources().getString(R.string.itemStoreCategories),"",R.drawable.services));
	//
	//
	//			items.add(new SectionItem(getResources().getString(R.string.sectionMyShoplocal)));
	//			if(session.isLoggedInCustomer())
	//			{
	//				items.add(new EntryItem(getResources().getString(R.string.itemMyProfile),"",R.drawable.user_profile));
	//			}
	//			else
	//			{
	//				items.add(new EntryItem(getResources().getString(R.string.itemLogin),"",R.drawable.login));
	//			}
	//
	//			items.add(new EntryItem(getResources().getString(R.string.itemMyShoplocalOffers),"",R.drawable.offers));
	//			items.add(new EntryItem(getResources().getString(R.string.itemFavouriteStores),"",R.drawable.fave));
	//
	//			items.add(new SectionItem(getResources().getString(R.string.sectionOtherStuff)));
	//			items.add(new EntryItem(getResources().getString(R.string.itemSettings),"",R.drawable.settings));
	//			items.add(new EntryItem(getResources().getString(R.string.itemNewsEvents),"",R.drawable.news));
	//			items.add(new EntryItem(getResources().getString(R.string.itemChangeLocation),"",R.drawable.pin));
	//			items.add(new EntryItem(getResources().getString(R.string.itemWelcomeScreen),"",R.drawable.user_profile));
	//			items.add(new EntryItem(getResources().getString(R.string.itemLogintoBusiness),"",R.drawable.login));
	//
	//
	//			items.add(new SectionItem(getResources().getString(R.string.sectionAboutShoplocal)));
	//			items.add(new EntryItem(getResources().getString(R.string.itemAboutShoplocal),"",R.drawable.about_us));
	//			items.add(new EntryItem(getResources().getString(R.string.itemContactUs),"",R.drawable.contact_us));
	//			items.add(new EntryItem(getResources().getString(R.string.itemFacebook),"",R.drawable.facebook));
	//			items.add(new EntryItem(getResources().getString(R.string.itemTwitter),"",R.drawable.twitter));	
	//
	//			drawerAdapter=null;
	//
	//			drawerAdapter=new DrawerAdapter(context, 0, items);
	//
	//			drawerList.setAdapter(drawerAdapter);
	//
	//			drawerList.setOnItemClickListener(new DrawerItemClickListener());
	//
	//		}catch(Exception ex)
	//		{
	//			ex.printStackTrace();
	//		}
	//
	//	}



	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/* convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_menus, parent, false);
			}
			((TextView) convertView.findViewById(R.id.listText))
			.setText(getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/*convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_dropdown_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(R.id.txtSpinner))
			.setText(getItem(position));
			return convertView;
		}


	}

	void clearSearchArray()
	{
		Search_ID.clear();
		Search_SOTRE_NAME.clear();
		Search_BUILDING.clear();
		Search_STREET.clear();
		Search_LANDMARK.clear();
		Search_AREA.clear();
		Search_CITY.clear();
		Search_MOBILE_NO.clear();
		Search_PHOTO.clear();
		Search_DISTANCE.clear();
		Search_DESCRIPTION.clear();
		Search_HAS_OFFER.clear();
		Search_OFFER.clear();
		search_page=0;
		search_post_count=20;

		//	btnSearchLoadMore.setVisibility(View.GONE);

	}



	//	@Override
	//	public void onBackPressed() {
	//		// TODO Auto-generated method stub
	//		/*if(slide_menu.isMenuShowing())
	//		{
	//			slide_menu.toggle(true);
	//		}
	//		else
	//		{
	//		this.finish();
	//		}*/
	//		Intent intent=new Intent(context,HomeGrid.class);
	//		startActivity(intent);
	//		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	//		this.finish();
	//
	//
	//	}

	//		@Override
	//		public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
	//			// TODO Auto-generated method stub
	//			/*MenuItem extra=menu.add("Extra Settings").setIcon(R.drawable.action_overflow);
	//			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);*/
	//			return true;
	//		}
	//	
	//		@Override
	//		public boolean onOptionsItemSelected(MenuItem item) {
	//			// TODO Auto-generated method stub
	//			/*slide_menu.toggle(true);*/
	//			Intent intent=new Intent(context,HomeGrid.class);
	//			startActivity(intent);
	//			this.finish();
	//			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	//			return true;
	//		}

	//call search api
	void searchApi()
	{

		/** anirudh search text */
		Log.i("STORE SEARCH", "OFFERS SEARCH  " +edtSearch.getText().toString());	
		searchMap.put("Category_Name","All");
		searchMap.put("Search_Text",edtSearch.getText().toString());
		EventTracker.logEvent("Search_StoreCategory", searchMap);
		
		

		Intent intent=new Intent(context, SearchService.class);
		intent.putExtra("searchapi",searchReceiver);
		/*intent.putExtra("URL", STORE_URL+GET_SEARCH_URL+search_post_count+"/page/"+search_page);*/
		intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("search_post_count",search_post_count);
		intent.putExtra("search_page",search_page+1);
		intent.putExtra("search", edtSearch.getText().toString());

		/*intent.putExtra("isCheckLocation", true);
			intent.putExtra("latitude", latitued);
			intent.putExtra("longitude", longitude);*/

		if(!isLocationFound)
		{
			latitued="0";
			longitude="0";
			intent.putExtra("isCheckLocation", false);
		}
		else
		{
			intent.putExtra("isCheckLocation", true);
		}
		intent.putExtra("latitude", latitued);
		intent.putExtra("longitude", longitude);

		intent.putExtra("locality", locality);
		intent.putExtra("offer", offer);
		intent.putExtra("isCheckOffers", isCheckOffers);
		intent.putExtra("key", key);
		intent.putExtra("value", value);
		intent.putExtra("isShopLocal",IS_SHOPLOCAL);

		context.startService(intent);

		searchListProg.setVisibility(View.VISIBLE);



		//		else
		//		{
		//			Intent intent=new Intent(context, SearchService.class);
		//			intent.putExtra("searchapi",searchReceiver);
		//			/*intent.putExtra("URL", STORE_URL+GET_SEARCH_URL+search_post_count+"/page/"+search_page);*/
		//			intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
		//			intent.putExtra("api_header",API_HEADER);
		//			intent.putExtra("api_header_value", API_VALUE);
		//			intent.putExtra("search_post_count",search_post_count);
		//			intent.putExtra("search_page",search_page);
		//			intent.putExtra("search", edtSearch.getText().toString());
		//			intent.putExtra("key", key);
		//			intent.putExtra("value", value);
		//			intent.putExtra("isShopLocal",IS_SHOPLOCAL);
		//			/*		intent.putExtra("latitude", latitued);
		//				intent.putExtra("longitude", longitude);
		//				intent.putExtra("distance", "25");*/
		//
		//			intent.putExtra("offer", offer);
		//			intent.putExtra("isCheckOffers", isCheckOffers);
		//			/*	intent.putExtra("isCheckLocation", isCheckLocation);
		//				intent.putExtra("isCheckOffers", isCheckOffers);*/
		//			context.startService(intent);
		//			/*isGeneralSearch=true;*/
		//			searchListProg.setVisibility(View.VISIBLE);
		//		}
	}

	void loadNewsFeeds()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			/** Anirudh search text */
			Log.i("OFFERS SEARCH", "OFFERS SEARCH  " +edtSearch.getText().toString());
			searchMap.put("Search_OffersCategory","All");
			searchMap.put("Search_Text",edtSearch.getText().toString());

			EventTracker.logEvent("Search_OffersCategory", searchMap);

			Intent intent=new Intent(context, NewsFeedService.class);
			intent.putExtra("newsFeed",newsFeedReceiver);
			intent.putExtra("URL", NEWS_FEED_SEARCH_URL+"/"+GET_SEARCH_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("count",search_post_count);
			intent.putExtra("page",search_page+1);
			intent.putExtra("area_id",value);
			intent.putExtra("offer", offer);
			intent.putExtra("isSearch", true);
			intent.putExtra("search", edtSearch.getText().toString());
			intent.putExtra("isCheckOffers", isCheckOffers);
			context.startService(intent);
			searchListProg.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void locationService()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//calling location service.
			Intent intent=new Intent(context, LocationIntentService.class);
			intent.putExtra("receiverTag", mReceiver);
			startService(intent);
			/*ShopLocalMain.shopProgress.setVisibility(View.VISIBLE);*/
			searchListProg.setVisibility(View.VISIBLE);
			//			showToast("Location");
		}else
		{
			MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);

		}
	}


	/*
	 * Load All Categories
	 */
	void loadAllCategories()
	{
		Intent intent=new Intent(context,GetPlaceCategoryService .class);
		intent.putExtra("category",mStoreCategoryReceiver);
		intent.putExtra("URL", STORE_URL+STORE_CATEGORY);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("IsAppend", false);
		intent.putExtra("value", "shoplocal");
		context.startService(intent);
		searchListProg.setVisibility(View.VISIBLE);
	}

	@Override
	public void onReciverSearchResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		//adding facebook analytics for search in Offer Stream

		AppEventsLogger logger = AppEventsLogger.newLogger(this);

		Bundle parameters = new Bundle();
		parameters.putString("Area", areaName);
		parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Store");
		parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, edtSearch.getText().toString());


		try
		{
			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");

			/*ArrayList<String> TempID=new ArrayList<String>();
		ArrayList<String> TempSOTRE_NAME=new ArrayList<String>();	
		ArrayList<String> TempBUILDING=new ArrayList<String>();
		ArrayList<String> TempSTREET=new ArrayList<String>();
		ArrayList<String> TempLANDMARK=new ArrayList<String>();
		ArrayList<String> TempAREA=new ArrayList<String>();
		ArrayList<String> TempCITY=new ArrayList<String>();
		ArrayList<String> TempSTATE=new ArrayList<String>();
		ArrayList<String> TempCOUNTRY=new ArrayList<String>();
		ArrayList<String> TempMOBILE_NO=new ArrayList<String>();
		ArrayList<String> TempPHOTO=new ArrayList<String>();
		ArrayList<String> TempDISTANCE=new ArrayList<String>();*/

			/*	TempID.clear();
		TempSOTRE_NAME.clear();
		TempBUILDING.clear();
		TempSTREET.clear();
		TempLANDMARK.clear();
		TempAREA.clear();
		TempMOBILE_NO.clear();
		TempPHOTO.clear();
		TempDISTANCE.clear();*/
			searchListProg.setVisibility(View.GONE);
			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{

				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "TRUE");

				final ArrayList<String> TempID=resultData.getStringArrayList("ID");
				final ArrayList<String> TempSOTRE_NAME=resultData.getStringArrayList("SOTRE_NAME");
				ArrayList<String> TempBUILDING=resultData.getStringArrayList("BUILDING");
				ArrayList<String> TempSTREET=resultData.getStringArrayList("STREET");
				ArrayList<String> TempLANDMARK=resultData.getStringArrayList("LANDMARK");
				final ArrayList<String> TempAREA=resultData.getStringArrayList("AREA");
				final ArrayList<String> TempCITY=resultData.getStringArrayList("CITY");
				final ArrayList<String> TempMOBILE_NO=resultData.getStringArrayList("MOBILE_NO");
				final ArrayList<String> TempPHOTO=resultData.getStringArrayList("PHOTO");
				ArrayList<String> TempDISTANCE=resultData.getStringArrayList("DISTANCE");
				ArrayList<String> TempDescription=resultData.getStringArrayList("DESCRIPTION");
				ArrayList<String> TempHasOffers=new ArrayList<String>();
				ArrayList<String> TempOffers=new ArrayList<String>();


				TempHasOffers.clear();
				TempOffers.clear();

				TempHasOffers=resultData.getStringArrayList("HAS_OFFERS");
				TempOffers=resultData.getStringArrayList("OFFERS");

				TOTAL_SEARCH_PAGES=resultData.getString("TOTAL_PAGES");
				TOTAL_SEARCH_RECORDS=resultData.getString("TOTAL_RECORDS");

				if(isRefreshing)
				{
					clearSearchArray();
					isRefreshing=false;
				}

				for(int index=0;index<TempID.size();index++)
				{
					Search_ID.add(TempID.get(index));
					Search_SOTRE_NAME.add(TempSOTRE_NAME.get(index));
					Search_BUILDING.add(TempBUILDING.get(index));
					Search_STREET.add(TempSTREET.get(index));
					Search_LANDMARK.add(TempLANDMARK.get(index));
					Search_AREA.add(TempAREA.get(index));
					Search_CITY.add(TempCITY.get(index));
					Search_MOBILE_NO.add(TempMOBILE_NO.get(index));
					Search_PHOTO.add(TempPHOTO.get(index));
					Search_DISTANCE.add(TempDISTANCE.get(index));
					/*TempDISTANCE.add("0");*/
					Search_DESCRIPTION.add(TempDescription.get(index));
					Search_HAS_OFFER.add(TempHasOffers.get(index));
					Search_OFFER.add(TempOffers.get(index));
				}

				search_page++;
				//Get the first visible position of listview
				final int old_pos = listSearchResult.getRefreshableView().getFirstVisiblePosition();

				//				SearchApiListAdapter adapter=new SearchApiListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, Search_SOTRE_NAME, Search_PHOTO,Search_HAS_OFFER,Search_OFFER,Search_DESCRIPTION,Search_DISTANCE);
				//				listSearchResult.setAdapter(adapter);

				sadapter.notifyDataSetChanged();

				txtSearchCounter.setText(Search_ID.size()+"/"+TOTAL_SEARCH_RECORDS);


				//Hide button if Total records are equal to Records fetched.
				if(Integer.parseInt(TOTAL_SEARCH_RECORDS)==Search_ID.size())
				{
					btnSearchLoadMore.setVisibility(View.GONE);
				}

				listSearchResult.onRefreshComplete();


				//Restore visible position
				//				listSearchResult.post(new Runnable() {
				//
				//					@Override
				//					public void run() {
				//						// TODO Auto-generated method stub
				//						listSearchResult.getRefreshableView().setSelection(old_pos);
				//					}
				//				});


				listSearchResult.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						if(!isRefreshing)
						{
							Intent intent=new Intent(context, StoreDetailsActivity.class);
							intent.putExtra("STORE_ID", Search_ID.get(position-1));
							intent.putExtra("STORE", Search_SOTRE_NAME.get(position-1));
							intent.putExtra("PHOTO", Search_PHOTO.get(position-1));
							intent.putExtra("AREA", Search_AREA.get(position-1));
							intent.putExtra("CITY", Search_CITY.get(position-1));
							intent.putExtra("MOBILE_NO", Search_MOBILE_NO.get(position-1));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
				});
			}

			if(!SEARCH_STATUS.equalsIgnoreCase("true"))
			{
				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "FALSE");

				String message="";
				message=resultData.getString("SEARCH_MESSAGE");
				showToast(message);
				search_page--;
				searchListProg.setVisibility(View.GONE);
				//				listSearchResult.onRefreshComplete();
				if(search_page<0 || search_page==0)
				{
					search_page=1;
					clearArray();
				}
				//				if(message.startsWith("No records found"))
				//				{
				//					Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
				//					txtSearchCounter.setText("");
				//					btnSearchLoadMore.setVisibility(View.GONE);
				//					listSearchResult.onRefreshComplete();
				//
				//				}


			}
			logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED,parameters);

		}catch(Exception ex)
		{
			ex.printStackTrace();
			listSearchResult.onRefreshComplete();
		}


	}

	//CategoryList Adapter

	static class CategoryListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> category_name;
		Activity context;
		LayoutInflater inflate;
		Typeface tf;
		Animation anim_fromRight,anim_fromLeft;
		String isFav="";

		public CategoryListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> category_name) {
			super(context, resource, textViewResourceId, category_name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.category_name=category_name;

			inflate=context.getLayoutInflater();

			//getting font from asset directory.
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/DinDisplayProLight.otf");
			/*	anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.activity_slide_from_right);
				anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.activity_slide_from_left);*/

			anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return category_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return category_name.get(position);
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			//inflate a view and build ui for list.
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.categorylistlayout,null);
				holder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);
				holder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				holder.categoryLayout=(RelativeLayout)convertView.findViewById(R.id.categoryLayout);
				convertView.setTag(holder);

			}
			final ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtCategory.setText(category_name.get(position));
			hold.chkBox.setVisibility(View.GONE);
			/*if(position==0)
			{
				hold.chkBox.setVisibility(View.VISIBLE);
				hold.chkBox.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(hold.chkBox.isChecked())
						{
							offer="1";
							isCheckOffers=true;
							//	Toast.makeText(context, "Offer "+offer, Toast.LENGTH_SHORT).show();
						}
						else
						{
							offer="";
							isCheckOffers=false;
						}
					}
				});

			}else
			{
				hold.chkBox.setVisibility(View.GONE);
			}*/

			if(position==0)
			{
				/*hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#154285"));*/



			}
			if(position==1)
			{
				/*hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#ffa83d"));*/
			}
			if(position==2)
			{
				/*hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#154285"));*/
			}
			if(position==3)
			{
				/*hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#ffa83d"));*/
			}
			if(position==4)
			{
				/*hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#154285"));*/
			}
			if(position==5)
			{
				/*hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#ffa83d"));*/
			}
			if(position==6)
			{
				/*	hold.txtCategory.setTextColor(Color.parseColor("#ffffff"));
					hold.categoryLayout.setBackgroundColor(Color.parseColor("#154285"));*/
			}

			return convertView;
		}

		static class ViewHolder
		{
			ImageView imgStoreLogo;
			TextView txtCategory;
			CheckBox chkBox;
			RelativeLayout categoryLayout;

		}

	}

	//Search Api Adapter

	static class SearchApiListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> store_name,distance,logo,has_offer,offers;
		ArrayList<String>Search_DESCRIPTION;
		Activity context;
		LayoutInflater inflate;
		String photo;
		Typeface tf;

		public SearchApiListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> store_name,ArrayList<String>logo,ArrayList<String>has_offers,ArrayList<String>offers,ArrayList<String>Search_DESCRIPTION,ArrayList<String>distance) {
			super(context, resource, textViewResourceId, store_name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.store_name=store_name;
			this.distance=distance;
			this.logo=logo;
			this.Search_DESCRIPTION=Search_DESCRIPTION;

			this.offers=offers;
			this.has_offer=has_offers;
			inflate=context.getLayoutInflater();
			//getting font from asset directory.
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return store_name.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_search_api,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.band=(TextView)convertView.findViewById(R.id.band);
				holder.searchIsOffer=(ImageView)convertView.findViewById(R.id.searchIsOffer);
				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();

			if(logo.get(position).toString().length()!=0)
			{
				//Log.i("LOGO", "LOGO "+logo.get(position));	
				photo=logo.get(position).replaceAll(" ", "%20");
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo, hold.imgStoreLogo);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}
			hold.txtStore.setText(store_name.get(position));
			hold.txtSearchStoreDesc.setText(Search_DESCRIPTION.get(position));
			hold.txtStore.setTypeface(tf, Typeface.BOLD);
			hold.txtSearchStoreDesc.setTypeface(tf);
			hold.txtDistance.setTypeface(tf);

			//			if(IS_SHOPLOCAL)
			//			{
			hold.txtDistance.setVisibility(View.VISIBLE);


			if(distance.get(position)!=null)
			{
				/*if(Double.parseDouble(distance.get(position))!=-1)
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position)));
						hold.txtDistance.setText(""+formatted+" km.");
					}*/
				if(Double.parseDouble(distance.get(position))!=-1)
				{
					//	Log.i("Distance : ","Distance : "+distance.get(position));

					if(Double.parseDouble(distance.get(position))<1 && Double.parseDouble(distance.get(position))>0)
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position))*100);
						hold.txtDistance.setText(""+formatted+" m.");
					}
					else
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position)));
						if((Double.parseDouble(distance.get(position))*100)>8000)
						{
							hold.txtDistance.setVisibility(View.GONE);
						}
						else
						{
							hold.txtDistance.setVisibility(View.VISIBLE);
							hold.txtDistance.setText(""+formatted+" km.");
						}
						/*hold.txtDistance.setText(""+formatted+" km.");*/
					}

				}
				//				}
			}

//			hold.band.setVisibility(View.VISIBLE);
			hold.searchIsOffer.setVisibility(View.VISIBLE);
			if(has_offer.get(position).toString().length()!=0 && has_offer.get(position).equalsIgnoreCase("1"))
			{
				/*hold.txtBackOffers.setText(offers.get(position));*/
//				hold.band.setText("Special Offer : "+offers.get(position));
				hold.searchIsOffer.setVisibility(View.VISIBLE);
				/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
				hold.txtStore.setTextColor(Color.WHITE);*/
			}
			else
			{
				/*				hold.txtBackOffers.setText("offers");*/
//				hold.band.setVisibility(View.GONE);
				hold.searchIsOffer.setVisibility(View.GONE);
				/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
				hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
			}

			return convertView;
		}

		static class ViewHolder
		{
			ImageView imgStoreLogo,searchIsOffer;;
			TextView txtStore;
			TextView txtSearchStoreDesc;
			TextView txtDistance;
			TextView band;
		}


	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		/*txtSlideMenu_Register.setOnClickListener(this);
		txtSlideMenu_News.setOnClickListener(this);
		txtSlideMenu_Favourite.setOnClickListener(this);
		txtSlideMenu_Refresh.setOnClickListener(this);
		txtSlideMenu_Dashboard.setOnClickListener(this);
		txtSlideMenu_Search.setOnClickListener(this);
		txtSlideMenu_Logout.setOnClickListener(this);*/
		/*if(v.getId()==txtSlideMenu_Register.getId())
		{
			slide_menu.setSelectedView(v);
			imageLoaderList.clearCache();
			Intent intent=new Intent(this, SplitLoginSignUp.class);
			startActivity(intent);
			this.finish();
		}
		if(v.getId()==txtSlideMenu_StoreList.getId())
		{
			slide_menu.setSelectedView(v);
			imageLoaderList.clearCache();
			Intent intent=new Intent(this, DefaultSearchList.class);
			startActivity(intent);
			this.finish();
		}
		if(v.getId()==txtSlideMenu_News.getId())
		{
			slide_menu.setSelectedView(v);
			Intent intent=new Intent(this, SplashScreen.class);
			startActivity(intent);
			this.finish();
		}
		if(v.getId()==txtSlideMenu_Favourite.getId())
		{
			slide_menu.setSelectedView(v);
			Intent intent=new Intent(this, MerchantFavouriteStores.class);
			intent.putExtra("isFromDefault", true);
			startActivity(intent);
			finish();
		}

		if(v.getId()==txtSlideMenu_Dashboard.getId())
		{
			slide_menu.setSelectedView(v);
			Intent intent=new Intent(this, DashBoard.class);
			startActivity(intent);
			this.finish();
		}
		if(v.getId()==txtSlideMenu_Setting.getId())
		{
			slide_menu.setSelectedView(v);
			Intent intent=new Intent(this, ManageActivity.class);
			startActivity(intent);
			this.finish();
		}
		if(v.getId()==txtSlideMenu_Search.getId())
		{
			slide_menu.setSelectedView(v);
			slide_menu.toggle(true);
						imageLoaderList.clearCache();
			Intent intent=new Intent(this, SearchActivity.class);
			startActivity(intent);
			this.finish();
		}
		if(v.getId()==txtSlideMenu_Logout.getId())
		{
			slide_menu.setSelectedView(v);
			imageLoaderList.clearCache();
			session.logoutUser();
			this.finish();
		}*/
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		/*if(!IS_SHOPLOCAL)
		{

			SharedPreferences pref=context.getSharedPreferences("placeid", 0);
			Editor editor=pref.edit();
			if(itemPosition==0)
			{
				business_id=getResources().getString(R.string.malad_id);
				editor.putString("place_id", business_id);
				editor.commit();
			}
			else if(itemPosition==1)
			{
				business_id=getResources().getString(R.string.vashi_id);
				editor.putString("place_id", business_id);
				editor.commit();

			}
			else if(itemPosition==2)
			{
				business_id=getResources().getString(R.string.cyberabad_id);
				editor.putString("place_id", business_id);
				editor.commit();

			}
			else if(itemPosition==3)
			{
				business_id=getResources().getString(R.string.whitefield_id);
				editor.putString("place_id", business_id);
				editor.commit();
				Log.i("business_id", "business_id commited "+business_id);
			}
		}*/

		SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
		Editor editor=pref.edit();
		editor.putString("selected_dropdown", getDropDownObject(itemPosition,JSON_STRING));
		editor.putInt("dropdown_position", itemPosition);
		editor.commit();



		/*Log.i("business_id", "business_id commited "+getDropDownObject(itemPosition,JSON_STRING));*/

		//parse key and value to search
		Log.i("GET DROP DOWN ", "GET DROP DOWN ");
		parseKeyValue(getDropDownObject(itemPosition,JSON_STRING));


		return false;
	}

	void parseKeyValue(String json)
	{
		try {
			Log.i("PARSE ", "PARSE KEY  "+json);
			JSONObject jsonobject=new JSONObject(json);
			key=jsonobject.getString("key");
			value=jsonobject.getString("value");
			locality=value;
			IS_SHOPLOCAL=Boolean.parseBoolean(jsonobject.getString("isShopLocal"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void parseJson(String json)
	{
		try {

			Log.i("PARSE ", "PARSE  "+json);

			JSONObject jsonobject=new JSONObject(json);
			JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");
			for(int i=0;i<dropDownArray.length();i++)
			{
				JSONObject jsonObject=dropDownArray.getJSONObject(i);

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					//Shoplocal
					if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}else
				{
					//Inorbit
					if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	String getDropDownObject(int position,String json)
	{
		String dropdown="";
		if(dropdownItems.size()!=0)
		{

			Log.i("PARSE ", "PARSE getDropDownObject  "+json);
			//Log.i("BOOLEAN ", "BOOLEAN "+getResources().getBoolean(R.bool.isAppShopLocal)+" POSITION "+position);
			try {
				JSONObject jsonobject=new JSONObject(json);
				JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");

				/*	JSONObject jsonObject=dropDownArray.getJSONObject(position);
				dropdown=jsonObject.toString();

				Log.i("JSON", "JSON "+dropdown);*/

				for(int i=0;i<dropDownArray.length();i++)
				{
					JSONObject jsonObject=dropDownArray.getJSONObject(i);

					Log.i("PARSE ", "BOOLEAN PARSE getDropDownObject  "+jsonObject.toString()); 


					if(getResources().getBoolean(R.bool.isAppShopLocal))
					{
						//Shoplocal
						if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}else
					{
						//Inorbit
						if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}
				}

				Log.i("GET DROP DOWN ", "GET DROP DOWN "+dropdown);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();

			}


		}
		return dropdown;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.GetPlaceCategoryReceiver.GetCategory#onReceiveStoreCategories(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveStoreCategories(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String category_Status=resultData.getString("category_status");
			ArrayList<String>CATEGORY_NAME=new ArrayList<String>();
			ArrayList<String>CATEGORY_ID=new ArrayList<String>();

			if(category_Status.equalsIgnoreCase("true"))
			{
				CATEGORY_NAME=resultData.getStringArrayList("CATEGORY_NAME");
				CATEGORY_ID=resultData.getStringArrayList("CATEGORY_ID");

				for(int i=0;i<CATEGORY_NAME.size();i++)
				{
					Log.i("CATEGORY_NAME", "CATEGORY_ID : "+CATEGORY_ID.get(i)+" CATEGORY_NAME "+CATEGORY_NAME.get(i));
				}

				category.clear();
				category_id.clear();

				category.add("Offers");
				category_id.add("-1");
				for(int i=0;i<CATEGORY_NAME.size();i++)
				{
					category.add(CATEGORY_NAME.get(i));
					category_id.add(CATEGORY_ID.get(i));
				}



				categAdapter=new CategoryListAdapter(context, R.drawable.ic_launcher,  R.drawable.ic_launcher,category);
				listCategory.setAdapter(categAdapter);
				categAdapter.notifyDataSetChanged();

				listCategory.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub



						if(!isNewsCategory)
						{
							/** Anirudh event tracker to be changed */
							eventMap.put("Category_Name", category.get(position));
							EventTracker.logEvent("Search_StoreCategory", eventMap);

							if(position==0)
							{
								Intent intent=new Intent(context,CategorySearch.class);
								intent.putExtra("category_id", "");
								intent.putExtra("offer", "1");
								intent.putExtra("isCategory", false);
								startActivity(intent);
							}
							else if(!category_id.get(position).toString().equalsIgnoreCase("-1"))
							{
								Intent intent=new Intent(context,CategorySearch.class);
								intent.putExtra("category_id", category_id.get(position));
								Log.i("Categ", "Categ : "+category_id.get(position));
								intent.putExtra("offer", "");
								intent.putExtra("isCategory", true);
								startActivity(intent);
							}
						}
						else
						{
							
//							eventMap.put("StreamCategory", category.get(position));
							eventMap.put("Category_Name", category.get(position));
//							EventTracker.logEvent("Search_StreamCategory", eventMap);
							EventTracker.logEvent("Search_OffersCategory", eventMap);

							if(position==0)
							{
								Intent intent=new Intent(context,NewsFeedCategory.class);
								intent.putExtra("category_id", "");
								intent.putExtra("offer", "1");
								intent.putExtra("isCategory", false);
								startActivity(intent);
							}
							else if(!category_id.get(position).toString().equalsIgnoreCase("-1"))
							{
								Intent intent=new Intent(context,NewsFeedCategory.class);
								intent.putExtra("category_id", category_id.get(position));
								Log.i("Categ", "Categ : "+category_id.get(position));
								intent.putExtra("offer", "");
								intent.putExtra("isCategory", true);
								startActivity(intent);
							}
						}
					}
				});

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(buttonView.isChecked())
		{
			offer="1";
			isCheckOffers=true;
		}
		else
		{
			offer="";
			isCheckOffers=false;
		}
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		searchListProg.setVisibility(View.GONE);
		String status=resultData.getString("status");
		if(status.equalsIgnoreCase("set"))
		{
			try
			{
				latitued=resultData.getString("Lat");
				longitude=resultData.getString("Longi");


				if(latitued!=null || latitued.length()>0 && longitude!=null || longitude.length()>0)
				{

					Log.i("LAT", "LATI "+latitued);
					Log.i("LONG", "LATI "+longitude);
					searchApi();

					/*onLocationChanged(location);*/
					//	showToast("Location");
				}
				Log.i("Location ", "LOCATION : "+latitued+" , "+longitude);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{

			showToast(getResources().getString(R.string.locationAlert));
		}
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	void getLocationFromPrefs()
	{
		SharedPreferences pref=context.getSharedPreferences("location",0);
		latitued=pref.getString("latitued", "not found");
		longitude=pref.getString("longitude", "not found");
		isLocationFound=pref.getBoolean("isLocationFound",false);
		Log.i("Location found","Location found "+isLocationFound+" "+latitued+","+longitude);
	}


	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			//			Toast.makeText(context, "You clicked " + item.getTitle() , Toast.LENGTH_SHORT).show();
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			//			navigate();
		}
	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try
		{
			MenuItem extra=menu.add("close").setIcon(R.drawable.abs__ic_clear_disabled);
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}

		if(item.getTitle().toString().equalsIgnoreCase("close"))
		{
			finish();
			overridePendingTransition(0,R.anim.shrink_fade_out_center);
		}


		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//		if(back_pressed+2000 >System.currentTimeMillis())
		//		{
		//			finish();
		//		}
		//		else
		//		{
		//			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
		//			back_pressed=System.currentTimeMillis();
		//		}

		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
	}

	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	//	void navigate()
	//	{
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemOffers)))
	//		{
	//			drawer_item="";
	//			Intent intent=new Intent(context,NewsFeedActivity.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemOfferCategories)))
	//		{
	//			drawer_item="";
	//			isNewsCategory=true;
	//			mDrawerLayout.closeDrawer(Gravity.LEFT);
	//
	//		}
	//
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
	//		{
	//
	//			drawer_item="";
	//			Intent intent=new Intent(context,DefaultSearchList.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//
	//		}
	//		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemStoreCategories)))
	//		{
	//			drawer_item="";
	//			isNewsCategory=false;
	//			mDrawerLayout.closeDrawer(Gravity.LEFT);
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
	//		{
	//			drawer_item="";
	//			Intent intent=new Intent(context,LoginSignUpCustomer.class);
	//			startActivityForResult(intent, 2);
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent intent=new Intent(context,CustomerProfile.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
	//		{
	//			drawer_item="";
	//			if(session.isLoggedInCustomer())
	//			{
	//				Intent intent=new Intent(context,MyShoplocal.class);
	//				startActivity(intent);
	//				finish();
	//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//			}
	//			else
	//			{
	//				login();
	//			}
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			if(session.isLoggedInCustomer())
	//			{
	//				Intent intent=new Intent(context,MerchantFavouriteStores.class);
	//				startActivity(intent);
	//				finish();
	//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//			}
	//			else
	//			{
	//				login();
	//			}
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent intent=new Intent(context,SettingsActivity.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemNewsEvents)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent intent=new Intent(context,NewsActivity.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent intent=new Intent(context,LocationActivity.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemWelcomeScreen)))
	//		{
	//			drawer_item="";
	//			Intent intent=new Intent(context,SplashScreen.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			if(session.isLoggedIn())
	//			{
	//				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
	//				Editor 	editor=prefs.edit();
	//				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
	//				editor.commit();
	//
	//				Intent intent=new Intent(context,MerchantTalkHome.class);
	//				startActivity(intent);
	//				finish();
	//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//
	//			}
	//			
	//			else
	//			{
	//				Intent intent=new Intent(context,SplitLoginSignUp.class);
	//				startActivity(intent);
	//				finish();
	//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//
	//			}
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
	//		{
	//			drawer_item="";
	//			Intent intent=new Intent(context,AboutUs.class);
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
	//					"mailto",getResources().getString(R.string.contact_email), null));
	//			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
	//			startActivity(Intent.createChooser(emailIntent, "Send email..."));
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent intent=new Intent(context,FbTwitter.class);
	//			intent.putExtra("isMerchant", false);
	//			intent.putExtra("isFb", true);
	//			intent.putExtra("fb", "facebook");
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
	//		{
	//			drawer_item="";
	//			//mDrawerLayout.closeDrawers();
	//			Intent intent=new Intent(context,FbTwitter.class);
	//			intent.putExtra("isMerchant", false);
	//			intent.putExtra("isFb", false);
	//			intent.putExtra("twitter", "twitter");
	//			startActivity(intent);
	//			finish();
	//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
	//		}
	//	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			//			createDrawer();
		}
		if(requestCode==6)
		{
			VIA="Android";
			callShareApi(TEMP_POST_ID);
		}

		try
		{
			uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}

				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					VIA="Android-Facebook";
					callShareApi(TEMP_POST_ID);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}



	@Override
	public void onNewsFeedResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		searchListProg.setVisibility(View.GONE);

		try
		{

			String search_status=resultData.getString("status");
			String message=resultData.getString("message");
			ArrayList<NewsFeedModel> newsFeed=resultData.getParcelableArrayList("posts");

			listNewsFeedSearch.setVisibility(View.VISIBLE);

			//adding facebook analytics for search in Offer Stream

			AppEventsLogger logger = AppEventsLogger.newLogger(this);

			Bundle parameters = new Bundle();
			parameters.putString("Area", areaName);
			parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "Offers");
			parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, edtSearch.getText().toString());




			if(search_status.equalsIgnoreCase("true"))
			{

				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "TRUE");

				TOTAL_SEARCH_PAGES=resultData.getString("total_page");
				TOTAL_SEARCH_RECORDS=resultData.getString("total_record");

				Log.i("News FEED SIZE ", "News FEED SIZE "+newsFeed.size());



				try
				{

					if(isRefreshing)
					{
						clearArray();
						isRefreshing=false;
					}

					//					for(int i=0;i<newsFeed.size();i++)
					//					{
					//						NEWS_FEED_ID.add(newsFeed.get(i).getId());
					//						NEWS_FEED_NAME.add(newsFeed.get(i).getName());
					//						NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());
					//
					//						NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
					//						NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
					//						NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());
					//
					//						NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
					//						NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
					//						NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());
					//
					//						NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
					//						NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
					//						NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());
					//
					//						NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
					//						NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
					//						NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());
					//
					//						NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());
					//
					//						NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
					//						NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
					//						NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());
					//
					//						NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
					//						NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
					//						NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());
					//
					//						NEWS_DATE.add(newsFeed.get(i).getDate());
					//						NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());
					//
					//
					//						NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
					//						NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
					//						NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());
					//					}

					ArrayList<String> TEMP_NEWS_FEED_ID=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_NAME=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_AREA_ID=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_MOBNO1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_MOBNO2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_MOBNO3=new ArrayList<String>();


					ArrayList<String> TEMP_NEWS_FEED_TELLNO1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_TELLNO2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_TELLNO3=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_LATITUDE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_LONGITUDE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_DISTANCE=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_POST_ID=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_POST_TYPE=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_POST_TITLE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_DESCRIPTION=new ArrayList<String>();


					ArrayList<String> TEMP_NEWS_FEED_image_url1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_image_url2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_image_url3=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_thumb_url1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_thumb_url2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_thumb_url3=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_DATE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_IS_OFFERED=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_OFFER_DATE_TIME=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_place_total_like=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_place_total_share=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_place_verified=new ArrayList<String>();


					TEMP_NEWS_FEED_ID.clear();
					TEMP_NEWS_FEED_NAME.clear();
					TEMP_NEWS_FEED_AREA_ID.clear();

					TEMP_NEWS_FEED_MOBNO1.clear();
					TEMP_NEWS_FEED_MOBNO2.clear();
					TEMP_NEWS_FEED_MOBNO3.clear();


					TEMP_NEWS_FEED_TELLNO1.clear();
					TEMP_NEWS_FEED_TELLNO2.clear();
					TEMP_NEWS_FEED_TELLNO3.clear();

					TEMP_NEWS_FEED_LATITUDE.clear();
					TEMP_NEWS_FEED_LONGITUDE.clear();
					TEMP_NEWS_FEED_DISTANCE.clear();

					TEMP_NEWS_FEED_POST_ID.clear();
					TEMP_NEWS_FEED_POST_TYPE.clear();

					TEMP_NEWS_FEED_POST_TITLE.clear();
					TEMP_NEWS_FEED_DESCRIPTION.clear();


					TEMP_NEWS_FEED_image_url1.clear();
					TEMP_NEWS_FEED_image_url2.clear();
					TEMP_NEWS_FEED_image_url3.clear();

					TEMP_NEWS_FEED_thumb_url1.clear();
					TEMP_NEWS_FEED_thumb_url2.clear();
					TEMP_NEWS_FEED_thumb_url3.clear();

					TEMP_NEWS_DATE.clear();
					TEMP_NEWS_IS_OFFERED.clear();
					TEMP_NEWS_OFFER_DATE_TIME.clear();

					TEMP_NEWS_FEED_place_total_like.clear();
					TEMP_NEWS_FEED_place_total_share.clear();

					TEMP_NEWS_FEED_place_verified.clear();


					for(int i=0;i<newsFeed.size();i++)
					{
						TEMP_NEWS_FEED_ID.add(newsFeed.get(i).getId());
						TEMP_NEWS_FEED_NAME.add(newsFeed.get(i).getName());
						TEMP_NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());

						TEMP_NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
						TEMP_NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
						TEMP_NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());

						TEMP_NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
						TEMP_NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
						TEMP_NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());

						TEMP_NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
						TEMP_NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
						TEMP_NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());

						TEMP_NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
						TEMP_NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
						TEMP_NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());

						TEMP_NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());

						TEMP_NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
						TEMP_NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
						TEMP_NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());

						TEMP_NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
						TEMP_NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
						TEMP_NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());

						TEMP_NEWS_DATE.add(newsFeed.get(i).getDate());
						TEMP_NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());


						TEMP_NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
						TEMP_NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
						TEMP_NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());

						TEMP_NEWS_FEED_place_verified.add(newsFeed.get(i).getVerified());
					}

					for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
					{
						NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
						NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
						NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );

						NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
						NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
						NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));

						NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
						NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
						NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));

						NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
						NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
						NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));

						NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
						NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
						NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));

						NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));

						NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
						NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
						NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));

						NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
						NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
						NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));

						NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
						NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));


						NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
						NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
						NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));
					}




					//Get the first visible position of listview
					//					final int old_pos = listNewsFeedSearch.getRefreshableView().getFirstVisiblePosition();

					//					adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
					//							NEWS_FEED_place_total_like, NEWS_FEED_place_total_share);
					//
					//					listNewsFeedSearch.setAdapter(adapter);

					adapter.notifyDataSetChanged();

					search_page++;

					//Restore visible position
					//					listNewsFeedSearch.post(new Runnable() {
					//
					//						@Override
					//						public void run() {
					//							// TODO Auto-generated method stub
					//							listNewsFeedSearch.getRefreshableView().setSelection(old_pos);
					//						}
					//					});
					Log.i("ADAPTER COUNT", "ADAPTER COUNT "+adapter.getCount());
					Log.i("TOTAL PAGES AND RECORDS  ","TOTAL CURRENT PAGE, PAGES AND RECORDS : PAGE "+search_page +" TOTAL PAGES "+TOTAL_SEARCH_PAGES+" TOTAL_RECORD "+TOTAL_SEARCH_RECORDS);

					txtSearchCounter.setText(NEWS_FEED_ID.size()+"/"+TOTAL_SEARCH_RECORDS);

					listNewsFeedSearch.onRefreshComplete();
					listNewsFeedSearch.setOnItemClickListener(new OnItemClickListener() {


						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							if(!isRefreshing)
							{
								Intent intent=new Intent(context, OffersBroadCastDetails.class);
								intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
								intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
								intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
								intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
								intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
								intent.putExtra("fromCustomer", true);
								intent.putExtra("isFromStoreDetails", false);
								intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
								intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
								intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
								intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
								intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
								intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
								intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
								intent.putExtra("USER_LIKE","-1");

								context.startActivityForResult(intent, 5);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							}
						}
					});

				}catch(Exception ex)
				{
					ex.printStackTrace();

				}


			}
			else
			{
				parameters.putString(AppEventsConstants.EVENT_PARAM_SUCCESS, "FALSE");
				showToast(message);
				listNewsFeedSearch.setVisibility(View.GONE);
				listNewsFeedSearch.onRefreshComplete();
				txtSearchCounter.setText("");
			}

			logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED,parameters);



		}catch(Exception ex)
		{
			ex.printStackTrace();
			listNewsFeedSearch.setVisibility(View.GONE);
			listNewsFeedSearch.onRefreshComplete();
			txtSearchCounter.setText("");
		}
	}

	// Newsfeed Adapter

	class NewsFeedAdapter extends ArrayAdapter<String>
	{

		Activity context;
		private int lastPosition = -1;


		ArrayList<String> post_id;
		ArrayList<String> news_title;
		ArrayList<String> offer_title;
		ArrayList<String> offer_desc;

		ArrayList<String> offer_date;
		ArrayList<String> is_offer;

		ArrayList<String> photo_url1;
		ArrayList<String> photo_url2;
		ArrayList<String> photo_url3;



		ArrayList<String>mob1;
		ArrayList<String>mob2;
		ArrayList<String>mob3;
		
		ArrayList<String>tel1;
		ArrayList<String>tel2;
		ArrayList<String>tel3;
		
		ArrayList<String>valid_till;



		ArrayList<String> offer_total_like;
		ArrayList<String> offer_total_total_share;

		boolean [] isAnimEnd;   

		LayoutInflater inflator;

		Typeface tf;


		public NewsFeedAdapter(Activity context, int resource,
				int textViewResourceId,  ArrayList<String>post_id,ArrayList<String>news_title,ArrayList<String> offer_title,
				ArrayList<String> offer_desc,ArrayList<String> offer_date,
				ArrayList<String> is_offer,ArrayList<String> photo_url1,
				ArrayList<String> offer_total_like,ArrayList<String> offer_total_total_share,ArrayList<String>mob1,ArrayList<String>mob2,
				ArrayList<String>mob3,ArrayList<String>tel1,ArrayList<String>tel2,ArrayList<String>tel3,ArrayList<String>valid_till) {
			super(context, resource, textViewResourceId, offer_title);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();

			this.post_id=post_id;
			this.news_title=news_title;
			this.offer_title=offer_title;
			this.offer_desc=offer_desc;

			this.offer_date=offer_date;
			this.is_offer=is_offer;

			this.photo_url1=photo_url1;
			this.offer_total_like=offer_total_like;

			this.offer_total_total_share=offer_total_total_share;
			
			this.mob1=mob1;
			this.mob2=mob2;
			this.mob3=mob3;
			
			this.tel1=tel1;
			this.tel2=tel2;
			this.tel3=tel3;
			
			this.valid_till=valid_till;


			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_title.size()+" "+offer_desc.size()+" "+offer_date.size()+" "+is_offer.size()+" "+photo_url1.size());
			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_total_like.size()+" "+offer_total_total_share.size());

			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");


		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return offer_title.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return offer_title.get(position);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_in);
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflator.inflate(R.layout.newsfeedrow,null);

				holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
				//				holder.newsFeedOffersMonth=(TextView)convertView.findViewById(R.id.newsFeedOffersMonth);
				//
				//				holder.newsFeedviewOverLay=(View)convertView.findViewById(R.id.newsFeedviewOverLay);

				holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
				holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);

				holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
				holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);

				holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);

				holder.newFeedband=(TextView)convertView.findViewById(R.id.newFeedband);

				holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);

				holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);

				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();



			try
			{
				hold.newsFeedStoreName.setText(news_title.get(position));
				hold.newsFeedOffersText.setText(offer_title.get(position));
				hold.newsFeedOffersDesc.setText(offer_desc.get(position));

				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

				Date date_con = (Date) dt.parse(offer_date.get(position).toString());

				Date date=dateFormat.parse(offer_date.get(position).toString());
				DateFormat date_format=new SimpleDateFormat("hh:mm a");

				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
				hold.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));

				//				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				//				DateFormat dt2=new SimpleDateFormat("MMM");
				//				Date date_con = (Date) dt.parse(offer_date.get(position).toString());
				//				Calendar cal = Calendar.getInstance();
				//				cal.setTime(date_con);
				//				int year = cal.get(Calendar.YEAR);
				//				int month = cal.get(Calendar.MONTH);
				//				int day = cal.get(Calendar.DAY_OF_MONTH);
				//
				//				//	Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
				//				hold.newsFeedOffersDate.setText(day+"");
				//				hold.newsFeedOffersMonth.setText(dt2.format(date_con)+"");


				// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				// }
				// else
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
				// }
				hold.newsFeedTotalLike.setText(offer_total_like.get(position));


				hold.newsFeedTotalShare.setText(offer_total_total_share.get(position));

				hold.newsFeedTotalLike.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						//							showToast("Clicked On "+news_title.get(position));
						likePost(post_id.get(position));
					}
				});
				hold.newsFeedTotalShare.setOnClickListener(new OnClickListener() {@Override
					public void onClick(View arg0) {
					// TODO Auto-generated method stub
					TEMP_POST_ID=post_id.get(position);


					/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder.setTitle("Share");
					alertDialogBuilder
					.setMessage("Share using")
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(true)
					.setPositiveButton("Other",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();

							List<Intent> targetedShareIntents = new ArrayList<Intent>();
							Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
							sharingIntent.setType("text/plain");
							String shareBody = news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position);

							PackageManager pm = context.getPackageManager();
							List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

							for(final ResolveInfo app : activityList) {

								String packageName = app.activityInfo.packageName;
								Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
								targetedShareIntent.setType("text/plain");

								if(TextUtils.equals(packageName, "com.facebook.katana")){

								} else {
									targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
									targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, news_title.get(position) + " - " + "Offers");
									targetedShareIntent.setPackage(packageName);
									targetedShareIntents.add(targetedShareIntent);
								}

							}

							Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
							chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
							startActivityForResult(chooserIntent,6);

						}
					})
					.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.dismiss();

							if(photo_url1.get(position).toString().length()!=0)
							{
								String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");

								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(position) + " - " + "Offers")
								.setPicture(PHOTO_PARENT_URL+photo_source)
								.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
							}
							else
							{
								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(position) + " - " + "Offers")
								.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
							}
						}
					})
					;

					AlertDialog alertDialog = alertDialogBuilder.create();

					alertDialog.show();*/
					
					showDialog(position);


				}
				});


				hold.newFeedband.setVisibility(View.GONE);
				hold.newsIsOffer.setVisibility(View.VISIBLE);

				hold.newsFeedStoreName.startAnimation(animation);
				hold.newsFeedOffersText.startAnimation(animation);
				hold.newsFeedOffersDesc.startAnimation(animation);
				hold.newsFeedOffersDate.startAnimation(animation);
				hold.newsFeedTotalLike.startAnimation(animation);
				hold.newsFeedTotalShare.startAnimation(animation);


				if(is_offer.get(position).toString().length()!=0 && is_offer.get(position).equalsIgnoreCase("1"))
				{
					hold.newsIsOffer.setVisibility(View.VISIBLE);

					//hold.newFeedband.setText("Specail Offer : "+is_offer.get(position));
					/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
					hold.txtStore.setTextColor(Color.WHITE);*/

				}
				else
				{
					//hold.txtBackOffers.setText("offers");
					hold.newFeedband.setVisibility(View.GONE);
					hold.newsIsOffer.setVisibility(View.INVISIBLE);
					/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
					hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				hold.imgNewsFeedLogo.setVisibility(View.VISIBLE);
				if(photo_url1.get(position).toString().length()==0)
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));
					hold.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_CENTER);
					//					imageLoaderList.DisplayImage("",hold.imgNewsFeedLogo);
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(255, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(200, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.VISIBLE);
					/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgNewsFeedLogo);*/
					try{
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(hold.imgNewsFeedLogo);
					}
					catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){ 
						e.printStackTrace();
					}
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_XY);

					hold.imgNewsFeedLogo.startAnimation(animation);

				}

				//				Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
				//				convertView.startAnimation(animation);
				//				lastPosition = position;


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



			return convertView;
		}
		
		void showDialog(final int tempPos){
			try
			{
				dialogShare.show();

				listViewShare.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {

						String app = appName.get(position);
						
						
						
						String shareBody= "";
						
						String validTill="";
						
						String contact="";
						
						String contact_lable="\nContact:";
						
						Log.i("Is Offer ","Is Offer valid_till "+valid_till.get(tempPos));
						
						if(is_offer.get(tempPos).toString().length()!=0 && is_offer.get(tempPos).equalsIgnoreCase("1"))
						{
							try
							{
								DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
								DateFormat dt2=new SimpleDateFormat("MMM");
								DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

								Date date_con = (Date) dt.parse(valid_till.get(tempPos).toString());

								Date date=dateFormat.parse(valid_till.get(tempPos).toString());
								DateFormat date_format=new SimpleDateFormat("hh:mm a");

								Calendar cal = Calendar.getInstance();
								cal.setTime(date_con);
								int year = cal.get(Calendar.YEAR);
								int month = cal.get(Calendar.MONTH);
								int day = cal.get(Calendar.DAY_OF_MONTH);
								
								validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
								validTill="";
							}
							/*validTill="\nValid Till:"+valid_till.get(tempPos);*/
						}
						else
						{
							validTill="";
						}
						
						if(mob1.get(tempPos).toString().length()>0)
						{
							contact="+"+mob1.get(tempPos).toString();
						} 
						if(mob2.get(tempPos).toString().length()>0)
						{
							contact+=" +"+mob2.get(tempPos).toString();
						}
						if(mob3.get(tempPos).toString().length()>0)
						{
							contact+=" +"+mob3.get(tempPos).toString();
						}
						if(tel1.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						if(tel2.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						if(tel3.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						
						if(contact.length()==0)
						{
							contact_lable="";
						}
						
						
						shareBody=news_title.get(tempPos) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(tempPos) + "\n" + offer_desc.get(tempPos)+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
						
						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

							if(photo_url1.get(tempPos).toString().length()!=0)
							{
								String photo_source=photo_url1.get(tempPos).toString().replaceAll(" ", "%20");

								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPos) + " - " + "Offers")
								.setPicture(PHOTO_PARENT_URL+photo_source)
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
							else
							{
								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPos) + " - " + "Offers")
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
						}
						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
							Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
						}

						else if(!app.equalsIgnoreCase("facebook")){
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setPackage(packageNames.get(position));
							i.setType("text/plain");
							i.putExtra(Intent.EXTRA_SUBJECT, news_title.get(tempPos));
							i.putExtra(Intent.EXTRA_TEXT, shareBody);
							startActivityForResult(i,6);
						}

						dismissDialog();

					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		void dismissDialog(){
			dialogShare.dismiss();
		}




	}

	static class ViewHolder
	{
		TextView newsFeedOffersDate;
		TextView newsFeedOffersMonth;

		TextView newsFeedOffersText;
		TextView newsFeedOffersDesc;

		TextView newsFeedTotalLike;
		TextView newsFeedTotalShare;

		ImageView imgNewsFeedLogo;

		View newsFeedviewOverLay;

		TextView newFeedband;

		ImageView newsIsOffer;

		TextView newsFeedStoreName;

	}

	void likePost(String POST_ID)
	{
		if(isnetConnected.isNetworkAvailable())
		{
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context, FavouritePostService.class);
				intent.putExtra("favouritePost",mFav);
				intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("user_id", USER_ID);
				intent.putExtra("auth_id", AUTH_ID);
				intent.putExtra("post_id", POST_ID);
				context.startService(intent);
				searchListProg.setVisibility(View.VISIBLE);
			}
			else
			{
				loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
			}
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}
	private void callShareApi(String POST_ID) {
		// TODO Auto-generated method stub

		Intent intent = new Intent(context, ShareService.class);
		intent.putExtra("shareService", shareServiceResult);
		intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		intent.putExtra("post_id", POST_ID);
		intent.putExtra("via", VIA);
		context.startService(intent);


	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Discover");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==POST_LIKE)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	void clearArray()
	{
		NEWS_FEED_ID.clear();
		NEWS_FEED_NAME.clear();
		NEWS_FEED_AREA_ID.clear();

		NEWS_FEED_MOBNO1.clear();
		NEWS_FEED_MOBNO2.clear();
		NEWS_FEED_MOBNO3.clear();


		NEWS_FEED_TELLNO1.clear();
		NEWS_FEED_TELLNO2.clear();
		NEWS_FEED_TELLNO3.clear();

		NEWS_FEED_LATITUDE.clear();
		NEWS_FEED_LONGITUDE.clear();
		NEWS_FEED_DISTANCE.clear();

		NEWS_FEED_POST_ID.clear();
		NEWS_FEED_POST_TYPE.clear();

		NEWS_FEED_POST_TITLE.clear();
		NEWS_FEED_DESCRIPTION.clear();


		NEWS_FEED_image_url1.clear();
		NEWS_FEED_image_url2.clear();
		NEWS_FEED_image_url3.clear();

		NEWS_FEED_thumb_url1.clear();
		NEWS_FEED_thumb_url2.clear();
		NEWS_FEED_thumb_url3.clear();

		NEWS_DATE.clear();
		NEWS_IS_OFFERED.clear();
		NEWS_OFFER_DATE_TIME.clear();

		NEWS_FEED_place_total_like.clear();
		NEWS_FEED_place_total_share.clear();

		search_page=0;
		search_post_count=20;


	}



	@Override
	public void postLikeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			searchListProg.setVisibility(View.GONE);
			String postLikestatus=resultData.getString("postLikestatus");
			String postLikemsg=resultData.getString("postLikemsg");
			if(postLikestatus.equalsIgnoreCase("true"))
			{
				showToast(postLikemsg);
			}
			else if(postLikestatus.equalsIgnoreCase("false"))
			{
				showToast(postLikemsg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}

			}
			else if(postLikestatus.equalsIgnoreCase("error"))
			{
				showToast(postLikemsg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	@Override
	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String status = resultData.getString("share_status");

		String msg = resultData.getString("share_msg");
		try {

			if(status.equalsIgnoreCase("true")){

				showToast(msg);
			}
			else{

				showToast(msg);
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


}
