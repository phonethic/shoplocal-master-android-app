package com.phonethics.shoplocal;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CountryAdapter extends ArrayAdapter<String>{

	
	ArrayList<CountryModel> 		countryCodeArr;
	Activity						context;
	private static LayoutInflater 	inflator = null;
	ArrayList<String>				countryArr;
	ArrayList<String>				codeArr;
	
	
	
	public CountryAdapter(Activity context,ArrayList<String> countryArr,ArrayList<String> codeArr) {
		super(context, R.layout.list_row,countryArr);
		// TODO Auto-generated constructor stub
		// 
		// this is country adapter on temp branch now working here
	
		this.context		= context;
		this.countryArr 	= countryArr;
		this.codeArr 		= codeArr;
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return countryArr.size();
	}
	
	
	
	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return countryArr.get(position);
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null){
			ViewHolder holder 		= new ViewHolder();
			inflator				= context.getLayoutInflater();
			convertView 			= inflator.inflate(R.layout.list_row, null);
			
			holder.text_country			= (TextView) convertView.findViewById(R.id.text_country_list);
			holder.text_dial_code		= (TextView) convertView.findViewById(R.id.text_country_code_list);
			
			convertView.setTag(holder);
		}
		
		ViewHolder hold = (ViewHolder) convertView.getTag();
		
		try{
			
	
			hold.text_country.setText(getItem(position));
			hold.text_dial_code.setText(codeArr.get(position));
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return convertView;
	}
	
	static class ViewHolder{
		public TextView 	text_country,text_dial_code;
	}
	

}
