package com.phonethics.shoplocal;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.google.android.gms.maps.model.LatLng;
import com.phonethics.camera.CameraImageSave;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.ZoomCroppingActivity;
import com.phonethics.model.Model;
import com.phonethics.networkcall.BusinessOperationReceiver;
import com.phonethics.networkcall.BusinessOperationReceiver.OperationReceiver;
import com.phonethics.networkcall.BusinessOperationService;
import com.phonethics.networkcall.DeleteGalleryImagesResultReceiver;
import com.phonethics.networkcall.DeleteGalleryImagesResultReceiver.DeleteImageReceiver;
import com.phonethics.networkcall.DeleteGalleryImagesService;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.phonethics.networkcall.GetAreaResultReceiver;
import com.phonethics.networkcall.GetAreaResultReceiver.GetArea;
import com.phonethics.networkcall.GetAreaService;
import com.phonethics.networkcall.GetPerticularStoreDetailService;
import com.phonethics.networkcall.GetPerticularStoreOperationReceiver;
import com.phonethics.networkcall.GetPerticularStoreOperationReceiver.StoreOperation;
import com.phonethics.networkcall.GetPerticularStoreOperationService;
import com.phonethics.networkcall.GetPlaceCategoryReceiver;
import com.phonethics.networkcall.GetPlaceCategoryReceiver.GetCategory;
import com.phonethics.networkcall.GetPlaceCategoryService;
import com.phonethics.networkcall.GetShopLocalGallery;
import com.phonethics.networkcall.GetShopLocalGalleryReceiver;
import com.phonethics.networkcall.GetShopLocalGalleryReceiver.ShopLocalGallery;
import com.phonethics.networkcall.LoadGalleryReceiver;
import com.phonethics.networkcall.LoadGalleryReceiver.LoadGallery;
import com.phonethics.networkcall.LoadStoreGallery;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;
import com.phonethics.networkcall.PostPhotoReceiver;
import com.phonethics.networkcall.PostPhotoReceiver.photoreceiver;
import com.phonethics.networkcall.PostStorePhoto;
import com.phonethics.networkcall.StoreDropDownReceiver;
import com.phonethics.networkcall.StoreDropDownReceiver.StoreDropDown;
import com.phonethics.networkcall.StoreOwnerDropDownService;
import com.phonethics.networkcall.UpdateStoreDetailsReceiver;
import com.phonethics.networkcall.UpdateStoreDetailsReceiver.UpdateStoreReceiver;
import com.phonethics.networkcall.UpdateStoreDetailsService;
import com.squareup.picasso.Picasso;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;


@SuppressLint("ValidFragment")
public class EditStore extends SherlockFragmentActivity implements LoadGallery,photoreceiver,StoreDropDown,GetCategory
,GetAllStore,StoreOperation,ShopLocalGallery, DeleteImageReceiver,OperationReceiver,UpdateStoreReceiver, GetArea {
	ActionBar actionBar;
	static Activity context;

	/*static ImageLoader imageLoader;*/
	static NetworkCheck isnetConnected;

	//Session Manger
	SessionManager session;

	static String USER_ID="";
	static String AUTH_ID="";

	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	String business_id="";
	boolean isSplitLogin=false;

	static String STORE_URL;
	static String SOTRES_PATH;

	static String STORE;
	static String STORE_OPERATION;
	static String STORE_GALLERY;
	static String SHOPLOCAL_GALLERY;
	static String STORE_CATEGORY;
	static String STORE_OWNER_LIST;
	static String ADD_BUSINESS_STORE_PATH;
	static String ADD_BUSINESS_STORE_CONTACT_PATH;
	static String BUSINESS_STORE_OPERATION_PATH;

	static String API_HEADER;
	static String API_VALUE;

	static String STORE_ID="";
	static String STORE_NAME;
	static String STORE_PHOTO;
	static String BUSINESS_ID;

	static LoadGalleryReceiver mReceiver;
	PostPhotoReceiver mPostPhoto;

	GridView imageGrid;

	static ProgressBar progUpload;

	private static final int ACTION_TAKE_PHOTO_B = 1;
	private static final int SELECT_PHOTO = 2;
	private static final int THUMBNAIL_SIZE =480;


	String FILE_PATH="";
	String FILE_NAME="";
	String FILE_TYPE="";

	static String FILE_PAGER_PATH="";
	static String FILE_PAGER_NAME="";
	static String FILE_PAGER_TYPE="";

	String encodedpath="";
	static ArrayList<String>PHOTO_SOURCE=new ArrayList<String>();

	static ViewPager mPager;

	ArrayList<String>pages=new ArrayList<String>();

	static boolean imageget=false;
	static Intent imagegetdata = null;


	//Register Store reciver

	GetAllStores_Of_Perticular_User_Receiver mGetStoreReceiver;

	GetPerticularStoreOperationReceiver mGetStoreOperationReceiver;

	//Shoplocal Gallery
	GetShopLocalGalleryReceiver mShopLocal;

	StoreDropDownReceiver mStoreDropDownReceiver;

	GetPlaceCategoryReceiver mStoreCategoryReceiver;


	//location
	static ArrayList<String> STORE_CATEGORY_ID=new ArrayList<String>();
	String STORE_PARENT_ID;


	//Store Pager
	static String store_name;
	static String store_parent;
	static String store_category;
	static String store_street;
	static String store_landmark;
	static String store_area;
	static String store_city;
	static String store_pincode;
	static String store_desc;

	static String store_landline,store_mobileno,store_faxno,store_tollfree,store_time_from,store_time_to;
	static String store_emailId,store_web;

	static String store_landline_no2;
	static String store_landline_no3;

	static String store_mobile_no2;
	static String store_mobile_no3;

	static String store_facebook_url;
	static String store_twitter_url;

	static String place_status="1";

	static String PHOTO_URL="";


	static Calendar time = Calendar.getInstance(); 
	static int hour;
	static int min;

	/**************************FILE CROP***********************/
	public static final int REQUEST_CODE_GALLERY      = 6;
	public static final int REQUEST_CODE_TAKE_PICTURE = 7;
	public static final int REQUEST_CODE_CROP_IMAGE   = 8;

	public static final int REQUEST_CODE_PAGER_GALLERY      = 9;
	public static final int REQUEST_CODE_TAKE_PAGER_PICTURE = 10;
	public static final int REQUEST_CODE_CROP_PAGER_IMAGE   = 11;

	/** CUSTOM CAMERA */
	final static int REQ_CODE_GETCLICKEDIMAGE = 12;
	final static int REQ_CODE_GETCLICKEDIMAGEGALLERY = 13;
	final static int REQ_CODE_GETCLICKEDIMAGE_GALLERY = 14;
	final static int REQ_CODE_GETSELECTEDIMAGEGALLERY_GALLERY = 15;


	/** SELECT GALLERY */
	final static int REQ_CODE_LOGO_SELECTGALLERY = 16; //Receive image from gallery - logo
	final static int REQ_CODE_LOGO_SELECTGALLERYCROP = 17; //Recieve image from gallery after crop - logo
	final static int REQ_CODE_GALLERY_SELECTGALLERY = 18; //Receive image from gallery - gallery
	final static int REQ_CODE_GAlLERY_SELECTGALLERYCROP = 19; //Recieve image from gallery after crop - gallery

	private static File      mFileTemp;

	static Uri mImageCaptureUri;
	static Uri mImagePagerCaptureUri;


	Dialog categoryDialog;
	Dialog parentStoreDialog;
	Dialog gallery_image_title;

	String imageTitle="";

	static ArrayList<String> tempCatId=new ArrayList<String>();
	ArrayList<String> tempCatName=new ArrayList<String>();

	ArrayList<String> tempCatAdapterId=new ArrayList<String>();
	ArrayList<String> tempCatAdapterName=new ArrayList<String>();
	/*
	String [] tempCatId;
	String [] tempCatName;*/

	//STORE ID OF PARENT STORE;
	static String OWNER_ID="0";
	String categ_list="";

	static ProgressBar progStoreUpdate;

	static boolean isNew=false; //to check whether to create new store or update Existing;

	//TABS
	Tab tab1;
	Tab tab2;


	int manageGallery=0;

	int cnt=0;

	//malad 
	static String latitued="";
	static String longitude="";


	int mappos=-1;
	private PagerTitleStrip pagerTabs;

	static Dialog shopLocalGallery;
	static Dialog chooseLogoDialog;


	GridView shoplocalGrid;

	String shopLocalIconUrl;
	static boolean isShopLocalIcon=false;

	static TextView chooseExisting;
	static TextView takePic;
	static TextView chooseShoplocal;
	static TextView removeLogo;

	Button shopIconDone;
	Button shopIconCancel;

	static ArrayList<String>SHOPLOCAL_SOURCE=new ArrayList<String>();
	static String SHOPLOCAL_ICON_URL;

	RelativeLayout galleryImageChooser;

	ImageView addGalleryImage;
	Button btnChooseGalleryImage;

	static String mCountryCode;

	DeleteGalleryImagesResultReceiver deleteImages;
	String DELETE_IMG_URL;
	String GalleryImageId;

	Button done;
	Button edit;
	Button cancelEditing;

	LinearLayout editLay;
	LinearLayout editDoneLay;

	ArrayList<String>STORE_ID_=new ArrayList<String>();
	ArrayList<String>SOURCE=new ArrayList<String>();
	ArrayList<String>TITLE=new ArrayList<String>();
	ArrayList<String>IMAGE_DATE=new ArrayList<String>();

	static ArrayList<String>ID = new ArrayList<String>();
	static ArrayList<String> IDSTODELETE = new ArrayList<String>();

	static boolean showEditMode = false;

	GridAdapter adapterImg;

	int count = 0;


	static boolean sun=true;
	static boolean mon=true;
	static boolean tue=true;
	static boolean wed=true;
	static boolean thu=true;
	static boolean fri=true;
	static boolean sat=true;


	//Add Business Store-operation Recevier
	static BusinessOperationReceiver mBusinessStoreOperationReceiver;

	//Update Business
	static UpdateStoreDetailsReceiver mUpdate;

	Context acontext;

	//Area Dialog
	static Dialog locationDialog;
	static ListView locationList;

	static DBUtil dbUtil;

	static ArrayList<String> local_city=new ArrayList<String>();
	static ArrayList<String> local_pin=new ArrayList<String>();
	static ArrayList<String> local_area=new ArrayList<String>();
	static ArrayList<String> local_areaID=new ArrayList<String>();

	static int removeStoreLogo=0;

	static String area_id;

	static String PHOTO="";
	String Place_Id;

	String URLTOAPPEND = "";

	String Separator = "&gallery_id=";

	String url_ids = "";

	AreaBroadCast areaBroadCast;

	GetAreaResultReceiver getAreaRec; 

	static String AREA_URL;
	static String ISO_URL;

	String countryCode;

	//String Gallery_Id = "gallery_id[]=";

	static Map<String, String> eventMap;

	static Dialog cropDialog;
	Button btnDoneCropping;

	/** Decalre ImageView objects */
	//ImageView getCroppedImage;
	ImageView rotateLeft;
	ImageView rotateRight;


	static byte [] pager_byte=null;
	static byte[] gallery_byte = null ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);

		place_status="1";

		setContentView(R.layout.activity_edit_store);
		// Show the Up button in the action bar.
		context=this;
		acontext=this;

		EventTracker.startLocalyticsSession(context);

		dbUtil=new DBUtil(context);
		actionBar=getSupportActionBar();
		actionBar.setTitle("Store Settings");
		actionBar.setDisplayHomeAsUpEnabled(true);

		editDoneLay = (LinearLayout) findViewById(R.id.editDoneLay);
		editLay = (LinearLayout) findViewById(R.id.editLay);
		done = (Button) findViewById(R.id.done);
		edit = (Button) findViewById(R.id.Edit);
		cancelEditing = (Button) findViewById(R.id.Cancel);
		//Network check
		isnetConnected=new NetworkCheck(context);

		getAreaRec=new GetAreaResultReceiver(new Handler());
		getAreaRec.setReciver(this);

		AREA_URL = getResources().getString(R.string.areas);
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		countryCode = tm.getSimCountryIso();

		//		ISO_URL = "?iso-code=" + countryCode;
		ISO_URL = "?iso-code=" + "in";
		Log.d("DeviceId","DeviceId " + countryCode);

		eventMap=new HashMap<String, String>();
		try
		{
			getCountryCode();




		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
		mImageCaptureUri = Uri.fromFile(mFileTemp);


		mImagePagerCaptureUri= Uri.fromFile(mFileTemp);

		FILE_PATH="/sdcard/temp_photo.jpg";

		PHOTO_URL=getResources().getString(R.string.photo_url);





		//Image Loader
		/*imageLoader=new ImageLoader(context);*/

		//Session Manager
		session=new SessionManager(getApplicationContext());

		//Shoplocal Gallery Dialog
		shopLocalGallery=new Dialog(context);
		shopLocalGallery.setContentView(R.layout.shoplocal_gallery_dialog);
		shopLocalGallery.setTitle("Choose Logo");
		shopLocalGallery.setCancelable(true);
		shopLocalGallery.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
		shoplocalGrid=(GridView)shopLocalGallery.findViewById(R.id.shoplocalGrid);


		/** Set components for crop view */

		//Location Gallery Dialog
		locationDialog=new Dialog(context);
		locationDialog.setContentView(R.layout.layout_location_chooser_dialog);
		locationDialog.setCancelable(true);
		locationDialog.setTitle("Choose Location");
		locationDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		locationList=(ListView)locationDialog.findViewById(R.id.locationList);


		shopIconDone=(Button)shopLocalGallery.findViewById(R.id.shopIconDone);
		shopIconCancel=(Button)shopLocalGallery.findViewById(R.id.shopIconCancel);

		chooseLogoDialog=new Dialog(context);
		chooseLogoDialog.setTitle("Select an Option");
		chooseLogoDialog.setContentView(R.layout.logopickerdialog);
		chooseLogoDialog.setCancelable(true);
		chooseLogoDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;


		galleryImageChooser=(RelativeLayout)findViewById(R.id.galleryImageChooser);
		addGalleryImage=(ImageView)findViewById(R.id.addGalleryImage);
		btnChooseGalleryImage=(Button)findViewById(R.id.btnChooseGalleryImage);


		//Store API
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		SOTRES_PATH=getResources().getString(R.string.allStores);

		//Store Gallery
		STORE_GALLERY=getResources().getString(R.string.store_gallery);

		//Shoplocal Gallery
		SHOPLOCAL_GALLERY=getResources().getString(R.string .shoplocal_gellery);

		//Store Category
		STORE_CATEGORY=getResources().getString(R.string.place_category);

		//Owner Dropdown
		STORE_OWNER_LIST=getResources().getString(R.string.listparentsplace);

		//Store operation
		STORE_OPERATION=getResources().getString(R.string.get_stores_operation);

		//Store
		STORE=getResources().getString(R.string.allStores);

		//DeleteGalleryImage
		DELETE_IMG_URL = getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi)+getResources().getString(R.string.store_gallery_delete);
		//GalleryImageId = "?" + getResources().getString(R.string.gallery_img_id);
		Place_Id = "?" + getResources().getString(R.string.place_id);

		ADD_BUSINESS_STORE_PATH=getResources().getString(R.string.add_store);
		ADD_BUSINESS_STORE_CONTACT_PATH=getResources().getString(R.string.add_store_contact);
		BUSINESS_STORE_OPERATION_PATH=getResources().getString(R.string.store_operation);


		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//Getting data from intent
		Bundle b=getIntent().getExtras();

		//Store opration Receiver
		mBusinessStoreOperationReceiver=new BusinessOperationReceiver(new Handler());
		mBusinessStoreOperationReceiver.setReceiver(this);

		//Store Update
		mUpdate=new UpdateStoreDetailsReceiver(new Handler());
		mUpdate.setReceiver(this);

		//Registering Receiver
		mReceiver=new LoadGalleryReceiver(new Handler());
		mReceiver.setReceiver(this);

		//Register post photo reciver
		mPostPhoto=new PostPhotoReceiver(new Handler());
		mPostPhoto.setReceiver(this);

		//Category Receiver
		mStoreCategoryReceiver=new GetPlaceCategoryReceiver(new Handler());
		mStoreCategoryReceiver.setReceiver(this);

		//Store Owner Drop down Receiver
		mStoreDropDownReceiver=new StoreDropDownReceiver(new Handler());
		mStoreDropDownReceiver.setStoreDropDownReceiver(this);

		//Register store details receiver
		mGetStoreReceiver=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
		mGetStoreReceiver.setReceiver(this);

		//Register store operation receiver
		mGetStoreOperationReceiver=new GetPerticularStoreOperationReceiver(new Handler());
		mGetStoreOperationReceiver.setReceiver(this);

		//ShopLocal Service 
		mShopLocal=new GetShopLocalGalleryReceiver(new Handler());
		mShopLocal.setReceiver(this);

		//DeleteImagesFromGallery Service

		deleteImages = new DeleteGalleryImagesResultReceiver(new Handler());
		deleteImages.setReceiver(this);

		//Gridview
		imageGrid=(GridView)findViewById(R.id.imageGrid);

		//Dilogs
		categoryDialog=new Dialog(context);
		gallery_image_title=new Dialog(context);
		parentStoreDialog=new Dialog(context);

		categoryDialog.setContentView(R.layout.categorylistdialog);
		gallery_image_title.setContentView(R.layout.gallery_image_title);
		parentStoreDialog.setContentView(R.layout.parentlistdialog);

		//Porgressbar
		progUpload=(ProgressBar)findViewById(R.id.progUpload);
		progStoreUpdate=(ProgressBar)findViewById(R.id.progStoreUpdate);

		pagerTabs = (PagerTitleStrip) findViewById(R.id.pagerTabs);
		//ViewPager
		mPager=(ViewPager)findViewById(R.id.viewPagerManageStore);
		pages.add("Location");
		pages.add("Details");
		pages.add("Contact");
		pages.add("Description");
		pages.add("Operations");
		pages.add("Gallery");

	/*	pagerTabs.setTextColor(Color.WHITE);
		pagerTabs.setIndicatorColor(Color.WHITE);
		pagerTabs.setIndicatorHeight(5);*/


		//loadShopLocalGallery();

		try
		{
			if(dbUtil.getAreaRowCount()==0)
			{
				if(isnetConnected.isNetworkAvailable())
				{
					getAreas();
				}
			}
			else
			{
				local_city=dbUtil.getAllAreaCity();
				local_areaID=dbUtil.getAllAreaIds();
				local_pin=dbUtil.getAllAreaPin();
				local_area=dbUtil.getAllAreas();
				locationList.setAdapter(new LocationListAdapter(context, 0, 0, local_area));
			}


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, pages);
		mPager.setAdapter(adapter);
		mPager.setOffscreenPageLimit(pages.size());
		//		pagerTabs.setTextColor(Color.BLACK);

		if(b!=null)
		{
			HashMap<String,String>user_details=session.getUserDetails();
			USER_ID=user_details.get(KEY_USER_ID).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID).toString();
			isSplitLogin=session.isLoggedIn();
			isNew=b.getBoolean("isNew");
			STORE_ID=b.getString("STORE_ID");
			STORE_NAME=b.getString("STORE_NAME");
			STORE_PHOTO=b.getString("STORE_LOGO");
			manageGallery=b.getInt("manageGallery");

			Log.i("STORE_ID","R STORE_ID "+STORE_ID);



			/*BUSINESS_ID=b.getString("business_id");*/

			/*if((!STORE_ID.equals("") || STORE_ID.length()!=0)&&(!BUSINESS_ID.equals("")||BUSINESS_ID.length()!=0))*/
			if(!isNew)
			{
				if((!STORE_ID.equals("") || STORE_ID.length()!=0) && isNew==false)
				{
					if(manageGallery!=1)
					{
						getPerticularStoreDetails();
						actionBar.setTitle(getResources().getString(R.string.editStoreTitle));

					}
					/*getPerticularStoreOperationDetails();*/

				}
			}
			else
			{
				actionBar.setTitle(getResources().getString(R.string.addStoreTitle));
				loadAllCategories();
				//loadOwnerList();

			}
		}

		btnChooseGalleryImage.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
			
				if(!isNew)
				{
					if(!progUpload.isShown())
					{
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));
						alertDialogBuilder.setMessage("Photo");
						//null should be your on click listener
						alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
							
								//takePictureForGrid();
								//takePictureForGrid();
							}
						});
						alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								openGalleryForGrid();
							}
						});
						alertDialogBuilder.show();
					}
				}else
				{
					//showToast(getResources().getString(R.string.storeCreateAlert));
					mPager.setCurrentItem(0);
				}
			}
		});
		/*loadGallery();*/

		/*	//Action bar with Tab navigation.
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		//Add Tabs

		tab1=actionBar.newTab().setText("Manage Store");
		tab2=actionBar.newTab().setText("Store Gallery");

		 */


		/*tab1.setTabListener(new TabListener() {

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			

			}

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
			
				mPager.setVisibility(View.VISIBLE);
				imageGrid.setVisibility(View.GONE);
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
			

			}
		});

		tab2.setTabListener(new TabListener() {

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			

			}

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
			
				mPager.setVisibility(View.GONE);
				imageGrid.setVisibility(View.VISIBLE);
				if(!isNew)
				{
					loadGallery();
				}
				else
				{
					if(STORE_ID==null)
					{
						Toast.makeText(context, "Create Store First then try to upload images", Toast.LENGTH_LONG).show();
						tab1.select();
						actionBar.setSelectedNavigationItem(tab1.getPosition());

					}
					else
					{
						loadGallery();
					}
				}
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
			

			}
		});*/
		if(manageGallery!=1)
		{
			edit.setVisibility(View.GONE);
			mPager.setVisibility(View.VISIBLE);
			imageGrid.setVisibility(View.GONE);
			galleryImageChooser.setVisibility(View.GONE);
			/*actionBar.addTab(tab1);*/
		}
		/*actionBar.addTab(tab2);*/

		if(manageGallery==1)
		{
			actionBar.setTitle(getResources().getString(R.string.manageGalleryTitle));
			edit.setVisibility(View.VISIBLE);
			imageGrid.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.GONE);
			pagerTabs.setVisibility(View.GONE);
			//galleryImageChooser.setVisibility(View.VISIBLE);
			if(!isNew)
			{
				loadGallery();
			}
			/*tab2.select();
			actionBar.setSelectedNavigationItem(tab2.getPosition());*/
		}


		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		imageGrid.setLayoutAnimation(controller);


		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
			
				/*mappos=position;*/
				if(position==4)
				{
					try
					{
						EditText edtManageStoreName=(EditText)mPager.findViewById(R.id.edtManageStoreName);

						EditText edtManageDesc2=(EditText)mPager.findViewById(R.id.edtManageDesc2);

						TextView txtOperationStoreName=(TextView)mPager.findViewById(R.id.txtOperationStoreName);;
						TextView txtOperationDesc=(TextView)mPager.findViewById(R.id.txtOperationDesc);

						txtOperationStoreName.setText(edtManageStoreName.getText().toString());
						txtOperationDesc.setText(edtManageDesc2.getText().toString());
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			

			}

			@Override
			public void onPageScrollStateChanged(int state) {
			
				/*if(state!=ViewPager.SCROLL_STATE_IDLE)
				{
					RelativeLayout mapLayout=(RelativeLayout)mPager.findViewById(R.id.mapLayout);
					mapLayout.setVisibility(ViewGroup.INVISIBLE);
				}
				else
				{
					RelativeLayout mapLayout=(RelativeLayout)mPager.findViewById(R.id.mapLayout);
					mapLayout.setVisibility(ViewGroup.VISIBLE);
				}*/
			}
		});

		edit.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
			

				//editDoneLay.setVisibility(View.GONE);

				edit.setVisibility(View.GONE);
				editLay.setVisibility(View.VISIBLE);
				showEditMode = true;

				//loadGallery();
				//				adapterImg.notifyDataSetChanged();
				//				imageGrid.invalidateViews();

				adapterImg=new GridAdapter(context, SOURCE,ID,TITLE);
				imageGrid.setAdapter(adapterImg);
			}
		});

		done.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
			

				//showEditMode = false;

				Log.d("IDSTODELETESIZE", "IDSTODELETESIZE " + IDSTODELETE.size());

				if(IDSTODELETE.size()>0){


					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle("Shoplocal");
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.deleteGalleryImage))
					.setCancelable(false)
					.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							for(int i=0;i<IDSTODELETE.size();i++){

								Log.d("IDSTODELETE", "IDSTODELETE " + IDSTODELETE.get(i));

								URLTOAPPEND = URLTOAPPEND + IDSTODELETE.get(i);  

								if(i!=IDSTODELETE.size()-1){

									URLTOAPPEND = URLTOAPPEND + ",";
								}
								//callDeleteImage();


							}

							Log.d("URLTOSEND","URLTOSEND "  +Separator + URLTOAPPEND);
							url_ids = Separator + URLTOAPPEND;
							callDeleteImage(url_ids);
						}


					})
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();



				}
				else{

					//showToast(getResources().getString(R.string.deleteImageValidation));
				}

			}
		});

		cancelEditing.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
			

				//				editDoneLay.setVisibility(View.GONE);
				IDSTODELETE.clear();
				URLTOAPPEND = "";
				//url_ids = "";

				editLay.setVisibility(View.VISIBLE);

				//editDoneLay.setVisibility(View.VISIBLE);

				edit.setVisibility(View.VISIBLE);
				editLay.setVisibility(View.GONE);
				showEditMode = false;

				//loadGallery();
				//				adapterImg.notifyDataSetChanged();
				//				imageGrid.invalidateViews();

				adapterImg=new GridAdapter(context, SOURCE,ID,TITLE);
				imageGrid.setAdapter(adapterImg);

			}
		});
	} //onCreate Ends Here

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}



	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		try
		{
			if(areaBroadCast!=null)
			{
				context.unregisterReceiver(areaBroadCast);
				areaBroadCast=null;
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	class LocationListAdapter extends ArrayAdapter<String>
	{

		ArrayList<String> locations;
		Activity context;
		LayoutInflater inflator;

		public LocationListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> locations) {
			super(context, resource, textViewResourceId, locations);

			this.locations=locations;
			this.context=context;
			inflator=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
		
			return locations.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		
			if(convertView==null)
			{
				LocationViewHolder holder=new LocationViewHolder();
				convertView=inflator.inflate(R.layout.layout_location_row, null);
				holder.txtLocationRow=(TextView)convertView.findViewById(R.id.txtLocationRow);
				convertView.setTag(holder);
			}
			LocationViewHolder hold=(LocationViewHolder)convertView.getTag();
			hold.txtLocationRow.setText(locations.get(position));
			return convertView;
		}






	}

	static class LocationViewHolder
	{
		TextView txtLocationRow;
	}



	@Override
	public void invalidateOptionsMenu() {
	
		super.invalidateOptionsMenu();

	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	

		if(!isNew && manageGallery!=1)
		{
			//showToast("Invalidate called " +place_status);
			SubMenu subMenu1 = menu.addSubMenu("Extra Settings");

			if(place_status.equalsIgnoreCase("0"))
			{
				subMenu1.add("Show store");
			}
			if (place_status.equalsIgnoreCase("1"))
			{
				subMenu1.add("Hide store");
			}
			subMenu1.add("Delete store");


			MenuItem subMenu1Item = subMenu1.getItem();
			subMenu1Item.setIcon(R.drawable.ic_delete_store);
			subMenu1Item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		return true;

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	
		if(item.getTitle().toString().equalsIgnoreCase("Extra Settings"))
		{

		}
		else if(item.getTitle().toString().equalsIgnoreCase("Delete store"))
		{
			try
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.deleteStoreMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Delete",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						DeleteStoreAsync delete=new DeleteStoreAsync();
						delete.execute("");

					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}
		else if(item.getTitle().toString().equalsIgnoreCase("Show store"))
		{
			try
			{


				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.showStore))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Show",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						place_status="1";

						PlaceStatusAsync place=new PlaceStatusAsync();
						place.execute("");
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();


			}catch(Exception exception)
			{
				exception.printStackTrace();
			}


		}
		else if(item.getTitle().toString().equalsIgnoreCase("Hide store"))
		{
			try
			{



				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.hideStoreMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Hide",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						place_status="0";

						PlaceStatusAsync place=new PlaceStatusAsync();
						place.execute("");
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();

				//				fetchLocationData();
				//				fetchContactData();
				//				fetchStoreTimingData();
				//				if(isValidLocationData() && isValidContactData())
				//				{
				//					updateStoreData();
				//					updateStore_timeData();
				//				}


				//			SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
				//			Editor editor=prefs.edit();
				//			editor.putBoolean("isPlaceRefreshRequired", true);
				//			editor.commit();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			finishTo();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;

	}

	private class PlaceStatusAsync extends AsyncTask<String, Void, String>
	{



		@Override
		protected void onCancelled() {
		
			super.onCancelled();
			progUpload.setVisibility(View.GONE);
		}


		@Override
		protected void onPreExecute() {
		
			super.onPreExecute();
			progUpload.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {
		
			String status = "";
			try {

				BufferedReader bufferedReader = null;


				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
				HttpPost httpPost=new HttpPost(STORE_URL+ADD_BUSINESS_STORE_PATH);

				Log.i("", "Service Response URL "+STORE_URL+ADD_BUSINESS_STORE_PATH);

				//Adding header.
				/*httpPost.addHeader("X-API-KEY", "Fool");*/
				httpPost.addHeader("X-HTTP-Method-Override", "PUT");
				httpPost.addHeader(API_HEADER, API_VALUE);

				JSONObject json = new JSONObject();

				json.put("user_id", USER_ID);
				json.put("auth_id", AUTH_ID);
				json.put("place_id", STORE_ID);
				json.put("place_status", place_status);

				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);


				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
				status=stringBuffer.toString();
			} catch (JSONException e) {

				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}catch(ConnectTimeoutException c)
			{
				c.printStackTrace();
			}
			catch(SocketTimeoutException st)
			{
				st.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}


			return status;
		}

		@Override
		protected void onPostExecute(String result) {
		
			super.onPostExecute(result);
			progUpload.setVisibility(View.GONE);

			try
			{

				String store_status;
				String store_msg = "";
				String store_data="";
				store_status=getStatus(result);
				if(store_status.equalsIgnoreCase("true"))
				{
					store_msg=getMessage(result);
					store_data=getData(result);
					//	showToast("Place Status Orignal : "+place_status);
					//				if(place_status.equalsIgnoreCase("0"))
					//				{
					//					place_status="1";
					//				}
					//				else if(place_status.equalsIgnoreCase("1"))
					//				{
					//					place_status="0";
					//				}

					//		showToast("Place Status Again : "+place_status);
					invalidateActionBar();
					SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();

				}
				else
				{
					store_msg=getMessage(result);
					showToast(store_msg);
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}

		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {

				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		String getMessage(String status)
		{
			String message="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				message=jsonobject.getString("message");

			} catch (JSONException e) {

				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return message;
		}

		String getData(String status)
		{
			String data="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject getDataObject=jsonobject.getJSONObject("data");
				data=getDataObject.getString("place_id");

			} catch (JSONException e) {

				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return data;
		}


	}

	private class DeleteStoreAsync extends AsyncTask<String, Void, String>
	{

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		String URL="";



		@Override
		protected void onCancelled() {
		
			super.onCancelled();
			progUpload.setVisibility(View.GONE);
		}



		@Override
		protected void onPreExecute() {
		
			super.onPreExecute();
			progUpload.setVisibility(View.VISIBLE);
		}



		@Override
		protected String doInBackground(String... params) {
		
			String status = "";

			try
			{
				BufferedReader bufferedReader = null;


				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/

				//Adding Place ID to Delete
				nameValuePairs.add(new BasicNameValuePair("place_id",STORE_ID));


				URL=STORE_URL+ADD_BUSINESS_STORE_PATH;

				//Creating URL
				String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
				URL+="?"+paramString;


				HttpDelete httpDelete=new HttpDelete(URL);



				//Adding header.
				/*httpPost.addHeader("X-API-KEY", "Fool");*/
				//				httpDelete.addHeader("X-HTTP-Method-Override", "PUT");
				httpDelete.addHeader(API_HEADER, API_VALUE);
				httpDelete.addHeader("user_id", USER_ID);
				httpDelete.addHeader("auth_id", AUTH_ID);

				Log.i("", "Service Response URL Delete "+URL);
				Log.i("", "Service Response URL Delete "+USER_ID);
				Log.i("", "Service Response URL AUTH_ID "+AUTH_ID);

				HttpResponse response=httpClient.execute(httpDelete);

				bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer=new StringBuffer("");
				String line="";
				String LineSeparator=System.getProperty("line.separator");

				while((line=bufferedReader.readLine())!=null)
				{
					stringBuffer.append(line+LineSeparator);
				}
				bufferedReader.close();
				Log.i("Response : ", "Service Response Store Details Delete: "+stringBuffer.toString());
				status=stringBuffer.toString();




			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return  status;
		}



		@Override
		protected void onPostExecute(String result) {
		
			super.onPostExecute(result);
			try
			{
				progUpload.setVisibility(View.GONE);

				String store_status;
				String store_msg = "";
				String store_data="";
				store_status=getStatus(result);
				if(store_status.equalsIgnoreCase("true"))
				{
					store_msg=getMessage(result);
					store_data=getData(result);
					showToast(store_msg);
					finishOnDelete();

					SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();

				}
				else
				{
					store_msg=getMessage(result);
					String error_code=getErrorCode(result);
					showToast(store_msg);
					if(error_code.equalsIgnoreCase("-116"))
					{
						session.logoutUser();
					}
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}



		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {

				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		String getErrorCode(String status)
		{
			String error_code="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				error_code=jsonobject.getString("code");

			} catch (JSONException e) {

				e.printStackTrace();
				error_code="-1";
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				error_code="-1";
			}
			return error_code;
		}

		String getMessage(String status)
		{
			String message="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				message=jsonobject.getString("message");

			} catch (JSONException e) {

				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return message;
		}

		String getData(String status)
		{
			String data="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject getDataObject=jsonobject.getJSONObject("data");
				data=getDataObject.getString("place_id");

			} catch (JSONException e) {

				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return data;
		}


	}


	void invalidateActionBar()
	{
		this.supportInvalidateOptionsMenu();
	}



	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter
	{
		Activity context;
		ArrayList<String> pages;
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages) {
			super(fm);

			this.context=context;
			this.pages=pages;
		}

		@Override
		public CharSequence getPageTitle(int position) {
		
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
		
			return new PageFragment(pages.get(position), context);
		}

		@Override
		public int getCount() {
		
			return pages.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
		

			/*FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);*/
		}
	}

	@SuppressLint("ValidFragment")
	public static class PageFragment extends Fragment implements   Receiver, GetAllStore,LocationListener
	{
		String title="";

		Activity context;
		View view;

		ProgressBar progManageLocation;
		Animation anim;
		Button btnMangeStore_ImageChooser;

		//Drop Pin
		TextView shopDropPin;
		LocationManager 	locationManager;


		double saved_lati, saved_longi;

		LatLng 				latLng;
		Dialog 				loc_dialog;
		TextView textAcc ;
		boolean isGPSEnabled;
		boolean isNetworkEnabled;
		//		GoogleMap googleMap;
		//		MarkerOptions markerOptions;
		//		LatLng latLng;
		Location location;
		String provider, current_address;
		double current_latitude, current_longitude;



		//		LatLng current_latlng;
		RelativeLayout mapLayout;
		TextView txtDropMessage;
		TextView shopDropMessage2;
		ImageView mapImage;
		Button btnManageLocationtSave;

		//Location Page
		Button btnMangeStoreSave;
		TextView txtManageParent;
		TextView txtManageCategory;
		EditText edtManageStoreName;
		EditText edtManageStreet;
		EditText edtManageLandMark;

		TextView edtManageArea;
		TextView edtManageCity;
		TextView edtManagePinCode;
		EditText edtManageDesc;


		//Description
		EditText edtManageDesc2;
		Button btnMangeStoreDescSave;
		Button btnMangeStoreDescSkip;



		//Contact Page 
		TextView txtMangeContactStoreName;
		EditText edtManageLandline;
		EditText edtManageMobile;
		EditText edtManageFax;
		EditText edtManageTollFree;
		EditText edtManageEmailId;
		EditText edtManageWebSite;
		Button btnManageContactSave;
		EditText edtManageFb;
		EditText edtManageTwitter;

		TableRow landlinerow1;
		TableRow landlinerow2;
		TableRow landlinerow3;

		TableRow mobileRow1;
		TableRow mobileRow2;
		TableRow mobileRow3;

		Button add1;
		Button add2;
		Button add3;

		Button add4;
		Button add5;
		Button add6;

		View landLineRowSeprator2;
		View landLineRowSeprator3;

		View mobileRowSeprator2;
		View mobileRowSeprator3;

		EditText edtManageLandline2;
		EditText edtManageLandline3;


		EditText edtManageMobile2;
		EditText edtManageMobile3;





		//Store Operation Page
		ImageView imgManageStore_ThumbPreview;
		TextView txtMangeStoreTimeFrom;
		TextView txtMangeStoreTimeTo;
		ToggleButton bManagetoggleSun;
		ToggleButton bManagetoggleMon;
		ToggleButton bManagetoggleTue;
		ToggleButton bManagetoggleWed;
		ToggleButton bManagetoggleThu;
		ToggleButton bManagetoggleFri;
		ToggleButton bManagetoggleSat;
		TextView txtOperationStoreName;
		TextView txtOperationDesc;



		Button btnManageTime;

		//Gallery Page
		GridView imageGridMerchant;






		//Location Service
		public LocationResultReceiver mLocation;


		Typeface tf;



		public PageFragment()
		{

		}
		public PageFragment(String title,Activity context)
		{
			this.title=title;
			this.context=context;
			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1200);

		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
		
			super.onCreate(savedInstanceState);
			setRetainInstance(true);

			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");





			//Location Service
			mLocation=new LocationResultReceiver(new Handler());
			mLocation.setReceiver(this);


			// Getting LocationManager object from System Service LOCATION_SERVICE
			locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

			loc_dialog = new Dialog(context);

			loc_dialog.setContentView(R.layout.accuracy_dialog);

			loc_dialog.setTitle("Fetching your position..");
			loc_dialog.setCancelable(false);





		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
		


			/*			txtPlaceText.startAnimation(anim);*/
			if(title.equalsIgnoreCase("Location"))
			{
				view=inflater.inflate(R.layout.merchant1, null);
				shopDropPin=(TextView)view.findViewById(R.id.shopDropPin);
				txtDropMessage=(TextView)view.findViewById(R.id.txtDropMessage);
				shopDropMessage2=(TextView)view.findViewById(R.id.shopDropMessage2);
				mapImage=(ImageView)view.findViewById(R.id.mapImage);
				btnManageLocationtSave=(Button)view.findViewById(R.id.btnManageLocationtSave);
				Button skipButton=(Button)view.findViewById(R.id.skipButton);


				textAcc = (TextView) loc_dialog.findViewById(R.id.text_accuracy);

				try
				{
					if(isnetConnected.isNetworkAvailable())
					{
						//						Criteria criteria1 = new Criteria();
						//						provider = locationManager.getBestProvider(criteria1, false);
						//						showToast(provider);
						//						location = locationManager.getLastKnownLocation(provider);
						//
						//						isGPSEnabled=locationManager
						//								.isProviderEnabled(LocationManager.GPS_PROVIDER);
						//						isNetworkEnabled = locationManager
						//								.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
						//
						//						showToast("GPS : "+isGPSEnabled);
						//						showToast("Network : "+isNetworkEnabled);

						//						if(isGPSEnabled)
						//						{
						//							location=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						//							onLocationChanged(location);
						//						}
						//						else if(isNetworkEnabled)
						//						{
						//							location=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						//							onLocationChanged(location);
						//						}

						//				onLocationChanged(location);
						//						if(!isGpsEnable())
						//						{
						//							showGpsAlert();
						//						}
						//						else
						//						{
						//
						//							showAccuracyDialog();
						//						}
					}
					else
					{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				ImageView info_dropPin=(ImageView)view.findViewById(R.id.info_dropPin);

				shopDropPin.setTypeface(tf);
				txtDropMessage.setTypeface(tf);
				skipButton.setTypeface(tf);
				btnManageLocationtSave.setTypeface(tf);
				shopDropMessage2.setTypeface(tf);

				info_dropPin.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View view) {
					
						AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
						alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
						alertDialogBuilder3
						.setMessage(getResources().getString(R.string.dropPinMessage))
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(false)
						.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.dismiss();
							}
						})
						;

						AlertDialog alertDialog3 = alertDialogBuilder3.create();

						alertDialog3.show();
					}
				} );

				if(!isNew)
				{
					btnManageLocationtSave.setVisibility(View.VISIBLE);
					btnManageLocationtSave.setOnClickListener(new android.view.View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
						
							fetchLocationData();
							if(isValidLocationData())
							{
								updateStoreData();
							}
						}
					});
				}
				else
				{
					btnManageLocationtSave.setVisibility(View.GONE);
				}

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					skipButton.setOnClickListener(new android.view.View.OnClickListener() {

						@Override
						public void onClick(View v) {
						
							mPager.setCurrentItem(mPager.getCurrentItem()+1);
						}
					});
				}
				/*	mapLayout=(RelativeLayout)view.findViewById(R.id.mapLayout);*/
				//Map
				/*ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo netInfo = cm.getActiveNetworkInfo();*/

				/*LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

				 */
				/*SupportMapFragment supportMapFragment = (SupportMapFragment) 
						getSupportFragmentManager().findFragmentById(R.id.map);*/

				// Getting a reference to the map
				/*		googleMap = supportMapFragment.getMap();
				googleMap.setMyLocationEnabled(true);*/
				/*googleMap.setMyLocationEnabled(true);*/

				/*googleMap.animateCamera(CameraUpdateFactory.zoomTo(18));*/

				/*googleMap.setOnMarkerDragListener(this);*/

				/*if (netInfo != null && netInfo.isConnected()) {

					Criteria criteria = new Criteria();
					provider = locationManager.getBestProvider(criteria, false);
					location = locationManager.getLastKnownLocation(provider);

					if(location!=null){
						onLocationChanged(location);
					}
					else {

						AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
						alertDialogBuilder3.setTitle("Location Check");
						alertDialogBuilder3
						.setMessage("No location found. Please check GPS settings")
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(false)
						.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.dismiss();
							}
						})
						;

						AlertDialog alertDialog3 = alertDialogBuilder3.create();

						alertDialog3.show();
					}
				}else{



					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder.setTitle("Check Connection");
					alertDialogBuilder
					.setMessage("No internet connection.")
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
						}
					})
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
						}
					});

					AlertDialog alertDialog = alertDialogBuilder.create();

					alertDialog.show();
				}*/

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{

					//Drop pin
					mapImage.setOnClickListener(new android.view.View.OnClickListener() {

						@Override
						public void onClick(View v) {
						

							if(!isNew)
							{
								AlertDialog.Builder alert=new AlertDialog.Builder(context);
								alert.setTitle(getResources().getString(R.string.changeLocationAlert));


								alert.setPositiveButton("Yes", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
									

										locationService();
										//		
										dialog.dismiss();


									}
								});
								alert.setNegativeButton("Not now", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
									
										dialog.dismiss();

									}
								});
								alert.show();
							}
							else
							{
								AlertDialog.Builder alert=new AlertDialog.Builder(context);
								alert.setTitle("Are you at the store?");


								alert.setPositiveButton("I am at the Store", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
									
										EventTracker.logEvent("Store_DropPin", false);
										locationService();
										//									if(isnetConnected.isNetworkAvailable())
										//									{
										//										//										Criteria criteria1 = new Criteria();
										//										//										provider = locationManager.getBestProvider(criteria1, false);
										//										//										location = locationManager.getLastKnownLocation(provider);
										//										//										onLocationChanged(location);
										//										if(!isGpsEnable())
										//										{
										//											showGpsAlert();
										//										}
										//										else
										//										{
										//
										//											showAccuracyDialog();
										//										}
										//									}
										//									else
										//									{
										//										showToast(getResources().getString(R.string.noInternetConnection));
										//									}
										dialog.dismiss();


									}
								});
								alert.setNegativeButton("Not now", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
									
										dialog.dismiss();

									}
								});
								alert.show();
							}
						}

					});
				}
			}
			if(title.equalsIgnoreCase("Details"))
			{
				view=inflater.inflate(R.layout.managestorelocation, null);
				progManageLocation=(ProgressBar)view.findViewById(R.id.progManageLocation);
				//Location Page
				txtManageParent=(TextView)view.findViewById(R.id.txtManageParent);
				txtManageCategory=(TextView)view.findViewById(R.id.txtManageCategory);
				btnMangeStoreSave=(Button)view.findViewById(R.id.btnMangeStoreSave);
				edtManageStoreName=(EditText)view.findViewById(R.id.edtManageStoreName);
				edtManageStreet=(EditText)view.findViewById(R.id.edtManageStreet);
				edtManageLandMark=(EditText)view.findViewById(R.id.edtManageLandMark);
				edtManageArea=(TextView)view.findViewById(R.id.edtManageArea);
				edtManageCity=(TextView)view.findViewById(R.id.edtManageCity);
				edtManagePinCode=(TextView)view.findViewById(R.id.edtManagePinCode);
				edtManageDesc=(EditText)view.findViewById(R.id.edtManageDesc);

				txtManageParent.setTypeface(tf);
				txtManageCategory.setTypeface(tf);
				btnMangeStoreSave.setTypeface(tf);
				edtManageStoreName.setTypeface(tf);
				edtManageStreet.setTypeface(tf);
				edtManageLandMark.setTypeface(tf);
				edtManageArea.setTypeface(tf);
				edtManageCity.setTypeface(tf);
				edtManagePinCode.setTypeface(tf);
				edtManageDesc.setTypeface(tf);

				if(isNew)
				{
					if(dbUtil.getActiveArea().length()==0)
					{
						edtManageArea.setText("Click to Select Area");	
					}
					else
					{
						edtManageArea.setText(dbUtil.getActiveArea());	
					}
					//					edtManageArea.setText(dbUtil.getActiveArea());
					edtManageCity.setText(dbUtil.getActiveAreaCity());
					edtManagePinCode.setText(dbUtil.getActiveAreaPin());
					area_id=dbUtil.getActiveAreaID();
					Log.i("area_id", "area_id"+area_id);
				}

				edtManageArea.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					
						locationDialog.show();
					}
				});

				locationList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
					
						edtManageArea.setText(local_area.get(position));
						edtManagePinCode.setText(local_pin.get(position));
						edtManageCity.setText(local_city.get(position));
						area_id=local_areaID.get(position);
						Log.i("Selected Area Id ", "Selected Area Id "+area_id);
						locationDialog.dismiss();
					}
				});

				btnMangeStoreSave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						fetchLocationData();
						/*if(store_name==null || store_name.length()==0)
						{
							Toast.makeText(context, "Store name cannot be blank",Toast.LENGTH_SHORT).show();
						}
						else if(store_area==null || store_area.length()==0 )
						{
							Toast.makeText(context, "Area cannot be blank",Toast.LENGTH_SHORT).show();
						}
						else if(store_city==null || store_city.length()==0 )
						{
							Toast.makeText(context, "City cannot be blank",Toast.LENGTH_SHORT).show();
						}
						else if(store_pincode==null || store_pincode.length()==0 )
						{
							Toast.makeText(context, "Pincode cannot be blank",Toast.LENGTH_SHORT).show();
						}
						else if(tempCatId.size()==0)
						{
							Toast.makeText(context, "Please select atleast one category",Toast.LENGTH_SHORT).show();
						}
						else
						{
							updateStoreData();
						}*/
						if(isValidLocationData())
						{
							updateStoreData();
						}
					}
				});

			}
			if(title.equalsIgnoreCase("contact"))
			{
				view=inflater.inflate(R.layout.managestorecontact, null);


				//contact page
				txtMangeContactStoreName=(TextView)view.findViewById(R.id.txtMangeContactStoreName);
				edtManageLandline=(EditText)view.findViewById(R.id.edtManageLandline);
				edtManageMobile=(EditText)view.findViewById(R.id.edtManageMobile);
				edtManageFax=(EditText)view.findViewById(R.id.edtManageFax);
				edtManageTollFree=(EditText)view.findViewById(R.id.edtManageTollFree);
				btnManageContactSave=(Button)view.findViewById(R.id.btnManageContactSave);
				edtManageEmailId=(EditText)view.findViewById(R.id.edtManageEmailId);
				edtManageWebSite=(EditText)view.findViewById(R.id.edtManageWebSite);

				edtManageFb=(EditText)view.findViewById(R.id.edtManageFb);
				edtManageTwitter=(EditText)view.findViewById(R.id.edtManageTwitter);

				TextView txtTempHead=(TextView)view.findViewById(R.id.txtTempHead);

				Button btnManageContactSkip=(Button)view.findViewById(R.id.btnManageContactSkip);

				landlinerow1=(TableRow)view.findViewById(R.id.landlinerow1);
				landlinerow2=(TableRow)view.findViewById(R.id.landlinerow2);
				landlinerow3=(TableRow)view.findViewById(R.id.landlinerow3);

				mobileRow1=(TableRow)view.findViewById(R.id.mobileRow1);
				mobileRow2=(TableRow)view.findViewById(R.id.mobileRow2);
				mobileRow3=(TableRow)view.findViewById(R.id.mobileRow3);

				add1=(Button)view.findViewById(R.id.add1);
				add2=(Button)view.findViewById(R.id.add2);
				add3=(Button)view.findViewById(R.id.add3);

				add4=(Button)view.findViewById(R.id.add4);
				add5=(Button)view.findViewById(R.id.add5);
				add6=(Button)view.findViewById(R.id.add6);

				edtManageLandline2=(EditText)view.findViewById(R.id.edtManageLandline2);
				edtManageLandline3=(EditText)view.findViewById(R.id.edtManageLandline3);


				edtManageMobile2=(EditText)view.findViewById(R.id.edtManageMobile2);
				edtManageMobile3=(EditText)view.findViewById(R.id.edtManageMobile3);




				/*	 landLineRowSeprator2=(View)view.findViewById(R.id.landLineRowSeprator2);


				 mobileRowSeprator2=(View)view.findViewById(R.id.mobileRowSeprator2);
				 mobileRowSeprator3=(View)view.findViewById(R.id.mobileRowSeprator3);*/

				add1.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					
						if(!landlinerow2.isShown())
						{
							landlinerow2.setVisibility(View.VISIBLE);




						}else if(!landlinerow3.isShown())
						{
							landlinerow3.setVisibility(View.VISIBLE);
						}

						if(landlinerow2.isShown() && landlinerow3.isShown())
						{
							add1.setVisibility(View.GONE);
						}
					}
				});
				add2.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					
						if(landlinerow2.isShown())
						{
							landlinerow2.setVisibility(View.GONE);
							edtManageLandline2.setText("");
							store_landline_no2="";
						}
						if(!landlinerow2.isShown())
						{
							add1.setVisibility(View.VISIBLE);
						}
					}
				});
				add3.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					

						if(landlinerow3.isShown())
						{
							landlinerow3.setVisibility(View.GONE);
							edtManageLandline3.setText("");
							store_landline_no3="";
						}
						if(!landlinerow3.isShown())
						{
							add1.setVisibility(View.VISIBLE);
						}
					}
				});

				add4.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					
						if(!mobileRow2.isShown())
						{
							mobileRow2.setVisibility(View.VISIBLE);
						}else if(!mobileRow3.isShown())
						{
							mobileRow3.setVisibility(View.VISIBLE);
						}

						if(mobileRow2.isShown() && mobileRow3.isShown())
						{
							add4.setVisibility(View.GONE);
						}
					}
				});
				add5.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					
						if(mobileRow2.isShown())
						{
							mobileRow2.setVisibility(View.GONE);
							edtManageMobile2.setText("");
							store_mobile_no2="";
						}
						if(!mobileRow2.isShown())
						{
							add4.setVisibility(View.VISIBLE);
						}
					}
				});

				add6.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
					
						if(mobileRow3.isShown())
						{
							mobileRow3.setVisibility(View.GONE);
							edtManageMobile3.setText("");
							store_mobile_no3="";
						}
						if(!mobileRow3.isShown())
						{
							add4.setVisibility(View.VISIBLE);
						}
					}
				});

				txtTempHead.setTypeface(tf);
				txtMangeContactStoreName.setTypeface(tf);
				edtManageLandline.setTypeface(tf);
				edtManageMobile.setTypeface(tf);
				edtManageFax.setTypeface(tf);
				edtManageTollFree.setTypeface(tf);
				btnManageContactSave.setTypeface(tf);
				edtManageEmailId.setTypeface(tf);
				edtManageWebSite.setTypeface(tf);
				btnManageContactSkip.setTypeface(tf);

				edtManageLandline2.setTypeface(tf);
				edtManageLandline3.setTypeface(tf);

				edtManageMobile2.setTypeface(tf);
				edtManageMobile3.setTypeface(tf);

				edtManageFb.setTypeface(tf);
				edtManageTwitter.setTypeface(tf);

				btnManageContactSkip.setOnClickListener( new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						try
						{
							mPager.setCurrentItem(mPager.getCurrentItem()+1);
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});
				btnManageContactSave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						fetchLocationData();
						fetchContactData();

						/*if(store_mobileno==null || store_mobileno.length()==0)
						{
							Toast.makeText(context, "Mobile No. cannot be blank",Toast.LENGTH_SHORT).show();
						}
						else if(store_mobileno.length()!=10)
						{
							Toast.makeText(context, "Mobile no must be 10 digit.",Toast.LENGTH_SHORT).show();
						}
						else if(store_landline.length()>0 && store_landline.length()!=11)
						{
							Toast.makeText(context, "Landline no must be 11 digit.",Toast.LENGTH_SHORT).show();
						}
						else if(store_faxno.length()>0 && store_faxno.length()!=11)
						{
							Toast.makeText(context, "Mobile no must be 11 digit.",Toast.LENGTH_SHORT).show();
						}
						else if(store_tollfree.length()>0 && store_tollfree.length()!=11)
						{
							Toast.makeText(context, "Toll free no must be 11 digit.",Toast.LENGTH_SHORT).show();
						}
						else if(store_emailId.length()>0 && !isValidEmail(store_emailId))
						{
							Toast.makeText(context, "Please enter valid email-id.",Toast.LENGTH_SHORT).show();
						}
						else
						{
							updateStoreData();
						}*/
						Log.i("Validation ", "Validation  "+isValidContactData());
						if(isValidLocationData() && isValidContactData())
						{
							updateStoreData();
						}
					}
				});


			}
			if(title.equalsIgnoreCase("Description"))
			{
				view=inflater.inflate(R.layout.merchantdescription, null);
				edtManageDesc2=(EditText)view.findViewById(R.id.edtManageDesc2);
				btnMangeStoreDescSave=(Button)view.findViewById(R.id.btnMangeStoreDescSave);
				btnMangeStoreDescSkip=(Button)view.findViewById(R.id.btnMangeStoreDescSkip);

				edtManageDesc2.setTypeface(tf);
				btnMangeStoreDescSave.setTypeface(tf);
				btnMangeStoreDescSkip.setTypeface(tf);

				btnMangeStoreDescSkip.setOnClickListener(new android.view.View.OnClickListener(){

					@Override
					public void onClick(View v) {
					
						try
						{

							mPager.setCurrentItem(mPager.getCurrentItem()+1);
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});
				btnMangeStoreDescSave.setOnClickListener(new android.view.View.OnClickListener(){

					@Override
					public void onClick(View v) {
					
						try
						{

							fetchLocationData();
							fetchContactData();

							/*if(store_mobileno==null || store_mobileno.length()==0)
							{
								Toast.makeText(context, "Mobile No. cannot be blank",Toast.LENGTH_SHORT).show();
							}
							else if(store_mobileno.length()!=10)
							{
								Toast.makeText(context, "Mobile no must be 10 digit.",Toast.LENGTH_SHORT).show();
							}
							else if(store_landline.length()>0 && store_landline.length()!=11)
							{
								Toast.makeText(context, "Landline no must be 11 digit.",Toast.LENGTH_SHORT).show();
							}
							else if(store_faxno.length()>0 && store_faxno.length()!=11)
							{
								Toast.makeText(context, "Mobile no must be 11 digit.",Toast.LENGTH_SHORT).show();
							}
							else if(store_tollfree.length()>0 && store_tollfree.length()!=11)
							{
								Toast.makeText(context, "Toll free no must be 11 digit.",Toast.LENGTH_SHORT).show();
							}
							else if(store_emailId.length()>0 && !isValidEmail(store_emailId))
							{
								Toast.makeText(context, "Please enter valid email-id.",Toast.LENGTH_SHORT).show();
							}
							else
							{
								updateStoreData();
							}*/
							Log.i("Validation ", "Validation  "+isValidContactData());

							if(isValidLocationData() && isValidContactData())
							{
								updateStoreData();
							}

							/*	mPager.setCurrentItem(mPager.getCurrentItem()+1);*/
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});

			}
			if(title.equalsIgnoreCase("operations"))
			{
				view=inflater.inflate(R.layout.managestoretimings, null);

				txtMangeStoreTimeFrom=(TextView)view.findViewById(R.id.txtMangeStoreTimeFrom);
				txtMangeStoreTimeTo=(TextView)view.findViewById(R.id.txtMangeStoreTimeTo);
				txtOperationStoreName=(TextView)view.findViewById(R.id.txtOperationStoreName);;
				txtOperationDesc=(TextView)view.findViewById(R.id.txtOperationDesc);

				imgManageStore_ThumbPreview=(ImageView)view.findViewById(R.id.imgManageStore_ThumbPreview);
				bManagetoggleSun=(ToggleButton)view.findViewById(R.id.bManagetoggleSun);
				bManagetoggleMon=(ToggleButton)view.findViewById(R.id.bManagetoggleMon);
				bManagetoggleTue=(ToggleButton)view.findViewById(R.id.bManagetoggleTue);
				bManagetoggleWed=(ToggleButton)view.findViewById(R.id.bManagetoggleWed);
				bManagetoggleThu=(ToggleButton)view.findViewById(R.id.bManagetoggleThu);
				bManagetoggleFri=(ToggleButton)view.findViewById(R.id.bManagetoggleFri);
				bManagetoggleSat=(ToggleButton)view.findViewById(R.id.bManagetoggleSat);

				btnMangeStore_ImageChooser=(Button)view.findViewById(R.id.btnMangeStore_ImageChooser);

				btnMangeStore_ImageChooser.setTypeface(tf);
				txtMangeStoreTimeFrom.setTypeface(tf);
				txtMangeStoreTimeTo.setTypeface(tf);

				/*try
				{
					fetchLocationData();
					txtOperationStoreName.setText(store_name);
					txtOperationDesc.setText(store_desc);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}*/


				btnManageTime=(Button)view.findViewById(R.id.btnManageTime);
				Button btnManageTimeSkip=(Button)view.findViewById(R.id.btnManageTimeSkip);
				btnManageTimeSkip.setTypeface(tf);
				btnManageTimeSkip.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						try
						{
							mPager.setCurrentItem(mPager.getCurrentItem()+1);
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});

				/*	DateFormat  dt=new SimpleDateFormat("hh:mm");*/



				try {
					//Formating time int 24 clock format.
					/*
					String now = new SimpleDateFormat("hh:mm aa").format(new java.util.Date().getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);

					txtMangeStoreTimeFrom.setText(time24);

					txtMangeStoreTimeTo.setText(time24);*/

					String now = new SimpleDateFormat("hh:mm aa").format(new java.util.Date().getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);

					store_time_from=time24;
					store_time_to=time24;

					txtMangeStoreTimeFrom.setText(now);

					txtMangeStoreTimeTo.setText(now);

				} catch (ParseException e) {
	
					e.printStackTrace();
				}


				/*	txtMangeStoreTimeFrom.setText(dt.format(new Date()));

				txtMangeStoreTimeTo.setText(dt.format(new Date()));
				 */
				/*	btnMangeStore_ImageChooser.setOnClickListener(new android.view.View.OnClickListener()*/ 
				imgManageStore_ThumbPreview.setOnClickListener(new android.view.View.OnClickListener(){

					@Override
					public void onClick(View v) {


					

						/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Inorbit");
						alertDialogBuilder.setMessage("Photo");
						//null should be your on click listener
						alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
							
								takePicture();
							}
						});
						alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								openGallery();
							}
						});
						alertDialogBuilder.show();*/
						try
						{
							chooseExisting=(TextView)chooseLogoDialog.findViewById(R.id.chooseExisting);
							takePic=(TextView)chooseLogoDialog.findViewById(R.id.takePic);
							chooseShoplocal=(TextView)chooseLogoDialog.findViewById(R.id.chooseShoplocal);
							removeLogo=(TextView)chooseLogoDialog.findViewById(R.id.removeLogo);
							chooseLogoDialog.show();

							//							if(!FILE_PAGER_PATH.equals("") && FILE_PAGER_PATH.length()!=0)
							//							{
							//								removeLogo.setVisibility(View.VISIBLE);
							//							}
							if(pager_byte!=null)
							{
								removeLogo.setVisibility(View.VISIBLE);
							}
							else if(isShopLocalIcon)
							{
								removeLogo.setVisibility(View.VISIBLE);
							}
							else if(!PHOTO.equals("") && PHOTO.length()!=0)
							{
								removeLogo.setVisibility(View.VISIBLE);
							}
							else
							{
								removeLogo.setVisibility(View.GONE);
							}

							removeLogo.setOnClickListener(new android.view.View.OnClickListener() {

								@Override
								public void onClick(View arg0) {
								
									/*if(!FILE_PAGER_PATH.equals("") && FILE_PAGER_PATH.length()!=0)
									{
										removeStoreLogo=1;
										chooseLogoDialog.dismiss();
										imgManageStore_ThumbPreview.setImageResource(R.drawable.add_store_logo);
									}*/
									if(pager_byte!=null)
									{
										removeStoreLogo=1;
										chooseLogoDialog.dismiss();
										imgManageStore_ThumbPreview.setImageResource(R.drawable.add_store_logo);
									}
									else if(isShopLocalIcon)
									{
										removeStoreLogo=1;
										chooseLogoDialog.dismiss();
										imgManageStore_ThumbPreview.setImageResource(R.drawable.add_store_logo);
									}
									else if(PHOTO.length()!=0)
									{
										removeStoreLogo=1;
										chooseLogoDialog.dismiss();
										imgManageStore_ThumbPreview.setImageResource(R.drawable.add_store_logo);
									}
									chooseLogoDialog.dismiss();
								}
							});

							chooseExisting.setOnClickListener(new android.view.View.OnClickListener() {

								@Override
								public void onClick(View v) {
								
									eventMap.put("source", "Gallery");
									EventTracker.logEvent("Store_LogoAdd", eventMap);
									openGallery();
									chooseLogoDialog.dismiss();
									isShopLocalIcon=false;
								}
							});
							takePic.setOnClickListener(new android.view.View.OnClickListener() {

								@Override
								public void onClick(View v) {
								
									eventMap.put("source", "Camera");
									EventTracker.logEvent("Store_LogoAdd", eventMap);
									chooseLogoDialog.dismiss();

									takePictureForLogoFromCamera();
									//takePictureForGridFromCamera();
									//takePictureForGrid();

									isShopLocalIcon=false;
								}
							});
							chooseShoplocal.setOnClickListener(new android.view.View.OnClickListener() {

								@Override
								public void onClick(View v) {
								
									eventMap.put("source", "Shoplocal");
									EventTracker.logEvent("Store_LogoAdd", eventMap);	
									isShopLocalIcon=true;
									shopLocalGallery.show();
									chooseLogoDialog.dismiss();

								}
							});

						}catch(Exception ex)
						{
							ex.printStackTrace();
						}


					}//click ends here

				});

				bManagetoggleSun.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						sun=isChecked;
					}
				});
				bManagetoggleMon.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						mon=isChecked;
					}
				});
				bManagetoggleTue.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						tue=isChecked;
					}
				});
				bManagetoggleWed.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						wed=isChecked;
					}
				});
				bManagetoggleThu.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						thu=isChecked;
					}
				});
				bManagetoggleFri.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						fri=isChecked;
					}
				});
				bManagetoggleSat.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						sat=isChecked;
					}
				});

				txtMangeStoreTimeFrom.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						new TimeFragment(context, "From").show(getFragmentManager(), "Time");
					}
				});


				txtMangeStoreTimeTo.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						new TimeFragment(context, "To").show(getFragmentManager(), "Time2");;
					}
				});

				btnManageTime.setTypeface(tf);
				btnManageTime.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						fetchLocationData();
						fetchContactData();
						fetchStoreTimingData();

						Log.i("Validation ", "Validation  "+isValidContactData());

						if(!isNew)
						{
							if(isValidLocationData() && isValidContactData())
							{
								updateStoreData();
								updateStore_timeData();
							}
						}
						else
						{
							if(isValidLocationData() && isValidContactData())
							{
								updateStoreData();

							}

						}
					}
				});
			}

			if(title.equalsIgnoreCase("Gallery"))
			{
				ArrayList<String>ID=new ArrayList<String>();

				ArrayList<String>SOURCE=new ArrayList<String>();
				ArrayList<String>TITLE=new ArrayList<String>();
				ArrayList<String>IMAGE_DATE=new ArrayList<String>();

				view=inflater.inflate(R.layout.merchantgallery, null);
				imageGridMerchant=(GridView)view.findViewById(R.id.imageGridMerchant);
				if(!isNew)
				{
					loadGallery();
				}
				else
				{
					//Set Grid Adapter with Add Photo 
					ID.add(0, "-1");
					TITLE.add(0, "Add Image");
					SOURCE.add(0, "");

					GridAdapter adapter=new GridAdapter(context, SOURCE,ID,TITLE);
					/*	imageGrid.setAdapter(adapter);*/
					try
					{
						imageGridMerchant.setAdapter(adapter);
						imageGridMerchant.setOnItemClickListener(new GridItemClickListener(true));
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}



					PHOTO_SOURCE=SOURCE;
				}

				Button btnMangeStoreGalleryave=(Button)view.findViewById(R.id.btnMangeStoreGalleryave);
				Button btnMangeStoreGallerySkip=(Button)view.findViewById(R.id.btnMangeStoreGallerySkip);

				btnMangeStoreGalleryave.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						try
						{
							if(STORE_ID!=null && STORE_ID.length()!=0)
							{
								Intent intent=new Intent(context,ShowPlaceMessage.class);
								intent.putExtra("STOREID", STORE_ID);
								startActivity(intent);
								context.finish();
								context.overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
							}
							else
							{
								showToast("Please create a store");
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});
				btnMangeStoreGallerySkip.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						try
						{
							if(STORE_ID!=null && STORE_ID.length()!=0)
							{
								Intent intent=new Intent(context,ShowPlaceMessage.class);
								startActivity(intent);
								context.finish();
								context.overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
							}
							else
							{
								showToast("Please create a store");
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});



			}




			return view;
		}



		//Time picker dialog

		//Creating Time dialoge
		public class TimeFragment extends DialogFragment implements OnTimeSetListener
		{
			Context mTContext;
			String timeset;
			public TimeFragment(Context context,String timeset) {
				mTContext = context;
				this.timeset=timeset;
			}
			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
			
				if(savedInstanceState!=null)
				{
					mTContext=getActivity().getApplicationContext();
				}
				hour=time.get(Calendar.HOUR_OF_DAY);
				min=time.get(Calendar.MONDAY);
				return new TimePickerDialog(mTContext, this, hour, min, false);
			}
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			
				time.set(Calendar.HOUR_OF_DAY,hourOfDay);
				time.set(Calendar.MINUTE,minute);
				updateTime(timeset);
			}


		}

		//Updating time on TextViews
		void updateTime(String timeset )
		{
			if(timeset.equalsIgnoreCase("From"))
			{
				DateFormat  dt=new SimpleDateFormat("hh:mm");


				try {
					//Formating time int 24 clock format.
					hour=time.get(Calendar.HOUR_OF_DAY);
					min=time.get(Calendar.MONDAY);

					String now = new SimpleDateFormat("hh:mm aa").format(time.getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);
					store_time_from=time24;
					/*txtMangeStoreTimeFrom.setText(time24);*/
					txtMangeStoreTimeFrom.setText(now);

				} catch (ParseException e) {
	
					e.printStackTrace();
				}




			}
			if(timeset.equalsIgnoreCase("To"))
			{
				DateFormat  dt=new SimpleDateFormat("hh:mm");


				try {
					//Formating time int 24 clock format.
					hour=time.get(Calendar.HOUR_OF_DAY);
					min=time.get(Calendar.MONDAY);


					String now = new SimpleDateFormat("hh:mm aa").format(time.getTime());
					System.out.println("time in 12 hour format : " + now);
					SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
					SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
					String time24;
					time24 = outFormat.format(inFormat.parse(now));

					System.out.println("time in 24 hour format : " + time24);
					/*txtMangeStoreTimeTo.setText(time24);*/
					txtMangeStoreTimeTo.setText(now);
					store_time_to=time24;

				} catch (ParseException e) {
	
					e.printStackTrace();
				}

				/*txtMangeStoreTimeTo.setText(dt.format(time.getTime()));*/

			}
		}


		//Open Gallery Image for Store Logo.
		private void openGallery() {

			//			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			//			photoPickerIntent.setType("image/*");
			//			getActivity().startActivityForResult(photoPickerIntent, REQUEST_CODE_PAGER_GALLERY);

			/*try {
				Intent intent = new Intent(context,PhotoActivity.class);
				intent.putExtra("REQUEST_CODE_PAGER_GALLERY",REQUEST_CODE_PAGER_GALLERY);
				getActivity().startActivityForResult(intent, REQUEST_CODE_PAGER_GALLERY);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}*/

			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			getActivity().startActivityForResult(photoPickerIntent, REQ_CODE_LOGO_SELECTGALLERY);
		}





		void locationService()
		{
			try
			{
				if(isnetConnected.isNetworkAvailable())
				{
					//calling location service.
					Intent intent=new Intent(context, LocationIntentService.class);
					intent.putExtra("receiverTag", mLocation);
					context.startService(intent);
					/*ShopLocalMain.shopProgress.setVisibility(View.VISIBLE);*/
					progStoreUpdate.setVisibility(View.VISIBLE);
				}else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}



		/*
		 * (non-Javadoc)
		 * @see com.phonethics.networkcall.LocationResultReceiver.Receiver#onReceiveResult(int, android.os.Bundle)
		 */
		@Override
		public void onReceiveResult(int resultCode, Bundle resultData) {
		
			/*ShopLocalMain.shopProgress.setVisibility(View.GONE);*/
			progStoreUpdate.setVisibility(View.GONE);
			String status=resultData.getString("status");
			//showToast("Receiver called "+status);
			if(status.equalsIgnoreCase("set"))
			{
				try
				{
					latitued=resultData.getString("Lat");
					longitude=resultData.getString("Longi");

					location=new Location("locations");
					if(latitued!=null || latitued.length()>0 && longitude!=null || longitude.length()>0)
					{
						/*location.setLatitude(Double.parseDouble(latitued));
						location.setLongitude(Double.parseDouble(longitude));*/


						setImage(latitued,longitude);
						/*onLocationChanged(location);*/
						//	showToast("Location");
					}
					Log.i("Location ", "LOCATION : "+latitued+" , "+longitude);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			else
			{

				try
				{

					if(latitued==null || latitued.length()==0 && longitude==null || longitude.length()==0)
					{
						showToast(getResources().getString(R.string.locationAlert));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}
		}

		/*
		 * All Store Receiver
		 * (non-Javadoc)
		 * @see com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore#onReceiveUsersAllStore(int, android.os.Bundle)
		 */
		@Override
		public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
		
			progStoreUpdate.setVisibility(View.GONE);
		}

		public void showGpsAlert(){

			// Build the alert dialog
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Location Services Not Active");
			builder.setMessage("Please enable Location Services and GPS Satellites for better Results");
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialogInterface, int i) {
					// Show location settings when the user acknowledges the alert dialog
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(intent, 10);
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
				
					//if user doesnt turn on location services put location found as false
					try
					{
						showAccuracyDialog();
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});
			Dialog alertDialog = builder.create();
			alertDialog.setCanceledOnTouchOutside(false);
			alertDialog.show();


		}

		public boolean isGpsEnable(){
			boolean isGpsOn = false;
			try{
				/*	LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
				isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);*/
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return isGpsOn;
		}



		public void showAccuracyDialog(){


			try{
				if(locationManager!=null){


					try{
						if(isGpsEnable()){
							Log.d("", "ShopLocal GPS Enable");
							//						provider = 	LocationManager.GPS_PROVIDER;
							//						location = locationManager.getLastKnownLocation(provider);
							provider=LocationManager.GPS_PROVIDER;
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							//							showToast("IN  gps");
							if(location==null)
							{
								if(isGPSEnabled)
								{
									provider=LocationManager.GPS_PROVIDER;
									location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
									//									showToast("IN error gps");
								}
								else
								{
									provider=LocationManager.NETWORK_PROVIDER;
									location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
									//									showToast("IN error net");
								}
							}
						}else{
							Log.d("", "Inorbit GPS Disable");
							//						Criteria criteria1 = new Criteria();
							//						provider = locationManager.getBestProvider(criteria1, false);
							//						location = locationManager.getLastKnownLocation(provider);
							provider=LocationManager.NETWORK_PROVIDER;
							location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
							//							showToast("IN  net");
							if(location==null)
							{
								if(isGPSEnabled)
								{
									provider=LocationManager.GPS_PROVIDER;
									location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
									//									showToast("IN error gps");
								}
								else
								{
									provider=LocationManager.NETWORK_PROVIDER;
									location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
									//									showToast("IN error net");
								}
							}
						}

						//					if(location==null){
						//						Log.d("", "Inorbit GPS location null");
						//						Criteria criteria1 = new Criteria();
						//						provider = locationManager.getBestProvider(criteria1, false);
						//						location = locationManager.getLastKnownLocation(provider);
						//					}
					}catch(NullPointerException npx){
						npx.printStackTrace();
						//					Criteria criteria1 = new Criteria();
						//					provider = locationManager.getBestProvider(criteria1, false);
						if(isGPSEnabled)
						{
							provider=LocationManager.GPS_PROVIDER;
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							//							showToast("IN error gps");
						}
						else
						{
							provider=LocationManager.NETWORK_PROVIDER;
							location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
							//							showToast("IN error net");
						}
					}



					//edit_data.setText("A : "+location.getAccuracy()+" Lat : "+location.getLatitude()+" Lo : "+location.getLongitude());
					Log.d("", "Shoplocal LatLong NOT Null");
					//					showToast("Final Provider "+provider);
					locationManager.requestLocationUpdates(provider, 1000*15,  0,this);
					//textAcc.setText("Required Accuracy : 10 meters\nCurrent Accuracy : "+location.getAccuracy()+" meters");
					Button bttn = (Button) loc_dialog.findViewById(R.id.bttn_dismiss);
					bttn.setText("Save with current accuracy");
					bttn.setTextSize(14);

					bttn.setOnClickListener(new android.view.View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
						
							loc_dialog.dismiss();


							Log.i("", "ShopLocal Location changed "+" Lat "+saved_lati+" Long "+saved_longi);

							setImage(saved_lati+"", saved_longi+"");

						}
					});




					//								locationManager.requestLocationUpdates(provider, 1000*15,  0,new LocationListener() {
					//				
					//									@Override
					//									public void onStatusChanged(String provider, int status, Bundle extras) {
					//									
					//				
					//									}
					//				
					//									@Override
					//									public void onProviderEnabled(String provider) {
					//									
					//				
					//									}
					//				
					//									@Override
					//									public void onProviderDisabled(String provider) {
					//									
					//				
					//									}
					//				
					//									@Override
					//									public void onLocationChanged(Location location) {
					//									
					//									
					//										
					//										latitude = location.getLatitude();
					//
					//
					//										// Getting longitude of the current location
					//										longitude = location.getLongitude();
					//
					//										// Creating a LatLng object for the current location
					//										latLng = new LatLng(latitude, longitude);
					//
					//										// Showing the current location in Google Map
					//										saved_lati = latitude;
					//
					//										saved_longi = longitude;
					//
					//
					//										textAcc.setText("Required Accuracy : 10 meters\nCurrent Accuracy : "+location.getAccuracy() +" meters");
					//										Log.i("", "ShopLocal Location changed "+location.getAccuracy()+" Lat "+latitude+" Long "+longitude);
					//										Toast.makeText(context, "Changed Accuracy " +location.getAccuracy(), 0).show();
					//										if(location.getAccuracy() <=10){
					//											dialog.dismiss();
					//
					//
					//
					//											saved_lati = latitude;
					//
					//											saved_longi = longitude;
					//
					//
					//											LatLng prkpos = new LatLng(saved_lati, saved_longi);
					//
					//
					//
					//										}
					//										
					//									}
					//								});



					loc_dialog.show();
					if(!loc_dialog.isShowing())
					{
						locationManager.removeUpdates(this);
					}

				}else{
					Log.d("", "ShopLocal location manager null");
				}

			}catch(Exception ex){
				ex.printStackTrace();
			}






		}


		@Override
		public void onStop() {

			EventTracker.endFlurrySession(context);
			super.onStop();
			try
			{

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}


		@Override
		public void onLocationChanged(Location location) {
		
			//			latitude = location.getLatitude();
			//
			//
			//			// Getting longitude of the current location
			//			longitude = location.getLongitude();

			// Creating a LatLng object for the current location
			//	latLng = new LatLng( location.getLatitude(), location.getLongitude());

			// Showing the current location in Google Map
			//			saved_lati = location.getLatitude();
			//
			//			saved_longi = location.getLongitude();
			//
			//
			//			textAcc.setText("Required Accuracy : 10 meters\nCurrent Accuracy : "+location.getAccuracy() +" meters");
			//			Log.i("", "ShopLocal Location changed "+location.getAccuracy()+" Lat "+saved_lati+" Long "+saved_longi);
			//			//Toast.makeText(context, "Changed Accuracy " +location.getAccuracy(), 0).show();
			//			if(location.getAccuracy() <=10){
			//				loc_dialog.dismiss();
			//
			//
			//
			//				saved_lati = location.getLatitude();
			//
			//				saved_longi = location.getLongitude();
			//
			//
			//				//				LatLng prkpos = new LatLng(saved_lati, saved_longi);
			//
			//				//				setPref();
			//
			//			}

			try
			{


				if(location!=null)
				{


					// Creating a LatLng object for the current location
					//			latLng = new LatLng(latitude, longitude);

					// Showing the current location in Google Map
					saved_lati = location.getLatitude();

					saved_longi = location.getLongitude();

					//					showToast("Location called");

					textAcc.setText("Required Accuracy : 10 meters\nCurrent Accuracy : "+location.getAccuracy() +" meters");
				}
				else
				{
					textAcc.setText("Fetching Location please wait.");
				}
				//		Log.i("", "ShopLocal Location changed "+location.getAccuracy()+" Lat "+latitude+" Long "+longitude);
				//Toast.makeText(context, "Changed Accuracy " +location.getAccuracy(), 0).show();
				if(location!=null)
				{
					if(location.getAccuracy() <=10){
						loc_dialog.dismiss();



						saved_lati = location.getLatitude();

						saved_longi = location.getLongitude();


						//						LatLng prkpos = new LatLng(saved_lati, saved_longi);



					}
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}
		@Override
		public void onProviderDisabled(String provider) {
		

		}
		@Override
		public void onProviderEnabled(String provider) {
		

		}
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		

		}





	} //end of Fragment class

	@Override
	protected void onResume() {

		super.onResume();
		/*if(imageget==true && imagegetdata!=null){
			showCropDialog(imagegetdata);
		}*/
		EventTracker.startLocalyticsSession(getApplicationContext());
		try
		{
			areaBroadCast=new AreaBroadCast();
			IntentFilter filter = new IntentFilter(RequestTags.TagAreas);
			context.registerReceiver(areaBroadCast, filter);

		}catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	public void getAreas(){

		Intent intent = new Intent(context, GetAreaService.class);
		intent.putExtra("getArea", getAreaRec);
		intent.putExtra("URL", STORE_URL+AREA_URL+ISO_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		context.startService(intent);
		progStoreUpdate.setVisibility(View.VISIBLE);

	}

	public class AreaBroadCast extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {
		
			Log.i("Broadcast Called", "Broadcast called in EditStore");
			updateList();

		}

	}
	void updateList()
	{
		local_city=dbUtil.getAllAreaCity();
		local_areaID=dbUtil.getAllAreaIds();
		local_pin=dbUtil.getAllAreaPin();
		local_area=dbUtil.getAllAreas();
		locationList.setAdapter(new LocationListAdapter(context, 0, 0, local_area));

	}
	@Override
	protected void onStop() {
	
		super.onStop();


		try
		{
			if(areaBroadCast!=null)
			{
				context.unregisterReceiver(areaBroadCast);
				areaBroadCast=null;
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}





	/*
	 * Upadate store receiver
	 * 
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.UpdateStoreDetailsReceiver.UpdateStoreReceiver#updateStoreReceiver(int, android.os.Bundle)
	 */
	@Override
	public void updateStoreReceiver(int resultCode, Bundle resultData) {
	
		try
		{
			EditStore.progStoreUpdate.setVisibility(View.GONE);
			String status=resultData.getString("store_status");
			String store_data=resultData.getString("store_data");
			String msg=resultData.getString("store_msg");
			if(status.equalsIgnoreCase("true"))
			{
				if(removeStoreLogo==1)
				{
					removeStoreLogo=0;
				}

				if(mPager.getCurrentItem()!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(mPager.getCurrentItem()+1,true);
				}
				/*if(mPager.getCurrentItem()==mPager.getAdapter().getCount())
			{

			}*/

				if(!isNew)
				{
					//	Toast.makeText(context, "Store updated successfully", Toast.LENGTH_SHORT).show();
					showToast(msg);
					invalidateActionBar();
					EventTracker.logEvent("Store_Modified", false);
					/*if(mPager.getCurrentItem()!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(mPager.getCurrentItem()+1,true);
				}*/
				}
				else
				{
					showToast(msg);
					//Toast.makeText(context, "Store created successfully", Toast.LENGTH_SHORT).show();
					STORE_ID=store_data;
					Log.i("STORE_ID", "STORE_ID CREATED" +STORE_ID);
					updateStore_timeData();
					isNew=false;
					EventTracker.logEvent("Store_Added", false);
					invalidateActionBar();
				}
				//Fetch and store all stores of users in db.
				//					loadAllStoreForUser();

				SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
				Editor editor=prefs.edit();
				editor.putBoolean("isPlaceRefreshRequired", true);
				editor.commit();

			}
			if(status.equalsIgnoreCase("false"))
			{

				showToast(msg);

				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					session.logoutUser();
					/*	finishTo();*/
					invalidAuthFinish();
				}
			}
			else if(status.equalsIgnoreCase("error"))
			{
				showToast(msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.BusinessOperationReceiver.OperationReceiver#onReciveOperationResult(int, android.os.Bundle)
	 */
	@Override
	public void onReciveOperationResult(int reulstCode, Bundle resultData) {
	
		try
		{
			EditStore.progStoreUpdate.setVisibility(View.GONE);
			String store_operation=resultData.getString("store_operation");

			if(store_operation.equalsIgnoreCase("false"))
			{
				String store_msg=resultData.getString("store_msg");
				showToast(store_msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					session.logoutUser();
					/*finishTo();*/
					invalidAuthFinish();
				}
				//			if(store_msg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				session.logoutUser();
				//				finishTo();
				//			}
			}
			if(store_operation.equalsIgnoreCase("error"))
			{
				String store_msg=resultData.getString("store_msg");
				showToast(store_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Get data of location pages.
	static void fetchLocationData()
	{
		/*String store_name,store_street,store_landmark,store_area,store_city,store_pincode;*/
		//Location page details.
		View page=(View)mPager.getChildAt(1);
		/*TextView tempManageParent=(TextView)page.findViewById(R.id.txtManageParent);
				TextView tempManageCategory=(TextView)page.findViewById(R.id.txtManageCategory);*/
		EditText tempManageStoreName=(EditText)mPager.findViewById(R.id.edtManageStoreName);
		EditText tempManageStreet=(EditText)mPager.findViewById(R.id.edtManageStreet);
		EditText tempManageLandMark=(EditText)mPager.findViewById(R.id.edtManageLandMark);
		TextView tempManageArea=(TextView)mPager.findViewById(R.id.edtManageArea);
		TextView tempManageCity=(TextView)mPager.findViewById(R.id.edtManageCity);
		TextView tempManagePinCode=(TextView)mPager.findViewById(R.id.edtManagePinCode);
		EditText tempManageDesc=(EditText)mPager.findViewById(R.id.edtManageDesc2);

		/*store_parent=tempManageParent.getText().toString();
				store_category=tempManageCategory.getText().toString();*/
		store_name=tempManageStoreName.getText().toString();
		store_street=tempManageStreet.getText().toString();
		store_landmark=tempManageLandMark.getText().toString();
		store_area=tempManageArea.getText().toString();
		store_city=tempManageCity.getText().toString();
		store_pincode=tempManagePinCode.getText().toString();
		store_desc=tempManageDesc.getText().toString();
	}

	static void fetchContactData()
	{
		try
		{

			//Contact Page 
			//			TextView tempMangeContactStoreName=(TextView)mPager.findViewById(R.id.txtMangeContactStoreName);
			EditText tempManageLandline=(EditText)mPager.findViewById(R.id.edtManageLandline);
			EditText tempManageMobile=(EditText)mPager.findViewById(R.id.edtManageMobile);
			EditText tempManageFax=(EditText)mPager.findViewById(R.id.edtManageFax);
			EditText tempManageTollFree=(EditText)mPager.findViewById(R.id.edtManageTollFree);
			EditText tempManageEmailId=(EditText)mPager.findViewById(R.id.edtManageEmailId);
			EditText tempManageWebsite=(EditText)mPager.findViewById(R.id.edtManageWebSite);

			EditText tempedtManageLandline2=(EditText)mPager.findViewById(R.id.edtManageLandline2);
			EditText tempedtManageLandline3=(EditText)mPager.findViewById(R.id.edtManageLandline3);


			EditText tempedtManageMobile2=(EditText)mPager.findViewById(R.id.edtManageMobile2);
			EditText tempedtManageMobile3=(EditText)mPager.findViewById(R.id.edtManageMobile3);

			EditText tempedtManageFb=(EditText)mPager.findViewById(R.id.edtManageFb);
			EditText tempedtManageTwitter=(EditText)mPager.findViewById(R.id.edtManageTwitter);

			/*static String store_landline,store_mobileno,store_faxno,store_tollfree,store_time_from,store_time_to;*/
			/*store_name=tempMangeContactStoreName.getText().toString();*/
			store_landline=tempManageLandline.getText().toString();
			store_mobileno=mCountryCode+tempManageMobile.getText().toString();
			store_faxno=tempManageFax.getText().toString();
			store_tollfree=tempManageTollFree.getText().toString();
			store_emailId=tempManageEmailId.getText().toString();
			store_web=tempManageWebsite.getText().toString();

			store_landline_no2=tempedtManageLandline2.getText().toString();
			store_landline_no3=tempedtManageLandline3.getText().toString();

			store_mobile_no2=mCountryCode+tempedtManageMobile2.getText().toString();
			store_mobile_no3=mCountryCode+tempedtManageMobile3.getText().toString();

			store_facebook_url=tempedtManageFb.getText().toString();
			store_twitter_url=tempedtManageTwitter.getText().toString();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	static boolean isValidLocationData()
	{
		if(store_name==null || store_name.length()==0)
		{
			//					Toast.makeText(context, "Store name cannot be blank",Toast.LENGTH_SHORT).show();
			showToast(context.getResources().getString(R.string.storeNameAlert));

			return false;
		}
		else if(area_id==null || area_id.length()==0)
		{
			showToast("Area cannot be blank");
			return false;
		}
		else if(store_area==null || store_area.length()==0 )
		{
			//					Toast.makeText(context, "Area cannot be blank",Toast.LENGTH_SHORT).show();
			showToast("Area cannot be blank");

			return false;
		}
		else if(store_city==null || store_city.length()==0 )
		{
			//					Toast.makeText(context, "City cannot be blank",Toast.LENGTH_SHORT).show();
			showToast("City cannot be blank");
			return false;
		}
		else if(store_pincode==null || store_pincode.length()==0 )
		{
			//					Toast.makeText(context, "Pincode cannot be blank",Toast.LENGTH_SHORT).show();
			showToast("Pincode cannot be blank");
			return false;
		}
		else if(store_pincode.length()<6)
		{
			//					Toast.makeText(context, "Pincode must be 6 digit.",Toast.LENGTH_SHORT).show();
			showToast("Pincode must be 6 digit");
			return false;
		}
		else if(tempCatId.size()==0 && STORE_CATEGORY_ID.size()==0)
		{
			//					Toast.makeText(context, "Please select atleast one category",Toast.LENGTH_SHORT).show();
			showToast(context.getResources().getString(R.string.storeCategoryAlert));
			return false;
		}
		else
		{
			return true;
		}

	}

	//			void showToast(String text)
	//			{
	//				Toast.makeText(context, text,Toast.LENGTH_LONG).show();
	//			}
	static boolean isValidContactData()
	{
		/*if(store_mobileno==null || store_mobileno.length()==0)
				{
					Toast.makeText(context, "Mobile No. cannot be blank",Toast.LENGTH_SHORT).show();
					return false;
				}*/
		/*else*/
		/*	if(store_mobileno.length()>2 && store_mobileno.length()!=12)*/

		try
		{

			EditText tempedtManageLandline2=(EditText)mPager.findViewById(R.id.edtManageLandline2);
			EditText tempedtManageLandline3=(EditText)mPager.findViewById(R.id.edtManageLandline3);

			EditText tempManageMobile=(EditText)mPager.findViewById(R.id.edtManageMobile);
			EditText tempedtManageMobile2=(EditText)mPager.findViewById(R.id.edtManageMobile2);
			EditText tempedtManageMobile3=(EditText)mPager.findViewById(R.id.edtManageMobile3);

			if(tempManageMobile.getText().toString().length()>0 && tempManageMobile.getText().toString().length()!=10)
			{
				//						Toast.makeText(context, "Mobile no must be 10 digit",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.mobileValidationAlert));

				return false;
			}
			/*		else if(store_mobile_no2.length()>2 && store_mobile_no2.length()!=12)*/
			else if(tempedtManageMobile2.getText().toString().length()>0 && tempedtManageMobile2.getText().toString().length()!=10)
			{
				//						Toast.makeText(context, "Mobile no must be 10 digit",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.mobileValidationAlert));
				return false;
			}
			/*	else if(store_mobile_no3.length()>2 && store_mobile_no3.length()!=12)*/
			else if(tempedtManageMobile3.getText().toString().length()>0 && tempedtManageMobile3.getText().toString().length()!=10)
			{
				//						Toast.makeText(context, "Mobile no must be 10 digit",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.mobileValidationAlert));
				return false;
			}
			else if(store_landline.length()>0 && store_landline.length()!=11)
			{
				//						Toast.makeText(context, "Landline no must be 11 digit",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.landlineValidationAlert));
				return false;
			}
			else if(store_landline_no2.length()>0 && store_landline_no2.length()!=11)
			{
				//						Toast.makeText(context, "Landline no must be 11 digit",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.landlineValidationAlert));
				return false;
			}
			else if(store_landline_no3.length()>0 && store_landline_no3.length()!=11)
			{
				//						Toast.makeText(context, "Landline no must be 11 digit",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.landlineValidationAlert));
				return false;
			}
			else if(store_emailId.length()>0 && !isValidEmail(store_emailId))
			{
				//						Toast.makeText(context, "Please enter valid email-id",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.emailIdAlert));
				return false;
			}

			else if(( (tempManageMobile.getText().toString().length()==0) && (tempedtManageMobile2.getText().toString().length()==0) && (tempedtManageMobile3.getText().toString().length()==0)  ) && ((store_landline.length()==0) && (store_landline_no2.toString().length()==0) && (store_landline_no3.length()==0))) 
			{
				//						Toast.makeText(context, "Enter valid contact numbers that customers can call to Buy or contact you",Toast.LENGTH_SHORT).show();
				showToast(context.getResources().getString(R.string.contactInfo));
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception ex)
		{

			ex.printStackTrace();
			return false;
		}
		/*else if(store_faxno.length()>0 && store_faxno.length()!=11)
				{
					Toast.makeText(context, "Mobile no must be 11 digit.",Toast.LENGTH_SHORT).show();
					return false;
				}
				else if(store_tollfree.length()>0 && store_tollfree.length()!=11)
				{
					Toast.makeText(context, "Toll free no must be 11 digit.",Toast.LENGTH_SHORT).show();
					return false;
				}*/


	}

	//Validating Email id.
	public final static  boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}   


	static void fetchStoreTimingData()
	{
		View page=(View)mPager.getChildAt(3);

		TextView txtTempTimeFrom=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeFrom);
		TextView txtTempTimeTo=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeTo);
		ToggleButton TemptoggleSun=(ToggleButton)mPager.findViewById(R.id.bManagetoggleSun);
		ToggleButton TemptoggleMon=(ToggleButton)mPager.findViewById(R.id.bManagetoggleMon);
		ToggleButton TemptoggleTue=(ToggleButton)mPager.findViewById(R.id.bManagetoggleTue);
		ToggleButton TemptoggleWed=(ToggleButton)mPager.findViewById(R.id.bManagetoggleWed);
		ToggleButton TemptoggleThu=(ToggleButton)mPager.findViewById(R.id.bManagetoggleThu);
		ToggleButton TemptoggleFri=(ToggleButton)mPager.findViewById(R.id.bManagetoggleFri);
		ToggleButton TemptoggleSat=(ToggleButton)mPager.findViewById(R.id.bManagetoggleSat);



		sun=TemptoggleSun.isChecked();
		mon=TemptoggleMon.isChecked();
		tue=TemptoggleTue.isChecked();
		wed=TemptoggleWed.isChecked();
		thu=TemptoggleThu.isChecked();
		fri=TemptoggleFri.isChecked();
		sat=TemptoggleSat.isChecked();

		/*store_time_from=txtTempTimeFrom.getText().toString();
				store_time_to=txtTempTimeTo.getText().toString();*/

		if(store_time_from==null || store_time_from.length()==0)
		{
			store_time_from="00:00:00";

		}else if(store_time_to==null || store_time_to.length()==0)
		{
			store_time_to="00:00:00";
		}

	}

	static void updateStore_timeData()
	{
		try
		{
			Intent intent=new Intent(context, BusinessOperationService.class);
			intent.putExtra("business_operation", mBusinessStoreOperationReceiver);
			intent.putExtra("URL", STORE_URL+BUSINESS_STORE_OPERATION_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("starttime",store_time_from);
			intent.putExtra("closetime", store_time_to);
			intent.putExtra("store_id", STORE_ID);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			//If Store is updating!
			if(isNew==false)
			{
				intent.putExtra("isput", true);
			}
			else
			{
				intent.putExtra("isput", false);
			}
			intent.putExtra("isSunOpen", sun);
			intent.putExtra("isMonOpen", mon);
			intent.putExtra("isTueOpen", tue);
			intent.putExtra("isWedOpen", wed);
			intent.putExtra("isThuOpen", thu);
			intent.putExtra("isFriOpen", fri);
			intent.putExtra("isSatOpen", sat);
			context.startService(intent);
			progStoreUpdate.setVisibility(View.VISIBLE);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	static void updateStoreData()
	{

		try
		{
			if(isnetConnected.isNetworkAvailable())
			{
				Intent intent=new Intent(context, UpdateStoreDetailsService.class);
				intent.putExtra("updateStore", mUpdate);
				intent.putExtra("URL", STORE_URL+ADD_BUSINESS_STORE_PATH);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("store_name", store_name);
				intent.putExtra("place_parent", OWNER_ID);
				ArrayList<String>test=new ArrayList<String>();
				/*for(int i=0;i<tempCatId.length;i++)
				{
					test.add(tempCatId[i]);	
				}*/
				intent.putStringArrayListExtra("category",tempCatId);
				/*			intent.putStringArrayListExtra("category",test);*/
				intent.putExtra("description", store_desc);
				intent.putExtra("building", "");
				intent.putExtra("street", store_street);
				intent.putExtra("landmark", store_landmark);
				intent.putExtra("area", store_area);
				intent.putExtra("pincode", store_pincode);
				intent.putExtra("city", store_city);
				intent.putExtra("place_status", place_status);
				intent.putExtra("area_id",area_id);

				/*if(!getResources().getBoolean(R.bool.isAppShopLocal))
				{
					latitued="19.1885679899768";  //malad 
					longitude="72.8349024609436";
				}*/

				intent.putExtra("latitude", latitued);
				intent.putExtra("longitude", longitude);

				if(store_mobileno!=null && store_mobileno.length()<=2)
				{
					store_mobileno="";
				}


				EditText tempManageMobile=(EditText)mPager.findViewById(R.id.edtManageMobile);
				EditText tempedtManageMobile2=(EditText)mPager.findViewById(R.id.edtManageMobile2);
				EditText tempedtManageMobile3=(EditText)mPager.findViewById(R.id.edtManageMobile3);

				if(tempManageMobile.getText().toString().length()!=10)
				{
					store_mobileno="";

				}
				if(tempedtManageMobile2.getText().toString().length()!=10)
				{
					store_mobile_no2="";
				}

				if(tempedtManageMobile3.getText().toString().length()!=10)
				{
					store_mobile_no3="";
				}


				intent.putExtra("mobile",store_mobileno);
				intent.putExtra("mobile2",store_mobile_no2);
				intent.putExtra("mobile3",store_mobile_no3);


				intent.putExtra("landline", store_landline);
				intent.putExtra("landline2", store_landline_no2);
				intent.putExtra("landline3", store_landline_no3);



				intent.putExtra("fax", store_faxno);
				intent.putExtra("tollfree", store_tollfree);
				intent.putExtra("store_email", store_emailId);
				intent.putExtra("store_Web", store_web);
				intent.putExtra("store_fb",store_facebook_url);
				intent.putExtra("store_twitter",store_twitter_url);

				/*String encodedPagerpath="";
				if(!FILE_PAGER_PATH.equals("") && FILE_PAGER_PATH.length()!=0)
				{
					byte [] b=imageTobyteArray(FILE_PAGER_PATH,150,150);
					if(b!=null)
					{
						encodedPagerpath=Base64.encodeToString(b, Base64.DEFAULT);
						if(encodedPagerpath==null || encodedPagerpath.equals("")){
							encodedPagerpath="";
						}
					}
				}*/
				String encodedPagerpath="";
				if(pager_byte!=null)
				{


					encodedPagerpath=Base64.encodeToString(pager_byte, Base64.DEFAULT);
					if(encodedPagerpath==null || encodedPagerpath.equals("")){
						encodedPagerpath="";
					}

				}

				if(!encodedPagerpath.equals("") || encodedPagerpath.length()!=0)
				{
					intent.putExtra("filename", FILE_PAGER_NAME);
					intent.putExtra("filetype", FILE_PAGER_TYPE);
					intent.putExtra("userfile", encodedPagerpath);
				}
				intent.putExtra("user_id", USER_ID);
				intent.putExtra("auth_id", AUTH_ID);

				//If Store is not updating!
				if(isNew==false)
				{
					intent.putExtra("place_id", STORE_ID);
					intent.putExtra("isUpdating", true);
				}
				else if(isNew==true)
				{
					intent.putExtra("place_id", STORE_ID);
					intent.putExtra("isUpdating", false);

				}
				if(isShopLocalIcon==true)
				{
					intent.putExtra("isShopLocalIcon", isShopLocalIcon);
					intent.putExtra("gallery_image", SHOPLOCAL_ICON_URL);
				}
				if(removeStoreLogo==1)
				{
					intent.putExtra("removeStoreLogo", removeStoreLogo);
				}
				context.startService(intent);
				progStoreUpdate.setVisibility(View.VISIBLE);
			}
			else
			{
				showToast(context.getResources().getString(R.string.noInternetConnection));
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	static void setImage(String latitude,String longitude)
	{
		try
		{


			ImageView staticMap=(ImageView)mPager.findViewById(R.id.mapImage);
			Button btnSkip=(Button)mPager.findViewById(R.id.skipButton);
			TextView shopDropMessage2=(TextView)mPager.findViewById(R.id.shopDropMessage2);
			TextView txtDropMessage=(TextView)mPager.findViewById(R.id.txtDropMessage);

			txtDropMessage.setVisibility(View.INVISIBLE);
			shopDropMessage2.setVisibility(View.INVISIBLE);

			txtDropMessage.setVisibility(View.INVISIBLE);
			shopDropMessage2.setVisibility(View.VISIBLE);
			shopDropMessage2.setText("Location Found.Swipe to continue.");
			shopDropMessage2.setTextColor(Color.WHITE);
			btnSkip.setText("Proceed");



			String url="http://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&zoom=16&size="+300+"x"+150+"&maptype=roadmap&markers=color:blue|label:S|"+latitude+","+longitude+"&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g";
			staticMap.setScaleType(ScaleType.FIT_XY);
			staticMap.setAdjustViewBounds(true);
			/*imageLoader.DisplayImage(url, staticMap);*/
			try{
				Picasso.with(context).load(url)
				.placeholder(R.drawable.ic_place_holder)
				.error(R.drawable.ic_place_holder)
				.into(staticMap);
			}
			catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){ 
				e.printStackTrace();
			}


			if(isNew)
			{
				if(mPager.getCurrentItem()!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(mPager.getCurrentItem()+1);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Call Service to get perticular store details
	void getPerticularStoreDetails()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, GetPerticularStoreDetailService.class);
			intent.putExtra("getStore",mGetStoreReceiver);
			intent.putExtra("URL", STORE_URL+STORE);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("store_id", STORE_ID);
			intent.putExtra("isMerchant", true);
			/*		intent.putExtra("business_id", BUSINESS_ID);*/
			context.startService(intent);
			progUpload.setVisibility(View.VISIBLE);
		}else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	//Call Service to get perticular store operation details
	void getPerticularStoreOperationDetails()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, GetPerticularStoreOperationService.class);
			intent.putExtra("getOperation",mGetStoreOperationReceiver);
			intent.putExtra("URL", STORE_URL+STORE_OPERATION);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("store_id", STORE_ID);
			/*intent.putExtra("business_id", BUSINESS_ID);*/
			context.startService(intent);
			progUpload.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	/*
	 * Load All Categories
	 */
	void loadAllCategories()
	{
		if(isnetConnected.isNetworkAvailable())
		{

			Intent intent=new Intent(context,GetPlaceCategoryService .class);
			intent.putExtra("category",mStoreCategoryReceiver);
			intent.putExtra("URL", STORE_URL+STORE_CATEGORY);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("IsAppend", true);
			intent.putExtra("value", "shoplocal");
			/*		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);*/
			context.startService(intent);
			progUpload.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	/*
	 * Load All Owner List.
	 */
	void loadOwnerList()
	{
		Intent intent=new Intent(context,StoreOwnerDropDownService .class);
		intent.putExtra("storedrop",mStoreDropDownReceiver);
		intent.putExtra("URL", STORE_URL+STORE_OWNER_LIST);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("IsAppend", true);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		context.startService(intent);
		progUpload.setVisibility(View.VISIBLE);
	}

	static void loadGallery()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//Toast.makeText(context, "Loading Gallery", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(context, LoadStoreGallery.class);
			intent.putExtra("loadgall",mReceiver);
			intent.putExtra("URL", STORE_URL+STORE_GALLERY);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			/*		intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);*/
			intent.putExtra("store_id", STORE_ID);
			context.startService(intent);
			progUpload.setVisibility(View.VISIBLE);
		}
		else
		{
			Toast.makeText(context, context.getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
		}
	}
	void loadShopLocalGallery()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//	Toast.makeText(context, "Loading Gallery", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(context, GetShopLocalGallery.class);
			intent.putExtra("getShopLocalGallery",mShopLocal);
			intent.putExtra("URL", STORE_URL+SHOPLOCAL_GALLERY);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			context.startService(intent);
			progUpload.setVisibility(View.VISIBLE);
		}
		else
		{
			Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
		}
	}
	void postGalleryData()
	{
		try
		{
			if(isnetConnected.isNetworkAvailable())
			{
				String encodedpath="";
				/*byte [] b=imageTobyteArray(FILE_PATH,480,480);*/
				if(gallery_byte!=null)
				{
					encodedpath=Base64.encodeToString(gallery_byte, Base64.DEFAULT);
					Log.i("Encode ", "Details : "+encodedpath);
					Intent intent=new Intent(context, PostStorePhoto.class);
					intent.putExtra("postPhoto",mPostPhoto);
					intent.putExtra("URL", STORE_URL+STORE_GALLERY);
					intent.putExtra("api_header",API_HEADER);
					intent.putExtra("api_header_value", API_VALUE);
					intent.putExtra("user_id", USER_ID);
					intent.putExtra("auth_id", AUTH_ID);
					intent.putExtra("store_id", STORE_ID);
					intent.putExtra("title", imageTitle);
					intent.putExtra("filename", FILE_NAME);
					intent.putExtra("filetype", FILE_TYPE);
					intent.putExtra("userfile", encodedpath);
					context.startService(intent);
					progUpload.setVisibility(View.VISIBLE);
				}
				else
				{
					encodedpath="undefined";
					Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show();
				}
			}
			else
			{
				Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_SHORT).show();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.GetShopLocalGalleryReceiver.ShopLocalGallery#onReceiveShopLocalGallery(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveShopLocalGallery(int resultCode, Bundle resultData) {

		progUpload.setVisibility(View.GONE);

		try
		{
			String GALLERY_STATUS=resultData.getString("GALLERY_STATUS");
			String GALLERY_MESSAGE=resultData.getString("GALLERY_MESSAGE");
			SHOPLOCAL_SOURCE=resultData.getStringArrayList("SOURCE");
			if(GALLERY_STATUS.equalsIgnoreCase("true")){

				showToast(GALLERY_MESSAGE);
				ShopLocalGridAdapter adapter=new ShopLocalGridAdapter(context, SHOPLOCAL_SOURCE);
				shoplocalGrid.setAdapter(adapter);
				shoplocalGrid.setOnItemClickListener(new ShopLocalGridItemClickListener());

				shopIconDone.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						shopLocalGallery.dismiss();
					}
				});
				shopIconCancel.setOnClickListener(new android.view.View.OnClickListener() {

					@Override
					public void onClick(View v) {
					
						shopLocalGallery.dismiss();
					}
				});
			}
			else
			{
				if(GALLERY_MESSAGE.startsWith(getResources().getString(R.string.invalidAuth)))
				{
					finishTo();
				}
				if(GALLERY_MESSAGE!=null)
				{
					//showToast(GALLERY_MESSAGE);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//GridAdapter 

	class ShopLocalGridAdapter extends BaseAdapter
	{
		ArrayList<String>photo;
		Activity context;
		LayoutInflater inflate;
		String image;

		public ShopLocalGridAdapter(Activity context,ArrayList<String>photo)
		{
			this.context=context;
			this.photo=photo;
			inflate=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
		
			return photo.size();
		}

		@Override
		public Object getItem(int position) {
		
			return photo.get(position);
		}

		@Override
		public long getItemId(int position) {
		
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
		
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.shoplocalgridicon,null);
				holder.shopLocalIcon=(ImageView)convertView.findViewById(R.id.shopLocalIcon);
				convertView.setTag(holder);
			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			if(photo.get(position)!=null || photo.get(position).length()!=0)
			{
				image=photo.get(position).replaceAll(" ", "%20");
				/*imageLoader.DisplayImage(PHOTO_URL+image,hold.shopLocalIcon);*/

				try{
					Picasso.with(context).load(PHOTO_URL+image)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.shopLocalIcon);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}


			return convertView;
		}

		class ViewHolder
		{
			ImageView shopLocalIcon;

		}

	}//Adapter ends here


	//Shoplocal Icon gallery.
	public class ShopLocalGridItemClickListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
			if(SHOPLOCAL_SOURCE.size()!=0)
			{
				SHOPLOCAL_ICON_URL=SHOPLOCAL_SOURCE.get(position);
				setShopLocalIcon(SHOPLOCAL_SOURCE.get(position));
				isShopLocalIcon=true;
				shopLocalGallery.dismiss();
			}


		}
	}

	void setShopLocalIcon(String url)
	{
		try
		{

			ImageView imageIcon=(ImageView)mPager.findViewById(R.id.imgManageStore_ThumbPreview);
			/*imageLoader.DisplayImage(PHOTO_URL+url, imageIcon);*/

			try{
				Picasso.with(context).load(PHOTO_URL+url)
				.placeholder(R.drawable.ic_place_holder)
				.error(R.drawable.ic_place_holder)
				.into(imageIcon);
			}
			catch(IllegalArgumentException illegalArg){
				illegalArg.printStackTrace();
			}
			catch(Exception e){ 
				e.printStackTrace();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	@Override
	public void onReceiveGalleryResult(int resultCode, Bundle resultData) {
		try
		{
			GridView imageGridMerchant;



			String status=resultData.getString("SEARCH_STATUS");
			String message=resultData.getString("MESSAGE");

			ArrayList<String>THUMB_URL=new ArrayList<String>();


			ID.clear();
			STORE_ID_.clear();
			SOURCE.clear();
			TITLE.clear();
			IMAGE_DATE.clear();
			THUMB_URL.clear();


			progUpload.setVisibility(View.GONE);

			if(status.equalsIgnoreCase("true"))
			{

				//ID=resultData.getStringArrayList("ID");
				ID=resultData.getStringArrayList("ID");
				STORE_ID_=resultData.getStringArrayList("STORE_ID");
				SOURCE=resultData.getStringArrayList("SOURCE");
				TITLE=resultData.getStringArrayList("TITLE");
				IMAGE_DATE=resultData.getStringArrayList("IMAGE_DATE");
				THUMB_URL=resultData.getStringArrayList("THUMB_URL");

				ID.add(0, "-1");
				TITLE.add(0, "Add Image");
				SOURCE.add(0, "");
				THUMB_URL.add(0, "");

				Log.d("SIZEOFID","SIZEOFID " + ID.size());

				adapterImg=new GridAdapter(context, THUMB_URL,ID,TITLE);
				imageGrid.setAdapter(adapterImg);

				//Toast.makeText(context, " " + imageGrid.getAdapter().getCount(), 0).show();



				//    imageGrid.setOnItemLongClickListener(new OnItemLongClickListener() {
				//
				//     @Override
				//     public boolean onItemLongClick(AdapterView arg0, View arg1,final int position, long arg3) {
				//      
				//
				//      //Toast.makeText(context, ""+position, 0).show();
				//
				//      if(position == 0){
				//
				//
				//      }
				//      else{
				//
				//
				//
				//       AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				//       alertDialogBuilder3.setTitle("Shoplocal");
				//       alertDialogBuilder3
				//       .setMessage("Are you sure you want to delete this image")
				//       .setCancelable(false)
				//       .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				//        public void onClick(DialogInterface dialog,int id) {
				//
				//         callDeleteImage(ID.get(position));
				//
				//        }
				//
				//
				//       })
				//       .setNegativeButton("No",new DialogInterface.OnClickListener() {
				//        public void onClick(DialogInterface dialog,int id) {
				//         // if this button is clicked, just close
				//         // the dialog box and do nothing
				//         dialog.cancel();
				//        }
				//       });;
				//
				//       AlertDialog alertDialog3 = alertDialogBuilder3.create();
				//
				//       alertDialog3.show();
				//
				//      }
				//
				//
				//      return false;
				//     }
				//    });


				try
				{
					if(manageGallery!=1){
						imageGridMerchant=(GridView)mPager.findViewById(R.id.imageGridMerchant);
						imageGridMerchant.setAdapter(adapterImg);
						imageGridMerchant.setOnItemClickListener(new GridItemClickListener(true));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}


				PHOTO_SOURCE=SOURCE;

				imageGrid.setOnItemClickListener(new GridItemClickListener(false));

				for(int i=0;i<ID.size();i++)
				{

					Log.i("======SOURCE", "SOURCE "+SOURCE.get(i));
				}


			}else
			{
				//Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

				//		    ID.clear();
				//		    STORE_ID_.clear();
				//		    SOURCE.clear();
				//		    TITLE.clear();
				//		    IMAGE_DATE.clear();

				//Set Grid Adapter with Add Photo 
				ID.add(0, "-1");
				TITLE.add(0, "Add Image");
				SOURCE.add(0, "");

				Log.d("SIZEOFID","SIZEOFID " + ID.size());

				GridAdapter adapter=new GridAdapter(context, SOURCE,ID,TITLE);
				imageGrid.setAdapter(adapter);
				try
				{
					imageGridMerchant=(GridView)mPager.findViewById(R.id.imageGridMerchant);
					imageGridMerchant.setAdapter(adapter);
					imageGridMerchant.setOnItemClickListener(new GridItemClickListener(true));
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}



				PHOTO_SOURCE=SOURCE;

				imageGrid.setOnItemClickListener(new GridItemClickListener(false));

				for(int i=0;i<ID.size();i++)
				{

					Log.i("======SOURCE", "SOURCE "+SOURCE.get(i));
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	@Override
	public void onReceivePhoto(int resultCode, Bundle resultData) {
	
		try
		{
			String status=resultData.getString("broadcast_image_status");
			String message=resultData.getString("broadcast_message");
			progUpload.setVisibility(View.GONE);
			if(status.equalsIgnoreCase("true"))
			{
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				loadGallery();
			}
			else if(status.equalsIgnoreCase("false"))
			{
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					session.logoutUser();
					/*finishTo();*/
					invalidAuthFinish();
				}

			}
			else if(status.equalsIgnoreCase("error"))
			{
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
	

		finishTo();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


	}


	/*
	 * (non-Javadoc)
	 * @see com.actionbarsherlock.app.SherlockFragmentActivity#onOptionsItemSelected(android.view.MenuItem)
	 */

	void invalidAuthFinish()
	{
		try
		{

			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					try
					{
						clearAllData();
						session.logoutUser();
						Intent intent=new Intent(context, MerchantTalkHome.class);
						intent.putExtra("isSplitLogin",true);
						intent.putExtra("USER_ID", USER_ID);
						intent.putExtra("AUTH_ID", AUTH_ID);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();

			/*showToast(getResources().getString(R.string.invalidAuth)+ " Try logging in again ");*/

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	@Override
	public void finish() {
	
		super.finish();
		try
		{
			clearAllData();

			Log.i("finish called", "finish called");

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void finishTo()
	{
		try
		{
			if(manageGallery!=1)
			{
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage("Are you sure you want to leave this screen?")
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
						try
						{
							clearAllData();
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
						Intent intent=new Intent(context, MerchantTalkHome.class);
						intent.putExtra("isSplitLogin",true);
						intent.putExtra("USER_ID", USER_ID);
						intent.putExtra("AUTH_ID", AUTH_ID);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				});
				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
			else
			{
				Intent intent=new Intent(context, MerchantTalkHome.class);
				intent.putExtra("isSplitLogin",true);
				intent.putExtra("USER_ID", USER_ID);
				intent.putExtra("AUTH_ID", AUTH_ID);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Intent intent=new Intent(context, MerchantTalkHome.class);
			intent.putExtra("isSplitLogin",true);
			intent.putExtra("USER_ID", USER_ID);
			intent.putExtra("AUTH_ID", AUTH_ID);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}

	}

	void clearAllData()
	{
		hour=0;
		min=0;
		store_landline="";
		store_landline_no2="";
		store_landline_no3="";

		store_mobileno="";
		store_mobile_no2="";
		store_mobile_no3="";

		store_faxno="";
		store_tollfree="";
		store_time_from="";
		store_time_to="";
		store_emailId="";
		store_web="";
		latitued="";
		longitude="";
		place_status="";

		//clear
		area_id="";
		PHOTO="";
		removeStoreLogo=0;

		showEditMode = false;

		ID.clear();
		IDSTODELETE.clear();

		SHOPLOCAL_ICON_URL="";
		SHOPLOCAL_SOURCE.clear();
		mCountryCode="";

		isNew=false;

		tempCatId.clear();

		PHOTO_URL="";

		FILE_PAGER_PATH="";
		FILE_PAGER_NAME="";
		FILE_PAGER_TYPE="";

		local_city.clear();
		local_pin.clear();
		local_area.clear();
		local_areaID.clear();

		STORE_CATEGORY_ID.clear();

		STORE_ID="";

		pager_byte=null;
		gallery_byte=null;
	}

	void finishOnDelete()
	{
		try
		{
			clearAllData();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		Intent intent=new Intent(context, MerchantTalkHome.class);
		intent.putExtra("isSplitLogin",true);
		intent.putExtra("USER_ID", USER_ID);
		intent.putExtra("AUTH_ID", AUTH_ID);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}
	/**************************************** Adapter *********************************************************/

	static class PlaceDropDown extends ArrayAdapter<String>
	{


		Activity context;
		LayoutInflater inflate;
		ArrayList<String>dropDownItem;
		ArrayList<String>store_id;

		public PlaceDropDown(Activity context, int resource,
				int textViewResourceId, ArrayList<String>dropDownItem,ArrayList<String>store_id) {
			super(context, resource, textViewResourceId, dropDownItem);

			this.context=context;
			this.dropDownItem=dropDownItem;
			this.store_id=store_id;
			inflate=context.getLayoutInflater();
		}


		@Override
		public int getCount() {
		
			return dropDownItem.size();
		}

		@Override
		public String getItem(int position) {
		
			return dropDownItem.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.parentstorelist, null);
				holder.txtParentItem=(TextView)convertView.findViewById(R.id.txtParentItem);
				convertView.setTag(holder);
			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtParentItem.setText(dropDownItem.get(position));
			return convertView;
		}

		static class ViewHolder
		{
			TextView txtParentItem;
		}

	}


	/*class CategoryDropDown extends ArrayAdapter<String> 
	{

		LayoutInflater inflator;
		ArrayList<String>list;
		ArrayList<String>list_id;
		ArrayList<String>checked_id;
		ArrayList<Integer>tempPos=new ArrayList<Integer>(); //Temporary array to store position of IDS
		ArrayList<Boolean>isChecked=new ArrayList<Boolean>();
		Activity context;

		public CategoryDropDown(Activity context, int resource,
				int textViewResourceId, ArrayList<String>list,ArrayList<String>list_id,ArrayList<String>checked_id) {
			super(context, resource, textViewResourceId, list);

			this.context=context;
			this.list=list;
			this.list_id=list_id;
			this.checked_id=checked_id;
			inflator=context.getLayoutInflater();




			//Searching for Position where ID's are matching and adding that position to tempPos Array.
			for(int i=0;i<checked_id.size();i++)
			{
				Log.i("TEMP CAT", "TEMP CAT ADAPTER: "+checked_id.get(i));
				for(int j=0;j<list_id.size();j++)
				{
					if(list_id.get(j).toString().equalsIgnoreCase(checked_id.get(i).toString()))
					{
						tempPos.add(j);
					}

				}
			}

			//making size equal of tempPos to checked_id.
			for(int i=0;i<checked_id.size()-STORE_CATEGORY_ID.size();i++)
			{
				tempPos.add(-1);
			}



				Log.i("SIZE", "SIZES : "+isChecked.length);

			//Sorting tempPos.
			Collections.sort(tempPos);

			//Creating Checked array based on position
			for(int i=0;i<tempPos.size();i++)
			{
				Log.i("TEMP POS", "TEMP POS "+tempPos.get(i));
				if(tempPos.get(i)!=-1)
				{
					//add true at position where id is not -1
					isChecked.add(tempPos.get(i), true);
				}
				else
				{
					isChecked.add(i, false);
				}


			}




		}




		@Override
		public int getCount() {
		
			return list.size();
		}



		@Override
		public long getItemId(int position) {
		
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
		


			convertView=inflator.inflate(R.layout.categorylistlayout, null);
			final CheckBox chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
			TextView txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);
			chkBox.setChecked(isChecked.get(position));
			chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				


					if(chkBox.isChecked())
					{
							Toast.makeText(context, "Checkbox checked"+chkBox.isChecked(), Toast.LENGTH_SHORT).show();
						tempCatName.add(list.get(position));
						tempCatId.add(list_id.get(position));
						tempCatName[position]=list.get(position);
						tempCatId[position]=list_id.get(position);
					}
					else
					{
						Toast.makeText(context, "Checkbox checked"+chkBox.isChecked(), Toast.LENGTH_SHORT).show();
						tempCatName.remove(list.get(position));
						tempCatId.remove(list_id.get(position));
						tempCatName[position]="";
						tempCatId[position]="";
					}
				}
			});

			txtCategory.setText(list.get(position));

			return convertView;
		}
	}
	 */

	//New Category Adapter

	class CategoryDropDownNew extends ArrayAdapter<String> 
	{

		LayoutInflater inflator;
		ArrayList<String>cat_name;
		ArrayList<String>cat_id;

		Activity context;

		public CategoryDropDownNew(Activity context, ArrayList<String>cat_name,ArrayList<String>cat_id) {
			super(context, R.layout.categorylistlayout, cat_name);

			this.context=context;
			this.cat_name=cat_name;
			this.cat_id=cat_id;
			inflator=context.getLayoutInflater();
			//			InorbitLog.d("onAdapter");
			for(int i=0;i<cat_name.size();i++){
				//				InorbitLog.d(cat_name.get(i)+" --- " +cat_id.get(i));

				Log.i("CATEGORY","CATEGORY "+cat_name.get(i)+" CATEGORY ID " +cat_id.get(i));
			}

		}




		@Override
		public int getCount() {
		
			return cat_name.size();
		}



		@Override
		public long getItemId(int position) {
		
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
		
			View view=null;
			if(convertView==null)
			{
				final ViewHolder viewHolder=new ViewHolder();
				inflator=context.getLayoutInflater();
				convertView=inflator.inflate(R.layout.edit_categorylistlayout, null);
				viewHolder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				viewHolder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);

				viewHolder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					

						if(isChecked){
							if(tempCatId.contains(cat_id)){

							}else{
								tempCatId.add(cat_id.get(position));
								tempCatName.add(cat_name.get(position));
							}
						}else{
							tempCatId.remove(cat_id.get(position));
							tempCatName.remove(cat_name.get(position));
						}

					}
				});

				convertView.setTag(viewHolder);

				//viewHolder.chkBox.setTag(list.get(position));
			}

			ViewHolder holder=(ViewHolder)convertView.getTag();


			//holder.chkBox.setChecked(list.get(position).isSelected());
			holder.txtCategory.setText(cat_name.get(position));
			//			InorbitLog.d(cat_name.get(position));

			Log.i("CATEGORY","CATEGORY "+cat_name.get(position));


			if(STORE_CATEGORY_ID.contains(cat_id.get(position))){
				holder.chkBox.setChecked(true);

			}else{
				holder.chkBox.setChecked(false);
			}

			return convertView;
		}
	}


	class CategoryDropDown extends ArrayAdapter<Model> 
	{

		LayoutInflater inflator;
		ArrayList<Model>list;
		ArrayList<Model>list_id;
		Activity context;

		public CategoryDropDown(Activity context, int resource,
				int textViewResourceId, ArrayList<Model>list,ArrayList<Model>list_id) {
			super(context, resource, textViewResourceId, list);

			this.context=context;
			this.list=list;
			this.list_id=list_id;
			inflator=context.getLayoutInflater();

		}




		@Override
		public int getCount() {
		
			return list.size();
		}



		@Override
		public long getItemId(int position) {
		
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
		
			View view=null;
			if(convertView==null)
			{
				final ViewHolder viewHolder=new ViewHolder();
				convertView=inflator.inflate(R.layout.edit_categorylistlayout, null);
				viewHolder.chkBox=(CheckBox)convertView.findViewById(R.id.chkBox);
				viewHolder.txtCategory=(TextView)convertView.findViewById(R.id.txtCategory);

				viewHolder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
						Model element=(Model)viewHolder.chkBox.getTag();
						element.setSelected(buttonView.isChecked());
						if(buttonView.isChecked())
						{

							tempCatName.add(list.get(position).getName());
							tempCatId.add(list_id.get(position).getId());
						}
						else
						{

							tempCatName.remove(list.get(position).getName());
							tempCatId.remove(list_id.get(position).getId());
						}
					}
				});

				convertView.setTag(viewHolder);
				viewHolder.chkBox.setTag(list.get(position));
			}
			else
			{
				view=convertView;
				((ViewHolder)convertView.getTag()).chkBox.setTag(list.get(position));
			}
			ViewHolder holder=(ViewHolder)convertView.getTag();
			holder.chkBox.setChecked(list.get(position).isSelected());
			holder.txtCategory.setText(list.get(position).getName());

			return convertView;
		}
	}

	static class ViewHolder
	{
		TextView txtCategory;
		CheckBox chkBox;
	}




	static class GridAdapter extends BaseAdapter
	{
		ArrayList<String>photo,title,id;
		Activity context;
		LayoutInflater inflate;
		String image;

		public GridAdapter(Activity context,ArrayList<String>photo,ArrayList<String>id,ArrayList<String>title)
		{
			this.context=context;
			this.photo=photo;
			this.title=title;
			this.id=id;
			inflate=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
		
			return photo.size();
		}

		@Override
		public Object getItem(int position) {
		
			return photo.get(position);
		}

		@Override
		public long getItemId(int position) {
		
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {

			final int pos = position;
			ViewHolder holder;

			if(convertView==null)
			{

				Log.d("INIF", "INIF");
				holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.gridimagelayout,null);

				holder.photo=(ImageView)convertView.findViewById(R.id.imgGridImage);
				holder.txtImageTitle=(TextView)convertView.findViewById(R.id.txtImageTitle);
				holder.toChkDelete=(CheckBox)convertView.findViewById(R.id.toChkDelete);

				if(showEditMode){

					holder.toChkDelete.setVisibility(View.VISIBLE);
				}
				//				else{
				//					
				//					holder.toChkDelete.setVisibility(View.INVISIBLE);
				//				}

				//logic to check selected images

				holder.toChkDelete.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					

						if(buttonView.isChecked()){

							IDSTODELETE.add(ID.get(pos));
						}
						else{

							IDSTODELETE.remove(ID.get(pos));
						}

					}


				});

				convertView.setTag(holder);
			}
			else{
				Log.d("INIF", "INELSE");	
			}

			holder=(ViewHolder)convertView.getTag();
			if(photo.get(position)!=null || photo.get(position).length()!=0)
			{
				image=photo.get(position).replaceAll(" ", "%20");
				/*imageLoader.DisplayImage(PHOTO_URL+image,hold.photo);*/
				try{
					if(position!=0){
						Picasso.with(context).load(PHOTO_URL+image)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(holder.photo);
					}
					else{
						holder.photo.setImageResource(R.drawable.photo);
					}

				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			if(position==0)
			{
				holder.photo.setImageResource(R.drawable.photo);
				holder.toChkDelete.setVisibility(View.INVISIBLE);
				/*hold.photo.setOnClickListener(new OnClickListener() {
			if(position==0)
			{
				/*hold.photo.setBackgroundResource(R.drawable.photo);*/
				//hold.toChkDelete.setVisibility(View.INVISIBLE);
				/*hold.photo.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
					

					}
				});*/
			}
			//	hold.txtImageTitle.setText(title.get(position).toString());
			holder.txtImageTitle.setVisibility(View.GONE);

			return convertView;
		}

		static class ViewHolder
		{
			ImageView photo;
			TextView txtImageTitle;
			CheckBox toChkDelete;
		}

	}



	//Open camera to take picture.
	public  void takePicture(Context context)
	{
		try
		{
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
			startActivityForResult(intent, ACTION_TAKE_PHOTO_B);
			/*			Toast.makeText(context, "Click Pic from Camera", Toast.LENGTH_SHORT).show();*/
		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Open gallery to choose photo to upload.
	public  void choosePict(Context context)
	{
		try
		{
			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, SELECT_PHOTO);
		}
		catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		/*			Toast.makeText(context, "Choose From Gallery", Toast.LENGTH_SHORT).show();*/
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		try
		{
			boolean deleted = mFileTemp.delete();
			Log.i("deleted ", "deleted "+mFileTemp.getAbsolutePath().toString()+mFileTemp.getName()+" "+deleted);
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}

		try
		{
			Log.i("RESULT CODE","REQUEST CODE"+requestCode+" Result Code"+resultCode);

			//Image For gallery grid
			if(requestCode==REQUEST_CODE_TAKE_PICTURE && resultCode == RESULT_OK)
			{
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";


				Bitmap bitmap = null;
				CameraImageSave cameraImageSave = new CameraImageSave();

				try{
					bitmap = cameraImageSave.getBitmapFromFile(480);

					if(bitmap != null){

						ByteArrayOutputStream stream = new ByteArrayOutputStream();

						cameraImageSave.deleteFromFile();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						gallery_byte = stream.toByteArray();

						cameraImageSave.deleteFromFile();
						cameraImageSave = null;
						bitmap = null;

						setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0, gallery_byte.length));
					}
				}
				catch(Exception e){
					Log.e("Exception","EditStore.java -> onActivityForResult() -> REQ_CODE = REQUEST_CODE_TAKE_PICTURE");
					e.printStackTrace();
				}
			}

			//logo
			if(requestCode == REQ_CODE_GETCLICKEDIMAGE && resultCode == RESULT_OK){
				try{

					CameraImageSave cameraImageSave = new CameraImageSave();

					Bitmap bitmap = cameraImageSave.getBitmapFromFile(150);


					if(bitmap != null){
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						pager_byte = stream.toByteArray();
						cameraImageSave.deleteFromFile();
						bitmap = null;
						cameraImageSave = null;
					}

					setPhoto(BitmapFactory.decodeByteArray(pager_byte , 0, pager_byte.length), true);

				}
				catch(Exception e){ }
			}


			//A - RECEIVE IMAGE FROM GALLERY for LOGO
			if(requestCode == REQ_CODE_LOGO_SELECTGALLERY){
				if(resultCode == RESULT_OK){
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(EditStore.this, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQ_CODE_LOGO_SELECTGALLERYCROP);
				}
			}

			if(requestCode == REQ_CODE_LOGO_SELECTGALLERYCROP){
				if(resultCode == RESULT_OK){
					String filePath = Environment.getExternalStorageDirectory()
							+ File.separator + "temp_photo_crop.jpg";
					try{
						FileInputStream in = new FileInputStream(filePath);
						BufferedInputStream buf = new BufferedInputStream(in);
						Bitmap bitmap = BitmapFactory.decodeStream(buf);

						bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, true);

						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
						pager_byte = stream.toByteArray();
						bitmap = null;

						setPhoto(BitmapFactory.decodeByteArray(pager_byte , 0,pager_byte.length),true);
					}
					catch(Exception e){}
				}
			}

			//A - RECEIVE IMAGE FROM GALLERY for GALLERY
			if(requestCode == REQ_CODE_GALLERY_SELECTGALLERY){
				if(resultCode == RESULT_OK){
					Uri imageUri=data.getData();
					String[] filePathColumn={MediaStore.Images.Media.DATA};

					Cursor c=getContentResolver().query(imageUri, filePathColumn, null, null, null);
					c.moveToFirst();

					int columnIndex=c.getColumnIndex(filePathColumn[0]);
					String picturePath=c.getString(columnIndex);

					c.close();

					Intent intent=new Intent(EditStore.this, ZoomCroppingActivity.class);
					intent.putExtra("imagepath", picturePath);
					intent.putExtra("comingfrom", "gallery");
					startActivityForResult(intent, REQ_CODE_GAlLERY_SELECTGALLERYCROP);
				}
			}

			if(requestCode == REQ_CODE_GAlLERY_SELECTGALLERYCROP){
				if(resultCode == RESULT_OK){

					FILE_TYPE="image/jpeg";
					FILE_NAME="temp_photo.jpg";
					FILE_PATH="/sdcard/temp_photo.jpg";
					try{
						CameraImageSave cameraImageSave = new CameraImageSave();
						String filePath = cameraImageSave.getImagePath();


						Bitmap bitmap = cameraImageSave.getBitmapFromFile(480);

						if(bitmap != null){
							Uri uri = Uri.fromFile(new File(filePath));

							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
							gallery_byte = stream.toByteArray();
							bitmap = null;
							cameraImageSave = null;
						}
					}
					catch(Exception e) {
						Log.e("Exception","EditStore.java -> onActivityForResult() -> RequestCode = REQ_CODE_GAlLERY_SELECTGALLERYCROP");
						e.printStackTrace();
					}

					setPhoto(BitmapFactory.decodeByteArray(gallery_byte , 0, gallery_byte.length));
				}
			}

		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}


	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}


	//Convert image into byte array
	static 	byte[] imageTobyteArray(String path,int width,int height)
	{	byte[] b = null ;
	if(!path.equals("") || path.length()!=0)
	{


		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inDither=true;//optional
		options.inPreferredConfig=Bitmap.Config.RGB_565;//optional

		Bitmap bm = BitmapFactory.decodeFile(path,options);

		options.inSampleSize = calculateInSampleSize(options, width, height);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		bm=BitmapFactory.decodeFile(path,options);


		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object   
		b= baos.toByteArray();
	}
	return b;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		/*if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}*/

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	//Get File Name of Image File
	public static String getFileNameByUri(Uri uri)
	{
		String fileName="unknown";//default fileName
		Uri filePathUri = uri;
		if (uri.getScheme().toString().compareTo("content")==0)
		{      
			Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
			if (cursor.moveToFirst())
			{
				int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
				filePathUri = Uri.parse(cursor.getString(column_index));
				fileName = filePathUri.getLastPathSegment().toString();
			}
		}
		else if (uri.getScheme().compareTo("file")==0)
		{
			fileName = filePathUri.getLastPathSegment().toString();
		}
		else
		{
			fileName = fileName+"_"+filePathUri.getLastPathSegment().toString();
		}
		return fileName;
	}


	void setPhoto(Bitmap bitmap)
	{

		/*ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		user_file= stream.toByteArray();*/

		/*imgThumbPreview.setImageBitmap(bitmap);
		imgThumbPreview.setScaleType(ScaleType.FIT_CENTER);*/
		try
		{
			gallery_image_title.show();

			addGalleryImage.setImageBitmap(bitmap);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		//Posting image for gallery.
		try
		{
			gallery_image_title.setCancelable(false);
			gallery_image_title.setTitle("Set Title for image");
			Button galleryImageDone=(Button)gallery_image_title.findViewById(R.id.galleryImageDone);
			Button galleryImageCancel=(Button)gallery_image_title.findViewById(R.id.galleryImageCancel);
			final EditText galleryImageTitle=(EditText)gallery_image_title.findViewById(R.id.galleryImageTitle);

			galleryImageDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View view) {
				
					imageTitle=galleryImageTitle.getText().toString();
					galleryImageTitle.setText("");
					postGalleryData();
					gallery_image_title.dismiss();
				}
			});
			galleryImageCancel.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View view) {
				
					galleryImageTitle.setText("");
					imageTitle="";
					postGalleryData();
					gallery_image_title.dismiss();
				}
			});


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void setPhoto(Bitmap bitmap,boolean isPager)
	{

		/*ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		user_file= stream.toByteArray();*/

		/*imgThumbPreview.setImageBitmap(bitmap);
		imgThumbPreview.setScaleType(ScaleType.FIT_CENTER);*/
		try
		{
			removeStoreLogo=0;
			ImageView imgThumb=(ImageView)mPager.findViewById(R.id.imgManageStore_ThumbPreview);
			imgThumb.setImageBitmap(bitmap);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Image path for selected image from gallery.
	void setImagePath(Uri uri)
	{

		FILE_PATH=getRealPathFromURI(uri);
		Log.i("Image  : ", "Image Path Choosen : "+getRealPathFromURI(uri));

	}
	void setImagePath(String imagePath)
	{
		FILE_PATH=imagePath;
		Log.i("Image  : ", "Image Path Captured : "+imagePath);
	}


	//Image name for selected image from gallery.
	void setImageName(String imageName)
	{
		FILE_NAME=imageName;
		Log.i("Image  : ", "Image Name : "+imageName);
	}


	//Get absolute path of of Image File
	private String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}


	/**
	 * Get the uri of the captured file
	 * @return A Uri which path is the path of an image file, stored on the dcim folder
	 */
	private  Uri getImageUri() {


		DateFormat  dt=new SimpleDateFormat("d MMM yyyy");
		DateFormat  dt2=new SimpleDateFormat("hh:mm a");
		Date date=new Date();
		String CAPTURE_TITLE=dt.format(date).toString()+" "+dt2.format(date).toString()+".jpeg";
		/*			String CAPTURE_TITLE="hi.jpg";*/
		File file = new File(Environment.getExternalStorageDirectory() + "/.shoplocalPics", CAPTURE_TITLE);
		Uri imgUri = Uri.fromFile(file);
		Log.i("Captured", "File Path: "+file.getAbsolutePath()+" Name : "+file.getName());
		/*		txtImagePath.setText(file.getAbsolutePath());
				txtImageName.setText(CAPTURE_TITLE);*/
		FILE_PATH=file.getAbsolutePath();
		FILE_TYPE="image/jpeg";
		FILE_NAME=CAPTURE_TITLE;

		return imgUri;
	}

	//Down scaling bitmap to get Thumbnai
	public static Bitmap getThumbnail(Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;//optional
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;//optional
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional


		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);

		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}

	public static class GridItemClickListener implements OnItemClickListener
	{
		boolean isPagerGallery;
		public GridItemClickListener(boolean isPagerGallery)
		{
			this.isPagerGallery=isPagerGallery;
			Log.i("isPagerGallery", "isPagerGallery "+isPagerGallery);
		}
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		
			if(position==0)
			{
				if(!isNew)
				{
					if(!progUpload.isShown())
					{
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Shoplocal");
						alertDialogBuilder.setMessage("Photo");
						//null should be your on click listener
						alertDialogBuilder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
							
								if(isPagerGallery)
								{
									eventMap.put("source", "Camera");
									EventTracker.logEvent("Store_ImageAdd",eventMap );
								}
								else
								{
									eventMap.put("source", "Camera");
									EventTracker.logEvent("StoreGallery_ImageAdd",eventMap );
								}
								/** ANirudh gallery change */
								takePictureForGALLERY();
							}
						});
						alertDialogBuilder.setNegativeButton("Choose Existing", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								if(isPagerGallery)
								{
									eventMap.put("source", "Gallery");
									EventTracker.logEvent("Store_ImageAdd",eventMap );	
								}
								else
								{
									eventMap.put("source", "Gallery");
									EventTracker.logEvent("StoreGallery_ImageAdd",eventMap );	
								}

								openGalleryForGrid();
							}
						});
						alertDialogBuilder.show();
					}
				}else
				{
					showToast(context.getResources().getString(R.string.storeCreateAlert));
					mPager.setCurrentItem(0);
				}
			}
			else
			{
				Intent intent=new Intent(context,StorePagerGallery.class);
				intent.putExtra("photo_source", PHOTO_SOURCE);
				intent.putExtra("position", position);
				context.startActivity(intent);
			}

			/*		Intent intent=new Intent(context,StorePagerGallery.class);
			PHOTO_SOURCE.add(0, "");
			intent.putExtra("photo_source", PHOTO_SOURCE);
			intent.putExtra("position", position);
			startActivity(intent);
			PHOTO_SOURCE.remove(0);*/
		}
	}




	@Override
	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
	
		progUpload.setVisibility(View.INVISIBLE);
		try
		{
			String store_status=resultData.getString("store_status");

			String ID;
			String REC_BUSINESS_ID;
			String REC_SOTRE_NAME;	
			String BUILDING;
			String STREET;
			String LANDMARK;
			String AREA;
			String AREA_ID;
			String PINCODE;
			String CITY;
			String STATE;
			String COUNTRY;
			String LATITUDE_PLACE;
			String LONGITUDE_PLACE;

			String TELNO1;
			String TELNO2;
			String TELNO3;

			String MOBILE_NO;
			String MOBNO2;
			String MOBNO3;


			String FAX1;
			String FAX2;
			String TOLLFREE1;	
			String TOLLFREE2;

			String DESCRIPTION;
			String EMAIL;
			String WEBSITE;
			String STATUS;


			if(store_status.equalsIgnoreCase("true"))
			{
				loadAllCategories();
				//			loadOwnerList();

				ArrayList<String> Time1_OPEN=resultData.getStringArrayList("TIME1_OPEN");
				ArrayList<String> Time1_CLOSE=resultData.getStringArrayList("TIME1_CLOSE");
				ArrayList<String> PLACE_STATUS=resultData.getStringArrayList("PLACE_STATUS");
				ArrayList<String> DAY=resultData.getStringArrayList("DAY");
				STATUS=resultData.getString("STATUS");

				ID=resultData.getString("ID");
				REC_BUSINESS_ID=resultData.getString("BUSINESS_ID");
				REC_SOTRE_NAME=resultData.getString("SOTRE_NAME");
				STORE_PARENT_ID=resultData.getString("STORE_PARENT_ID");
				STORE_CATEGORY_ID=resultData.getStringArrayList("CATEGORY_ID");


				BUILDING=resultData.getString("BUILDING");
				STREET=resultData.getString("STREET");
				LANDMARK=resultData.getString("LANDMARK");
				AREA=resultData.getString("AREA");
				AREA_ID=resultData.getString("AREA_ID");
				PINCODE=resultData.getString("PINCODE");
				CITY=resultData.getString("CITY");
				STATE=resultData.getString("STATE");
				COUNTRY=resultData.getString("COUNTRY");
				LATITUDE_PLACE=resultData.getString("LATITUDE");
				LONGITUDE_PLACE=resultData.getString("LONGITUDE");
				TELNO1=resultData.getString("TELNO1");

				if(LATITUDE_PLACE.equalsIgnoreCase("0"))
				{
					latitued="";
				}
				else if(LONGITUDE_PLACE.equalsIgnoreCase("0"))
				{
					longitude="";
				}
				else
				{
					latitued=LATITUDE_PLACE;
					longitude=LONGITUDE_PLACE;
				}

				TELNO2=resultData.getString("TELNO2");
				TELNO3=resultData.getString("TELNO3");



				MOBILE_NO=resultData.getString("MOBILE_NO");
				MOBNO2=resultData.getString("MOBNO2");
				MOBNO3=resultData.getString("MOBNO3");

				FAX1=resultData.getString("FAX1");
				FAX2=resultData.getString("FAX2");
				TOLLFREE1=resultData.getString("TOLLFREE1");
				TOLLFREE2=resultData.getString("TOLLFREE2");
				PHOTO=resultData.getString("PHOTO");
				DESCRIPTION=resultData.getString("DESCRIPTION");
				EMAIL=resultData.getString("EMAIL");
				WEBSITE=resultData.getString("WEBSITE");
				String FACEBOOK=resultData.getString("FACEBOOK");
				String TWITTER=resultData.getString("TWITTER");

				if(!LATITUDE_PLACE.equalsIgnoreCase("0") || !LONGITUDE_PLACE.equalsIgnoreCase("0"))
				{
					setImage(latitued, longitude);
				}

				area_id=AREA_ID;

				setDataToPager(ID,REC_BUSINESS_ID,REC_SOTRE_NAME,DESCRIPTION,BUILDING,STREET,LANDMARK,AREA
						,PINCODE,CITY,STATE,COUNTRY,LATITUDE_PLACE,LONGITUDE_PLACE,TELNO1,TELNO2,TELNO3,MOBILE_NO,MOBNO2,MOBNO3,FAX1,FAX2,
						TOLLFREE1,TOLLFREE2,PHOTO,EMAIL,WEBSITE,FACEBOOK,TWITTER);

				setOperationData(Time1_OPEN,Time1_CLOSE,DAY,PLACE_STATUS);

				place_status=STATUS;
				//showToast("Place Status On Receive : "+place_status);
				invalidateActionBar();


			}
			else if(store_status.equalsIgnoreCase("false"))
			{

				String store_msg=resultData.getString("MSG");
				Log.i("Socket", "Socket "+store_msg);
				progUpload.setVisibility(View.INVISIBLE);
				showToast(store_msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					session.logoutUser();
					/*finishTo();*/
					invalidAuthFinish();
				}
				//			if(store_msg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				session.logoutUser();
				//				finishTo();
				//			}
			}
			else if(store_status.equalsIgnoreCase("error"))
			{
				String store_msg=resultData.getString("MSG");
				showToast(store_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	static void showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	//Store details
	/*@Override
	public void onReceiveStoreDetail(int resultCode, Bundle resultData) {
	
		progStoreUpdate.setVisibility(View.INVISIBLE);
		String store_status=resultData.getString("store_status");
		String ID;
		String REC_BUSINESS_ID;
		String REC_SOTRE_NAME;	
		String BUILDING;
		String STREET;
		String LANDMARK;
		String AREA;
		String PINCODE;
		String CITY;
		String STATE;
		String COUNTRY;
		String LATITUDE;
		String LONGITUDE;
		String TELNO1;
		String TELNO2;
		String MOBILE_NO;
		String MOBNO2;
		String FAX1;
		String FAX2;
		String TOLLFREE1;	
		String TOLLFREE2;
		String PHOTO;
		String DESCRIPTION;
		String EMAIL;
		String WEBSITE;



		if(store_status.equalsIgnoreCase("set"))
		{
			loadAllCategories();
			loadOwnerList();

			ID=resultData.getString("ID");
			REC_BUSINESS_ID=resultData.getString("BUSINESS_ID");
			REC_SOTRE_NAME=resultData.getString("SOTRE_NAME");
			STORE_PARENT_ID=resultData.getString("STORE_PARENT_ID");
			STORE_CATEGORY_ID=resultData.getStringArrayList("CATEGORY_ID");


			BUILDING=resultData.getString("BUILDING");
			STREET=resultData.getString("STREET");
			LANDMARK=resultData.getString("LANDMARK");
			AREA=resultData.getString("AREA");

			PINCODE=resultData.getString("PINCODE");
			CITY=resultData.getString("CITY");
			STATE=resultData.getString("STATE");
			COUNTRY=resultData.getString("COUNTRY");
			LATITUDE=resultData.getString("LATITUDE");
			LONGITUDE=resultData.getString("LONGITUDE");
			TELNO1=resultData.getString("TELNO1");



			TELNO2=resultData.getString("TELNO2");
			MOBILE_NO=resultData.getString("MOBILE_NO");
			MOBNO2=resultData.getString("MOBNO2");
			FAX1=resultData.getString("FAX1");
			FAX2=resultData.getString("FAX2");
			TOLLFREE1=resultData.getString("TOLLFREE1");
			TOLLFREE2=resultData.getString("TOLLFREE2");
			PHOTO=resultData.getString("PHOTO");
			DESCRIPTION=resultData.getString("DESCRIPTION");
			EMAIL=resultData.getString("EMAIL");
			WEBSITE=resultData.getString("WEBSITE");


			setDataToPager(ID,REC_BUSINESS_ID,REC_SOTRE_NAME,DESCRIPTION,BUILDING,STREET,LANDMARK,AREA
					,PINCODE,CITY,STATE,COUNTRY,LATITUDE,LONGITUDE,TELNO1,TELNO2,MOBILE_NO,MOBNO2,FAX1,FAX2,
					TOLLFREE1,TOLLFREE2,PHOTO,EMAIL,WEBSITE);

		}

	}*/



	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.GetPerticularStoreOperationReceiver.StoreOperation#onReceiveStoreOperation(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveStoreOperation(int resultCode, Bundle resultData) {
	
		progUpload.setVisibility(View.INVISIBLE);
		String operation_Status=resultData.getString("operation_Status");
		ArrayList<String> Time1_OPEN=resultData.getStringArrayList("TIME1_OPEN");
		ArrayList<String> Time1_CLOSE=resultData.getStringArrayList("TIME1_CLOSE");
		ArrayList<String> PLACE_STATUS=resultData.getStringArrayList("PLACE_STATUS");
		ArrayList<String> DAY=resultData.getStringArrayList("DAY");
		if(operation_Status.equalsIgnoreCase("set"))
		{
			setOperationData(Time1_OPEN,Time1_CLOSE,DAY,PLACE_STATUS);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.GetPlaceCategoryReceiver.GetCategory#getStoreCategories(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveStoreCategories(int resultCode, Bundle resultData) {
	
		try
		{
			progUpload.setVisibility(View.GONE);
			String category_Status=resultData.getString("category_status");
			ArrayList<String>CATEGORY_NAME=new ArrayList<String>();
			ArrayList<String>CATEGORY_ID=new ArrayList<String>();
			if(category_Status.equalsIgnoreCase("true"))
			{
				CATEGORY_NAME=resultData.getStringArrayList("CATEGORY_NAME");
				CATEGORY_ID=resultData.getStringArrayList("CATEGORY_ID");

				//			if(!isNew)
				//			{
				//				setCategoryDropDown(CATEGORY_NAME,CATEGORY_ID);
				//			}
				//			else
				//			{
				//				setNewCategoryDropDown(CATEGORY_NAME,CATEGORY_ID);
				//			}
				setAllCategoryDropDown(CATEGORY_NAME, CATEGORY_ID);
			}
			else if(category_Status.equalsIgnoreCase("false"))
			{
				String category_msg=resultData.getString("category_msg");
				showToast(category_msg);
			}
			else if(category_Status.equalsIgnoreCase("error"))
			{
				String category_msg=resultData.getString("category_msg");
				showToast(category_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.StoreDropDownReceiver.StoreDropDown#storeDropDown(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveStoreDropDown(int resultCode, Bundle resultData) {
	
		progUpload.setVisibility(View.GONE);
		ArrayList<String> STORE_ID=new ArrayList<String>();
		ArrayList<String> STORE_NAME=new ArrayList<String>();
		String dropDown_Status=resultData.getString("dropdown_status");
		if(dropDown_Status.equalsIgnoreCase("true"))
		{
			STORE_ID=resultData.getStringArrayList("STORE_ID");
			STORE_NAME=resultData.getStringArrayList("STORE_NAME");

			setOwnerDropDown(STORE_NAME,STORE_ID);
		}
		else if(dropDown_Status.equalsIgnoreCase("false"))
		{
			String dropdown_msg=resultData.getString("dropdown_msg");
			showToast(dropdown_msg);
			if(dropdown_msg.startsWith(getResources().getString(R.string.invalidAuth)))
			{
				session.logoutUser();
				finishTo();
			}
		}
	}


	void setOwnerDropDown(final ArrayList<String>dropDownItem,final ArrayList<String>STORE_ID)
	{
		try
		{
			View page=(View)mPager.getChildAt(1);
			final TextView ownerDropDown=(TextView)page.findViewById(R.id.txtManageParent);

			/*PlaceDropDown adapter=new PlaceDropDown(context, R.drawable.ic_launcher, R.drawable.ic_launcher, dropDownItem);

		ownerDropDown.setAdapter(adapter);*/

			ListView listOwner=(ListView)parentStoreDialog.findViewById(R.id.parentDilogList);


			//Adding default None 
			dropDownItem.add(0,"None");
			STORE_ID.add(0,"0");

			PlaceDropDown adapter=new PlaceDropDown(context, R.drawable.ic_launcher,R.drawable.ic_launcher, dropDownItem,STORE_ID);

			listOwner.setAdapter(adapter);

			parentStoreDialog.setTitle("Select Parent Store");
			ownerDropDown.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				
					parentStoreDialog.show();		
				}
			});


			String [] array = new String[dropDownItem.size()];

			array=dropDownItem.toArray(array);

			listOwner.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
				
					ownerDropDown.setText(dropDownItem.get(position)); //owner name
					OWNER_ID=STORE_ID.get(position); //Owner id
					parentStoreDialog.dismiss();
				}
			});

			for(int i=0;i<dropDownItem.size();i++)
			{
				array[i]=dropDownItem.get(i);
				if(STORE_ID.get(i).equalsIgnoreCase(STORE_PARENT_ID))
				{
					ownerDropDown.setText(dropDownItem.get(i));
				}
				else
				{
					ownerDropDown.setText("None");
				}

				Log.i("PARENT","PARENT "+dropDownItem.get(i));
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		/*		ArrayAdapter adapter = ArrayAdapter.createFromResource(            
				context, array, android.R.layout.simple_spinner_item);
		 */
		/*ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, array);

		ownerDropDown.setAdapter(spinnerArrayAdapter);

		ownerDropDown.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
			
				Toast.makeText(context, "Selected : "+dropDownItem.get(position), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			

			}
		});*/
	}

	//New set Category
	void setAllCategoryDropDown(final ArrayList<String> ALL_CATEGORY_NAME,final ArrayList<String> ALL_CATEGORY_ID){
		try{

			View page	=	(View)mPager.getChildAt(1);
			final TextView categDropDown	=	(TextView)mPager.findViewById(R.id.txtManageCategory);

			String categ = "";

			Log.i("Inside New Categ","onDropDown");
			for(int i=0;i<ALL_CATEGORY_NAME.size();i++){
				Log.i("Inside New Categ","onDropDown -"+ALL_CATEGORY_NAME.get(i)+" +++ " +ALL_CATEGORY_ID.get(i));
			}

			final ListView listCateg=(ListView)categoryDialog.findViewById(R.id.categDilogList);
			listCateg.setAdapter(new CategoryDropDownNew(context,ALL_CATEGORY_NAME, ALL_CATEGORY_ID));

			categoryDialog.setTitle("Select Category");

			//Setting Selected Categories
			ArrayList<String> names = new ArrayList<String>();

			for(int i=0;i<ALL_CATEGORY_ID.size();i++){
				if(STORE_CATEGORY_ID.contains(ALL_CATEGORY_ID.get(i))){
					names.add(ALL_CATEGORY_NAME.get(i));
				}else{

				}
			}

			categ_list=names.toString();
			categ_list=categ_list.substring(1, categ_list.length()-1).trim();
			if(categ_list.equalsIgnoreCase("")){
				categ_list = "Select Category *";
			}
			categDropDown.setText(categ_list);

			categDropDown.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				
					categoryDialog.show();		
				}
			});



			Button btnDone=(Button)categoryDialog.findViewById(R.id.btnDoneSelection);
			btnDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				


					//Removing duplicates
					categ_list=tempCatName.toString();
					categ_list=categ_list.substring(1, categ_list.length()-1).trim();
					if(categ_list.equalsIgnoreCase("")){
						categ_list = "Select Category *";
					}
					categDropDown.setText(categ_list);
					categoryDialog.dismiss();

					STORE_CATEGORY_ID=tempCatId;

					//					STORE_CATEGORY_ID.clear();
					//
					//					for(int i=0;i<tempCatId.size();i++)
					//					{
					//
					//
					//						STORE_CATEGORY_ID.add(tempCatId.get(i));
					//
					//					}
					//
					//					listCateg.setAdapter(new CategoryDropDownNew(context,ALL_CATEGORY_NAME, ALL_CATEGORY_ID));

				}
			});

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}



	void setCategoryDropDown(final ArrayList<String>dropDownItem,final ArrayList<String>CATEG_ID)
	{
		try
		{
			View page=(View)mPager.getChildAt(1);
			final TextView categDropDown=(TextView)page.findViewById(R.id.txtManageCategory);

			String categ = "";

			final ListView listCateg=(ListView)categoryDialog.findViewById(R.id.categDilogList);

			final ArrayList<Boolean> isChecked=new ArrayList<Boolean>();

			final ArrayList<Integer> tempPos=new ArrayList<Integer>();

			final ArrayList<String>setCattemp=new ArrayList<String>();

			//Adding Categories to temp ArrayList that store has. 
			setCattemp.addAll(STORE_CATEGORY_ID);

			//Making sizes equal of  tempCat and CATEG_ID i.e All Category id and temporary id array to pass it to list.
			for(int i=0;i<CATEG_ID.size()-STORE_CATEGORY_ID.size();i++)
			{
				setCattemp.add("-1");
			}

			/*
			 * Test
			 */

			//Searching for Position where ID's are matching and adding that position to tempPos Array.
			for(int i=0;i<setCattemp.size();i++)
			{
				Log.i("TEMP CAT", "TEMP CAT ADAPTER: "+setCattemp.get(i));
				for(int j=0;j<CATEG_ID.size();j++)
				{
					if(CATEG_ID.get(j).toString().equalsIgnoreCase(setCattemp.get(i).toString()))
					{
						tempPos.add(j);
					}

				}
			}

			//making size equal of tempPos to checked_id.
			for(int i=0;i<setCattemp.size()-STORE_CATEGORY_ID.size();i++)
			{
				tempPos.add(-1);
			}



			/*isChecked=new boolean[tempPos.size()];*/

			/*	Log.i("SIZE", "SIZES : "+isChecked.length);*/

			//Sorting tempPos.
			Collections.sort(tempPos);

			//Creating Checked array based on position
			for(int i=0;i<tempPos.size();i++)
			{
				Log.i("TEMP POS", "TEMP POS "+tempPos.get(i));
				if(tempPos.get(i)!=-1)
				{
					//add true at position where id is not -1
					isChecked.add(tempPos.get(i), true);
				}
				else
				{
					isChecked.add(i, false);
				}

			}




			ArrayList<Model> list=new ArrayList<Model>();
			ArrayList<Model> list_id=new ArrayList<Model>();



			for(int i=0;i<dropDownItem.size();i++)
			{
				list_id.add(getID(dropDownItem.get(i),CATEG_ID.get(i),isChecked.get(i)));
				list.add(get(dropDownItem.get(i),isChecked.get(i)));
				/*if(STORE_ID.get(i).toString().equalsIgnoreCase(CATEGORY_ID.get(i).toString()))
			{
				categ+=dropDownItem.get(i);
			}*/

			}

			Log.i("SIZE", "SIZE "+" CATEG NAME "+list.size()+" CATEG_ID SIZE "+list_id.size()+" isChecked size "+isChecked.size());

			//Searching inside of All Categories for Category that store has. 
			for(int i=0;i<STORE_CATEGORY_ID.size();i++)
			{
				for(int j=0;j<CATEG_ID.size();j++)
				{
					if(CATEG_ID.get(j).toString().equalsIgnoreCase(STORE_CATEGORY_ID.get(i).toString()))
					{
						categ+=dropDownItem.get(j)+",";
						/*tempCatName.add(dropDownItem.get(j));
					tempCatName[i]=dropDownItem.get(j);*/
					}


				}
			}

			for(int i=0;i<list.size();i++)
			{
				if(list.get(i).isSelected())
				{
					tempCatId.add(list_id.get(i).getId());
					tempCatName.add(list.get(i).getName());
				}
			}


			for(int i=0;i<tempCatName.size();i++)
			{
				Log.i("Temp", "Print selected cat "+tempCatName.get(i)+" id "+tempCatId.get(i));
			}



			categ=categ.substring(0, categ.length()-1).trim();

			categDropDown.setText(categ);


			CategoryDropDown adapter=new CategoryDropDown(context, R.drawable.ic_launcher,  R.drawable.ic_launcher, list, list_id);

			listCateg.setAdapter(adapter);

			categoryDialog.setTitle("Select Category");

			categDropDown.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				
					categoryDialog.show();		
				}
			});

			Button btnDone=(Button)categoryDialog.findViewById(R.id.btnDoneSelection);
			btnDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				


					//Removing duplicates
					HashSet<String> hs = new HashSet<String>();
					HashSet<String> hs2 = new HashSet<String>();

					hs.addAll(tempCatId);
					hs2.addAll(tempCatName);

					tempCatId.clear();
					tempCatId.addAll(hs);

					tempCatName.clear();
					tempCatName.addAll(hs2);

					for(int i=0;i<tempCatName.size();i++)
					{
						//		Toast.makeText(context, "Selected category : "+tempCatId.get(i)+" : "+tempCatName.get(i) , Toast.LENGTH_SHORT).show();
						/*		categDropDown.setText(text)*/

						/*categ_list+=tempCatName[i];*/

					}



					//Copying selected items to String
					categ_list=tempCatName.toString();
					//Setting selected categories to Textview

					categ_list=categ_list.substring(1, categ_list.length()-1).trim();

					categDropDown.setText(categ_list);

					Log.i("", "Categ list length"+categ_list.length());

					if(categ_list.length()==0)
					{
						categDropDown.setText("Click to select Category *");
					}

					categoryDialog.dismiss();

				}
			});

			//			if(categ_list.length()==0)
			//			{
			//				categDropDown.setText("Click to select Category *");
			//			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		/*	tempCatId=new String[CATEG_ID.size()];
		tempCatName=new String[CATEG_ID.size()];*/

		//Adding stores category ids to tempCatId.
		//tempCatId.addAll(STORE_CATEGORY_ID); temp catid

		/*for(int i=0;i<STORE_CATEGORY_ID.size();i++)
		{
			tempCatId[i]=STORE_CATEGORY_ID.get(i);
		}*/

		/*
		 * Commented purposly
		//Searching inside of All Categories for Category that store has. 
		for(int i=0;i<STORE_CATEGORY_ID.size();i++)
		{
			for(int j=0;j<CATEG_ID.size();j++)
			{
				if(CATEG_ID.get(j).toString().equalsIgnoreCase(STORE_CATEGORY_ID.get(i).toString()))
				{
					//categ+=dropDownItem.get(j)+" ";
					tempCatName.add(dropDownItem.get(j));
					tempCatName[i]=dropDownItem.get(j);
				}


			}
		}

		for(int i=0;i<tempCatName.size();i++)
		{
			categ+=tempCatName.get(i)+" ";
		}

		tempCatAdapterName.clear();
		tempCatAdapterName.addAll(tempCatName);

		tempCatAdapterId.clear();
		tempCatAdapterId.addAll(tempCatId);


		for(int i=0;i<tempCatId.size();i++)
		{
			Log.i("Temp Cat Id", "Temp Cat id inside "+tempCatId.get(i)+" CAT NAME "+tempCatName.get(i));
		}

		Log.i("SIZE", "SIZES : CATEG_ID "+CATEG_ID.size()+" STORE_CATEGORY_ID "+STORE_CATEGORY_ID.size()+" TEMP CATEG "+setCattemp.size());

		//Setting categories that store has.
		categDropDown.setText(categ);

		//Setting Adapter for category select.
		CategoryDropDown adapter=new CategoryDropDown(context, R.drawable.ic_launcher, R.drawable.ic_launcher, dropDownItem,CATEG_ID,setCattemp);

		listCateg.setAdapter(adapter);

		categoryDialog.setTitle("Select Category");

		categDropDown.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			
				categoryDialog.show();		
			}
		});



		Button btnDone=(Button)categoryDialog.findViewById(R.id.btnDoneSelection);
		btnDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			
				for(int i=0;i<tempCatName.size();i++)
				{
					Toast.makeText(context, "Selected category : "+tempCatId.get(i)+" : "+tempCatName.get(i) , Toast.LENGTH_SHORT).show();
									categDropDown.setText(text)

						categ_list+=tempCatName[i];

				}

				//Copying selected items to String
				categ_list=tempCatName.toString();



				//Setting selected categories to Textview
				categDropDown.setText(categ_list);




				setCattemp.clear();

				setCattemp.addAll(tempCatId);

				tempCatName.clear();
				tempCatId.clear();


				//Making sizes equal of  tempCat and CATEG_ID i.e All Category id and temporary id array to pass it to list.
				for(int i=0;i<CATEG_ID.size()-STORE_CATEGORY_ID.size();i++)
				{
					setCattemp.add("-1");
				}

				//Searching inside of All Categories for Category that store has. 
				for(int i=0;i<STORE_CATEGORY_ID.size();i++)
				{
					for(int j=0;j<CATEG_ID.size();j++)
					{
						if(CATEG_ID.get(j).toString().equalsIgnoreCase(setCattemp.get(i).toString()))
						{
							//categ+=dropDownItem.get(j)+" ";
							tempCatName.add(dropDownItem.get(j));
							tempCatName[i]=dropDownItem.get(j);
						}


					}
				}

				tempCatId.addAll(setCattemp);

				CategoryDropDown adapter=new CategoryDropDown(context, R.drawable.ic_launcher, R.drawable.ic_launcher, dropDownItem,CATEG_ID,setCattemp);
				listCateg.setAdapter(adapter);
			}
		});*/


		/*	CategoryDropDown adapter=new CategoryDropDown(context, R.drawable.ic_launcher, R.drawable.ic_launcher, dropDownItem);

		categDropDown.setAdapter(adapter);*/

		/*String [] array = new String[dropDownItem.size()];

		array=dropDownItem.toArray(array);
		for(int i=0;i<dropDownItem.size();i++)
		{
			array[i]=dropDownItem.get(i);
		}*/
		/*categDropDown.setItems(dropDownItem, "Spinner",this);*/

		/*		ArrayAdapter adapter = ArrayAdapter.createFromResource(            
				context, array, android.R.layout.simple_spinner_item);
		 */
		/*		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, array);

		categDropDown.setAdapter(spinnerArrayAdapter);*/

		/*categDropDown.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
			
				Toast.makeText(context, "Selected : "+dropDownItem.get(position), Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			

			}
		});*/
	}

	void setNewCategoryDropDown(final ArrayList<String>dropDownItem,final ArrayList<String>CATEG_ID)
	{
		try
		{
			View page=(View)mPager.getChildAt(0);
			final TextView categDropDown=(TextView)mPager.findViewById(R.id.txtManageCategory);

			String categ = "";

			final ListView listCateg=(ListView)categoryDialog.findViewById(R.id.categDilogList);

			ArrayList<Model> list=new ArrayList<Model>();
			ArrayList<Model> list_id=new ArrayList<Model>();

			for(int i=0;i<dropDownItem.size();i++)
			{
				list_id.add(getID(dropDownItem.get(i),CATEG_ID.get(i),false));
				list.add(get(dropDownItem.get(i),false));
				/*if(STORE_ID.get(i).toString().equalsIgnoreCase(CATEGORY_ID.get(i).toString()))
			{
				categ+=dropDownItem.get(i);
			}*/

			}

			CategoryDropDown adapter=new CategoryDropDown(context, R.drawable.ic_launcher,  R.drawable.ic_launcher, list, list_id);

			listCateg.setAdapter(adapter);

			categoryDialog.setTitle("Select Category");

			categDropDown.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				
					categoryDialog.show();		
				}
			});

			Button btnDone=(Button)categoryDialog.findViewById(R.id.btnDoneSelection);
			btnDone.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View v) {
				
					for(int i=0;i<tempCatName.size();i++)
					{
						//Toast.makeText(context, "Selected category : "+tempCatId.get(i)+" : "+tempCatName.get(i) , Toast.LENGTH_SHORT).show();
						/*		categDropDown.setText(text)*/

						/*categ_list+=tempCatName[i];*/

					}

					//Copying selected items to String
					categ_list=tempCatName.toString();
					//Setting selected categories to Textview

					categ_list=categ_list.substring(1, categ_list.length()-1).trim();

					categDropDown.setText(categ_list);

					if(categ_list.length()==0)
					{
						categDropDown.setText("Click to select Category *");
					}

					categoryDialog.dismiss();

				}
			});

			//			if(categ_list.length()==0)
			//			{
			//				categDropDown.setText("Click to select Category *");
			//			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*public class SpinnerItemSelectedListener implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
		
			Toast.makeText(context, "Selected : "+dropDownItem.get(position), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
		

		}

	}*/

	private Model get(String s,boolean selected) {
		return new Model(s,selected);
	}
	private Model getID(String name,String id,boolean selected) {
		Model m=new Model(name,selected);
		m.setId(id);
		return m;
	}

	void setOperationData(ArrayList<String> Time1_OPEN,ArrayList<String> Time1_Close,ArrayList<String> DAY,ArrayList<String> PLACE_STATUS)
	{
		try
		{
			View page3=(View)mPager.getChildAt(3);

			//time page.
			TextView txtMangeStoreTimeFrom=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeFrom);
			TextView txtMangeStoreTimeTo=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeTo);
			ToggleButton bManagetoggleSun=(ToggleButton)mPager.findViewById(R.id.bManagetoggleSun);
			ToggleButton bManagetoggleMon=(ToggleButton)mPager.findViewById(R.id.bManagetoggleMon);
			ToggleButton bManagetoggleTue=(ToggleButton)mPager.findViewById(R.id.bManagetoggleTue);
			ToggleButton bManagetoggleWed=(ToggleButton)mPager.findViewById(R.id.bManagetoggleWed);
			ToggleButton bManagetoggleThu=(ToggleButton)mPager.findViewById(R.id.bManagetoggleThu);
			ToggleButton bManagetoggleFri=(ToggleButton)mPager.findViewById(R.id.bManagetoggleFri);
			ToggleButton bManagetoggleSat=(ToggleButton)mPager.findViewById(R.id.bManagetoggleSat);


			for(int i=0;i<Time1_OPEN.size();i++)
			{
				/*txtMangeStoreTimeFrom.setText(Time1_OPEN.get(i));*/
				store_time_from=Time1_OPEN.get(i);

			}
			for(int i=0;i<Time1_Close.size();i++)
			{

				/*txtMangeStoreTimeTo.setText(Time1_Close.get(i));*/
				store_time_to=Time1_Close.get(i);
			}

			try
			{
				//Open Time
				String time = Time1_OPEN.get(0);

				DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
				Date date = sdf.parse(time);

				System.out.println("Time Converted open: " + sdf.format(date));

				DateFormat time_open=new SimpleDateFormat("hh:mm aa");
				System.out.println("Time Converted open: " + time_open.format(date));

				txtMangeStoreTimeFrom.setText(time_open.format(date));


				//Close Time
				String time2 = Time1_Close.get(0);

				DateFormat sdf2 = new SimpleDateFormat("hh:mm:ss");
				Date date2 = sdf.parse(time2);

				System.out.println("Time Converted Close: " + sdf2.format(date));

				DateFormat time_close=new SimpleDateFormat("hh:mm aa");
				System.out.println("Time Converted Close: " + time_close.format(date2));

				txtMangeStoreTimeTo.setText(time_close.format(date2));



			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			for(int i=0;i<DAY.size();i++)
			{
				Log.i("DAY","DAY : "+DAY.get(i)+" STATUS : "+PLACE_STATUS.get(i));
			}
			for(int i=0;i<DAY.size();i++)
			{
				if(DAY.get(i).equalsIgnoreCase("SUNDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleSun.setChecked(true);
					}
					else
					{
						bManagetoggleSun.setChecked(false);
					}

				}
				if(DAY.get(i).equalsIgnoreCase("MONDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleMon.setChecked(true);
					}
					else
					{
						bManagetoggleMon.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("TUESDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleTue.setChecked(true);
					}
					else
					{
						bManagetoggleTue.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("WEDNESDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleWed.setChecked(true);
					}
					else
					{
						bManagetoggleWed.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("THURSDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleThu.setChecked(true);
					}
					else
					{
						bManagetoggleThu.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("FRIDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleFri.setChecked(true);
					}
					else
					{
						bManagetoggleFri.setChecked(false);
					}
				}
				if(DAY.get(i).equalsIgnoreCase("SATURDAY"))
				{
					if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
					{
						bManagetoggleSat.setChecked(true);
					}
					else
					{
						bManagetoggleSat.setChecked(false);
					}
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		/*Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		int comma_counter=0;
		int close_counter=0;

		txtMangeStoreTimeFrom.setText(getTime(Time1_OPEN));
		txtMangeStoreTimeTo.setText(getTime(Time1_Close));

		//Setting status of store to open-close toggle
		char []ca=Time1_Close.toCharArray();

		for(int i=0;i<ca.length;i++)
		{
			if(ca[i]==',')
			{
				comma_counter++;
			}
			if(ca[i]=='c' || ca[i]=='C')
			{
				close_counter++;
				map.put(close_counter,comma_counter);
			}
		}
		for(int i=0;i<=6;i++)
		{
			//Rertriving values from Map
			for(Integer key:map.keySet())
			{
				Log.i("Closed", "Closed at "+map.get(key));
				if(i==Integer.parseInt(map.get(key).toString()))
				{
					Log.i("Toggle", "Toggled at i "+i);
					switch (i) {
					case 0:
						bManagetoggleSun.setChecked(false);
						Log.i("Toggle", "Toggled at 0");
						break;
					case 1:
						bManagetoggleMon.setChecked(false);
						Log.i("Toggle", "Toggled at 1");
						break;
					case 2:
						bManagetoggleTue.setChecked(false);
						Log.i("Toggle", "Toggled at 2");
						break;
					case 3:
						bManagetoggleWed.setChecked(false);
						Log.i("Toggle", "Toggled at 3");
						break;
					case 4:
						bManagetoggleThu.setChecked(false);
						Log.i("Toggle", "Toggled at 4");
						break;
					case 5:
						bManagetoggleFri.setChecked(false);
						Log.i("Toggle", "Toggled at 5");
						break;
					case 6:
						bManagetoggleSat.setChecked(false);
						Log.i("Toggle", "Toggled at 6");
						break;
					default:
						break;
					}
				}
			}
		}*/
	}

	String getTime(String time)
	{
		//Splitting string
		String [] splitstring;
		String time_status="";

		//Splitting string based on comma
		splitstring=time.split( ",\\s*");

		ArrayList<String>output=new ArrayList<String>();


		//Adding string to arraylist
		for(String string:splitstring)
		{
			output.add(string);
		}


		for(int i=0;i<output.size();i++)
		{
			if(!output.get(i).equalsIgnoreCase("closed"))
			{
				time_status=output.get(i);
			}
			Log.i("Closed","Closed seprated "+output.get(i));
		}

		Log.i("Closed","Opened at "+time_status);

		return time_status;

	}

	/************************************************* Image Capturing Section ***********************************************/

	//Take image from gallery for Grid
	private static void openGalleryForGrid() {

		//		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		//		photoPickerIntent.setType("image/*");
		//		context.startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);

		/*try {
			Intent intent = new Intent(context,PhotoActivity.class);
			intent.putExtra("REQUEST_CODE_GALLERY",REQUEST_CODE_GALLERY);
			context.startActivityForResult(intent, REQUEST_CODE_GALLERY);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}*/

		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		context.startActivityForResult(photoPickerIntent, REQ_CODE_GALLERY_SELECTGALLERY);
	}



	//Picture from gallery
	private static void takePictureForGALLERY() {

		try {
			try {
				Intent intent = new Intent(context,CameraView.class);
				intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
				context.startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}


	//Take picture from custom camera for Grid
	private static void takePictureForLogoFromCamera() {

		//		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		try {

			/*mImageCaptureUri = Uri.fromFile(mFileTemp);
				intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
				intent.putExtra("return-data", false);
				context.startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);*/

			try {
				/*Intent intent = new Intent(context,PhotoActivity.class);
				intent.putExtra("REQUEST_CODE_TAKE_PICTURE",REQUEST_CODE_TAKE_PICTURE);
				context.startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);*/
				Intent intent=new Intent(context, CameraView.class);
				context.startActivityForResult(intent, REQ_CODE_GETCLICKEDIMAGE);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}

	void setDataToPager(String id,String bsiness_id,String store_name,String desc,String building,String street,String landmark,
			String area,String pincode,String city,String state,String country,String lati,String logi,String telno1,String telno2,String telno3,
			String mobno1,String mobno2,String mobno3,String fax1,String fax2,String tollfree1,String tollfree2,String photo,String email,String web,String fb,String twitter)
	{

		try
		{
			//			View page1=(View)mPager.getChildAt(1);
			//			View page2=(View)mPager.getChildAt(2);
			//			View page3=(View)mPager.getChildAt(3);

			//location page
			EditText edtManageStoreName=(EditText)mPager.findViewById(R.id.edtManageStoreName);
			EditText edtManageDesc=(EditText)mPager.findViewById(R.id.edtManageDesc2);
			EditText edtManageStreet=(EditText)mPager.findViewById(R.id.edtManageStreet);
			EditText edtManageLandMark=(EditText)mPager.findViewById(R.id.edtManageLandMark);
			TextView edtManageArea=(TextView)mPager.findViewById(R.id.edtManageArea);
			TextView edtManageCity=(TextView)mPager.findViewById(R.id.edtManageCity);
			TextView edtManagePinCode=(TextView)mPager.findViewById(R.id.edtManagePinCode);

			edtManageStoreName.setText(store_name);
			edtManageDesc.setText(desc);
			edtManageStreet.setText(street);
			edtManageLandMark.setText(landmark);
			edtManageArea.setText(area);
			edtManageCity.setText(city);
			edtManagePinCode.setText(pincode);

			//contact page.
			TextView txtMangeContactStoreName=(TextView)mPager.findViewById(R.id.txtMangeContactStoreName);


			EditText edtManageLandline=(EditText)mPager.findViewById(R.id.edtManageLandline);
			EditText edtManageLandline2=(EditText)mPager.findViewById(R.id.edtManageLandline2);
			EditText edtManageLandline3=(EditText)mPager.findViewById(R.id.edtManageLandline3);


			EditText edtManageMobile=(EditText)mPager.findViewById(R.id.edtManageMobile);
			EditText edtManageMobile2=(EditText)mPager.findViewById(R.id.edtManageMobile2);
			EditText edtManageMobile3=(EditText)mPager.findViewById(R.id.edtManageMobile3);
			EditText edtManageFb=(EditText)mPager.findViewById(R.id.edtManageFb);
			EditText edtManageTwitter=(EditText)mPager.findViewById(R.id.edtManageTwitter);

			/*	EditText edtManageFax=(EditText)mPager.findViewById(R.id.edtManageFax);
			EditText edtManageTollFree=(EditText)mPager.findViewById(R.id.edtManageTollFree);*/
			EditText edtManageEmailId=(EditText)mPager.findViewById(R.id.edtManageEmailId);
			EditText edtManageWebSite=(EditText)mPager.findViewById(R.id.edtManageWebSite);


			TableRow landlinerow2=(TableRow)mPager.findViewById(R.id.landlinerow2);
			TableRow landlinerow3=(TableRow)mPager.findViewById(R.id.landlinerow3);


			TableRow mobileRow2=(TableRow)mPager.findViewById(R.id.mobileRow2);
			TableRow mobileRow3=(TableRow)mPager.findViewById(R.id.mobileRow3);


			edtManageFb.setText(fb);
			edtManageTwitter.setText(twitter);

			if(telno2.length()>= 10 )
			{
				landlinerow2.setVisibility(View.VISIBLE);
			}

			if(telno3.length()>= 10)
			{
				landlinerow3.setVisibility(View.VISIBLE);
			}

			if( mobno2.length()>= 10 )
			{
				mobileRow2.setVisibility(View.VISIBLE);
			}

			if(mobno3.length()>= 10)
			{
				mobileRow3.setVisibility(View.VISIBLE);
			}

			Button tadd1=(Button)mPager.findViewById(R.id.add1);
			Button tadd4=(Button)mPager.findViewById(R.id.add4);

			if(landlinerow2.isShown() && landlinerow3.isShown())
			{
				tadd1.setVisibility(View.GONE);
			}

			if(mobileRow3.isShown() && mobileRow3.isShown())
			{
				tadd4.setVisibility(View.GONE);
			}



			txtMangeContactStoreName.setText(store_name);

			/*edtManageLandline.setText(telno1);
			edtManageLandline2.setText(telno2);
			edtManageLandline3.setText(telno3);
			 */



			if(mobno1!=null && mobno1.length()>0)
			{
				mobno1=mobno1.substring(mCountryCode.length());
			}
			if(mobno2!=null && mobno2.length()>0)
			{
				mobno2=mobno2.substring(mCountryCode.length());
			}
			if(mobno3!=null && mobno3.length()>0)
			{
				mobno3=mobno3.substring(mCountryCode.length());
			}
			/*edtManageMobile.setText(mobno1);
			edtManageMobile2.setText(mobno2);
			edtManageMobile3.setText(mobno3);*/
			/*edtManageFax.setText(fax1);
			edtManageTollFree.setText(tollfree1);*/
			edtManageEmailId.setText(email);
			edtManageWebSite.setText(web);

			if(mobno1.length()==10)
			{
				edtManageMobile.setText(mobno1);
			}
			if(mobno2.length()==10)
			{
				edtManageMobile2.setText(mobno2);
			}
			if(mobno3.length()==10)
			{
				edtManageMobile3.setText(mobno3);
			}

			if(telno1.length()==11)
			{

				edtManageLandline.setText(telno1);


			}
			if(telno2.length()==11)
			{
				edtManageLandline2.setText(telno2);
			}
			if(telno3.length()==11)
			{
				edtManageLandline3.setText(telno3);
			}

			if(email.equalsIgnoreCase("null"))
			{
				edtManageEmailId.setText("");
			}
			if(web.equalsIgnoreCase("null"))
			{
				edtManageWebSite.setText("");
			}

			//time page.
			/*TextView txtMangeStoreTimeFrom=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeFrom);
			TextView txtMangeStoreTimeTo=(TextView)mPager.findViewById(R.id.txtMangeStoreTimeTo);*/
			ImageView imgManageStore_ThumbPreview=(ImageView)mPager.findViewById(R.id.imgManageStore_ThumbPreview);
			/*ToggleButton bManagetoggleSun=(ToggleButton)mPager.findViewById(R.id.bManagetoggleSun);
			ToggleButton bManagetoggleMon=(ToggleButton)mPager.findViewById(R.id.bManagetoggleMon);
			ToggleButton bManagetoggleTue=(ToggleButton)mPager.findViewById(R.id.bManagetoggleTue);
			ToggleButton bManagetoggleWed=(ToggleButton)mPager.findViewById(R.id.bManagetoggleWed);
			ToggleButton bManagetoggleThu=(ToggleButton)mPager.findViewById(R.id.bManagetoggleThu);
			ToggleButton bManagetoggleFri=(ToggleButton)mPager.findViewById(R.id.bManagetoggleFri);
			ToggleButton bManagetoggleSat=(ToggleButton)mPager.findViewById(R.id.bManagetoggleSat);*/

			imgManageStore_ThumbPreview.setScaleType(ScaleType.FIT_XY);
			Log.i("PHOTO IN", "PHOTO IN"+photo);
			photo=photo.replaceAll(" ", "%20");
			Log.i("PHOTO IN", "PHOTO IN After"+photo);

			String iphoto=PHOTO_URL+photo;
			if(photo.length()!=0)
			{
				Log.i("PHOTO IN", "PHOTO IN After inside"+photo);
				/*imageLoader.DisplayImage(iphoto, imgManageStore_ThumbPreview);*/
				try{
					Picasso.with(context).load(iphoto)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(imgManageStore_ThumbPreview);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}


			/*		EditText edtManageMobile=(EditText)page2.findViewById(R.id.edtManageMobile);
		EditText edtManageFax=(EditText)page2.findViewById(R.id.edtManageFax);
		EditText edtManageTollFree=(EditText)page2.findViewById(R.id.edtManageTollFree);
			 */
		}catch(IndexOutOfBoundsException ex)
		{
			ex.printStackTrace();	
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void getCountryCode()
	{
		SharedPreferences pref=context.getSharedPreferences("merchantCountryCode",0);
		mCountryCode=pref.getString("mCountryCode", "91");

		Log.i("Countrycode found","Countrycode found "+mCountryCode);
	}

	private void callDeleteImage(String UrlToAppend) {
	

		Intent intent = new Intent(context,DeleteGalleryImagesService.class);
		intent.putExtra("deleteImages", deleteImages);
		intent.putExtra("URL", DELETE_IMG_URL + Place_Id + "=" + STORE_ID + UrlToAppend);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);

		Log.d("WHOLEURL","WHOLEURL " + DELETE_IMG_URL + Place_Id + "=" + STORE_ID + UrlToAppend);

		context.startService(intent);
	}

	@Override
	public void onDeleteImage(int resultCode, Bundle resultData) {
	

		try
		{

			String status = resultData.getString("status");

			String message = resultData.getString("message");

			if(status.equalsIgnoreCase("true")){

				//showToast(message);

				//			count++;
				//
				//			if(!isNew)
				//			{
				//				if(count == IDSTODELETE.size()){
				//
				//					count = 0;
				IDSTODELETE.clear();
				URLTOAPPEND = "";
				loadGallery();
				//				}

				//}
			}
			else if(status.equalsIgnoreCase("false")){

				showToast(message);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					session.logoutUser();
					/*finishTo();*/
					invalidAuthFinish();
				}
			}
			else if(status.equalsIgnoreCase("error")){
				showToast(message);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}



	@Override
	public void onGetAreaReceive(int resultCode, Bundle resultData) {
	
		progStoreUpdate.setVisibility(View.GONE);
		try
		{

			String status = resultData.getString("area_status");


			if(status.equalsIgnoreCase("error"))
			{
				String message=resultData.getString("message");
				showToast(message);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}






}
