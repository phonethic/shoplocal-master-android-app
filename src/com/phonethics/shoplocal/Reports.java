package com.phonethics.shoplocal;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.phonethics.networkcall.GetPeopleCountReciever;
import com.phonethics.networkcall.GetPeopleCountReciever.PeopleCountReceiver;
import com.phonethics.networkcall.GetPeopleCountService;
import com.phonethics.networkcall.GetPerticularStoreDetailService;

public class Reports extends SherlockActivity implements PeopleCountReceiver,GetAllStore{

	TextView totalPeople;

	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	GetPeopleCountReciever getCount;
	GetAllStores_Of_Perticular_User_Receiver mGetStoreReceiver;

	//Api Urls
	static String API_HEADER;
	static String API_VALUE;

	//URL
	static String URL;
	static String GET_STORE_URL;
	static String GET_STORE;

	Activity context;

	String STORE_ID;

	TextView totalLikes;
	TextView totalViews;
	TextView totalShares;

	ActionBar actionBar;

	Typeface tf;

	Button buttonPostReport;
	String storeId;
	String storeName;


	static String USER_ID="";
	static String AUTH_ID="";

	//Session Manger
	SessionManager session;
	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	ImageView reportInfo;

	NetworkCheck network;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reports);

		context=this;
		session=new SessionManager(context);

		network=new NetworkCheck(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle("Reports");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		try
		{
			//Session Data
			HashMap<String,String>user_details=session.getUserDetails();
			USER_ID=user_details.get(KEY_USER_ID).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID).toString();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);


		URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api)+getResources().getString(R.string.peopleCount);

		GET_STORE=getResources().getString(R.string.allStores);
		GET_STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);

		totalPeople = (TextView) findViewById(R.id.totalPeople);
		totalViews = (TextView) findViewById(R.id.totalViews);
		totalLikes = (TextView) findViewById(R.id.totalLikes);
		totalShares= (TextView) findViewById(R.id.totalShares);

		buttonPostReport=(Button)findViewById(R.id.buttonPostReport);

		totalPeople.setTypeface(tf);
		totalViews.setTypeface(tf);
		totalLikes.setTypeface(tf);
		totalShares.setTypeface(tf);

		reportInfo=(ImageView)findViewById(R.id.reportInfo);

		getCount = new GetPeopleCountReciever(new Handler());
		getCount.setReceiver(this);

		mGetStoreReceiver=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
		mGetStoreReceiver.setReceiver(this);

		reportInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.reportsMessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
						//Logout user


					}
				});

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		});

		animate(buttonPostReport).setDuration(200);
		buttonPostReport.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(buttonPostReport).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(buttonPostReport).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});


		try {

			Bundle b = getIntent().getExtras();

			STORE_ID = b.getString("STORE_ID");
			Log.d("STOREID","STOREID" + STORE_ID);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


		if(network.isNetworkAvailable())
		{
			getTotalUsers();
			getPerticularStoreDetails();
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}

	}

	// Method the total number of customers
	private void getTotalUsers() {
		// TODO Auto-generated method stub

		Intent intent = new Intent(context, GetPeopleCountService.class);
		intent.putExtra("getPeopleCount", getCount);
		intent.putExtra("URL", URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("store_id", STORE_ID);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		context.startService(intent);
	}


	//Call Service to get perticular store details
	void getPerticularStoreDetails()
	{
		/*showToast("Service Started");*/
		Intent intent=new Intent(context, GetPerticularStoreDetailService.class);
		intent.putExtra("getStore",mGetStoreReceiver);
		intent.putExtra("URL", GET_STORE_URL+GET_STORE);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("store_id", STORE_ID);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		intent.putExtra("isMerchant", true);
		/*		intent.putExtra("business_id", BUSINESS_ID);*/
		context.startService(intent);
		/*relOffersProgress.setVisibility(View.VISIBLE);*/
	}



	@Override
	public void onReceiveCountApi(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String profile_status = resultData.getString("prof_status");
			String totalPeopleCount = resultData.getString("totalpeoplecount");


			if(profile_status.equalsIgnoreCase("true")){

				totalPeople.setText(totalPeopleCount);
			}
			else if(profile_status.equalsIgnoreCase("error"))
			{
				
			}
			//		else{
			//			
			//			Toast.makeText(context,"In else",0).show();
			//		}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String TOTAL_LIKES = resultData.getString("TOTAL_LIKES");
			String TOTAL_VIEWS =  resultData.getString("TOTAL_VIEWS");
			String TOTAL_SHARE =  resultData.getString("TOTAL_SHARE");

			Log.d("LIKED", "LIKED" + TOTAL_LIKES);

			String store_status=resultData.getString("store_status");

			if(store_status.equalsIgnoreCase("true"))
			{

				totalLikes.setText(TOTAL_LIKES);
				totalViews.setText(TOTAL_VIEWS);
				totalShares.setText(TOTAL_SHARE);
				storeId=resultData.getString("ID");
				storeName=resultData.getString("SOTRE_NAME");



				buttonPostReport.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(context, ViewPost.class);
						intent.putExtra("storeName", storeName);
						intent.putExtra("STORE_ID", storeId);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});


			}
			else if(store_status.equalsIgnoreCase("false"))
			{

				String store_msg=resultData.getString("MSG");
				showToast(store_msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					invalidAuthFinish();
				}
				//			if(store_msg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				session.logoutUser();
				//				finishTo();
				//			}
			}
			else if(store_status.equalsIgnoreCase("error"))
			{
				String store_msg=resultData.getString("MSG");
				showToast(store_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	void invalidAuthFinish()
	{
		try
		{
			
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					try
					{
						session.logoutUser();
						Intent intent=new Intent(context, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();

			/*showToast(getResources().getString(R.string.invalidAuth)+ " Try logging in again ");*/
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		finishTo();
	}

	void finishTo()
	{
		Intent intent=new Intent(context, MerchantTalkHome.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());

	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}


}
