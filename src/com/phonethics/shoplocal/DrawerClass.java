package com.phonethics.shoplocal;

import java.util.ArrayList;

import com.phonethics.model.ExpandibleDrawer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;

public class DrawerClass {

	Activity context;

	SessionManager session;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	DBUtil dbUtil;

	boolean showMore=false;
	
	String PREF_NAME="DRAWER_MODE";
	String PREF_KEY="DRAWER_STAE";

	public DrawerClass(Activity context)
	{
		this.context=context;
		session=new SessionManager(context);
		dbUtil=new DBUtil(context);

	}

//	public void showMoreData(boolean showMore)
//	{
//		this.showMore=showMore;
//	}

	
	public boolean isShowMore() {
		getDrawerPref();
		return showMore;
	}

	public void setShowMore(boolean showMore) {
		this.showMore = showMore;
		setDrawerPref();
	}

	public ArrayList<ExpandibleDrawer> getDrawerAdapter()
	{
		drawerMenu.clear();

		getDrawerPref();
		//Shoplocal
		ExpandibleDrawer drawer=new ExpandibleDrawer();

		Log.i("Active Area", "Active "+dbUtil.getActiveArea());
		Log.i("isShowMore", "isShowMore "+showMore);
		if(dbUtil.getActiveArea().length()!=0)
		{
			drawer.setHeader(dbUtil.getActiveArea());

		}
		else
		{
			drawer.setHeader(context.getResources().getString(R.string.sectionShopLocal));
		}


		drawer.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.open_status)));
		drawer.setCustom_state(1);
		

		drawer.setItem(context.getResources().getString(R.string.itemAllStores));	
		drawer.setIcon(R.drawable.ic_drawer_allstores);

		drawer.setItem(context.getResources().getString(R.string.itemDiscover));
		drawer.setIcon(R.drawable.ic_drawer_offer);


		drawer.setItem(context.getResources().getString(R.string.itemMyShoplocalOffers));	
		drawer.setIcon(R.drawable.ic_drawer_myshoplocal);
		//		drawer.setIcon(R.drawable.myshoplocal_flat);

		drawerMenu.add(drawer);

		if(showMore)
		{
			//Personalize
			ExpandibleDrawer drawer2=new ExpandibleDrawer();
			drawer2.setHeader(context.getResources().getString(R.string.sectionPersonalize));
//			drawer2.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.open_status)));
			drawer2.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.close_status)));
			drawer2.setCustom_state(0);
			if(session.isLoggedInCustomer())
			{
				drawer2.setItem(context.getResources().getString(R.string.itemMyProfile));
				//			drawer2.setIcon(R.drawable.ic_drawer_profile);
				drawer2.setIcon(R.drawable.myprofile);
			}
			else
			{
				drawer2.setItem(context.getResources().getString(R.string.itemLogin));	
				drawer2.setIcon(R.drawable.ic_drawer_login);
			}


			drawer2.setItem(context.getResources().getString(R.string.itemChangeLocation));	
			drawer2.setIcon(R.drawable.ic_drawer_changelocation);
			//		drawer2.setIcon(R.drawable.location_flat);

			drawer2.setItem(context.getResources().getString(R.string.itemSettings));
			drawer2.setIcon(R.drawable.ic_drawer_settings);
			//		drawer2.setIcon(R.drawable.settings_flat);
			drawerMenu.add(drawer2);

			//Business

			ExpandibleDrawer drawer3=new ExpandibleDrawer();
			drawer3.setHeader(context.getResources().getString(R.string.sectionBusiness));
			drawer3.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.close_status)));
			drawer3.setCustom_state(0);
			//
			//		drawer3.setItem(context.getResources().getString(R.string.itemAllStores));	
			//		drawer3.setIcon(R.drawable.ic_drawer_allstores);

			drawer3.setItem(context.getResources().getString(R.string.itemFavouriteStores));	
			drawer3.setIcon(R.drawable.ic_drawer_fav);


			drawerMenu.add(drawer3);

			//About Shoplocal

			ExpandibleDrawer drawer4=new ExpandibleDrawer();
			drawer4.setHeader(context.getResources().getString(R.string.sectionAboutShoplocal));
			drawer4.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.close_status)));
			drawer4.setCustom_state(0);

			drawer4.setItem(context.getResources().getString(R.string.itemAboutShoplocal));	
			drawer4.setIcon(R.drawable.ic_drawer_about);
			
			drawer4.setItem(context.getResources().getString(R.string.itemShare));	
			drawer4.setIcon(R.drawable.ic_drawer_share);

			drawer4.setItem(context.getResources().getString(R.string.itemContactUs));	
			drawer4.setIcon(R.drawable.ic_drawer_contact);

			drawer4.setItem(context.getResources().getString(R.string.itemFacebook));	
			drawer4.setIcon(R.drawable.ic_drawer_facebook);

			drawer4.setItem(context.getResources().getString(R.string.itemTwitter));	
			drawer4.setIcon(R.drawable.ic_drawer_twitter);

			

			drawerMenu.add(drawer4);

			//Sell

			ExpandibleDrawer drawer5=new ExpandibleDrawer();
			drawer5.setHeader(context.getResources().getString(R.string.sectionSell));
			drawer5.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.close_status)));
			drawer5.setCustom_state(0);

			if(session.isLoggedIn())
			{
				drawer5.setItem(context.getResources().getString(R.string.itemBusinessDashboard));
				drawer5.setIcon(R.drawable.ic_drawer_business__dashboard);
			}
			else
			{
				drawer5.setItem(context.getResources().getString(R.string.itemLogintoBusiness));
				drawer5.setIcon(R.drawable.ic_drawer_business__dashboard);
			}


			drawerMenu.add(drawer5);
		}


		return drawerMenu;
	}
	
	void setDrawerPref()
	{
		try
		{
			Log.i("showMore", "showMore "+showMore);
			SharedPreferences prfs=context.getSharedPreferences(PREF_NAME,1);
			Editor editor=prfs.edit();
			editor.putBoolean(PREF_KEY, showMore);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	void getDrawerPref()
	{
		try
		{
			SharedPreferences prfs=context.getSharedPreferences(PREF_NAME,1);
			showMore=prfs.getBoolean(PREF_KEY, false);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void navigate(String drawer_item)
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemDiscover)))
		{
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemLogin)))
		{
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			context.startActivityForResult(intent, 2);
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemMyProfile)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				context.startActivity(intent);
				context.finish();
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemChangeLocation)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemSettings)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemAllStores)))
		{

			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemFavouriteStores)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemAboutShoplocal)))
		{
			drawer_item="";
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);

		}
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemContactUs)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
					"mailto",context.getResources().getString(R.string.contact_email), null));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
			context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
		}
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			context.startActivity(intent);
			context.finish();
			context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=context.getSharedPreferences(context.getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(context.getResources().getString(R.string.usertype),context.getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				context.startActivity(intent);
				context.finish();
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				context.startActivityForResult(intent, 4);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
		}
		if( drawer_item.equalsIgnoreCase(context.getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=context.getSharedPreferences(context.getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(context.getResources().getString(R.string.usertype),context.getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				context.startActivity(intent);
				context.finish();
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}

	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(context.getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				//				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				context.startActivityForResult(intent, 2);
				context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}




}
