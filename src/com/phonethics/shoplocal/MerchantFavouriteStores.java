package com.phonethics.shoplocal;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.DatePreferencesClass;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.networkcall.GetUsersFavPlaceReceiver;
import com.phonethics.networkcall.GetUsersFavPlaceReceiver.ReceiveUserLikedPlaces;
import com.phonethics.networkcall.GetUsersFavPlacesService;
/*import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;*/
import com.squareup.picasso.Picasso;




public class MerchantFavouriteStores extends SherlockActivity implements OnClickListener, OnNavigationListener,ReceiveUserLikedPlaces {

	ActionBar actionBar;
	Activity context;

	static DBUtil dbutil;

	PullToRefreshListView list;
	/*static ImageLoader imageLoaderList;*/

	static boolean swipeToRight=true;
	static int SWIPE_POSITION;

	boolean isRefreshing=false;

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> STORE_NAME=new ArrayList<String>();	
	ArrayList<String> BUILDING=new ArrayList<String>();
	ArrayList<String> STREET=new ArrayList<String>();
	ArrayList<String> LANDMARK=new ArrayList<String>();
	ArrayList<String> AREA_ID=new ArrayList<String>();
	ArrayList<String> AREA=new ArrayList<String>();
	ArrayList<String> CITY=new ArrayList<String>();
	ArrayList<String> STATE=new ArrayList<String>();
	ArrayList<String> COUNTRY=new ArrayList<String>();

	ArrayList<String> MOBILE_NO=new ArrayList<String>();
	ArrayList<String> MOBILE_NO2=new ArrayList<String>();
	ArrayList<String> MOBILE_NO3=new ArrayList<String>();

	ArrayList<String> TEL_NO=new ArrayList<String>();
	ArrayList<String> TEL_NO2=new ArrayList<String>();
	ArrayList<String> TEL_NO3=new ArrayList<String>();


	ArrayList<String> PHOTO=new ArrayList<String>();
	ArrayList<String> DISTANCE=new ArrayList<String>();
	ArrayList<String> DESCRIPTION=new ArrayList<String>();
	ArrayList<String> EMAIL=new ArrayList<String>();
	ArrayList<String> WEBSITE=new ArrayList<String>();

	ArrayList<String> PLACE_PARENT=new ArrayList<String>();
	ArrayList<String> TOTAL_LIKE=new ArrayList<String>();
	ArrayList<String> VERIFIED=new ArrayList<String>();

	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	boolean isFromDefault=false;

	//Session Manger
	SessionManager session;
	boolean isSplitLogin=false;

	/*	SlidingMenu slide_menu ;*/

	//Slide menu
	TextView txtSlideMenu_Register;
	TextView txtSlideMenu_News;
	TextView txtSlideMenu_StoreList;
	TextView txtSlideMenu_Favourite;
	TextView txtSlideMenu_Refresh;
	TextView txtSlideMenu_Dashboard;
	TextView txtSlideMenu_Search;
	TextView txtSlideMenu_Setting;
	TextView txtSlideMenu_Logout;

	String business_id="292";
	int sposition=0;

	static String PHOTO_URL;

	long rowCount;

	//Network
	NetworkCheck isnetConnected;


	int post_count=20;
	static int page=0;

	static String API_HEADER;
	static String API_VALUE;

	static String user_id;
	static String auth_id;

	static String PLACE_URL;
	static String FAVOURITE_API;

	GetUsersFavPlaceReceiver mUserFav;
	ProgressBar progMerchantFav;

	//User Name
	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";


	boolean isLoggedInCustomer;

	static boolean IS_SHOPLOCAL=false;

	ArrayList<String>dropdownItems=new ArrayList<String>();
	String JSON_STRING;
	String key;
	String value;


	int dropdown_position;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;


	private static long back_pressed;

	DrawerExpandibleAdapter drawerAdapter;
	ExpandableListView drawerList;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	String drawer_item="";


	RelativeLayout preLoginLayout;
	Button loginButton;
	Button cancelButton;
	TextView loginTextInfo;
	Typeface tf;

	DrawerClass drawerClass;

	SearchListAdapter adapter;
	
	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;
	
	boolean moreData=false;

	String FAV_DATE_TIME_STAMP="FAV_DATE_TIME_STAMP";
	String KEY="FAV_DATE_TIME";
	
	DatePreferencesClass datePref;
	
	Button gotoMarketButton;
	Button gotoOffersButton;

	LinearLayout noRecordsLayout;
	
	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;
	
	LocalNotification localNotification;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merchant_favourite_stores);
		actionBar=getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setTitle(getResources().getString(R.string.itemFavouriteStores));
		actionBar.setIcon(R.drawable.ic_drawer);
		actionBar.show();

		//Context
		context=this;
		
		localNotification=new LocalNotification(getApplicationContext());
		
		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		
		datePref=new DatePreferencesClass(context, FAVOURITE_API, KEY);

		drawerClass=new DrawerClass(context);
		
		moreData=drawerClass.isShowMore();

		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		//list
		list=(PullToRefreshListView)findViewById(R.id.dafault_FavlistSearchResult);

		preLoginLayout=(RelativeLayout)findViewById(R.id.preLoginLayout);
		loginButton=(Button)findViewById(R.id.loginButton);
		cancelButton=(Button)findViewById(R.id.cancelButton);
		loginTextInfo=(TextView)findViewById(R.id.loginTextInfo);
		//Image Loader
		/*imageLoaderList=new ImageLoader(context);*/

		Bundle b=getIntent().getExtras();

		//Session Manager

		session=new SessionManager(getApplicationContext());
		isSplitLogin=session.isLoggedIn();


		//Login Details
		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		user_id=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		auth_id=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
		isLoggedInCustomer=session.isLoggedInCustomer();

		JSON_STRING=getResources().getString(R.string.dropdownjson);

		//Database
		dbutil=new DBUtil(context);


		progMerchantFav=(ProgressBar)findViewById(R.id.progMerchantFav);

		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		mUserFav=new GetUsersFavPlaceReceiver(new Handler());
		mUserFav.setReceiver(this);

		isnetConnected=new NetworkCheck(context);


		PHOTO_URL=getResources().getString(R.string.photo_url);
		business_id=getResources().getString(R.string.shoplocal_id);

		PLACE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api);
		FAVOURITE_API=getResources().getString(R.string.favourite_places);
		
		gotoMarketButton=(Button)findViewById(R.id.gotoMarketButton);
		gotoOffersButton=(Button)findViewById(R.id.gotoOffersButton);
		noRecordsLayout=(LinearLayout)findViewById(R.id.noRecordsLayout);

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);





		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);
		
		drawerList.addFooterView(footerView);
		
		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}
		
		list_item_footerMore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();
				
			}
		});


		gotoMarketButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				drawer_item="";
				Intent intent=new Intent(context,DefaultSearchList.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		gotoOffersButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				drawer_item="";
				Intent intent=new Intent(context,NewsFeedActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getResources().getString(R.string.itemFavouriteStores));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);


		//Creating Drawer
		createDrawer();

		if(session.isLoggedInCustomer())
		{
			preLoginLayout.setVisibility(View.GONE);
			list.setVisibility(View.VISIBLE);
		}
		else
		{
			preLoginLayout.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}


		/*try
		{
			SharedPreferences pref=context.getSharedPreferences("placeid", 0);
			business_id=pref.getString("place_id", getResources().getString(R.string.shoplocal_id));
			Log.i("business_id", "business_id"+business_id);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}*/


		try
		{
			SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
			dropdown_position=pref.getInt("dropdown_position",0);
			Log.i("dropdown_position", "dropdown_position"+dropdown_position);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		try
		{
			if(session.isLoggedInCustomer())
			{
				clearArray();
				changeData();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}




		//List Navigation
		//		Context acontext = getSupportActionBar().getThemedContext();
		/*ArrayAdapter<CharSequence> business_list = ArrayAdapter.createFromResource(acontext, R.array.business_name, R.layout.list_menus);
						business_list.setDropDownViewResource(R.layout.list_dropdown_item);*/

		//parse json of dropdown and set it to navigation
		//		parseJson(JSON_STRING);

		//Create Adapter
		//		CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(context, 0, 0, dropdownItems);


		//Setting Navigation Type.
		//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		//		actionBar.setListNavigationCallbacks(business_list, this);
		//		actionBar.setSelectedNavigationItem(dropdown_position);

		/*if(b!=null)
		{
			isFromDefault=b.getBoolean("isFromDefault");
		}*/

		//Pulltorefresh

		adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher,ID, STORE_NAME, DISTANCE, PHOTO,MOBILE_NO,DESCRIPTION);
		list.setAdapter(adapter);

		list.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				isRefreshing=true;
				try
				{
					//delete all entries from database

					/*dbutil.deleteAllGeoFavText();*/

					//set page to 1 
					page=0;
					/*	list.setVisibility(View.INVISIBLE);*/



					//load fresh entries
					if(isnetConnected.isNetworkAvailable())
					{
						/*clearArray();*/
						/*changeData();*/
						loadEntries();
					}
					else
					{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		//Last Item visible
		list.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub

				if(isnetConnected.isNetworkAvailable())
				{

					if(ID.size()==Integer.parseInt(TOTAL_RECORDS))
					{
						progMerchantFav.setVisibility(View.GONE);
					}
					else if(page<Integer.parseInt(TOTAL_PAGES))
					{

						if(!progMerchantFav.isShown())
						{
//							page++;
							Log.i("Page", "Page "+page);

							loadEntries();
							Log.i("ID SIZE","ID SIZE  "+ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						}
						else
						{
							//							MyToast.showToast(context, "Please wait while loading",2);
							showToast(getResources().getString(R.string.loadingAlert));
						}

					}
					else
					{
						Log.i("ID SIZE","ID SIZE ELSE "+ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						progMerchantFav.setVisibility(View.GONE);
					}
				}

			}
		});

		loginButton.setTypeface(tf);
		cancelButton.setTypeface(tf);
		loginTextInfo.setTypeface(tf);

		loginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EventTracker.logEvent("FavouriteStore_Login", false);
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		EventTracker.startLocalyticsSession(getApplicationContext());
		
		

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		
		
		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		
		
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
		        pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
		
		
		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}
				
				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
				
			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
			
			
		}
		
		
		/** ANirudh custom share dialog */
		dialogShare = new Dialog(MerchantFavouriteStores.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));


	}//onCreate Ends here
	

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}
		
	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}
	
	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		uiHelper.onStop();
		super.onStop();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}
	
	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		uiHelper.onPause();
		super.onPause();
	}
	

	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();
			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();

		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			
			
			navigate();
			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{

			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{

			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{

			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}
			else
			{
				login();
			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT,RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}*/
		
			showDialogTab();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
		dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{

				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}
	
	void showDialogTab(){
	    
		dialogShare.show();
		
		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){
					
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(MerchantFavouriteStores.this)
						.setLink("http://shoplocal.co.in")
						.build();
						uiHelper.trackPendingDialogCall(shareDialog.present());
						dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}
				
				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}
				
				dismissDialog();
				
			}
		});
	}
	
	void dismissDialog(){
		dialogShare.dismiss();
	}


	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			if(requestCode==2)
			{

				if(session.isLoggedInCustomer())
				{
					EventTracker.logEvent("FavouriteStore_LoginSuccess", false);
					preLoginLayout.setVisibility(View.GONE);
					list.setVisibility(View.VISIBLE);
					//Login Details
					HashMap<String,String>user_details=session.getUserDetailsCustomer();
					user_id=user_details.get(KEY_USER_ID_CUSTOMER).toString();
					auth_id=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
					isLoggedInCustomer=session.isLoggedInCustomer();

					clearArray();
					changeData();
				}
				else
				{
					preLoginLayout.setVisibility(View.VISIBLE );
					list.setVisibility(View.GONE);
				}

				//				getDateProfile();

				createDrawer();

				/*addProfile();*/
			}
			if(requestCode==4)
			{
				if(session.isLoggedIn())
				{

					SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
					Editor 	editor=prefs.edit();
					editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
					editor.commit();

					boolean isAddStore=data.getBooleanExtra("isAddStore", false);
					if(isAddStore)
					{
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						context.finish();
					}
					else
					{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			}

		}catch(IllegalStateException ilgx)
		{
			ilgx.printStackTrace();
		}
		catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}


		if (resultCode != RESULT_OK) {

			return;
		}



	}

	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/* convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_menus, parent, false);
			}
			((TextView) convertView.findViewById(R.id.listText))
			.setText(getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/*convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_dropdown_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(R.id.txtSpinner))
			.setText(getItem(position));
			return convertView;
		}


	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try
		{
//			SearchView searchView = new SearchView(actionBar.getThemedContext());
//			searchView.setQueryHint("Search");
//			searchView.setIconified(true);
//
//			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
//			.setIcon(R.drawable.abs__ic_search)
//			.setActionView(searchView)
//			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//
//			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//				@Override
//				public boolean onQueryTextSubmit(String newText) {
//
//					Intent intent=new Intent(context,SearchActivity.class);
//					intent.putExtra("genericSearch", true);
//					intent.putExtra("search_text", newText);
//					startActivity(intent);
//					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
//					return true;
//				}
//
//
//
//				@Override
//				public boolean onQueryTextChange(String newText) {
//
//
//					return true;
//				}
//			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}


		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}


	void changeData()
	{
		//If data is in database pull it and show it
		try
		{
			
			rowCount=dbutil.getFavRowCount();
			Log.i("Fav", "Fav rowCount= "+rowCount);
			datePref.checkDateDiff();
			if(datePref.getDiffHours()>Integer.parseInt(getString(R.string.pref_hours)))
			{
				if(isnetConnected.isNetworkAvailable())
				{
					isRefreshing=true;
					dbutil.deleteAllGeoFavText();
					page=0;
					loadEntries();	
				}
			}
			else
			{
				//if rowcount is greater than 0 fetch data from db and show it.
				if(rowCount>0)
				{
					loadLocalEntries();

				}else if(isnetConnected.isNetworkAvailable())
				{
					try
					{

						loadEntries();	

					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
				else
				{

					MyToast.showToast(context, getResources().getString(R.string.noInternetConnection),1);
					list.onRefreshComplete();
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}


	//Store List Adapter


	void finishTo()
	{
		/*imageLoaderList.clearCache();*/
		
		if(isFromDefault)
		{
			Intent intent=new Intent(this, DefaultSearchList.class);
			startActivity(intent);
			finish();
		}
		else
		{
			this.finish();
		}
	}



	class SearchListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> store_name,distance,logo,MOBILE_NO,id,desc;
		Activity context;
		LayoutInflater inflate;
		Typeface tf;
		Animation anim_fromRight,anim_fromLeft;
		String isFav="";

		public SearchListAdapter(Activity context, int resource,
				int textViewResourceId,ArrayList<String> id, ArrayList<String> store_name,ArrayList<String>distance,ArrayList<String>logo,ArrayList<String>MOBILE_NO,ArrayList<String>desc) {
			super(context, resource, textViewResourceId, store_name);
			// TODO Auto-generated constructor stub
			this.store_name=store_name;
			this.distance=distance;
			this.logo=logo;
			this.context=context;
			this.MOBILE_NO=MOBILE_NO;
			this.id=id;
			this.desc=desc;
			inflate=context.getLayoutInflater();
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
			/*	anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.activity_slide_from_right);
			anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.activity_slide_from_left);*/

			/*anim_fromRight=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim_fromLeft=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);*/


		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return store_name.get(position);
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_merchant_fav_places,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				convertView.setTag(holder);

			}
			final ViewHolder hold=(ViewHolder)convertView.getTag();

			if(logo.get(position).toString().length()==0)
			{
				//Log.i("LOGO", "LOGO "+logo.get(position));	
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}
			else
			{
				String photo_source=logo.get(position).toString().replaceAll(" ", "%20");
			/*	imageLoaderList.DisplayImage(PHOTO_URL+photo_source, hold.imgStoreLogo);*/
				try{
					Picasso.with(context).load(PHOTO_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			hold.txtStore.setText(store_name.get(position).toString().toUpperCase(Locale.US));

			hold.txtSearchStoreDesc.setText(desc.get(position));

			/*	hold.txtStore.setTextColor(Color.parseColor("#143f85"));*/
			hold.txtStore.setTypeface(tf);

			hold.txtDistance.setVisibility(View.VISIBLE);

			/*if(swipeToRight && SWIPE_POSITION==position)
			{
				hold.linearCall.setVisibility(ViewGroup.VISIBLE);
				hold.txtBackOffers.setVisibility(View.INVISIBLE);
				if(!hold.linearCall.isShown())
					hold.linearCall.startAnimation(anim_fromRight);


			}else if(!swipeToRight && SWIPE_POSITION==position)
			{
				hold.linearCall.setVisibility(ViewGroup.INVISIBLE);
				hold.txtBackOffers.setVisibility(View.VISIBLE);
				if(!hold.txtBackOffers.isShown())
					hold.txtBackOffers.startAnimation(anim_fromLeft);

			}*/
			hold.txtDistance.setVisibility(View.GONE);
			/*if(distance.get(position)!=null)
			{
				if(Double.parseDouble(distance.get(position))!=-1)
				{
					Log.i("Distance : ","Distance : "+distance.get(position));
					DecimalFormat format = new DecimalFormat("##.##");
					String formatted = format.format(Double.parseDouble(distance.get(position)));
					hold.txtDistance.setText(""+formatted+" km.");
				}
			}*/

			/*hold.imgCall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(MOBILE_NO.get(position).length()!=0)
					{
						Intent call = new Intent(android.content.Intent.ACTION_DIAL);
						call.setData(Uri.parse("tel:"+MOBILE_NO.get(position)));
						context.startActivity(call);

					}
					else
					{
						Toast.makeText(context, "No mobile number", Toast.LENGTH_SHORT).show();
					}
				}
			});*/
			/*hold.imgFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						dbutil.open();
						dbutil.updateGeoFavById(Integer.parseInt(id.get(position)), "1");
						dbutil.close();
						notifyDataSetChanged();
						hold.imgFav.setVisibility(View.GONE);
						hold.imgRemoveFav.setVisibility(View.VISIBLE);
						hold.txtFav.setText("Unfavourite");
						DefaultSearchList.clearAnim(position);

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});

			hold.imgRemoveFav.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						dbutil.open();
						dbutil.updateGeoFav_FavById(Integer.parseInt(id.get(position)), "0");
						dbutil.updateGeoFavById(Integer.parseInt(id.get(position)), "0");
						dbutil.close();
						notifyDataSetChanged();
						hold.imgFav.setVisibility(View.VISIBLE);
						hold.imgRemoveFav.setVisibility(View.GONE);
						hold.txtFav.setText("Favourite");
						MerchantFavouriteStores.clearAnim(position);

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});*/


			/*isFav=dbutil.getGeoFav_Favorite(Integer.parseInt(id.get(position)));
			if(isFav.equalsIgnoreCase("1"))
			{
				hold.img_Star.setVisibility(View.VISIBLE);

				hold.imgRemoveFav.setVisibility(View.VISIBLE);

			}else
			{
				if(SWIPE_POSITION==position)
				{
					Animation animFadeOut=AnimationUtils.loadAnimation(context, R.anim.shrink_and_slide_out_left);
					animFadeOut.setStartOffset(350);
					hold.img_Star.setVisibility(View.GONE);
					hold.searchFront.startAnimation(animFadeOut);
					hold.searchBack.startAnimation(animFadeOut);
					animFadeOut.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							hold.searchFront.setVisibility(View.GONE);
							hold.searchBack.setVisibility(View.GONE);
						}
					});
					animFadeOut.setFillAfter(true);
					animFadeOut=null;
				}

			}*/

			return convertView;
		}


	}
	static class ViewHolder
	{
		ImageView imgStoreLogo,imgCall,imgFav,img_Star,imgRemoveFav;
		TextView txtStore,txtSearchStoreDesc;
		TextView txtDistance,txtBackOffers,txtFav;
		RelativeLayout linearCall;
		RelativeLayout searchFront,searchBack;
	}


	public static void clearAnim(int position){
		/*list.closeAnimate(position);*/
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		/*clearArray();
		changeData();*/

		business_id=itemPosition+"";

		SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
		Editor editor=pref.edit();
		editor.putString("selected_dropdown", getDropDownObject(itemPosition,JSON_STRING));
		editor.putInt("dropdown_position", itemPosition);
		editor.commit();

		Log.i("business_id", "business_id commited "+getDropDownObject(itemPosition,JSON_STRING));

		//parse key and value to search
		parseKeyValue(getDropDownObject(itemPosition,JSON_STRING));
		clearArray();
		changeData();


		/*SharedPreferences pref=context.getSharedPreferences("placeid", 0);
		Editor editor=pref.edit();
		if(itemPosition==0)
		{
			business_id=getResources().getString(R.string.malad_id);
			editor.putString("place_id", business_id);
			editor.commit();
			Log.i("business_id", "business_id commited "+business_id);
			clearArray();
			changeData();


		}
		else if(itemPosition==1)
		{
			business_id=getResources().getString(R.string.vashi_id);
			editor.putString("place_id", business_id);
			editor.commit();
			Log.i("business_id", "business_id commited "+business_id);
			clearArray();
			changeData();
		}
		else if(itemPosition==2)
		{
			business_id=getResources().getString(R.string.cyberabad_id);
			editor.putString("place_id", business_id);
			editor.commit();
			Log.i("business_id", "business_id commited "+business_id);
			clearArray();
			changeData();
		}
		else if(itemPosition==3)
		{
			business_id=getResources().getString(R.string.whitefield_id);
			editor.putString("place_id", business_id);
			editor.commit();
			Log.i("business_id", "business_id commited "+business_id);
			clearArray();
			changeData();

		}*/
		return false;
	}

	void parseKeyValue(String json)
	{
		try {
			Log.i("PARSE ", "PARSE KEY  "+json);
			JSONObject jsonobject=new JSONObject(json);
			key=jsonobject.getString("key");
			value=jsonobject.getString("value");
			IS_SHOPLOCAL=Boolean.parseBoolean(jsonobject.getString("isShopLocal"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void parseJson(String json)
	{
		try {

			Log.i("PARSE ", "PARSE  "+json);

			JSONObject jsonobject=new JSONObject(json);
			JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");
			for(int i=0;i<dropDownArray.length();i++)
			{
				JSONObject jsonObject=dropDownArray.getJSONObject(i);

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					//Shoplocal
					if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}else
				{
					//Inorbit
					if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	String getDropDownObject(int position,String json)
	{
		String dropdown="";
		if(dropdownItems.size()!=0)
		{

			Log.i("PARSE ", "PARSE getDropDownObject  "+json);
			//Log.i("BOOLEAN ", "BOOLEAN "+getResources().getBoolean(R.bool.isAppShopLocal)+" POSITION "+position);
			try {
				JSONObject jsonobject=new JSONObject(json);
				JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");

				/*	JSONObject jsonObject=dropDownArray.getJSONObject(position);
				dropdown=jsonObject.toString();

				Log.i("JSON", "JSON "+dropdown);*/

				for(int i=0;i<dropDownArray.length();i++)
				{
					JSONObject jsonObject=dropDownArray.getJSONObject(i);

					Log.i("PARSE ", "BOOLEAN PARSE getDropDownObject  "+jsonObject.toString()); 


					if(getResources().getBoolean(R.bool.isAppShopLocal))
					{
						//Shoplocal
						if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}else
					{
						//Inorbit
						if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}
				}

				Log.i("GET DROP DOWN ", "GET DROP DOWN "+dropdown);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();

			}


		}
		return dropdown;
	}

	void clearArray()
	{
		ID.clear();
		STORE_NAME.clear();	
		BUILDING.clear();
		STREET.clear();
		LANDMARK.clear();
		AREA.clear();
		CITY.clear();
		STATE.clear();
		COUNTRY.clear();
		MOBILE_NO.clear();
		MOBILE_NO2.clear();
		MOBILE_NO3.clear();

		TEL_NO.clear();
		TEL_NO2.clear();
		TEL_NO3.clear();

		PHOTO.clear();
		DISTANCE.clear();
		DESCRIPTION.clear();
		EMAIL.clear();
		WEBSITE.clear();

		page=0;
		post_count=20;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.GetUsersFavPlaceReceiver.ReceiveUserLikedPlaces#onReceiveUserLikedPlaces(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveUserLikedPlaces(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			progMerchantFav.setVisibility(View.GONE);
			String LIKE_STATUS=resultData.getString("LIKE_STATUS");
			String LIKE_MESSAGE=resultData.getString("LIKE_MESSAGE");
			list.setVisibility(View.VISIBLE);
			if(LIKE_STATUS.equalsIgnoreCase("true"))
			{
				ArrayList<String> TempID=new ArrayList<String>();
				ArrayList<String> TempPlaceParent=new ArrayList<String>();
				ArrayList<String> TempSOTRE_NAME=new ArrayList<String>();	
				ArrayList<String> TempBUILDING=new ArrayList<String>();
				ArrayList<String> TempSTREET=new ArrayList<String>();
				ArrayList<String> TempLANDMARK=new ArrayList<String>();
				ArrayList<String> TempAREA_ID=new ArrayList<String>();
				ArrayList<String> TempAREA=new ArrayList<String>();
				ArrayList<String> TempCITY=new ArrayList<String>();
				ArrayList<String> TempSTATE=new ArrayList<String>();
				ArrayList<String> TempCOUNTRY=new ArrayList<String>();

				ArrayList<String> TempMOBILE_NO=new ArrayList<String>();
				ArrayList<String> TempMOBILE_NO2=new ArrayList<String>();
				ArrayList<String> TempMOBILE_NO3=new ArrayList<String>();

				ArrayList<String> TempTEL_NO=new ArrayList<String>();
				ArrayList<String> TempTEL_NO2=new ArrayList<String>();
				ArrayList<String> TempTEL_NO3=new ArrayList<String>();


				ArrayList<String> TempPHOTO=new ArrayList<String>();
				ArrayList<String> TempDISTANCE=new ArrayList<String>();
				ArrayList<String> TempHasOffers=new ArrayList<String>();
				ArrayList<String> TempOffers=new ArrayList<String>();
				ArrayList<String> TempDescription=new ArrayList<String>();
				ArrayList<String> TempTotalLike=new ArrayList<String>();
				ArrayList<String> TempVERIFIED=new ArrayList<String>();
				ArrayList<String> TempEmail=new ArrayList<String>();
				ArrayList<String> TempWebSite=new ArrayList<String>();

				TempID.clear();
				TempPlaceParent.clear();
				TempSOTRE_NAME.clear();
				TempBUILDING.clear();
				TempSTREET.clear();
				TempLANDMARK.clear();
				TempAREA_ID.clear();
				TempAREA.clear();
				TempCITY.clear();

				TempMOBILE_NO.clear();
				TempMOBILE_NO2.clear();
				TempMOBILE_NO3.clear();

				TempTEL_NO.clear();
				TempTEL_NO2.clear();
				TempTEL_NO3.clear();




				TempPHOTO.clear();
				TempDISTANCE.clear();
				TempHasOffers.clear();
				TempOffers.clear();
				TempDescription.clear();
				TempTotalLike.clear();
				TempVERIFIED.clear();
				TempEmail.clear();
				TempWebSite.clear();

				TempID=resultData.getStringArrayList("ID");
				TempPlaceParent=resultData.getStringArrayList("PLACE_PARENT");
				TempSOTRE_NAME=resultData.getStringArrayList("STORE_NAME");
				TempDescription=resultData.getStringArrayList("DESCRIPTION");
				TempBUILDING=resultData.getStringArrayList("BUILDING");
				TempSTREET=resultData.getStringArrayList("STREET");
				TempLANDMARK=resultData.getStringArrayList("LANDMARK");
				TempAREA_ID=resultData.getStringArrayList("AREA_ID");
				TempAREA=resultData.getStringArrayList("AREA");
				TempCITY=resultData.getStringArrayList("CITY");

				TempMOBILE_NO=resultData.getStringArrayList("MOBILE_NO");
				TempMOBILE_NO2=resultData.getStringArrayList("MOBILE_NO2");
				TempMOBILE_NO3=resultData.getStringArrayList("MOBILE_NO3");

				TempTEL_NO=resultData.getStringArrayList("TEL_NO");
				TempTEL_NO2=resultData.getStringArrayList("TEL_NO2");
				TempTEL_NO3=resultData.getStringArrayList("TEL_NO3");

				TempPHOTO=resultData.getStringArrayList("PHOTO");
				TempEmail=resultData.getStringArrayList("EMAIL");
				TempWebSite=resultData.getStringArrayList("WEBSITE");
				TempTotalLike=resultData.getStringArrayList("TOTAL_LIKE");
				TempVERIFIED=resultData.getStringArrayList("VERIFIED");


				Log.i("FAV", "FAV API ACTIVITY "+"SIZE "+TempID.size()+" "+TempPlaceParent.size()+" "+TempSOTRE_NAME.size()+" "+TempDescription.size()+" "+TempBUILDING.size()+" "+TempSTREET.size()
						+" "+TempLANDMARK.size()+" "+TempAREA.size()+" "+TempCITY.size()+" "+TempMOBILE_NO.size()+" "+TempPHOTO.size()+" "+TempEmail.size()+" "+TempWebSite.size()+" "+TempTotalLike.size());

				if(isRefreshing)
				{
					clearArray();
					isRefreshing=false;
					dbutil.deleteAllGeoFavText();
				}

				for(int index=0;index<TempID.size();index++)
				{
					ID.add(TempID.get(index));
					PLACE_PARENT.add(TempPlaceParent.get(index));
					STORE_NAME.add(TempSOTRE_NAME.get(index));
					DESCRIPTION.add(TempDescription.get(index));
					BUILDING.add(TempBUILDING.get(index));
					STREET.add(TempSTREET.get(index));
					LANDMARK.add(TempLANDMARK.get(index));
					AREA_ID.add(TempAREA_ID.get(index));
					AREA.add(TempAREA.get(index));
					CITY.add(TempCITY.get(index));

					MOBILE_NO.add(TempMOBILE_NO.get(index));
					MOBILE_NO2.add(TempMOBILE_NO2.get(index));
					MOBILE_NO3.add(TempMOBILE_NO3.get(index));

					TEL_NO.add(TempTEL_NO.get(index));
					TEL_NO2.add(TempTEL_NO2.get(index));
					TEL_NO3.add(TempTEL_NO3.get(index));



					PHOTO.add(TempPHOTO.get(index));
					TempDISTANCE.add("0");
					DISTANCE.add("0");
					EMAIL.add(TempEmail.get(index));
					WEBSITE.add(TempWebSite.get(index));
					TOTAL_LIKE.add(TempTotalLike.get(index));
					VERIFIED.add(TempVERIFIED.get(index));


				}


				Log.i("FAV", "FAV API OG "+"SIZE "+ID.size()+" "+PLACE_PARENT.size()+" "+STORE_NAME.size()+" "+DESCRIPTION.size()+" "+BUILDING.size()+" "+STREET.size()
						+" "+LANDMARK.size()+" "+AREA.size()+" "+CITY.size()+" "+MOBILE_NO.size()+" "+PHOTO.size()+" "+EMAIL.size()+" "+WEBSITE.size()+" "+TOTAL_LIKE.size());

				TOTAL_PAGES=resultData.getString("TOTAL_PAGES");
				TOTAL_RECORDS=resultData.getString("TOTAL_RECORDS");
				page++;
				long entries_Affected=0;
				for(int i=0;i<TempID.size();i++)
				{

					if(dbutil.isGeoFavIdExists(Integer.parseInt(TempID.get(i)))==false)
					{
						dbutil.open();
						entries_Affected=dbutil.create_geotextFav(Integer.parseInt(TempID.get(i)),Integer.parseInt(TempPlaceParent.get(i)),TempSOTRE_NAME.get(i),TempDescription.get(i), TempBUILDING.get(i), TempSTREET.get(i),TempLANDMARK.get(i),Integer.parseInt(TempAREA_ID.get(i)),TempAREA.get(i), TempCITY.get(i), 
								TempMOBILE_NO.get(i),TempMOBILE_NO2.get(i),TempMOBILE_NO3.get(i),TempTEL_NO.get(i),TempTEL_NO2.get(i),TempTEL_NO3.get(i), TempPHOTO.get(i), TempDISTANCE.get(i), Integer.parseInt(TOTAL_PAGES), Integer.parseInt(TOTAL_RECORDS),page,"0","0"," ",Integer.parseInt(TempTotalLike.get(i)),TempVERIFIED.get(i));
						dbutil.close();
					}
				}
				
				datePref.setDateInPref();

				//				SearchListAdapter adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher,ID, STORE_NAME, DISTANCE, PHOTO,MOBILE_NO,DESCRIPTION);
				//				list.setAdapter(adapter);

				adapter.notifyDataSetChanged();
				list.onRefreshComplete();

				list.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						if(!isRefreshing)
						{
							Intent intent=new Intent(context, StoreDetailsActivity.class);
							intent.putExtra("STORE_ID", ID.get(position-1));
							intent.putExtra("STORE", STORE_NAME.get(position-1));
							intent.putExtra("PHOTO", PHOTO.get(position-1));
							intent.putExtra("AREA", AREA.get(position-1));
							intent.putExtra("CITY", CITY.get(position-1));
							intent.putExtra("MOBILE_NO", MOBILE_NO.get(position-1));
							startActivity(intent);
							/*overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);*/
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
				});

				Log.i("Entries", "Entries Affected : "+entries_Affected);

				SharedPreferences pref=context.getSharedPreferences("FAVPage", 0);
				Editor editor=pref.edit();

				editor.putString(business_id+"FAV TOTAL_PAGES",TOTAL_PAGES);
				editor.putString(business_id+"FAV TOTAL_RECORDS",TOTAL_RECORDS);
				editor.putString(business_id+"FAV page",page+"");
				editor.commit();
				
				localNotification.checkPref();
				localNotification.setFavPref(true, localNotification.isFavAlarmFired());

			}
			else if(LIKE_STATUS.equalsIgnoreCase("false"))
			{

				String error_code=resultData.getString("error_code");
				clearArray();
				//				changeData();
				list.onRefreshComplete();
				
				adapter.notifyDataSetChanged();
				//No Records Found
				if(error_code.equalsIgnoreCase("-241"))
				{
					localNotification.checkPref();
					localNotification.setFavPref(false, localNotification.isFavAlarmFired());
					
					noRecordsLayout.setVisibility(View.VISIBLE);
					list.setVisibility(View.GONE);
				}
				//				list.setVisibility(View.GONE);
//				String invalid=getResources().getString(R.string.invalidAuth);
				Log.i("LIKE_MESSAGE ","LIKE_MESSAGE  "+LIKE_MESSAGE+" INV "+getResources().getString(R.string.invalidAuth));
				showToast(LIKE_MESSAGE);
				
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
					createDrawer();
					if(session.isLoggedInCustomer())
					{
						preLoginLayout.setVisibility(View.GONE);
						list.setVisibility(View.VISIBLE);
					}
					else
					{
						preLoginLayout.setVisibility(View.VISIBLE);
						list.setVisibility(View.GONE);
					}
					
				}
//				if(LIKE_MESSAGE.startsWith(invalid))
//				{
//					//showToast("Logout and Login again to view your favourites");
//					//					MyToast.showToast(context,"Logout and Login again to view your favourites",3);
//					session.logoutCustomer();
//				}
			}
			else if(LIKE_STATUS.equalsIgnoreCase("error"))
			{
				showToast(LIKE_MESSAGE);
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void loadEntries()
	{
		EventTracker.logEvent("FavouriteStore_Fetch", false);
		
		
		Intent intent=new Intent(context, GetUsersFavPlacesService.class);
		intent.putExtra("userfav",mUserFav);
		intent.putExtra("URL", PLACE_URL+FAVOURITE_API);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("search_post_count",post_count);
		intent.putExtra("search_page",page+1);


		intent.putExtra("user_id",user_id);
		intent.putExtra("auth_id",auth_id);
		context.startService(intent);
		progMerchantFav.setVisibility(View.VISIBLE);

	}
	void loadLocalEntries()
	{
		try
		{
			ArrayList<String> TempID=new ArrayList<String>();
			ArrayList<String> TempPlaceParent=new ArrayList<String>();
			ArrayList<String> TempSOTRE_NAME=new ArrayList<String>();	
			ArrayList<String> TempBUILDING=new ArrayList<String>();
			ArrayList<String> TempSTREET=new ArrayList<String>();
			ArrayList<String> TempLANDMARK=new ArrayList<String>();
			ArrayList<String> TempAREA=new ArrayList<String>();
			ArrayList<String> TempAREA_ID=new ArrayList<String>();
			ArrayList<String> TempCITY=new ArrayList<String>();
			ArrayList<String> TempSTATE=new ArrayList<String>();
			ArrayList<String> TempCOUNTRY=new ArrayList<String>();

			ArrayList<String> TempMOBILE_NO=new ArrayList<String>();
			ArrayList<String> TempMOBILE_NO2=new ArrayList<String>();
			ArrayList<String> TempMOBILE_NO3=new ArrayList<String>();

			ArrayList<String> TempTEL_NO=new ArrayList<String>();
			ArrayList<String> TempTEL_NO2=new ArrayList<String>();
			ArrayList<String> TempTEL_NO3=new ArrayList<String>();

			ArrayList<String> TempPHOTO=new ArrayList<String>();
			ArrayList<String> TempDISTANCE=new ArrayList<String>();
			ArrayList<String> TempHasOffers=new ArrayList<String>();
			ArrayList<String> TempOffers=new ArrayList<String>();
			ArrayList<String> TempDescription=new ArrayList<String>();
			ArrayList<String> TempTotalLike=new ArrayList<String>();
			ArrayList<String> TempEmail=new ArrayList<String>();
			ArrayList<String> TempWebSite=new ArrayList<String>();
			//			if(IS_SHOPLOCAL)
			//			{


			TempID.clear();
			TempPlaceParent.clear();
			TempSOTRE_NAME.clear();
			TempBUILDING.clear();
			TempSTREET.clear();
			TempLANDMARK.clear();
			TempAREA.clear();
			TempAREA_ID.clear();
			TempCITY.clear();

			TempMOBILE_NO.clear();
			TempMOBILE_NO2.clear();
			TempMOBILE_NO3.clear();

			TempTEL_NO.clear();
			TempTEL_NO2.clear();
			TempTEL_NO3.clear();

			TempPHOTO.clear();
			TempDISTANCE.clear();
			TempHasOffers.clear();
			TempOffers.clear();
			TempDescription.clear();
			TempTotalLike.clear();
			TempEmail.clear();
			TempWebSite.clear();

			TempID=dbutil.getAllFavGeoStoreId();
			TempSOTRE_NAME=dbutil.getAllFavGeoStoreName();
			TempAREA=dbutil.getAllFavGeoAreas();
			TempAREA_ID=dbutil.getAllFavGeoAreaID();
			TempCITY=dbutil.getAllFavGeoCity();
			TempPHOTO=dbutil.getAllFavGeoPhoto();


			TempMOBILE_NO=dbutil.getGeoFavAllMobileNos();
			TempMOBILE_NO2=dbutil.getGeoFavAllMobileNos2();
			TempMOBILE_NO3=dbutil.getGeoFavAllMobileNos3();

			TempTEL_NO=dbutil.getGeoFavAllTelNos();
			TempTEL_NO2=dbutil.getGeoFavAllTelNos2();
			TempTEL_NO3=dbutil.getGeoFavAllTelNos3();


			TempDISTANCE=dbutil.getAllFavDistances();
			TempDescription=dbutil.getAllFavGeoStoreDESC();


			for(int index=0;index<TempID.size();index++)
			{
				ID.add(TempID.get(index));
				STORE_NAME.add(TempSOTRE_NAME.get(index));
				AREA_ID.add(TempAREA_ID.get(index));
				AREA.add(TempAREA.get(index));
				CITY.add(TempCITY.get(index));

				MOBILE_NO.add(TempMOBILE_NO.get(index));
				MOBILE_NO2.add(TempMOBILE_NO2.get(index));
				MOBILE_NO3.add(TempMOBILE_NO3.get(index));

				TEL_NO.add(TempTEL_NO.get(index));
				TEL_NO2.add(TempTEL_NO2.get(index));
				TEL_NO3.add(TempTEL_NO3.get(index));


				PHOTO.add(TempPHOTO.get(index));
				DISTANCE.add(TempDISTANCE.get(index));
				DESCRIPTION.add(TempDescription.get(index));

			}
			SharedPreferences pref=context.getSharedPreferences("FAVPage", 0);
			page=Integer.parseInt(pref.getString(business_id+"FAV page", "1"));
			TOTAL_PAGES=pref.getString(business_id+"FAV TOTAL_PAGES", "1");
			TOTAL_RECORDS=pref.getString(business_id+"FAV TOTAL_RECORDS", "1");

			Log.i("Business_id", "business_id current page "+page+" Total Pages "+TOTAL_PAGES+" TOTAL_RECORDS "+TOTAL_RECORDS);

			Log.i("CHECK SIZE", "CHECK SIZE : "+TempID.size()+" "+TempPlaceParent.size()+" "+TempSOTRE_NAME.size()+" "+TempAREA.size()+" "+TempDISTANCE.size());

			for(int index=0;index<TempID.size();index++)
			{
				Log.i("", "--------------------------------------------------");
				Log.i("STATUS", "STATUS : "+"ID "+ID.get(index)+" STORE NAME "+STORE_NAME.get(index)+" MOBILE NO : "+MOBILE_NO.get(index)+" DESC "+DESCRIPTION.get(index));

			}


			//			SearchListAdapter adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher,ID, STORE_NAME, DISTANCE, PHOTO,MOBILE_NO,DESCRIPTION);
			//			list.setAdapter(adapter);

			//List Refresh completed

			adapter.notifyDataSetChanged();
			list.onRefreshComplete();

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					if(!isRefreshing)
					{
						Intent intent=new Intent(context, StoreDetailsActivity.class);
						intent.putExtra("STORE_ID", ID.get(position-1));
						intent.putExtra("STORE", STORE_NAME.get(position-1));
						intent.putExtra("PHOTO", PHOTO.get(position-1));
						intent.putExtra("AREA", AREA.get(position-1));
						intent.putExtra("CITY", CITY.get(position-1));
						intent.putExtra("MOBILE_NO", MOBILE_NO.get(position-1));
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			});
			//			}
			//			else
			//			{
			//				TempID=dbutil.getAllFavGeoStoreId(Integer.parseInt(value));
			//				TempSOTRE_NAME=dbutil.getAllFavGeoStoreName(Integer.parseInt(value));
			//				TempAREA=dbutil.getAllFavGeoAreas(Integer.parseInt(value));
			//				TempCITY=dbutil.getAllFavGeoCity(Integer.parseInt(value));
			//				TempPHOTO=dbutil.getAllFavGeoPhoto(Integer.parseInt(value));
			//				TempMOBILE_NO=dbutil.getGeoFavAllMobileNos(Integer.parseInt(value));
			//				TempDISTANCE=dbutil.getAllFavDistances(Integer.parseInt(value));
			//				TempDescription=dbutil.getAllFavGeoStoreDESC(Integer.parseInt(value));
			//
			//
			//				for(int index=0;index<TempID.size();index++)
			//				{
			//					ID.add(TempID.get(index));
			//					STORE_NAME.add(TempSOTRE_NAME.get(index));
			//					AREA.add(TempAREA.get(index));
			//					CITY.add(TempCITY.get(index));
			//					MOBILE_NO.add(TempMOBILE_NO.get(index));
			//					PHOTO.add(TempPHOTO.get(index));
			//					DISTANCE.add(TempDISTANCE.get(index));
			//					DESCRIPTION.add(TempDescription.get(index));
			//
			//				}
			//				SharedPreferences pref=context.getSharedPreferences("FAVPage", 0);
			//				page=Integer.parseInt(pref.getString(business_id+"FAV page", "1"));
			//				TOTAL_PAGES=pref.getString(business_id+"FAV TOTAL_PAGES", "1");
			//				TOTAL_RECORDS=pref.getString(business_id+"FAV TOTAL_RECORDS", "1");
			//
			//				Log.i("Business_id", "business_id current page "+page+" Total Pages "+TOTAL_PAGES+" TOTAL_RECORDS "+TOTAL_RECORDS);
			//
			//				Log.i("CHECK SIZE", "CHECK SIZE : "+TempID.size()+" "+TempPlaceParent.size()+" "+TempSOTRE_NAME.size()+" "+TempAREA.size()+" "+TempDISTANCE.size());
			//
			//				for(int index=0;index<TempID.size();index++)
			//				{
			//					Log.i("", "--------------------------------------------------");
			//					Log.i("STATUS", "STATUS : "+"ID "+ID.get(index)+" STORE NAME "+STORE_NAME.get(index)+" MOBILE NO : "+MOBILE_NO.get(index)+" DESC "+DESCRIPTION.get(index));
			//
			//				}
			//
			//
			//				SearchListAdapter adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher,ID, STORE_NAME, DISTANCE, PHOTO,MOBILE_NO,DESCRIPTION);
			//				list.setAdapter(adapter);
			//
			//				//List Refresh completed
			//				list.onRefreshComplete();
			//
			//				list.setOnItemClickListener(new OnItemClickListener() {
			//
			//					@Override
			//					public void onItemClick(AdapterView<?> arg0, View arg1,
			//							int position, long arg3) {
			//						// TODO Auto-generated method stub
			//						if(!isRefreshing)
			//						{
			//							Intent intent=new Intent(context, StoreDetailsActivity.class);
			//							intent.putExtra("STORE_ID", ID.get(position-1));
			//							intent.putExtra("STORE", STORE_NAME.get(position-1));
			//							intent.putExtra("PHOTO", PHOTO.get(position-1));
			//							intent.putExtra("AREA", AREA.get(position-1));
			//							intent.putExtra("CITY", CITY.get(position-1));
			//							intent.putExtra("MOBILE_NO", MOBILE_NO.get(position-1));
			//							startActivity(intent);
			//							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//						}
			//					}
			//				});
			//			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}


}
