package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.localytics.LocalyticsSession;
import com.phonethics.model.GetBroadCast;
import com.phonethics.networkcall.BroadCastApiReceiver;
import com.phonethics.networkcall.BroadCastApiReceiver.BroadCastApi;
import com.phonethics.networkcall.BroadCastService;
import com.phonethics.networkcall.FavouritePlaceResultReceiver;
import com.phonethics.networkcall.FavouritePlaceResultReceiver.FavouriteInterface;
import com.phonethics.networkcall.FavouritePlaceService;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.phonethics.networkcall.GetPerticularStoreDetailService;
import com.phonethics.networkcall.LoadGalleryReceiver;
import com.phonethics.networkcall.LoadGalleryReceiver.LoadGallery;
import com.phonethics.networkcall.LoadStoreGallery;
import com.phonethics.networkcall.PlaceShareResultReceiver;
import com.phonethics.networkcall.PlaceShareResultReceiver.PlaceShareResultInterface;
import com.phonethics.networkcall.PlaceShareService;
import com.phonethics.networkcall.UnFavouritePlaceReceiver;
import com.phonethics.networkcall.UnFavouritePlaceReceiver.UnfavouriteInterface;
import com.phonethics.networkcall.UnFavouritePlaceService;
import com.squareup.picasso.Picasso;

public class StoreDetailsActivity extends SherlockFragmentActivity implements BroadCastApi, GetAllStore, FavouriteInterface, UnfavouriteInterface, OnPageChangeListener, PlaceShareResultInterface, LoadGallery {

	ViewPager storePager;
	PagerTitleStrip pagerStoreTabs;

	static String PAGE_ABOUT_STORE="About Store";
	static String PAGE_ADDRESS="Address";
	static String PAGE_POST="Post/Offers";
	static String PAGE_IMAGE_GALLERY="Photo Gallery";
	static String PAGE_REPORTS="REPORTS";

	ArrayList<String>pages=new ArrayList<String>();
	StorePageAdapter storeAdapter;
	static ProgressBar storeProgress;

	//Session Manger
	static SessionManager session;

	static String USER_ID="";
	static String AUTH_ID="";

	//User Name
	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	static String USER_LIKE="-1";
	static boolean getLike=false;
	static boolean getPostLike=false;
	static boolean likePlace=false;

	int loadGallery=1;

	static Activity context;
	ActionBar actionBar;

	static NetworkCheck isnetConnected;

	//Receiver
	static BroadCastApiReceiver mBroadCast;
	static GetAllStores_Of_Perticular_User_Receiver mGetStoreReceiver;

	static FavouritePlaceResultReceiver mFavourite;
	static UnFavouritePlaceReceiver mUnFavourite;
	static LoadGalleryReceiver mReceiver;

	PlaceShareResultReceiver placeShare;

	static String STORE_URL,STORE_GALLERY_URL;
	static String STORE_GALLERY;
	static String GET_STORE;
	static String API_HEADER;
	static String API_VALUE;
	static String GET_STORE_URL;
	static String LIKE_API;
	static String PLACE_SHARE_URL;
	static String PLACE_SHARE_API;
	static String PHOTO_PARENT_URL;
	static String GET_BROADCAST_URL;

	static String STORE_ID="";
	static String REC_SOTRE_NAME;	
	static String STREET;
	static String LANDMARK;
	static String DESCRIPTION;

	String STORE="";
	static String MOBILE_NO="";

	static String TELNO1;
	static String TELNO2;
	static String TELNO3;

	static String MOBNO2;
	static String MOBNO3;

	static String PHOTO="";
	static String CITY="";
	static String AREA="";
	static String FB="";
	static String TWITTER="";
	static String WEBSITE;
	static String EMAIL;

	static String LATITUDE_PLACE="0";
	static String LONGITUDE_PLACE="0";

	static String OPEN_TIME="";
	static String CLOSE_TIME="";

	/*	static ImageLoader imageLoaderList;*/

	//Broadcast

	static ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> TYPE=new ArrayList<String>();	
	ArrayList<String> TITLE=new ArrayList<String>();
	ArrayList<String> BODY=new ArrayList<String>();
	ArrayList<String> POST_USER_LIKE=new ArrayList<String>();
	ArrayList<String>TOTAL_SHARE=new ArrayList<String>();



	ArrayList<String> PHOTO_SOURCE=new ArrayList<String>();
	ArrayList<String> DATE=new ArrayList<String>();

	ArrayList<String>TOTAL_LIKE=new ArrayList<String>();


	static ArrayList<String> callnumbers=new ArrayList<String>();

	//Call Dialog
	static Dialog calldailog;
	static ListView listCall;

	static ArrayList<String>SOURCE=new ArrayList<String>();

/*	static Dialog store_mapDialog;*/

	static String latitued;
	static String longitude;
	static boolean isLocationFound;

	//Event maps
	Map<String, String> view;
	static Map<String, String> favourite = new HashMap<String, String>();
	static Map<String, String> unfavourite = new HashMap<String, String>();
	static Map<String, String> share = new HashMap<String, String>();
	static Map<String, String> viewgallery = new HashMap<String, String>();
	
	Map<String, String> callDone = new HashMap<String, String>();

	boolean isEventFired;

/*	ImageView store_MapImage;*/

	private static UiLifecycleHelper uiHelper;

	static String contactno="";
	static String mapLink="";
	static String mapLinkLable="";

	TextView tvActionBarTitle;

	/* Custom dialog for share */
	static ArrayList<String> packageNames = new ArrayList<String>();
	static ArrayList<String> appName = new ArrayList<String>();
	static ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	static String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	static Dialog dialogShare;
	static ListView listViewShare;

	/** Check if facebook is present in phone */
	static boolean isFacebookPresent = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_details);

		context=this;

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);


		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		isnetConnected=new NetworkCheck(context);
		session=new SessionManager(context);

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		//LayoutInflater inflater = LayoutInflater.from(this);
		View actionBarView = getLayoutInflater().inflate(R.layout.actionbarlayoutforstoredetail, null);
		tvActionBarTitle = (TextView) actionBarView.findViewById(R.id.textViewActionBarTitle);
		actionBar.setCustomView(actionBarView);

		/*	imageLoaderList=new ImageLoader(context);*/

		mGetStoreReceiver=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
		mGetStoreReceiver.setReceiver(this);

		placeShare=new PlaceShareResultReceiver(new Handler());
		placeShare.setReceiver(this);

		mBroadCast=new BroadCastApiReceiver(new Handler());
		mBroadCast.setReceiver(this);

		mFavourite=new FavouritePlaceResultReceiver(new Handler());
		mFavourite.setReceiver(this);

		mUnFavourite=new UnFavouritePlaceReceiver(new Handler());
		mUnFavourite.setReceiver(this);

		mReceiver=new LoadGalleryReceiver(new Handler());
		mReceiver.setReceiver(this);


		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		GET_STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);

		PLACE_SHARE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		PLACE_SHARE_API=getResources().getString(R.string.share);

		GET_BROADCAST_URL=getResources().getString(R.string.broadcasts);

		//Store API
		STORE_GALLERY_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);

		//Store Gallery
		STORE_GALLERY=getResources().getString(R.string.store_gallery);


		LIKE_API=getResources().getString(R.string.like);

		//Photo url	
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//Store
		GET_STORE=getResources().getString(R.string.allStores);

		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		storePager=(ViewPager)findViewById(R.id.storePager);
		pagerStoreTabs=(PagerTitleStrip)findViewById(R.id.pagerStoreTabs);
		storeProgress=(ProgressBar)findViewById(R.id.storeProgress);

		//		pagerStoreTabs.setVisibility(View.GONE);
		//		storePager.setVisibility(View.GONE);

		calldailog=new Dialog(context);
		calldailog.setContentView(R.layout.calldialog);
		calldailog.setTitle("Contact");
		calldailog.setCancelable(true);
		calldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		/*store_mapDialog=new Dialog(context);
		store_mapDialog.setContentView(R.layout.store_pager_map);
		store_mapDialog.setTitle("Map");
		store_mapDialog.setCancelable(true);
		store_mapDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		store_MapImage=(ImageView)store_mapDialog.findViewById(R.id.storePagerMapImage);*/

		listCall=(ListView)calldailog.findViewById(R.id.listCall);



		try
		{
			getLocationFromPrefs();

			Bundle b=getIntent().getExtras();
			if(b!=null)
			{
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

				STORE_ID=b.getString("STORE_ID");

				pages.add(PAGE_ABOUT_STORE);
				pages.add(PAGE_ADDRESS);
				pages.add(PAGE_POST);
				pages.add(PAGE_IMAGE_GALLERY);

				storeAdapter=new StorePageAdapter(getSupportFragmentManager(), getFragments(),pages);
				storePager.setAdapter(storeAdapter);
//				pagerStoreTabs.setViewPager(storePager);
//				pagerStoreTabs.setBackgroundColor(Color.TRANSPARENT);
//				pagerStoreTabs.setTextColor(Color.WHITE);
//				pagerStoreTabs.setIndicatorColor(Color.WHITE);
//				pagerStoreTabs.setIndicatorHeight(5);

				storePager.setOffscreenPageLimit(storeAdapter.getCount());

//				pagerStoreTabs.setOnPageChangeListener(this);
				storePager.setOnPageChangeListener(this);

				if(session.isLoggedInCustomer())
				{
					getLike=true;
					getPerticularStoreDetails();
				}
				else
				{
					getPerticularStoreDetails();
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		//Flurry map initialize
		view = new HashMap<String, String>();


		isEventFired = false;


		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, shareText);


		final PackageManager pm = context.getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}

		/** Anirudh- Custom share dialog */
		dialogShare = new Dialog(context);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(context, 0, context.getLayoutInflater(), appName, packageNames, appIcon));



	}//onCreate Ends here


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try
		{
			//			if(session.isLoggedIn())
			//			{
			//			MenuItem extra=menu.add("Reports");
			//			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			//			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Reports"))
		{
			Intent intent=new Intent(context,Reports.class);
			intent.putExtra("STORE_NAME", REC_SOTRE_NAME);
			intent.putExtra("STORE_LOGO", PHOTO);
			intent.putExtra("STORE_ID", STORE_ID);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		else
		{
			clear();
			this.finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		return true;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		clear();
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	void clear()
	{
		SOURCE.clear();
		PHOTO="";
		USER_LIKE="-1";
		callnumbers.clear();
		ID.clear();
		STORE_ID="";
		REC_SOTRE_NAME="";	
		STREET="";
		LANDMARK="";
		DESCRIPTION="";

		STORE="";
		MOBILE_NO="";

		TELNO1="";
		TELNO2="";
		TELNO3="";

		MOBNO2="";
		MOBNO3="";

		PHOTO="";
		CITY="";
		AREA="";
		FB="";
		TWITTER="";

		LATITUDE_PLACE="0";
		LONGITUDE_PLACE="0";

		OPEN_TIME="";
		CLOSE_TIME="";

		FB="";
		TWITTER="";
		WEBSITE="";
		EMAIL="";

		latitued="";
		longitude="";
		isLocationFound=false;

		contactno="";
		mapLink="";
		mapLinkLable="";


	}



	private List<Fragment> getFragments()
	{
		List<Fragment> fList = new ArrayList<Fragment>();

		fList.add(StorePageFragment.newInstance(PAGE_ABOUT_STORE));
		fList.add(StorePageFragment.newInstance(PAGE_ADDRESS));
		fList.add(StorePageFragment.newInstance(PAGE_POST));
		fList.add(StorePageFragment.newInstance(PAGE_IMAGE_GALLERY));

		return fList;

	}


	private class StorePageAdapter extends FragmentStatePagerAdapter
	{

		ArrayList<String> pages;
		List<Fragment>fragments;
		public StorePageAdapter(FragmentManager fm,List<Fragment>fragments,ArrayList<String> pages) {
			super(fm);
			// TODO Auto-generated constructor stub

			this.pages=pages;
			this.fragments=fragments;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return fragments.size();
		}

	}

	public static class StorePageFragment extends Fragment
	{
		String Page;
		public static final String EXTRA_MESSAGE = "PAGE";
		View view;

		//About Store
		ImageView pagerLogo;
		TextView pagerOpenCloseTime;
		TextView pagerOpenCloseDays;
		TextView pagerStoreDesc;
		ImageView pagerStoreCall;
		ImageView pagerStoreFav;
		ImageView pagerStoreShare;

		//PAGE_POST
		ListView pagerPostList;

		//Address
		TextView pagerAddress;
		ImageView pagerLocate;
		TextView pagerPhoneNumbers;
		TextView pagerEmail;
		ImageView pagerFacebook;
		ImageView pagerTwitter;
		ImageView pagerWebsite;
//		ImageView storePagerMapImage;
//		Button buttonTakeMeThere;
		//Pager Gallery
		GridView grid;

		Map<String, String> call= new HashMap<String, String>();
		Map<String, String> facebook = new HashMap<String, String>();
		Map<String, String> twitter = new HashMap<String, String>();;
		Map<String, String> website = new HashMap<String, String>();
		Map<String, String> email = new HashMap<String, String>();
		Map<String, String> viewmap = new HashMap<String, String>();
		Map<String, String> directions = new HashMap<String, String>();
		Map<String, String> viewoffer = new HashMap<String, String>();
		//Map<String, String> viewgallery = new HashMap<String, String>();


		public StorePageFragment()
		{
		}

		public static final StorePageFragment newInstance(String Page)
		{
			StorePageFragment f=new StorePageFragment();
			Bundle bdl=new Bundle(1);
			bdl.putString(EXTRA_MESSAGE, Page);
			f.setArguments(bdl);
			return f;

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			String page = getArguments().getString(EXTRA_MESSAGE);

			if(page.equalsIgnoreCase(PAGE_ABOUT_STORE))
			{
				view=inflater.inflate(R.layout.store_details_about, null);
				pagerStoreCall=(ImageView)view.findViewById(R.id.pagerStoreCall);
				pagerStoreFav=(ImageView)view.findViewById(R.id.pagerStoreFav);
				pagerStoreShare=(ImageView)view.findViewById(R.id.pagerStoreShare);

				pagerStoreCall.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						
						if(callnumbers.size()==0)
						{
							showToast("No contact numbers!!");	
						}
						else
						{

							Map<String, String> call= new HashMap<String, String>();

							call.put("AreaName",AREA);
							call.put("Source","StoreDetails");
							EventTracker.logEvent("StoreCall", call);

							calldailog.show();
						}
						
					}
				});



				listCall.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						try{

							call.put("AreaName",AREA);
							call.put("Source","StoreDetails");
							EventTracker.logEvent("StoreCallDone", call);

							Intent call = new Intent(android.content.Intent.ACTION_DIAL);
							call.setData(Uri.parse("tel:"+callnumbers.get(position)));
							startActivity(call);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							calldailog.dismiss();

						}
						catch(Exception e){
							e.printStackTrace();
						}

					}
				});

				pagerStoreFav.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						// TODO Auto-generated method stub
						try
						{

							if(session.isLoggedInCustomer())
							{
								//								getLike=true;
								//								likePlace=true;
								if(USER_LIKE.equalsIgnoreCase("-1"))
								{
									getPerticularStoreDetails();
								}
								else if(USER_LIKE.equalsIgnoreCase("1"))
								{
									unLikePlace();
								}
								else if(USER_LIKE.equalsIgnoreCase("0"))
								{
									likePlace();
									//									likePlace=false;
									//									getLike=false;
								}
							}
							else
							{

								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
								alertDialogBuilder.setTitle("Store Details");
								alertDialogBuilder
								.setMessage(getResources().getString(R.string.placeFavAlert))
								.setIcon(R.drawable.ic_launcher)
								.setCancelable(false)
								.setPositiveButton("Login",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {

										dialog.dismiss();
										Intent intent=new Intent(context,LoginSignUpCustomer.class);
										context.startActivityForResult(intent,2);
										context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

									}
								})
								.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										dialog.dismiss();
									}
								})
								;

								AlertDialog alertDialog = alertDialogBuilder.create();

								alertDialog.show();

							}

						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});

				pagerStoreShare.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						// TODO Auto-generated method stub
						try
						{



							if(MOBILE_NO.length()>0)
							{
								contactno=MOBILE_NO;
							} 
							if(MOBNO2.length()>0)
							{
								contactno+=" +"+MOBNO2;
							}
							if(MOBNO3.length()>0)
							{
								contactno+=" +"+MOBNO3;
							}
							if(TELNO1.length()>0)
							{
								contactno+=" "+TELNO1;
							}
							if(TELNO2.length()>0)
							{
								contactno+=" "+TELNO2;
							}
							if(TELNO3.length()>0)
							{
								contactno+=" "+TELNO3;
							}

							if(!LATITUDE_PLACE.equalsIgnoreCase("0") && !LONGITUDE_PLACE.equalsIgnoreCase("0"))
							{
								mapLinkLable="\non Google Map : \n";
								/*mapLink="http://maps.google.com/maps?saddr="+LATITUDE_PLACE+","+LONGITUDE_PLACE;*/
								mapLink="http://maps.google.com/?q="+LATITUDE_PLACE+","+LONGITUDE_PLACE;
								Log.i("MAP LINK", "MAP LINK "+mapLink);
							}

							try
							{


								//								Intent sendIntent = new Intent();
								//								sendIntent.setType("text/plain");
								//								sendIntent.setAction(Intent.ACTION_SEND);
								//								sendIntent.putExtra(Intent.EXTRA_SUBJECT,"" );
								//								sendIntent.putExtra(Intent.EXTRA_TEXT,REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+contactno+"\nCity : "+CITY+"\nTimings : "+OPEN_TIME+" To "+CLOSE_TIME+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink);
								//								context.startActivityForResult(sendIntent,11);


								// TODO Auto-generated method stub

								/*
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
								alertDialogBuilder.setTitle("Share");
								alertDialogBuilder
								.setMessage("Share using")
								.setIcon(R.drawable.ic_launcher)
								.setCancelable(true)
								.setPositiveButton("Other",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {

										dialog.dismiss();

										List<Intent> targetedShareIntents = new ArrayList<Intent>();
										Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
										sharingIntent.setType("text/plain");
										String shareBody = REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+contactno+"\nCity : "+CITY+"\nTimings : "+OPEN_TIME+" To "+CLOSE_TIME+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink;

										PackageManager pm = context.getPackageManager();
										List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

										for(final ResolveInfo app : activityList) {

											String packageName = app.activityInfo.packageName;
											Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
											targetedShareIntent.setType("text/plain");

											if(TextUtils.equals(packageName, "com.facebook.katana")){

											} else {
												targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
												targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"");
												targetedShareIntent.setPackage(packageName);
												targetedShareIntents.add(targetedShareIntent);
											}

										}

										Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
										chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
										context.startActivityForResult(chooserIntent,11);

									}
								})
								.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										dialog.dismiss();

										String shareBody = REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+contactno+"\nCity : "+CITY+"\nTimings : "+OPEN_TIME+" To "+CLOSE_TIME+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink;

										if(PHOTO.length()!=0)
										{
											String photo_source=PHOTO.replaceAll(" ", "%20");

											FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
											.setLink("http://shoplocal.co.in")
											.setName(REC_SOTRE_NAME)
											.setPicture(PHOTO_PARENT_URL+photo_source)
											.setDescription(shareBody)
											.build();
											uiHelper.trackPendingDialogCall(shareDialog.present());
										}
										else
										{
											FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
											.setLink("http://shoplocal.co.in")
											.setName(REC_SOTRE_NAME)
											.setDescription(shareBody)
											.build();
											uiHelper.trackPendingDialogCall(shareDialog.present());
										}
									}
								})
								;

								AlertDialog alertDialog = alertDialogBuilder.create();

								alertDialog.show();*/

								showDialog();




							}catch(Exception ex)
							{
								ex.printStackTrace();
							}






							/*						txtAddress.setText(STREET+" "+LANDMARK+"\n"+AREA);
						txtTimings.setText("Timings "+time_open+" To "+time_close);*/


						}catch(Exception ex)
						{
							ex.printStackTrace();
						}

					}
				});



			}

			if(page.equalsIgnoreCase(PAGE_ADDRESS))
			{
				view=inflater.inflate(R.layout.store_details_address, null);

				pagerAddress=(TextView)view.findViewById(R.id.pagerAddress);
				pagerLocate=(ImageView)view.findViewById(R.id.pagerLocate);
				pagerPhoneNumbers=(TextView)view.findViewById(R.id.pagerPhoneNumbers);
				pagerEmail=(TextView)view.findViewById(R.id.pagerEmail);
				pagerFacebook=(ImageView)view.findViewById(R.id.pagerFacebook);
				pagerTwitter=(ImageView)view.findViewById(R.id.pagerTwitter);
				pagerWebsite=(ImageView)view.findViewById(R.id.pagerWebsite);

//				storePagerMapImage=(ImageView)store_mapDialog.findViewById(R.id.storePagerMapImage);
//				buttonTakeMeThere=(Button)store_mapDialog.findViewById(R.id.buttonTakeMeThere);

				pagerPhoneNumbers.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(callnumbers.size()==0)
						{
							showToast("No contact numbers!!");	
						}
						else
						{
							Map<String, String> call= new HashMap<String, String>();

							call.put("AreaName",AREA);
							call.put("Source","StoreDetails");
							EventTracker.logEvent("StoreCall", call);

							calldailog.show();
						}
					}
				});


				listCall.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {

						// TODO Auto-generated method stub
						call.put("AreaName",AREA);
						call.put("Source","StoreDetails");
						EventTracker.logEvent("StoreCallDone", call);

						Intent call = new Intent(android.content.Intent.ACTION_DIAL);
						call.setData(Uri.parse("tel:"+callnumbers.get(position)));
						startActivity(call);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						calldailog.dismiss();
					}
				});


//				buttonTakeMeThere.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View arg0) {
//						// TODO Auto-generated method stub
//						try
//						{
//							if(!LONGITUDE_PLACE.equalsIgnoreCase("0") && !LATITUDE_PLACE.equalsIgnoreCase("0"))
//							{
//								if(isLocationFound)
//								{
//									if(AREA != null || REC_SOTRE_NAME != null){
//										directions.put("AreaName",AREA);
//										directions.put("StoreName",REC_SOTRE_NAME);
//										EventTracker.logEvent("StoreDetail_Directions", directions);
//									}
//
//									/*String uri = "http://maps.google.com/maps?saddr=" + LAT +","+ LONG +"&daddr="+ latitued +","+ longitude;*/
//									String uri = "http://maps.google.com/maps?saddr=" + latitued +","+ longitude +"&daddr="+ LATITUDE_PLACE +","+ LONGITUDE_PLACE;
//									Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
//									intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//									startActivity(intent);
//									store_mapDialog.dismiss();
//								}
//								else
//								{
//									showToast(getResources().getString(R.string.locationAlert));
//								}
//							}
//
//						}catch(ActivityNotFoundException ac)
//						{
//							ac.printStackTrace();
//						}
//						catch(Exception ex)
//						{
//							ex.printStackTrace();
//						}
//					}
//				});

				Log.i("LAT LONG ", "LAT LONG "+LATITUDE_PLACE+" , "+LONGITUDE_PLACE);
				if(!LONGITUDE_PLACE.equalsIgnoreCase("0") && !LATITUDE_PLACE.equalsIgnoreCase("0"))
				{
					Log.i("LAT LONG ", "LAT LONG true "+LATITUDE_PLACE+" , "+LONGITUDE_PLACE);
					pagerLocate.setVisibility(View.VISIBLE);
				}
				else
				{
					Log.i("LAT LONG ", "LAT LONG false "+LATITUDE_PLACE+" , "+LONGITUDE_PLACE);
					pagerLocate.setVisibility(View.GONE);
				}

				pagerLocate.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(!LONGITUDE_PLACE.equalsIgnoreCase("0") && !LATITUDE_PLACE.equalsIgnoreCase("0"))
						{
							if(AREA != null || REC_SOTRE_NAME != null){
								viewmap.put("AreaName",AREA);
								viewmap.put("StoreName",REC_SOTRE_NAME);
								EventTracker.logEvent("StoreDetail_ViewMap", viewmap);
							}

							/*			viewmap.put("AreaName",AREA);
							viewmap.put("StoreName",REC_SOTRE_NAME);
							EventTracker.logEvent("StoreDetail_ViewMap", viewmap);*/

							directions.put("AreaName",AREA);
							directions.put("StoreName",REC_SOTRE_NAME);
							EventTracker.logEvent("StoreDetail_Directions", directions);

							try
							{
								if(!LONGITUDE_PLACE.equalsIgnoreCase("0") && !LATITUDE_PLACE.equalsIgnoreCase("0"))
								{
									if(isLocationFound)
									{
										directions.put("AreaName",AREA);
										directions.put("StoreName",REC_SOTRE_NAME);
										EventTracker.logEvent("StoreDetail_Directions", directions);

										/*String uri = "http://maps.google.com/maps?saddr=" + LAT +","+ LONG +"&daddr="+ latitued +","+ longitude;*/
										String uri = "http://maps.google.com/maps?saddr=" + latitued +","+ longitude +"&daddr="+ LATITUDE_PLACE +","+ LONGITUDE_PLACE;
										Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
										intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
										startActivity(intent);
										/*store_mapDialog.dismiss();*/
									}
									else
									{
										showToast(getResources().getString(R.string.locationAlert));
									}
								}

							}catch(ActivityNotFoundException ac)
							{
								ac.printStackTrace();
							}
							catch(Exception ex)
							{
								ex.printStackTrace();
							}

							/*store_mapDialog.show();*/
						}
						else
						{
							showToast("Store is not located on Map");
						}
					}
				});

				pagerEmail.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(EMAIL.length()!=0)
						{
							if(AREA != null || REC_SOTRE_NAME != null){
								email.put("AreaName",AREA);
								email.put("StoreName",REC_SOTRE_NAME);
								EventTracker.logEvent("StoreDetail_Email", email);
							}

							Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
									"mailto",EMAIL, null));
							emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
							context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
						}
						else
						{
							showToast("No email address");
						}
					}
				});

				pagerFacebook.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(FB.length()!=0)
						{
							if(AREA != null || REC_SOTRE_NAME != null){
								facebook.put("AreaName",AREA);
								facebook.put("StoreName",REC_SOTRE_NAME);
								EventTracker.logEvent("StoreDetail_Facebook", facebook);
							}

							Intent intent=new Intent(context,AboutWebView.class);
							intent.putExtra("url", "SocialURL");
							intent.putExtra("SocialURL", FB);
							intent.putExtra("isFb", true);
							context.startActivity(intent);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
						else
						{
							showToast("Facebook not set");
						}
					}
				});

				pagerTwitter.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(TWITTER.length()!=0)
						{
							if(AREA != null || REC_SOTRE_NAME != null){
								twitter.put("AreaName",AREA);
								twitter.put("StoreName",REC_SOTRE_NAME);
								EventTracker.logEvent("StoreDetail_Twitter", twitter);
							}

							Intent intent=new Intent(context,AboutWebView.class);
							intent.putExtra("url", "SocialURL");
							intent.putExtra("SocialURL", TWITTER);
							intent.putExtra("isFb", false);
							context.startActivity(intent);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
						else
						{
							showToast("Twitter not set");
						}
					}
				});

				pagerWebsite.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(WEBSITE.length()!=0)
						{
							if(AREA != null || REC_SOTRE_NAME != null){
								website.put("AreaName",AREA);
								website.put("StoreName",REC_SOTRE_NAME);
								EventTracker.logEvent("StoreDetail_Website", website);
							}
							Intent intent=new Intent(context,AboutWebView.class);
							intent.putExtra("url", "Web");
							intent.putExtra("Web", WEBSITE);
							context.startActivity(intent);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
						else
						{
							showToast("Website not set");
						}
					}
				});


			}

			if(page.equalsIgnoreCase(PAGE_POST))
			{
				view=inflater.inflate(R.layout.store_details_posts, null);
				pagerPostList=(ListView)view.findViewById(R.id.pagerPostList);

			}

			if(page.equalsIgnoreCase(PAGE_IMAGE_GALLERY))
			{
				view=inflater.inflate(R.layout.store_details_photo_gallery, null);
				grid=(GridView)view.findViewById(R.id.pagerGalleryGrid);

				grid.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub

						if(SOURCE.size()!=0)
						{
							ArrayList<String> url=new ArrayList<String>();
							url.add(" ");
							for(int i=0;i<SOURCE.size();i++)
							{
								url.add(SOURCE.get(i));
							}

							Intent intent=new Intent(context,StorePagerGallery.class);
							intent.putStringArrayListExtra("photo_source", url);
							intent.putExtra("position", position+1);
							context.startActivity(intent);
							context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
				});
			}




			return view;
		}

		void showDialog(){

			try
			{
				dialogShare.show();

				listViewShare.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {

						String app = appName.get(position);
						/*					Toast.makeText(context, app, Toast.LENGTH_LONG).show();*/
						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

							dialogShare.dismiss();

							String contactLable="\nContact : +";
							if(contactno.length()==0)
							{
								contactLable="";
							}

							String shareBody = REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+" "+CITY+contactLable+contactno+"\nTimings : "+OPEN_TIME+" To "+CLOSE_TIME+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink+"(Shared via: http://www.shoplocal.co.in )";

							if(PHOTO.length()!=0)
							{
								String photo_source=PHOTO.replaceAll(" ", "%20");

								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(REC_SOTRE_NAME)
								.setPicture(PHOTO_PARENT_URL+photo_source)
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
							}
							else
							{
								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(REC_SOTRE_NAME)
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
							}
						}
						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
							Toast.makeText(context, "Looks like you dont have facebook installed in your device!", Toast.LENGTH_LONG).show();
						}

						if(!app.equalsIgnoreCase("facebook")){
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setPackage(packageNames.get(position));
							i.setType("text/plain");
							//i.putExtra(Intent.EXTRA_TEXT, shareText);
							//String shareBody = REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+contactno+"\nCity : "+CITY+"\nTimings : "+OPEN_TIME+" To "+CLOSE_TIME+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink;
							String contactLable="\nContact : +";
							if(contactno.length()==0)
							{
								contactLable="";
							}

							String shareBody = REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+CITY+contactLable+contactno+"\nTimings : "+OPEN_TIME+" To "+CLOSE_TIME+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink+"(Shared via: http://www.shoplocal.co.in )";
							i.putExtra(Intent.EXTRA_SUBJECT, REC_SOTRE_NAME);
							i.putExtra(Intent.EXTRA_TEXT, shareBody);
							context.startActivityForResult(i,11);
						}

						dismissDialog();

					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		void dismissDialog(){
			dialogShare.dismiss();
		}



	}//Fragment Ends here



	void getLocationFromPrefs()
	{
		SharedPreferences pref=context.getSharedPreferences("location",0);
		latitued=pref.getString("latitued", "0");
		longitude=pref.getString("longitude", "0");
		isLocationFound=pref.getBoolean("isLocationFound",false);

		Log.i("Location found","Location found "+isLocationFound+" "+latitued+","+longitude);
	}

	void setImage(String latitude,String longitude)
	{
		try
		{
			Log.i("LAT LONG ", "setImage LAT LONG "+latitude+" , "+longitude);
			if(!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
			{
//				DisplayMetrics displaymetrics = new DisplayMetrics();
//				context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//				int width = displaymetrics.widthPixels;
//
//				String url="http://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&zoom=16&size="+300+"x"+150+"&maptype=roadmap&markers=color:blue|label:S|"+latitude+","+longitude+"&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g";
//				store_MapImage.setScaleType(ScaleType.FIT_XY);
//				store_MapImage.setAdjustViewBounds(true);
//				/*imageLoaderList.DisplayImage(url, store_MapImage);*/
//				try{
//					Picasso.with(context).load(url)
//					.placeholder(R.drawable.ic_place_holder)
//					.error(R.drawable.ic_place_holder)
//					.into(store_MapImage);
//				}
//				catch(IllegalArgumentException illegalArg){
//					illegalArg.printStackTrace();
//				}
//				catch(Exception e){ 
//					e.printStackTrace();
//				}


//				Log.i("MAP URL", "MAP URL "+url);
				ImageView pagerLocate=(ImageView)storePager.findViewById(R.id.pagerLocate);

				Log.i("LAT LONG ", "LAT LONG "+LATITUDE_PLACE+" , "+LONGITUDE_PLACE);
				if(!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0"))
				{
					Log.i("LAT LONG ", "LAT LONG true "+latitude+" , "+longitude);
					pagerLocate.setVisibility(View.VISIBLE);
				}
				else
				{
					Log.i("LAT LONG ", "LAT LONG false "+latitude+" , "+longitude);
					pagerLocate.setVisibility(View.GONE);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			if(requestCode==2)
			{

				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

				if(session.isLoggedInCustomer())
				{
					getLike=true;
					likePlace=true;
					getPerticularStoreDetails();
				}
				else
				{

				}
			}
			if(requestCode==5)
			{
				if(data==null)
				{
					//showToast("Data is null");
					Log.i("DATA","Data is null");
				}
				else
				{
					//("Data is not null");
					Log.i("DATA","Data is not null");
				}
				//showToast("Got Result");

				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				if(session.isLoggedInCustomer())
				{
					getPostLike=true;
					//					loadBroadCast();
				}

			}
			if(requestCode==11)
			{
				Log.i("", "");
				callShareApi("Android");
			}


			try
			{
				uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
					@Override
					public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
						Log.e("Activity", String.format("Error: %s", error.toString()));
					}

					@Override
					public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
						Log.i("Activity", "Success!");

						callShareApi("Android-Facebook");
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


		}catch(NullPointerException ex)
		{
			ex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	private void callShareApi(String Via) {

		if(AREA != null || REC_SOTRE_NAME != null){
			share.put("AreaName",AREA);
			share.put("StoreName",REC_SOTRE_NAME);
			EventTracker.logEvent("StoreDetail_Share", share);
		}
		Intent intent = new Intent(context, PlaceShareService.class);
		intent.putExtra("placeShareService", placeShare);
		intent.putExtra("URL",PLACE_SHARE_URL + PLACE_SHARE_API);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		/*	intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);*/
		intent.putExtra("place_id", STORE_ID);
		intent.putExtra("via", Via);
		context.startService(intent);


	}


	static void likePlace()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			if(AREA != null || REC_SOTRE_NAME != null){
				favourite.put("AreaName",AREA);
				favourite.put("StoreName",REC_SOTRE_NAME);
				EventTracker.logEvent("StoreDetail_Favourite", favourite);
			}
			Intent intent=new Intent(context, FavouritePlaceService.class);
			intent.putExtra("favourite",mFavourite);
			intent.putExtra("URL", GET_STORE_URL+LIKE_API);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("place_id", STORE_ID);
			context.startService(intent);
			storeProgress.setVisibility(View.VISIBLE);

		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	static void unLikePlace()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			if(AREA != null || REC_SOTRE_NAME != null){
				unfavourite.put("AreaName",AREA);
				unfavourite.put("StoreName",REC_SOTRE_NAME);
				EventTracker.logEvent("StoreDetail_UnFavourite", unfavourite);
			}

			Intent intent=new Intent(context, UnFavouritePlaceService.class);
			intent.putExtra("unfavourite",mUnFavourite);
			intent.putExtra("URL", GET_STORE_URL+LIKE_API);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("place_id", STORE_ID);
			context.startService(intent);
			storeProgress.setVisibility(View.VISIBLE);

		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	//Call Service to get perticular store details
	static void getPerticularStoreDetails()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			/*showToast("Service Started");*/
			Intent intent_GetStore=new Intent(context, GetPerticularStoreDetailService.class);
			intent_GetStore.putExtra("getStore",mGetStoreReceiver);
			intent_GetStore.putExtra("URL", GET_STORE_URL+GET_STORE);
			intent_GetStore.putExtra("api_header",API_HEADER);
			intent_GetStore.putExtra("api_header_value", API_VALUE);
			intent_GetStore.putExtra("store_id", STORE_ID);
			if(getLike)
			{
				intent_GetStore.putExtra("getLike", getLike);
				intent_GetStore.putExtra("user_id",USER_ID);
			}
			/*		intent.putExtra("business_id", BUSINESS_ID);*/
			context.startService(intent_GetStore);
			//relOffersProgress.setVisibility(View.VISIBLE);
			storeProgress.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	static void loadGallery()
	{
		if(isnetConnected.isNetworkAvailable())
		{

			if(AREA != null || REC_SOTRE_NAME != null){
				viewgallery.put("AreaName",AREA);
				viewgallery.put("StoreName",REC_SOTRE_NAME);
				EventTracker.logEvent("StoreDetail_ViewGallery", viewgallery);
			}


			//Toast.makeText(context, "Loading Gallery", Toast.LENGTH_SHORT).show();
			Intent intent_gallery=new Intent(context, LoadStoreGallery.class);
			intent_gallery.putExtra("loadgall",mReceiver);
			intent_gallery.putExtra("URL", STORE_GALLERY_URL+STORE_GALLERY);
			intent_gallery.putExtra("api_header",API_HEADER);
			intent_gallery.putExtra("api_header_value", API_VALUE);
			/*			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);*/
			intent_gallery.putExtra("store_id", STORE_ID);
			context.startService(intent_gallery);
			/*progUpload.setVisibility(View.VISIBLE);*/
			storeProgress.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	static void loadBroadCast()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent_loadBroadCast=new Intent(context, BroadCastService.class);
			intent_loadBroadCast.putExtra("broadcast",mBroadCast);
			intent_loadBroadCast.putExtra("URL", STORE_URL+GET_BROADCAST_URL);
			intent_loadBroadCast.putExtra("api_header",API_HEADER);
			intent_loadBroadCast.putExtra("api_header_value", API_VALUE);
			intent_loadBroadCast.putExtra("store_id", STORE_ID);
			if(getPostLike)
			{
				intent_loadBroadCast.putExtra("getLike",getPostLike);
				intent_loadBroadCast.putExtra("user_id",USER_ID);
			}
			context.startService(intent_loadBroadCast);

			storeProgress.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}

	static void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub


		try
		{
			storeProgress.setVisibility(View.GONE);

			String store_status=resultData.getString("store_status");

			String ID;
			String REC_BUSINESS_ID;

			String BUILDING;

			String PINCODE;

			String STATE;
			String COUNTRY;


			String FAX1;
			String FAX2;
			String TOLLFREE1;	
			String TOLLFREE2;
			//			String PHOTO;



			String TEMP_USER_LIKE;



			if(store_status.equalsIgnoreCase("true"))
			{



				final ArrayList<String> Time1_OPEN=resultData.getStringArrayList("TIME1_OPEN");
				final ArrayList<String> Time1_CLOSE=resultData.getStringArrayList("TIME1_CLOSE");
				ArrayList<String> PLACE_STATUS=resultData.getStringArrayList("PLACE_STATUS");
				ArrayList<String> DAY=resultData.getStringArrayList("DAY");

				ID=resultData.getString("ID");
				REC_BUSINESS_ID=resultData.getString("BUSINESS_ID");
				REC_SOTRE_NAME=resultData.getString("SOTRE_NAME");



				BUILDING=resultData.getString("BUILDING");
				STREET=resultData.getString("STREET");
				LANDMARK=resultData.getString("LANDMARK");
				AREA=resultData.getString("AREA");

				PINCODE=resultData.getString("PINCODE");
				CITY=resultData.getString("CITY");
				STATE=resultData.getString("STATE");
				COUNTRY=resultData.getString("COUNTRY");
				LATITUDE_PLACE=resultData.getString("LATITUDE");
				LONGITUDE_PLACE=resultData.getString("LONGITUDE");

				TELNO1=resultData.getString("TELNO1");
				TELNO2=resultData.getString("TELNO2");
				TELNO3=resultData.getString("TELNO3");
				MOBILE_NO=resultData.getString("MOBILE_NO");
				MOBNO2=resultData.getString("MOBNO2");
				MOBNO3=resultData.getString("MOBNO3");
				FB=resultData.getString("FACEBOOK");
				TWITTER=resultData.getString("TWITTER");
				TOLLFREE1=resultData.getString("TOLLFREE1");
				TOLLFREE2=resultData.getString("TOLLFREE2");
				PHOTO=resultData.getString("PHOTO");
				DESCRIPTION=resultData.getString("DESCRIPTION");
				EMAIL=resultData.getString("EMAIL");
				WEBSITE=resultData.getString("WEBSITE");
				USER_LIKE=resultData.getString("USER_LIKE");

				Log.i("STORE LAT LONG", "STORE LAT LONG "+LATITUDE_PLACE+" , "+LONGITUDE_PLACE);

				//actionBar.setTitle(REC_SOTRE_NAME);
				tvActionBarTitle.setText(REC_SOTRE_NAME);

				/** Check if actionbar title lines is > 1 and reduce text size */
				tvActionBarTitle.post(new Runnable() {

					@Override
					public void run() {

						int lineCount    = tvActionBarTitle.getLineCount();
						if(lineCount > 1){
							tvActionBarTitle.setTextSize(15.0f);
						}
					}
				});

				setImage(LATITUDE_PLACE,LONGITUDE_PLACE);




				view.put("AreaName",AREA);
				view.put("StoreName", REC_SOTRE_NAME);
				EventTracker.logEvent("StoreDetail_View", view);

				for(int i=0;i<DAY.size();i++)
				{
					if(DAY.get(i).equalsIgnoreCase("SUNDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{
							//closed
						}

					}
					if(DAY.get(i).equalsIgnoreCase("MONDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{

						}
					}
					if(DAY.get(i).equalsIgnoreCase("TUESDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{

						}
					}
					if(DAY.get(i).equalsIgnoreCase("WEDNESDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{

						}
					}
					if(DAY.get(i).equalsIgnoreCase("THURSDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{

						}
					}
					if(DAY.get(i).equalsIgnoreCase("FRIDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{

						}
					}
					if(DAY.get(i).equalsIgnoreCase("SATURDAY"))
					{
						if(PLACE_STATUS.get(i).equalsIgnoreCase("0"))
						{
							//open
						}
						else
						{

						}
					}
				}



				if(MOBILE_NO.length()>5)
				{
					callnumbers.add("+"+MOBILE_NO);
				}
				if(MOBNO2.length()>5)
				{
					callnumbers.add("+"+MOBNO2);
				}
				if(MOBNO3.length()>5)
				{
					callnumbers.add("+"+MOBNO3);
				}


				if(TELNO1.length()>5)
				{
					callnumbers.add(TELNO1);
				}
				if(TELNO2.length()>5)
				{
					callnumbers.add(TELNO2);
				}
				if(TELNO3.length()>5)
				{
					callnumbers.add(TELNO3);
				}

				listCall.setAdapter(new CallListDialog(context, 0, callnumbers));

				//				storeShare.setOnClickListener(new OnClickListener() {
				//
				//					@Override
				//					public void onClick(View arg0) {
				//						// TODO Auto-generated method stub
				//						try
				//						{
				//							String contactno="";
				//							String mapLink="";
				//							String mapLinkLable="";
				//
				//							if(MOBILE_NO.length()>0)
				//							{
				//								contactno="\nContact Number : "+MOBILE_NO;
				//							}
				//							else
				//							{
				//								contactno="";
				//							}
				//							if(!LATITUDE_PLACE.equalsIgnoreCase("0") && !LONGITUDE_PLACE.equalsIgnoreCase("0"))
				//							{
				//								mapLinkLable="\nFind us on Google Map : \n";
				//								/*mapLink="http://maps.google.com/maps?saddr="+LATITUDE_PLACE+","+LONGITUDE_PLACE;*/
				//								mapLink="http://maps.google.com/?q="+LATITUDE_PLACE+","+LONGITUDE_PLACE;
				//								Log.i("MAP LINK", "MAP LINK "+mapLink);
				//							}
				//
				//							try
				//							{
				//								//Open Time
				//								String time =Time1_OPEN.get(0);
				//
				//								DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
				//								Date date = sdf.parse(time);
				//
				//								System.out.println("Time Converted open: " + sdf.format(date));
				//
				//								DateFormat time_open=new SimpleDateFormat("hh:mm aa");
				//								System.out.println("Time Converted open: " + time_open.format(date));
				//
				//								/*txtMangeStoreTimeFrom.setText(time_open.format(date));*/
				//
				//
				//								//Close Time
				//								String time2 = Time1_CLOSE.get(0);
				//
				//								DateFormat sdf2 = new SimpleDateFormat("hh:mm:ss");
				//								Date date2 = sdf.parse(time2);
				//
				//								System.out.println("Time Converted Close: " + sdf2.format(date));
				//
				//								DateFormat time_closed=new SimpleDateFormat("hh:mm aa");
				//								System.out.println("Time Converted Close: " + time_closed.format(date2));
				//
				//								/*txtMangeStoreTimeTo.setText(time_closed.format(date2));*/
				//
				//								Intent sendIntent = new Intent();
				//								sendIntent.setType("text/plain");
				//								sendIntent.setAction(Intent.ACTION_SEND);
				//								// sendIntent.putExtra(Intent.EXTRA_SUBJECT, txtBroadCastTitle.getText().toString());
				//
				//								sendIntent.putExtra(Intent.EXTRA_SUBJECT,"" );
				//								sendIntent.putExtra(Intent.EXTRA_TEXT,REC_SOTRE_NAME+"\nAddress : "+STREET+" "+LANDMARK+"\n"+AREA+contactno+"\nCity : "+CITY+"\nTimings : "+time_open.format(date)+" To "+time_closed.format(date2)+"\nDescription : "+DESCRIPTION+mapLinkLable+mapLink);
				//								startActivityForResult(sendIntent,11);
				//
				//
				//							}catch(Exception ex)
				//							{
				//								ex.printStackTrace();
				//							}
				//
				//
				//
				//
				//
				//
				//							/*						txtAddress.setText(STREET+" "+LANDMARK+"\n"+AREA);
				//						txtTimings.setText("Timings "+time_open+" To "+time_close);*/
				//
				//
				//						}catch(Exception ex)
				//						{
				//							ex.printStackTrace();
				//						}
				//					}
				//				});

				String time = "" ;
				String time2="";
				try
				{
					//Open Time
					//Open Time
					time =Time1_OPEN.get(0);

					DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
					Date date = sdf.parse(time);

					System.out.println("Time Converted open: " + sdf.format(date));

					DateFormat time_open=new SimpleDateFormat("hh:mm aa");
					System.out.println("Time Converted open: " + time_open.format(date));

					/*txtMangeStoreTimeFrom.setText(time_open.format(date));*/


					//Close Time
					time2 = Time1_CLOSE.get(0);

					DateFormat sdf2 = new SimpleDateFormat("hh:mm:ss");
					Date date2 = sdf.parse(time2);

					System.out.println("Time Converted Close: " + sdf2.format(date));

					DateFormat time_closed=new SimpleDateFormat("hh:mm aa");
					System.out.println("Time Converted Close: " + time_closed.format(date2));

					OPEN_TIME=time_open.format(date);
					CLOSE_TIME=time_closed.format(date2);

					/*txtMangeStoreTimeTo.setText(time_closed.format(date2));*/

					if(getLike && USER_LIKE.equalsIgnoreCase("1"))
					{
						//						searchFav.setImageResource(R.drawable.ic_heart_filled);
						setDataToPager(PHOTO, time_open.format(date), time_closed.format(date2), "", DESCRIPTION, true);
					}
					else
					{
						//						searchFav.setImageResource(R.drawable.ic_heart_unfilled);
						setDataToPager(PHOTO, time_open.format(date), time_closed.format(date2), "", DESCRIPTION, false);
					}

					setAddressData(STREET, LANDMARK, AREA, CITY, MOBILE_NO,MOBNO2,MOBNO3,TELNO1,TELNO2,TELNO3, EMAIL);

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}



				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+PHOTO, imgSearchedStorePhoto);*/

				//If user has already liked the store 


				if(likePlace && USER_LIKE.equalsIgnoreCase("0"))
				{
					likePlace();
					likePlace=false;
					getLike=false;

				}
				else if(likePlace && USER_LIKE.equalsIgnoreCase("1"))
				{
					unLikePlace();
					likePlace=false;
					getLike=false;
				}



			}
			else if(store_status.equalsIgnoreCase("false"))
			{

				String store_msg=resultData.getString("MSG");
				Log.i("Socket", "Socket "+store_msg);
				storeProgress.setVisibility(View.GONE);
				showToast(store_msg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}
				//				if(store_msg.equalsIgnoreCase(getResources().getString(R.string.invalidAuth)))
				//				{
				//					/*session.logoutUser();
				//				finish();
				//				overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);*/
				//				}
			}
			else if(store_status.equalsIgnoreCase("error"))
			{
				String store_msg=resultData.getString("MSG");
				showToast(store_msg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	class CallListDialog extends ArrayAdapter<String>
	{
		ArrayList<String> callNumbers;
		Activity context;
		LayoutInflater inflator;

		public CallListDialog(Activity context, int resource, ArrayList<String> callNumbers) {
			super(context, resource, callNumbers);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();
			this.callNumbers=callNumbers;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return callNumbers.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			try
			{
				if(convertView==null)
				{
					CallListViewHolder holder=new CallListViewHolder();
					convertView=inflator.inflate(R.layout.calllistlayout, null);
					holder.contactNo=(TextView)convertView.findViewById(R.id.contactNo);

					convertView.setTag(holder);
				}

				CallListViewHolder hold=(CallListViewHolder) convertView.getTag();

				hold.contactNo.setText(callNumbers.get(position));
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return convertView;
		}



	}

	static class CallListViewHolder
	{
		TextView contactNo;
	}



	void setDataToPager(String photo_url,final String open_time,final String close_time,String open_close_status,final String store_desc,boolean isLike)
	{
		try
		{
			//About Store
			ImageView pagerLogo=(ImageView)storePager.findViewById(R.id.pagerLogo);
			TextView pagerOpenCloseTime=(TextView)storePager.findViewById(R.id.pagerOpenCloseTime);
			TextView pagerOpenCloseDays=(TextView)storePager.findViewById(R.id.pagerOpenCloseDays);
			TextView pagerStoreDesc=(TextView)storePager.findViewById(R.id.pagerStoreDesc);
			ImageView pagerStoreFav=(ImageView)storePager.findViewById(R.id.pagerStoreFav);
			//ImageView pagerStoreShare=(ImageView)storePager.findViewById(R.id.pagerStoreFav);

			//Address


			if(photo_url.length()!=0)
			{
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_url, pagerLogo);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_url)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(pagerLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				pagerLogo.setImageResource(R.drawable.ic_place_holder);
			}

			pagerOpenCloseTime.setText(open_time+" "+close_time);
			pagerOpenCloseDays.setText(open_close_status);
			pagerStoreDesc.setText(""+store_desc);

			if(isLike)
			{
				pagerStoreFav.setImageResource(R.drawable.ic_store_liked);
			}
			else
			{
				pagerStoreFav.setImageResource(R.drawable.ic_store_unliked);
			}


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setAddressData(String street,String landmark,String area,String city,String mob1,String mob2,String mob3,String tel1,String tel2,String tel3,String email)
	{
		try
		{
			TextView pagerAddress=(TextView)storePager.findViewById(R.id.pagerAddress);
			TextView pagerPhoneNumbers=(TextView)storePager.findViewById(R.id.pagerPhoneNumbers);
			TextView pagerEmail=(TextView)storePager.findViewById(R.id.pagerEmail);
			ImageView pagerSep1=(ImageView)storePager.findViewById(R.id.pagerSep1);

			pagerAddress.setText(street+" "+landmark+"\n"+area+"\n"+city);

			if(mob1.length()==0 && mob2.length()==0 && mob3.length()==0 && tel1.length()==0 && tel2.length()==0 && tel3.length()==0)
			{
				pagerPhoneNumbers.setVisibility(View.GONE);
				pagerSep1.setVisibility(View.GONE);
			}
			else
			{
				try
				{
					String contact_nos="";
					if(mob1.length()!=0)
					{
						contact_nos+="+"+mob1+"\n";
					}
					if(mob2.length()!=0)
					{
						contact_nos+="+"+mob2+"\n";
					}
					if(mob3.length()!=0)
					{
						contact_nos+="+"+mob3+"\n";
					}
					if(tel1.length()!=0)
					{
						contact_nos+=tel1+"\n";
					}
					if(tel2.length()!=0)
					{
						contact_nos+=tel2+"\n";
					}
					if(tel3.length()!=0)
					{
						contact_nos+=tel3;
					}

					pagerPhoneNumbers.setText(contact_nos);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}

			if(email.length()!=0)
			{
				pagerEmail.setText(email);
			}
			else
			{
				pagerEmail.setVisibility(View.GONE);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	@Override
	public void onReceiverBroadCasts(int resultCode, Bundle resultData) {

		// TODO Auto-generated method stub
		storeProgress.setVisibility(ViewGroup.GONE);
		String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");
		String message=resultData.getString("SEARCH_MESSAGE");
		Log.i("Status ", "Status "+SEARCH_STATUS);

		getPostLike=false;
		if(SEARCH_STATUS.equalsIgnoreCase("true"))
		{



			ArrayList<GetBroadCast>getBroadcast=resultData.getParcelableArrayList("boradcast");

			ID.clear();
			TITLE.clear();
			BODY.clear();
			TYPE.clear();
			PHOTO_SOURCE.clear();
			DATE.clear();
			POST_USER_LIKE.clear();
			TOTAL_LIKE.clear();



			for(int i=0;i<getBroadcast.size();i++)
			{
				ID.add(getBroadcast.get(i).getId());
				TITLE.add(getBroadcast.get(i).getTitle());
				TYPE.add(getBroadcast.get(i).getType());
				BODY.add(getBroadcast.get(i).getDescription());
				DATE.add(getBroadcast.get(i).getDate());
				POST_USER_LIKE.add(getBroadcast.get(i).getUser_like());
				TOTAL_LIKE.add(getBroadcast.get(i).getTotal_like());
				TOTAL_SHARE.add(getBroadcast.get(i).getTotal_share());
				PHOTO_SOURCE.add(getBroadcast.get(i).getImage_url1());
			}

			setPostAdapters();

			//Log.i("SIZE", "SIZE TITLE "+TITLE.size()+" DATE "+DATE.size()+" PHOTO_SOURCE "+PHOTO_SOURCE.size());



			//			SearchListAdapter adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, TITLE,PHOTO_SOURCE,DATE,TOTAL_LIKE,TOTAL_SHARE);
			//			broadCastList.setAdapter(adapter);
			//			broadCastList.setOnItemClickListener(new OnItemClickListener() {
			//
			//				@Override
			//				public void onItemClick(AdapterView<?> arg0, View arg1,
			//						int position, long arg3) {
			//					// TODO Auto-generated method stub
			//					Intent intent=new Intent(context, OffersBroadCastDetails.class);
			//					intent.putExtra("storeName", REC_SOTRE_NAME);
			//					intent.putExtra("title", TITLE.get(position));
			//					intent.putExtra("body", BODY.get(position));
			//					intent.putExtra("POST_ID", ID.get(position));
			//					intent.putExtra("PLACE_ID", STORE_ID);
			//					intent.putExtra("fromCustomer", true);
			//					intent.putExtra("mob1", mob1);
			//					intent.putExtra("mob2", mob2);
			//					intent.putExtra("mob3", mob3);
			//					intent.putExtra("tel1", tel1);
			//					intent.putExtra("tel2", tel2);
			//					intent.putExtra("tel3", tel3);
			//					intent.putExtra("isFromStoreDetails", true);
			//					intent.putExtra("photo_source", PHOTO_SOURCE.get(position));
			//					if(POST_USER_LIKE.size()==0)
			//					{
			//						intent.putExtra("USER_LIKE","-1");
			//					}else
			//					{
			//						intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
			//
			//					}
			//
			//					context.startActivityForResult(intent, 5);
			//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//
			//				}
			//			});


			/*}*/
			//adapter.notifyDataSetChanged();
		}
		//showToast(message);
		if(!SEARCH_STATUS.equalsIgnoreCase("true"))
		{
			//			broadCastListProgress.setVisibility(ViewGroup.GONE);
			//	showToast("PROBLEM "+message);
			Log.i("Status ", "Status INSIDE"+SEARCH_STATUS);
			//			broadCastList.setVisibility(View.GONE);
			showToast(message);
		}

	}

	void setPostAdapters()
	{
		try
		{
			ListView broadCastList=(ListView)storePager.findViewById(R.id.pagerPostList);

			SearchListAdapter adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, TITLE,PHOTO_SOURCE,DATE,TOTAL_LIKE,TOTAL_SHARE);
			broadCastList.setAdapter(adapter);
			broadCastList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context, OffersBroadCastDetails.class);
					intent.putExtra("storeName", REC_SOTRE_NAME);
					intent.putExtra("title", TITLE.get(position));
					intent.putExtra("body", BODY.get(position));
					intent.putExtra("POST_ID", ID.get(position));
					intent.putExtra("PLACE_ID", STORE_ID);
					intent.putExtra("fromCustomer", true);

					intent.putExtra("mob1", MOBILE_NO);
					intent.putExtra("mob2", MOBNO2);
					intent.putExtra("mob3", MOBNO3);
					intent.putExtra("tel1", TELNO1);
					intent.putExtra("tel2", TELNO2);
					intent.putExtra("tel3", TELNO3);
					intent.putExtra("isFromStoreDetails", true);
					intent.putExtra("photo_source", PHOTO_SOURCE.get(position));
					if(POST_USER_LIKE.size()==0)
					{
						intent.putExtra("USER_LIKE","-1");
					}else
					{
						intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));

					}

					context.startActivityForResult(intent, 5);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
			});



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	class SearchListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> TITLE,distance,logo,photo,date,TOTAL_LIKE, TOTAL_SHARE;
		Activity context;
		LayoutInflater inflate;

		public SearchListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> TITLE,ArrayList<String>photo,ArrayList<String>date,ArrayList<String>TOTAL_LIKE, ArrayList<String>TOTAL_SHARE) {
			super(context, resource, textViewResourceId, TITLE);
			// TODO Auto-generated constructor stub
			this.TITLE=TITLE;
			this.photo=photo;
			this.context=context;
			this.date=date;
			this.TOTAL_LIKE=TOTAL_LIKE;

			this.TOTAL_SHARE = TOTAL_SHARE;
			inflate=context.getLayoutInflater();

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return TITLE.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return TITLE.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.offers_broadcast_layout,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
				holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
				holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
				holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
				holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
				holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
				holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
				holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);

				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			/*hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);*/

			hold.txtTitle.setText(TITLE.get(position));

			try
			{
				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				Date date_con = (Date) dt.parse(date.get(position).toString());
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				Date dateTemp=dateFormat.parse(date.get(position).toString());

				DateFormat date_format=new SimpleDateFormat("HH:mm");

				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				//				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+date.get(position));
				Log.i("DATE ", "TIME : "+date_format.format(dateTemp));
				hold.txtDate.setText(day+"");
				hold.txtMonth.setText(dt2.format(date_con)+"");
				hold.txtBroadCastStoreTotalViews.setVisibility(View.GONE);

				// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				// }
				// else
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
				// }
				hold.txtBroadCastStoreTotalLike.setText(TOTAL_LIKE.get(position));


				hold.txtBroadCastStoreTotalShare.setText(TOTAL_SHARE.get(position));

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				if(photo.get(position).toString().equalsIgnoreCase("notfound"))
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));
					/*	imageLoaderList.DisplayImage("",hold.imgBroadcastLogo);*/
					hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					String photo_source=photo.get(position).toString().replaceAll(" ", "%20");
					hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
					hold.viewOverLay.setVisibility(View.VISIBLE);
					/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgBroadcastLogo);*/
					try{
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(hold.imgBroadcastLogo);
					}
					catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){ 
						e.printStackTrace();
					}
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


			return convertView;
		}
	}
	static class ViewHolder
	{

		TextView txtTitle,txtDate,txtMonth;
		ImageView imgBroadcastLogo;
		View viewOverLay;
		TextView txtBroadCastStoreTotalLike;
		TextView txtBroadCastStoreTotalViews;
		TextView txtBroadCastStoreTotalShare;


	}

	@Override
	public void likeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			// TODO Auto-generated method stub
			String likestatus=resultData.getString("likestatus");
			String likemsg=resultData.getString("likemsg");
			storeProgress.setVisibility(View.GONE);
			if(likestatus.equalsIgnoreCase("true"))
			{


				USER_LIKE="1";
				showToast(likemsg);
				changeLikeImage(true);
				//			searchFav.setImageResource(R.drawable.ic_heart_filled);
			}
			else if(likestatus.equalsIgnoreCase("false"))
			{
				showToast(likemsg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}
				//			if(likemsg.equalsIgnoreCase(getResources().getString(R.string.invalidAuth)))
				//			{
				//				showToast("Please login to mark place as favourite");
				//				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				//				context.startActivityForResult(intent,2);
				//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				//			}
			}
			else if(likestatus.equalsIgnoreCase("error"))
			{
				showToast(likemsg);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	void changeLikeImage(boolean status)
	{
		try
		{
			ImageView pagerStoreFav=(ImageView)storePager.findViewById(R.id.pagerStoreFav);
			if(status)
			{

				pagerStoreFav.setImageResource(R.drawable.ic_store_liked);

			}
			else
			{

				pagerStoreFav.setImageResource(R.drawable.ic_store_unliked);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void unFavouriteResult(int resultCode, Bundle resultData) {

		try
		{
			// TODO Auto-generated method stub
			String unlikestatus=resultData.getString("unlikestatus");
			String unlikemsg=resultData.getString("unlikemsg");
			storeProgress.setVisibility(View.GONE);
			if(unlikestatus.equalsIgnoreCase("true"))
			{
				USER_LIKE="0";
				showToast(unlikemsg);
				changeLikeImage(false);
				//			searchFav.setImageResource(R.drawable.ic_heart_unfilled);
			}
			else if(unlikestatus.equalsIgnoreCase("false"))
			{
				showToast(unlikemsg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}
			}
			else if(unlikestatus.equalsIgnoreCase("error"))
			{
				showToast(unlikemsg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onPageSelected(int position) {
		// TODO Auto-generated method stub
		if(position==2)
		{
			if(ID.size()==0)
			{
				loadBroadCast();
			}
		}
		if(position==3)
		{
			if(SOURCE.size()==0)
			{
				loadGallery();
			}
		}
	}


	@Override
	public void placeShareStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String status = resultData.getString("share_status");

		String msg = resultData.getString("share_msg");



		try {

			if(status.equalsIgnoreCase("true")){

				//showToast(msg);
				Log.i("Place Share","Place share "+msg);
				showToast(msg);
			}
			else{

				showToast(msg);
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}

	}


	@Override
	public void onReceiveGalleryResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		ArrayList<String>GALLERY_ID=new ArrayList<String>();
		ArrayList<String>STORE_ID=new ArrayList<String>();

		ArrayList<String>TITLE=new ArrayList<String>();
		ArrayList<String>IMAGE_DATE=new ArrayList<String>();

		ArrayList<String>THUMB_URL=new ArrayList<String>();
		
		String status=resultData.getString("SEARCH_STATUS");
		String message=resultData.getString("MESSAGE");

		storeProgress.setVisibility(View.GONE);

		Log.i("GALLERY_ID", "GALLERY_ID "+status);

		if(status.equalsIgnoreCase("true"))
		{
			try
			{


				GALLERY_ID=resultData.getStringArrayList("ID");
				STORE_ID=resultData.getStringArrayList("STORE_ID");
				SOURCE=resultData.getStringArrayList("SOURCE");
				TITLE=resultData.getStringArrayList("TITLE");
				IMAGE_DATE=resultData.getStringArrayList("IMAGE_DATE");
				THUMB_URL=resultData.getStringArrayList("THUMB_URL");

				for(int i=0;i<SOURCE.size();i++)
				{

					Log.i("======SOURCE", "SOURCE "+SOURCE.get(i));
				}
				setGalleryData(THUMB_URL);
				
			}catch(IllegalStateException il)
			{
				il.printStackTrace();
			}
			catch(NullPointerException npe)
			{
				npe.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}else
		{
			/*Toast.makeText(context, message, Toast.LENGTH_SHORT).show();*/
		}

	}

	void setGalleryData(ArrayList<String>source)
	{
		try
		{
			GridView pagerGalleryGrid=(GridView)storePager.findViewById(R.id.pagerGalleryGrid);

			pagerGalleryGrid.setAdapter(new GridAdapter(context, 0, 0, source));

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	class GridAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> source;
		LayoutInflater inflator;
		Activity context;
		public GridAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> source) {
			super(context, resource, textViewResourceId, source);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();
			this.source=source;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return source.size();
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				GridViewHolder holder=new GridViewHolder();
				convertView=inflator.inflate(R.layout.store_details_gallery_image, null);
				holder.pagerGalleryImage=(ImageView)convertView.findViewById(R.id.pagerGalleryImage);
				convertView.setTag(holder);
			}

			GridViewHolder hold=(GridViewHolder)convertView.getTag();

			try
			{
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+source.get(position), hold.pagerGalleryImage);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+source.get(position))
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.pagerGalleryImage);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return convertView;
		}



	}
	static class GridViewHolder
	{
		ImageView pagerGalleryImage;
	}


}
