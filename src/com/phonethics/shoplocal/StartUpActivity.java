package com.phonethics.shoplocal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.crittercism.app.Crittercism;
import com.phonethics.localnotification.LocalNotification;

public class StartUpActivity extends Activity {

	DBUtil dbutil;
	Activity context;
	String value;
	boolean isTourEnded=false;

	String PREF_NAME="TOUR_PREF";
	String TOUR_KEY="TOUR_VALUE";

	LocalNotification local;

	int launchVia=0;

	Map<String,String> eventMap=new HashMap<String, String>();
	
	private  int SPLASH_TIME_OUT = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_up);
		context=this;
		dbutil=new DBUtil(context);
		local=new LocalNotification(getApplicationContext());
		
		  new Handler().postDelayed(new Runnable() {
			  
	            /*
	             * Showing splash screen with a timer. This will be useful when you
	             * want to show case your app logo / company
	             */
	 
	            @Override
	            public void run() {
	                // This method will be executed once the timer is over
	                // Start your app main activity
	            	
	            	try
	        		{
	        			Crittercism.initialize(getApplicationContext(), getResources().getString(R.string.critism));
	        			local=new LocalNotification(getApplicationContext());

	        			/*		Set<String> tags = new HashSet<String>(); 
	        			tags=PushManager.shared().getTags();

	        			tags.add("customer");

	        			PushManager.shared().setTags(tags);
	        			 */
	        			/*		Log.i("TAGS", "TAGS "+tags.size());*/

	        			try
	        			{
	        				Bundle bundle=getIntent().getExtras();

	        				if(bundle!=null)
	        				{
	        					launchVia=bundle.getInt("requestCode",0);
	        				}
	        			}catch(Exception ex)
	        			{
	        				ex.printStackTrace();
	        			}
	        			//Canceling All Alarm 
	        			/*local.caneclAllAlarm();*/

	        			//			if(dbutil.getAreaRowCount()==0)
	        			//			{
	        			//				Intent intent=new Intent(context, LocationActivity.class);
	        			//				startActivity(intent);
	        			//				this.finish();
	        			//			}
	        			//			else
	        			//			{
	        			value=dbutil.getActiveAreaPlace();

	        			/*			if(dbutil.getAreaRowCount()==0)
	        			{
	        				Intent intent=new Intent(context, LocationActivity.class);
	        				startActivity(intent);
	        				this.finish();
	        			}*/
	        			Log.i("Active Area", "Active Area "+value+" row count "+dbutil.getAreaRowCount());
	        			if(value.length()!=0)
	        			{
	        				Log.i("Active Area", "Active Area in "+value+" row count "+dbutil.getAreaRowCount());

	        				Intent intent=new Intent(context, DefaultSearchList.class);
	        				intent.putExtra("opeDrawer", true);
	        				startActivity(intent);
	        				context.finish();

	        			}
	        			else
	        			{
	        				//Checking if tour is being ended by signing up with merchant or customer or skipped with pressing Later
	        				isTourEnded=getTourPrefs();

	        				//if not ended
	        				if(!isTourEnded)
	        				{
	        					Intent intent=new Intent(context, TourActivity.class);
	        					startActivity(intent);
	        					context.finish();
	        				}
	        				else
	        				{
	        					Log.i("Active Area", "Active Area out "+value+" row count "+dbutil.getAreaRowCount());

	        					if(value.length()==0)
	        					{
	        						Intent intent=new Intent(context, LocationActivity.class);
	        						startActivity(intent);
	        						context.finish();
	        					}
	        					else
	        					{
	        						Intent intent=new Intent(context, DefaultSearchList.class);
	        						intent.putExtra("opeDrawer", true);
	        						startActivity(intent);
	        						context.finish();
	        					}
	        				}
	        			}


	        			try
	        			{

	        				EventTracker.startLocalyticsSession(getApplicationContext());

	        				Log.i("requestCode", "requestCode "+launchVia);

	        				if(launchVia==Integer.parseInt(getResources().getString(R.string.Edit_Store_alert)))
	        				{
	        					eventMap.put("Type", "Edit_Store");
	        				}
	        				else if(launchVia==Integer.parseInt(getResources().getString(R.string.Talk_Now_alert)))
	        				{
	        					eventMap.put("Type", "Talk_Now");
	        				}
	        				else if(launchVia==Integer.parseInt(getResources().getString(R.string.Login_alert)))
	        				{
	        					eventMap.put("Type", "Customer_SignIn");
	        				}
	        				else if(launchVia==Integer.parseInt(getResources().getString(R.string.Favourite_alert)))
	        				{
	        					eventMap.put("Type", "Customer_Favourite");
	        				}
	        				else if(launchVia==Integer.parseInt(getResources().getString(R.string.Customer_Profile_alert)))
	        				{ 
	        					eventMap.put("Type", "Customer_SpecialDate");
	        				}
	        				else if(launchVia==Integer.parseInt(getResources().getString(R.string.App_Launch)))
	        				{
	        					eventMap.put("Type", "App_NotLaunched");
	        				}


	        				if(eventMap.size()!=0)
	        				{
	        					Log.i("Request Code ", "Request Code "+eventMap.toString());
	        				}


	        			}catch(Exception ex)
	        			{
	        				ex.printStackTrace();
	        			}
	        			EventTracker.logEvent("App_Launches", false);


	        			//			}
	        		}catch(Exception ex)
	        		{
	        			ex.printStackTrace();
	        		}
	            }
	        }, SPLASH_TIME_OUT);
	
		
		
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onResume() {
		super.onResume();
		try
		{
			if(eventMap.size()!=0)
			{
				Log.i("Request Code ", "Request Code "+eventMap.toString());
				EventTracker.logEvent("App_LaunchLocalNotification",eventMap );
			}
			checkFirstTimeInstall();
			checkLastUsedMonth();
			checkLastUsedDate();
			checkLastUpdate();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		//registerReceiver(new BroadCast_AppUpgrade(), new IntentFilter("android.intent.action.PACKAGE_REPLACED"));
		/*	EventTracker.startLocalyticsSession(getApplicationContext());*/

		//		//facebook adv. event
		//
		//		AppEventsLogger.activateApp(getApplicationContext(), getResources().getString(R.string.fb_appid));
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(context);
		//unregisterReceiver(new BroadCast_AppUpgrade());
		super.onStop();
		//				try
		//				{
		//					handler.removeCallbacks(runnable);
		//				}catch(Exception ex)
		//				{
		//					ex.printStackTrace();
		//				}
	}

	void checkFirstTimeInstall(){
		try{
			final String PREFS_NAME = "MyPrefsFile";

			SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);

			if (settings.getBoolean("my_first_time", true)) {
				//the app is being launched for first time, do something        
				// record the fact that the app has been started at least once
				EventTracker.logEvent("App_Installs", false);
				settings.edit().putBoolean("my_first_time", false).commit(); 
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	void checkLastUpdate(){
		SharedPreferences prefs =  context.getSharedPreferences("verCode", 1);
		SharedPreferences.Editor editor = prefs.edit();
		int versionNew = 0;
		try{
			PackageInfo pInfoNew = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNew = pInfoNew.versionCode;
		}
		catch(Exception e){}

		int tempVer = prefs.getInt("verCode", -1);

		if(tempVer == -1){ //Check for first time install
			try{
				PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				int version = pInfo.versionCode;
				editor.putInt("verCode", version);
				editor.commit();
			}
			catch(Exception e){}
		}

		if(versionNew != tempVer){
			try
			{
				if(local.isAppLaunchedFired()==true)
				{
					local.clearAppLaunchPrefs();
				}
				if(local.isLoginAlarmFired()==true)
				{
					local.clearLoginPrefs();
				}
				if(local.isPlaceAlarmFired()==true)
				{
					local.clearPlacePrefs();
				}
				if(local.isPostAlarmFired()==true)
				{
					local.clearPostPrefs();
				}
				if(local.isProfilAlarmFired()==true)
				{
					local.clearProfilePrefs();
				}
				if(local.isFavAlarmFired()==true)
				{
					local.clearLoginPrefs();
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			EventTracker.logEvent("App_Upgrades", false);
			editor.putInt("verCode", versionNew);
			editor.commit();
		}
	}

	void checkLastUsedDate()
	{
		try
		{
			String current_date="";
			String lastDate="";
			Date sysDate=new Date();

			SimpleDateFormat formatter = new SimpleDateFormat("MMMM d yyyy");

			//Shared Pref
			SharedPreferences prefs =  context.getSharedPreferences("dateUsed", 1);

			SharedPreferences.Editor editor = prefs.edit();

			String tempDate = prefs.getString("dateUsed", "");

			if(tempDate.length()==0){
				current_date=formatter.format(sysDate);
				Log.i("First Time Stamp", "First Time Stamp "+current_date);
				editor.putString("dateUsed", current_date);
				editor.commit();
				EventTracker.logEvent("App_DailyEngagedUsers", false);
			}
			else
			{
				lastDate=prefs.getString("dateUsed", "");	
				try
				{
					Date date=formatter.parse(lastDate);
					if(date.before(formatter.parse(formatter.format(sysDate))))
					{
						current_date=formatter.format(sysDate);
						editor.putString("dateUsed", current_date);
						editor.commit();
						EventTracker.logEvent("App_DailyEngagedUsers", false);
						lastDate=prefs.getString("dateUsed", "");	
					}

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}





		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	void checkLastUsedMonth(){

		try{
			Calendar cal = Calendar.getInstance();

			int month = cal.get(Calendar.MONTH)+1;

			SharedPreferences prefs =  context.getSharedPreferences("monthUsed", 1);
			SharedPreferences.Editor editor = prefs.edit();


			int tempMonth = prefs.getInt("monthUsed", -1);

			Log.i("MONTH", "MONTH "+month);

			if(tempMonth == -1){
				editor.putInt("monthUsed", month);
				editor.commit();
				Log.i("MONTH", "MONTH FIRST TIME "+month);
				EventTracker.logEvent("App_MonthlyEngagedUsers", false);
			}

			Log.i("Date time","MONTH Temp Month "+prefs.getInt("monthUsed", -1));

			if(month != tempMonth){
				editor.putInt("monthUsed", month);
				editor.commit();
				Log.i("MONTH", "MONTH CHANGED "+month);
				EventTracker.logEvent("App_MonthlyEngagedUsers", false);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}



	boolean getTourPrefs()
	{
		boolean tour;
		SharedPreferences prefs=context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
		tour=prefs.getBoolean(TOUR_KEY, false);
		return tour;

	}


}
