package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.CalendarAdapter;
import com.phonethics.model.GetBroadCast;
import com.phonethics.model.GetSpecialDates;

import com.phonethics.networkcall.GetSpecialDatesResultReceiver;
import com.phonethics.networkcall.GetSpecialDatesService;
import com.phonethics.networkcall.GetSpecialDatesResultReceiver.GetSpecialDate;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SureShop extends SherlockActivity implements GetSpecialDate {

	ActionBar actionBar;

	GetSpecialDatesResultReceiver getSpecialDates;

	static String STORE_URL;
	static String SPECIAL_DATE;


	static String API_HEADER;
	static String API_VALUE;

	Activity context;

	ArrayList<String> allTypes = new ArrayList<String>();
	ArrayList<String> allCount = new ArrayList<String>();
	ArrayList<String> calenderDates = new ArrayList<String>();

	// Calender View

	public GregorianCalendar month, itemmonth;// calendar instances.

	public CalendarAdapter adapter;// adapter instance
	public Handler handler;// for grabbing some event values for showing the dot
	// marker.
	public ArrayList<String> items; // container to store calendar items which
	// needs showing the event marker
	ArrayList<String> event;

	//LinearLayout rLayout;
	ListView eventList;

	ArrayList<String> date;
	ArrayList<String> desc = new ArrayList<String>();;



	static int cntMonthNext=0;
	static int cntMonthPrevious=0;

	EventsAdapter eventsAdapter;

	ArrayList<GetSpecialDates> getDates = new ArrayList<GetSpecialDates>();

	static String USER_ID="";
	static String AUTH_ID="";

	SessionManager session;

	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	ProgressBar pBar;

	ImageView infoIcon;

	NetworkCheck network;
	
	DBUtil dbutil;
	
	String mobileCurrentDate;
	String calenderFullDate;
	String selectedGridDate;
	
	boolean isLoggedInCustomer;
	
	String STORE_ID="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sure_shop);
		Locale.setDefault(Locale.US);

		context = this;


		session = new SessionManager(getApplicationContext());
		network=new NetworkCheck(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle("SureShop");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();
		
		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			STORE_ID=b.getString("STORE_ID");
		}

//		dbutil = new DBUtil(context);

		pBar = (ProgressBar) findViewById(R.id.pBar);
		infoIcon = (ImageView) findViewById(R.id.info_icon);

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		HashMap<String,String>user_details=session.getUserDetails();
		USER_ID=user_details.get(KEY_USER_ID).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID).toString();

		STORE_URL = getResources().getString(R.string.server_url)+getResources().getString(R.string.merchant_api);
		SPECIAL_DATE = getResources().getString(R.string.special_date); 

		Log.d("URLIS","URLIS" + STORE_URL+SPECIAL_DATE);

		getSpecialDates = new GetSpecialDatesResultReceiver(new Handler());
		getSpecialDates.setReciver(this);

		// calender view display

		//rLayout = (LinearLayout) findViewById(R.id.text);
		eventList = (ListView) findViewById(R.id.eventsList);

		month = (GregorianCalendar) GregorianCalendar.getInstance();
		itemmonth = (GregorianCalendar) month.clone();

		items = new ArrayList<String>();

		adapter = new CalendarAdapter(this, month);


		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(adapter);

		handler = new Handler();
		handler.post(calendarUpdater);

		TextView title = (TextView) findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

		RelativeLayout previous = (RelativeLayout) findViewById(R.id.previous);
		
		isLoggedInCustomer=session.isLoggedIn();

		if(network.isNetworkAvailable()){
			
			
//			
//			if(!isLoggedInCustomer)
//			{
//
//				showToast("Please login.");
//				Intent intent=new Intent(context,SplitLoginSignUp.class);
//				context.startActivityForResult(intent,2);
//				overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
//			}
//			else{
			
				callSpecialDates();
//			}
			

		}else{

			showToast(getResources().getString(R.string.noInternetConnection));
		}



		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();

				desc.clear();
				allCount.clear();

				eventsAdapter = null;
			}
		});

		RelativeLayout next = (RelativeLayout) findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();

				desc.clear();
				allCount.clear();

				eventsAdapter = null;
			}
		});



		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// removing the previous view if added
				//				if (((ListView) eventList).getChildCount() > 0) {
				//					((ListView) eventList).removeAllViews();
				//				}

				//desc = new ArrayList<String>();
				date = new ArrayList<String>();
				((CalendarAdapter) parent.getAdapter()).setSelected(v);
				selectedGridDate= CalendarAdapter.dayString
						.get(position);
				String[] separatedTime = selectedGridDate.split("-");
				String gridvalueString = separatedTime[2].replaceFirst("^0*",
						"");// taking last part of date. ie; 2 from 2012-12-02.
				int gridvalue = Integer.parseInt(gridvalueString);
				// navigate to next or previous month on clicking offdays.
				if ((gridvalue > 10) && (position < 8)) {
					setPreviousMonth();
					refreshCalendar();
				} else if ((gridvalue < 7) && (position > 28)) {
					setNextMonth();
					refreshCalendar();
				}
				((CalendarAdapter) parent.getAdapter()).setSelected(v);
				
				calenderFullDate = selectedGridDate;
				
				Log.d("CALENDERFULLDATE","CALENDERFULLDATE " + calenderFullDate);

				selectedGridDate = selectedGridDate.substring(5, selectedGridDate.length());

				Log.d("SELECTED GRIDDATE","SELECTED GRIDDATE " + selectedGridDate);



				//				for (int i = 0; i < getDates.size(); i++) {
				//if (Utility.startDates.get(i).equals(selectedGridDate)) {

				//Log.d("SELECTED GRIDDATE","MERI WALI DATE" + calenderDates.get(i));

				desc.clear();
				allCount.clear();

				for(int j=0;j<getDates.size();j++){

					String tempdate = getDates.get(j).getCalenderdate().substring(5, getDates.get(j).getCalenderdate().length());

					if(tempdate.equals(selectedGridDate)){
						//desc.add(Utility.nameOfEvent.get(i));

						//eventList.setVisibility(View.VISIBLE);

						for(int k=0;k<getDates.get(j).getType().size();k++){

							desc.add(getDates.get(j).getType().get(k));
							allCount.add(getDates.get(j).getCount().get(k));
						}


					}	
					else{

						if(eventsAdapter!=null){

							//Toast.makeText(context, "not matched", 0).show();
							//								
							//								items.clear();
							//								desc.clear();
							//								allCount.clear();
							//								eventsAdapter.notifyDataSetChanged();
							//								//eventsAdapter.clear();

							//eventList.setVisibility(View.INVISIBLE);
						}
					}
				}

				//				}

				//				if (desc.size() > 0) {
				//					
				//					for (int i = 0; i < desc.size(); i++) {


				//						TextView rowTextView = new TextView(SureShop.this);
				//
				//						// set some properties of rowTextView or something
				//
				//						if(desc.get(i).equalsIgnoreCase("1")){
				//							
				//							rowTextView.setText("Event: " + "Birthday");
				//						}
				//						else if(desc.get(i).equalsIgnoreCase("2")){
				//
				//							rowTextView.setText("Event: " + "Spouse's Birthday");
				//						}
				//						else if(desc.get(i).equalsIgnoreCase("3")){
				//
				//							rowTextView.setText("Event: " + "Anniversary");
				//						}
				//						else if(desc.get(i).equalsIgnoreCase("4")){
				//
				//							rowTextView.setText("Event: " + "Special Occasion");
				//						}
				//						else if(desc.get(i).equalsIgnoreCase("5")){
				//
				//							rowTextView.setText("Event: " + "Special Date1");	
				//						}
				//						else if(desc.get(i).equalsIgnoreCase("6")){
				//
				//							rowTextView.setText("Event: " + "Special Date2");	
				//						}
				//						else if(desc.get(i).equalsIgnoreCase("7")){
				//
				//							rowTextView.setText("Event: " + "Special Date3");	
				//						}
				//
				//
				//
				//						//rowTextView.setText("Event: " + desc.get(i));
				//						rowTextView.setTextColor(Color.BLACK);
				//
				//						Log.d("TEXTVIEW","TEXTVIEW" + desc.get(i));
				//
				//						// add the textview to the linearlayout
				//						//rLayout.addView(rowTextView); 
				eventsAdapter = null;

				eventsAdapter = new EventsAdapter(context,R.drawable.ic_launcher, desc, allCount);
				eventList.setAdapter(eventsAdapter);

				eventsAdapter.notifyDataSetChanged();
				//					}
				//				}

				//				desc = null;

			}

		});


		infoIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);

				// set title
				alertDialogBuilder.setTitle("Sure Shop");

				// set dialog message
				alertDialogBuilder
				.setMessage("Sureshop displays important dates like birthdays & anniversaries of local shoppers. you can press 'talk' and send them customised offers. You can see dates upto the next 30 days from today")
				.setCancelable(false)
				.setIcon(R.drawable.ic_launcher)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity


					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
				alertDialog.show();
			}
		});


		Date now = new Date();
		Date alsoNow = Calendar.getInstance().getTime();
		mobileCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(now);
		
		Log.d("currentDate", "currentDate " + mobileCurrentDate);
	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}


	private void callSpecialDates() {
		// TODO Auto-generated method stub

		Intent intent = new Intent(context, GetSpecialDatesService.class);
		intent.putExtra("getSpecialDates", getSpecialDates);
		intent.putExtra("URL", STORE_URL+SPECIAL_DATE);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		intent.putExtra("place_id", STORE_ID);
		pBar.setVisibility(View.VISIBLE);
		context.startService(intent);

	}



	@Override
	public void onGetSpecialDates(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		pBar.setVisibility(View.GONE);

		String status = resultData.getString("getSpecialDateStatus");

		String message  = resultData.getString("message");

		//		calenderDates = resultData.getStringArrayList("calenderDates");
		//		allTypes = resultData.getStringArrayList("allTypes");
		//		allCount = resultData.getStringArrayList("allCount");



		//		eventsAdapter = new EventsAdapter(context,R.drawable.ic_launcher, allTypes, allCount);
		//		eventList.setAdapter(eventsAdapter);


		try {


			if(status.equalsIgnoreCase("true")){

				getDates = resultData.getParcelableArrayList("getAllSpecialDates");

				for(int i=0;i<getDates.size();i++){

					Log.d("DATES","DATES " + getDates.get(i).getCalenderdate());

					for(int m=0;m < getDates.get(i).getCount().size();m++){

						Log.d("COUNT NEW","COUNT NEW " + getDates.get(i).getCount().get(m));
					}

					if(mobileCurrentDate.equalsIgnoreCase(getDates.get(i).getCalenderdate())){
						
						Log.d("INSIDE MATCHED","INSIDE MATCHED");
						
						String tempdate = getDates.get(i).getCalenderdate().substring(5, getDates.get(i).getCalenderdate().length());
						
						String newtempDate = mobileCurrentDate.substring(5,mobileCurrentDate.length());

						if(tempdate.equals(newtempDate)){
							//desc.add(Utility.nameOfEvent.get(i));

							//eventList.setVisibility(View.VISIBLE);
							
							desc.clear();
							allCount.clear();

							for(int k=0;k<getDates.get(i).getType().size();k++){

//								desc.add(getDates.get(i).getType().get(k));
//								allCount.add(getDates.get(i).getCount().get(k));
								
								Log.d("IFMATCHED","IFMATCHED " +  getDates.get(i).getType().get(k));
								Log.d("IFMATCHED","IFMATCHED " +  getDates.get(i).getCount().get(k));
								
								desc.add(getDates.get(i).getType().get(k));
								allCount.add( getDates.get(i).getCount().get(k));
								
							}


							eventsAdapter = null;

							eventsAdapter = new EventsAdapter(context,R.drawable.ic_launcher, desc, allCount);
							eventList.setAdapter(eventsAdapter);

							eventsAdapter.notifyDataSetChanged();
						}	
						
					}

				}

				handler.post(calendarUpdater);


			}
			else{

				showToast(message);
			}


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	void showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onStop() {
		cntMonthNext=0;
		super.onStop();
	}

	protected void setNextMonth() {
		/*if (month.get(GregorianCalendar.MONTH) == month
			    .getActualMaximum(GregorianCalendar.MONTH)) {
			   month.set((month.get(GregorianCalendar.YEAR) + 1),
			     month.getActualMinimum(GregorianCalendar.MONTH), 1);
			  } else {*/
		Calendar c=Calendar.getInstance();
		int currMonth=c.get(Calendar.MONTH);

		int diff=month.get(GregorianCalendar.MONTH)-currMonth;

		if(diff<1){
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) + 1);
		}
		else{
			showToast("Cannot move ahead");
		}
		//}
	}

	protected void setPreviousMonth() {
		//showToast("Please check your interent connection");
		Calendar c=Calendar.getInstance();
		int currMonth=c.get(Calendar.MONTH);


		int diff=currMonth-month.get(GregorianCalendar.MONTH);

		if(diff < 0){
			month.set(GregorianCalendar.MONTH,
					month.get(GregorianCalendar.MONTH) - 1);
			cntMonthPrevious++;
		}

		else {
			showToast("Cannot move ahead");
		}

	}


	public void refreshCalendar() {
		TextView title = (TextView) findViewById(R.id.title);

		adapter.refreshDays();
		adapter.notifyDataSetChanged();
		handler.post(calendarUpdater); // generate some calendar items

		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}

	public Runnable calendarUpdater = new Runnable() {

		@Override
		public void run() {

			//eventList.removeAllViews();

			//eventsAdapter.notifyDataSetChanged();
			items.clear();

			// Print dates of the current week
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			String itemvalue;
			//event = Utility.readCalendarEvent(SureShop.this);
			//Log.d("=====Event====", event.toString());
			//Log.d("=====Date ARRAY====", Utility.startDates.toString());

			for (int i = 0; i < getDates.size(); i++) {
				itemvalue = df.format(itemmonth.getTime());
				itemmonth.add(GregorianCalendar.DATE, 1);
				//items.add(Utility.startDates.get(i).toString());

				String tmpDate = getDates.get(i).getCalenderdate().substring(5, getDates.get(i).getCalenderdate().length());
				items.add(tmpDate);
			}
			adapter.setItems(items);
			adapter.notifyDataSetChanged();
		}
	};


	static class EventsAdapter extends ArrayAdapter<String>
	{
		
		DBUtil dbutil = new DBUtil(getContext());

		ArrayList<String> eventType;
		ArrayList<String> eventCount;

		LayoutInflater inflate;

		public EventsAdapter(Activity context, int resource, ArrayList<String> eventType, ArrayList<String> eventCount) {
			super(context, resource);
			// TODO Auto-generated constructor stub

			this.eventType = eventType;
			this.eventCount = eventCount;

			inflate=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return eventType.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.sureshopeventdetails,null);

				holder.event_type=(TextView)convertView.findViewById(R.id.eventType);
				holder.event_count=(TextView)convertView.findViewById(R.id.eventCount);

				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();

			//hold.event_type.setText(" " + eventType.get(position));
			hold.event_count.setText(" " + eventCount.get(position));
			
			dbutil.open();
			hold.event_type.setText(dbutil.getDateTypeById(eventType.get(position)));
			dbutil.close();
			

			/*if(eventType.get(position).equalsIgnoreCase("1")){

				//ev.setText("Event: " + "Birthday");
				hold.event_type.setText(" " + "Birthday");
			}
			else if(eventType.get(position).equalsIgnoreCase("2")){

				//rowTextView.setText("Event: " + "Spouse's Birthday");
				hold.event_type.setText(" " + "Spouse's Birthday");
			}
			else if(eventType.get(position).equalsIgnoreCase("3")){

				//rowTextView.setText("Event: " + "Anniversary");
				hold.event_type.setText(" " + "Anniversary");
			}
			else if(eventType.get(position).equalsIgnoreCase("4")){

				//rowTextView.setText("Event: " + "Special Occasion");
				hold.event_type.setText(" " + "Special Occasion");
			}
			else if(eventType.get(position).equalsIgnoreCase("5")){

				//rowTextView.setText("Event: " + "Special Date1");
				hold.event_type.setText(" " + "Special Date1");
			}
			else if(eventType.get(position).equalsIgnoreCase("6")){

				//rowTextView.setText("Event: " + "Special Date2");	
				hold.event_type.setText(" " + "Special Date2");
			}
			else if(eventType.get(position).equalsIgnoreCase("7")){

				//rowTextView.setText("Event: " + "Special Date3");	
				hold.event_type.setText(" " + "Special Date3");
			}*/




			return convertView;
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return eventType.get(position);
		}


		static class ViewHolder
		{
			Button talk_now;
			TextView event_type;
			TextView event_count;
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		finishto();
	}

	void finishto()
	{
		Intent intent=new Intent(context, MerchantsHomeGrid.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

		this.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		finishto();
		return true;
	}

}
