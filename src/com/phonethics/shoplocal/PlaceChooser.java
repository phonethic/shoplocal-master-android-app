package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.model.StoreListModel;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.squareup.picasso.Picasso;

public class PlaceChooser extends SherlockActivity implements GetAllStore {

	ActionBar actionBar;
	Activity context;

	//ListView
	ListView singlePlaceChooserList;
	ListView MultiPlaceChooserList;
	Button btnSelectStore;

	//Photo url
	String PHOTO_PARENT_URL;

	//ImageLoader
/*	ImageLoader imageLoaderList;*/

	GetAllStores_Of_Perticular_User_Receiver mAllStores;
	CreateBroadCastListAdapter cadapter;

	StoreSearchListAdapter adapter;

	String SELECTED_STORE_ID;

	ArrayList<String>tempPlaceId=new ArrayList<String>();

	NetworkCheck isnetConnected;

	static String API_HEADER;
	static String API_VALUE;
	static String STORE_URL;
	static String SOTRES_PATH;

	static String USER_ID="";
	static String AUTH_ID="";

	//Session Manger
	SessionManager session;
	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	//
	int choiceMode=0;
	int viewPost=-1;
	int AddStore=0;
	int manageGallery=0;
	int report=-1;
	int sure_shop=-1;

	RelativeLayout progressLayout;

	ArrayList<String> STOREID=new ArrayList<String>();
	ArrayList<String> STORE_NAME=new ArrayList<String>();
	ArrayList<String>PHOTO=new ArrayList<String>();
	ArrayList<String> STORE_DESC=new ArrayList<String>();

	DBUtil dbUtil;
	
	EditText searchText;
	Button searchBtn;
	
	int textlength = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_chooser);

		context=this;
		isnetConnected=new NetworkCheck(context);
		actionBar=getSupportActionBar();
		actionBar.setTitle("Choose Place");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		dbUtil=new DBUtil(context);

		//Session Manager
		session=new SessionManager(getApplicationContext());

		//Views
		singlePlaceChooserList=(ListView)findViewById(R.id.singlePlaceChooserList);
		MultiPlaceChooserList=(ListView)findViewById(R.id.MultiPlaceChooserList);
		btnSelectStore=(Button)findViewById(R.id.btnSelectStore);
		progressLayout=(RelativeLayout)findViewById(R.id.progressLayout);

		//Photo URL
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//Store Path
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		SOTRES_PATH=getResources().getString(R.string.allStores);

		//API KEYS
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//ImageLoader
		/*imageLoaderList=new ImageLoader(context);

		imageLoaderList.clearFileCache();*/



		mAllStores=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
		mAllStores.setReceiver(this);
		
		searchText = (EditText) findViewById(R.id.searchText);
		

		//Bundle Value
		try
		{
			Bundle bundle=getIntent().getExtras();
			if(bundle!=null)
			{
				//Session Data
				HashMap<String,String>user_details=session.getUserDetails();

				USER_ID=user_details.get(KEY_USER_ID).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID).toString();

				choiceMode=bundle.getInt("choiceMode");
				viewPost=bundle.getInt("viewPost");
				report=bundle.getInt("report");
				sure_shop=bundle.getInt("sure_shop");
				AddStore=bundle.getInt("AddStore");
				manageGallery=bundle.getInt("manageGallery");
				if(choiceMode==1)
				{
					singlePlaceChooserList.setVisibility(View.VISIBLE);
					MultiPlaceChooserList.setVisibility(View.GONE);
					btnSelectStore.setVisibility(View.GONE);
					
					
				}
				//Broadcast
				else if(choiceMode==2)
				{
					singlePlaceChooserList.setVisibility(View.GONE);
					MultiPlaceChooserList.setVisibility(View.VISIBLE);
					btnSelectStore.setVisibility(View.VISIBLE);
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			/*	ArrayList<String> STOREID=new ArrayList<String>();
			ArrayList<String> STORE_NAME=new ArrayList<String>();
			ArrayList<String>PHOTO=new ArrayList<String>();
			ArrayList<String> STORE_DESC=new ArrayList<String>();*/

			STOREID=dbUtil.getAllPlacesIDs();
			STORE_NAME=dbUtil.getAllPlacesNames();
			PHOTO=dbUtil.getAllPlacesPhotoUrl();
			STORE_DESC=dbUtil.getAllPlacesDesc();

			adapter=new StoreSearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, STORE_NAME, PHOTO,STORE_DESC);
			singlePlaceChooserList.setAdapter(adapter);



			ArrayList<StoreListModel> list=new ArrayList<StoreListModel>();
			ArrayList<StoreListModel> list_id=new ArrayList<StoreListModel>();
			ArrayList<StoreListModel> list_id_desc=new ArrayList<StoreListModel>();

			for(int i=0;i<STOREID.size();i++)
			{
				list.add( get(STORE_NAME.get(i), false) );
				list_id.add( getID(STORE_NAME.get(i),STOREID.get(i),false) );
				list_id_desc.add( getID(STORE_NAME.get(i),STOREID.get(i),STORE_DESC.get(i),false) );

			}

			cadapter=new CreateBroadCastListAdapter(context,0,0,list,list_id,PHOTO,list_id_desc);
			MultiPlaceChooserList.setAdapter(cadapter);
			
			// Searching in mode 1 
			
			if(choiceMode == 1){
				
				
				searchText.setOnEditorActionListener(new OnEditorActionListener() {
					
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						// TODO Auto-generated method stub\
						
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							
							
						}
						
						return false;
					}
				});
				
				searchText.addTextChangedListener(new TextWatcher() {
					
					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub
						
						ArrayList<String> STOREID_SUB = new ArrayList<String>();
						ArrayList<String> STORE_NAME_SUB = new ArrayList<String>();
						ArrayList<String> PHOTO_SUB = new ArrayList<String>();
						final ArrayList<String> STORE_DESC_SUB = new ArrayList<String>();
						
						textlength = searchText.getText().length();
						
						for (int i = 0; i < STORE_NAME.size(); i++)
						{
							if (textlength <= STORE_NAME.get(i).length())
							{
								if (searchText.getText().toString().
										equalsIgnoreCase((String) STORE_NAME.get(i).subSequence(0, textlength)))
								{
									STORE_NAME_SUB.add(STORE_NAME.get(i));
									STOREID_SUB.add(STOREID.get(i));
									PHOTO_SUB.add(PHOTO.get(i));
									STORE_DESC_SUB.add(STORE_DESC.get(i));	


								}
							}
						}
						
						adapter=new StoreSearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, STORE_NAME_SUB, PHOTO_SUB,STORE_DESC_SUB);
						singlePlaceChooserList.setAdapter(adapter);				
						
					}
					
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						
					}
				});
			}
			else{
				
				searchText.setVisibility(View.GONE);
			}
		

			btnSelectStore.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Removing duplicates

					/*HashSet<String> hs = new HashSet<String>();
						HashSet<String> hs2 = new HashSet<String>();

						hs.addAll(tempPlaceId);
						hs2.addAll(tempPlaceName);

						tempPlaceName.clear();
						tempPlaceId.clear();

						tempPlaceName.addAll(hs2);
						tempPlaceId.addAll(hs);

						for(int i=0;i<tempPlaceName.size();i++)
						{
							Toast.makeText(context, "Slected Place "+tempPlaceName.get(i)+" ID "+tempPlaceId.get(i), Toast.LENGTH_SHORT).show();
							Log.i("PLACE ", "Slected Place "+tempPlaceName.get(i)+" ID "+tempPlaceId.get(i));
						}*/


					/*for(Position s:cadapter.getcheckedposition())
						{
							if(s.ischeckedflag)
							{
								Log.i("PLACE ", "Slected Place "+s.getPlacename()+" ID "+s.getId());
							}
						}*/
					/*cadapter.notifyDataSetChanged();
						cadapter.notifyDataSetChanged();*/
					tempPlaceId.clear();
					ArrayList<StoreListModel>temp=cadapter.TITLE	;
					for(int i=0;i<temp.size();i++)
					{
						StoreListModel model=temp.get(i);
						/*cadapter.notifyDataSetChanged();*/
						if(model.isSelected())
						{
							Log.i("PLACE ", "Slected Place "+STORE_NAME.get(i)+" ID "+STOREID.get(i));
							tempPlaceId.add(STOREID.get(i));
						}
					}
					if(tempPlaceId.size()!=0)
					{
						Intent intent=new Intent(context, CreateBroadCast.class);
						intent.putStringArrayListExtra("SELECTED_STORE_ID",tempPlaceId);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}
					else
					{
						showToast(getResources().getString(R.string.choosePlaceAlert));
					}

				}
			});

			singlePlaceChooserList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					//Edit Store
					if(viewPost==2)
					{
						SELECTED_STORE_ID=STOREID.get(position);
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isSplitLogin",true);
						intent.putExtra("USER_ID", USER_ID);
						intent.putExtra("AUTH_ID", AUTH_ID);
						intent.putExtra("STORE_NAME", STORE_NAME.get(position));
						intent.putExtra("STORE_LOGO", PHOTO.get(position));
						intent.putExtra("STORE_ID", STOREID.get(position));
						intent.putExtra("manageGallery",manageGallery);
						/*			intent.putExtra("business_id", BUSINESS_REC_ID);*/
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}
					/*//Create Store
					 else if(viewPost==2 && AddStore==1)
					 {
						 Intent intent=new Intent(context, EditStore.class);
						 intent.putExtra("isNew", true);
						 startActivity(intent);
						 finish();
					 }*/
					//View Posts
					else if(viewPost==1)
					{
						Intent intent=new Intent(context, ViewPost.class);
						intent.putExtra("storeName", STORE_NAME.get(position));
						intent.putExtra("STORE_ID", STOREID.get(position));
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}
					else if(report == 1){

						Intent intent=new Intent(context, Reports.class);
						intent.putExtra("STORE_ID", STOREID.get(position));
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();
					}
					else if(sure_shop==1)
					{

						Intent intent=new Intent(context,SureShop.class);
						intent.putExtra("SureShop",1);
						intent.putExtra("STORE_NAME", STORE_NAME.get(position));
						intent.putExtra("STORE_LOGO", PHOTO.get(position));
						intent.putExtra("STORE_ID", STOREID.get(position));
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();

					}


				}
			});


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		//		//Loading Stores
		//		if(isnetConnected.isNetworkAvailable())
		//		{
		//			loadAllStoreForUser();
		//		}
		//		else 
		//		{
		//			showToast(getResources().getString(R.string.noInternetConnection));
		//		}

	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		finishto();
		return true;
	}

	void loadAllStoreForUser()
	{

		Intent intent=new Intent(context, GetAllStores_Of_Perticular_User.class);
		intent.putExtra("getAllStores",mAllStores);
		intent.putExtra("URL", STORE_URL+SOTRES_PATH);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		context.startService(intent);
		progressLayout.setVisibility(ViewGroup.VISIBLE);
		/*relBroadCastProgress.setVisibility(ViewGroup.VISIBLE);*/
	}

	//Create New Post
	class CreateBroadCastListAdapter extends ArrayAdapter<StoreListModel>
	{
		ArrayList<StoreListModel> TITLE;
		ArrayList<StoreListModel> ID;
		ArrayList<StoreListModel>list_id_desc;
		ArrayList<String>distance,logo;
		Activity context;
		LayoutInflater inflate;

		public CreateBroadCastListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<StoreListModel> TITLE,ArrayList<StoreListModel> ID,ArrayList<String>logo,ArrayList<StoreListModel>list_id_desc) {
			super(context, resource, textViewResourceId, TITLE);
			// TODO Auto-generated constructor stub
			this.TITLE=TITLE;
			this.logo=logo;
			this.context=context;
			this.ID=ID;
			this.list_id_desc=list_id_desc;
			inflate=context.getLayoutInflater();

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return TITLE.size();
		}


		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		StoreListModel getselectedposition(int position) {
			return ((StoreListModel) getItem(position));
		}

		ArrayList<StoreListModel> getcheckedposition() {
			ArrayList<StoreListModel> checkedposition = new ArrayList<StoreListModel>();
			for (StoreListModel p : ID) {
				if (p.isSelected())
					checkedposition.add(p);
			}
			return checkedposition;
		}

		OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				getselectedposition((Integer) buttonView.getTag()).selected = isChecked;
			}
		};


		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				final ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_selectstore,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.placeCheckBox=(CheckBox)convertView.findViewById(R.id.placeCheckBox);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.placeCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						// TODO Auto-generated method stub
						StoreListModel element=(StoreListModel)holder.placeCheckBox.getTag();
						element.setSelected(buttonView.isChecked());


					}
				});
				convertView.setTag(holder);
				holder.placeCheckBox.setTag(TITLE.get(position));

			}
			else
			{

				((ViewHolder)convertView.getTag()).placeCheckBox.setTag(TITLE.get(position));
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.placeCheckBox.setChecked(TITLE.get(position).isSelected());


			if(logo.get(position).toString().length()!=0)
			
			{
				//Log.i("LOGO", "LOGO "+logo.get(position));	
				String photo_source=logo.get(position).toString().replaceAll(" ", "%20");
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgStoreLogo);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}

			hold.txtTitle.setText(TITLE.get(position).getPlacename());
			hold.txtSearchStoreDesc.setText(list_id_desc.get(position).getPlacedesc());
			return convertView;
		}



	}

	static class ViewHolder
	{

		TextView txtTitle;
		ImageView imgStoreLogo;
		TextView txtStore;
		CheckBox placeCheckBox;
		TextView txtSearchStoreDesc;

	}


	class StoreSearchListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> TITLE,distance,logo;
		Activity context;
		LayoutInflater inflate;
		ArrayList<String>STORE_DESC;
		public StoreSearchListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> TITLE,ArrayList<String>logo,ArrayList<String>STORE_DESC) {
			super(context, resource, textViewResourceId, TITLE);
			// TODO Auto-generated constructor stub
			this.TITLE=TITLE;
			this.logo=logo;
			this.context=context;
			this.STORE_DESC=STORE_DESC;
			inflate=context.getLayoutInflater();

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return TITLE.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return TITLE.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				StoreSearchViewHolder holder=new StoreSearchViewHolder();
				convertView=inflate.inflate(R.layout.layout_place_search_api,null);

				holder.txtTitle=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.band=(TextView)convertView.findViewById(R.id.band);
				holder.txtSearchStoreDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				convertView.setTag(holder);

			}
			StoreSearchViewHolder hold=(StoreSearchViewHolder)convertView.getTag();

			hold.band.setVisibility(View.GONE);
			hold.txtSearchStoreDistance.setVisibility(View.GONE);
			if(logo.get(position).toString().length()!=0)
			{
				//Log.i("LOGO", "LOGO "+logo.get(position));	
				String photo_source=logo.get(position).toString().replaceAll(" ", "%20");
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgStoreLogo);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}

			hold.txtTitle.setText(TITLE.get(position));
			hold.txtSearchStoreDesc.setText(STORE_DESC.get(position));

			return convertView;
		}

	}

	static class StoreSearchViewHolder
	{

		TextView txtTitle;
		ImageView imgStoreLogo;
		TextView txtStore;
		TextView txtSearchStoreDesc;
		TextView band;
		TextView txtSearchStoreDistance;

	}


	//Receiving all Stores for particular User
	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore#onReceiveUsersAllStore(int, android.os.Bundle)
	 */
	@Override
	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		progressLayout.setVisibility(ViewGroup.GONE);
		String Status=resultData.getString("SEARCH_STATUS");
		if(Status.equalsIgnoreCase("true"))
		{



			adapter=new StoreSearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, STORE_NAME, PHOTO,STORE_DESC);
			singlePlaceChooserList.setAdapter(adapter);



			ArrayList<StoreListModel> list=new ArrayList<StoreListModel>();
			ArrayList<StoreListModel> list_id=new ArrayList<StoreListModel>();
			ArrayList<StoreListModel> list_id_desc=new ArrayList<StoreListModel>();

			for(int i=0;i<STOREID.size();i++)
			{
				list.add( get(STORE_NAME.get(i), false) );
				list_id.add( getID(STORE_NAME.get(i),STOREID.get(i),false) );
				list_id_desc.add( getID(STORE_NAME.get(i),STOREID.get(i),STORE_DESC.get(i),false) );

			}

			//If no store 
			if(STOREID.size()==0)
			{
				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context, EditStore.class);
							intent.putExtra("isNew", true);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500); 




				}
			}
			if(STOREID.size()==1)
			{
				//If only one store present.
				if(viewPost==0)
				{

					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context, CreateBroadCast.class);
							intent.putStringArrayListExtra("SELECTED_STORE_ID",STOREID);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500); 




				}
				//View Post
				else if(viewPost==1)
				{
					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context, ViewPost.class);
							intent.putExtra("storeName", STORE_NAME.get(0));
							intent.putExtra("STORE_ID", STOREID.get(0));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500); 


				}
				//Edit Store
				else if(viewPost==2 && manageGallery==0)
				{
					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							SELECTED_STORE_ID=STOREID.get(0);
							Intent intent=new Intent(context, EditStore.class);
							intent.putExtra("isSplitLogin",true);
							intent.putExtra("USER_ID", USER_ID);
							intent.putExtra("AUTH_ID", AUTH_ID);
							intent.putExtra("STORE_NAME", STORE_NAME.get(0));
							intent.putExtra("STORE_LOGO", PHOTO.get(0));
							intent.putExtra("STORE_ID", STOREID.get(0));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

							finish();
						} 
					}, 500); 
				}
				//Gallery
				else if(viewPost==2 && manageGallery==1)
				{
					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context,EditStore.class);
							intent.putExtra("manageGallery",1);
							intent.putExtra("STORE_NAME", STORE_NAME.get(0));
							intent.putExtra("STORE_LOGO", PHOTO.get(0));
							intent.putExtra("STORE_ID", STOREID.get(0));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500); 
				}
				else if(report==1){

					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context,Reports.class);
							intent.putExtra("manageGallery",1);
							intent.putExtra("STORE_NAME", STORE_NAME.get(0));
							intent.putExtra("STORE_LOGO", PHOTO.get(0));
							intent.putExtra("STORE_ID", STOREID.get(0));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500);
				}
				else if(sure_shop==1)
				{
					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context,SureShop.class);
							intent.putExtra("SureShop",1);
							intent.putExtra("STORE_NAME", STORE_NAME.get(0));
							intent.putExtra("STORE_LOGO", PHOTO.get(0));
							intent.putExtra("STORE_ID", STOREID.get(0));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500);
				}

			}
			cadapter=new CreateBroadCastListAdapter(context,0,0,list,list_id,PHOTO,list_id_desc);

			/*			ArrayList<Position> list=new ArrayList<Position>();
				ArrayList<Position> list_id=new ArrayList<Position>();

				for(int i=0;i<STOREID.size();i++)
				{
					list.add(getName(i, false, STOREID.get(i),STORE_NAME.get(i)));
					list_id.add(getName(i, false, STOREID.get(i),STORE_NAME.get(i)));
				}

				cadapter=new CreateBroadCastListAdapter(context,list,list_id,PHOTO);
			 */			MultiPlaceChooserList.setAdapter(cadapter);

			 btnSelectStore.setOnClickListener(new OnClickListener() {

				 @Override
				 public void onClick(View v) {
					 // TODO Auto-generated method stub
					 //Removing duplicates

					 /*HashSet<String> hs = new HashSet<String>();
						HashSet<String> hs2 = new HashSet<String>();

						hs.addAll(tempPlaceId);
						hs2.addAll(tempPlaceName);

						tempPlaceName.clear();
						tempPlaceId.clear();

						tempPlaceName.addAll(hs2);
						tempPlaceId.addAll(hs);

						for(int i=0;i<tempPlaceName.size();i++)
						{
							Toast.makeText(context, "Slected Place "+tempPlaceName.get(i)+" ID "+tempPlaceId.get(i), Toast.LENGTH_SHORT).show();
							Log.i("PLACE ", "Slected Place "+tempPlaceName.get(i)+" ID "+tempPlaceId.get(i));
						}*/


					 /*for(Position s:cadapter.getcheckedposition())
						{
							if(s.ischeckedflag)
							{
								Log.i("PLACE ", "Slected Place "+s.getPlacename()+" ID "+s.getId());
							}
						}*/
					 /*cadapter.notifyDataSetChanged();
						cadapter.notifyDataSetChanged();*/
					 tempPlaceId.clear();
					 ArrayList<StoreListModel>temp=cadapter.TITLE	;
					 for(int i=0;i<temp.size();i++)
					 {
						 StoreListModel model=temp.get(i);
						 /*cadapter.notifyDataSetChanged();*/
						 if(model.isSelected())
						 {
							 Log.i("PLACE ", "Slected Place "+STORE_NAME.get(i)+" ID "+STOREID.get(i));
							 tempPlaceId.add(STOREID.get(i));
						 }
					 }
					 if(tempPlaceId.size()!=0)
					 {
						 Intent intent=new Intent(context, CreateBroadCast.class);
						 intent.putStringArrayListExtra("SELECTED_STORE_ID",tempPlaceId);
						 startActivity(intent);
						 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						 finish();
					 }
					 else
					 {
						 showToast(getResources().getString(R.string.choosePlaceAlert));
					 }

				 }
			 });

			 //Search EditText
			 /*createBroadCastFilterEditText.addTextChangedListener(new TextWatcher() {

					 @Override
					 public void onTextChanged(CharSequence s, int start, int before, int count) {
						 // TODO Auto-generated method stub
						 final ArrayList<StoreListModel> TEMP_STOREID=new ArrayList<StoreListModel>();
						 final ArrayList<StoreListModel> TEMP_STORE_NAME=new ArrayList<StoreListModel>();
						 final ArrayList<String>TEMP_PHOTO=new ArrayList<String>();

						 ctextLength=createBroadCastFilterEditText.getText().length();

						 TEMP_STOREID.clear();
						 TEMP_STORE_NAME.clear();
						 TEMP_PHOTO.clear();

						 for(int i=0;i<STORE_NAME.size();i++)
						 {
							 //If length of text to be searched is less than or equals to store name then only search.
							 if(ctextLength<=STORE_NAME.get(i).length())
							 {
								//If character typed is in store name then store data 
								 if(createBroadCastFilterEditText.getText().toString().equalsIgnoreCase((String)STORE_NAME.get(i).subSequence(0, ctextLength)))
								 {
								 TEMP_STOREID.add(getID(STORE_NAME.get(i),STOREID.get(i),false));
								 TEMP_STORE_NAME.add(get(STORE_NAME.get(i), false));
								 TEMP_PHOTO.add(PHOTO.get(i));
								 }
							 }
						 }



						 //Re-creating List
						 cadapter=new CreateBroadCastListAdapter(context,0,0,TEMP_STORE_NAME,TEMP_STOREID,PHOTO);


						 MultiPlaceChooserList.setAdapter(cadapter);



					 }

					 @Override
					 public void beforeTextChanged(CharSequence s, int start, int count,
							 int after) {
						 // TODO Auto-generated method stub

					 }

					 @Override
					 public void afterTextChanged(Editable s) {
						 // TODO Auto-generated method stub

					 }
				 });
			  */


			 //Searching inside list
			 /*edtsearchList.addTextChangedListener(new TextWatcher() {

					 @Override
					 public void onTextChanged(CharSequence s, int start, int before, int count) {
						 // TODO Auto-generated method stub
						 final ArrayList<String> TEMP_STOREID=new ArrayList<String>();
						 final ArrayList<String> TEMP_STORE_NAME=new ArrayList<String>();
						 final ArrayList<String>TEMP_PHOTO=new ArrayList<String>();

						 textLength=edtsearchList.getText().length();

						 //Creating Temp Array's to store Searched Data
						 TEMP_STOREID.clear();
						 TEMP_STORE_NAME.clear();
						 TEMP_PHOTO.clear();

						 for(int i=0;i<STORE_NAME.size();i++)
						 {
							 //If length of text to be searched is less than or equals to store name then only search.
							 if(textLength<=STORE_NAME.get(i).length())
							 {
								 //If character typed is in store name then store data 
								 if(edtsearchList.getText().toString().equalsIgnoreCase((String)STORE_NAME.get(i).subSequence(0, textLength)))
								 {
									 TEMP_STOREID.add(STOREID.get(i));
									 TEMP_STORE_NAME.add(STORE_NAME.get(i));
									 TEMP_PHOTO.add(PHOTO.get(i));
								 }
							 }

						 }

						 //Re-creating List
						 adapter=new StoreSearchListAdapter(context,  R.drawable.ic_launcher, R.drawable.ic_launcher, TEMP_STORE_NAME, TEMP_PHOTO);
						 singlePlaceChooserList.setAdapter(adapter);

						 //Handling on click of List Item.
						 singlePlaceChooserList.setOnItemClickListener(new OnItemClickListener() {

							 @Override
							 public void onItemClick(AdapterView<?> arg0, View arg1,
									 int position, long arg3) {
								 // TODO Auto-generated method stub
								 SELECTED_STORE_ID=TEMP_STOREID.get(position);
								 dialog.dismiss();
								 if(!isMyBroadCast)
								 {
									 Intent intent=new Intent(context, CreateBroadCast.class);
									 intent.putExtra("isSplitLogin",true);
									 						intent.putExtra("USER_ID", USER_ID);
									intent.putExtra("AUTH_ID", AUTH_ID);
									 intent.putExtra("SELECTED_STORE_ID",SELECTED_STORE_ID);
									 intent.putExtra("MERCHANT_BUSINESS_ID",MERCHANT_BUSINESS_ID);
									 startActivity(intent);
									 finish();
								 }
								 else
								 {
									 if(!relBroadCastProgress.isShown())
										 relBroadCastProgress.setVisibility(ViewGroup.VISIBLE);
									 Intent intent=new Intent(context, BroadCastService.class);
									 intent.putExtra("broadcast",mBroadCast);
									 intent.putExtra("URL", BROADCAST_URL+GET_BROADCAST_URL);
									 intent.putExtra("api_header",API_HEADER);
									 intent.putExtra("api_header_value", API_VALUE);
									 intent.putExtra("store_id", SELECTED_STORE_ID);
									 context.startService(intent);
								 }

							 }
						 });


					 }

					 @Override
					 public void beforeTextChanged(CharSequence s, int start, int count,
							 int after) {
						 // TODO Auto-generated method stub

					 }

					 @Override
					 public void afterTextChanged(Editable s) {
						 // TODO Auto-generated method stub

					 }
				 });*/

			 singlePlaceChooserList.setOnItemClickListener(new OnItemClickListener() {

				 @Override
				 public void onItemClick(AdapterView<?> arg0, View arg1,
						 int position, long arg3) {
					 // TODO Auto-generated method stub
					 //Edit Store
					 if(viewPost==2)
					 {
						 SELECTED_STORE_ID=STOREID.get(position);
						 Intent intent=new Intent(context, EditStore.class);
						 intent.putExtra("isSplitLogin",true);
						 intent.putExtra("USER_ID", USER_ID);
						 intent.putExtra("AUTH_ID", AUTH_ID);
						 intent.putExtra("STORE_NAME", STORE_NAME.get(position));
						 intent.putExtra("STORE_LOGO", PHOTO.get(position));
						 intent.putExtra("STORE_ID", STOREID.get(position));
						 intent.putExtra("manageGallery",manageGallery);
						 /*			intent.putExtra("business_id", BUSINESS_REC_ID);*/
						 startActivity(intent);
						 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						 finish();
					 }
					 /*//Create Store
					 else if(viewPost==2 && AddStore==1)
					 {
						 Intent intent=new Intent(context, EditStore.class);
						 intent.putExtra("isNew", true);
						 startActivity(intent);
						 finish();
					 }*/
					 //View Posts
					 else if(viewPost==1)
					 {
						 Intent intent=new Intent(context, ViewPost.class);
						 intent.putExtra("storeName", STORE_NAME.get(position));
						 intent.putExtra("STORE_ID", STOREID.get(position));
						 startActivity(intent);
						 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						 finish();
					 }
					 else if(report == 1){

						 Intent intent=new Intent(context, Reports.class);
						 intent.putExtra("STORE_ID", STOREID.get(position));
						 startActivity(intent);
						 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						 finish();
					 }
					 else if(sure_shop==1)
					 {

						 Intent intent=new Intent(context,SureShop.class);
						 intent.putExtra("SureShop",1);
						 intent.putExtra("STORE_NAME", STORE_NAME.get(position));
						 intent.putExtra("STORE_LOGO", PHOTO.get(position));
						 intent.putExtra("STORE_ID", STOREID.get(position));
						 startActivity(intent);
						 overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						 finish();

					 }


				 }
			 });

		}
		else if(Status.equalsIgnoreCase("false"))
		{
			String msg=resultData.getString("SEARCH_MESSAGE");
			showToast(msg);
			if(msg.startsWith("No records found."))
			{
				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					Handler handler = new Handler(); 
					handler.postDelayed(new Runnable() { 
						public void run() { 
							Intent intent=new Intent(context, EditStore.class);
							intent.putExtra("isNew", true);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							finish();
						} 
					}, 500); 

				}
			}
			if(msg.startsWith(getResources().getString(R.string.invalidAuth)))
			{
				session.logoutUser();
				Intent intent=new Intent(context, SplitLoginSignUp.class);
				startActivity(intent);
				this.finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

			}

		}
	}

	private StoreListModel get(String placename,boolean selected) {
		return new StoreListModel(placename,selected);
	}
	private StoreListModel getID(String placename,String id,boolean selected) {
		StoreListModel sm=new StoreListModel(placename, selected);
		sm.setId(id);
		return  sm;
	}

	private StoreListModel getID(String placename,String id,String placedesc,boolean selected) {
		StoreListModel sm=new StoreListModel(placename,placedesc,selected);
		sm.setId(id);
		return  sm;
	}

	void showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	void finishto()
	{
		Intent intent=new Intent(context, MerchantTalkHome.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

		this.finish();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		finishto();
	}


}
