package com.phonethics.shoplocal;

public class CountryModel {
	
	String	cName;
	String	cCode;
	String	cShort;
	
	
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcCode() {
		return cCode;
	}
	public void setcCode(String cCode) {
		this.cCode = cCode;
	}
	public String getcShort() {
		return cShort;
	}
	public void setcShort(String cShort) {
		this.cShort = cShort;
	}
	
	
	
}
