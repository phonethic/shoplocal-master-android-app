package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.actionbarsherlock.widget.SearchView;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.DrawerAdapter;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.model.SectionItem;




public class AboutUs extends SherlockActivity implements OnClickListener {

	TextView aboutAppLable;
	TextView versionLable;
	TextView prefTutorial;
	TextView contactLable;
	TextView pref_number;
	TextView pref_ContactMail;
	TextView infoLable;
	TextView pref_PrivacyPolicy;
	TextView pref_Terms;
	TextView pref_Faq;
	TextView versionNo;
	TextView textViewShoplocal;

	Typeface tf;

	ActionBar actionBar;
	Activity context;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ExpandableListView drawerList;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();

	private static long back_pressed;


	String drawer_item="";
	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	SessionManager session;

	DrawerExpandibleAdapter drawerAdapter;

	DrawerClass drawerClass;

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	LocalNotification localNotification;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setTheme(R.style.Theme_City_custom);
		//		requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		context=this;

		localNotification=new LocalNotification(getApplicationContext());

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		session=new SessionManager(context);

		drawerClass=new DrawerClass(context);

		actionBar=getSupportActionBar();
		actionBar.setTitle(getString(R.string.itemAboutShoplocal));
		actionBar.setHomeButtonEnabled(true);

		moreData=drawerClass.isShowMore();

		//		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#330000ff")));
		//		actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));

		//		actionBar.setDisplayHomeAsUpEnabled(true);

		actionBar.setIcon(R.drawable.ic_drawer);

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);

		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});

		//Creating Drawer
		createDrawer();	


		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getString(R.string.itemAboutShoplocal));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);






		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		aboutAppLable=(TextView)findViewById(R.id.aboutAppLable);
		versionLable=(TextView)findViewById(R.id.versionLable);
		prefTutorial=(TextView)findViewById(R.id.prefTutorial);
		contactLable=(TextView)findViewById(R.id.contactLable);
		pref_number=(TextView)findViewById(R.id.pref_number);
		pref_ContactMail=(TextView)findViewById(R.id.pref_ContactMail);
		infoLable=(TextView)findViewById(R.id.infoLable);
		pref_PrivacyPolicy=(TextView)findViewById(R.id.pref_PrivacyPolicy);
		pref_Terms=(TextView)findViewById(R.id.pref_Terms);
		pref_Faq=(TextView)findViewById(R.id.pref_Faq);
		versionNo=(TextView)findViewById(R.id.versionNo);
		textViewShoplocal = (TextView) findViewById(R.id.textViewShoplocal);
		

		aboutAppLable.setTypeface(tf);
		versionLable.setTypeface(tf);
		prefTutorial.setTypeface(tf);
		contactLable.setTypeface(tf);
		pref_number.setTypeface(tf);
		pref_ContactMail.setTypeface(tf);
		pref_PrivacyPolicy.setTypeface(tf);
		infoLable.setTypeface(tf);
		pref_Terms.setTypeface(tf);
		pref_Faq.setTypeface(tf);
		versionNo.setTypeface(tf);

		prefTutorial.setOnClickListener(this);
		pref_number.setOnClickListener(this);
		pref_ContactMail.setOnClickListener(this);
		pref_PrivacyPolicy.setOnClickListener(this);
		pref_Terms.setOnClickListener(this);
		pref_Faq.setOnClickListener(this);
		textViewShoplocal.setOnClickListener(this);

		try
		{
			PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNo.setText(pinfo.versionName);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		EventTracker.startLocalyticsSession(getApplicationContext());

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}


		}

		/** Custom share dialog */
		dialogShare = new Dialog(AboutUs.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));

	}//onCreate Ends here

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		uiHelper.onStop();
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}



	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			//			drawerMenu.clear();
			//			
			//			//Shoplocal
			//			ExpandibleDrawer drawer=new ExpandibleDrawer();
			//			
			//			drawer.setHeader(getResources().getString(R.string.sectionShopLocal));
			//			drawer.setOpened_state(Integer.parseInt(getResources().getString(R.string.open_status)));
			//			
			//			drawer.setItem(getResources().getString(R.string.itemDiscover));
			//			drawer.setIcon(R.drawable.ic_drawer_offer);
			//			
			//			drawer.setItem(getResources().getString(R.string.itemMyShoplocalOffers));	
			//			drawer.setIcon(R.drawable.ic_drawer_myshoplocal);
			//			
			//			
			//			drawerMenu.add(drawer);
			//			
			//			//Personalize
			//			ExpandibleDrawer drawer2=new ExpandibleDrawer();
			//			drawer2.setHeader(getResources().getString(R.string.sectionPersonalize));
			//			drawer2.setOpened_state(Integer.parseInt(getResources().getString(R.string.open_status)));
			//			if(session.isLoggedInCustomer())
			//			{
			//				drawer2.setItem(getResources().getString(R.string.itemMyProfile));
			//				drawer2.setIcon(R.drawable.ic_drawer_profile);
			//			}
			//			else
			//			{
			//				drawer2.setItem(getResources().getString(R.string.itemLogin));	
			//				drawer2.setIcon(R.drawable.ic_drawer_login);
			//			}
			//			
			//			
			//			drawer2.setItem(getResources().getString(R.string.itemChangeLocation));	
			//			drawer2.setIcon(R.drawable.ic_drawer_changelocation);
			//			
			//			drawer2.setItem(getResources().getString(R.string.itemSettings));
			//			drawer2.setIcon(R.drawable.ic_drawer_settings);
			//			
			//			drawerMenu.add(drawer2);
			//			
			//			//Business
			//			
			//			ExpandibleDrawer drawer3=new ExpandibleDrawer();
			//			drawer3.setHeader(getResources().getString(R.string.sectionBusiness));
			//			drawer3.setOpened_state(Integer.parseInt(getResources().getString(R.string.close_status)));
			//			
			//			drawer3.setItem(getResources().getString(R.string.itemAllStores));	
			//			drawer3.setIcon(R.drawable.ic_drawer_allstores);
			//			
			//			drawer3.setItem(getResources().getString(R.string.itemFavouriteStores));	
			//			drawer3.setIcon(R.drawable.ic_drawer_fav);
			//			
			//					
			//			drawerMenu.add(drawer3);
			//			
			//			//About Shoplocal
			//			
			//			ExpandibleDrawer drawer4=new ExpandibleDrawer();
			//			drawer4.setHeader(getResources().getString(R.string.sectionAboutShoplocal));
			//			drawer4.setOpened_state(Integer.parseInt(getResources().getString(R.string.close_status)));
			//			
			//			
			//			drawer4.setItem(getResources().getString(R.string.itemAboutShoplocal));	
			//			drawer4.setIcon(R.drawable.ic_drawer_about);
			//			
			//			drawer4.setItem(getResources().getString(R.string.itemContactUs));	
			//			drawer4.setIcon(R.drawable.ic_drawer_contact);
			//			
			//			drawer4.setItem(getResources().getString(R.string.itemFacebook));	
			//			drawer4.setIcon(R.drawable.ic_drawer_facebook);
			//			
			//			drawer4.setItem(getResources().getString(R.string.itemTwitter));	
			//			drawer4.setIcon(R.drawable.ic_drawer_twitter);
			//			
			//			
			//			drawerMenu.add(drawer4);
			//			
			//			//Sell
			//			
			//			ExpandibleDrawer drawer5=new ExpandibleDrawer();
			//			drawer5.setHeader(getResources().getString(R.string.sectionSell));
			//			drawer5.setOpened_state(Integer.parseInt(getResources().getString(R.string.close_status)));
			//			
			//			
			//			if(session.isLoggedIn())
			//			{
			//			drawer5.setItem(getResources().getString(R.string.itemBusinessDashboard));
			//			drawer5.setIcon(R.drawable.ic_drawer_business__dashboard);
			//			}
			//			else
			//			{
			//			drawer5.setItem(getResources().getString(R.string.itemLogintoBusiness));
			//			drawer5.setIcon(R.drawable.ic_drawer_business__dashboard);
			//			}
			//			
			//			
			//			drawerMenu.add(drawer5);
			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();


			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}




		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			//			drawerClass.navigate(drawer_item);


		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			//			drawerClass.navigate(drawer_item);
			//			Log.i("TRIM EVENT ", "TRIM EVENT "+drawer_item.replaceAll(" ", ""));
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();
			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{

			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			drawer_item="";
			mDrawerLayout.closeDrawer(Gravity.LEFT);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}*/


			showDialogTab();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{



				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}

	void showDialogTab(){

		dialogShare.show();

		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(AboutUs.this)
					.setLink("http://shoplocal.co.in")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}

				dismissDialog();

			}
		});
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		try
		{
			//			SearchView searchView = new SearchView(actionBar.getThemedContext());
			//			searchView.setQueryHint("Search");
			//			searchView.setIconified(true);
			//
			//			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
			//			.setIcon(R.drawable.abs__ic_search)
			//			.setActionView(searchView)
			//			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			//
			//			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			//
			//				@Override
			//				public boolean onQueryTextSubmit(String newText) {
			//
			//					Intent intent=new Intent(context,SearchActivity.class);
			//					intent.putExtra("genericSearch", true);
			//					intent.putExtra("search_text", newText);
			//					startActivity(intent);
			//					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
			//					return true;
			//				}
			//
			//
			//
			//				@Override
			//				public boolean onQueryTextChange(String newText) {
			//
			//
			//					return true;
			//				}
			//			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}

		return true;
	}


	void finishTo()
	{

		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{

			localNotification.alarm();
			
			finish();

		}
		else
		{
			Toast.makeText(getBaseContext(), getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try
		{
			if(v.getId()==prefTutorial.getId())
			{

			}
			if(v.getId()==pref_number.getId())
			{
				Intent call = new Intent(android.content.Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:"+getResources().getString(R.string.contact_no)));
				startActivity(call);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
			if(v.getId()==pref_ContactMail.getId())
			{
				Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
						"mailto",getResources().getString(R.string.contact_email), null));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
				startActivity(Intent.createChooser(emailIntent, "Send email..."));
			}
			if(v.getId()==pref_PrivacyPolicy.getId())
			{

				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "PrivacyPolicy");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);



			}
			if(v.getId()==pref_Terms.getId())
			{
				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "Terms");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
			if(v.getId()==pref_Faq.getId())
			{
				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "Faq");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
			if(v.getId()==textViewShoplocal.getId())
			{
				Intent intent=new Intent(context,AboutWebView.class);
				intent.putExtra("url", "Web");
				intent.putExtra("Web", "http://shoplocal.co.in/");
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}


}
