package com.phonethics.shoplocal;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.crittercism.app.Crittercism;
import com.facebook.AppEventsLogger;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.customviewpager.JazzyViewPager.TransitionEffect;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.networkcall.LoadGalleryReceiver;
import com.phonethics.networkcall.LoadGalleryReceiver.LoadGallery;
import com.phonethics.networkcall.LoadStoreGallery;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;

public class SplashScreen extends SherlockFragmentActivity implements Receiver,LoadGallery {

	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;
	Activity context;
	ImageView splashImage;
	Button open;


	JazzyViewPager mPager;
	ArrayList<String> urls=new ArrayList<String>();
	ArrayList<String> titles=new ArrayList<String>();

	NetworkCheck network;

	static String IMAGE_URL="";

	LoadGalleryReceiver mReceiver;
	LocationResultReceiver mLocationReceiver;

	String STORE_ID="";
	static String STORE_URL,STORE_GALLERY_URL;
	static String STORE_GALLERY;
	static String API_HEADER;
	static String API_VALUE;
	String photo_url="";

	ImageView appSplash;
	private static long SLEEP_TIME = 1; 

	SessionManager session;

	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	Vibrator vibrate;
	Handler handler ;
	Runnable runnable;

	ActionBar actionBar;


	ArrayList<String>dropdownItems=new ArrayList<String>();
	String JSON_STRING;
	String key;
	String value;


	int dropdown_position;
	boolean IS_SHOPLOCAL=false;


	DBUtil dbutil;

	Button chooseLocation;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();


	private static long back_pressed;

	String drawer_item="";

	DrawerExpandibleAdapter drawerAdapter;
	ExpandableListView drawerList;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	int width;
	ImageView discoverButton;

	DrawerClass drawerClass;

	ArrayList<String> categ_id=new ArrayList<String>();
	ArrayList<String> categ_name=new ArrayList<String>();
	ArrayList<String> store_Categ=new ArrayList<String>();

	ArrayList<String> remove=new ArrayList<String>();

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	static Activity contextt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash_screen);

		EventTracker.trackLocation(true);

		contextt=this;

		//Crittercism.initialize(getApplicationContext(), getResources().getString(R.string.critism));



		STORE_ID="292";

		context=this;

		EventTracker.startLocalyticsSession(context);


		drawerClass=new DrawerClass(context);

		dbutil=new DBUtil(context);

		JSON_STRING=getResources().getString(R.string.dropdownjson);

		moreData=drawerClass.isShowMore();

		actionBar=getSupportActionBar();
		actionBar.setTitle(getResources().getString(R.string.welcomeTitle));
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayUseLogoEnabled(true);
		actionBar.setIcon(R.drawable.ic_drawer);

		actionBar.hide();

		handler = new Handler();
		//Session
		session=new SessionManager(context);

		//Vibrator service
		vibrate=(Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);

		discoverButton=(ImageView)findViewById(R.id.discoverButton);
		open=(Button)findViewById(R.id.open);
		chooseLocation=(Button)findViewById(R.id.chooseLocation);

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);

		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.spdrawerList);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		width = metrics.widthPixels; 


		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});


		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getResources().getString(R.string.welcomeTitle));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		discoverButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				mDrawerLayout.openDrawer(Gravity.LEFT);
			}
		});



		//Creating Drawer
		createDrawer();


		network=new NetworkCheck(context);

		IMAGE_URL=getResources().getString(R.string.photo_url);
		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalGallery");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}else if(network.isNetworkAvailable())
		{

			/*DeleteRecursive(cacheDir);*/
		}

		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		//Registering Receiver
		mReceiver=new LoadGalleryReceiver(new Handler());
		mReceiver.setReceiver(this);

		mLocationReceiver=new LocationResultReceiver(new Handler());
		mLocationReceiver.setReceiver(this);

//		urls.add("drawable://"+R.drawable.slide_one);
//		urls.add("drawable://"+R.drawable.slide_two);
//		urls.add("drawable://"+R.drawable.slide_three);
//		urls.add("drawable://"+R.drawable.slide_four);
//		urls.add("drawable://"+R.drawable.slide_five);
//		urls.add("drawable://"+R.drawable.slide_six);

		titles.add("");
		titles.add("");
		titles.add("");
		titles.add("");
		titles.add("");
		titles.add("");

		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		//Store API
		STORE_GALLERY_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);

		//Store Gallery
		STORE_GALLERY=getResources().getString(R.string.store_gallery);

		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);



		mPager=(JazzyViewPager)findViewById(R.id.viewPagerSplash);
		appSplash=(ImageView)findViewById(R.id.appSplash);
//		mPager.setTransitionEffect(TransitionEffect.ZoomIn);
		
		open.setVisibility(View.GONE);
		mPager.setOffscreenPageLimit(4);

		/*open.animate().setDuration(200);*/
		animate(open).setDuration(200);

		open.setOnTouchListener(new OnTouchListener() {


			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub

				if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
					animate(open).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
					/*open.animate().setInterpolator(sOvershooter).
                            scaleX(1f).scaleY(1f);*/
					animate(open).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(open).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}

				return false;
			}
		});


		open.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imageLoader.clearMemoryCache();
				vibrate.vibrate(50);
				enter();
			}
		});


		if(dbutil.getAreaRowCount()==0)
		{
			chooseLocation.setVisibility(View.VISIBLE);
			PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, urls,titles,true);
			mPager.setAdapter(adapter);
			Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
			anim.setFillAfter(true);
			appSplash.startAnimation(anim);
			appSplash.setVisibility(View.GONE);
			//			open.setVisibility(View.VISIBLE);
			chooseLocation.setVisibility(View.VISIBLE);
		}
		else
		{
			value=dbutil.getActiveAreaPlace();
			if(value.length()!=0)
			{
				if(network.isNetworkAvailable())
				{
					loadGallery();
					//				open.setVisibility(View.VISIBLE);
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
					PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, urls,titles,true);
					mPager.setAdapter(adapter);
					Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
					anim.setFillAfter(true);
					appSplash.startAnimation(anim);
					appSplash.setVisibility(View.GONE);
					//					open.setVisibility(View.VISIBLE);
				}
			}
			else
			{

				PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, urls,titles,true);
				mPager.setAdapter(adapter);
				Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
				anim.setFillAfter(true);
				appSplash.startAnimation(anim);
				appSplash.setVisibility(View.GONE);
				open.setVisibility(View.GONE);
				chooseLocation.setVisibility(View.VISIBLE);
			}
		}

		chooseLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(context, LocationActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();

			}
		} );

		try
		{
			checkFirstTimeInstall();
			checkLastUsedMonth();
			checkLastUsedDate();
			checkLastUpdate();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}//onCreate Ends here


	void checkLastUsedDate()
	{
		try
		{
			String current_date="";
			String lastDate="";
			Date sysDate=new Date();

			SimpleDateFormat formatter = new SimpleDateFormat("MMMM d yyyy");

			//Shared Pref
			SharedPreferences prefs = getSharedPreferences("dateUsed", 1);

			SharedPreferences.Editor editor = prefs.edit();

			String tempDate = prefs.getString("dateUsed", "");

			if(tempDate.length()==0){
				current_date=formatter.format(sysDate);
				Log.i("First Time Stamp", "First Time Stamp "+current_date);
				editor.putString("dateUsed", current_date);
				editor.commit();
				EventTracker.logEvent("App_DailyEngagedUsers", false);
			}
			else
			{
				lastDate=prefs.getString("dateUsed", "");	
				try
				{
					Date date=formatter.parse(lastDate);
					if(date.before(formatter.parse(formatter.format(sysDate))))
					{
						current_date=formatter.format(sysDate);
						editor.putString("dateUsed", current_date);
						editor.commit();
						EventTracker.logEvent("App_DailyEngagedUsers", false);
						lastDate=prefs.getString("dateUsed", "");	
					}

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}





		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	void checkLastUsedMonth(){

		try{
			Calendar cal = Calendar.getInstance();

			int month = cal.get(Calendar.MONTH)+1;

			SharedPreferences prefs = SplashScreen.this.getSharedPreferences("monthUsed", 1);
			SharedPreferences.Editor editor = prefs.edit();


			int tempMonth = prefs.getInt("monthUsed", -1);

			Log.i("MONTH", "MONTH "+month);

			if(tempMonth == -1){
				editor.putInt("monthUsed", month);
				editor.commit();
				Log.i("MONTH", "MONTH FIRST TIME "+month);
				EventTracker.logEvent("App_MonthlyEngagedUsers", false);
			}

			Log.i("Date time","MONTH Temp Month "+prefs.getInt("monthUsed", -1));

			if(month != tempMonth){
				editor.putInt("monthUsed", month);
				editor.commit();
				Log.i("MONTH", "MONTH CHANGED "+month);
				EventTracker.logEvent("App_MonthlyEngagedUsers", false);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}


	void checkFirstTimeInstall(){
		try{
			final String PREFS_NAME = "MyPrefsFile";

			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

			if (settings.getBoolean("my_first_time", true)) {
				//the app is being launched for first time, do something        
				// record the fact that the app has been started at least once
				EventTracker.logEvent("App_Installs", false);
				settings.edit().putBoolean("my_first_time", false).commit(); 
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	void checkLastUpdate(){
		SharedPreferences prefs = SplashScreen.this.getSharedPreferences("verCode", 1);
		SharedPreferences.Editor editor = prefs.edit();
		int versionNew = 0;
		try{
			PackageInfo pInfoNew = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionNew = pInfoNew.versionCode;
		}
		catch(Exception e){}
		
		int tempVer = prefs.getInt("verCode", -1);
		
		if(tempVer == -1){ //Check for first time install
		try{
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			int version = pInfo.versionCode;
			editor.putInt("verCode", version);
			editor.commit();
		}
		catch(Exception e){}
		}

		if(versionNew != tempVer){
			EventTracker.logEvent("App_Upgrades", false);
			editor.putInt("verCode", versionNew);
			editor.commit();
		}
	}


	/*@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}*/


	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public int GetPixelFromDips(float pixels) {
		// Get the screen's density scale 
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();


		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();

			return false;
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.logEvent("App_Launches", false);
		//registerReceiver(new BroadCast_AppUpgrade(), new IntentFilter("android.intent.action.PACKAGE_REPLACED"));
		EventTracker.startLocalyticsSession(getApplicationContext());

		//		//facebook adv. event
		//
		//		AppEventsLogger.activateApp(getApplicationContext(), getResources().getString(R.string.fb_appid));
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{

			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();

			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}


	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();
				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}


		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			try
			{
				imageLoader.clearMemoryCache();
				finish();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			Toast.makeText(getBaseContext(),getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}


	void enter()
	{
		imageLoader.clearMemoryCache();


		Intent intent=new Intent(context, NewsFeedActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		finish();




		//		if(getResources().getBoolean(R.bool.isAppShopLocal))
		//		{
		//			SharedPreferences prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
		//			if(prefs.getString(getResources().getString(R.string.usertype), "none").toString().equalsIgnoreCase(getResources().getString(R.string.customer)))
		//			{
		//				Intent intent=new Intent(context, NewsFeedActivity.class);
		//				startActivity(intent);
		//				/*overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);*/
		//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		//
		//				finish();
		//
		//			}
		//			else if(prefs.getString(getResources().getString(R.string.usertype), "none").toString().equalsIgnoreCase(getResources().getString(R.string.merchant)))
		//			{
		//
		//				if(session.isLoggedIn())
		//				{
		//					Intent intent=new Intent(context, MerchantTalkHome.class);
		//					intent.putExtra("isSplitLogin",true);
		//					intent.putExtra("choiceMode", 2);
		//					intent.putExtra("viewPost", 0);
		//					startActivity(intent);
		//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		//					finish();
		//				}
		//				else
		//				{
		//
		//					Intent intent=new Intent(context, SplitLoginSignUp.class);
		//					startActivity(intent);
		//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		//
		//					finish();
		//
		//				}
		//			}
		//			else
		//			{
		//				Intent intent=new Intent(context, AppGate.class);
		//				startActivity(intent);
		//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		//
		//				finish();
		//			}
		//
		//		}
		//		else
		//		{
		//			Intent intent=new Intent(context, HomeGrid.class);
		//			startActivity(intent);
		//			/*overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);*/
		//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		//
		//			finish();
		//		}
	}

	void parseKeyValue(String json)
	{
		try {
			JSONObject jsonobject=new JSONObject(json);
			key=jsonobject.getString("key");
			value=jsonobject.getString("value");
			IS_SHOPLOCAL=Boolean.parseBoolean(jsonobject.getString("isShopLocal"));


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void parseJson(String json)
	{
		try {


			JSONObject jsonobject=new JSONObject(json);
			JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");
			for(int i=0;i<dropDownArray.length();i++)
			{
				JSONObject jsonObject=dropDownArray.getJSONObject(i);

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					//Shoplocal
					if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}else
				{
					//Inorbit
					if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	String getDropDownObject(int position,String json)
	{
		String dropdown="";
		if(dropdownItems.size()!=0)
		{

			try {
				JSONObject jsonobject=new JSONObject(json);
				JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");

				/*	JSONObject jsonObject=dropDownArray.getJSONObject(position);
				dropdown=jsonObject.toString();

				Log.i("JSON", "JSON "+dropdown);*/

				for(int i=0;i<dropDownArray.length();i++)
				{
					JSONObject jsonObject=dropDownArray.getJSONObject(i);

					//		Log.i("PARSE ", "BOOLEAN PARSE getDropDownObject  "+jsonObject.toString()); 


					if(getResources().getBoolean(R.bool.isAppShopLocal))
					{
						//Shoplocal
						if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}else
					{
						//Inorbit
						if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}
				}

				Log.i("GET DROP DOWN ", "GET DROP DOWN "+dropdown);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();

			}


		}
		return dropdown;
	}

	void locationService()
	{
		Log.i("Enter","Enter loc service");
		if(network.isNetworkAvailable())
		{
			//calling location service.
			Intent intent=new Intent(context, LocationIntentService.class);
			intent.putExtra("receiverTag", mLocationReceiver);
			context.startService(intent);
			/*ShopLocalMain.shopProgress.setVisibility(View.VISIBLE);*/

		}else
		{
			MyToast.showToast(context,(getResources().getString(R.string.noInternetConnection)),1);

		}
	}

	void loadGallery()
	{

		//Toast.makeText(context, "Loading Gallery", Toast.LENGTH_SHORT).show();
		Intent intent=new Intent(context, LoadStoreGallery.class);
		intent.putExtra("loadgall",mReceiver);
		intent.putExtra("URL", STORE_GALLERY_URL+STORE_GALLERY);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		/*			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);*/
		if(!IS_SHOPLOCAL)
		{
			intent.putExtra("store_id", value);
		}
		else
		{
			intent.putExtra("store_id", value);
		}
		Log.i("VALUE","VALUE: "+value);
		context.startService(intent);
		/*progUpload.setVisibility(View.VISIBLE);*/

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EventTracker.endFlurrySession(context);
		//unregisterReceiver(new BroadCast_AppUpgrade());
		super.onStop();
//				try
//				{
//					handler.removeCallbacks(runnable);
//				}catch(Exception ex)
//				{
//					ex.printStackTrace();
//				}
	}

	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter
	{
		Activity context;
		ArrayList<String> pages;
		boolean localGallery;
		ArrayList<String>title;
		public PageAdapter(FragmentManager fm,Activity context,ArrayList<String> pages,ArrayList<String>title,boolean localGallery) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.pages=pages;
			this.localGallery=localGallery;
			this.title=title;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new PageFragment(pages.get(position),title.get(position), context,localGallery);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return pages.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Object obj = super.instantiateItem(container, position);
			mPager.setObjectForPosition(obj, position);
			return obj;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			if(object != null){
				return ((Fragment)object).getView() == view;
			}else{
				return false;
			}
		}

		/*@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}*/
	}

	private class SplashLauncher extends Thread {
		@Override
		/**
		 * Sleep for some time and than start new activity.
		 */
		public void run() {
			try {
				// Sleeping
				Thread.sleep(SLEEP_TIME*1000);
			} catch (Exception e) {

			}

			/*// Start main activity
			Intent mainactivity = new Intent(context,  SplashScreen.class);
			startActivity(mainactivity);
			finish();
			overridePendingTransition(0, 0);*/
			enter();
			/*mPager.setVisibility(View.VISIBLE);*/

		}
	}	

	@SuppressLint("ValidFragment")
	public  class PageFragment extends Fragment 
	{
		String url="";
		Activity context;
		View view;
		ImageView splashImage;
		ProgressBar prog;
		TextView txtPlaceText;
		TextView imageTitle;
		TextView imageDesc;
		Animation anim;
		boolean localGallery;
		String title;
		public PageFragment()
		{

		}
		public PageFragment(String url,String title,Activity context,boolean localGallery)
		{
			this.url=url;
			this.context=context;
			this.localGallery=localGallery;
			this.title=title;
			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
			anim.setDuration(1200);
		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			view=inflater.inflate(R.layout.splashimagelayout, null);
			splashImage=(ImageView)view.findViewById(R.id.splImage);
			prog=(ProgressBar)view.findViewById(R.id.progressBar);
			txtPlaceText=(TextView)view.findViewById(R.id.txtPlaceText);
			txtPlaceText.setText(getResources().getString(R.string.actionBarTitle));
			imageTitle=(TextView)view.findViewById(R.id.imageTitle);
			imageDesc=(TextView)view.findViewById(R.id.imageDesc);


			WebView webview = (WebView)view.findViewById(R.id.splashWeb);
			webview.getSettings().setJavaScriptEnabled(true);
			webview.loadDataWithBaseURL("","<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\"><style type=\"text/css\">body{color:#FFF;background-color:#A1C365;}h2,h3,h4,h5{margin:0px;padding:0px;}h2{font-size:25px;}h3{font-size:17px;}h4{font-size:15px;}h5{font-size:10px;color: #516233; }</style></head><body>"+title+"</body></html>", "text/html", "UTF-8", "");
			//			webview.setBackgroundColor(0x00000000);

			//			webview.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

			Log.i("TITLE","TITLE " +title);
			/*	txtPlaceText.setShadowLayer(5, 1, 3, Color.parseColor("#112e73"));*/
			/*			txtPlaceText.startAnimation(anim);*/

			try
			{
				if(!localGallery)
				{
					imageLoader.displayImage(IMAGE_URL+url, splashImage,options, new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.VISIBLE);
							txtPlaceText.setVisibility(View.VISIBLE);

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.INVISIBLE);
							txtPlaceText.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.INVISIBLE);
							txtPlaceText.setVisibility(View.INVISIBLE);
						}

						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.INVISIBLE);
							txtPlaceText.setVisibility(View.VISIBLE);
						}
					});
				}
				else
				{
					imageLoader.displayImage(url, splashImage,options, new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String imageUri, View view) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.VISIBLE);
							txtPlaceText.setVisibility(View.VISIBLE);

						}

						@Override
						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.INVISIBLE);
							txtPlaceText.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.INVISIBLE);
							txtPlaceText.setVisibility(View.INVISIBLE);
						}

						@Override
						public void onLoadingCancelled(String imageUri, View view) {
							// TODO Auto-generated method stub
							prog.setVisibility(View.INVISIBLE);
							txtPlaceText.setVisibility(View.VISIBLE);
						}
					});
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return view;
		}

	}


	void DeleteRecursive(File file) {
		/*if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
			{

				Log.i("Delete:", "File "+child.getAbsolutePath());
				DeleteRecursive(child);
			}

		fileOrDirectory.delete();*/

		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					DeleteRecursive(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}


	@Override
	public void onReceiveGalleryResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		ArrayList<String>GALLERY_ID=new ArrayList<String>();
		ArrayList<String>STORE_ID=new ArrayList<String>();
		ArrayList<String>SOURCE=new ArrayList<String>();
		ArrayList<String>TITLE=new ArrayList<String>();
		ArrayList<String>IMAGE_DATE=new ArrayList<String>();

		String status=resultData.getString("SEARCH_STATUS");
		String message=resultData.getString("MESSAGE");


		if(status.equalsIgnoreCase("true"))
		{


			GALLERY_ID=resultData.getStringArrayList("ID");
			STORE_ID=resultData.getStringArrayList("STORE_ID");
			SOURCE=resultData.getStringArrayList("SOURCE");
			TITLE=resultData.getStringArrayList("TITLE");
			IMAGE_DATE=resultData.getStringArrayList("IMAGE_DATE");

			/*			ID.add(0, "-1");
			TITLE.add(0, "Add Image");
			SOURCE.add(0, "");
			 */

			try
			{

				PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, SOURCE,TITLE,false);
				mPager.setAdapter(adapter);
				//				open.setVisibility(View.VISIBLE);
				chooseLocation.setVisibility(View.GONE);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			//mPager.setTransitionEffect(TransitionEffect.ZoomIn);

			Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
			anim.setFillAfter(true);
			appSplash.startAnimation(anim);
			appSplash.setVisibility(View.GONE);

			try
			{

				//Handler


//								runnable = new Runnable() {
//				
//									@Override
//									public void run() {
//										// TODO Auto-generated method stub
//										try{
//											Log.i("Current Item ", "Current Item : "+mPager.getCurrentItem()+" Adapter count "+mPager.getAdapter().getCount());
//											if(mPager.getCurrentItem()!=mPager.getAdapter().getCount()-1)
//											{
//												mPager.setCurrentItem(mPager.getCurrentItem()+1,true);
//												
//												try
//												{
//
//												}catch(Exception ex)
//												{
//													ex.printStackTrace();
//												}
//											}
//											else if(mPager.getCurrentItem()==mPager.getAdapter().getCount()-1)
//											{
//												mPager.setCurrentItem(0);							
//											}
//				
//										}catch(Exception ex)
//										{
//											ex.printStackTrace();
//										}
//										handler.postDelayed(this, 2500);
//									}
//				
//								};
//								handler.postDelayed(runnable,5000);

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}


			mPager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub
					try
					{
						

//						ImageView ivTitle = (ImageView)mPager.findViewById(R.id.splImage);
//
//						Animation fadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
//						ivTitle.startAnimation(fadeInAnimation );
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int state) {
					// TODO Auto-generated method stub
					if(state!=ViewPager.SCROLL_STATE_IDLE)
					{
						//						handler.removeCallbacks(runnable);
					}
					else if(state==ViewPager.SCROLL_STATE_IDLE)
					{
						//						handler.postDelayed(runnable, 5000);
						//						Log.i("CURRENT ","CURRENT ITEM "+mPager.getAdapter().getCount());
						//						if(mPager.getCurrentItem()==mPager.getAdapter().getCount())
						//						{
						//							mPager.setCurrentItem(0);							
						//						}
					}
				}
			});

		}else
		{
			//			showToast(message);
			PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, urls,titles,true);
			mPager.setAdapter(adapter);
			Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
			anim.setFillAfter(true);
			appSplash.startAnimation(anim);
			appSplash.setVisibility(View.GONE);
			//			open.setVisibility(View.VISIBLE);
			//			enter();
		}
	}

	void showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			Log.i("Enter", "Enter loc");
			//open.setVisibility(View.VISIBLE);

			SharedPreferences prefs=getSharedPreferences("location", 0);
			Editor editor=prefs.edit();

			String status=resultData.getString("status");
			String latitued = null;
			String longitude = null;
			latitued=resultData.getString("Lat");
			longitude=resultData.getString("Longi");
			if(status.equalsIgnoreCase("set"))
			{

				editor.putString("latitued", latitued);
				editor.putString("longitude", longitude);
				editor.commit();
				Log.i("LOCATION  ", "LOCATION COMMITED "+latitued+","+longitude);
			}
			else
			{
				editor.putString("latitued", latitued);
				editor.putString("longitude", longitude);
				editor.commit();
				Log.i("LOCATION  ", "LOCATION COMMITED EXCEPTION "+latitued+","+longitude);
			}


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}



	}






}
