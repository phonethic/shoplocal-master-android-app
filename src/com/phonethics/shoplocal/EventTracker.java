package com.phonethics.shoplocal;

import java.util.Map;

import com.localytics.LocalyticsSession;
import com.phonethics.shoplocal.*;

import android.content.Context;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.flurry.android.FlurryAgent;

public class EventTracker {

	static LocalyticsSession localyticsSession;
	static Context eventContext;

	public static void startLocalyticsSession(Context context){

		try
		{
			String keyL = context.getResources().getString(R.string.localyticsKey);
			eventContext = context;
			//facebook app activation event
			//this function is called here as it needs to be called from onResume() of every activity.

			//AppEventsLogger.activateApp(context, context.getString(R.string.fb_appid));

			localyticsSession = new LocalyticsSession(context, keyL);
			localyticsSession.open();
			localyticsSession.upload();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void endLocalyticsSession(Context context){
		try
		{
			localyticsSession.close();
			localyticsSession.upload();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public static void logEvent(String event, boolean isScreen){
		try
		{
			FlurryAgent.logEvent(event);

			if(isScreen)
				localyticsSession.tagScreen(event);
			else
				localyticsSession.tagEvent(event);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void logEvent(String event, Map<String, String> param){
		try
		{
			FlurryAgent.logEvent(event, param);

			localyticsSession.tagEvent(event, param);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public static void startFlurrySession(Context context){
		try
		{
			String keyF = context.getResources().getString(R.string.flurryKey);

			FlurryAgent.onStartSession(context, keyF);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void endFlurrySession(Context context){
		try
		{
			FlurryAgent.onEndSession(context);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void trackLocation(boolean trackLocation){
		try
		{
			FlurryAgent.setReportLocation(trackLocation);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void reportException(String errorId, String message, String errorClass){
		try
		{
			FlurryAgent.onError(errorId, message, errorClass);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void setUserAge(Context context, int age){
		try
		{
			if(age<1 || age>109){
				Toast.makeText(context, "Invalid age " + age , Toast.LENGTH_SHORT).show();
			}
			else{
				FlurryAgent.setAge(age);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void setUserGender(byte gender){
		try
		{
			FlurryAgent.setGender(gender);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public static String getEventFromDrawer(String name){

		try
		{
			if(name.equals(eventContext.getResources().getString(R.string.itemAllStores))){
				return "Tab_MarketPlace";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemDiscover))){
				return "Tab_OffersStream";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemMyShoplocalOffers))){
				return "Tab_MyShoplocal";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemMyProfile))){
				return "Tab_CustomerProfile";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemLogin))){
				return "Tab_CustomerLogin";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemChangeLocation))){
				return "Tab_Location";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemSettings))){
				return "Tab_Settings";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemFavouriteStores))){
				return "Tab_FavouriteBusiness";
			}


			else if(name.equals(eventContext.getResources().getString(R.string.itemAboutShoplocal))){
				return "Tab_AboutShoplocal";
			}

			else if(name.equals(eventContext.getResources().getString(R.string.itemContactUs))){
				return "Tab_ContactUs";
			}


			else if(name.equals(eventContext.getResources().getString(R.string.itemShare))){
				return "Tab_ShareThisApp";
			}

			else 
				return "";
		}catch(Exception ex)
		{
			
			ex.printStackTrace();
			return "";
		}
	}



}
