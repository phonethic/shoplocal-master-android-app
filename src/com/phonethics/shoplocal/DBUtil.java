package com.phonethics.shoplocal;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.ViewGroup.MarginLayoutParams;

public class DBUtil {


	private final 			Context 		ourContext;
//	private static final 	String 			DATABASE_NAME = "HyperCity.db";
	private static final 	String 			DATABASE_NAME = "Shoplocal.db";
	private static final 	int  			DATABASE_VERSOIN=3;
	private 				DbHelper 		dbHelper;
	private 				SQLiteDatabase 	sqlDataBase;
	private static final 	String 			MERCHANT_TABLE	=	"MERCHANT";

	/*
	 * Table Name
	 */
	private static final 	String 			TABLE_BUSINESS			 ="BUSINESS";
	private static final 	String 			TABLE_BUSINESS_STORE	 ="BUSINESS_STORE";
	private static final 	String 			TABLE_STORE_TIME		 ="STORE_TIME";
	private static final    String 			TABLE_TEXT_SEARCH		 ="TEXT_SEARCH";
	private static final    String 			TABLE_GEOLOCATION_SEARCH ="GEOLOCATION_SEARCH";



	private static final String TABLE_DATE_CATEGORIES ="DATE_CATEGORIES";

	//Tables
	private static final 	String 			TABLE_AREAS 				="AREA_DESCRIPTION";
	private static final    String 			TABLE_PLACE_CHOOSER	 		="MY_PLACES";
	private static final    String 			TABLE_GEOTEXT_SEARCH	 	="ALL_PLACES";
	private static final    String 			TABLE_GEOTEXT_FAV_SEARCH 	="FAVOURITE_PLACES";
	private static final    String 			TABLE_NEWS_FEED	 			="NEWS_FEEDS";


	/*
	 * BUSINESS FIELD'S
	 */
	public static final String		KEY_BUSINESS_ID								="id";
	public static final String		KEY_BUSINESS_USER_ID						="business_user_id";
	public static final String		KEY_BUSINESS_NAME							="business_name";
	public static final String		KEY_BUSINESS_DESCRIPTION					="description";
	public static final String		KEY_BUSINESS_TYPE_ID						="business_type_id";
	public static final String		KEY_BUSINESS_YEAR_OF_ESTABLISHMENT			="year_of_establish";
	public static final String		KEY_BUSINESS_WEB							="website";
	public static final String		KEY_BUSINESS_LOGO							="logo";


	/*
	 * STORE FIELD'S
	 */
	public static final String		KEY_STORE_ID								="id";
	public static final String		KEY_STORE_BUSINESS_ID						="business_id";
	public static final String		KEY_STORE_NAME								="store_name";
	public static final String		KEY_STORE_BUILDING							="building";
	public static final String		KEY_STORE_STREET 						    ="street";
	public static final String		KEY_STORE_LANDMARK							="landmark";
	public static final String		KEY_STORE_AREA								="area";
	public static final String		KEY_STORE_PINCODE							="pincode";
	public static final String		KEY_STORE_CITY								="city";
	public static final String		KEY_STORE_STATE								="state";
	public static final String		KEY_STORE_COUNTRY							="country";
	public static final String		KEY_STORE_GEOLOCATION						="geolocation";
	public static final String		KEY_STORE_TEL_NO1							="tel_no1";
	public static final String		KEY_STORE_TEL_NO2							="tel_no2";
	public static final String		KEY_STORE_MOB_NO1							="mob_no1";
	public static final String		KEY_STORE_MOB_NO2							="mob_no2";
	public static final String		KEY_STORE_FAXNO1							="fax_no1";
	public static final String		KEY_STORE_FAXNO2							="fax_no2";
	public static final String		KEY_STORE_TOLL_FREE_NO1						="toll_free_no1";
	public static final String		KEY_STORE_TOLL_FREE_NO2						="toll_free_no2";
	public static final String		KEY_STORE_PHOTO								="photo";


	/*
	 * STORE OPERATION TIME FILED'S
	 */
	public static final String		KEY_STOREOP_ID								="id";
	public static final String		KEY_STOREOP_STORE_ID						="store_id";
	public static final String		KEY_STOREOP_DISPLAY_TIME_STATUS				="display_time_status";
	public static final String		KEY_STOREOP_DUAL_TIME_STATUS				="dual_time_status";
	public static final String		KEY_STOREOP_TIME1_OPEN						="time1_open";
	public static final String		KEY_STOREOP_TIME1_CLOSE						="time1_close";
	public static final String		KEY_STOREOP_TIME2_OPEN						="time2_open";
	public static final String		KEY_STOREOP_TIME2_CLOSE						="time2_close";



	/*
	 * TEXT SEARCH FIELD'S
	 */
	public static final String		KEY_TEXT_SEARCH_STORE_ID					="id";
	public static final String		KEY_TEXT_SEARCH_STORE_NAME					="store_name";
	public static final String		KEY_TEXT_SEARCH_AREA						="area";
	public static final String		KEY_TEXT_SEARCH_CITY						="city";
	public static final String		KEY_TEXT_SEARCH_MOB_NO1						="mob_no1";
	public static final String		KEY_TEXT_SEARCH_PHOTO						="photo";
	public static final String 		KEY_TEXT_SEARCH_TOTAL_PAGES					="total_page";
	public static final String 		KEY_TEXT_SEARCH_TOTAL_RECORD				="total_record";

	/*
	 * GEO-LOCATION SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_GEO_SEARCH_ID							="id";
	public static final String		KEY_GEO_SEARCH_STORE_NAME					="store_name";
	public static final String		KEY_GEO_SEARCH_AREA							="area";
	public static final String		KEY_GEO_SEARCH_CITY							="city";
	public static final String		KEY_GEO_SEARCH_MOB_NO1						="mob_no1";
	public static final String		KEY_GEO_SEARCH_PHOTO						="photo";
	public static final String		KEY_GEO_SEARCH_DISTANCE						="distance";
	public static final String 		KEY_GEO_SEARCH_TOTAL_PAGES					="total_page";
	public static final String 		KEY_GEO_SEARCH_TOTAL_RECORD					="total_record";

	/*
	 * TEXT-GEO-LOCATION SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_GEO_TEXT_SEARCH_AUTO_ID					="_id";
	public static final String		KEY_GEO_TEXT_SEARCH_ID						="id";
	public static final String		KEY_GEO_TEXT_SEARCH_PLACE_PARENT			="place_parent";
	public static final String		KEY_GEO_TEXT_SEARCH_STORE_NAME				="store_name";
	public static final String		KEY_GEO_TEXT_SEARCH_DESCRIPTION				="description";
	public static final String		KEY_GEO_TEXT_SEARCH_BUILDING				="building";
	public static final String		KEY_GEO_TEXT_SEARCH_STREET					="street";
	public static final String		KEY_GEO_TEXT_SEARCH_LANDMARK				="landmark";
	public static final String		KEY_GEO_TEXT_SEARCH_AREA_ID					="area_id";
	public static final String		KEY_GEO_TEXT_SEARCH_AREA					="area";
	public static final String		KEY_GEO_TEXT_SEARCH_CITY					="city";
	public static final String		KEY_GEO_TEXT_SEARCH_MOB_NO1					="mob_no1";
	public static final String		KEY_GEO_TEXT_SEARCH_MOB_NO2					="mob_no2";
	public static final String		KEY_GEO_TEXT_SEARCH_MOB_NO3					="mob_no3";

	public static final String		KEY_GEO_TEXT_SEARCH_TEL_NO1					="tel_no1";
	public static final String		KEY_GEO_TEXT_SEARCH_TEL_NO2					="tel_no2";
	public static final String		KEY_GEO_TEXT_SEARCH_TEL_NO3					="tel_no3";


	public static final String		KEY_GEO_TEXT_SEARCH_PHOTO					="photo";
	public static final String		KEY_GEO_TEXT_SEARCH_DISTANCE				="distance";
	public static final String 		KEY_GEO_TEXT_SEARCH_TOTAL_PAGES				="total_page";
	public static final String 		KEY_GEO_TEXT_SEARCH_TOTAL_RECORD			="total_record";
	public static final String 		KEY_GEO_TEXT_SEARCH_CURRENT_PAGE			="current_page";
	public static final String 		KEY_GEO_TEXT_TOTAL_LIKE						="total_like";
	public static final String 		KEY_GEO_TEXT_VERIFIED						="verified";
//	public static final String 		KEY_GEO_TEXT_SEARCH_FAVOURITED				="favourite";
	public static final String 		KEY_GEO_TEXT_SEARCH_HAS_OFFER				="has_offer";
	public static final String 		KEY_GEO_TEXT_SEARCH_OFFER_TITLE				="title";



	/*
	 * FAV-TEXT-GEO-LOCATION SEARCH FIELD'S
	 * 
	 */
	public static final String		KEY_GEO_TEXT_FAV_AUTO_ID				="_id";
	public static final String		KEY_GEO_TEXT_FAV_SEARCH_ID				="id";
	public static final String		KEY_GEO_TEXT_FAV_PLACE_PARENT			="place_parent";
	public static final String		KEY_GEO_TEXT_FAV_STORE_NAME				="store_name";
	public static final String		KEY_GEO_TEXT_FAV_DESCRIPTION			="description";
	public static final String		KEY_GEO_TEXT_FAV_BUILDING				="building";
	public static final String		KEY_GEO_TEXT_FAV_STREET					="street";
	public static final String		KEY_GEO_TEXT_FAV_LANDMARK				="landmark";
	public static final String		KEY_GEO_TEXT_FAV_AREA_ID				="area_id";
	public static final String		KEY_GEO_TEXT_FAV_AREA					="area";
	public static final String		KEY_GEO_TEXT_FAV_CITY					="city";

	public static final String		KEY_GEO_TEXT_FAV_MOB_NO1				="mob_no1";
	public static final String		KEY_GEO_TEXT_FAV_MOB_NO2				="mob_no2";
	public static final String		KEY_GEO_TEXT_FAV_MOB_NO3				="mob_no3";

	public static final String		KEY_GEO_TEXT_FAV_TEL_NO1				="tel_no1";
	public static final String		KEY_GEO_TEXT_FAV_TEL_NO2				="tel_no2";
	public static final String		KEY_GEO_TEXT_FAV_TEL_NO3				="tel_no3";



	public static final String		KEY_GEO_TEXT_FAV_PHOTO					="photo";
	public static final String		KEY_GEO_TEXT_FAV_DISTANCE				="distance";
	public static final String 		KEY_GEO_TEXT_FAV_TOTAL_PAGES			="total_page";
	public static final String 		KEY_GEO_TEXT_FAV_TOTAL_RECORD			="total_record";
	public static final String 		KEY_GEO_TEXT_FAV_CURRENT_PAGE			="current_page";
	public static final String 		KEY_GEO_TEXT_FAV_TOTAL_LIKE				="total_like";
	public static final String 		KEY_GEO_TEXT_FAV_VERIFIED				="verified";
	public static final String 		KEY_GEO_TEXT_FAV_FAVOURITED				="favourite";
	public static final String 		KEY_GEO_TEXT_FAV_SEARCH_HAS_OFFER		="has_offer";
	public static final String 		KEY_GEO_TEXT_FAV_SEARCH_OFFER_TITLE		="title";





	public static final String	rowId 				= "ROW_ID";
	public static final String	id					= "ID";
	public static final String	businessId 			= "BUSINESS_ID";
	public static final String	store_name			= "STORE_NAME";
	public static final String 	building			= "BUILDING";
	public static final String	street				= "STREET";
	public static final String 	landmark			= "LAND_MARK";
	public static final String	area				= "AREA";
	public static final String 	pincode				= "PIN_CODE";
	public static final String	city				= "CITY";
	public static final String	state				= "STATE";
	public static final String	country				= "COUNTRY";
	public static final String	latitude			= "LATITUDE";
	public static final String	longitude			= "LONGITUDE";
	public static final String	tel_no1				= "TEL_NO1";
	public static final String	tel_no2				= "TEL_NO2";
	public static final String	mob_no1				= "MOB_NO1";
	public static final String	mob_no2				= "MOB_NO2";
	public static final String	fax_no1				= "FAX_NO1";
	public static final String	fax_no2				= "FAX_NO2";
	public static final String	toll_free_no1		= "TOLL_FREE_NO1";
	public static final String	toll_free_no2		= "TOLL_FREE_NO2";
	public static final String	photo				= "PHOTO";
	public static final String	fav					= "FAVOURITE";


	// fields for TABLE_AREAS
	public static final String key_area_auto_id		="_id";
	public static final String area_id	 			= "id";
	public static final String area_name			= "sublocality";
	public static final String area_locality		= "locality";
	public static final String area_latitude		= "latitude";
	public static final String area_longitude		= "longitude";	
	public static final String area_pincode			= "pincode";
	public static final String area_city	 		= "city";
	public static final String area_state			= "state";
	public static final String area_country			= "country";
	public static final String area_country_code 	= "country_code";
	public static final String area_iso_code		= "iso_code";
	public static final String area_placeID			= "shoplocal_place_id";
	public static final String area_published_status= "published_status";
	public static final String area_active			= "is_active";


	// Fields for Table Date Categories
	public static final String date_area_auto_id    ="_id";
	public static final String date_id	 		    = "DATE_ID";
	public static final String date_name	 	    = "DATE_NAME";

	//Fields For Table PLACES

	public static final String KEY_PLACE_AUTO_ID="_id";
	public static final String KEY_PLACE_ID="PLACE_ID";
	public static final String KEY_PLACE_PARENT="place_parent";
	public static final String KEY_PLACE_NAME="place_name";

	public static final String KEY_PLACE_DESCRIPTION="place_description";
	public static final String KEY_PLACE_BUILDING="building";

	public static final String KEY_PLACE_STREET="place_street";
	public static final String KEY_PLACE_LANDMARK="place_landmark";

	public static final String KEY_PLACE_AREA="place_area";
	public static final String KEY_PLACE_PINCODE="place_pincode";
	public static final String KEY_PLACE_CITY="place_city";
	public static final String KEY_PLACE_STATE="place_state";

	public static final String KEY_PLACE_COUNTRY="place_country";
	public static final String KEY_PLACE_LATITUDE="place_latitude";
	public static final String KEY_PLACE_LONGITUDE="place_longitude";



	public static final String KEY_PLACE_TELLNO1="place_tellno1";
	public static final String KEY_PLACE_TELLNO2="place_tellno2";
	public static final String KEY_PLACE_TELLNO3="place_tellno3";


	public static final String KEY_PLACE_MOBNO1="place_mobno1";
	public static final String KEY_PLACE_MOBNO2="place_mobno2";
	public static final String KEY_PLACE_MOBNO3="place_mobno3";


	public static final String KEY_PLACE_image_url="place_image_url";
	public static final String KEY_PLACE_email="place_email";
	public static final String KEY_PLACE_website="place_website";

	public static final String KEY_PLACE_facebook="place_facebook";
	public static final String KEY_PLACE_twitter="place_twitter";
	public static final String KEY_PLACE_place_status="place_status";
	public static final String KEY_PLACE_place_published="place_published";

	public static final String KEY_PLACE_place_total_like="place_total_like";
	public static final String KEY_PLACE_place_total_share="place_total_share";
	public static final String KEY_PLACE_place_total_view="place_total_view";


	//Fields For Table News Feeds

	public static final String KEY_NEWS_FEED_AUTO_ID="_id";
	public static final String KEY_NEWS_FEED_ID="id";
	public static final String KEY_NEWS_FEED_NAME="name";
	public static final String KEY_NEWS_FEED_AREA_ID="area_id";

	public static final String KEY_NEWS_FEED_MOBNO1="mobno1";
	public static final String KEY_NEWS_FEED_MOBNO2="mobno2";
	public static final String KEY_NEWS_FEED_MOBNO3="mobno3";


	public static final String KEY_NEWS_FEED_TELLNO1="tellno1";
	public static final String KEY_NEWS_FEED_TELLNO2="tellno2";
	public static final String KEY_NEWS_FEED_TELLNO3="tellno3";

	public static final String KEY_NEWS_FEED_LATITUDE="latitude";
	public static final String KEY_NEWS_FEED_LONGITUDE="longitude";
	public static final String KEY_NEWS_FEED_DISTANCE="distance";

	public static final String KEY_NEWS_FEED_POST_ID="post_id";
	public static final String KEY_NEWS_FEED_POST_TYPE="type";

	public static final String KEY_NEWS_FEED_POST_TITLE="title";
	public static final String KEY_NEWS_FEED_DESCRIPTION="description";


	public static final String KEY_NEWS_FEED_image_url1="image_url1";
	public static final String KEY_NEWS_FEED_image_url2="image_url2";
	public static final String KEY_NEWS_FEED_image_url3="image_url3";

	public static final String KEY_NEWS_FEED_thumb_url1="thumb_url1";
	public static final String KEY_NEWS_FEED_thumb_url2="thumb_url2";
	public static final String KEY_NEWS_FEED_thumb_url3="thumb_url3";

	public static final String KEY_NEWS_DATE="date";
	public static final String KEY_NEWS_IS_OFFERED="is_offered";
	public static final String KEY_NEWS_OFFER_DATE_TIME="offer_date_time";

	public static final String KEY_NEWS_FEED_place_total_like="total_like";
	public static final String KEY_NEWS_FEED_place_total_share="total_share";
	
	public static final String KEY_NEWS_FEED_verified="verified";

	public static final String KEY_NEWS_FEED_TOTAL_PAGE	="total_page";
	public static final String KEY_NEWS_FEED_TOTAL_RECORD="total_record";
	public static final String KEY_NEWS_FEED_CURRENT_PAGE="current_page";











	public DBUtil(Context c)
	{
		ourContext=c;
	}


	private static class  DbHelper extends SQLiteOpenHelper{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSOIN);
			// TODO Auto-generated constructor stub
			try
			{
				context.deleteDatabase("HyperCity.db");
			}catch(Exception ex)
			{
				Log.i("DATABASE CONSTRUCT", "DATABASE CONSTRUCT");
				ex.printStackTrace();
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			
			db.execSQL("CREATE TABLE "+
					MERCHANT_TABLE +" ("+
					rowId		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					id 			+" TEXT, "+
					businessId 	+" TEXT, "+
					store_name 	+" TEXT, "+
					building 	+" TEXT, "+
					street 		+" TEXT, "+
					landmark 	+" TEXT, "+
					area 		+" TEXT, "+
					pincode 	+" TEXT, "+
					city 		+" TEXT, "+
					state 		+" TEXT, "+
					country 	+" TEXT, "+
					latitude	+" TEXT, "+
					longitude	+" TEXT, "+
					tel_no1		+" TEXT, "+
					tel_no2		+" TEXT, "+
					mob_no1		+" TEXT, "+
					mob_no2		+" TEXT, "+
					fax_no1		+" TEXT, "+
					fax_no2		+" TEXT, "+
					toll_free_no1	+" TEXT, "+
					toll_free_no2	+" TEXT, "+
					photo			+" TEXT, "+
					fav			+ " TEXT);");	

			/*
			 * Creating Business store
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_BUSINESS_STORE +" ("+
					KEY_STORE_ID		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_STORE_BUSINESS_ID 			+" INTEGER, "+
					KEY_STORE_NAME 	+" TEXT, "+
					KEY_STORE_BUILDING 	+" TEXT, "+
					KEY_STORE_STREET 	+" TEXT, "+
					KEY_STORE_LANDMARK 		+" TEXT, "+
					KEY_STORE_AREA 	+" TEXT, "+
					KEY_STORE_PINCODE 		+" INTEGER, "+
					KEY_STORE_CITY 	+" TEXT, "+
					KEY_STORE_STATE 		+" TEXT, "+
					KEY_STORE_COUNTRY 		+" TEXT, "+
					KEY_STORE_GEOLOCATION 	+" TEXT, "+
					KEY_STORE_TEL_NO1	+" TEXT, "+
					KEY_STORE_TEL_NO2	+" TEXT, "+
					KEY_STORE_MOB_NO1		+" TEXT, "+
					KEY_STORE_MOB_NO2		+" TEXT, "+
					KEY_STORE_FAXNO1		+" TEXT, "+
					KEY_STORE_FAXNO2		+" TEXT, "+
					KEY_STORE_TOLL_FREE_NO1		+" TEXT, "+
					KEY_STORE_TOLL_FREE_NO2		+" TEXT, "+
					KEY_STORE_PHOTO	+ " TEXT);");	

			/*
			 * Creating store operation
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_STORE_TIME +" ("+
					KEY_STOREOP_ID		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_STOREOP_STORE_ID 			+" INTEGER, "+
					KEY_STOREOP_DISPLAY_TIME_STATUS 	+" INTEGER, "+
					KEY_STOREOP_DUAL_TIME_STATUS 	+" INTEGER, "+
					KEY_STOREOP_TIME1_OPEN 		+" TEXT, "+
					KEY_STOREOP_TIME1_CLOSE 	+" TEXT, "+
					KEY_STOREOP_TIME2_OPEN 		+" TEXT, "+
					KEY_STOREOP_TIME2_CLOSE 	+ " TEXT);");	

			/*
			 * Creating Business
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_BUSINESS +" ("+
					KEY_BUSINESS_ID		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_BUSINESS_USER_ID 			+" INTEGER, "+
					KEY_BUSINESS_NAME 	+" TEXT, "+
					KEY_BUSINESS_DESCRIPTION 	+" TEXT, "+
					KEY_BUSINESS_TYPE_ID 		+" INTEGER, "+
					KEY_BUSINESS_YEAR_OF_ESTABLISHMENT 	+" INTEGER, "+
					KEY_BUSINESS_WEB 		+" TEXT, "+
					KEY_BUSINESS_LOGO 	+ " TEXT);");	


			/*
			 * TEXT SEARCH FIELD'S

			public static final String		KEY_TEXT_SEARCH_STORE_NAME					="store_name";
			public static final String		KEY_TEXT_SEARCH_AREA						="area";
			public static final String		KEY_TEXT_SEARCH_CITY						="city";
			public static final String		KEY_TEXT_SEARCH_MOB_NO1						="mob_no1";
			public static final String		KEY_TEXT_SEARCH_PHOTO						="photo";
			public static final String 		KEY_TEXT_SEARCH_TOTAL_PAGES					="total_page";
			public static final String 		KEY_TEXT_SEARCH_TOTAL_RECORD				="total_record";*/


			/*
			 * Creating Text Search
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_TEXT_SEARCH +" ("+
					KEY_TEXT_SEARCH_STORE_ID		+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_TEXT_SEARCH_STORE_NAME 	+" TEXT, "+
					KEY_TEXT_SEARCH_AREA 		+" TEXT, "+
					KEY_TEXT_SEARCH_CITY 		+" TEXT, "+
					KEY_TEXT_SEARCH_MOB_NO1 	+" TEXT, "+
					KEY_TEXT_SEARCH_TOTAL_PAGES +" INTEGER, "+
					KEY_TEXT_SEARCH_TOTAL_RECORD + " INTEGER);");	
			/*
			 * Creating FAv Geo+Text Search
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_GEOTEXT_FAV_SEARCH +" ("+
					KEY_GEO_TEXT_FAV_AUTO_ID  +" INTEGER PRIMARY KEY AUTOINCREMENT , "+
					KEY_GEO_TEXT_FAV_SEARCH_ID		+" INTEGER NOT NULL UNIQUE, "+
					KEY_GEO_TEXT_FAV_PLACE_PARENT +" INTEGER,  "+
					KEY_GEO_TEXT_FAV_STORE_NAME 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_DESCRIPTION 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_BUILDING 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_STREET 		+" TEXT, "+
					KEY_GEO_TEXT_FAV_LANDMARK 		+" TEXT, "+
					KEY_GEO_TEXT_FAV_AREA_ID 		+" INTEGER, "+
					KEY_GEO_TEXT_FAV_AREA 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_CITY 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_MOB_NO1 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_MOB_NO2 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_MOB_NO3 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_TEL_NO1 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_TEL_NO2 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_TEL_NO3 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_PHOTO 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_DISTANCE 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_FAVOURITED 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_SEARCH_HAS_OFFER 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_SEARCH_OFFER_TITLE 	+" TEXT, "+
					KEY_GEO_TEXT_FAV_CURRENT_PAGE +" INTEGER, "+
					KEY_GEO_TEXT_FAV_TOTAL_PAGES +" INTEGER, "+
					KEY_GEO_TEXT_FAV_TOTAL_LIKE +" INTEGER, "+
					KEY_GEO_TEXT_FAV_VERIFIED +" TEXT, "+
					KEY_GEO_TEXT_FAV_TOTAL_RECORD + " INTEGER);");	

			/*
			 * Creating Geo+Text Search
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_GEOTEXT_SEARCH +" ("+
					KEY_GEO_TEXT_SEARCH_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_GEO_TEXT_SEARCH_ID		+" INTEGER NOT NULL UNIQUE, "+
					KEY_GEO_TEXT_SEARCH_PLACE_PARENT +" INTEGER,  "+
					KEY_GEO_TEXT_SEARCH_STORE_NAME 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_DESCRIPTION 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_BUILDING 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_STREET 		+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_LANDMARK 		+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_AREA_ID		+" INTEGER, "+
					KEY_GEO_TEXT_SEARCH_AREA 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_CITY 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_MOB_NO1 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_MOB_NO2 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_MOB_NO3 	+" TEXT, "+

					KEY_GEO_TEXT_SEARCH_TEL_NO1 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_TEL_NO2 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_TEL_NO3 	+" TEXT, "+


					KEY_GEO_TEXT_SEARCH_PHOTO 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_DISTANCE 	+" TEXT, "+
	
					KEY_GEO_TEXT_SEARCH_HAS_OFFER 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_OFFER_TITLE 	+" TEXT, "+
					KEY_GEO_TEXT_SEARCH_CURRENT_PAGE +" INTEGER, "+
					KEY_GEO_TEXT_SEARCH_TOTAL_PAGES +" INTEGER, "+
					KEY_GEO_TEXT_TOTAL_LIKE +" INTEGER, "+
					KEY_GEO_TEXT_VERIFIED +" TEXT, "+
					KEY_GEO_TEXT_SEARCH_TOTAL_RECORD + " INTEGER);");	



			/*public static final String KEY_PLACE_AUTO_ID="_id";
			public static final String KEY_PLACE_ID="id";
			public static final String KEY_PLACE_PARENT="place_parent";
			public static final String KEY_PLACE_NAME="place_name";

			public static final String KEY_PLACE_DESCRIPTION="place_description";
			public static final String KEY_PLACE_BUILDING="building";

			public static final String KEY_PLACE_STREET="place_street";
			public static final String KEY_PLACE_LANDMARK="place_landmark";

			public static final String KEY_PLACE_AREA="place_area";
			public static final String KEY_PLACE_PINCODE="place_pincode";
			public static final String KEY_PLACE_CITY="place_city";
			public static final String KEY_PLACE_STATE="place_state";

			public static final String KEY_PLACE_COUNTRY="place_country";
			public static final String KEY_PLACE_LATITUDE="place_latitude";
			public static final String KEY_PLACE_LONGITUDE="place_longitude";



			public static final String KEY_PLACE_TELLNO1="place_tellno1";
			public static final String KEY_PLACE_TELLNO2="place_tellno2";
			public static final String KEY_PLACE_TELLNO3="place_tellno3";


			public static final String KEY_PLACE_MOBNO1="place_mobno1";
			public static final String KEY_PLACE_MOBNO2="place_mobno2";
			public static final String KEY_PLACE_MOBNO3="place_mobno3";


			public static final String KEY_PLACE_image_url="place_image_url";
			public static final String KEY_PLACE_email="place_email";
			public static final String KEY_PLACE_website="place_website";

			public static final String KEY_PLACE_facebook="place_facebook";
			public static final String KEY_PLACE_twitter="place_twitter";
			public static final String KEY_PLACE_place_status="place_status";
			public static final String KEY_PLACE_place_published="place_published";

			public static final String KEY_PLACE_place_total_like="place_total_like";
			public static final String KEY_PLACE_place_total_share="place_total_share";
			public static final String KEY_PLACE_place_total_view="place_total_view";
			 */


			/*
			 * Creating TABLE_PLACE_CHOOSER 
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_PLACE_CHOOSER +" ("+
					KEY_PLACE_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_PLACE_ID		+" INTEGER NOT NULL UNIQUE, "+
					KEY_PLACE_PARENT +" INTEGER,  "+
					KEY_PLACE_NAME 	+" TEXT, "+
					KEY_PLACE_DESCRIPTION 	+" TEXT, "+
					KEY_PLACE_BUILDING 	+" TEXT, "+
					KEY_PLACE_STREET 		+" TEXT, "+
					KEY_PLACE_LANDMARK 		+" TEXT, "+
					KEY_PLACE_AREA 	+" TEXT, "+
					KEY_PLACE_PINCODE 	+" TEXT, "+
					KEY_PLACE_CITY 	+" TEXT, "+
					KEY_PLACE_STATE 	+" TEXT, "+
					KEY_PLACE_COUNTRY 	+" TEXT, "+
					KEY_PLACE_LATITUDE 	+" TEXT, "+
					KEY_PLACE_LONGITUDE 	+" TEXT, "+
					KEY_PLACE_TELLNO1 	+" TEXT, "+
					KEY_PLACE_TELLNO2 +" TEXT, "+
					KEY_PLACE_TELLNO3 +" TEXT, "+

					KEY_PLACE_MOBNO1 	+" TEXT, "+
					KEY_PLACE_MOBNO2 +" TEXT, "+
					KEY_PLACE_MOBNO3 +" TEXT, "+

					KEY_PLACE_image_url 	+" TEXT, "+
					KEY_PLACE_email +" TEXT, "+
					KEY_PLACE_website +" TEXT, "+

					KEY_PLACE_facebook 	+" TEXT, "+
					KEY_PLACE_twitter +" TEXT, "+
					KEY_PLACE_place_status +" TEXT, "+
					KEY_PLACE_place_published 	+" TEXT, "+

					KEY_PLACE_place_total_like +" TEXT, "+
					KEY_PLACE_place_total_share +" TEXT, "+
					KEY_PLACE_place_total_view +" TEXT);");	


			//			//Fields For Table News Feeds
			//
			//			public static final String KEY_NEWS_FEED_AUTO_ID="_id";
			//			public static final String KEY_NEWS_FEED_ID="id";
			//			public static final String KEY_NEWS_FEED_NAME="name";
			//			
			//			public static final String KEY_NEWS_FEED_MOBNO1="mobno1";
			//			public static final String KEY_NEWS_FEED_MOBNO2="mobno2";
			//			public static final String KEY_NEWS_FEED_MOBNO3="mobno3";
			//			
			//			
			//			public static final String KEY_NEWS_FEED_TELLNO1="tellno1";
			//			public static final String KEY_NEWS_FEED_TELLNO2="tellno2";
			//			public static final String KEY_NEWS_FEED_TELLNO3="tellno3";
			//			
			//			public static final String KEY_NEWS_FEED_LATITUDE="latitude";
			//			public static final String KEY_NEWS_FEED_LONGITUDE="longitude";
			//			public static final String KEY_NEWS_FEED_DISTANCE="distance";
			//			
			//			public static final String KEY_NEWS_FEED_POST_ID="post_id";
			//			public static final String KEY_NEWS_FEED_POST_TYPE="type";
			//			
			//			public static final String KEY_NEWS_FEED_POST_TITLE="title";
			//			public static final String KEY_NEWS_FEED_DESCRIPTION="description";
			//			
			//			
			//			public static final String KEY_NEWS_FEED_image_url1="image_url1";
			//			public static final String KEY_NEWS_FEED_image_url2="image_url2";
			//			public static final String KEY_NEWS_FEED_image_url3="image_url3";
			//			
			//			public static final String KEY_NEWS_FEED_thumb_url1="thumb_url1";
			//			public static final String KEY_NEWS_FEED_thumb_url2="thumb_url2";
			//			public static final String KEY_NEWS_FEED_thumb_url3="thumb_url3";
			//
			//			public static final String KEY_NEWS_DATE="date";
			//			public static final String KEY_NEWS_IS_OFFERED="is_offered";
			//			public static final String KEY_NEWS_OFFER_DATE_TIME="offer_date_time";
			//
			//			public static final String KEY_NEWS_FEED_place_total_like="total_like";
			//			public static final String KEY_NEWS_FEED_place_total_share="total_share";

			//			public static final String KEY_NEWS_FEED_TOTAL_PAGE	="total_page";
			//			public static final String KEY_NEWS_FEED_TOTAL_RECORD="total_record";
			//			public static final String KEY_NEWS_FEED_CURRENT_PAGE="current_page";


			/*
			 * Creating TABLE_NEWS_FEED 
			 */
			db.execSQL("CREATE TABLE IF NOT EXISTS "+
					TABLE_NEWS_FEED +" ("+
					KEY_NEWS_FEED_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					KEY_NEWS_FEED_ID		+" INTEGER , "+
					KEY_NEWS_FEED_AREA_ID	+" INTEGER , "+
					KEY_NEWS_FEED_NAME +" TEXT,  "+
					KEY_PLACE_NAME 	+" TEXT, "+

					KEY_NEWS_FEED_MOBNO1 	+" TEXT, "+
					KEY_NEWS_FEED_MOBNO2 +" TEXT, "+
					KEY_NEWS_FEED_MOBNO3 +" TEXT, "+

					KEY_NEWS_FEED_TELLNO1 	+" TEXT, "+
					KEY_NEWS_FEED_TELLNO2 +" TEXT, "+
					KEY_NEWS_FEED_TELLNO3 +" TEXT, "+

					KEY_NEWS_FEED_LATITUDE 	+" TEXT, "+
					KEY_NEWS_FEED_LONGITUDE 	+" TEXT, "+
					KEY_NEWS_FEED_DISTANCE 	+" TEXT, "+

					KEY_NEWS_FEED_POST_ID 	+" INTEGER NOT NULL UNIQUE, "+
					KEY_NEWS_FEED_POST_TYPE 	+" TEXT, "+

					KEY_NEWS_FEED_POST_TITLE 	+" TEXT, "+
					KEY_NEWS_FEED_DESCRIPTION 	+" TEXT, "+

					KEY_NEWS_FEED_image_url1 	+" TEXT, "+
					KEY_NEWS_FEED_image_url2 	+" TEXT, "+
					KEY_NEWS_FEED_image_url3 	+" TEXT, "+

					KEY_NEWS_FEED_thumb_url1 	+" TEXT, "+
					KEY_NEWS_FEED_thumb_url2 	+" TEXT, "+
					KEY_NEWS_FEED_thumb_url3 	+" TEXT, "+

					KEY_NEWS_DATE 				+" TEXT, "+
					KEY_NEWS_IS_OFFERED 		+" TEXT, "+
					KEY_NEWS_OFFER_DATE_TIME 	+" TEXT, "+

					KEY_NEWS_FEED_TOTAL_PAGE 	+" INTEGER, "+
					KEY_NEWS_FEED_TOTAL_RECORD	+" INTEGER, "+
					KEY_NEWS_FEED_CURRENT_PAGE 	+" INTEGER, "+

					KEY_NEWS_FEED_place_total_like 	+" TEXT, "+
					KEY_NEWS_FEED_verified	+" TEXT, "+
					KEY_NEWS_FEED_place_total_share +" TEXT);");	


			/*
			 * Create table AREA
			 */

			db.execSQL("CREATE TABLE IF NOT EXISTS " +
					TABLE_AREAS +" ("+
					key_area_auto_id + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					area_id + " INTEGER NOT NULL UNIQUE, " +
					area_name + " TEXT, " +
					area_locality + " TEXT, " +
					area_latitude + " TEXT, " +
					area_longitude + " TEXT, " +
					area_pincode + " TEXT, " +
					area_city + " TEXT, " +
					area_state + " TEXT, " +
					area_country + " TEXT, " +
					area_country_code + " TEXT, " +
					area_iso_code + " TEXT, " +
					area_placeID + " TEXT, " +
					area_published_status + " INTEGER, " +
					area_active + " INTEGER);");


			/*
			 * Create table Date Category
			 */

			db.execSQL("CREATE TABLE IF NOT EXISTS " +
					TABLE_DATE_CATEGORIES + " (" +
					date_area_auto_id + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
					date_id + " TEXT, " +
					date_name + " TEXT);");

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.i("Database", "DATABASE UPDATED");
			db.execSQL("DROP TABLE IF EXISTS "+MERCHANT_TABLE);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_BUSINESS);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_BUSINESS_STORE);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_STORE_TIME);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_TEXT_SEARCH);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_GEOTEXT_SEARCH);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_GEOTEXT_FAV_SEARCH);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_AREAS);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_DATE_CATEGORIES);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_PLACE_CHOOSER);
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_NEWS_FEED);
			onCreate(db);

		}


	}

	public  DBUtil open() throws SQLException
	{
		dbHelper=new DbHelper(ourContext);
		sqlDataBase=dbHelper.getWritableDatabase();

		return this;

	}

	public void close()
	{
		dbHelper.close();
	}


	/*
	 * Create Store
	 *
	 */
	public long create_store(int store_id,int store_bid,String store_name,String buildingName,String street,
			String landmark,String area,int pincode,String city,String state,String country,String geolocation,
			String telno1,String telno2,String mobno1,String mobno2,String faxno1,String faxno2,String toll_freeno1,String toll_freeno2,String photo)
	{
		Log.i("INSIDE ", "DATA BASE STORE");

		ContentValues cv=new ContentValues();
		cv.put(KEY_STORE_ID, store_id);
		cv.put(KEY_STORE_BUSINESS_ID, store_bid);
		cv.put(KEY_STORE_NAME, store_name);
		cv.put(KEY_STORE_BUILDING, buildingName);
		cv.put(KEY_STORE_STREET, street);
		cv.put(KEY_STORE_LANDMARK, landmark);
		cv.put(KEY_STORE_AREA, area);
		cv.put(KEY_STORE_PINCODE, pincode);
		cv.put(KEY_STORE_CITY, city);
		cv.put(KEY_STORE_STATE, state);
		cv.put(KEY_STORE_COUNTRY, country);
		cv.put(KEY_STORE_GEOLOCATION, geolocation);
		cv.put(KEY_STORE_TEL_NO1, telno1);
		cv.put(KEY_STORE_TEL_NO2, telno2);
		cv.put(KEY_STORE_TEL_NO1, telno1);
		cv.put(KEY_STORE_MOB_NO1, mobno1);
		cv.put(KEY_STORE_MOB_NO2, mobno2);
		cv.put(KEY_STORE_FAXNO1, faxno1);
		cv.put(KEY_STORE_FAXNO2, faxno2);
		cv.put(KEY_STORE_TOLL_FREE_NO1, toll_freeno1);
		cv.put(KEY_STORE_TOLL_FREE_NO2, toll_freeno2);
		cv.put(KEY_STORE_PHOTO, photo);
		return sqlDataBase.insert(TABLE_BUSINESS_STORE, null, cv);

	}

	/*
	 * Create Store operation
	 *
	 */
	public long create_store_operation(int storeop_id,int storeop_storeid,int storeop_disptime_status,
			int storeop_dualtime_status,String time1_open,String time1_close,String time2_open,String time2_close)
	{
		Log.i("INSIDE ", "DATA BASE STORE OPERATION");
		ContentValues cv=new ContentValues();
		cv.put(KEY_STOREOP_ID, storeop_id);
		cv.put(KEY_STOREOP_STORE_ID, storeop_storeid);
		cv.put(KEY_STOREOP_DISPLAY_TIME_STATUS, storeop_disptime_status);
		cv.put(KEY_STOREOP_DUAL_TIME_STATUS, storeop_dualtime_status);
		cv.put(KEY_STOREOP_TIME1_OPEN, time1_open);
		cv.put(KEY_STOREOP_TIME1_CLOSE, time1_close);
		cv.put(KEY_STOREOP_TIME2_OPEN, time2_open);
		cv.put(KEY_STOREOP_TIME2_CLOSE, time2_close);

		return sqlDataBase.insert(TABLE_STORE_TIME, null, cv);

	}

	/*
	 * Create BUSINESS
	 *
	 */
	//	public long create_business(int business_id,int business_uerid,String business_name,
	//			String business_description,String btypeid,int year_of_establishment,String web,String logo)
	//	{
	//		Log.i("INSIDE ", "DATA BASE STORE BUSINESS");
	//
	//		ContentValues cv=new ContentValues();
	//		cv.put(KEY_BUSINESS_ID, business_id);
	//		cv.put(KEY_BUSINESS_USER_ID, business_uerid);
	//		cv.put(KEY_BUSINESS_NAME, business_name);
	//		cv.put(KEY_BUSINESS_DESCRIPTION, business_description);
	//		cv.put(KEY_BUSINESS_TYPE_ID, btypeid);
	//		cv.put(KEY_BUSINESS_YEAR_OF_ESTABLISHMENT, year_of_establishment);
	//		cv.put(KEY_BUSINESS_WEB, web);
	//		cv.put(KEY_BUSINESS_LOGO, logo);
	//
	//		return sqlDataBase.insert(TABLE_BUSINESS, null, cv);
	//
	//	}


	/*
	 * Create GEO+TEXT
	 *
	 */
	/*public static final String		KEY_GEO_TEXT_SEARCH_STORE_NAME				="store_name";
	 *	public static final String		KEY_GEO_TEXT_SEARCH_ID						="id";
	public static final String		KEY_GEO_TEXT_SEARCH_BUILDING				="building";
	public static final String		KEY_GEO_TEXT_SEARCH_STREET					="street";
	public static final String		KEY_GEO_TEXT_SEARCH_LANDMARK				="landmakr";
	public static final String		KEY_GEO_TEXT_SEARCH_AREA					="area";
	public static final String		KEY_GEO_TEXT_SEARCH_CITY					="city";
	public static final String		KEY_GEO_TEXT_SEARCH_MOB_NO1					="mob_no1";
	public static final String		KEY_GEO_TEXT_SEARCH_PHOTO					="photo";
	public static final String		KEY_GEO_TEXT_SEARCH_DISTANCE				="distance";
	public static final String 		KEY_GEO_TEXT_SEARCH_TOTAL_PAGES				="total_page";
	public static final String 		KEY_GEO_TEXT_SEARCH_TOTAL_RECORD			="total_record";*/
	public long create_geotext(int geo_txt_id,int place_parent,String geo_text_search_storename,String description,
			String geo_text_building,String geo_text_street,String geo_text_landmark,int area_id,String geo_text_area,String geo_text_city,
			String geo_text_mobno1,String geo_text_mobno2,String geo_text_mobno3,String geo_text_telno1,String geo_text_telno2,String geo_text_telno3,String geo_text_photo,String distance,int total_pages,int total_record,int current_page,String fav,String hasoffer,String offertite,int total_like,String verified)
	{
		Log.i("INSIDE ", "DATA BASE STORE BUSINESS" +geo_text_search_storename+" "+geo_text_area);

		ContentValues cv=new ContentValues();
		cv.put(KEY_GEO_TEXT_SEARCH_ID, geo_txt_id);
		cv.put(KEY_GEO_TEXT_SEARCH_PLACE_PARENT, place_parent);
		cv.put(KEY_GEO_TEXT_SEARCH_STORE_NAME, geo_text_search_storename);
		cv.put(KEY_GEO_TEXT_SEARCH_DESCRIPTION, description);
		cv.put(KEY_GEO_TEXT_SEARCH_BUILDING, geo_text_building);
		cv.put(KEY_GEO_TEXT_SEARCH_STREET, geo_text_street);
		cv.put(KEY_GEO_TEXT_SEARCH_LANDMARK, geo_text_landmark);
		cv.put(KEY_GEO_TEXT_SEARCH_AREA_ID, area_id);
		cv.put(KEY_GEO_TEXT_SEARCH_AREA, geo_text_area);
		cv.put(KEY_GEO_TEXT_SEARCH_CITY, geo_text_city);
		cv.put(KEY_GEO_TEXT_SEARCH_MOB_NO1, geo_text_mobno1);
		cv.put(KEY_GEO_TEXT_SEARCH_MOB_NO2, geo_text_mobno2);
		cv.put(KEY_GEO_TEXT_SEARCH_MOB_NO3, geo_text_mobno3);

		cv.put(KEY_GEO_TEXT_SEARCH_TEL_NO1, geo_text_telno1);
		cv.put(KEY_GEO_TEXT_SEARCH_TEL_NO1, geo_text_telno2);
		cv.put(KEY_GEO_TEXT_SEARCH_TEL_NO1, geo_text_telno3);

		cv.put(KEY_GEO_TEXT_SEARCH_PHOTO, geo_text_photo);
		cv.put(KEY_GEO_TEXT_SEARCH_DISTANCE, distance);
		cv.put(KEY_GEO_TEXT_SEARCH_TOTAL_PAGES, total_pages);
		cv.put(KEY_GEO_TEXT_SEARCH_TOTAL_RECORD, total_record);
//		cv.put(KEY_GEO_TEXT_SEARCH_FAVOURITED, fav);
		cv.put(KEY_GEO_TEXT_SEARCH_HAS_OFFER, hasoffer);
		cv.put(KEY_GEO_TEXT_SEARCH_OFFER_TITLE, offertite);
		cv.put(KEY_GEO_TEXT_SEARCH_CURRENT_PAGE, current_page);
		cv.put(KEY_GEO_TEXT_TOTAL_LIKE, total_like);
		cv.put(KEY_GEO_TEXT_VERIFIED, verified);

		return sqlDataBase.insertWithOnConflict(TABLE_GEOTEXT_SEARCH, null, cv,SQLiteDatabase.CONFLICT_REPLACE);
//		return sqlDataBase.insert(TABLE_GEOTEXT_SEARCH, null, cv);

	}
	//***************************** Favourite Geo text search ****************************//
	public long create_geotextFav(int geo_txt_id,int place_parent,String geo_text_search_storename,String description,
			String geo_text_building,String geo_text_street,String geo_text_landmark,int area_id,String geo_text_area,String geo_text_city,
			String geo_text_mobno1,String geo_text_mobno2,String geo_text_mobno3,String geo_text_telno1,String geo_text_telno2,String geo_text_telno3,String geo_text_photo,String distance,int total_pages,int total_record,int current_page,String fav,String hasoffer,String offertite,int total_like,String verified)
	{
		Log.i("INSIDE ", "DATA BASE STORE BUSINESS "+geo_txt_id);

		ContentValues cv=new ContentValues();
		cv.put(KEY_GEO_TEXT_FAV_SEARCH_ID, geo_txt_id);
		cv.put(KEY_GEO_TEXT_FAV_PLACE_PARENT, place_parent);
		cv.put(KEY_GEO_TEXT_FAV_STORE_NAME, geo_text_search_storename);
		cv.put(KEY_GEO_TEXT_FAV_DESCRIPTION, description);
		cv.put(KEY_GEO_TEXT_FAV_BUILDING, geo_text_building);
		cv.put(KEY_GEO_TEXT_FAV_STREET, geo_text_street);
		cv.put(KEY_GEO_TEXT_FAV_LANDMARK, geo_text_landmark);
		cv.put(KEY_GEO_TEXT_FAV_AREA_ID, area_id);
		cv.put(KEY_GEO_TEXT_FAV_AREA, geo_text_area);
		cv.put(KEY_GEO_TEXT_FAV_CITY, geo_text_city);
		
		cv.put(KEY_GEO_TEXT_FAV_MOB_NO1, geo_text_mobno1);
		cv.put(KEY_GEO_TEXT_FAV_MOB_NO2, geo_text_mobno1);
		cv.put(KEY_GEO_TEXT_FAV_MOB_NO3, geo_text_mobno1);
		
		cv.put(KEY_GEO_TEXT_FAV_TEL_NO1, geo_text_mobno1);
		cv.put(KEY_GEO_TEXT_FAV_TEL_NO2, geo_text_mobno1);
		cv.put(KEY_GEO_TEXT_FAV_TEL_NO3, geo_text_mobno1);
		
		cv.put(KEY_GEO_TEXT_FAV_PHOTO, geo_text_photo);
		cv.put(KEY_GEO_TEXT_FAV_DISTANCE, distance);
		cv.put(KEY_GEO_TEXT_FAV_TOTAL_PAGES, total_pages);
		cv.put(KEY_GEO_TEXT_FAV_TOTAL_RECORD, total_record);
		cv.put(KEY_GEO_TEXT_FAV_FAVOURITED, fav);
		cv.put(KEY_GEO_TEXT_FAV_SEARCH_HAS_OFFER, hasoffer);
		cv.put(KEY_GEO_TEXT_FAV_SEARCH_OFFER_TITLE, offertite);
		cv.put(KEY_GEO_TEXT_FAV_CURRENT_PAGE, current_page);
		cv.put(KEY_GEO_TEXT_FAV_TOTAL_LIKE, total_like);
		cv.put(KEY_GEO_TEXT_FAV_VERIFIED, verified);
		

		Log.i("Current Page", "Current Page "+current_page);

		return sqlDataBase.insert(TABLE_GEOTEXT_FAV_SEARCH, null, cv);

	}

	//	public static final String KEY_PLACE_AUTO_ID="_id";
	//	public static final String KEY_PLACE_ID="id";
	//	public static final String KEY_PLACE_PARENT="place_parent";
	//	public static final String KEY_PLACE_NAME="place_name";
	//
	//	public static final String KEY_PLACE_DESCRIPTION="place_description";
	//	public static final String KEY_PLACE_BUILDING="building";
	//
	//	public static final String KEY_PLACE_STREET="place_street";
	//	public static final String KEY_PLACE_LANDMARK="place_landmark";
	//
	//	public static final String KEY_PLACE_AREA="place_area";
	//	public static final String KEY_PLACE_PINCODE="place_pincode";
	//	public static final String KEY_PLACE_CITY="place_city";
	//	public static final String KEY_PLACE_STATE="place_state";
	//
	//	public static final String KEY_PLACE_COUNTRY="place_country";
	//	public static final String KEY_PLACE_LATITUDE="place_latitude";
	//	public static final String KEY_PLACE_LONGITUDE="place_longitude";
	//
	//
	//
	//	public static final String KEY_PLACE_TELLNO1="place_tellno1";
	//	public static final String KEY_PLACE_TELLNO2="place_tellno2";
	//	public static final String KEY_PLACE_TELLNO3="place_tellno3";
	//
	//
	//	public static final String KEY_PLACE_MOBNO1="place_mobno1";
	//	public static final String KEY_PLACE_MOBNO2="place_mobno2";
	//	public static final String KEY_PLACE_MOBNO3="place_mobno3";
	//
	//
	//	public static final String KEY_PLACE_image_url="place_image_url";
	//	public static final String KEY_PLACE_email="place_email";
	//	public static final String KEY_PLACE_website="place_website";
	//
	//	public static final String KEY_PLACE_facebook="place_facebook";
	//	public static final String KEY_PLACE_twitter="place_twitter";
	//	public static final String KEY_PLACE_place_status="place_status";
	//	public static final String KEY_PLACE_place_published="place_published";

	//	public static final String KEY_PLACE_place_total_like="place_total_like";
	//	public static final String KEY_PLACE_place_total_share="place_total_share";
	//	public static final String KEY_PLACE_place_total_view="place_total_view";

	/*
	 * Create Store
	 *
	 */
	public long create_place_chooser(int PLACE_KEY_PLACE_ID,int PLACE_KEY_PLACE_PARENT,String PLACE_KEY_PLACE_NAME,String PLACE_KEY_PLACE_DESCRIPTION,String PLACE_KEY_PLACE_BUILDING,
			String PLACE_KEY_PLACE_STREET,String PLACE_KEY_PLACE_LANDMARK,String PLACE_KEY_PLACE_AREA,String PLACE_KEY_PLACE_PINCODE,String PLACE_KEY_PLACE_CITY,String PLACE_KEY_PLACE_STATE,String PLACE_KEY_PLACE_COUNTRY,
			String PLACE_KEY_PLACE_LATITUDE,String PLACE_KEY_PLACE_LONGITUDE,String PLACE_KEY_PLACE_TELLNO1,String PLACE_KEY_PLACE_TELLNO2,String PLACE_KEY_PLACE_TELLNO3,String PLACE_KEY_PLACE_MOBNO1,String PLACE_KEY_PLACE_MOBNO2,
			String PLACE_KEY_PLACE_MOBNO3,String PLACE_KEY_PLACE_image_url,String PLACE_KEY_PLACE_email,String PLACE_KEY_PLACE_website,
			String PLACE_KEY_PLACE_facebook,String PLACE_KEY_PLACE_twitter,String PLACE_KEY_PLACE_place_status,String PLACE_KEY_PLACE_place_published,
			String PLACE_KEY_PLACE_place_total_like,String PLACE_KEY_PLACE_place_total_share,String PLACE_KEY_PLACE_place_total_view)
	{
		Log.i("INSIDE ", "DATA BASE PLACE_CHOOSER");

		ContentValues cv=new ContentValues();

		cv.put(KEY_PLACE_ID, PLACE_KEY_PLACE_ID);
		cv.put(KEY_PLACE_PARENT, PLACE_KEY_PLACE_PARENT);
		cv.put(KEY_PLACE_NAME, PLACE_KEY_PLACE_NAME);
		cv.put(KEY_PLACE_DESCRIPTION, PLACE_KEY_PLACE_DESCRIPTION);
		cv.put(KEY_PLACE_BUILDING, PLACE_KEY_PLACE_BUILDING);
		cv.put(KEY_PLACE_STREET, PLACE_KEY_PLACE_STREET);
		cv.put(KEY_PLACE_LANDMARK, PLACE_KEY_PLACE_LANDMARK);
		cv.put(KEY_PLACE_AREA, PLACE_KEY_PLACE_AREA);
		cv.put(KEY_PLACE_PINCODE, PLACE_KEY_PLACE_PINCODE);
		cv.put(KEY_PLACE_CITY, PLACE_KEY_PLACE_CITY);
		cv.put(KEY_PLACE_STATE, PLACE_KEY_PLACE_STATE);
		cv.put(KEY_PLACE_COUNTRY, PLACE_KEY_PLACE_COUNTRY);
		cv.put(KEY_PLACE_LATITUDE, PLACE_KEY_PLACE_LATITUDE);
		cv.put(KEY_PLACE_LONGITUDE, PLACE_KEY_PLACE_LONGITUDE);
		cv.put(KEY_PLACE_TELLNO1, PLACE_KEY_PLACE_TELLNO1);
		cv.put(KEY_PLACE_TELLNO2, PLACE_KEY_PLACE_TELLNO2);
		cv.put(KEY_PLACE_TELLNO3, PLACE_KEY_PLACE_TELLNO3);
		cv.put(KEY_PLACE_MOBNO1, PLACE_KEY_PLACE_MOBNO1);
		cv.put(KEY_PLACE_MOBNO2, PLACE_KEY_PLACE_MOBNO2);
		cv.put(KEY_PLACE_MOBNO3, PLACE_KEY_PLACE_MOBNO3);
		cv.put(KEY_PLACE_image_url, PLACE_KEY_PLACE_image_url);
		cv.put(KEY_PLACE_email, PLACE_KEY_PLACE_email);
		cv.put(KEY_PLACE_website, PLACE_KEY_PLACE_website);
		cv.put(KEY_PLACE_facebook, PLACE_KEY_PLACE_facebook);
		cv.put(KEY_PLACE_twitter, PLACE_KEY_PLACE_twitter);
		cv.put(KEY_PLACE_place_status, PLACE_KEY_PLACE_place_status);
		cv.put(KEY_PLACE_place_published, PLACE_KEY_PLACE_place_published);

		cv.put(KEY_PLACE_place_total_like, PLACE_KEY_PLACE_place_total_like);
		cv.put(KEY_PLACE_place_total_share, PLACE_KEY_PLACE_place_total_share);
		cv.put(KEY_PLACE_place_total_view, PLACE_KEY_PLACE_place_total_view);

		return sqlDataBase.insert(TABLE_PLACE_CHOOSER, null, cv);

	}
	
	//Row count
	
	public long getMyPlaceCount()
	{
		String rawQuery="SELECT COUNT("+KEY_PLACE_ID+") AS ROW_COUNT FROM "+TABLE_PLACE_CHOOSER ;
		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return rowcount;

	}
	

	//	//Fields For Table News Feeds
	//
	//			public static final String KEY_NEWS_FEED_AUTO_ID="_id";
	//			public static final String KEY_NEWS_FEED_ID="id";
	//			public static final String KEY_NEWS_FEED_NAME="name";
	//			
	//			public static final String KEY_NEWS_FEED_MOBNO1="mobno1";
	//			public static final String KEY_NEWS_FEED_MOBNO2="mobno2";
	//			public static final String KEY_NEWS_FEED_MOBNO3="mobno3";
	//			
	//			
	//			public static final String KEY_NEWS_FEED_TELLNO1="tellno1";
	//			public static final String KEY_NEWS_FEED_TELLNO2="tellno2";
	//			public static final String KEY_NEWS_FEED_TELLNO3="tellno3";
	//			
	//			public static final String KEY_NEWS_FEED_LATITUDE="latitude";
	//			public static final String KEY_NEWS_FEED_LONGITUDE="longitude";
	//			public static final String KEY_NEWS_FEED_DISTANCE="distance";
	//			
	//			public static final String KEY_NEWS_FEED_POST_ID="post_id";
	//			public static final String KEY_NEWS_FEED_POST_TYPE="type";
	//			
	//			public static final String KEY_NEWS_FEED_POST_TITLE="title";
	//			public static final String KEY_NEWS_FEED_DESCRIPTION="description";
	//			
	//			
	//			public static final String KEY_NEWS_FEED_image_url1="image_url1";
	//			public static final String KEY_NEWS_FEED_image_url2="image_url2";
	//			public static final String KEY_NEWS_FEED_image_url3="image_url3";
	//			
	//			public static final String KEY_NEWS_FEED_thumb_url1="thumb_url1";
	//			public static final String KEY_NEWS_FEED_thumb_url2="thumb_url2";
	//			public static final String KEY_NEWS_FEED_thumb_url3="thumb_url3";
	//
	//			public static final String KEY_NEWS_DATE="date";
	//			public static final String KEY_NEWS_IS_OFFERED="is_offered";
	//			public static final String KEY_NEWS_OFFER_DATE_TIME="offer_date_time";
	//
	//			public static final String KEY_NEWS_FEED_place_total_like="total_like";
	//			public static final String KEY_NEWS_FEED_place_total_share="total_share";

	//			public static final String KEY_NEWS_FEED_TOTAL_PAGE	="total_page";
	//			public static final String KEY_NEWS_FEED_TOTAL_RECORD="total_record";
	//	

	public long create_newsFeed(int place_id,int area_id,String news_place_name,String news_place_mobno1,String news_place_mobno2,String news_place_mobno3,
			String news_place_tellno1,String news_place_tellno2,String news_place_tellno3,String news_place_latitude,
			String news_place_longitude,String distance,int post_id,String type,String news_place_title,String news_place_description,
			String news_place_image_url1,String news_place_image_url2,String news_place_image_url3,String news_place_thumb_url1,String news_place_thumb_url2,
			String news_place_thumb_url3,String news_place_date,String news_place_is_offered,String news_place_offer_date_time,String news_place_total_like,
			String news_place_total_share,int news_place_total_page,int news_place_total_record,int current_page,String verified)
	{

		Log.i("INSIDE DATABASE"," INSIDE DATABASE NEWS FEED ");

		ContentValues cv=new ContentValues();

		cv.put(KEY_NEWS_FEED_ID, place_id);
		cv.put(KEY_NEWS_FEED_AREA_ID, area_id);
		cv.put(KEY_NEWS_FEED_NAME, news_place_name);


		cv.put(KEY_NEWS_FEED_MOBNO1, news_place_mobno1);
		cv.put(KEY_NEWS_FEED_MOBNO2, news_place_mobno2);
		cv.put(KEY_NEWS_FEED_MOBNO3, news_place_mobno3);

		cv.put(KEY_NEWS_FEED_TELLNO1, news_place_tellno1);
		cv.put(KEY_NEWS_FEED_TELLNO2, news_place_tellno2);
		cv.put(KEY_NEWS_FEED_TELLNO3, news_place_tellno3);

		cv.put(KEY_NEWS_FEED_LATITUDE, news_place_latitude);
		cv.put(KEY_NEWS_FEED_LONGITUDE, news_place_longitude);
		cv.put(KEY_NEWS_FEED_DISTANCE, distance);


		cv.put(KEY_NEWS_FEED_POST_ID, post_id);
		cv.put(KEY_NEWS_FEED_POST_TYPE, type);

		cv.put(KEY_NEWS_FEED_POST_TITLE, news_place_title);
		cv.put(KEY_NEWS_FEED_DESCRIPTION, news_place_description);

		cv.put(KEY_NEWS_FEED_image_url1, news_place_image_url1);
		cv.put(KEY_NEWS_FEED_image_url2, news_place_image_url2);
		cv.put(KEY_NEWS_FEED_image_url3, news_place_image_url3);

		cv.put(KEY_NEWS_FEED_thumb_url1, news_place_thumb_url1);
		cv.put(KEY_NEWS_FEED_thumb_url2, news_place_thumb_url2);
		cv.put(KEY_NEWS_FEED_thumb_url3, news_place_thumb_url3);

		cv.put(KEY_NEWS_DATE, news_place_date);
		cv.put(KEY_NEWS_IS_OFFERED, news_place_is_offered);
		cv.put(KEY_NEWS_OFFER_DATE_TIME, news_place_offer_date_time);

		cv.put(KEY_NEWS_FEED_place_total_like, news_place_total_like);
		cv.put(KEY_NEWS_FEED_place_total_share, news_place_total_share);
		cv.put(KEY_NEWS_FEED_verified, verified);



		cv.put(KEY_NEWS_FEED_TOTAL_PAGE, news_place_total_page);
		cv.put(KEY_NEWS_FEED_TOTAL_RECORD, news_place_total_record);
		cv.put(KEY_NEWS_FEED_CURRENT_PAGE,current_page);

		return	sqlDataBase.insertWithOnConflict(TABLE_NEWS_FEED, null, cv,SQLiteDatabase.CONFLICT_REPLACE);

//		return sqlDataBase.insert(TABLE_NEWS_FEED, null, cv);

	}


	//	Place names
	public ArrayList<String> getAllPlacesNames()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_PLACE_NAME+" FROM "+TABLE_PLACE_CHOOSER;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_PLACE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	//	Place Desc
	public ArrayList<String> getAllPlacesDesc()
	{
		ArrayList<String> desc = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_PLACE_DESCRIPTION+" FROM "+TABLE_PLACE_CHOOSER;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				desc.add(cursor.getString(cursor.getColumnIndex(KEY_PLACE_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return desc;

	}

	//	Place ID
	public ArrayList<String> getAllPlacesIDs()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_PLACE_ID+" FROM "+TABLE_PLACE_CHOOSER;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_PLACE_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	//	Place Photourl
	public ArrayList<String> getAllPlacesPhotoUrl()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_PLACE_image_url+" FROM "+TABLE_PLACE_CHOOSER;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_PLACE_image_url)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}


	// News Feed

	//	Post ID
	public ArrayList<String> getAllNewsFeedsID(int area_id)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_POST_ID+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_POST_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}
	// New Feed Store ID
	public ArrayList<String> getAllNewsFeedsStoreID(int area_id)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_ID+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	// New Feed Store Name
	public ArrayList<String> getAllNewsFeedsStoreName(int area_id)
	{
		ArrayList<String> name = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_NAME+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				name.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return name;

	}

	// New Feed Store Area
	public ArrayList<String> getAllNewsFeedsStoreArea(int area_id)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_AREA_ID+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_AREA_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}

	// New Feed Store Lat
	public ArrayList<String> getAllNewsFeedsLat(int area_id)
	{
		ArrayList<String> lat = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_LATITUDE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				lat.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_LATITUDE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return lat;

	}

	// New Feed Store Long
	public ArrayList<String> getAllNewsFeedsLong(int area_id)
	{
		ArrayList<String> longitude = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_LONGITUDE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				longitude.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_LONGITUDE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return longitude;

	}

	// New Feed Store Distance
	public ArrayList<String> getAllNewsFeedsDistance(int area_id)
	{
		ArrayList<String> distance = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_DISTANCE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				distance.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return distance;

	}

	// New Feed Store Types
	public ArrayList<String> getAllNewsFeedsTypes(int area_id)
	{
		ArrayList<String> types = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_POST_TYPE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				types.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_POST_TYPE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return types;

	}

	//All Titles of News Feed
	public ArrayList<String> getAllNewsFeedsTitles(int area_id)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_POST_TITLE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_POST_TITLE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	//All Description of News Feed
	public ArrayList<String> getAllNewsFeedsDescs(int area_id)
	{
		ArrayList<String> desc = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_DESCRIPTION+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				desc.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return desc;

	}

	//All is_Offer of News Feed
	public ArrayList<String> getAllNewsFeedsIsOffer(int area_id)
	{
		ArrayList<String> isOffered = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_IS_OFFERED+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				isOffered.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_IS_OFFERED)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return isOffered;

	}

	//All OFFER_DATE_TIME of News Feed
	public ArrayList<String> getAllNewsFeedsOfferDate(int area_id)
	{
		ArrayList<String> offer_date_Time = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_OFFER_DATE_TIME+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				offer_date_Time.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_OFFER_DATE_TIME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return offer_date_Time;

	}

	//All DATE of News Feed
	public ArrayList<String> getAllNewsFeedsDATE(int area_id)
	{
		ArrayList<String> date = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_DATE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				date.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_DATE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return date;

	}

	//All PhotoUrl of News Feed
	public ArrayList<String> getAllNewsFeedsPhotoUrl1(int area_id)
	{
		ArrayList<String> photo1 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_image_url1+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo1.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_image_url1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo1;

	}

	//All PhotoUrl2 of News Feed
	public ArrayList<String> getAllNewsFeedsPhotoUrl2(int area_id)
	{
		ArrayList<String> photo2 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_image_url2+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo2.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_image_url2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo2;

	}

	//All PhotoUrl3 of News Feed
	public ArrayList<String> getAllNewsFeedsPhotoUrl3(int area_id)
	{
		ArrayList<String> photo3 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_image_url3+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo3.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_image_url3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo3;

	}

	//All Thumbs1 of News Feed
	public ArrayList<String> getAllNewsFeedsThumbs1(int area_id)
	{
		ArrayList<String> thumbs1 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_thumb_url1+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				thumbs1.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_thumb_url1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return thumbs1;

	}

	//All Thumbs2 of News Feed
	public ArrayList<String> getAllNewsFeedsThumbs2(int area_id)
	{
		ArrayList<String> thumbs2 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_thumb_url2+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				thumbs2.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_thumb_url2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return thumbs2;

	}

	//All Thumbs3 of News Feed
	public ArrayList<String> getAllNewsFeedsThumbs3(int area_id)
	{
		ArrayList<String> thumbs3 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_thumb_url3+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				thumbs3.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_thumb_url3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return thumbs3;

	}

	//All Total_like of News Feed
	public ArrayList<String> getAllNewsFeedTotalLike(int area_id)
	{
		ArrayList<String> total_like = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_place_total_like+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_like.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_place_total_like)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_like;

	}

	//All Total_share of News Feed
	public ArrayList<String> getAllNewsFeedTotalShare(int area_id)
	{
		ArrayList<String> total_share = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_place_total_share+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_share.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_place_total_share)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_share;

	}

	//All Mob1 of News Feed
	public ArrayList<String> getAllNewsFeedMob1(int area_id)
	{
		ArrayList<String> mobno1 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_MOBNO1+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobno1.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_MOBNO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobno1;

	}

	//All Mob2 of News Feed
	public ArrayList<String> getAllNewsFeedMob2(int area_id)
	{
		ArrayList<String> mobno1 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_MOBNO2+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobno1.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_MOBNO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobno1;

	}

	//All Mob2 of News Feed
	public ArrayList<String> getAllNewsFeedMob3(int area_id)
	{
		ArrayList<String> mobno1 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_MOBNO3+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobno1.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_MOBNO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobno1;

	}

	//All tell1 of News Feed
	public ArrayList<String> getAllNewsFeedTell1(int area_id)
	{
		ArrayList<String> telno1 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_TELLNO1+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				telno1.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_TELLNO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return telno1;

	}

	//All tell2 of News Feed
	public ArrayList<String> getAllNewsFeedTell2(int area_id)
	{
		ArrayList<String> telno2 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_TELLNO2+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				telno2.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_TELLNO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return telno2;

	}

	//All tell3 of News Feed
	public ArrayList<String> getAllNewsFeedTell3(int area_id)
	{
		ArrayList<String> telno3 = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_TELLNO3+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				telno3.add(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_TELLNO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return telno3;

	}

	//All total_record of News Feed
	public Integer getAllNewsFeedTotalRecord(int area_id)
	{
		int total_record = 0;
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_TOTAL_RECORD+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_record=Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_TOTAL_RECORD)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_record;

	}

	//All total_page of News Feed
	public Integer getAllNewsFeedTotalPages(int area_id)
	{
		int total_page=0;
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_TOTAL_PAGE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_page=Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_TOTAL_PAGE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_page;

	}

	//All current page of News Feed
	public Integer getAllNewsFeedCurrentPages(int area_id)
	{
		int current_page=0;
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_NEWS_FEED_CURRENT_PAGE+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				current_page=Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_NEWS_FEED_CURRENT_PAGE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return current_page;

	}


	//News feed row count
	public long getNewsFeedRowCount(int area_id)
	{
		String rawQuery="SELECT COUNT("+KEY_NEWS_FEED_POST_ID+") AS ROW_COUNT FROM "+TABLE_NEWS_FEED +" WHERE "+KEY_NEWS_FEED_AREA_ID+" = "+area_id;
		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return rowcount;

	}

	//Update Current Page Status for News Feed

	public void updateNewsFeedsCurrentPage(int page,int area_id)
	{
		try
		{
			open();
			ContentValues cv=new ContentValues();
			cv.put(KEY_NEWS_FEED_CURRENT_PAGE, page);
			sqlDataBase.update(TABLE_NEWS_FEED, cv,KEY_NEWS_FEED_AREA_ID+"="+area_id, null);
			close();

			/*	getAllCurrentPages();*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void updateNewsFeedsTotalPage(int page,int area_id)
	{
		try
		{
			open();
			ContentValues cv=new ContentValues();
			cv.put(KEY_NEWS_FEED_TOTAL_PAGE, page);
			sqlDataBase.update(TABLE_NEWS_FEED, cv,KEY_NEWS_FEED_AREA_ID+"="+area_id, null);
			close();

			/*	getAllCurrentPages();*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	public void updateNewsFeedsTotalRecord(int page,int area_id)
	{
		try
		{
			open();
			ContentValues cv=new ContentValues();
			cv.put(KEY_NEWS_FEED_TOTAL_RECORD, page);
			sqlDataBase.update(TABLE_NEWS_FEED, cv,KEY_NEWS_FEED_AREA_ID+"="+area_id, null);
			close();

			/*	getAllCurrentPages();*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	// Delete all record by Area Id in NewsFeed Table

	void deleteNewsFeedByArea(int area_id)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_NEWS_FEED, KEY_NEWS_FEED_AREA_ID+" = "+area_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}




	/*
	 * Getting total records from Geo-text search
	 */

	public Integer getTotalRecords(int place_parent)
	{
		int total_records = 0;
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TOTAL_RECORD+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_records=cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TOTAL_RECORD));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_records;

	}

	/*
	 * Getting total no of pages from Geo-text search
	 */

	public Integer getTotalPages(int place_parent)
	{
		int total_pages = 0;
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TOTAL_PAGES+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_pages=cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TOTAL_PAGES));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_pages;

	}

	/*
	 * Getting Curren tpage from Geo-text search
	 */

	public Integer getCurentPage(int place_parent)
	{
		int current_page = 0;
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_CURRENT_PAGE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				current_page=cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_CURRENT_PAGE));

				/*Log.i("PAGE", "PAGE->>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_CURRENT_PAGE)));*/


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return current_page;

	}

	public ArrayList<Integer> getAllCurrentPages()
	{
		ArrayList<Integer> currentPage = new ArrayList<Integer>();
		try
		{

			open();

			String query="SELECT "+KEY_GEO_TEXT_SEARCH_CURRENT_PAGE+" FROM "+TABLE_GEOTEXT_SEARCH;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				currentPage.add(cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_CURRENT_PAGE)));


			}
			cursor.close();
			for(int i=0;i<currentPage.size();i++)
			{
				Log.i("PAGE", "PAGE->>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+currentPage.get(i));
			}
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return currentPage;

	}

	public void updateCurrentPage(int page)
	{
		try
		{
			open();
			ContentValues cv=new ContentValues();
			cv.put(KEY_GEO_TEXT_SEARCH_CURRENT_PAGE, page);
			sqlDataBase.update(TABLE_GEOTEXT_SEARCH, cv,null, null);
			close();

			/*	getAllCurrentPages();*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public long getRowCount(int area_id)
	{
		String rawQuery="SELECT COUNT("+KEY_GEO_TEXT_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_SEARCH +" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;
		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return rowcount;

	}

	public long getRowCount(String area)
	{
		String rawQuery="SELECT COUNT("+KEY_GEO_TEXT_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_SEARCH +" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

		Log.i("ROW COUNT","ROW COUNT QUERY "+ rawQuery);

		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		Log.i("ROW COUNT","ROW COUNT "+ rowcount);
		return rowcount;

	}

	//Fav row count
	public long getFavRowCount()
	{
		String rawQuery="SELECT COUNT("+KEY_GEO_TEXT_FAV_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_FAV_SEARCH;
		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return rowcount;

	}

	//Fav row count
	public long getFavRowCount(int place_parent)
	{
		String rawQuery="SELECT COUNT("+KEY_GEO_TEXT_FAV_SEARCH_ID+") AS ROW_COUNT FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent;
		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return rowcount;

	}

	/*
	 * Getting Store name by id from Geo-Text table
	 */

	public String getGeoStoreName(int store_id)
	{
		String title = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_STORE_NAME));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllGeoStoreName(int area_id)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllGeoStoreName(String area)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*			String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllGeoStoreName()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_STORE_NAME+" FROM "+TABLE_GEOTEXT_SEARCH+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllGeoStoreDesc(int area_id)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_DESCRIPTION+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_DESCRIPTION+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllGeoStoreDesc(String area)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_DESCRIPTION+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_DESCRIPTION+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllTotalLikes(int area_id)
	{
		ArrayList<String> total_like = new ArrayList<String>();
		try
		{

			open();



			/*			String query="SELECT "+KEY_GEO_TEXT_TOTAL_LIKE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_TOTAL_LIKE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_like.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_TOTAL_LIKE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_like;

	}

	public ArrayList<String> getAllTotalLikes(String area)
	{
		ArrayList<String> total_like = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_TOTAL_LIKE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_TOTAL_LIKE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				total_like.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_TOTAL_LIKE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return total_like;

	}


	/*
	 * Getting Photo url by id from Geo-Text table
	 */

	public String getGeoPhoto(int store_id)
	{
		String photo = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_PHOTO+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_PHOTO));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllGeoPhoto(int area_id)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_PHOTO+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_PHOTO+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_PHOTO)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}


	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllGeoPhoto(String area)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_PHOTO+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_PHOTO+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_PHOTO)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllDistances(int area_id)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_DISTANCE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_DISTANCE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	public ArrayList<String> getAllDistances(String area)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_DISTANCE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_DISTANCE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllGeoAreas(int area_id)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}

	public ArrayList<String> getAllGeoAreas(String area_name)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area_name+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area_name+"'";

			Log.i("SEARCH QUERY", "SEARCH QUERY "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}
	
	public ArrayList<String> getAllGeoAreaId(String area_name)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area_name+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area_name+"'";

			Log.i("SEARCH QUERY", "SEARCH QUERY "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_AREA_ID))+"");


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}
	
	public ArrayList<String> getAllGeoAreaId(int area_id)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area_name+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Log.i("SEARCH QUERY", "SEARCH QUERY "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_AREA_ID))+"");


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}


	public ArrayList<String> getAllGeoAreas()
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}
	/*
	 * Get all City from Geo-Text table
	 */

	public ArrayList<String> getAllGeoCity(int area_id)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_CITY+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_CITY+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;


			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_CITY)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	public ArrayList<String> getAllGeoCity(String area)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_CITY+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_CITY+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_CITY)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}



	/*
	 * Get all Id's Geo-Text table
	 */

	public ArrayList<String> getAllGeoStoreId(int area_id)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	public ArrayList<String> getAllGeoStoreId(String area)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		for(int i=0;i<ids.size();i++)
		{

		}
		return ids;


	}


	/*
	 * Get all Id's Geo-Text table
	 */

	public ArrayList<String> getAllGeoStoreParentId(int area)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_PLACE_PARENT)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}


	public ArrayList<String> getAllGeoStoreParentId(String area)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Log.i("QUERY", "QUERY "+query);
			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_PLACE_PARENT)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	/*
	 * Get all Offers Geo-Text table
	 */

	public ArrayList<String> getAllStoreOffers(int area_id)
	{
		ArrayList<String> offer_title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_OFFER_TITLE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_OFFER_TITLE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				offer_title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_OFFER_TITLE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return offer_title;

	}

	public ArrayList<String> getAllStoreOffers(String area)
	{
		ArrayList<String> offer_title = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_OFFER_TITLE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_OFFER_TITLE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				offer_title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_OFFER_TITLE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return offer_title;

	}

	/*Has offers */
	public String hasOffers(int store_id)
	{
		String hasOffer = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_HAS_OFFER+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				hasOffer=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_HAS_OFFER));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return hasOffer;

	}

	/*offers */
	public String getPerticularOffers(int store_id)
	{
		String offer = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_OFFER_TITLE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				offer=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_OFFER_TITLE));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return offer;

	}

	/*
	 * Get all Offers flag Geo-Text table
	 */

	public ArrayList<String> getAllOffersFlag(int area_id)
	{
		ArrayList<String> offer_flag = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_HAS_OFFER+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_HAS_OFFER+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				offer_flag.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_HAS_OFFER)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return offer_flag;

	}

	public ArrayList<String> getAllOffersFlag(String area)
	{
		ArrayList<String> offer_flag = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_HAS_OFFER+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_HAS_OFFER+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				offer_flag.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_HAS_OFFER)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return offer_flag;

	}

	/*
	 * Get City by id from Geo-Text table
	 */

	public String getGeoCity(int store_id)
	{
		String city = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_CITY+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_CITY));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	/*
	 * Get Area by id from Geo-Text table
	 */

	public String getGeoArea(int store_id)
	{
		String area = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_AREA+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_AREA));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}

	/*
	 * Get Street by id from Geo-Text table
	 */

	public String getGeoStreet(int store_id)
	{
		String area = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_STREET+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_STREET));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}


	/*
	 * Get Mobile Numbers by id from Geo-Text table
	 */

	public String getGeoMobileNos(int store_id)
	{
		String mobileno = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO1));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}



	/*
	 * Get Fav by id from Geo-Text table
	 */

//	public String getGeoFavorite(int store_id)
//	{
//		String fav = "";
//		try
//		{
//
//			open();
//
//
//
//			String query="SELECT "+KEY_GEO_TEXT_SEARCH_FAVOURITED+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;
//
//			Cursor  cursor = sqlDataBase.rawQuery(query,null);
//
//
//			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
//			{
//				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
//				fav=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_FAVOURITED));
//
//
//			}
//			cursor.close();
//		}catch(SQLException sqx)
//		{
//			sqx.printStackTrace();
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//		finally
//		{
//			close();
//		}
//		return fav;
//
//	}


	/*
	 * Getting all Mobile No's name from Geo-Text table
	 */

	public ArrayList<String> getGeoAllMobileNos(int area_id)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoAllMobileNos2(int area_id)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO2+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoAllMobileNos3(int area_id)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_SEARCH_STORE_NAME;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO3+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos(String area)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos2(String area)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO2+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}

	public ArrayList<String> getGeoAllMobileNos3(String area)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO3+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_MOB_NO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoAllTELNos(int area_id)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TEL_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TEL_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}
	public ArrayList<String> getGeoAllTELNos2(int area_id)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TEL_NO2+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TEL_NO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}
	
	public ArrayList<String> getGeoAllTELNos3(int area_id)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TEL_NO3+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TEL_NO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos(String area)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TEL_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TEL_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos2(String area)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TEL_NO2+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TEL_NO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}

	public ArrayList<String> getGeoAllTELNos3(String area)
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			/*String query="SELECT "+KEY_GEO_TEXT_SEARCH_MOB_NO1+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'"+" ORDER BY "+KEY_GEO_TEXT_SEARCH_DISTANCE;*/
			String query="SELECT "+KEY_GEO_TEXT_SEARCH_TEL_NO3+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'";

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_TEL_NO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}


	/*
	 * GetLandmark by id from Geo-Text table
	 */

	public String getGeoLandmark(int store_id)
	{
		String landmark = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_LANDMARK+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				landmark=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_LANDMARK));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return landmark;

	}


	/*
	 * Get Building by id from Geo-Text table
	 */

	public String getGeoBuilding(int store_id)
	{
		String building = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_BUILDING+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				building=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_BUILDING));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return building;

	}

	/*
	 * Get Distance by id from Geo-Text table
	 */

	public String getGeoDistance(int store_id)
	{
		String distance = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_DISTANCE+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				distance=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_DISTANCE));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return distance;

	}

	/*
	 * Get Distance by id from Geo-Text table
	 */

	public boolean isGeoIdExists(int store_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_SEARCH_ID+" FROM "+TABLE_GEOTEXT_SEARCH+" WHERE "+KEY_GEO_TEXT_SEARCH_ID+"=" + store_id;

			Cursor  cursor  = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				id=cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_SEARCH_ID))+"";

				if(id.equalsIgnoreCase(store_id+""))
				{
					exists=true;
					break;
				}

			}
			cursor.close();
			close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		Log.i("ID EXISTIS","IS ID EXISTS " +exists);
		return exists;

	}

	public boolean isNewsFeedPostIdExists(int post_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			open();



			String query="SELECT "+KEY_NEWS_FEED_POST_ID+" FROM "+TABLE_NEWS_FEED+" WHERE "+KEY_NEWS_FEED_POST_ID+"=" + post_id;

			Cursor  cursor  = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				id=cursor.getInt(cursor.getColumnIndex(KEY_NEWS_FEED_POST_ID))+"";

				if(id.equalsIgnoreCase(post_id+""))
				{
					exists=true;
					break;
				}

			}
			cursor.close();
			close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		Log.i("ID EXISTIS","IS ID EXISTS " +exists);
		return exists;

	}

	public boolean isGeoFavIdExists(int store_id)
	{
		String id = "";
		boolean exists=false;

		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_SEARCH_ID+"=" + store_id;

			Cursor  cursor  = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				id=cursor.getInt(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_SEARCH_ID))+"";

				if(id.equalsIgnoreCase(store_id+""))
				{
					exists=true;
					break;
				}

			}
			cursor.close();
			close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		Log.i("ID EXISTIS","IS ID EXISTS " +exists);
		return exists;

	}


	void deleteGeoText(int area_id)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_SEARCH, KEY_GEO_TEXT_SEARCH_AREA_ID+" = "+area_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}
	void deleteGeoText(String area)
	{
		try
		{

			open();
			int res=sqlDataBase.delete(TABLE_GEOTEXT_SEARCH, KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'", null);
			Log.i("DELETED ", "DELETED  : "+res);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText(int place_parent)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText(String area)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, KEY_GEO_TEXT_SEARCH_AREA+" like '"+area+"'", null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	//Delete GeoFav
	void deleteAllGeoFavText()
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH,null,null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}


	public long getGeoRowCount()
	{
		/*long rowcount=0;
		String query="SELECT count(*) FROM "+DATABASE_TABLE_NAME;
		Cursor  cursor = sqlDataBase.rawQuery(query,null);*/

		/*cursor.moveToFirst();
		rowcount=cursor.getInt(0);
		cursor.close();*/
		long rowCount=0;
		try
		{
			open();
			rowCount=DatabaseUtils.queryNumEntries(sqlDataBase, TABLE_GEOTEXT_SEARCH);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}
		return rowCount ;
	}


//	public void updateGeoFavById(int id,String isFav)
//	{
//		try
//		{
//			ContentValues cv=new ContentValues();
//			cv.put(KEY_GEO_TEXT_SEARCH_FAVOURITED, isFav);
//			int l=sqlDataBase.update(TABLE_GEOTEXT_SEARCH, cv, KEY_GEO_TEXT_SEARCH_ID + "=" + id, null);
//			Log.i("Updated","Updated "+l+" STORE ID "+id+" isFAV "+isFav);
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
//
//	}

	//******************************** GEO-TEXT-FAV *****************************************//


	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoStoreName()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_STORE_NAME+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	/*
	 * Getting all Store name from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoStoreName(int place_parent)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_STORE_NAME+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_STORE_NAME)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllFavGeoStoreDESC()
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_DESCRIPTION+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	public ArrayList<String> getAllFavGeoStoreDESC(int place_parent)
	{
		ArrayList<String> title = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_DESCRIPTION+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_DESCRIPTION)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	/*
	 * Get all Fav Id's Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreId()
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_SEARCH_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}
	/*
	 * Get all Fav Id's Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoStoreId(int place_parent)
	{
		ArrayList<String> ids = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_SEARCH_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				ids.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_SEARCH_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return ids;

	}

	/*
	 * Getting all Mobile No's name from Geo-Text Fav table
	 */

	public ArrayList<String> getGeoFavAllMobileNos()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_MOB_NO1+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_MOB_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoFavAllMobileNos2()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_MOB_NO2+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_MOB_NO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoFavAllMobileNos3()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_MOB_NO3+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_MOB_NO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoFavAllTelNos()
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_TEL_NO1+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_TEL_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}
	
	public ArrayList<String> getGeoFavAllTelNos2()
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_TEL_NO2+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_TEL_NO2)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}
	
	public ArrayList<String> getGeoFavAllTelNos3()
	{
		ArrayList<String> tel = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_TEL_NO3+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				tel.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_TEL_NO3)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return tel;

	}
	
	
	public ArrayList<String> getGeoFavAllMobileNos(int place_parent)
	{
		ArrayList<String> mobileno = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_MOB_NO1+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_MOB_NO1)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}

	/*
	 * Get Fav by id from Geo-Text table
	 */

	public String getGeoFav_Favorite(int store_id)
	{
		String fav = "";
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_FAVOURITED+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_SEARCH_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				fav=cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_FAVOURITED));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return fav;

	}


	/*
	 * Get all City from Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoCity()
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_CITY+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_CITY)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	/*
	 * Get all City from Geo-Text Fav table
	 */

	public ArrayList<String> getAllFavGeoCity(int place_parent)
	{
		ArrayList<String> city = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_CITY+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_CITY)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoPhoto()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_PHOTO+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_PHOTO)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoPhoto(int place_parent)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_PHOTO+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_PHOTO)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavDistances()
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_DISTANCE+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Get all Photos from Geo-Text table
	 */

	public ArrayList<String> getAllFavDistances(int place_parent)
	{
		ArrayList<String> photo = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_DISTANCE+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_DISTANCE)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}


	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoAreas()
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_AREA+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}
	public ArrayList<String> getAllFavGeoAreaID()
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_AREA_ID+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_AREA_ID)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}
	/*
	 * Get all Area from Geo-Text table
	 */

	public ArrayList<String> getAllFavGeoAreas(int place_parent)
	{
		ArrayList<String> area = new ArrayList<String>();
		try
		{

			open();



			String query="SELECT "+KEY_GEO_TEXT_FAV_AREA+" FROM "+TABLE_GEOTEXT_FAV_SEARCH+" WHERE "+KEY_GEO_TEXT_FAV_PLACE_PARENT+" = "+place_parent+" ORDER BY "+KEY_GEO_TEXT_FAV_STORE_NAME;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area.add(cursor.getString(cursor.getColumnIndex(KEY_GEO_TEXT_FAV_AREA)));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}

	/*
	 * Delete Fav by id
	 */
	void deleteGeoFavText(int store_id)
	{
		try
		{

			open();
			sqlDataBase.delete(TABLE_GEOTEXT_FAV_SEARCH, KEY_GEO_TEXT_FAV_SEARCH_ID+"="+store_id, null);
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{

			close();
		}

	}

	public void updateGeoFav_FavById(int id,String isFav)
	{
		try
		{
			ContentValues cv=new ContentValues();
			cv.put(KEY_GEO_TEXT_FAV_FAVOURITED, isFav);
			int l=sqlDataBase.update(TABLE_GEOTEXT_FAV_SEARCH, cv, KEY_GEO_TEXT_FAV_SEARCH_ID + "=" + id, null);
			Log.i("Updated","Updated "+l+" STORE ID "+id+" isFAV "+isFav);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	//******************************** Store Table ******************************************//


	/*
	 * Getting Store name from Store  table
	 */

	public String getStoreName(int store_id)
	{
		String title = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_NAME+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				title=cursor.getString(cursor.getColumnIndex(KEY_STORE_NAME));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return title;

	}

	/*
	 * Getting Street name from Store  table
	 */

	public String getStreet(int store_id)
	{
		String street = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_STREET+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				street=cursor.getString(cursor.getColumnIndex(KEY_STORE_STREET));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return street;

	}

	/*
	 * Getting Landmark name from Store  table
	 */

	public String getLandmark(int store_id)
	{
		String landmark = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_LANDMARK+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				landmark=cursor.getString(cursor.getColumnIndex(KEY_STORE_LANDMARK));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return landmark;

	}

	/*
	 * Getting Area name from Store  table
	 */

	public String getArea(int store_id)
	{
		String area = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_AREA+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				area=cursor.getString(cursor.getColumnIndex(KEY_STORE_AREA));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return area;

	}

	/*
	 * Getting City name from Store  table
	 */

	public String getCity(int store_id)
	{
		String city = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_CITY+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				city=cursor.getString(cursor.getColumnIndex(KEY_STORE_CITY));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return city;

	}

	/*
	 * Getting Pincode from Store  table
	 */

	public String getPincode(int store_id)
	{
		String pincode = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_PINCODE+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				pincode=cursor.getInt(cursor.getColumnIndex(KEY_STORE_PINCODE))+"";


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return pincode;

	}


	/*
	 * Getting MOBILE_NO from Store  table
	 */

	public String getMobileNo(int store_id)
	{
		String mobileno = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_MOB_NO1+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				mobileno=cursor.getString(cursor.getColumnIndex(KEY_STORE_MOB_NO1))+"";


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return mobileno;

	}

	/*
	 * Getting GEOLOCATION from Store  table
	 */

	public String getGeoLocation(int store_id)
	{
		String geoLocation = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_GEOLOCATION+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				geoLocation=cursor.getString(cursor.getColumnIndex(KEY_STORE_GEOLOCATION));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return geoLocation;

	}

	/*
	 * Getting Photo from Store table
	 */

	public String getPhoto(int store_id)
	{
		String photo = "";
		try
		{

			open();



			String query="SELECT "+KEY_STORE_PHOTO+" FROM "+TABLE_BUSINESS_STORE+" WHERE "+KEY_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				photo=cursor.getString(cursor.getColumnIndex(KEY_STORE_PHOTO));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return photo;

	}

	/*
	 * Getting Description from Business  table
	 */

	public String getBusinessDesc(int business_id)
	{
		String desc = "";
		try
		{

			open();



			String query="SELECT "+KEY_BUSINESS_DESCRIPTION+" FROM "+TABLE_BUSINESS+" WHERE "+KEY_BUSINESS_ID+"=" + business_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				desc=cursor.getString(cursor.getColumnIndex(KEY_BUSINESS_DESCRIPTION));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return desc;

	}

	/*
	 * Getting LOGO from Business  table
	 */
	public String getBusinessLogo(int business_id)
	{
		String logo = "";
		try
		{

			open();



			String query="SELECT "+KEY_BUSINESS_LOGO+" FROM "+TABLE_BUSINESS+" WHERE "+KEY_BUSINESS_ID+"=" + business_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				logo=cursor.getString(cursor.getColumnIndex(KEY_BUSINESS_LOGO));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return logo;

	}

	/*
	 * Getting TIME1 OPEN from Operation  table
	 */
	public String getTime1_Open(int store_id)
	{
		String time1 = "";
		try
		{

			open();



			String query="SELECT "+KEY_STOREOP_TIME1_OPEN+" FROM "+TABLE_STORE_TIME+" WHERE "+KEY_STOREOP_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				time1=cursor.getString(cursor.getColumnIndex(KEY_STOREOP_TIME1_OPEN));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return time1;

	}

	/*
	 * Getting TIME1 CLOSE from Operation  table
	 */
	public String getTime1_Close(int store_id)
	{
		String time1_close = "";
		try
		{

			open();

			String query="SELECT "+KEY_STOREOP_TIME1_CLOSE+" FROM "+TABLE_STORE_TIME+" WHERE "+KEY_STOREOP_STORE_ID+"=" + store_id;

			Cursor  cursor = sqlDataBase.rawQuery(query,null);


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
			{
				/*	data+=c.getString(iRowid)+" "+c.getString(iDate)+" "+c.getString(iCost)+"\n";*/
				time1_close=cursor.getString(cursor.getColumnIndex(KEY_STOREOP_TIME1_CLOSE));


			}
			cursor.close();
		}catch(SQLException sqx)
		{
			sqx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		return time1_close;

	}






	public void addMerchantDetails(ArrayList<MerchantDetails> merchantArr){
		for(int i=0;i<merchantArr.size();i++){

			MerchantDetails merchant = merchantArr.get(i);
			ContentValues values = new ContentValues();
			Log.d("==StrorName==", "== "+i+" "+merchant.getStore_name()+" -- "+merchant.getFav());
			values.put(id, merchant.getId());
			values.put(businessId, merchant.getBusinessId());
			values.put(store_name, merchant.getStore_name());
			values.put(building, merchant.getBuilding());
			values.put(street, merchant.getStreet());
			values.put(landmark, merchant.getLandmark());
			values.put(area, merchant.getArea());
			values.put(pincode, merchant.getPincode());
			values.put(city, merchant.getCity());
			values.put(state, merchant.getState());
			values.put(country, merchant.getCountry());
			values.put(latitude, merchant.getLatitude());
			values.put(longitude, merchant.getLongitude());
			values.put(tel_no1, merchant.getTel_no1());
			values.put(tel_no2, merchant.getTel_no2());
			values.put(mob_no1, merchant.getMob_no1());
			values.put(mob_no2, merchant.getMob_no2());
			values.put(fax_no1, merchant.getFax_no1());
			values.put(fax_no2, merchant.getFax_no2());
			values.put(toll_free_no1, merchant.getToll_free_no1());
			values.put(toll_free_no1, merchant.getToll_free_no2());
			values.put(photo, merchant.getPhoto());

			/*Log.i("Image Url:", "imagePath "+"http://stage.phonethics.in/hyperlocal/"+merchant.getPhoto());*/

			values.put(fav, merchant.getFav());


			try {
				sqlDataBase.insert(MERCHANT_TABLE, null, values);
			}
			catch (Exception e)
			{
				Log.e("==Inert DB Error==", e.toString());
				e.printStackTrace();
			}

		}
	}

	public ArrayList<MerchantDetails> getStoreNames(){

		ArrayList<MerchantDetails> storeArray = new ArrayList<MerchantDetails>();
		Cursor cursor;

		try{

			cursor = sqlDataBase.query(
					MERCHANT_TABLE, new String[]{store_name,building,street,landmark,area,pincode,city,state,country,businessId,photo,fav},null,null,null,null,null,null
					);


			cursor.moveToFirst();
			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			if (!cursor.isAfterLast()){
				do{

					/*storeArray.add(cursor.getString(0));*/

					MerchantDetails merchant = new MerchantDetails();
					merchant.setStore_name(cursor.getString(0));
					merchant.setBuilding(cursor.getString(1));
					merchant.setStreet(cursor.getString(2));
					merchant.setLandmark(cursor.getString(3));
					merchant.setArea(cursor.getString(4));
					merchant.setPincode(cursor.getString(5));
					merchant.setCity(cursor.getString(6));
					merchant.setState(cursor.getString(7));
					merchant.setCountry(cursor.getString(8));
					merchant.setBusinessId(cursor.getString(9));
					merchant.setPhoto(cursor.getString(10));
					merchant.setFav(cursor.getString(11));
					storeArray.add(merchant);

				}while(cursor.moveToNext());
			}


			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return storeArray;
	}


	public ArrayList<Object> getStoreIds(){

		ArrayList<Object> iDArray = new ArrayList<Object>();
		Cursor cursor;

		try{

			cursor = sqlDataBase.query(
					MERCHANT_TABLE, new String[]{businessId},null,null,null,null,null,null
					);


			cursor.moveToFirst();
			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			if (!cursor.isAfterLast()){
				do{

					iDArray.add(cursor.getString(0));

				}while(cursor.moveToNext());
			}


			cursor.close();
		}catch (SQLException e) 
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
		return iDArray;
	}

	public void addFav(String id){
		open();
		ContentValues values = new ContentValues();
		values.put(fav, "1");
		try {
			sqlDataBase.update(MERCHANT_TABLE, values, businessId + "=" + id, null);
			close();
		}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}

	public void removeFav(String id){
		open();
		ContentValues values = new ContentValues();
		values.put(fav, "0");
		try {
			sqlDataBase.update(MERCHANT_TABLE, values, businessId + "=" + id, null);
			close();
		}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}

	public ArrayList<MerchantDetails> getFavStores(){

		ArrayList<MerchantDetails> arr = new ArrayList<MerchantDetails>();
		Cursor cursor;
		String query = "SELECT * FROM " + MERCHANT_TABLE + " WHERE " + fav+"=1";
		try{
			cursor = sqlDataBase.rawQuery(query, null);
			/*cursor.moveToFirst();*/
			int iStore_name = cursor.getColumnIndex(store_name);
			int iBuilding	= cursor.getColumnIndex(building);
			int iStreet = cursor.getColumnIndex(street);
			int iLandmark = cursor.getColumnIndex(landmark);
			int iArea		= cursor.getColumnIndex(area);
			int iPincode = cursor.getColumnIndex(pincode);
			int iCity	= cursor.getColumnIndex(city);
			int iState = cursor.getColumnIndex(state);
			int iCountry = cursor.getColumnIndex(country);
			int iBusinessId = cursor.getColumnIndex(businessId);
			int iPhoto=cursor.getColumnIndex(photo);
			int iFav = cursor.getColumnIndex(fav);

			// if there is data available after the cursor's pointer, add
			// it to the ArrayList that will be returned by the method.
			/*	if (!cursor.isAfterLast()){
			do{

				//storeArray.add(cursor.getString(0));


				MerchantDetails merchant = new MerchantDetails();
				merchant.setStore_name(cursor.getString(0));
				merchant.setBuilding(cursor.getString(1));
				merchant.setStreet(cursor.getString(2));
				merchant.setLandmark(cursor.getString(3));
				merchant.setArea(cursor.getString(4));
				merchant.setPincode(cursor.getString(5));
				merchant.setCity(cursor.getString(6));
				merchant.setState(cursor.getString(7));
				merchant.setCountry(cursor.getString(8));

				arr.add(merchant);

			}while(cursor.moveToNext());
		}*/


			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				MerchantDetails merchant = new MerchantDetails();
				merchant.setStore_name(cursor.getString(iStore_name));
				merchant.setBuilding(cursor.getString(iBuilding));
				merchant.setStreet(cursor.getString(iStreet));
				merchant.setLandmark(cursor.getString(iLandmark));
				merchant.setArea(cursor.getString(iArea));
				merchant.setPincode(cursor.getString(iPincode));
				merchant.setCity(cursor.getString(iCity));
				merchant.setState(cursor.getString(iState));
				merchant.setCountry(cursor.getString(iCountry));
				merchant.setBusinessId(cursor.getString(iBusinessId));
				merchant.setPhoto(cursor.getString(iPhoto));
				merchant.setFav(cursor.getString(iFav));
				arr.add(merchant);
			}

			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

		return arr;
	}


	public ArrayList<String> getStoresId1(){
		ArrayList<String> idArr = new ArrayList<String>();
		Cursor cursor;
		String query = "SELECT BUSINESS_ID FROM " + MERCHANT_TABLE;
		String queryFav = "SELECT BUSINESS_ID FROM " + MERCHANT_TABLE + " WHERE " + fav+"=1";

		try{

			cursor = sqlDataBase.rawQuery(query, null);
			int iBus_ids = cursor.getColumnIndex(businessId);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				idArr.add(cursor.getString(iBus_ids));
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

		return idArr;
	}

	public ArrayList<String> getStoresFav(){
		ArrayList<String> idArr = new ArrayList<String>();
		Cursor cursor;

		String query = "SELECT BUSINESS_ID FROM " + MERCHANT_TABLE + " WHERE " + fav+"=1";

		try{

			cursor = sqlDataBase.rawQuery(query, null);
			int iBus_ids = cursor.getColumnIndex(businessId);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				idArr.add(cursor.getString(iBus_ids));
			}
			cursor.close();
		}catch (SQLException e) {
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}

		return idArr;
	}


	public long getCount(){ 
		open();
		long rowCount = DatabaseUtils.queryNumEntries(sqlDataBase, MERCHANT_TABLE);
		Log.d("======== 1", "cursor count "+ rowCount);
		close();
		return rowCount ;

	}

	public String getId(int start,int offset){
		open();
		String id="";
		String query = "SELECT "+businessId+" FROM "+MERCHANT_TABLE+" LIMIT "+ start+","+offset;
		Cursor cursor;
		try{
			cursor = sqlDataBase.rawQuery(query, null);
			if(cursor!=null){
				cursor.moveToFirst();
			}
			int iBus_ids = cursor.getColumnIndex(businessId);
			id = cursor.getString(iBus_ids);
			Log.d("===", "id === "+id);
			close();
		}catch(Exception ex){
			Log.e("DB ERROR", ex.toString());
			ex.printStackTrace();
		}
		return id;
	}


	/*
	 *  Queries for Area Table ======================================
	 */

	public long createArea(Integer id, String name, String latitude, String longitude, String pincode, String city, String country, String countryCode, String isoCode, String placeId,String locality,String state,int active,int published_status){

		Log.d("GETINAREA","GETINAREA" + name);

		ContentValues cv=new ContentValues();
		cv.put(area_id, id);
		cv.put(area_name, name);
		cv.put(area_latitude, latitude);
		cv.put(area_longitude, longitude);
		cv.put(area_pincode, pincode);
		cv.put(area_city, city);
		cv.put(area_country, country);
		cv.put(area_country_code, countryCode);
		cv.put(area_iso_code, isoCode);
		cv.put(area_placeID, placeId);
		cv.put(area_locality, locality);
		cv.put(area_state, state);
		cv.put(area_active, area_active);
		cv.put(area_published_status, published_status);

		return sqlDataBase.insertWithOnConflict(TABLE_AREAS, null, cv,SQLiteDatabase.CONFLICT_REPLACE);
	}

	public Integer updatePlaceStatus(int id,int state)
	{
		int rows_affected=0;
		try
		{
			open();
			ContentValues cv2=new ContentValues();
			cv2.put(area_active, 0);
			sqlDataBase.update(TABLE_AREAS,cv2,null, null);
			close();
			
			open();
			ContentValues cv=new ContentValues();
			cv.put(area_active, state);
			rows_affected=sqlDataBase.update(TABLE_AREAS,cv,area_id+"="+id, null);
			close();
			
			

			Log.i("ROW UPDATED", "ROW UPDATED "+rows_affected);
			/*	getAllCurrentPages();*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
			return rows_affected;
		}
		return rows_affected;

	}


	public String getAreaNameById(int areaId)
	{
		String addr  = "";

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS +" WHERE "+ area_id+" = " + areaId;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			addr=cursor.getString(cursor.getColumnIndex(area_name));

		}

		cursor.close();
		close();
		return addr;
	}


	public String getAreaStatus(int areaId)
	{
		String area_active  = "";

		String query="SELECT "+area_active+" FROM "+TABLE_AREAS +" WHERE "+ area_id+" = " + areaId;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area_active=cursor.getString(cursor.getColumnIndex(area_active));

		}

		cursor.close();
		close();
		return area_active;
	}

	public String getActiveArea()
	{
		String area  = "";

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(area_name));

		}

		cursor.close();
		close();
		return area;
	}
	
	public String getActiveAreaPin()
	{
		String area  = "";

		String query="SELECT "+area_pincode+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(area_pincode));

		}

		cursor.close();
		close();
		return area;
	}
	public String getActiveAreaCity()
	{
		String area  = "";

		String query="SELECT "+area_city+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(area_city));

		}

		cursor.close();
		close();
		return area;
	}
	
	public String getAreaLat()
	{
		String lat  = "";

		String query="SELECT "+area_latitude+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			lat=cursor.getString(cursor.getColumnIndex(area_latitude));

		}

		cursor.close();
		close();
		return lat;
	}
	public String getAreaLong()
	{
		String longitude  = "";

		String query="SELECT "+area_longitude+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			longitude=cursor.getString(cursor.getColumnIndex(area_longitude));

		}

		cursor.close();
		close();
		return longitude;
	}
	
	public String getActiveAreaID()
	{
		String area  = "";

		String query="SELECT "+area_id+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(area_id));

		}

		cursor.close();
		close();
		return area;
	}
	
	public String getActiveAreaPlace()
	{
		String area  = "";

		String query="SELECT "+area_placeID+" FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			area=cursor.getString(cursor.getColumnIndex(area_placeID));

		}

		cursor.close();
		close();
		return area;
	}

	public ArrayList<String> getAllAreas()
	{
		ArrayList<String>  allAreas =new ArrayList<String>();

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreas.add(cursor.getString(cursor.getColumnIndex(area_name)));

		}

		cursor.close();
		close();
		return allAreas;
	}
	public ArrayList<String> getAllPublishedAreas(int status)
	{
		ArrayList<String>  allAreas =new ArrayList<String>();

		String query="SELECT "+area_name+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreas.add(cursor.getString(cursor.getColumnIndex(area_name)));

		}

		cursor.close();
		close();
		return allAreas;
	}

	public ArrayList<String> getAllAreaIds()
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();

		String query="SELECT "+area_id+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getInt(cursor.getColumnIndex(area_id))+"");

		}

		cursor.close();
		close();
		return allAreasIds;
	}
	
	public ArrayList<String> getAllPublishedAreaIds(int status)
	{
		ArrayList<String>  allAreasIds =new ArrayList<String>();

		String query="SELECT "+area_id+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIds.add(cursor.getInt(cursor.getColumnIndex(area_id))+"");

		}

		cursor.close();
		close();
		return allAreasIds;
	}

	public ArrayList<String> getAllAreaLat()
	{
		ArrayList<String>  allAreaslats =new ArrayList<String>();

		String query="SELECT "+area_latitude+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslats.add(cursor.getString(cursor.getColumnIndex(area_latitude)));

		}

		cursor.close();
		close();
		return allAreaslats;
	}
	
	public ArrayList<String> getAllPublishedAreaLat(int status)
	{
		ArrayList<String>  allAreaslats =new ArrayList<String>();

		String query="SELECT "+area_latitude+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslats.add(cursor.getString(cursor.getColumnIndex(area_latitude)));

		}

		cursor.close();
		close();
		return allAreaslats;
	}

	public ArrayList<String> getAllAreaLong()
	{
		ArrayList<String>  allAreaslong=new ArrayList<String>();

		String query="SELECT "+area_longitude+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslong.add(cursor.getString(cursor.getColumnIndex(area_longitude)));

		}

		cursor.close();
		close();
		return allAreaslong;
	}
	
	public ArrayList<String> getAllPublishedAreaLong(int status)
	{
		ArrayList<String>  allAreaslong=new ArrayList<String>();

		String query="SELECT "+area_longitude+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreaslong.add(cursor.getString(cursor.getColumnIndex(area_longitude)));

		}

		cursor.close();
		close();
		return allAreaslong;
	}
	

	public ArrayList<String> getAllAreaPin()
	{
		ArrayList<String>  allAreasPin=new ArrayList<String>();

		String query="SELECT "+area_pincode+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPin.add(cursor.getString(cursor.getColumnIndex(area_pincode)));

		}

		cursor.close();
		close();
		return allAreasPin;
	}
	
	public ArrayList<String> getAllPublishedAreaPin(int status)
	{
		ArrayList<String>  allAreasPin=new ArrayList<String>();

		String query="SELECT "+area_pincode+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPin.add(cursor.getString(cursor.getColumnIndex(area_pincode)));

		}

		cursor.close();
		close();
		return allAreasPin;
	}

	public ArrayList<String> getAllAreaCity()
	{
		ArrayList<String>  allAreasCity=new ArrayList<String>();

		String query="SELECT "+area_city+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCity.add(cursor.getString(cursor.getColumnIndex(area_city)));

		}

		cursor.close();
		close();
		return allAreasCity;
	}
	
	public ArrayList<String> getAllPublishedAreaCity(int status)
	{
		ArrayList<String>  allAreasCity=new ArrayList<String>();

		String query="SELECT "+area_city+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCity.add(cursor.getString(cursor.getColumnIndex(area_city)));

		}

		cursor.close();
		close();
		return allAreasCity;
	}
	
	

	public ArrayList<String> getAllAreaCountries()
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();

		String query="SELECT "+area_country+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(area_country)));

		}

		cursor.close();
		close();
		return allAreasCountry;
	}
	
	
	
	public ArrayList<String> getAllPublishedAreaCountries(int status)
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();

		String query="SELECT "+area_country+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(area_country)));

		}

		cursor.close();
		close();
		return allAreasCountry;
	}
	
	public ArrayList<String> getAllAreaCountryCodes()
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();

		String query="SELECT "+area_country_code+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(area_country_code)));

		}

		cursor.close();
		close();
		return allAreasCountry;
	}
	
	public ArrayList<String> getAllPublishedAreaCountryCodes(int status)
	{
		ArrayList<String>  allAreasCountry=new ArrayList<String>();

		String query="SELECT "+area_country_code+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCountry.add(cursor.getString(cursor.getColumnIndex(area_country_code)));

		}

		cursor.close();
		close();
		return allAreasCountry;
	}


	public ArrayList<String> getAllAreaCodes()
	{
		ArrayList<String>  allAreasCodes=new ArrayList<String>();

		String query="SELECT "+area_country_code+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasCodes.add(cursor.getString(cursor.getColumnIndex(area_country_code)));

		}

		cursor.close();
		close();
		return allAreasCodes;
	}


	
	public ArrayList<String> getAllAreaIsoCode()
	{
		ArrayList<String>  allAreasIsoCode=new ArrayList<String>();

		String query="SELECT "+area_iso_code+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIsoCode.add(cursor.getString(cursor.getColumnIndex(area_iso_code)));

		}

		cursor.close();
		close();
		return allAreasIsoCode;
	}
	
	public ArrayList<String> getAllPublishedAreaIsoCode(int status)
	{
		ArrayList<String>  allAreasIsoCode=new ArrayList<String>();

		String query="SELECT "+area_iso_code+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasIsoCode.add(cursor.getString(cursor.getColumnIndex(area_iso_code)));

		}

		cursor.close();
		close();
		return allAreasIsoCode;
	}

	public ArrayList<String> getAllAreaPlaceId()
	{
		ArrayList<String>  allAreasPlaceIds=new ArrayList<String>();

		String query="SELECT "+area_placeID+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPlaceIds.add(cursor.getString(cursor.getColumnIndex(area_placeID)));

		}

		cursor.close();
		close();
		return allAreasPlaceIds;
	}
	public ArrayList<String> getAllPublishedAreaPlaceId(int status)
	{
		ArrayList<String>  allAreasPlaceIds=new ArrayList<String>();

		String query="SELECT "+area_placeID+" FROM "+TABLE_AREAS+" where "+area_published_status+" = "+status;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasPlaceIds.add(cursor.getString(cursor.getColumnIndex(area_placeID)));

		}

		cursor.close();
		close();
		return allAreasPlaceIds;
	}

	public ArrayList<String> getAllAreaLocality()
	{
		ArrayList<String>  allAreasLocality=new ArrayList<String>();

		String query="SELECT "+area_locality+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasLocality.add(cursor.getString(cursor.getColumnIndex(area_locality)));

		}

		cursor.close();
		close();
		return allAreasLocality;
	}

	public ArrayList<String> getAllAreaState()
	{
		ArrayList<String>  allAreasState=new ArrayList<String>();

		String query="SELECT "+area_state+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasState.add(cursor.getString(cursor.getColumnIndex(area_state)));

		}

		cursor.close();
		close();
		return allAreasState;
	}

	public ArrayList<String> getAllAreaActive()
	{
		ArrayList<String>  allAreasActive=new ArrayList<String>();

		String query="SELECT "+area_active+" FROM "+TABLE_AREAS;

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			allAreasActive.add(cursor.getString(cursor.getColumnIndex(area_active)));

		}

		cursor.close();
		close();
		return allAreasActive;
	}

	public void deleteAreasTable()
	{

		try
		{
			open();
			sqlDataBase.delete(TABLE_AREAS, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void deletePlacesTable()
	{

		try
		{
			open();
			sqlDataBase.delete(TABLE_PLACE_CHOOSER, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 *  Queries for Table Date_Categories
	 */


	/*public long createDateCategory(String id, String name){

		Log.d("GETINDATE","GETINDATE" + name);

		ContentValues cv=new ContentValues();
		cv.put(date_id, id);
		cv.put(date_name, name);

		return sqlDataBase.insert(TABLE_DATE_CATEGORIES, null, cv);
	}
*/
	public String getDateTypeById(String dateId)
	{
		String dateType  = "";

		String query="SELECT "+date_name+" FROM "+TABLE_DATE_CATEGORIES +" WHERE "+ date_id+" = '"+ dateId+"'";

		open();
		Cursor  cursor = sqlDataBase.rawQuery(query,null);

		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
		{
			dateType=cursor.getString(cursor.getColumnIndex(date_name));

		}

		cursor.close();
		close();
		return dateType;
	}

	public void deleteTableDateCategory()
	{

		try
		{
			open();
			sqlDataBase.delete(TABLE_DATE_CATEGORIES, null, null);
			close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public long getAreaRowCount()
	{
		String rawQuery="SELECT COUNT("+key_area_auto_id+") AS ROW_COUNT FROM "+TABLE_AREAS ;

		Log.i("ROW COUNT","ROW COUNT AREA QUERY "+ rawQuery);

		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		Log.i("ROW COUNT","ROW COUNT AREA "+ rowcount);
		return rowcount;

	}
	
	public long getActiveAreaRowCount()
	{
		String rawQuery="SELECT COUNT("+key_area_auto_id+") AS ROW_COUNT FROM "+TABLE_AREAS +" WHERE "+ area_active+" = " + 1;

		Log.i("ROW COUNT","ROW COUNT AREA QUERY "+ rawQuery);

		long rowcount=0;

		try
		{
			open();
			Cursor c=sqlDataBase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();
			close();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			close();
		}
		Log.i("ROW COUNT","ROW COUNT AREA "+ rowcount);
		return rowcount;

	}

}
