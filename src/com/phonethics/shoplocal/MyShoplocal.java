package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.model.NewsFeedModel;
import com.phonethics.networkcall.FavouritePostResultReceiver;
import com.phonethics.networkcall.FavouritePostResultReceiver.FavouritePostInterface;
import com.phonethics.networkcall.FavouritePostService;
import com.phonethics.networkcall.MyShopLocalReceiver;
import com.phonethics.networkcall.MyShopLocalReceiver.MyShop;
import com.phonethics.networkcall.MyShopLocalService;
import com.phonethics.networkcall.ShareResultReceiver;
import com.phonethics.networkcall.ShareResultReceiver.ShareResultInterface;
import com.phonethics.networkcall.ShareService;
import com.phonethics.shoplocal.NewsFeedCategory.NewsFeedAdapter;
import com.squareup.picasso.Picasso;



public class MyShoplocal extends SherlockActivity implements MyShop, FavouritePostInterface, ShareResultInterface {

	ActionBar actionBar;

	Activity context;

	PullToRefreshListView listMyShoplocal;
	ProgressBar myShoplocalProgress;

	TextView txtMyShopLocalCounter;


	NetworkCheck network;

	static String API_HEADER;
	static String API_VALUE;

	static String user_id;
	static String auth_id;

	static String PLACE_URL;
	static String FAVOURITE_API;

	SessionManager session;

	//User Name
	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	MyShopLocalReceiver mReceiver;

	ArrayList<String> NEWS_FEED_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_NAME=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_AREA_ID=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_MOBNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO3=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_TELLNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_LATITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_LONGITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DISTANCE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_POST_TYPE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_TITLE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DESCRIPTION=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_image_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_thumb_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url3=new ArrayList<String>();

	ArrayList<String> NEWS_DATE=new ArrayList<String>();
	ArrayList<String> NEWS_IS_OFFERED=new ArrayList<String>();
	ArrayList<String> NEWS_OFFER_DATE_TIME=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_place_total_like=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_place_total_share=new ArrayList<String>();


	int post_count=20;
	int page=0;


	boolean isRefreshing=false;

	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

/*	ImageLoader imageLoaderList;*/

	String PHOTO_PARENT_URL;

	NewsFeedAdapter adapter=null;

	//Drawer
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ArrayList<Item> items=new ArrayList<Item>();
	ArrayList<Integer>list_icons=new ArrayList<Integer>();

	boolean isDrawerClosed=false;

	String drawer_item="";

	DrawerExpandibleAdapter drawerAdapter;
	ExpandableListView drawerList;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	private static long back_pressed;

	LinearLayout shopLocalCounterLayout;
	RelativeLayout preLoginLayout;
	Button loginButton;
	Button cancelButton;
	TextView loginTextInfo;
	Typeface tf;

	DrawerClass drawerClass;

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	ShareResultReceiver shareServiceResult;
	FavouritePostResultReceiver mFav;

	String TEMP_POST_ID="";

	static String BROADCAST_URL;
	static String LIKE_API;
	static String SHARE_URL;

	static String USER_ID;
	static String AUTH_ID;

	String VIA = "Android";



	int POST_LIKE=2;

	Button gotoMarketButton;
	Button gotoOffersButton;

	LinearLayout noRecordsLayout;

	private UiLifecycleHelper uiHelper;
	
	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;
	
	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	LocalNotification localNotification;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_shoplocal);

		context=this;

		localNotification=new LocalNotification(getApplicationContext());
		
		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		drawerClass=new DrawerClass(context);

		network=new NetworkCheck(context);

	/*	imageLoaderList=new ImageLoader(context);*/

		mReceiver=new MyShopLocalReceiver(new Handler());
		mReceiver.setReceiver(this);

		actionBar=getSupportActionBar();
		actionBar.setTitle(getResources().getString(R.string.itemMyShoplocalOffers));
		actionBar.setHomeButtonEnabled(true);
		actionBar.setIcon(R.drawable.ic_drawer);

		session=new SessionManager(getApplicationContext());
		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		moreData=drawerClass.isShowMore();

		mFav=new FavouritePostResultReceiver(new Handler());
		mFav.setReceiver(this);

		shareServiceResult = new ShareResultReceiver(new Handler());
		shareServiceResult.setReceiver(this);

		//Login Details
		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		user_id=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		auth_id=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		PLACE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api);
		FAVOURITE_API=getResources().getString(R.string.favourite_posts);

		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		LIKE_API=getResources().getString(R.string.like);

		SHARE_URL = "/" + getResources().getString(R.string.share);


		listMyShoplocal=(PullToRefreshListView)findViewById(R.id.listMyShoplocal);
		txtMyShopLocalCounter=(TextView)findViewById(R.id.txtMyShopLocalCounter);
		myShoplocalProgress=(ProgressBar)findViewById(R.id.myShoplocalProgress);

		shopLocalCounterLayout=(LinearLayout)findViewById(R.id.shopLocalCounterLayout);
		preLoginLayout=(RelativeLayout)findViewById(R.id.preLoginLayout);
		loginButton=(Button)findViewById(R.id.loginButton);
		cancelButton=(Button)findViewById(R.id.cancelButton);
		loginTextInfo=(TextView)findViewById(R.id.loginTextInfo);

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);

		gotoMarketButton=(Button)findViewById(R.id.gotoMarketButton);
		gotoOffersButton=(Button)findViewById(R.id.gotoOffersButton);
		noRecordsLayout=(LinearLayout)findViewById(R.id.noRecordsLayout);



		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});

		gotoMarketButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				drawer_item="";
				Intent intent=new Intent(context,DefaultSearchList.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
		gotoOffersButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				drawer_item="";
				Intent intent=new Intent(context,NewsFeedActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getResources().getString(R.string.itemMyShoplocalOffers));
				isDrawerClosed=true;

				//						Toast.makeText(context, "Closed",Toast.LENGTH_SHORT).show();
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				isDrawerClosed=false;


				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerStateChanged(int newState) {
				// TODO Auto-generated method stub
				super.onDrawerStateChanged(newState);
			}

			@Override
			public void syncState() {
				// TODO Auto-generated method stub
				super.syncState();
			}


		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		//Creating Drawer
		createDrawer();

		if(session.isLoggedInCustomer())
		{
			preLoginLayout.setVisibility(View.GONE);
			listMyShoplocal.setVisibility(View.VISIBLE);
			shopLocalCounterLayout.setVisibility(View.VISIBLE);
		}
		else
		{
			preLoginLayout.setVisibility(View.VISIBLE);
			listMyShoplocal.setVisibility(View.GONE);
			shopLocalCounterLayout.setVisibility(View.GONE);
		}

		adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
				NEWS_FEED_place_total_like, NEWS_FEED_place_total_share,NEWS_FEED_MOBNO1,NEWS_FEED_MOBNO2,NEWS_FEED_MOBNO3,NEWS_FEED_TELLNO1,NEWS_FEED_TELLNO2,NEWS_FEED_TELLNO3,NEWS_OFFER_DATE_TIME);
		listMyShoplocal.setAdapter(adapter);

		//Pull to refresh 

		listMyShoplocal.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				try
				{
					if(network.isNetworkAvailable())
					{
						if(!myShoplocalProgress.isShown())
						{
							isRefreshing=true;
							page=0;
							loadNewsFeeds();
						}
					}
					else
					{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		listMyShoplocal.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub
				try
				{
					if(network.isNetworkAvailable())
					{

						if(NEWS_FEED_ID.size()==Integer.parseInt(TOTAL_RECORDS))
						{

						}
						else if(page<Integer.parseInt(TOTAL_PAGES))
						{

							if(!myShoplocalProgress.isShown())
							{
								//								page++;
								Log.i("Page", "Page "+page);
								//Load Stores from Internet
								loadNewsFeeds();

								Log.i("ID SIZE","NEWS FEED ID SIZE  "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
							}
							else
							{
								showToast(getResources().getString(R.string.loadingAlert));

							}

						}
						else
						{
							Log.i("ID SIZE","NEWS FEED ID SIZE ELSE "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
							myShoplocalProgress.setVisibility(View.GONE);
						}
					}
					else
					{
						listMyShoplocal.onRefreshComplete();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}
		});

		try
		{
			if(network.isNetworkAvailable())
			{
				if(session.isLoggedInCustomer())
				{
					clearArray();
					loadNewsFeeds();
				}
			}
			else
			{
				showToast(getResources().getString(R.string.noInternetConnection));
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		loginButton.setTypeface(tf);
		cancelButton.setTypeface(tf);
		loginTextInfo.setTypeface(tf);

		loginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EventTracker.logEvent("MyShoplocal_Login", false);
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

			}
		});

		EventTracker.startLocalyticsSession(getApplicationContext());
		
		
		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		
		
		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		
		
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
		        pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
		
		
		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}
				
				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
				
			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}
		
		
		/** Anirudh custom share dialog */
		dialogShare = new Dialog(MyShoplocal.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));
		
		

	    
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		uiHelper.onPause();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();

			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();

		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();
			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			drawer_item="";
			if(session.isLoggedInCustomer())
			{
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}
			else
			{
				login();
			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}*/
			
		
			showDialogTab();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			Log.i("Event name",event);
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			
			drawer_item="";
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
		dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}
	
	void showDialogTab(){
		
		dialogShare.show();
		
		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){
					
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(MyShoplocal.this)
						.setLink("http://shoplocal.co.in")
						.build();
						uiHelper.trackPendingDialogCall(shareDialog.present());
						dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}
				
				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}
				
				dismissDialog();
				
			}
		});
	}
	
	void dismissDialog(){
		dialogShare.dismiss();
	}


	void loadNewsFeeds()
	{
		if(network.isNetworkAvailable())
		{
			EventTracker.logEvent("MyShoplocal_Fetch", false);

			Intent intent=new Intent(context, MyShopLocalService.class);
			intent.putExtra("myShopLocal",mReceiver);
			intent.putExtra("URL", PLACE_URL+FAVOURITE_API);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("count",post_count);
			intent.putExtra("page",page+1);
			intent.putExtra("user_id",user_id);
			intent.putExtra("auth_id",auth_id);
			startService(intent);
			myShoplocalProgress.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		try
		{
//			SearchView searchView = new SearchView(actionBar.getThemedContext());
//			searchView.setQueryHint("Search");
//			searchView.setIconified(true);
//
//			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
//			.setIcon(R.drawable.abs__ic_search)
//			.setActionView(searchView)
//			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//
//			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//
//				@Override
//				public boolean onQueryTextSubmit(String newText) {
//
//					Intent intent=new Intent(context,SearchActivity.class);
//					intent.putExtra("genericSearch", true);
//					intent.putExtra("search_text", newText);
//					startActivity(intent);
//					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
//					return true;
//				}
//
//
//
//				@Override
//				public boolean onQueryTextChange(String newText) {
//
//
//					return true;
//				}
//			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;

	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}


		return true;
	}

	@Override
	public void myShopLocalFeeds(int resultCode, Bundle resultData) {

		myShoplocalProgress.setVisibility(View.GONE);

		try
		{

			String search_status=resultData.getString("status");
			String message=resultData.getString("message");
			ArrayList<NewsFeedModel> newsFeed=resultData.getParcelableArrayList("posts");

			if(search_status.equalsIgnoreCase("true"))
			{
				TOTAL_PAGES=resultData.getString("total_page");
				TOTAL_RECORDS=resultData.getString("total_record");

				Log.i("News FEED SIZE ", "News FEED SIZE "+newsFeed.size());

				try
				{

					if(isRefreshing)
					{
						clearArray();
						isRefreshing=false;
					}

					for(int i=0;i<newsFeed.size();i++)
					{
						NEWS_FEED_ID.add(newsFeed.get(i).getId());
						NEWS_FEED_NAME.add(newsFeed.get(i).getName());
						NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());

						NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
						NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
						NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());

						NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
						NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
						NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());

						NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
						NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
						NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());

						NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
						NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
						NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());

						NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());

						NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
						NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
						NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());

						NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
						NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
						NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());

						NEWS_DATE.add(newsFeed.get(i).getDate());
						NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());


						NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
						NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
						NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());
					}

					//					for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
					//					{
					//						NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
					//						NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
					//						NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );
					//
					//						NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
					//						NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
					//						NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));
					//
					//						NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
					//						NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
					//						NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));
					//
					//						NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
					//						NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
					//						NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));
					//
					//						NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
					//						NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
					//						NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));
					//
					//						NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));
					//
					//						NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
					//						NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
					//						NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));
					//
					//						NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
					//						NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
					//						NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));
					//
					//						NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
					//						NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));
					//
					//
					//						NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
					//						NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
					//						NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));
					//					}


					//Get the first visible position of listview
					//					final int old_pos = listMyShoplocal.getRefreshableView().getFirstVisiblePosition();

					//					adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
					//							NEWS_FEED_place_total_like, NEWS_FEED_place_total_share);
					//
					//					listMyShoplocal.setAdapter(adapter);

					adapter.notifyDataSetChanged();
					page++;

					//Restore visible position
					//					listMyShoplocal.post(new Runnable() {
					//
					//						@Override
					//						public void run() {
					//							// TODO Auto-generated method stub
					//							listMyShoplocal.getRefreshableView().setSelection(old_pos);
					//						}
					//					});

					Log.i("TOTAL PAGES AND RECORDS  ","TOTAL CURRENT PAGE, PAGES AND RECORDS : PAGE "+page +" TOTAL PAGES "+TOTAL_PAGES+" TOTAL_RECORD "+TOTAL_RECORDS);

					txtMyShopLocalCounter.setText(NEWS_FEED_ID.size()+"/"+TOTAL_RECORDS);

					listMyShoplocal.onRefreshComplete();
					listMyShoplocal.setOnItemClickListener(new OnItemClickListener() {


						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							if(!isRefreshing)
							{
								EventTracker.logEvent("MyShoplocal_Details", false);

								Intent intent=new Intent(context, OffersBroadCastDetails.class);
								intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
								intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
								intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
								intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
								intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
								intent.putExtra("fromCustomer", true);
								intent.putExtra("isFromStoreDetails", false);
								intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
								intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
								intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
								intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
								intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
								intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
								intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
								//								if(POST_USER_LIKE.size()==0)
								//								{
								intent.putExtra("USER_LIKE","-1");
								//								}else
								//								{
								//									intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
								//
								//								}

								context.startActivityForResult(intent, 5);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							}
						}
					});

				}catch(Exception ex)
				{
					ex.printStackTrace();

				}


			}
			else if(search_status.equalsIgnoreCase("false"))
			{
				String error_code=resultData.getString("error_code");
				//				showToast(message);
				listMyShoplocal.onRefreshComplete();
				if(session.isLoggedInCustomer())
				{
					
					if(error_code.equalsIgnoreCase("-240"))
					{
						localNotification.checkPref();
						localNotification.setFavPref(false, localNotification.isFavAlarmFired());
						
						shopLocalCounterLayout.setVisibility(View.GONE);
						noRecordsLayout.setVisibility(View.VISIBLE);
						listMyShoplocal.setVisibility(View.GONE);
					}
				}
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
					createDrawer();
					if(session.isLoggedInCustomer())
					{
						preLoginLayout.setVisibility(View.GONE);
						listMyShoplocal.setVisibility(View.VISIBLE);
						shopLocalCounterLayout.setVisibility(View.VISIBLE);
					}
					else
					{
						preLoginLayout.setVisibility(View.VISIBLE);
						listMyShoplocal.setVisibility(View.GONE);
						shopLocalCounterLayout.setVisibility(View.GONE);
					}
				}
				//				if(message.equalsIgnoreCase(getResources().getString(R.string.invalidAuth)))
				//				{
				//					session.logoutCustomer();
				//					Intent intent=new Intent(context,NewsFeedActivity.class);
				//					intent.putExtra("isNewsCategory", true);
				//					startActivity(intent);
				//					finish();
				//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				//
				//				}
			}
			else if(search_status.equalsIgnoreCase("error"))
			{
				showToast(message);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	class NewsFeedAdapter extends ArrayAdapter<String>
	{

		private int lastPosition = -1;

		Activity context;

		ArrayList<String> post_id;
		ArrayList<String> news_title;
		ArrayList<String> offer_title;
		ArrayList<String> offer_desc;

		ArrayList<String> offer_date;
		ArrayList<String> is_offer;

		ArrayList<String> photo_url1;
		ArrayList<String> photo_url2;
		ArrayList<String> photo_url3;



		ArrayList<String>mob1;
		ArrayList<String>mob2;
		ArrayList<String>mob3;
		
		ArrayList<String>tel1;
		ArrayList<String>tel2;
		ArrayList<String>tel3;
		
		ArrayList<String>valid_till;



		ArrayList<String> offer_total_like;
		ArrayList<String> offer_total_total_share;

		boolean [] isAnimEnd;   

		LayoutInflater inflator;

		Typeface tf;


		public NewsFeedAdapter(Activity context, int resource,
				int textViewResourceId,  ArrayList<String>post_id,ArrayList<String>news_title,ArrayList<String> offer_title,
				ArrayList<String> offer_desc,ArrayList<String> offer_date,
				ArrayList<String> is_offer,ArrayList<String> photo_url1,
				ArrayList<String> offer_total_like,ArrayList<String> offer_total_total_share,ArrayList<String>mob1,ArrayList<String>mob2,
				ArrayList<String>mob3,ArrayList<String>tel1,ArrayList<String>tel2,ArrayList<String>tel3,ArrayList<String>valid_till) {
			super(context, resource, textViewResourceId, offer_title);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();

			this.post_id=post_id;
			this.news_title=news_title;
			this.offer_title=offer_title;
			this.offer_desc=offer_desc;

			this.offer_date=offer_date;
			this.is_offer=is_offer;

			this.photo_url1=photo_url1;
			this.offer_total_like=offer_total_like;

			this.offer_total_total_share=offer_total_total_share;
			
			this.mob1=mob1;
			this.mob2=mob2;
			this.mob3=mob3;
			
			this.tel1=tel1;
			this.tel2=tel2;
			this.tel3=tel3;
			
			this.valid_till=valid_till;


			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_title.size()+" "+offer_desc.size()+" "+offer_date.size()+" "+is_offer.size()+" "+photo_url1.size());
			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_total_like.size()+" "+offer_total_total_share.size());

			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");


		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return offer_title.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return offer_title.get(position);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_in);
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflator.inflate(R.layout.newsfeedrow,null);

				holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
				//				holder.newsFeedOffersMonth=(TextView)convertView.findViewById(R.id.newsFeedOffersMonth);
				//
				//				holder.newsFeedviewOverLay=(View)convertView.findViewById(R.id.newsFeedviewOverLay);

				holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
				holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);

				holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
				holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);

				holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);

				holder.newFeedband=(TextView)convertView.findViewById(R.id.newFeedband);

				holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);

				holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);

				holder.newsFeedOffersText.setTypeface(tf);
				holder.newsFeedOffersDesc.setTypeface(tf);
				holder.newsFeedTotalLike.setTypeface(tf);

				holder.newsFeedTotalShare.setTypeface(tf);
				holder.newFeedband.setTypeface(tf);

				holder.newsFeedStoreName.setTypeface(tf);



				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();



			try
			{
				hold.newsFeedStoreName.setText(news_title.get(position));
				hold.newsFeedOffersText.setText(offer_title.get(position));
				hold.newsFeedOffersDesc.setText(offer_desc.get(position));

				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

				Date date_con = (Date) dt.parse(offer_date.get(position).toString());

				Date date=dateFormat.parse(offer_date.get(position).toString());
				DateFormat date_format=new SimpleDateFormat("hh:mm a");

				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
				hold.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));
				//				hold.newsFeedOffersMonth.setText(dt2.format(date_con)+"");


				// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				// }
				// else
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
				// }
				hold.newsFeedTotalLike.setText(offer_total_like.get(position));


				hold.newsFeedTotalShare.setText(offer_total_total_share.get(position));

				hold.newsFeedTotalLike.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						//							showToast("Clicked On "+news_title.get(position));
						likePost(post_id.get(position));
					}
				});
				hold.newsFeedTotalShare.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						TEMP_POST_ID=post_id.get(position);

						/*
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Share");
						alertDialogBuilder
						.setMessage("Share using")
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(true)
						.setPositiveButton("Other",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.dismiss();
							
								List<Intent> targetedShareIntents = new ArrayList<Intent>();
								Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
								sharingIntent.setType("text/plain");
								String shareBody = news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position);

								PackageManager pm = context.getPackageManager();
								List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

								for(final ResolveInfo app : activityList) {

									String packageName = app.activityInfo.packageName;
									Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
									targetedShareIntent.setType("text/plain");

									if(TextUtils.equals(packageName, "com.facebook.katana")){
										
									} else {
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, news_title.get(position) + " - " + "Offers");
										targetedShareIntent.setPackage(packageName);
										targetedShareIntents.add(targetedShareIntent);
									}

								}

								Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
								chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
								startActivityForResult(chooserIntent,6);

							}
						})
						.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.dismiss();
								
								if(photo_url1.get(position).toString().length()!=0)
								{
									String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");

									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(news_title.get(position) + " - " + "Offers")
									.setPicture(PHOTO_PARENT_URL+photo_source)
									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
								else
								{
									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(news_title.get(position) + " - " + "Offers")
									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
							}
						})
						;

						AlertDialog alertDialog = alertDialogBuilder.create();

						alertDialog.show();*/
					
						showDialog(position);
					}
				});


				hold.newFeedband.setVisibility(View.GONE);
				hold.newsIsOffer.setVisibility(View.VISIBLE);


				hold.newsFeedStoreName.startAnimation(animation);
				hold.newsFeedOffersText.startAnimation(animation);
				hold.newsFeedOffersDesc.startAnimation(animation);
				hold.newsFeedOffersDate.startAnimation(animation);
				hold.newsFeedTotalLike.startAnimation(animation);
				hold.newsFeedTotalShare.startAnimation(animation);


				if(is_offer.get(position).toString().length()!=0 && is_offer.get(position).equalsIgnoreCase("1"))
				{
					hold.newsIsOffer.setVisibility(View.VISIBLE);

					//hold.newFeedband.setText("Specail Offer : "+is_offer.get(position));
					/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
					hold.txtStore.setTextColor(Color.WHITE);*/

				}
				else
				{
					//hold.txtBackOffers.setText("offers");
					hold.newFeedband.setVisibility(View.GONE);
					hold.newsIsOffer.setVisibility(View.INVISIBLE);
					/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
					hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				Log.i("PHOTO URL ","PHOTO URL"+photo_url1.get(position).toString());
				hold.imgNewsFeedLogo.setVisibility(View.VISIBLE);
				if(photo_url1.get(position).toString().length()==0)
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));
					//hold.imgNewsFeedLogo.setVisibility(View.GONE);
					hold.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_CENTER);
					//					imageLoaderList.DisplayImage("",hold.imgNewsFeedLogo);
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(255, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(200, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.VISIBLE);
					/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgNewsFeedLogo);*/
					try{
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(hold.imgNewsFeedLogo);
					}
					catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){ 
						e.printStackTrace();
					}
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_XY);

					hold.imgNewsFeedLogo.startAnimation(animation);

				}

				//				Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
				//				convertView.startAnimation(animation);
				//				lastPosition = position;







			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



			return convertView;
		}
		
		void showDialog(final int tempPos){
			try
			{
				dialogShare.show();

				listViewShare.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {

						String app = appName.get(position);
						
						
						
						String shareBody= "";
						
						String validTill="";
						
						String contact="";
						
						String contact_lable="\nContact:";
						
						Log.i("Is Offer ","Is Offer valid_till "+valid_till.get(tempPos));
						
						if(is_offer.get(tempPos).toString().length()!=0 && is_offer.get(tempPos).equalsIgnoreCase("1"))
						{
							try
							{
								DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
								DateFormat dt2=new SimpleDateFormat("MMM");
								DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

								Date date_con = (Date) dt.parse(valid_till.get(tempPos).toString());

								Date date=dateFormat.parse(valid_till.get(tempPos).toString());
								DateFormat date_format=new SimpleDateFormat("hh:mm a");

								Calendar cal = Calendar.getInstance();
								cal.setTime(date_con);
								int year = cal.get(Calendar.YEAR);
								int month = cal.get(Calendar.MONTH);
								int day = cal.get(Calendar.DAY_OF_MONTH);
								
								validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
								validTill="";
							}
							/*validTill="\nValid Till:"+valid_till.get(tempPos);*/
						}
						else
						{
							validTill="";
						}
						
						if(mob1.get(tempPos).toString().length()>0)
						{
							contact="+"+mob1.get(tempPos).toString();
						} 
						if(mob2.get(tempPos).toString().length()>0)
						{
							contact+=" +"+mob2.get(tempPos).toString();
						}
						if(mob3.get(tempPos).toString().length()>0)
						{
							contact+=" +"+mob3.get(tempPos).toString();
						}
						if(tel1.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						if(tel2.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						if(tel3.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						
						if(contact.length()==0)
						{
							contact_lable="";
						}
						
						
						shareBody=news_title.get(tempPos) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(tempPos) + "\n" + offer_desc.get(tempPos)+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
						
						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

							if(photo_url1.get(tempPos).toString().length()!=0)
							{
								String photo_source=photo_url1.get(tempPos).toString().replaceAll(" ", "%20");

								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPos) + " - " + "Offers")
								.setPicture(PHOTO_PARENT_URL+photo_source)
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
							else
							{
								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPos) + " - " + "Offers")
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
						}
						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
							Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
						}

						else if(!app.equalsIgnoreCase("facebook")){
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setPackage(packageNames.get(position));
							i.setType("text/plain");
							i.putExtra(Intent.EXTRA_SUBJECT, news_title.get(tempPos));
							i.putExtra(Intent.EXTRA_TEXT, shareBody);
							startActivityForResult(i,6);
						}

						dismissDialog();

					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		void dismissDialog(){
			dialogShare.dismiss();
		}



	}

	static class ViewHolder
	{
		TextView newsFeedOffersDate;
		TextView newsFeedOffersMonth;

		TextView newsFeedOffersText;
		TextView newsFeedOffersDesc;

		TextView newsFeedTotalLike;
		TextView newsFeedTotalShare;

		ImageView imgNewsFeedLogo;

		View newsFeedviewOverLay;

		TextView newFeedband;

		ImageView newsIsOffer;

		TextView newsFeedStoreName;

	}

	void likePost(String POST_ID)
	{
		if(network.isNetworkAvailable())
		{
			if(session.isLoggedInCustomer())
			{
				EventTracker.logEvent("MyShoplocal_Liked", false);
				Intent intent=new Intent(context, FavouritePostService.class);
				intent.putExtra("favouritePost",mFav);
				intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("user_id", user_id);
				intent.putExtra("auth_id", auth_id);
				intent.putExtra("post_id", POST_ID);
				context.startService(intent);
				myShoplocalProgress.setVisibility(View.VISIBLE);
			}
			else
			{
				loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
			}
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Discover");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==POST_LIKE)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}


	private void callShareApi(String POST_ID) {
		// TODO Auto-generated method stub
		try{
			EventTracker.logEvent("MyShoplocal_Shared", false);
			Intent intent = new Intent(context, ShareService.class);
			intent.putExtra("shareService", shareServiceResult);
			intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", user_id);
			intent.putExtra("auth_id", auth_id);
			intent.putExtra("post_id", POST_ID);
			intent.putExtra("via", VIA);
			context.startService(intent);
		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	void clearArray()
	{
		NEWS_FEED_ID.clear();
		NEWS_FEED_NAME.clear();
		NEWS_FEED_AREA_ID.clear();

		NEWS_FEED_MOBNO1.clear();
		NEWS_FEED_MOBNO2.clear();
		NEWS_FEED_MOBNO3.clear();


		NEWS_FEED_TELLNO1.clear();
		NEWS_FEED_TELLNO2.clear();
		NEWS_FEED_TELLNO3.clear();

		NEWS_FEED_LATITUDE.clear();
		NEWS_FEED_LONGITUDE.clear();
		NEWS_FEED_DISTANCE.clear();

		NEWS_FEED_POST_ID.clear();
		NEWS_FEED_POST_TYPE.clear();

		NEWS_FEED_POST_TITLE.clear();
		NEWS_FEED_DESCRIPTION.clear();


		NEWS_FEED_image_url1.clear();
		NEWS_FEED_image_url2.clear();
		NEWS_FEED_image_url3.clear();

		NEWS_FEED_thumb_url1.clear();
		NEWS_FEED_thumb_url2.clear();
		NEWS_FEED_thumb_url3.clear();

		NEWS_DATE.clear();
		NEWS_IS_OFFERED.clear();
		NEWS_OFFER_DATE_TIME.clear();

		NEWS_FEED_place_total_like.clear();
		NEWS_FEED_place_total_share.clear();

		page=0;
		post_count=20;


	}




	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
			if(session.isLoggedInCustomer())
			{
				EventTracker.logEvent("MyShoplocal_LoginSuccess", false);
				preLoginLayout.setVisibility(View.GONE);
				listMyShoplocal.setVisibility(View.VISIBLE);
				shopLocalCounterLayout.setVisibility(View.VISIBLE);
				//Login Details
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				user_id=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				auth_id=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

				clearArray();
				loadNewsFeeds();
			}
			else
			{
				preLoginLayout.setVisibility(View.VISIBLE );
				listMyShoplocal.setVisibility(View.GONE);
				shopLocalCounterLayout.setVisibility(View.GONE);
			}
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
		if(requestCode==6)
		{
			VIA="Android";
			callShareApi(TEMP_POST_ID);
		}
		
		try
		{
			uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}

				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					VIA="Android-Facebook";
					callShareApi(TEMP_POST_ID);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}

	@Override
	public void postLikeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		myShoplocalProgress.setVisibility(View.GONE);
		String postLikestatus=resultData.getString("postLikestatus");
		String postLikemsg=resultData.getString("postLikemsg");
		if(postLikestatus.equalsIgnoreCase("true"))
		{
			showToast(postLikemsg);
			try
			{

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			showToast(postLikemsg);
			if(postLikemsg.startsWith(getResources().getString(R.string.invalidAuth)))
			{
				session.logoutCustomer();
			}
			else if(postLikemsg.startsWith(getResources().getString(R.string.connectionTimedOut)))
			{

			}
			else
			{

			}

		}
	}


	@Override
	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String status = resultData.getString("share_status");

		String msg = resultData.getString("share_msg");



		try {

			if(status.equalsIgnoreCase("true")){

				showToast(msg);
			}
			else{

				showToast(msg);
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}


}
