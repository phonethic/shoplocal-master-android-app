package com.phonethics.shoplocal;


import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager.BadTokenException;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.AppEventsConstants;
import com.facebook.AppEventsLogger;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.phonethics.networkcall.MerchantLoginReceiver;
import com.phonethics.networkcall.MerchantLoginReceiver.LoginReceiver;
import com.phonethics.networkcall.MerchantLoginService;
import com.phonethics.networkcall.MerchantRegister;
import com.phonethics.networkcall.MerchantResultReceiver;
import com.phonethics.networkcall.MerchantResultReceiver.MerchantRegisterInterface;
import com.urbanairship.push.PushManager;

public class LoginSignUpCustomer extends SherlockActivity implements OnClickListener,LoginReceiver, MerchantRegisterInterface {

	ActionBar actionBar;
	Activity context;


	Button btnLoginCustomer;
	Button btnSignUpCustomer;

	//To check Internet Connectivity.
	NetworkCheck isnetConnected;


	//Login Recevier
	public MerchantLoginReceiver mLoginRecevier;
	static String API_HEADER;
	static String API_VALUE;
	static String LOGIN_PATH;
	static String LOGIN_URL;

	String USER_ID,AUTH_ID;


	EditText edtCustomerMobileNo,edtCustomerPassword;
	ProgressBar progLoginCustomer;

	//Session Manger Class
	SessionManager session;
	//User Name
	public static final String KEY_USER_NAME_CUSTOMER="mobileno_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";



	//Login back
	ImageView imgloginBackCustomer;

	/*ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;*/


	RelativeLayout merchantLogin;
	TextView forgotPasswordCustomer;
	int signUp=0;

	TextView txtMerchantField;

	Typeface tf;
	TextView txtCustomerPlaceHolder;
	TextView signUpCustomerText;

	Dialog dailog;
	Dialog privacy_dailog;
	Dialog tcdailog;

	Button btnDesclaimer;
	TextView desclaimerText;
	TextView privacy;

	WebView privacyWeb;
	WebView web;

	Button buttonOk;

	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	String cCountryCode;

	TextView countryPicker;

	AlertDialog 	alert			 =	null;
	CountryAdapter	adapter;
	ArrayList<String> countryarr;
	ArrayList<String> countryarrSearch;
	ArrayList<String> countryCode;
	ArrayList<String> countryCodeSearch;
	String	JSON_ARR_STRING;


	TextView loginCustomerTextBoxCondition;
	CheckBox chkBoxAccept;
	Button btnTermDone;
	Button btnTermCancel;
	TextView loginCustomerTextPrivacy;
	ImageView loginCustomer_info;

	boolean isFacebook = false;

	String 			fbUserid="";
	String 			fbAccessToken="";

	ProgressBar		pBarFb;
	private boolean pendingPublishReauthorization = false;
	private static final List<String> PERMISSIONS = Arrays.asList("email");

	RelativeLayout optionLayout;
	RelativeLayout manualSignInLayout;

	Button signUpFb;
	Button signUpCustomer;

	static String PAGER_STATE_PREF="PAGER_STATE_CUSTOMER_PREF";

	HashMap<String, String> user_details;

	//Set Merchant Register Receiver
	public MerchantResultReceiver mRegister;

	static String REGISTER_URL;


	static String REGISTER_PATH;

	boolean isFromTour=false;

	DBUtil dbutil;
	String activeArea="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_sign_up_customer);
		actionBar=getSupportActionBar();
		actionBar.setTitle(getResources().getString(R.string.customer)+" login");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		context=this;
		//Database
		dbutil=new DBUtil(context);


		mRegister=new MerchantResultReceiver(new Handler());
		mRegister.setReceiver(this);


		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		txtMerchantField=(TextView)findViewById(R.id.txtMerchantField);
		txtCustomerPlaceHolder=(TextView)findViewById(R.id.txtCustomerPlaceHolder);
		countryPicker=(TextView)findViewById(R.id.countryPicker);
		signUpCustomerText=(TextView)findViewById(R.id.signUpCustomerText);
		loginCustomer_info=(ImageView)findViewById(R.id.loginCustomer_info);

		optionLayout = (RelativeLayout)findViewById(R.id.optionLayout);
		manualSignInLayout = (RelativeLayout)findViewById(R.id.manualSignInLayout);

		signUpFb = (Button) findViewById(R.id.signUpFb);
		signUpCustomer = (Button) findViewById(R.id.signUpCustomer);

		signUpFb.setTextSize(20);
		signUpCustomer.setTextSize(20);

		signUpFb.setText("Log in with Facebook");
		signUpCustomer.setText("Sign up with shoplocal");

		signUpFb.setTypeface(tf);
		signUpCustomer.setTypeface(tf);
		pBarFb =(ProgressBar)findViewById(R.id.pBarFb);

		REGISTER_URL=getResources().getString(R.string.server_url) + getResources().getString(R.string.user_api);
		REGISTER_PATH=getResources().getString(R.string.register_new);

		//if logged in through facebook

		Bundle b = getIntent().getExtras(); 

		//		Log.d("isFaceBook:", "isFaceBook: " + isFacebook);

		if(b!=null){

			isFacebook =  b.getBoolean("facebookLogin");
			isFromTour =  b.getBoolean("isFromTour",false);
			if(isFacebook){

				// call facebook sdk function

				login_facebook();
			}
			else{

				optionLayout.setVisibility(View.GONE);
				manualSignInLayout.setVisibility(View.VISIBLE);
			}

		}


		try
		{
			JSON_ARR_STRING = getText();
			getCountryCode();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		//Network Check
		isnetConnected=new NetworkCheck(context);

		// Session Manager
		session = new SessionManager(getApplicationContext()); 

		//Initializing Login Service Reciver
		mLoginRecevier=new MerchantLoginReceiver(new Handler());
		mLoginRecevier.setReceiver(this);

		//Defining URL's
		LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api);
		LOGIN_PATH=getResources().getString(R.string.login);



		//API KEY'S
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);


		//Dialog
		dailog=new Dialog(context);
		tcdailog=new Dialog(context);
		privacy_dailog=new Dialog(context);

		privacy_dailog.setContentView(R.layout.privacy_policy);
		privacy_dailog.setTitle("Privacy Policy");
		privacy_dailog.setCancelable(true);
		privacy_dailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		tcdailog.setContentView(R.layout.termsandcondition);
		tcdailog.setTitle("Terms and conditions");
		tcdailog.setCancelable(false);
		tcdailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		web=(WebView)tcdailog.findViewById(R.id.tcWeb);
		privacyWeb=(WebView)privacy_dailog.findViewById(R.id.privacyWeb);
		buttonOk=(Button)privacy_dailog.findViewById(R.id.buttonOk);

		dailog.setContentView(R.layout.disclaimer_dialog);
		dailog.setTitle("Why should I login?");
		dailog.setCancelable(true);
		dailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		btnDesclaimer=(Button)dailog.findViewById(R.id.btnDesclaimer);
		desclaimerText=(TextView)dailog.findViewById(R.id.desclaimerText);
		privacy=(TextView)dailog.findViewById(R.id.privacy);
		privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));

		privacyWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));

		web.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));

		loginCustomerTextBoxCondition=(TextView)findViewById(R.id.loginCustomerTextBoxCondition);
		loginCustomerTextPrivacy=(TextView)findViewById(R.id.loginCustomerTextPrivacy);

		chkBoxAccept=(CheckBox)tcdailog.findViewById(R.id.chkBoxAccept);
		btnTermDone=(Button)tcdailog.findViewById(R.id.btnTermDone);
		btnTermCancel=(Button)tcdailog.findViewById(R.id.btnTermCancel);

		loginCustomerTextBoxCondition.setText(Html.fromHtml("<u>Terms & Conditions</u>"));


		loginCustomerTextPrivacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));

		loginCustomerTextBoxCondition.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tcdailog.show();
			}
		});
		loginCustomerTextPrivacy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				privacy_dailog.show();
			}
		});

		loginCustomer_info.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.mobile_desclaimer))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		});

		btnTermDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chkBoxAccept.setChecked(true);
				if(chkBoxAccept.isChecked())
				{
					tcdailog.dismiss();
				}
				proceedWithoutNext();

			}
		});
		btnTermCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tcdailog.dismiss();
				chkBoxAccept.setChecked(false);

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getString(R.string.terms_and_condition_retry))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Try again",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog2,int id) {

						tcdailog.show();
					}
				})
				.setNegativeButton("Do not register",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog2,int id) {
						// TODO Auto-generated method stub
						dialog2.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		});



		countryPicker.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ParseJsonData(JSON_ARR_STRING);
				alert.show();
			}
		});

		privacy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dailog.dismiss();
				privacy_dailog.show();
			}
		});

		buttonOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				privacy_dailog.dismiss();
			}
		});



		btnDesclaimer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dailog.dismiss();
			}
		});


		btnLoginCustomer=(Button)findViewById(R.id.btnLoginCustomer);
		btnSignUpCustomer=(Button)findViewById(R.id.btnSingupCustomer);
		forgotPasswordCustomer=(TextView)findViewById(R.id.forgotPasswordCustomer);

		btnSignUpCustomer.setText("Proceed");


		edtCustomerMobileNo=(EditText)findViewById(R.id.edtCustomerMobileNo);
		edtCustomerPassword=(EditText)findViewById(R.id.edtCustomerPassword);
		progLoginCustomer=(ProgressBar)findViewById(R.id.progLoginCustomer);

		forgotPasswordCustomer.setTypeface(tf);
		edtCustomerPassword.setTypeface(tf);
		edtCustomerMobileNo.setTypeface(tf);
		signUpCustomerText.setTypeface(tf);

		edtCustomerMobileNo.setTextColor(Color.parseColor("#9c9c9c"));
		edtCustomerPassword.setTextColor(Color.parseColor("#9c9c9c"));
		txtCustomerPlaceHolder.setTextColor(Color.parseColor("#715b57"));
		countryPicker.setTextColor(Color.parseColor("#9c9c9c"));

		edtCustomerMobileNo.setSelection(edtCustomerMobileNo.getText().length());

		txtCustomerPlaceHolder.setText("Customer Login");
		txtCustomerPlaceHolder.setVisibility(View.GONE);

		try
		{
			forgotPasswordCustomer.setText(Html.fromHtml("<u>"+getResources().getString(R.string.forgot_password)+"</u>"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		animate(btnLoginCustomer).setDuration(200);
		btnLoginCustomer.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnLoginCustomer).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnLoginCustomer).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(btnLoginCustomer).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		animate(btnSignUpCustomer).setDuration(200);
		btnSignUpCustomer.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnSignUpCustomer).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnSignUpCustomer).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(btnSignUpCustomer).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		animate(forgotPasswordCustomer).setDuration(200);
		forgotPasswordCustomer.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(forgotPasswordCustomer).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(forgotPasswordCustomer).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(forgotPasswordCustomer).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});


		//Adding Click Listeners
		btnLoginCustomer.setOnClickListener(this);
		btnSignUpCustomer.setOnClickListener(this);
		forgotPasswordCustomer.setOnClickListener(this);

		txtMerchantField.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					/*AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.customerLoginBoxHeaderInfo))
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
						}
					})
					;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();*/
					dailog.show();
				}
			}
		});


		imgloginBackCustomer=(ImageView)findViewById(R.id.imgloginBackCustomer);

		user_details=new HashMap<String, String>();
		user_details=session.getUserDetailsCustomer();
		Log.i("MOBILE PREFS", "MOBILE PREFS CUSTO"+user_details.get(KEY_USER_NAME_CUSTOMER).toString());
		if(user_details.get(KEY_USER_NAME_CUSTOMER).toString().length()!=0)
		{
			//			if(user_details.get(KEY_USER_NAME_CUSTOMER).toString().toString().startsWith("91"))
			//			{
			//				edtCustomerMobileNo.setText(user_details.get(KEY_USER_NAME_CUSTOMER).toString().substring(2));
			//			}
			try
			{
				if(user_details.get(KEY_USER_NAME_CUSTOMER).toString().length()!=0)
				{
					Log.i("MOBILE PREFS", "MOBILE PREFS "+user_details.get(KEY_USER_NAME_CUSTOMER).toString().substring(cCountryCode.length()));
					edtCustomerMobileNo.setText(user_details.get(KEY_USER_NAME_CUSTOMER).toString().substring(cCountryCode.length()));
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();


			}
			//password.setText(user_details.get(KEY_PASSWORD).toString());
		}



		signUpCustomer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EventTracker.logEvent("CSignup_ShoplocalLoginOption", false);
				optionLayout.setVisibility(View.GONE);
				manualSignInLayout.setVisibility(View.VISIBLE);

			}
		});


		signUpFb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(isnetConnected.isNetworkAvailable()){

					EventTracker.logEvent("CSignUp_FBOption", false);
					isFacebook = true;	
					login_facebook();
				}
				else{

					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		if(pBarFb.getVisibility() == View.VISIBLE){

			signUpFb.setClickable(false);
			signUpCustomer.setClickable(false);
		}

		activeArea=dbutil.getActiveAreaID();
		EventTracker.startLocalyticsSession(getApplicationContext());



	}//onCreate Ends here

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());


	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	public void setText(String country, String code){

		try
		{

			countryPicker.setText(code);
			cCountryCode=code.substring(1);
			setPrefernces();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public String getText(){
		String fileString = "";
		try{
			InputStream is = context.getAssets().open("county_code.txt");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			fileString = new String(buffer,"UTF-8");
			Log.d("", "JsonString >> "+fileString);
		}catch(Exception ex){
			ex.printStackTrace();
		}




		return fileString;
	}


	public void ParseJsonData(String jsonString){

		/*countryModels 		= new ArrayList<CountryModel>();*/
		countryarr 			= new ArrayList<String>();
		countryCode 		= new ArrayList<String>();


		try{
			JSONArray jArr = new JSONArray(jsonString);
			for(int i=0;i<jArr.length();i++){
				CountryModel	countryModel = new CountryModel();
				JSONObject jObj = jArr.getJSONObject(i);
				countryModel.setcName(jObj.getString("name"));
				countryarr.add(jObj.getString("name"));
				countryModel.setcCode(jObj.getString("dial_code"));
				countryCode.add(jObj.getString("dial_code").replace(" ", ""));
				countryModel.setcShort(jObj.getString("code"));
				/*countryModels.add(countryModel);*/
			}

			countryarrSearch = countryarr;
			countryCodeSearch = countryCode;
			initDialog();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void initDialog(){

		try
		{
			AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle("Select County Code");
			alertDialog.setCancelable(true);
			View view = context.getLayoutInflater().inflate(R.layout.dialog_list, null);
			final ListView listView = (ListView) view.findViewById(R.id.list_county_code);
			final EditText editSearch = (EditText) view.findViewById(R.id.edit_search);

			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
						long arg3) {
					// TODO Auto-generated method stub
					//setText(countryModels.get(pos).getcName(), countryModels.get(pos).getcCode());
					setText(countryarrSearch.get(pos), countryCodeSearch.get(pos));

					alert.dismiss();

				}
			});
			adapter = new CountryAdapter(context, countryarr,countryCode);
			listView.setAdapter(adapter);
			editSearch.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub

					/*adapter.getFilter().filter(s);
				String newData = editSearch.getText().toString();
				Log.d("", "EditText >> "+newData);
				for(int i=0;i<countryarr.size();i++){
					if(countryarr.get(i).contains(newData)){
						countryarrSearch.add(countryarr.get(i));
					}
				}*/


					ArrayList<String> countryarrSearch_1 = new ArrayList<String>();
					ArrayList<String> countryCodeSearch_1 = new ArrayList<String>();

					int textLength 							 = editSearch.getText().toString().length();

					for(int i=0;i<countryarr.size();i++){
						if(textLength<= countryarr.get(i).length()){
							if(editSearch.getText().toString().equalsIgnoreCase((String)countryarr.get(i).subSequence(0, textLength))){
								countryarrSearch_1.add(countryarr.get(i));
								countryCodeSearch_1.add(countryCode.get(i));						
							}
						}
					}

					countryarrSearch = countryarrSearch_1;
					countryCodeSearch = countryCodeSearch_1;

					adapter = new CountryAdapter(context,  countryarrSearch,countryCodeSearch);
					adapter.notifyDataSetChanged();
					listView.setAdapter(adapter);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			alertDialog.setView(view);
			alert = alertDialog.create();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	void setPrefernces()
	{
		SharedPreferences pref=context.getSharedPreferences("customerCountryCode", 0);
		Editor editor=pref.edit();
		editor.putString("cCountryCode", cCountryCode);
		editor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub


		/*finishTo();*/
		Intent intent=new Intent();
		setResult(3, intent);
		this.finish();

		return true;
	}




	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*finishTo();*/
		Intent intent=new Intent();
		setResult(3, intent);
		this.finish();
	}

	@Override
	public void onReceiveLoginResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String auth_id=resultData.getString("login_authCode");
			String loginstatus=resultData.getString("loginstatus");
			String loginid=resultData.getString("loginid");

			Log.i("Data From Server-------", "Server -----loginstatus "+resultData.getString("loginstatus"));
			Log.i("Data From Server-------", "Server -----loginid "+loginid);
			Log.i("Data From Server-------", "Server -----loginauth_id "+auth_id);

			USER_ID=loginid;
			AUTH_ID=auth_id;
			progLoginCustomer.setVisibility(View.INVISIBLE);
			if(loginstatus.equalsIgnoreCase("true"))
			{

				if(isFacebook){

					session.createLoginSessionCustomer("", "", USER_ID, AUTH_ID,true);

					EventTracker.logEvent("CSignup_LoginSuccessFB", false);


					// registration completes

					AppEventsLogger logger = AppEventsLogger.newLogger(this);

					Bundle parameters = new Bundle();
					parameters.putString("User Type", "Customer");
					parameters.putString("Login Type", "Facebook");

					logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION,parameters);


				}
				else{

					session.createLoginSessionCustomer(cCountryCode+edtCustomerMobileNo.getText().toString(), edtCustomerPassword.getText().toString(), USER_ID, AUTH_ID, false);
				}

				try
				{

					setCustomerTag(USER_ID);
					if(activeArea.length()!=0)
					{
						setAreaTag(activeArea);
					}


				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				/*	Intent intent=new Intent(this, HomeGrid.class);
				startActivity(intent);*/
				Intent intent=new Intent();
				setResult(2, intent);
				finish();

				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

			}
			else 
			{
				Toast.makeText(context,auth_id, Toast.LENGTH_SHORT).show();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setCustomerTag(String userId)
	{
		try
		{
			Log.i("URBAN", "URBAN SET TAG");
			boolean isMisMatch=false;


			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			ArrayList<String>tempIdList=new ArrayList<String>();

			String temp="";
			String replace="";
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			//Traversing Tags to find out if there are any tags existing with own_id

			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains customer_id_
				if(temp.startsWith("customer_id_"))
				{
					//Replace customer_id_ with "" and compare it with ID
					replace=temp.replaceAll("customer_id_", "");
					tempIdList.add(replace);
					Log.i("TAG", "Urban TAG ID "+replace);

					if(!userId.equalsIgnoreCase(replace))
					{
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false && tempIdList.size()==0)
			{
				isMisMatch=true;
			}


			if(isMisMatch==true)
			{

				//Clear customer_id_ from Tag List
				for(int i=0;i<tempIdList.size();i++)
				{
					tags.remove("customer_id_"+tempIdList.get(i));
				}


				tags.add("customer_id_"+userId);

				Log.i("TAG", "Urban TAG AFTER "+tags.toString());

				PushManager.shared().setTags(tags);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void setAreaTag(String area)
	{
		Log.i("URBAN", "URBAN SET TAG");
		boolean isMisMatch=false;

		//Adding Tags

		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
		//Getting All Tags
		Iterator<String> iterator=tags.iterator();

		//Traversing Tags to find out if there are any tags existing with own_id

		while(iterator.hasNext())
		{
			temp=iterator.next();

			Log.i("TAG", "Urban TAG PRINT "+temp);
			//If tag contains own_
			if(temp.startsWith("area_"))
			{
				//Replace area_ with "" and compare it with ID
				replace=temp.replaceAll("area_", "");
				tempIdList.add(replace);
				Log.i("TAG", "Urban TAG ID "+replace);

				if(!area.equalsIgnoreCase(replace))
				{
					isMisMatch=true;
					break;
				}
			}
		}

		if(isMisMatch==false && tempIdList.size()==0)
		{
			isMisMatch=true;
		}


		if(isMisMatch==true)
		{

			//Clear area_ from Tag List
			for(int i=0;i<tempIdList.size();i++)
			{
				tags.remove("area_"+tempIdList.get(i));
			}


			tags.add("area_"+area);

			Log.i("TAG", "Urban TAG AFTER "+tags.toString());

			PushManager.shared().setTags(tags);
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==btnSignUpCustomer.getId())
		{

			if(isnetConnected.isNetworkAvailable())
			{
				proceed();
			}
			else
			{
				Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_LONG).show();
			}


		}
		if(v.getId()==forgotPasswordCustomer.getId())
		{

			//			Intent intent=new Intent(this, ShopLocalCustomerSignUp.class);
			//			intent.putExtra("ForgetPassword", 1);
			//			startActivity(intent);
			//			finish();
			//			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(v.getId()==btnLoginCustomer.getId())
		{



			if(edtCustomerMobileNo.getText().toString().length()==0)
			{
				Toast.makeText(context, getResources().getString(R.string.mobileAlert), Toast.LENGTH_SHORT).show();
			}
			else if(edtCustomerMobileNo.getText().toString().length()<10)
			{
				Toast.makeText(context, getResources().getString(R.string.mobileLengthAlert), Toast.LENGTH_SHORT).show();
			}
			else if(edtCustomerPassword.getText().toString().length()==0)
			{
				Toast.makeText(context, "Please enter password", Toast.LENGTH_SHORT).show();
			}
			else if(edtCustomerPassword.getText().toString().length()<6)
			{
				Toast.makeText(context, "Password must be atleast 6 character's long", Toast.LENGTH_SHORT).show();
			}
			else
			{
				loginCustomer();
			}




		}
	}//end of onclick Listener


	void proceed()
	{
		try
		{

			if(edtCustomerMobileNo.getText().toString().length()==0)
			{
				Toast.makeText(context, getResources().getString(R.string.mobileAlert), Toast.LENGTH_SHORT).show();
			}
			else if(edtCustomerMobileNo.getText().toString().length()<10)
			{
				Toast.makeText(context, getResources().getString(R.string.mobileLengthAlert), Toast.LENGTH_SHORT).show();
			}
			else
			{
				if(chkBoxAccept.isChecked())
				{
					user_details=session.getUserDetailsCustomer();

					//					String user_name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();
					//					if(user_name.length()>0)
					//					{
					//						user_name=user_name.substring(cCountryCode.length());
					//					}
					//					Log.i("Mobile NO ", "Mobile No "+user_name+" == "+edtCustomerMobileNo.getText().toString());
					//					if(edtCustomerMobileNo.getText().toString().equalsIgnoreCase(user_name))
					//					{
					//
					//					}
					//					else
					//					{
					//						clearPageState();
					//					}

					//					EventTracker.logEvent("CSignUp_Proceed", false);

					//					Intent intent=new Intent(this, ShopLocalCustomerSignUp.class);
					//					intent.putExtra("countryCode", cCountryCode);
					//					intent.putExtra("mobileno", edtCustomerMobileNo.getText().toString());
					//					startActivityForResult(intent, 2);
					//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					callRegistrationApi();
				}
				else
				{
					tcdailog.show();
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}//end

	void proceedWithoutNext()
	{
		try
		{

			if(edtCustomerMobileNo.getText().toString().length()==0)
			{
				//				Toast.makeText(context, "Please enter Mobile no.", Toast.LENGTH_SHORT).show();
			}
			else if(edtCustomerMobileNo.getText().toString().length()<10)
			{
				//				Toast.makeText(context, "Please enter correct Mobile no.", Toast.LENGTH_SHORT).show();
			}
			else
			{
				if(chkBoxAccept.isChecked())
				{
					user_details=session.getUserDetailsCustomer();
					String user_name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();
					//					if(user_name.length()>0)
					//					{
					//						user_name=user_name.substring(cCountryCode.length());
					//					}
					//					Log.i("Mobile NO ", "Mobile No "+user_name+" == "+edtCustomerMobileNo.getText().toString());
					//					if(edtCustomerMobileNo.getText().toString().equalsIgnoreCase(user_name))
					//					{
					//
					//					}
					//					else
					//					{
					//						clearPageState();
					//					}

					//					EventTracker.logEvent("CSignUp_Proceed", false);
					//					Intent intent=new Intent(this, ShopLocalCustomerSignUp.class);
					//					intent.putExtra("countryCode", cCountryCode);
					//					intent.putExtra("mobileno", edtCustomerMobileNo.getText().toString());
					//					startActivityForResult(intent, 2);
					//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					callRegistrationApi();
				}

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	} //ends

	void clearPageState()
	{
		try
		{
			SharedPreferences prefs=context.getSharedPreferences(PAGER_STATE_PREF, MODE_PRIVATE);
			Editor editor=prefs.edit();
			editor.clear();
			editor.clear();
			editor.commit();

			Log.i("CLeARED","CLEARED");

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void loginCustomer()
	{

		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, MerchantLoginService.class);
			intent.putExtra("merchantLogin", mLoginRecevier);
			intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);

			if(isFacebook){
				EventTracker.logEvent("Tour_LoginSuccessFB", false);
				intent.putExtra("facebook_user_id", fbUserid);
				intent.putExtra("facebook_access_token", fbAccessToken);
				intent.putExtra("fromFb",true);
			}
			else{
				EventTracker.logEvent("Tour_SignUpSuccess", false);
				intent.putExtra("contact_no", cCountryCode+edtCustomerMobileNo.getText().toString());
				intent.putExtra("password", edtCustomerPassword.getText().toString());

			}
			intent.putExtra("isCustomer",true);
			context.startService(intent);
			pBarFb.setVisibility(View.VISIBLE);

		}
		else
		{
			Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();
		}
	}

	void getCountryCode()
	{
		SharedPreferences pref=context.getSharedPreferences("customerCountryCode",0);
		cCountryCode=pref.getString("cCountryCode", "91");
		countryPicker.setText("+"+cCountryCode);
		Log.i("Countrycode found","Countrycode found "+cCountryCode);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			if(requestCode==2)
			{
				if(session.isLoggedInCustomer())
				{
					finish();
					overridePendingTransition(0,R.anim.shrink_fade_out_center);

				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		try
		{
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void login_facebook(){


		try
		{
			pBarFb.setVisibility(View.VISIBLE);
			//showToast("Please Wait");

			//List<String> permissions = session.getPermissions();


			Session.openActiveSession(context, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();

						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							//							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, PERMISSIONS);
							//							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						/*Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {*/

						Request request = Request.newMeRequest(session,
								new Request.GraphUserCallback() {
							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									try{
										//Log.d("", "Fb responcse == "+response.toString());
										//Log.d("", "FbUser >> "+ user.toString());

										fbUserid=user.getId();
										fbAccessToken=session.getAccessToken();
										Log.i("User Id", "FbName : "+fbAccessToken);
										Log.i("User Id ", "FbId : "+fbUserid);

										pBarFb.setVisibility(View.INVISIBLE);


										loginCustomer();

									}catch(Exception ex){
										ex.printStackTrace();
									}


								}

								else{
									EventTracker.logEvent("CSignup_LoginFailedFB", false);
									pBarFb.setVisibility(View.INVISIBLE);
									//									Log.i("ELSE", "ELSE");
									//									showToast("wrong");
								}
							}


						});
						request.executeAsync();

						//session.getAccessToken();
						//Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();
					}

					if(session.isClosed()){
						pBarFb.setVisibility(View.INVISIBLE);
						EventTracker.logEvent("CSignup_LoginFailedFB", false);
						//Toast.makeText(context, "Closed", 0).show();

						try {

							if ((exception instanceof FacebookOperationCanceledException ||
									exception instanceof FacebookAuthorizationException)) {
								new AlertDialog.Builder(LoginSignUpCustomer.this)
								.setTitle("Login Failed")
								.setMessage(exception.getMessage().toString())
								.setPositiveButton("OK", null)
								.show();

							}

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

						Log.d("EXCEPTION","EXCEPTION " + exception);

					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	void callRegistrationApi()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			if(!pBarFb.isShown())
			{
				EventTracker.logEvent("CSignUp_Proceed", false);

				//Calling Register Api to Register Merchant.
				Intent intent=new Intent(context, MerchantRegister.class);
				intent.putExtra("merchantRegister", mRegister);
				intent.putExtra("URL", REGISTER_URL+REGISTER_PATH);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("mobile_no", cCountryCode+edtCustomerMobileNo.getText().toString());
				context.startService(intent);
				pBarFb.setVisibility(View.VISIBLE);
				btnSignUpCustomer.setEnabled(false);
			}
			else
			{
				/*showToast("Wait");*/
			}
		}else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public void onReceiverMerchantRegister(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			btnSignUpCustomer.setEnabled(true);

			pBarFb.setVisibility(View.GONE);
			String passstatus=resultData.getString("passstatus");
			String passmsg=resultData.getString("passmsg");
			String error_code=resultData.getString("error_code");

			if(error_code.equalsIgnoreCase("-223")) //New user
			{
				EventTracker.logEvent("CSignUp_New", false);
			}
			else if(error_code.equalsIgnoreCase("-202")) //Returning user
			{
				EventTracker.logEvent("CSignUp_Return", false);
			}

			if(passstatus.equalsIgnoreCase("true")){

				Intent intent=new Intent(this, SignupCustomer.class);
				intent.putExtra("server_message", passmsg);
				intent.putExtra("mobileno", cCountryCode+edtCustomerMobileNo.getText().toString());
				intent.putExtra("isFromTour", isFromTour);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else{

				showToast(passmsg);
			}
		}catch(Exception  ex)
		{
			ex.printStackTrace();
		}
	}
}
