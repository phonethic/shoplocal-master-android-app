package com.phonethics.shoplocal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.DatePreferencesClass;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;
import com.phonethics.networkcall.SearchResultReceiver;
import com.phonethics.networkcall.SearchResultReceiver.SearchResult;
import com.phonethics.networkcall.SearchService;
import com.squareup.picasso.Picasso;

public class DefaultSearchList extends SherlockActivity implements Receiver,SearchResult
{

	ActionBar actionbar;
	Activity context;

	String STORE_URL;
	String LOGIN_URL;
	String API_HEADER;
	String API_VALUE;

	String GET_SEARCH_URL;
	String LOGIN_PATH;
	String LOGOUT_PATH;

	LocationResultReceiver mReceiver;
	SearchResultReceiver searchReceiver;

	RelativeLayout relDefaultSearchProgress,relDefault;

	boolean isCheckLocation=false;
	boolean isLogout=false;
	boolean isCheckOffers=false;

	String USER_ID,AUTH_ID;

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> PLACE_PARENT=new ArrayList<String>();
	ArrayList<String> SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> BUILDING=new ArrayList<String>();
	ArrayList<String> STREET=new ArrayList<String>();
	ArrayList<String> LANDMARK=new ArrayList<String>();
	ArrayList<String> AREA_ID=new ArrayList<String>();
	ArrayList<String> AREA=new ArrayList<String>();
	ArrayList<String> CITY=new ArrayList<String>();
	ArrayList<String> STATE=new ArrayList<String>();
	ArrayList<String> COUNTRY=new ArrayList<String>();
	ArrayList<String> MOBILE_NO=new ArrayList<String>();


	ArrayList<String> MOBILE_NO2=new ArrayList<String>();
	ArrayList<String> MOBILE_NO3=new ArrayList<String>();

	ArrayList<String> TEL_NO=new ArrayList<String>();
	ArrayList<String> TEL_NO2=new ArrayList<String>();
	ArrayList<String> TEL_NO3=new ArrayList<String>();


	ArrayList<String> PHOTO=new ArrayList<String>();
	ArrayList<String> DISTANCE=new ArrayList<String>();
	ArrayList<String> HAS_OFFER=new ArrayList<String>();
	ArrayList<String> OFFER=new ArrayList<String>();
	ArrayList<String> DESCRIPTION=new ArrayList<String>();
	ArrayList<String> TOTAL_LIKE=new ArrayList<String>();
	ArrayList<String> VERIFIED=new ArrayList<String>();


	//Search
	ArrayList<String> Search_ID=new ArrayList<String>();
	ArrayList<String> Search_SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> Search_BUILDING=new ArrayList<String>();
	ArrayList<String> Search_STREET=new ArrayList<String>();
	ArrayList<String> Search_LANDMARK=new ArrayList<String>();
	ArrayList<String> Search_AREA=new ArrayList<String>();
	ArrayList<String> Search_CITY=new ArrayList<String>();
	ArrayList<String> Search_STATE=new ArrayList<String>();
	ArrayList<String> Search_COUNTRY=new ArrayList<String>();


	ArrayList<String> Search_MOBILE_NO=new ArrayList<String>();
	ArrayList<String> Search_MOBILE_NO2=new ArrayList<String>();
	ArrayList<String> Search_MOBILE_NO3=new ArrayList<String>();

	ArrayList<String> Search_TEL_NO=new ArrayList<String>();
	ArrayList<String> Search_TEL_NO2=new ArrayList<String>();
	ArrayList<String> Search_TEL_NO3=new ArrayList<String>();


	ArrayList<String> Search_PHOTO=new ArrayList<String>();
	ArrayList<String> Search_DISTANCE=new ArrayList<String>();
	ArrayList<String> Search_DESCRIPTION=new ArrayList<String>();

	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	String TOTAL_SEARCH_PAGES="";
	String TOTAL_SEARCH_RECORDS="";

	static PullToRefreshListView list;
	String latitued="";
	String longitude="";

	TextView txtMsg;

	NetworkCheck isnetConnected;

	boolean isSplitLogin=false;

	//Session Manger
	SessionManager session;

	int post_count=20;
	static int page=0;

	int search_post_count=20;
	int search_page=1;

	DBUtil dbutil;
	Button defLoadMore;

	//Row Counter
	TextView txtCounter;
	long rowCount;

	 String offer="";

	Button btnSearchLoadMore;

	ProgressBar searchListProg;

	TextView txtSearchCounter;
	TextView txtCounterBack;

	static String PHOTO_PARENT_URL;
	boolean isRefreshing=false;



	String key="area_id";
	String value;

	int dropdown_position;
	boolean isLocationFound=false;


	ArrayList<Item> items=new ArrayList<Item>();
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	ExpandableListView drawerList;



	private static long back_pressed;

	String drawer_item="";
	DrawerExpandibleAdapter drawerAdapter;
	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();
	DrawerClass drawerClass;
	SearchListAdapter adapter;

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	String MARKET_DATE_TIME_STAMP="MARKET_DATE_TIME_STAMP";
	String KEY="MARKET_DATE_TIME";

	DatePreferencesClass datePref;
	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;
	boolean opeDrawer=false;
	LocalNotification localNotification;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(R.style.Theme_City_custom);

		setContentView(R.layout.activity_default_search_list);

		context=this;
		localNotification=new LocalNotification(getApplicationContext());

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		datePref=new DatePreferencesClass(context, MARKET_DATE_TIME_STAMP, KEY);;

		drawerClass=new DrawerClass(context);

		moreData=drawerClass.isShowMore();

		//Database
		dbutil=new DBUtil(context);

		//Session Manager
		session=new SessionManager(getApplicationContext());
		isSplitLogin=session.isLoggedIn();

		try
		{

			Bundle b=getIntent().getExtras();

			if(b!=null)
			{
				opeDrawer=b.getBoolean("opeDrawer",false);
			}

			moreData=drawerClass.isShowMore();
			value=dbutil.getActiveAreaID();
			getLocationFromPrefs();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		actionbar=getSupportActionBar();
		actionbar.setTitle(getResources().getString(R.string.itemAllStores));
		//		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();
		actionbar.setHomeButtonEnabled(true);

		actionbar.setIcon(R.drawable.ic_drawer);
		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);

		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});

		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionbar.setTitle(getString(R.string.itemAllStores));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionbar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		//Creating Drawer
		createDrawer();

		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		/*STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.dummy_search);*/
		GET_SEARCH_URL=getResources().getString(R.string.searchapi);

		LOGIN_PATH=getResources().getString(R.string.login);
		LOGOUT_PATH=getResources().getString(R.string.logout);

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.loginapi);

		//Api Key

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		isnetConnected=new NetworkCheck(context);

		relDefault=(RelativeLayout)findViewById(R.id.relDefault);

		searchListProg=(ProgressBar)findViewById(R.id.searchListProg);
		txtSearchCounter=(TextView)findViewById(R.id.txtSearchCounter);
		//		txtCounterBack=(TextView)findViewById(R.id.txtCounterBack);


		relDefaultSearchProgress=(RelativeLayout)findViewById(R.id.relDefaultSearchProgress);

		//Button Load more
		defLoadMore=(Button)findViewById(R.id.defLoadMore);
		defLoadMore.setVisibility(View.GONE);

		txtCounter=(TextView)findViewById(R.id.txtCounter);


		mReceiver=new LocationResultReceiver(new Handler());
		mReceiver.setReceiver(this);

		searchReceiver=new SearchResultReceiver(new Handler());
		searchReceiver.setReceiver(this);


		list=(PullToRefreshListView)findViewById(R.id.dafault_listSearchResult);

		//Animating listview.
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		list.setLayoutAnimation(controller);

		adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher,ID,SOTRE_NAME, DISTANCE, PHOTO,MOBILE_NO,HAS_OFFER,OFFER,DESCRIPTION,TOTAL_LIKE);
		list.setAdapter(adapter);

		//Pulltorefresh

		list.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				isRefreshing=true;
				try
				{
					//load fresh entries
					if(isnetConnected.isNetworkAvailable())
					{
						page=0;
						loadEntries();
					}
					else
					{
						MyToast.showToast(context, getResources().getString(R.string.noInternetConnection),1);
						list.onRefreshComplete();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		//Last Item visible
		list.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {

				if(isnetConnected.isNetworkAvailable())
				{

					if(ID.size()==Integer.parseInt(TOTAL_RECORDS))
					{
						defLoadMore.setVisibility(View.GONE);
					}
					else if(page<Integer.parseInt(TOTAL_PAGES))
					{

						if(!relDefaultSearchProgress.isShown())
						{
							Log.i("Page", "Page "+page);

							if(!relDefaultSearchProgress.isShown())
							{
								loadEntries();
							}
							else
							{
								showToast(getResources().getString(R.string.loadingAlert));
							}
							Log.i("ID SIZE","ID SIZE  "+ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						}
						else
						{
							showToast(getResources().getString(R.string.loadingAlert));
						}
					}
					else
					{
						Log.i("ID SIZE","ID SIZE ELSE "+ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						defLoadMore.setVisibility(View.GONE);
					}
				}
				else
				{
					list.onRefreshComplete();
				}

			}
		});

		//Load More entries
		defLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isnetConnected.isNetworkAvailable())
				{
					if(ID.size()==Integer.parseInt(TOTAL_RECORDS))
					{
						defLoadMore.setVisibility(View.GONE);
					}
					else if(page<Integer.parseInt(TOTAL_PAGES))
					{

						if(!relDefaultSearchProgress.isShown())
						{
							page++;
							Log.i("Page", "Page "+page);
							loadEntries();
							Log.i("ID SIZE","ID SIZE  "+ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						}
						else
						{
							showToast(getResources().getString(R.string.loadingAlert));
						}
					}
					else
					{
						Log.i("ID SIZE","ID SIZE ELSE "+ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						defLoadMore.setVisibility(View.GONE);
					}
				}
			}
		});

		try
		{
			clearArray();
			changeData();
			if(opeDrawer)
			{
				mDrawerLayout.openDrawer(Gravity.LEFT);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		EventTracker.startLocalyticsSession(getApplicationContext());

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}

		/** ANirudh- custom share dialog */
		dialogShare = new Dialog(DefaultSearchList.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));


	}//onCreate ends here!


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


	void createDrawer()
	{
		try
		{
			drawerMenu.clear();
			drawerClass.setShowMore(moreData);

			drawerMenu= drawerClass.getDrawerAdapter();
			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());
			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			navigate();
			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			drawer_item="";
			mDrawerLayout.closeDrawer(Gravity.LEFT);

		}
		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			showDialogTab();

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			if(session.isLoggedIn())
			{
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}

	void showDialogTab(){

		dialogShare.show();

		listViewShare.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(DefaultSearchList.this)
					.setLink("http://shoplocal.co.in")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}

				dismissDialog();

			}
		});
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}




	void locationService()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//calling location service.
			Intent intent=new Intent(context, LocationIntentService.class);
			intent.putExtra("receiverTag", mReceiver);
			context.startService(intent);
			relDefaultSearchProgress.setVisibility(View.VISIBLE);
		}else
		{
			MyToast.showToast(context,(getResources().getString(R.string.noInternetConnection)),1);
			list.onRefreshComplete();
		}
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();

	}


	void loadLocalEntries()
	{
		try
		{
			ArrayList<String> TempID=new ArrayList<String>();
			ArrayList<String> TempPlaceParent=new ArrayList<String>();
			ArrayList<String> TempSOTRE_NAME=new ArrayList<String>();	
			ArrayList<String> TempAREA_ID=new ArrayList<String>();
			ArrayList<String> TempAREA=new ArrayList<String>();
			ArrayList<String> TempCITY=new ArrayList<String>();
			ArrayList<String> TempMOBILE_NO=new ArrayList<String>();
			ArrayList<String> TempMOBILE_NO2=new ArrayList<String>();
			ArrayList<String> TempMOBILE_NO3=new ArrayList<String>();

			ArrayList<String> TempTEL_NO=new ArrayList<String>();
			ArrayList<String> TempTEL_NO2=new ArrayList<String>();
			ArrayList<String> TempTEL_NO3=new ArrayList<String>();
			ArrayList<String> TempPHOTO=new ArrayList<String>();
			ArrayList<String> TempDISTANCE=new ArrayList<String>();
			ArrayList<String> TempHasOffers=new ArrayList<String>();
			ArrayList<String> TempOffers=new ArrayList<String>();
			ArrayList<String> TempDescription=new ArrayList<String>();
			ArrayList<String> TempTotalLike=new ArrayList<String>();

			//clear temporary array 
			TempID.clear();
			TempPlaceParent.clear();
			TempSOTRE_NAME.clear();
			TempAREA_ID.clear();
			TempAREA.clear();
			TempCITY.clear();

			TempMOBILE_NO.clear();
			TempMOBILE_NO2.clear();
			TempMOBILE_NO3.clear();

			TempTEL_NO.clear();
			TempTEL_NO2.clear();
			TempTEL_NO3.clear();

			TempPHOTO.clear();
			TempDISTANCE.clear();
			TempHasOffers.clear();
			TempOffers.clear();
			TempDescription.clear();
			TempTotalLike.clear();



			//Pull data from database.
			TempID=dbutil.getAllGeoStoreId(Integer.parseInt(value));
			TempPlaceParent=dbutil.getAllGeoStoreParentId(Integer.parseInt(value));
			TempSOTRE_NAME=dbutil.getAllGeoStoreName(Integer.parseInt(value));
			TempAREA=dbutil.getAllGeoAreas(Integer.parseInt(value));
			TempAREA_ID=dbutil.getAllGeoAreaId(Integer.parseInt(value));
			TempCITY=dbutil.getAllGeoCity(Integer.parseInt(value));
			TempPHOTO=dbutil.getAllGeoPhoto(Integer.parseInt(value));

			TempMOBILE_NO=dbutil.getGeoAllMobileNos(Integer.parseInt(value));
			TempMOBILE_NO2=dbutil.getGeoAllMobileNos2(Integer.parseInt(value));
			TempMOBILE_NO3=dbutil.getGeoAllMobileNos3(Integer.parseInt(value));

			TempTEL_NO=dbutil.getGeoAllTELNos(Integer.parseInt(value));
			TempTEL_NO2=dbutil.getGeoAllTELNos2(Integer.parseInt(value));
			TempTEL_NO3=dbutil.getGeoAllTELNos3(Integer.parseInt(value));


			TempDISTANCE=dbutil.getAllDistances(Integer.parseInt(value));
			TempHasOffers=dbutil.getAllOffersFlag(Integer.parseInt(value));
			TempOffers=dbutil.getAllStoreOffers(Integer.parseInt(value));
			TempDescription=dbutil.getAllGeoStoreDesc(Integer.parseInt(value));
			TempTotalLike=dbutil.getAllTotalLikes(Integer.parseInt(value));


			for(int index=0;index<TempOffers.size();index++)
			{
				Log.i("OFFER TEST","OFFER TEST STORE NAME "+TempSOTRE_NAME.get(index)+" HAS OFFER  "+TempHasOffers.get(index)+" OFFER TITLE "+TempOffers.get(index));
			}

			Log.i("CHECK SIZE", "CHECK SIZE : "+TempID.size()+" "+TempPlaceParent.size()+" "+TempSOTRE_NAME.size()+" "+TempAREA.size()+" "+TempDISTANCE.size());
			//Getting Page and record status from database.
			SharedPreferences pref=context.getSharedPreferences("Page", 0);

			page=Integer.parseInt(pref.getString(value+"page", "1"));
			TOTAL_PAGES=pref.getString(value+"TOTAL_PAGES", "1");
			TOTAL_RECORDS=pref.getString(value+"TOTAL_RECORDS", "1");
			Log.i("Cached", "Cached Data current page "+page+" Total Pages "+TOTAL_PAGES+" TOTAL_RECORDS "+TOTAL_RECORDS);



			for(int index=0;index<TempID.size();index++)
			{
				ID.add(TempID.get(index));
				PLACE_PARENT.add(TempPlaceParent.get(index));
				SOTRE_NAME.add(TempSOTRE_NAME.get(index));
				AREA.add(TempAREA.get(index));
				AREA_ID.add(TempAREA_ID.get(index));
				CITY.add(TempCITY.get(index));
				MOBILE_NO.add(TempMOBILE_NO.get(index));
				MOBILE_NO2.add(TempMOBILE_NO2.get(index));
				MOBILE_NO3.add(TempMOBILE_NO3.get(index));
				TEL_NO.add(TempTEL_NO.get(index));
				TEL_NO2.add(TempTEL_NO2.get(index));
				TEL_NO3.add(TempTEL_NO3.get(index));
				PHOTO.add(TempPHOTO.get(index));
				DISTANCE.add(TempDISTANCE.get(index));
				HAS_OFFER.add(TempHasOffers.get(index));
				OFFER.add(TempOffers.get(index));
				DESCRIPTION.add(TempDescription.get(index));
				TOTAL_LIKE.add(TempTotalLike.get(index));


			}
			for(int index=0;index<AREA.size();index++)
			{
				Log.i("", "--------------------------------------------------");
				Log.i("Area", "Local "+SOTRE_NAME.get(index)+" - "+AREA.get(index)+" "+AREA.size());
			}

			txtCounter.setText(ID.size()+"/"+TOTAL_RECORDS);
			adapter.notifyDataSetChanged();

			if(page<Integer.parseInt(TOTAL_PAGES))
			{
				if(isnetConnected.isNetworkAvailable())
				{
					if(Integer.parseInt(TOTAL_RECORDS)==ID.size())
					{
						defLoadMore.setVisibility(View.GONE);
					}
				}

			}
			else
			{
				defLoadMore.setVisibility(View.GONE);
			}

			//List Refresh completed
			list.onRefreshComplete();
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					if(!isRefreshing)
					{
						EventTracker.logEvent("MarketPlace_Details", false);
						Intent intent=new Intent(context, StoreDetailsActivity.class);
						intent.putExtra("STORE_ID", ID.get(position-1));
						intent.putExtra("STORE", SOTRE_NAME.get(position-1));
						intent.putExtra("PHOTO", PHOTO.get(position-1));
						intent.putExtra("AREA", AREA.get(position-1));
						intent.putExtra("CITY", CITY.get(position-1));
						intent.putExtra("MOBILE_NO", MOBILE_NO.get(position-1));
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			});


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Search Entries 
	void loadEntries()
	{

		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, SearchService.class);
			intent.putExtra("searchapi",searchReceiver);

			intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("search_post_count",post_count);
			intent.putExtra("search_page",page+1);
			if(!isLocationFound)
			{
				latitued="0";
				longitude="0";
				intent.putExtra("isCheckLocation", false);
			}
			else
			{
				intent.putExtra("isCheckLocation", true);
			}

			intent.putExtra("latitude", latitued);
			intent.putExtra("longitude", longitude);
			intent.putExtra("offer", offer);
			intent.putExtra("isCheckOffers", isCheckOffers);
			intent.putExtra("key", key);
			intent.putExtra("value", value);
			context.startService(intent);

			relDefaultSearchProgress.setVisibility(ViewGroup.VISIBLE);
		}else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}

	}
	/******************************************************** SEARCH RESULT SECTION *********************************/
	//Update the ui after getting searched result.
	@Override
	public void onReciverSearchResult(int resultCode, Bundle resultData) {


		String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");
		Log.i("Status ", "Status "+SEARCH_STATUS);
		relDefaultSearchProgress.setVisibility(ViewGroup.GONE);
		if(SEARCH_STATUS.equalsIgnoreCase("true"))
		{
			try
			{
				list.setVisibility(View.VISIBLE);

				ArrayList<String> TempID=new ArrayList<String>();
				ArrayList<String> TempPlaceParent=new ArrayList<String>();
				ArrayList<String> TempSOTRE_NAME=new ArrayList<String>();	
				ArrayList<String> TempBUILDING=new ArrayList<String>();
				ArrayList<String> TempSTREET=new ArrayList<String>();
				ArrayList<String> TempLANDMARK=new ArrayList<String>();
				ArrayList<String> TempAREA_ID=new ArrayList<String>();
				ArrayList<String> TempAREA=new ArrayList<String>();
				ArrayList<String> TempCITY=new ArrayList<String>();
				ArrayList<String> TempSTATE=new ArrayList<String>();
				ArrayList<String> TempCOUNTRY=new ArrayList<String>();

				ArrayList<String> TempMOBILE_NO=new ArrayList<String>();
				ArrayList<String> TempMOBILE_NO2=new ArrayList<String>();
				ArrayList<String> TempMOBILE_NO3=new ArrayList<String>();

				ArrayList<String> TempTEL_NO=new ArrayList<String>();
				ArrayList<String> TempTEL_NO2=new ArrayList<String>();
				ArrayList<String> TempTEL_NO3=new ArrayList<String>();


				ArrayList<String> TempPHOTO=new ArrayList<String>();
				ArrayList<String> TempDISTANCE=new ArrayList<String>();
				ArrayList<String> TempHasOffers=new ArrayList<String>();
				ArrayList<String> TempOffers=new ArrayList<String>();
				ArrayList<String> TempDescription=new ArrayList<String>();
				ArrayList<String> TempTotalLike=new ArrayList<String>();
				ArrayList<String> TempVERIFIED=new ArrayList<String>();

				TempID.clear();
				TempPlaceParent.clear();
				TempSOTRE_NAME.clear();
				TempBUILDING.clear();
				TempSTREET.clear();
				TempLANDMARK.clear();
				TempAREA_ID.clear();
				TempAREA.clear();
				TempCITY.clear();
				TempMOBILE_NO.clear();
				TempPHOTO.clear();
				TempDISTANCE.clear();
				TempHasOffers.clear();
				TempOffers.clear();
				TempDescription.clear();
				TempTotalLike.clear();
				TempVERIFIED.clear();


				TempID=resultData.getStringArrayList("ID");
				TempPlaceParent=resultData.getStringArrayList("PLACE_PARENT");
				TempSOTRE_NAME=resultData.getStringArrayList("SOTRE_NAME");
				TempBUILDING=resultData.getStringArrayList("BUILDING");
				TempSTREET=resultData.getStringArrayList("STREET");
				TempLANDMARK=resultData.getStringArrayList("LANDMARK");
				TempAREA_ID=resultData.getStringArrayList("AREA_ID");
				TempAREA=resultData.getStringArrayList("AREA");
				TempCITY=resultData.getStringArrayList("CITY");

				TempMOBILE_NO=resultData.getStringArrayList("MOBILE_NO");
				TempMOBILE_NO2=resultData.getStringArrayList("MOBILE_NO2");
				TempMOBILE_NO3=resultData.getStringArrayList("MOBILE_NO3");

				TempTEL_NO=resultData.getStringArrayList("TEL_NO");
				TempTEL_NO2=resultData.getStringArrayList("TEL_NO2");
				TempTEL_NO3=resultData.getStringArrayList("TEL_NO3");


				TempPHOTO=resultData.getStringArrayList("PHOTO");
				TempDISTANCE=resultData.getStringArrayList("DISTANCE");
				TempHasOffers=resultData.getStringArrayList("HAS_OFFERS");
				TempOffers=resultData.getStringArrayList("OFFERS");
				TempDescription=resultData.getStringArrayList("DESCRIPTION");
				TempTotalLike=resultData.getStringArrayList("TOTAL_LIKE");
				TempVERIFIED=resultData.getStringArrayList("VERIFIED");

				//While refreshing clear all arrays.
				if(isRefreshing)
				{
					clearArray();
					isRefreshing=false;
					dbutil.deleteGeoText(Integer.parseInt(value));
				}

				for(int index=0;index<TempID.size();index++)
				{
					ID.add(TempID.get(index));
					PLACE_PARENT.add(TempPlaceParent.get(index));
					SOTRE_NAME.add(TempSOTRE_NAME.get(index));
					BUILDING.add(TempBUILDING.get(index));
					STREET.add(TempSTREET.get(index));
					LANDMARK.add(TempLANDMARK.get(index));
					AREA_ID.add(TempAREA_ID.get(index));
					AREA.add(TempAREA.get(index));
					CITY.add(TempCITY.get(index));

					MOBILE_NO.add(TempMOBILE_NO.get(index));
					MOBILE_NO2.add(TempMOBILE_NO2.get(index));
					MOBILE_NO3.add(TempMOBILE_NO3.get(index));

					TEL_NO.add(TempTEL_NO.get(index));
					TEL_NO2.add(TempTEL_NO2.get(index));
					TEL_NO3.add(TempTEL_NO3.get(index));



					PHOTO.add(TempPHOTO.get(index));
					DISTANCE.add(TempDISTANCE.get(index));
					HAS_OFFER.add(TempHasOffers.get(index));
					OFFER.add(TempOffers.get(index));
					TOTAL_LIKE.add(TempTotalLike.get(index));
					VERIFIED.add(TempVERIFIED.get(index));
					DESCRIPTION.add(TempDescription.get(index));
				}

				TOTAL_PAGES=resultData.getString("TOTAL_PAGES");
				TOTAL_RECORDS=resultData.getString("TOTAL_RECORDS");
				page++;

				//Inserting Records into database.
				for(int i=0;i<TempID.size();i++)
				{
					dbutil.open();

					Log.i("Page", "Page Array Size"+ID.size()+" PAGE After inserting "+page);

					//insert into GEO-TEXT table.
					dbutil.create_geotext(Integer.parseInt(TempID.get(i)),Integer.parseInt(TempPlaceParent.get(i)),TempSOTRE_NAME.get(i),TempDescription.get(i), TempBUILDING.get(i), TempSTREET.get(i),TempLANDMARK.get(i),Integer.parseInt(TempAREA_ID.get(i)),TempAREA.get(i).trim(), TempCITY.get(i), 
							TempMOBILE_NO.get(i),TempMOBILE_NO2.get(i),TempMOBILE_NO3.get(i),TempTEL_NO.get(i),TempTEL_NO2.get(i),TempTEL_NO3.get(i), TempPHOTO.get(i), TempDISTANCE.get(i), Integer.parseInt(TOTAL_PAGES), Integer.parseInt(TOTAL_RECORDS),page,"0",TempHasOffers.get(i),TempOffers.get(i),Integer.parseInt(TempTotalLike.get(i)),TempVERIFIED.get(i));
					dbutil.close();

				}

				SharedPreferences pref=context.getSharedPreferences("Page", 0);
				Editor editor=pref.edit();

				editor.putString(value+"TOTAL_PAGES",TOTAL_PAGES);
				editor.putString(value+"TOTAL_RECORDS",TOTAL_RECORDS);
				editor.putString(value+"page",page+"");
				editor.commit();

				Log.i("Caching ","Caching value"+value+" TOTAL_PAGES "+TOTAL_PAGES+" TOTAL_RECORDS "+TOTAL_RECORDS );
				//Updating Current Page
				dbutil.updateCurrentPage(page);
				datePref.setDateInPref();
				adapter.notifyDataSetChanged();

				txtCounter.setText(ID.size()+"/"+TOTAL_RECORDS);

				if(Integer.parseInt(TOTAL_RECORDS)==ID.size())
				{
					defLoadMore.setVisibility(View.GONE);
				}

				list.onRefreshComplete();
				list.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						if(!isRefreshing)
						{
							EventTracker.logEvent("MarketPlace_Details", false);
							Intent intent=new Intent(context, StoreDetailsActivity.class);
							intent.putExtra("STORE_ID", ID.get(position-1));
							intent.putExtra("STORE", SOTRE_NAME.get(position-1));
							intent.putExtra("PHOTO", PHOTO.get(position-1));
							intent.putExtra("AREA", AREA.get(position-1));
							intent.putExtra("CITY", CITY.get(position-1));
							intent.putExtra("MOBILE_NO", MOBILE_NO.get(position-1));
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}
		if(SEARCH_STATUS.equalsIgnoreCase("false"))
		{
			String message="";
			message=resultData.getString("SEARCH_MESSAGE");
			try
			{
				showToast(message);
				list.onRefreshComplete();
				relDefaultSearchProgress.setVisibility(ViewGroup.GONE);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}


	}

	void clearArray()
	{
		ID.clear();
		SOTRE_NAME.clear();
		PLACE_PARENT.clear();
		BUILDING.clear();
		STREET.clear();
		LANDMARK.clear();
		AREA_ID.clear();
		AREA.clear();
		CITY.clear();
		MOBILE_NO.clear();
		MOBILE_NO2.clear();
		MOBILE_NO3.clear();
		TEL_NO.clear();
		TEL_NO2.clear();
		TEL_NO3.clear();
		PHOTO.clear();
		DISTANCE.clear();
		DESCRIPTION.clear();
		HAS_OFFER.clear();
		OFFER.clear();
		TOTAL_LIKE.clear();
		page=0;
		post_count=20;

	}

	void clearSearchArray()
	{
		Search_ID.clear();
		Search_SOTRE_NAME.clear();
		Search_BUILDING.clear();
		Search_STREET.clear();
		Search_LANDMARK.clear();
		Search_AREA.clear();
		Search_CITY.clear();
		Search_MOBILE_NO.clear();
		Search_PHOTO.clear();
		Search_DISTANCE.clear();
		Search_DESCRIPTION.clear();
		search_page=1;
		search_post_count=20;
	}





	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		try
		{
			MenuItem extra=menu.add("Search");
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		return true;
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}
		if(item.getTitle().toString().equalsIgnoreCase("Search"))
		{
			Intent intent=new Intent(context,SearchActivity.class);
			intent.putExtra("isNewsCategory", false);
			startActivity(intent);
			overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
		}

		return true;
	}


	//location result
	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {

		relDefaultSearchProgress.setVisibility(ViewGroup.GONE);
		String status=resultData.getString("status");

		if(status.equalsIgnoreCase("set"))
		{
			try
			{
				latitued=resultData.getString("Lat");
				longitude=resultData.getString("Longi");


				if(latitued!=null || latitued.length()>0 && longitude!=null || longitude.length()>0)
				{
					if(!latitued.equalsIgnoreCase("not found") && !longitude.equalsIgnoreCase("not found"))
					{
						Log.i("LAT", "LATI "+latitued);
						Log.i("LONG", "LATI "+longitude);
						//commiting Locations
						SharedPreferences prefs=getSharedPreferences("location", 0);
						Editor editor=prefs.edit();
						editor.putString("latitued", latitued);
						editor.putString("longitude", longitude);
						editor.commit();
						loadEntries();
					}
					else
					{
						latitued="0";
						longitude="0";
						loadEntries();
						showToast(getResources().getString(R.string.locationAlert));
					}
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			showToast(getResources().getString(R.string.locationAlert));
			latitued="0";
			longitude="0";
		}

	}


	//Adapter to set Ui.
	class SearchListAdapter extends ArrayAdapter<String> /*implements SectionIndexer*/
	{
		ArrayList<String> store_name,distance,logo,MOBILE_NO,id,has_offer,offers,desc;
		Activity context;
		LayoutInflater inflate;
		Typeface tf;
		Animation anim_fromRight,anim_fromLeft;
		String isFav="",dbHasOffer,dbOffer;
		ArrayList<String>TOTAL_LIKE;
		
		public SearchListAdapter(Activity context, int resource,
				int textViewResourceId,ArrayList<String> id, ArrayList<String> store_name,ArrayList<String>distance,ArrayList<String>logo,ArrayList<String>MOBILE_NO,ArrayList<String>has_offers,ArrayList<String>offers,ArrayList<String>desc,ArrayList<String>TOTAL_LIKE) {
			super(context, resource, textViewResourceId, store_name);
			this.store_name=store_name;
			this.distance=distance;
			this.logo=logo;
			this.context=context;
			this.MOBILE_NO=MOBILE_NO;
			this.id=id;
			this.has_offer=has_offers;
			this.offers=offers;
			this.desc=desc;
			this.TOTAL_LIKE=TOTAL_LIKE;
			inflate=context.getLayoutInflater();

			//getting font from asset directory.
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");

		}
		@Override
		public int getCount() {
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			return store_name.get(position);
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			//inflate a view and build ui for list.
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_default_search_api,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				holder.imgCall=(ImageView)convertView.findViewById(R.id.searchCall);
				holder.imgFav=(ImageView)convertView.findViewById(R.id.searchFav);
				holder.linearCall=(RelativeLayout)convertView.findViewById(R.id.linearCall);
				holder.txtBackOffers=(TextView)convertView.findViewById(R.id.txtBackOffers);
				holder.img_Star=(ImageView)convertView.findViewById(R.id.img_Star);
				holder.imgRemoveFav=(ImageView)convertView.findViewById(R.id.searchRemoveFav);
				holder.txtFav=(TextView)convertView.findViewById(R.id.txtFav);
				holder.searchFront=(RelativeLayout)convertView.findViewById(R.id.searchFront);
				holder.searchBack=(RelativeLayout)convertView.findViewById(R.id.searchBack);
				holder.band=(TextView)convertView.findViewById(R.id.band);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.txtSearchStoreTotalLike=(TextView)convertView.findViewById(R.id.txtSearchStoreTotalLike);
				holder.defaultIsOffer=(ImageView)convertView.findViewById(R.id.defaultIsOffer);
				convertView.setTag(holder);

			}
			final ViewHolder hold=(ViewHolder)convertView.getTag();

			//Added Temp
			hold.searchBack.setVisibility(View.GONE);
			hold.imgFav.setVisibility(View.GONE);

			//
			//load store logos.

			if(logo.get(position).toString().length()==0)
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}
			else
			{
				//load store logos.replace space with %20 to make url correct.
				String photo_source=logo.get(position).toString().replaceAll(" ", "%20");
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}

			//set Total Like
			try
			{
				/*hold.txtSearchStoreTotalLike.setVisibility(View.GONE);*/
				if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
				{
					hold.txtSearchStoreTotalLike.setVisibility(View.GONE);
				}
				else
				{
					hold.txtSearchStoreTotalLike.setVisibility(View.VISIBLE);
				}
				hold.txtSearchStoreTotalLike.setText(TOTAL_LIKE.get(position));
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			//set store name.
			hold.txtStore.setText(store_name.get(position).toString().toUpperCase(Locale.US));

			hold.txtSearchStoreDesc.setText(desc.get(position));

			//set font for store name.

			hold.txtStore.setTypeface(tf, Typeface.BOLD);
			hold.txtSearchStoreDesc.setTypeface(tf);
			hold.txtDistance.setTypeface(tf);
			hold.txtDistance.setVisibility(View.VISIBLE);


			if(distance.get(position)!=null)
			{
				if(Double.parseDouble(distance.get(position))!=-1)
				{
					//	Log.i("Distance : ","Distance : "+distance.get(position));

					if(Double.parseDouble(distance.get(position))<1 && Double.parseDouble(distance.get(position))>0)
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position))*100);
						hold.txtDistance.setText(""+formatted+" m.");
					}
					else
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position)));
						if((Double.parseDouble(distance.get(position))*100)>8000)
						{
							hold.txtDistance.setVisibility(View.GONE);
						}
						else
						{
							hold.txtDistance.setVisibility(View.VISIBLE);
							hold.txtDistance.setText(""+formatted+" km.");
						}

					}

				}
			}

			hold.defaultIsOffer.setVisibility(View.VISIBLE);
			if(has_offer.get(position).toString().length()!=0 && has_offer.get(position).equalsIgnoreCase("1"))
			{
				hold.defaultIsOffer.setVisibility(View.VISIBLE);
			}
			else
			{
				hold.defaultIsOffer.setVisibility(View.GONE);
			}
			return convertView;
		}

	}
	 class ViewHolder
	{
		ImageView imgStoreLogo,imgCall,imgFav,img_Star,imgRemoveFav,defaultIsOffer;
		TextView txtStore,band,txtSearchStoreDesc;
		TextView txtDistance,txtBackOffers,txtFav;
		TextView txtSearchStoreTotalLike;
		RelativeLayout linearCall;
		RelativeLayout searchFront,searchBack;
	}




	@Override
	public void onBackPressed() {
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}

	}




	void changeData()
	{
		//If data is in database pull it and show it
		try
		{
			rowCount=dbutil.getRowCount(Integer.parseInt(value));
			datePref.checkDateDiff();
			if(datePref.getDiffHours()>Integer.parseInt(getString(R.string.pref_hours)))
			{
				if(isnetConnected.isNetworkAvailable())
				{
					isRefreshing=true;
					dbutil.deleteGeoText(Integer.parseInt(value));
					page=0;
					loadEntries();
				}
			}
			else
			{
				//if rowcount is greater than 0 fetch data from db and show it.
				if(rowCount>0)
				{
					loadLocalEntries();

				}else if(isnetConnected.isNetworkAvailable())
				{
					try
					{
						loadEntries();
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
				else
				{
					list.onRefreshComplete();
				}

			}
			Log.i("value", "value rowCount= "+rowCount);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}




	void getLocationFromPrefs()
	{
		SharedPreferences pref=context.getSharedPreferences("location",0);
		latitued=pref.getString("latitued", "0.0");
		longitude=pref.getString("longitude", "0.0");
		isLocationFound=pref.getBoolean("isLocationFound",false);
		Log.i("Location found","Location found "+isLocationFound+" "+latitued+","+longitude);
	}


	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();
		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}

}
