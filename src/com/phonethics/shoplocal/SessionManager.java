package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.urbanairship.push.PushManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {

	//Shared Preferences
	SharedPreferences pref;


	//Editor for Shared Preferences 
	Editor editor;

	//Shared Preference Mode
	int PRIVATE_MODE=0;

	//Shared Prefernce File Name
	private static final String PREF_NAME="SHOPLOCAL_SESSION";

	//All Shared Preferences Key
	private static final String IS_LOGIN="IS_LOGGED_IN";

	//User Name
	public static final String KEY_USER_NAME="mobileno";

	//Password
	public static final String KEY_PASSWORD="password";

	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	//Place Id
	public static final String KEY_PLACE_ID="place_id";

	//Customer
	//All Shared Preferences Key
	private static final String IS_LOGIN_CUSTOMER="IS_LOGGED_IN_CUSTOMER";

	//User Name
	public static final String KEY_USER_NAME_CUSTOMER="mobileno_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Place Id
	public static final String KEY_PLACE_ID_CUSTOMER="place_id_CUSTOMER";

	//Facebook Login
	public static final String FACEBOOK_LOGIN_CUSTOMER="facebook_login_CUSTOMER";

	//Context 
	Context context;

	//Constructor
	public SessionManager(Context context)
	{
		this.context=context;

		pref=context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor=pref.edit();

	}

	/*
	 * Create Login Session
	 */
	public void createLoginSession(String mobile_no,String password,String userid,String authid)
	{
		//Storing login value as true
		editor.putBoolean(IS_LOGIN, true);

		//Stroing mobile no in pref
		editor.putString(KEY_USER_NAME, mobile_no);

		//Storing password in pref
		editor.putString(KEY_PASSWORD, password);

		//Storing user id in pref
		editor.putString(KEY_USER_ID, userid);

		//Storing auth id in pref
		editor.putString(KEY_AUTH_ID, authid);

		//commit changes
		editor.commit();

		Log.i("Session", "Session Created");

	}

	public void createMerchantUser(String mobile_no)
	{
		//Stroing mobile no in pref
		editor.putString(KEY_USER_NAME, mobile_no);
		//commit changes
		editor.commit();

		Log.i("Session", "Session Created");
	}

	/*
	 * Create Login Session for customer
	 */
	public void createLoginSessionCustomer(String mobile_no,String password,String userid,String authid,boolean isFacebook)
	{
		//Storing login value as true
		editor.putBoolean(IS_LOGIN_CUSTOMER, true);

		//Stroing mobile no in pref
		editor.putString(KEY_USER_NAME_CUSTOMER, mobile_no);

		//Storing password in pref
		editor.putString(KEY_PASSWORD_CUSTOMER, password);

		//Storing user id in pref
		editor.putString(KEY_USER_ID_CUSTOMER, userid);

		//Storing auth id in pref
		editor.putString(KEY_AUTH_ID_CUSTOMER, authid);

		//Storing for facebook 
		editor.putBoolean(FACEBOOK_LOGIN_CUSTOMER, isFacebook);

		//commit changes
		editor.commit();

		Log.i("Session", "Session Created");

	}

	public void createUser(String mobile_no)
	{
		//Stroing mobile no in pref
		editor.putString(KEY_USER_NAME_CUSTOMER, mobile_no);
		//commit changes
		editor.commit();

		Log.i("Session", "Session Created");

	}

	/*
	 * Get stored session data
	 */
	public HashMap<String, String>getUserDetails()
	{

		HashMap<String, String>user=new HashMap<String, String>();

		//user name
		user.put(KEY_USER_NAME, pref.getString(KEY_USER_NAME, ""));

		//password
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, ""));

		//user id
		user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, ""));

		//auth id
		user.put(KEY_AUTH_ID, pref.getString(KEY_AUTH_ID, ""));

		//return user
		return user;

	}

	/*
	 * Get stored session data customer
	 */
	public HashMap<String, String>getUserDetailsCustomer()
	{

		HashMap<String, String>user=new HashMap<String, String>();

		//user name
		user.put(KEY_USER_NAME_CUSTOMER, pref.getString(KEY_USER_NAME_CUSTOMER, ""));

		//password
		user.put(KEY_PASSWORD_CUSTOMER, pref.getString(KEY_PASSWORD_CUSTOMER, ""));

		//user id
		user.put(KEY_USER_ID_CUSTOMER, pref.getString(KEY_USER_ID_CUSTOMER, ""));

		//auth id
		user.put(KEY_AUTH_ID_CUSTOMER, pref.getString(KEY_AUTH_ID_CUSTOMER, ""));

		//return user
		return user;

	}

	/*
	 * Check user logged in or not
	 */
	public void checkLogin()
	{
		//check Login Status
		if(!this.isLoggedIn())
		{
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(context, SplitLoginSignUp.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			context.startActivity(i);
		}
	}

	/*
	 * Check user logged in or not
	 */
	public void checkLoginCustomer()
	{
		//check Login Status
		if(!this.isLoggedInCustomer())
		{
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(context, SplitLoginSignUp.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			context.startActivity(i);
		}
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser(){
		// Clearing all data from Shared Preferences
		/*editor.clear();*/
		editor.remove(KEY_PASSWORD);
		editor.remove(KEY_USER_ID);
		editor.remove(KEY_AUTH_ID);
		editor.remove(IS_LOGIN);
		editor.commit();

		unTagMerchant();


	}

	void unTagMerchant()
	{
		try
		{
			//Get All Tags of the Devices
			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			//Check if the Tag has merchant tag. Then untag it.
			if(tags.contains("merchant"))
			{
				tags.remove("merchant");

			}

			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			String temp="";
			ArrayList<String>tempIdList=new ArrayList<String>();

			//Traversing Tags
			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains own_ then add them to another arraylist
				if(temp.startsWith("own_"))
				{
					tempIdList.add(temp);
				}
			}

			//Remove all tags contains own_id's.
			tags.removeAll(tempIdList);

			Log.i("TAGS", "TAGS SESSION "+tags.toString());

			//Set Tags to Urban Airship
			PushManager.shared().setTags(tags);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void unTagCustomer()
	{
		try
		{
			Log.i("URBAN", "URBAN SET TAG");

			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			ArrayList<String>tempIdList=new ArrayList<String>();

			String temp="";
			String replace="";
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			//Traversing Tags to find out if there are any tags existing with own_id
			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains customer_id_
				if(temp.startsWith("customer_id_"))
				{
					//Replace customer_id_ with "" and compare it with ID
					tempIdList.add(temp);
					Log.i("TAG", "Urban TAG ID "+replace);
				}
			}
			//Remove all tags contains own_id's.
			tags.removeAll(tempIdList);
			Log.i("TAG", "Urban TAG AFTER "+tags.toString());
			PushManager.shared().setTags(tags);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	/**
	 * Clear session details
	 * */
	public void logoutCustomer(){
		// Clearing all data from Shared Preferences
		/*editor.clear();*/
		editor.remove(KEY_PASSWORD_CUSTOMER);
		editor.remove(KEY_USER_ID_CUSTOMER);
		editor.remove(KEY_AUTH_ID_CUSTOMER);
		editor.remove(IS_LOGIN_CUSTOMER);
		editor.commit();

		unTagCustomer();
		Log.i("Session", "Session Logout");
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		Log.i("Session", "Session IS_LOGIN"+pref.getBoolean(IS_LOGIN, false));
		return pref.getBoolean(IS_LOGIN, false);
	}

	// Get Login State
	public boolean isLoggedInCustomer(){
		Log.i("Session", "Session IS_LOGIN"+pref.getBoolean(IS_LOGIN_CUSTOMER, false));
		return pref.getBoolean(IS_LOGIN_CUSTOMER, false);
	}


	// Get Login State
	public boolean isLoggedInFacebook(){
		Log.i("Session", "Session IS_LOGIN"+pref.getBoolean(FACEBOOK_LOGIN_CUSTOMER, false));
		return pref.getBoolean(FACEBOOK_LOGIN_CUSTOMER, false);
	}
}
