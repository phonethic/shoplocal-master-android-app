package com.phonethics.shoplocal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.customviewpager.JazzyViewPager;
import com.phonethics.customviewpager.JazzyViewPager.TransitionEffect;
import com.phonethics.shoplocal.StoreDetailsActivity.StorePageFragment;

public class StorePagerGallery extends SherlockFragmentActivity {

	static ImageLoader imageLoader;
	static DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;

	ActionBar actionbar;
	Activity context;
	ViewPager mPager;
	ArrayList<String> urls=new ArrayList<String>();
	ArrayList<String> IMAGE_TITLE1=new ArrayList<String>();
	int position=0;
	NetworkCheck network;


	static String PHOTO_PARENT_URL;


	boolean isCaption=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_pager_gallery);

		context=this;

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		actionbar=getSupportActionBar();
		actionbar.setTitle(getResources().getString(R.string.actionBarTitle));
		/*		actionbar.setSubtitle("Store Gallery");*/
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();

		network=new NetworkCheck(context);

		Bundle b=getIntent().getExtras();

		imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCacheGallery");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}/*else if(network.isNetworkAvailable())
		{

			DeleteRecursive(cacheDir);
		}
		 */
		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
		mPager=(ViewPager)findViewById(R.id.viewPagerStore);

		try
		{
			if(b!=null)
			{
				urls=b.getStringArrayList("photo_source");
				position=b.getInt("position");
				isCaption=b.getBoolean("isCaption",false);
				urls.remove(0);
				if(isCaption==true)
				{
					IMAGE_TITLE1=b.getStringArrayList("caption");
					if(IMAGE_TITLE1!=null)
					{
						IMAGE_TITLE1.remove(0);
					}
				}


				if(isCaption==false)
				{
					for(int i=0;i<urls.size();i++)
					{
						IMAGE_TITLE1.add("");
					}
				}

				PageAdapter adapter=new PageAdapter(getSupportFragmentManager(), context, getFragments(IMAGE_TITLE1,urls));
				mPager.setAdapter(adapter);
				mPager.setCurrentItem(position-1, true);

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuItem extra=menu.add("Share");
		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		extra.setIcon(R.drawable.ic_social_share);

		return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getTitle().toString().equalsIgnoreCase("Share"))
		{
			shareImage();
		}
		else
		{
			finishTo();	
		}


		return true;
	}

	void finishTo()
	{
		this.finish();
	}

	//Creating Fragment Instance and returing 
	private List<Fragment> getFragments(ArrayList<String>IMAGE_TITLE1,ArrayList<String>URL)
	{
		List<Fragment> fList = new ArrayList<Fragment>();

		for(int i=0;i<IMAGE_TITLE1.size();i++)
		{
			fList.add(PageFragment.newInstance("Gallery",IMAGE_TITLE1.get(i),URL.get(i)));
		}

		return fList;

	}


	//Creating Pages with PageAdapter.
	public class PageAdapter extends FragmentStatePagerAdapter
	{
		ArrayList<String> pages;
		ArrayList<String>IMAGE_TITLE1;

		List<Fragment>fragments;

		public PageAdapter(FragmentManager fm,Activity context,List<Fragment>fragments) {
			super(fm);
			// TODO Auto-generated constructor stub
			/*	this.pages=pages;
			this.IMAGE_TITLE1=IMAGE_TITLE1;*/
			this.fragments=fragments;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO Auto-generated method stub
			return pages.get(position);
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			/*	String photo=pages.get(position).toString().replaceAll(" ", "%20");
			String title=IMAGE_TITLE1.get(position).toString();*/
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return fragments.size();
		}

		//		@Override
		//		public Object instantiateItem(ViewGroup container, final int position) {
		//			Object obj = super.instantiateItem(container, position);
		//			mPager.setObjectForPosition(obj, position);
		//			return obj;
		//		}
		//
		//		@Override
		//		public boolean isViewFromObject(View view, Object object) {
		//			if(object != null){
		//				return ((Fragment)object).getView() == view;
		//			}else{
		//				return false;
		//			}
		//		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub
			/*
			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);*/
		}
	}



	public static class PageFragment extends Fragment 
	{
		public static final String EXTRA_MESSAGE = "PAGE";
		public static final String EXTRA_TITLE="TITLE";
		public static final String EXTRA_IMAGE="IMAGE";

		String url="";
		String title="";
		Activity context;
		View view;
		ImageView splashImage;
		TextView textCaption;
		ProgressBar prog;
		TextView txtPlaceText;
		Animation anim;
		public PageFragment()
		{

		}

		public static final PageFragment newInstance(String Page,String title,String url)
		{
			//Adding URL Page and Title to Bundle
			PageFragment f=new PageFragment();
			Bundle bdl=new Bundle(3);
			bdl.putString(EXTRA_MESSAGE, Page);
			bdl.putString(EXTRA_TITLE, title);
			bdl.putString(EXTRA_IMAGE, url);
			f.setArguments(bdl);
			return f;

		}

		//		public PageFragment(String url,String title,Activity context)
		//		{
		//			this.url=url;
		//			this.context=context;
		//			this.title=title;
		//			anim=AnimationUtils.loadAnimation(context, R.anim.grow_fade_in_center);
		//			anim.setDuration(1200);
		//		}
		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			try
			{
				//Getting Title and Url From Bundle
				title = getArguments().getString(EXTRA_TITLE);
				url= getArguments().getString(EXTRA_IMAGE);

				view=inflater.inflate(R.layout.storeimagepager, null);
				splashImage=(ImageView)view.findViewById(R.id.storeImageView);
				textCaption=(TextView)view.findViewById(R.id.textCaption);
				textCaption.setText(title);
				prog=(ProgressBar)view.findViewById(R.id.prog);

				/*			txtPlaceText.startAnimation(anim);*/

				imageLoader.displayImage(PHOTO_PARENT_URL+url, splashImage,options, new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub
						prog.setVisibility(View.VISIBLE);


					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						prog.setVisibility(View.INVISIBLE);

					}

					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						// TODO Auto-generated method stub
						prog.setVisibility(View.INVISIBLE);



					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						prog.setVisibility(View.INVISIBLE);

					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return view;
		}



	}

	void shareImage()
	{
		try
		{

			ImageView galleryImage=(ImageView)mPager.findViewById(R.id.storeImageView);
			Bitmap bitmap = ((BitmapDrawable)galleryImage.getDrawable()).getBitmap();
			if(bitmap!=null)
			{
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("image/jpeg");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
				File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
				try {
					f.createNewFile();
					FileOutputStream fo = new FileOutputStream(f);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (IOException e) {                       
					e.printStackTrace();
				}

				share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
				startActivity(Intent.createChooser(share, "Share Image"));
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}





}
