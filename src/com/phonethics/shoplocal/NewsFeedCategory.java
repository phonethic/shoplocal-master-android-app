package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.model.NewsFeedModel;
import com.phonethics.networkcall.FavouritePostResultReceiver;
import com.phonethics.networkcall.FavouritePostResultReceiver.FavouritePostInterface;
import com.phonethics.networkcall.FavouritePostService;
import com.phonethics.networkcall.NewsFeedReceiver;
import com.phonethics.networkcall.NewsFeedReceiver.NewsFeed;
import com.phonethics.networkcall.NewsFeedService;
import com.phonethics.networkcall.ShareResultReceiver;
import com.phonethics.networkcall.ShareResultReceiver.ShareResultInterface;
import com.phonethics.networkcall.ShareService;
import com.phonethics.shoplocal.NewsFeedActivity.NewsFeedAdapter;
import com.squareup.picasso.Picasso;

public class NewsFeedCategory extends SherlockActivity implements NewsFeed, FavouritePostInterface, ShareResultInterface {



	ActionBar actionBar;

	//URL'S
	static String STORE_URL;
	static String GET_SEARCH_URL;
	String PHOTO_PARENT_URL;

	Activity context;

	//Api header 
	static String API_HEADER;
	//Api header value
	static String API_VALUE;

	NewsFeedReceiver mReceiver;


	//Network 
	NetworkCheck network;

/*	ImageLoader imageLoaderList;*/

	//Listview
	PullToRefreshListView listCategNewsFeedResult;
	TextView txtCategNewsFeedCounter;
	ProgressBar newsFeedCategListProg;


	ArrayList<String> NEWS_FEED_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_NAME=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_AREA_ID=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_MOBNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO3=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_TELLNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_LATITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_LONGITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DISTANCE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_POST_TYPE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_TITLE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DESCRIPTION=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_image_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_thumb_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url3=new ArrayList<String>();

	ArrayList<String> NEWS_DATE=new ArrayList<String>();
	ArrayList<String> NEWS_IS_OFFERED=new ArrayList<String>();
	ArrayList<String> NEWS_OFFER_DATE_TIME=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_place_total_like=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_place_total_share=new ArrayList<String>();


	int post_count=20;
	int page=0;


	boolean isRefreshing=false;

	NewsFeedAdapter adapter=null;

	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	String offer="";
	String category_id="";

	boolean isCheckOffers=false;
	boolean isCategory=true;

	DBUtil dbutil;
	String value="";

	static String BROADCAST_URL;
	static String LIKE_API;
	static String SHARE_URL;

	static String USER_ID;
	static String AUTH_ID;

	String VIA = "Android";

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	int POST_LIKE=2;

	ShareResultReceiver shareServiceResult;
	FavouritePostResultReceiver mFav;

	SessionManager session;
	String TEMP_POST_ID="";

	private UiLifecycleHelper uiHelper;
	
	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	//String shareText = "Hea, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;
	
	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_feed_category);

		actionBar=getSupportActionBar();
		actionBar.setTitle(getString(R.string.actionBarTitle));
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		listCategNewsFeedResult=(PullToRefreshListView)findViewById(R.id.listCategNewsFeedResult);
		txtCategNewsFeedCounter=(TextView)findViewById(R.id.txtCategNewsFeedCounter);
		newsFeedCategListProg=(ProgressBar)findViewById(R.id.newsFeedCategListProg);

		context=this;
		network=new NetworkCheck(context);
		dbutil=new DBUtil(context);
		session=new SessionManager(context);

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);



	/*	imageLoaderList=new ImageLoader(context);*/

		mReceiver=new NewsFeedReceiver(new Handler());
		mReceiver.setReceiver(this);

		//Urls
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		GET_SEARCH_URL=getResources().getString(R.string.searchapi);

		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		LIKE_API=getResources().getString(R.string.like);

		SHARE_URL = "/" + getResources().getString(R.string.share);


		//API key
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

		mFav=new FavouritePostResultReceiver(new Handler());
		mFav.setReceiver(this);

		shareServiceResult = new ShareResultReceiver(new Handler());
		shareServiceResult.setReceiver(this);



		Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			value=dbutil.getActiveAreaID();
			category_id=bundle.getString("category_id");
			offer=bundle.getString("offer");
			isCategory=bundle.getBoolean("isCategory");
			if(!isCategory){
				isCheckOffers=true;
			}

			loadNewsFeeds();
		}

		adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
				NEWS_FEED_place_total_like, NEWS_FEED_place_total_share,NEWS_FEED_MOBNO1,NEWS_FEED_MOBNO2,NEWS_FEED_MOBNO3,NEWS_FEED_TELLNO1,NEWS_FEED_TELLNO2,NEWS_FEED_TELLNO3,NEWS_OFFER_DATE_TIME);

		listCategNewsFeedResult.setAdapter(adapter);
		//Pull to refresh 

		listCategNewsFeedResult.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				try
				{
					if(network.isNetworkAvailable())
					{
						if(!newsFeedCategListProg.isShown())
						{
							isRefreshing=true;
							page=0;
							loadNewsFeeds();
						}
					}
					else
					{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		listCategNewsFeedResult.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub
				if(network.isNetworkAvailable())
				{

					if(NEWS_FEED_ID.size()==Integer.parseInt(TOTAL_RECORDS))
					{

					}
					else if(page<Integer.parseInt(TOTAL_PAGES))
					{

						if(!newsFeedCategListProg.isShown())
						{
							//									page++;
							Log.i("Page", "Page "+page);
							//Load Stores from Internet
							loadNewsFeeds();

							Log.i("ID SIZE","NEWS FEED ID SIZE  "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						}
						else
						{
							showToast(getResources().getString(R.string.loadingAlert));

						}

					}
					else
					{
						Log.i("ID SIZE","NEWS FEED ID SIZE ELSE "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						newsFeedCategListProg.setVisibility(View.GONE);
					}
				}
				else
				{
					listCategNewsFeedResult.onRefreshComplete();
				}


			}
		});
		
		
		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		
		
		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		
		
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
		        pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
		
		
		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}
				
				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
				
			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}
		
		
		/** ANirudh custom share dialog */
		dialogShare = new Dialog(NewsFeedCategory.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));

	    



	}//onCreate Ends here



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		this.finish();
		return true;
	}




	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
	}



	void loadNewsFeeds()
	{
		if(network.isNetworkAvailable())
		{
			Intent intent=new Intent(context, NewsFeedService.class);
			intent.putExtra("newsFeed",mReceiver);
			intent.putExtra("URL", STORE_URL+"/"+GET_SEARCH_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("count",post_count);
			intent.putExtra("page",page+1);
			intent.putExtra("area_id",value);
			intent.putExtra("isCategory",isCategory);
			intent.putExtra("category_id",category_id);
			intent.putExtra("offer", offer);
			intent.putExtra("isCheckOffers", isCheckOffers);
			startService(intent);
			newsFeedCategListProg.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	@Override
	public void onNewsFeedResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		newsFeedCategListProg.setVisibility(View.GONE);

		try
		{

			String search_status=resultData.getString("status");
			String message=resultData.getString("message");
			ArrayList<NewsFeedModel> newsFeed=resultData.getParcelableArrayList("posts");





			if(search_status.equalsIgnoreCase("true"))
			{
				TOTAL_PAGES=resultData.getString("total_page");
				TOTAL_RECORDS=resultData.getString("total_record");

				Log.i("News FEED SIZE ", "News FEED SIZE "+newsFeed.size());

				try
				{

					if(isRefreshing)
					{
						clearArray();
						isRefreshing=false;
					}

					for(int i=0;i<newsFeed.size();i++)
					{
						NEWS_FEED_ID.add(newsFeed.get(i).getId());
						NEWS_FEED_NAME.add(newsFeed.get(i).getName());
						NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());

						NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
						NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
						NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());

						NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
						NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
						NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());

						NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
						NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
						NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());

						NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
						NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
						NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());

						NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());

						NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
						NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
						NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());

						NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
						NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
						NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());

						NEWS_DATE.add(newsFeed.get(i).getDate());
						NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());


						NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
						NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
						NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());
					}

					//					for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
					//					{
					//						NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
					//						NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
					//						NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );
					//
					//						NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
					//						NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
					//						NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));
					//
					//						NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
					//						NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
					//						NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));
					//
					//						NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
					//						NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
					//						NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));
					//
					//						NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
					//						NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
					//						NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));
					//
					//						NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));
					//
					//						NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
					//						NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
					//						NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));
					//
					//						NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
					//						NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
					//						NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));
					//
					//						NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
					//						NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));
					//
					//
					//						NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
					//						NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
					//						NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));
					//					}


					//Get the first visible position of listview
					//final int old_pos = listCategNewsFeedResult.getRefreshableView().getFirstVisiblePosition();

					page++;
					//					adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
					//							NEWS_FEED_place_total_like, NEWS_FEED_place_total_share);
					//
					//					listCategNewsFeedResult.setAdapter(adapter);

					adapter.notifyDataSetChanged();

					//Restore visible position
					//					listCategNewsFeedResult.post(new Runnable() {
					//
					//						@Override
					//						public void run() {
					//							// TODO Auto-generated method stub
					//							listCategNewsFeedResult.getRefreshableView().setSelection(old_pos);
					//						}
					//					});

					Log.i("TOTAL PAGES AND RECORDS  ","TOTAL CURRENT PAGE, PAGES AND RECORDS : PAGE "+page +" TOTAL PAGES "+TOTAL_PAGES+" TOTAL_RECORD "+TOTAL_RECORDS);

					txtCategNewsFeedCounter.setText(NEWS_FEED_ID.size()+"/"+TOTAL_RECORDS);

					listCategNewsFeedResult.onRefreshComplete();
					listCategNewsFeedResult.setOnItemClickListener(new OnItemClickListener() {


						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							if(!isRefreshing)
							{
								Intent intent=new Intent(context, OffersBroadCastDetails.class);
								intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
								intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
								intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
								intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
								intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
								intent.putExtra("fromCustomer", true);
								intent.putExtra("isFromStoreDetails", false);
								intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
								intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
								intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
								intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
								intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
								intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
								intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
								//								if(POST_USER_LIKE.size()==0)
								//								{
								intent.putExtra("USER_LIKE","-1");
								//								}else
								//								{
								//									intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
								//
								//								}

								context.startActivityForResult(intent, 5);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							}
						}
					});

				}catch(Exception ex)
				{
					ex.printStackTrace();

				}


			}
			else
			{
				showToast(message);

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}


	class NewsFeedAdapter extends ArrayAdapter<String>
	{

		Activity context;

		ArrayList<String> post_id;
		ArrayList<String> news_title;
		ArrayList<String> offer_title;
		ArrayList<String> offer_desc;

		ArrayList<String> offer_date;
		ArrayList<String> is_offer;

		ArrayList<String> photo_url1;
		ArrayList<String> photo_url2;
		ArrayList<String> photo_url3;



		ArrayList<String>mob1;
		ArrayList<String>mob2;
		ArrayList<String>mob3;
		
		ArrayList<String>tel1;
		ArrayList<String>tel2;
		ArrayList<String>tel3;
		
		ArrayList<String>valid_till;



		ArrayList<String> offer_total_like;
		ArrayList<String> offer_total_total_share;

		boolean [] isAnimEnd;   

		LayoutInflater inflator;

		Typeface tf;


		public NewsFeedAdapter(Activity context, int resource,
				int textViewResourceId,  ArrayList<String>post_id,ArrayList<String>news_title,ArrayList<String> offer_title,
				ArrayList<String> offer_desc,ArrayList<String> offer_date,
				ArrayList<String> is_offer,ArrayList<String> photo_url1,
				ArrayList<String> offer_total_like,ArrayList<String> offer_total_total_share,ArrayList<String>mob1,ArrayList<String>mob2,
				ArrayList<String>mob3,ArrayList<String>tel1,ArrayList<String>tel2,ArrayList<String>tel3,ArrayList<String>valid_till) {
			super(context, resource, textViewResourceId, offer_title);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();

			this.post_id=post_id;
			this.news_title=news_title;
			this.offer_title=offer_title;
			this.offer_desc=offer_desc;

			this.offer_date=offer_date;
			this.is_offer=is_offer;

			this.photo_url1=photo_url1;
			this.offer_total_like=offer_total_like;

			this.offer_total_total_share=offer_total_total_share;
			
			this.mob1=mob1;
			this.mob2=mob2;
			this.mob3=mob3;
			
			this.tel1=tel1;
			this.tel2=tel2;
			this.tel3=tel3;
			
			this.valid_till=valid_till;


			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_title.size()+" "+offer_desc.size()+" "+offer_date.size()+" "+is_offer.size()+" "+photo_url1.size());
			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_total_like.size()+" "+offer_total_total_share.size());

			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");


		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return offer_title.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return offer_title.get(position);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_in);
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflator.inflate(R.layout.newsfeedrow,null);

				holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
				//				holder.newsFeedOffersMonth=(TextView)convertView.findViewById(R.id.newsFeedOffersMonth);
				//
				//				holder.newsFeedviewOverLay=(View)convertView.findViewById(R.id.newsFeedviewOverLay);

				holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
				holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);

				holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
				holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);

				holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);

				holder.newFeedband=(TextView)convertView.findViewById(R.id.newFeedband);

				holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);

				holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);

				holder.newsFeedOffersText.setTypeface(tf);
				holder.newsFeedOffersDesc.setTypeface(tf);
				holder.newsFeedTotalLike.setTypeface(tf);

				holder.newsFeedTotalShare.setTypeface(tf);
				holder.newFeedband.setTypeface(tf);

				holder.newsFeedStoreName.setTypeface(tf);


				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();



			try
			{
				hold.newsFeedStoreName.setText(news_title.get(position));
				hold.newsFeedOffersText.setText(offer_title.get(position));
				hold.newsFeedOffersDesc.setText(offer_desc.get(position));

				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

				Date date_con = (Date) dt.parse(offer_date.get(position).toString());

				Date date=dateFormat.parse(offer_date.get(position).toString());
				DateFormat date_format=new SimpleDateFormat("hh:mm a");

				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
				hold.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));

				//				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				//				DateFormat dt2=new SimpleDateFormat("MMM");
				//				Date date_con = (Date) dt.parse(offer_date.get(position).toString());
				//				Calendar cal = Calendar.getInstance();
				//				cal.setTime(date_con);
				//				int year = cal.get(Calendar.YEAR);
				//				int month = cal.get(Calendar.MONTH);
				//				int day = cal.get(Calendar.DAY_OF_MONTH);
				//
				//				//	Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
				//				hold.newsFeedOffersDate.setText(day+"");
				//				hold.newsFeedOffersMonth.setText(dt2.format(date_con)+"");


				// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				// }
				// else
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
				// }
				hold.newsFeedTotalLike.setText(offer_total_like.get(position));


				hold.newsFeedTotalShare.setText(offer_total_total_share.get(position));

				hold.newsFeedTotalLike.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						//							showToast("Clicked On "+news_title.get(position));
						likePost(post_id.get(position));
					}
				});
				hold.newsFeedTotalShare.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						TEMP_POST_ID=post_id.get(position);

						/*
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Share");
						alertDialogBuilder
						.setMessage("Share using")
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(true)
						.setPositiveButton("Other",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.dismiss();
							
								List<Intent> targetedShareIntents = new ArrayList<Intent>();
								Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
								sharingIntent.setType("text/plain");
								String shareBody = news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position);

								PackageManager pm = context.getPackageManager();
								List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

								for(final ResolveInfo app : activityList) {

									String packageName = app.activityInfo.packageName;
									Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
									targetedShareIntent.setType("text/plain");

									if(TextUtils.equals(packageName, "com.facebook.katana")){
										
									} else {
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, news_title.get(position) + " - " + "Offers");
										targetedShareIntent.setPackage(packageName);
										targetedShareIntents.add(targetedShareIntent);
									}

								}

								Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
								chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
								startActivityForResult(chooserIntent,6);

							}
						})
						.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.dismiss();
								
								if(photo_url1.get(position).toString().length()!=0)
								{
									String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");

									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(news_title.get(position) + " - " + "Offers")
									.setPicture(PHOTO_PARENT_URL+photo_source)
									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
								else
								{
									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(news_title.get(position) + " - " + "Offers")
									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
							}
						})
						;

						AlertDialog alertDialog = alertDialogBuilder.create();

						alertDialog.show();*/
						
					
						showDialog(position);


					}});


				hold.newFeedband.setVisibility(View.GONE);
				hold.newsIsOffer.setVisibility(View.VISIBLE);

				hold.newsFeedStoreName.startAnimation(animation);
				hold.newsFeedOffersText.startAnimation(animation);
				hold.newsFeedOffersDesc.startAnimation(animation);
				hold.newsFeedOffersDate.startAnimation(animation);
				hold.newsFeedTotalLike.startAnimation(animation);
				hold.newsFeedTotalShare.startAnimation(animation);

				if(is_offer.get(position).toString().length()!=0 && is_offer.get(position).equalsIgnoreCase("1"))
				{
					hold.newsIsOffer.setVisibility(View.VISIBLE);

					//hold.newFeedband.setText("Specail Offer : "+is_offer.get(position));
					/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
					hold.txtStore.setTextColor(Color.WHITE);*/

				}
				else
				{
					//hold.txtBackOffers.setText("offers");
					hold.newFeedband.setVisibility(View.GONE);
					hold.newsIsOffer.setVisibility(View.INVISIBLE);
					/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
					hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
					hold.imgNewsFeedLogo.startAnimation(animation);
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				hold.imgNewsFeedLogo.setVisibility(View.VISIBLE);
				if(photo_url1.get(position).toString().length()==0)
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));
					//					hold.imgNewsFeedLogo.setVisibility(View.GONE);
					hold.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_CENTER);
					//					imageLoaderList.DisplayImage("",hold.imgNewsFeedLogo);
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(255, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(200, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.VISIBLE);
					/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgNewsFeedLogo);*/
					try{
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(hold.imgNewsFeedLogo);
					}
					catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){ 
						e.printStackTrace();
					}
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_XY);
					hold.imgNewsFeedLogo.startAnimation(animation);
				}

				//				Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
				//				convertView.startAnimation(animation);
				//				lastPosition = position;

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



			return convertView;
		}
		
		void showDialog(final int tempPos){
			try
			{
				dialogShare.show();

				listViewShare.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {

						String app = appName.get(position);
						
						
						
						String shareBody= "";
						
						String validTill="";
						
						String contact="";
						
						String contact_lable="\nContact:";
						
						Log.i("Is Offer ","Is Offer valid_till "+valid_till.get(tempPos));
						
						if(is_offer.get(tempPos).toString().length()!=0 && is_offer.get(tempPos).equalsIgnoreCase("1"))
						{
							try
							{
								DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
								DateFormat dt2=new SimpleDateFormat("MMM");
								DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

								Date date_con = (Date) dt.parse(valid_till.get(tempPos).toString());

								Date date=dateFormat.parse(valid_till.get(tempPos).toString());
								DateFormat date_format=new SimpleDateFormat("hh:mm a");

								Calendar cal = Calendar.getInstance();
								cal.setTime(date_con);
								int year = cal.get(Calendar.YEAR);
								int month = cal.get(Calendar.MONTH);
								int day = cal.get(Calendar.DAY_OF_MONTH);
								
								validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
								validTill="";
							}
							/*validTill="\nValid Till:"+valid_till.get(tempPos);*/
						}
						else
						{
							validTill="";
						}
						
						if(mob1.get(tempPos).toString().length()>0)
						{
							contact="+"+mob1.get(tempPos).toString();
						} 
						if(mob2.get(tempPos).toString().length()>0)
						{
							contact+=" +"+mob2.get(tempPos).toString();
						}
						if(mob3.get(tempPos).toString().length()>0)
						{
							contact+=" +"+mob3.get(tempPos).toString();
						}
						if(tel1.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						if(tel2.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						if(tel3.get(tempPos).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPos).toString();
						}
						
						if(contact.length()==0)
						{
							contact_lable="";
						}
						
						
						shareBody=news_title.get(tempPos) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(tempPos) + "\n" + offer_desc.get(tempPos)+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
						
						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

							if(photo_url1.get(tempPos).toString().length()!=0)
							{
								String photo_source=photo_url1.get(tempPos).toString().replaceAll(" ", "%20");

								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPos) + " - " + "Offers")
								.setPicture(PHOTO_PARENT_URL+photo_source)
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
							else
							{
								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPos) + " - " + "Offers")
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
						}
						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
							Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
						}

						else if(!app.equalsIgnoreCase("facebook")){
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setPackage(packageNames.get(position));
							i.setType("text/plain");
							i.putExtra(Intent.EXTRA_SUBJECT, news_title.get(tempPos));
							i.putExtra(Intent.EXTRA_TEXT, shareBody);
							startActivityForResult(i,6);
						}

						dismissDialog();

					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		void dismissDialog(){
			dialogShare.dismiss();
		}



	}

	static class ViewHolder
	{
		TextView newsFeedOffersDate;
		TextView newsFeedOffersMonth;

		TextView newsFeedOffersText;
		TextView newsFeedOffersDesc;

		TextView newsFeedTotalLike;
		TextView newsFeedTotalShare;

		ImageView imgNewsFeedLogo;

		View newsFeedviewOverLay;

		TextView newFeedband;

		ImageView newsIsOffer;

		TextView newsFeedStoreName;

	}

	void likePost(String POST_ID)
	{
		if(network.isNetworkAvailable())
		{
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context, FavouritePostService.class);
				intent.putExtra("favouritePost",mFav);
				intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("user_id", USER_ID);
				intent.putExtra("auth_id", AUTH_ID);
				intent.putExtra("post_id", POST_ID);
				context.startService(intent);
				newsFeedCategListProg.setVisibility(View.VISIBLE);
			}
			else
			{
				loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
			}
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}
	private void callShareApi(String POST_ID) {
		// TODO Auto-generated method stub

		Intent intent = new Intent(context, ShareService.class);
		intent.putExtra("shareService", shareServiceResult);
		intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("user_id", USER_ID);
		intent.putExtra("auth_id", AUTH_ID);
		intent.putExtra("post_id", POST_ID);
		intent.putExtra("via", VIA);
		context.startService(intent);


	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Discover");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==POST_LIKE)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	void clearArray()
	{
		NEWS_FEED_ID.clear();
		NEWS_FEED_NAME.clear();
		NEWS_FEED_AREA_ID.clear();

		NEWS_FEED_MOBNO1.clear();
		NEWS_FEED_MOBNO2.clear();
		NEWS_FEED_MOBNO3.clear();


		NEWS_FEED_TELLNO1.clear();
		NEWS_FEED_TELLNO2.clear();
		NEWS_FEED_TELLNO3.clear();

		NEWS_FEED_LATITUDE.clear();
		NEWS_FEED_LONGITUDE.clear();
		NEWS_FEED_DISTANCE.clear();

		NEWS_FEED_POST_ID.clear();
		NEWS_FEED_POST_TYPE.clear();

		NEWS_FEED_POST_TITLE.clear();
		NEWS_FEED_DESCRIPTION.clear();


		NEWS_FEED_image_url1.clear();
		NEWS_FEED_image_url2.clear();
		NEWS_FEED_image_url3.clear();

		NEWS_FEED_thumb_url1.clear();
		NEWS_FEED_thumb_url2.clear();
		NEWS_FEED_thumb_url3.clear();

		NEWS_DATE.clear();
		NEWS_IS_OFFERED.clear();
		NEWS_OFFER_DATE_TIME.clear();

		NEWS_FEED_place_total_like.clear();
		NEWS_FEED_place_total_share.clear();

		page=0;
		post_count=20;


	}



	@Override
	public void postLikeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		newsFeedCategListProg.setVisibility(View.GONE);
		String postLikestatus=resultData.getString("postLikestatus");
		String postLikemsg=resultData.getString("postLikemsg");
		if(postLikestatus.equalsIgnoreCase("true"))
		{
			showToast(postLikemsg);
			try
			{

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			showToast(postLikemsg);
			//			if(postLikemsg.startsWith(getResources().getString(R.string.invalidAuth)))
			//			{
			//				session.logoutCustomer();
			//			}
			String error_code=resultData.getString("error_code");
			if(error_code.equalsIgnoreCase("-221"))
			{
				session.logoutCustomer();
			}
			else if(postLikemsg.startsWith(getResources().getString(R.string.connectionTimedOut)))
			{

			}
			else
			{

			}

		}
	}


	@Override
	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String status = resultData.getString("share_status");

		String msg = resultData.getString("share_msg");
		try {

			if(status.equalsIgnoreCase("true")){

				showToast(msg);
			}
			else{

				showToast(msg);
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(session.isLoggedInCustomer())
		{
			HashMap<String,String>user_details=session.getUserDetailsCustomer();
			USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
		}
		if(requestCode==6)
		{
			VIA="Android";
			callShareApi(TEMP_POST_ID);
		}

		try
		{
			uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}

				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					VIA="Android-Facebook";
					callShareApi(TEMP_POST_ID);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}



}
