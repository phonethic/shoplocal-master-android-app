package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.BadTokenException;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.DatePreferencesClass;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.model.NewsFeedModel;
import com.phonethics.networkcall.FavouritePostResultReceiver;
import com.phonethics.networkcall.FavouritePostResultReceiver.FavouritePostInterface;
import com.phonethics.networkcall.FavouritePostService;
import com.phonethics.networkcall.NewsFeedReceiver;
import com.phonethics.networkcall.NewsFeedReceiver.NewsFeed;
import com.phonethics.networkcall.NewsFeedService;
import com.phonethics.networkcall.ShareResultReceiver;
import com.phonethics.networkcall.ShareResultReceiver.ShareResultInterface;
import com.phonethics.networkcall.ShareService;
import com.squareup.picasso.Picasso;



public class NewsFeedActivity extends SherlockActivity implements OnNavigationListener, NewsFeed, FavouritePostInterface, ShareResultInterface {

	ActionBar actionBar;

	//URL'S
	static String STORE_URL;
	static String GET_SEARCH_URL;

	Activity context;

	//Api header 
	static String API_HEADER;
	//Api header value
	static String API_VALUE;

	PullToRefreshListView listNewsFeed;
	PullToRefreshListView listNewsFeedSearch;

	TextView txtNewsFeedCount;
	TextView txtNewsFeedSearchCount;

	LinearLayout newsFeedLayout;
	LinearLayout newsFeedSearchLayout;

	EditText edtSearchNewsFeed;

	Button newsFeedMore;

	int dropdown_position;
	boolean isLocationFound=false;

	ArrayList<String>dropdownItems=new ArrayList<String>();
	String JSON_STRING;
	String key;
	String value;

	int lastVisiblePosition=0;

	String latitued="";
	String longitude="";

	DBUtil dbutil;

	NetworkCheck network;

	String locality="Lokhandwala,Andheri West";

	int post_count=20;
	int page=0;

	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	SessionManager session;

	NewsFeedReceiver mReceiver;

	ProgressBar newsProgress;

/*	ImageLoader imageLoaderList;
*/
	String PHOTO_PARENT_URL;

	NewsFeedAdapter adapter=null;

	boolean isRefreshing=false;

	int area_id=1;
	long rowCount;

	private static long back_pressed;

	String TEMP_POST_ID="";

	ArrayList<String> NEWS_FEED_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_NAME=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_AREA_ID=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_MOBNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_MOBNO3=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_TELLNO1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_TELLNO3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_LATITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_LONGITUDE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DISTANCE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_ID=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_POST_TYPE=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_POST_TITLE=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_DESCRIPTION=new ArrayList<String>();


	ArrayList<String> NEWS_FEED_image_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_image_url3=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_thumb_url1=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url2=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_thumb_url3=new ArrayList<String>();

	ArrayList<String> NEWS_DATE=new ArrayList<String>();
	ArrayList<String> NEWS_IS_OFFERED=new ArrayList<String>();
	ArrayList<String> NEWS_OFFER_DATE_TIME=new ArrayList<String>();

	ArrayList<String> NEWS_FEED_place_total_like=new ArrayList<String>();
	ArrayList<String> NEWS_FEED_place_total_share=new ArrayList<String>();

	private static final List<String> PERMISSIONS = Arrays.asList("email");


	String business_id="0";
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ArrayList<Item> items=new ArrayList<Item>();
	ArrayList<Integer>list_icons=new ArrayList<Integer>();

	boolean isDrawerClosed=false;

	String drawer_item="";

	DrawerExpandibleAdapter drawerAdapter;
	ExpandableListView drawerList;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();



	static String BROADCAST_URL;
	static String LIKE_API;
	static String SHARE_URL;

	static String USER_ID;
	static String AUTH_ID;

	String VIA = "Android";

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	int POST_LIKE=2;

	ShareResultReceiver shareServiceResult;
	FavouritePostResultReceiver mFav;

	RelativeLayout newsFeedBack;

	DrawerClass drawerClass;

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	String NEWSFEED_DATE_TIME_STAMP="NEWSFEED_DATE_TIME_STAMP";
	String KEY="NEWSFEED_DATE_TIME";
	//	SimpleDateFormat format;
	//	String date_time;
	//	long diffDays;

	DatePreferencesClass datePref;

	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;
	
	/** Events param */
	Map<String, String> eventTrackingParamsLike = new HashMap<String, String>();
	Map<String, String> eventTrackingParamsShare = new HashMap<String, String>();

	LocalNotification localNotification;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_feed);

		context=this;
		
		localNotification=new LocalNotification(getApplicationContext());
		
		dbutil=new DBUtil(context);
		session=new SessionManager(context);
		network=new NetworkCheck(context);

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		//Initializing Date Pref Class
		datePref=new DatePreferencesClass(context, NEWSFEED_DATE_TIME_STAMP, KEY);

		drawerClass=new DrawerClass(context);

		moreData=drawerClass.isShowMore();

/*		imageLoaderList=new ImageLoader(context);*/

		mReceiver=new NewsFeedReceiver(new Handler());
		mReceiver.setReceiver(this);

		mFav=new FavouritePostResultReceiver(new Handler());
		mFav.setReceiver(this);

		shareServiceResult = new ShareResultReceiver(new Handler());
		shareServiceResult.setReceiver(this);

		newsFeedBack=(RelativeLayout)findViewById(R.id.newsFeedBack);

		//Urls
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		GET_SEARCH_URL=getResources().getString(R.string.searchapi);


		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		LIKE_API=getResources().getString(R.string.like);

		SHARE_URL = "/" + getResources().getString(R.string.share);

		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//API key
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		actionBar=getSupportActionBar();
		actionBar.setTitle(getString(R.string.itemDiscover));

		actionBar.setHomeButtonEnabled(true);
		actionBar.setIcon(R.drawable.ic_drawer);

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);

		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();


		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});


		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getString(R.string.itemDiscover));
				isDrawerClosed=true;

				//						Toast.makeText(context, "Closed",Toast.LENGTH_SHORT).show();
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				isDrawerClosed=false;


				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerStateChanged(int newState) {
				// TODO Auto-generated method stub
				super.onDrawerStateChanged(newState);
			}

			@Override
			public void syncState() {
				// TODO Auto-generated method stub
				super.syncState();
			}


		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		//Creating Drawer
		createDrawer();

		//		JSON_STRING=getResources().getString(R.string.dropdownjson);

		try
		{

			value=dbutil.getActiveAreaID();
			//			Log.i("DEVICE","DEVICE  name"+" MODEL "+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
			//			SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
			//			dropdown_position=pref.getInt("dropdown_position",0);
			//			Log.i("dropdown_position", "dropdown_position"+dropdown_position);

			getLocationFromPrefs();

			//			ValueAnimator colorAnim = ObjectAnimator.ofInt(newsFeedBack, "backgroundColor", /*Red*/0xFFFF8080, /*Blue*/0xFF8080FF);
			//			colorAnim.setDuration(3000);
			//			colorAnim.setEvaluator(new ArgbEvaluator());
			//			colorAnim.setRepeatCount(ValueAnimator.INFINITE);
			//			colorAnim.setRepeatMode(ValueAnimator.REVERSE);
			//			colorAnim.start();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		//parse json of dropdown and set it to navigation
		//		parseJson(JSON_STRING);

		//Create Adapter
		//		CustomSpinnerAdapter business_list=new CustomSpinnerAdapter(context, 0, 0, dropdownItems);

		//Setting Navigation Type.
		//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		//		actionBar.setListNavigationCallbacks(business_list, this);
		//		actionBar.setSelectedNavigationItem(dropdown_position);



		newsProgress=(ProgressBar)findViewById(R.id.newsProgress);

		newsFeedMore=(Button)findViewById(R.id.newsFeedMore);

		listNewsFeed=(PullToRefreshListView)findViewById(R.id.listNewsFeed);
		listNewsFeedSearch=(PullToRefreshListView)findViewById(R.id.listNewsFeed);

		txtNewsFeedCount=(TextView)findViewById(R.id.txtNewsFeedCount);
		txtNewsFeedSearchCount=(TextView)findViewById(R.id.txtNewsFeedCount);

		newsFeedLayout=(LinearLayout)findViewById(R.id.newsFeedLayout);
		newsFeedSearchLayout=(LinearLayout)findViewById(R.id.newsFeedLayout);

		edtSearchNewsFeed=(EditText)findViewById(R.id.edtSearchNewsFeed);

		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		//		listNewsFeed.setLayoutAnimation(controller);

		//		if(network.isNetworkAvailable())
		//		{
		//			loadNewsFeeds();
		//		}
		//		else
		//		{
		//			showToast(getResources().getString(R.string.noInternetConnection));
		//		}

		adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
				NEWS_FEED_place_total_like, NEWS_FEED_place_total_share,NEWS_FEED_MOBNO1,NEWS_FEED_MOBNO2,NEWS_FEED_MOBNO3,NEWS_FEED_TELLNO1,NEWS_FEED_TELLNO2,NEWS_FEED_TELLNO3,NEWS_OFFER_DATE_TIME);

		listNewsFeed.setAdapter(adapter);


		newsFeedMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				if(!newsProgress.isShown())
				{
					//					Intent intent=new Intent(context,HomeGrid.class);
					//					startActivity(intent);
					//					finish();
					//					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					Intent intent=new Intent(context,LocationActivity.class);
					startActivity(intent);

					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});

		//Pull to refresh 

		listNewsFeed.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODnews_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position)O Auto-generated method stub
				try
				{
					if(network.isNetworkAvailable())
					{
						if(!newsProgress.isShown())
						{
							isRefreshing=true;
							//							dbutil.deleteNewsFeedByArea(Integer.parseInt(value));
							page=0;
							/*changeData();*/
							loadNewsFeeds();
						}
					}
					else
					{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});

		listNewsFeed.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub
				if(network.isNetworkAvailable())
				{

					if(NEWS_FEED_ID.size()==Integer.parseInt(TOTAL_RECORDS))
					{

					}
					else if(page<Integer.parseInt(TOTAL_PAGES))
					{

						if(!newsProgress.isShown())
						{
							//							page++;
							Log.i("Page", "Page "+page);
							//Load Stores from Internet
							loadNewsFeeds();

							Log.i("ID SIZE","NEWS FEED ID SIZE  "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						}
						else
						{
							showToast(getResources().getString(R.string.loadingAlert));

						}

					}
					else
					{
						Log.i("ID SIZE","NEWS FEED ID SIZE ELSE "+NEWS_FEED_ID.size()+" TOTAL  RECORD: "+TOTAL_RECORDS);
						newsProgress.setVisibility(View.GONE);
					}
				}
				else
				{
					listNewsFeed.onRefreshComplete();
				}


			}
		});

		try
		{
			clearArray();
			changeData();
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}

		EventTracker.startLocalyticsSession(getApplicationContext());

		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}


		}


		/** Anirudh- custom share dialog */
		dialogShare = new Dialog(NewsFeedActivity.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));


	}//OnCreate Ends Here





	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();
			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();

		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();

			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			drawer_item="";
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivityForResult(intent, 2);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";

			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";

			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}
			 */

			showDialogTab();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			Log.i("Event name",event);
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";

			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}

	}

	void showDialogTab(){
		try
		{
			dialogShare.show();

			listViewShare.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {

					String app = appName.get(position);
					if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(NewsFeedActivity.this)
						.setLink("http://shoplocal.co.in")
						.build();
						uiHelper.trackPendingDialogCall(shareDialog.present());
						dismissDialog();
					}
					else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
						Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
					}

					else if(!app.equalsIgnoreCase("facebook")){
						Intent i = new Intent(Intent.ACTION_SEND);
						i.setPackage(packageNames.get(position));
						i.setType("text/plain");
						i.putExtra(Intent.EXTRA_TEXT, shareText);
						startActivity(i);
					}

					dismissDialog();

				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}


	void changeData()
	{
		try
		{
			rowCount=dbutil.getNewsFeedRowCount(Integer.parseInt(value));

			datePref.checkDateDiff();

			//Date time Stamp is older than a day refresh Data
			if(datePref.getDiffHours()>Integer.parseInt(getString(R.string.pref_hours)))
			{
				if(network.isNetworkAvailable())
				{
					isRefreshing=true;
					dbutil.deleteNewsFeedByArea(Integer.parseInt(value));
					page=0;

					loadNewsFeeds();
				}
				else
				{
					listNewsFeed.onRefreshComplete();
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
			else
			{
				if(rowCount>0)
				{
					loadLocalNewsFeeds();

				}else if(network.isNetworkAvailable())
				{
					loadNewsFeeds();
				}
				else
				{
					listNewsFeed.onRefreshComplete();
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void loadNewsFeeds()
	{
		if(network.isNetworkAvailable())
		{
			Intent intent=new Intent(context, NewsFeedService.class);
			intent.putExtra("newsFeed",mReceiver);
			intent.putExtra("URL", STORE_URL+"/"+GET_SEARCH_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("count",post_count);
			intent.putExtra("page",page+1);
			intent.putExtra("area_id",value);
			context.startService(intent);
			newsProgress.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}
	void loadLocalNewsFeeds()
	{

		try
		{

			ArrayList<String> TEMP_NEWS_FEED_ID=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_NAME=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_AREA_ID=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_FEED_MOBNO1=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_MOBNO2=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_MOBNO3=new ArrayList<String>();


			ArrayList<String> TEMP_NEWS_FEED_TELLNO1=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_TELLNO2=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_TELLNO3=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_FEED_LATITUDE=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_LONGITUDE=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_DISTANCE=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_FEED_POST_ID=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_POST_TYPE=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_FEED_POST_TITLE=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_DESCRIPTION=new ArrayList<String>();


			ArrayList<String> TEMP_NEWS_FEED_image_url1=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_image_url2=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_image_url3=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_FEED_thumb_url1=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_thumb_url2=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_thumb_url3=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_DATE=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_IS_OFFERED=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_OFFER_DATE_TIME=new ArrayList<String>();

			ArrayList<String> TEMP_NEWS_FEED_place_total_like=new ArrayList<String>();
			ArrayList<String> TEMP_NEWS_FEED_place_total_share=new ArrayList<String>();


			TEMP_NEWS_FEED_ID.clear();
			TEMP_NEWS_FEED_NAME.clear();
			TEMP_NEWS_FEED_AREA_ID.clear();

			TEMP_NEWS_FEED_MOBNO1.clear();
			TEMP_NEWS_FEED_MOBNO2.clear();
			TEMP_NEWS_FEED_MOBNO3.clear();


			TEMP_NEWS_FEED_TELLNO1.clear();
			TEMP_NEWS_FEED_TELLNO2.clear();
			TEMP_NEWS_FEED_TELLNO3.clear();

			TEMP_NEWS_FEED_LATITUDE.clear();
			TEMP_NEWS_FEED_LONGITUDE.clear();
			TEMP_NEWS_FEED_DISTANCE.clear();

			TEMP_NEWS_FEED_POST_ID.clear();
			TEMP_NEWS_FEED_POST_TYPE.clear();

			TEMP_NEWS_FEED_POST_TITLE.clear();
			TEMP_NEWS_FEED_DESCRIPTION.clear();


			TEMP_NEWS_FEED_image_url1.clear();
			TEMP_NEWS_FEED_image_url2.clear();
			TEMP_NEWS_FEED_image_url3.clear();

			TEMP_NEWS_FEED_thumb_url1.clear();
			TEMP_NEWS_FEED_thumb_url2.clear();
			TEMP_NEWS_FEED_thumb_url3.clear();

			TEMP_NEWS_DATE.clear();
			TEMP_NEWS_IS_OFFERED.clear();
			TEMP_NEWS_OFFER_DATE_TIME.clear();

			TEMP_NEWS_FEED_place_total_like.clear();
			TEMP_NEWS_FEED_place_total_share.clear();


			//Database 
			TEMP_NEWS_FEED_ID=dbutil.getAllNewsFeedsStoreID(Integer.parseInt(value));
			TEMP_NEWS_FEED_NAME=dbutil.getAllNewsFeedsStoreName(Integer.parseInt(value));
			TEMP_NEWS_FEED_AREA_ID=dbutil.getAllNewsFeedsStoreArea(Integer.parseInt(value));

			TEMP_NEWS_FEED_MOBNO1=dbutil.getAllNewsFeedMob1(Integer.parseInt(value));
			TEMP_NEWS_FEED_MOBNO2=dbutil.getAllNewsFeedMob2(Integer.parseInt(value));
			TEMP_NEWS_FEED_MOBNO3=dbutil.getAllNewsFeedMob3(Integer.parseInt(value));

			TEMP_NEWS_FEED_TELLNO1=dbutil.getAllNewsFeedTell1(Integer.parseInt(value));
			TEMP_NEWS_FEED_TELLNO2=dbutil.getAllNewsFeedTell2(Integer.parseInt(value));
			TEMP_NEWS_FEED_TELLNO3=dbutil.getAllNewsFeedTell3(Integer.parseInt(value));

			TEMP_NEWS_FEED_LATITUDE=dbutil.getAllNewsFeedsLat(Integer.parseInt(value));
			TEMP_NEWS_FEED_LONGITUDE=dbutil.getAllNewsFeedsLong(Integer.parseInt(value));
			TEMP_NEWS_FEED_DISTANCE=dbutil.getAllNewsFeedsDistance(Integer.parseInt(value));

			TEMP_NEWS_FEED_POST_ID=dbutil.getAllNewsFeedsID(Integer.parseInt(value));
			TEMP_NEWS_FEED_POST_TYPE=dbutil.getAllNewsFeedsTypes(Integer.parseInt(value));


			TEMP_NEWS_FEED_POST_TITLE=dbutil.getAllNewsFeedsTitles(Integer.parseInt(value));
			TEMP_NEWS_FEED_DESCRIPTION=dbutil.getAllNewsFeedsDescs(Integer.parseInt(value));

			TEMP_NEWS_FEED_image_url1=dbutil.getAllNewsFeedsPhotoUrl1(Integer.parseInt(value));
			TEMP_NEWS_FEED_image_url2=dbutil.getAllNewsFeedsPhotoUrl2(Integer.parseInt(value));
			TEMP_NEWS_FEED_image_url3=dbutil.getAllNewsFeedsPhotoUrl3(Integer.parseInt(value));

			TEMP_NEWS_FEED_thumb_url1=dbutil.getAllNewsFeedsThumbs1(Integer.parseInt(value));
			TEMP_NEWS_FEED_thumb_url2=dbutil.getAllNewsFeedsThumbs2(Integer.parseInt(value));
			TEMP_NEWS_FEED_thumb_url3=dbutil.getAllNewsFeedsThumbs3(Integer.parseInt(value));

			TEMP_NEWS_DATE=dbutil.getAllNewsFeedsDATE(Integer.parseInt(value));
			TEMP_NEWS_IS_OFFERED=dbutil.getAllNewsFeedsIsOffer(Integer.parseInt(value));
			TEMP_NEWS_OFFER_DATE_TIME=dbutil.getAllNewsFeedsOfferDate(Integer.parseInt(value));

			TEMP_NEWS_FEED_place_total_like=dbutil.getAllNewsFeedTotalLike(Integer.parseInt(value));
			TEMP_NEWS_FEED_place_total_share=dbutil.getAllNewsFeedTotalShare(Integer.parseInt(value));

			for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
			{
				NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
				NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
				NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );

				NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
				NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
				NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));

				NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
				NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
				NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));

				NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
				NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
				NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));

				NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
				NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
				NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));

				NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));

				NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
				NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
				NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));

				NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
				NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
				NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));

				NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
				NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));


				NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
				NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
				NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));

			}

			page=dbutil.getAllNewsFeedCurrentPages(Integer.parseInt(value));
			TOTAL_PAGES=dbutil.getAllNewsFeedTotalPages(Integer.parseInt(value))+"";
			TOTAL_RECORDS=dbutil.getAllNewsFeedTotalRecord(Integer.parseInt(value))+"";

			Log.i("TOTAL PAGES AND RECORDS : ","TOTAL CURRENT PAGE,PAGES AND RECORDS LOCAL:  PAGE "+page +" TOTAL PAGES "+TOTAL_PAGES+" TOTAL_RECORD "+TOTAL_RECORDS);

			//			adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
			//					NEWS_FEED_place_total_like, NEWS_FEED_place_total_share);

			txtNewsFeedCount.setText(NEWS_FEED_ID.size()+"/"+TOTAL_RECORDS);

			//			listNewsFeed.setAdapter(adapter);

			adapter.notifyDataSetChanged();

			listNewsFeed.onRefreshComplete();

			listNewsFeed.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					if(!isRefreshing)
					{
						EventTracker.logEvent("OfferStream_Details", true);

						Intent intent=new Intent(context, OffersBroadCastDetails.class);
						intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
						intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
						intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
						intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
						intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
						intent.putExtra("fromCustomer", true);
						intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
						intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
						intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
						intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
						intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
						intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
						intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
						//						if(POST_USER_LIKE.size()==0)
						//						{
						intent.putExtra("USER_LIKE","-1");
						//						}else
						//						{
						//							intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
						//
						//						}

						context.startActivityForResult(intent, 5);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			});


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/* convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_menus, parent, false);
			}
			((TextView) convertView.findViewById(R.id.listText))
			.setText(getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/*convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_dropdown_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(R.id.txtSpinner))
			.setText(getItem(position));
			return convertView;
		}


	}



	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		//		MenuItem extra=menu.add("Extra Settings").setIcon(R.drawable.switch_view);
		//		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		try
		{
			MenuItem extra=menu.add("Search");
			extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);


			//			SearchView searchView = new SearchView(actionBar.getThemedContext());
			//			searchView.setQueryHint("Search");
			//			searchView.setIconified(true);
			//
			//			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
			//			.setIcon(R.drawable.abs__ic_search)
			//			.setActionView(searchView)
			//			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			//
			//			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			//
			//				@Override
			//				public boolean onQueryTextSubmit(String newText) {
			//					
			//					Intent intent=new Intent(context,SearchActivity.class);
			//					intent.putExtra("genericSearch", true);
			//					intent.putExtra("search_text", newText);
			//					startActivity(intent);
			//					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
			////					Toast.makeText(context, newText, Toast.LENGTH_SHORT).show();
			//					
			////					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			////					alertDialogBuilder3.setTitle("Shoplocal");
			////					alertDialogBuilder3
			////					.setMessage("Search : "+newText)
			////					.setIcon(R.drawable.ic_launcher)
			////			
			////					.setPositiveButton(getResources().getString(R.string.search_only_offers),new DialogInterface.OnClickListener() {
			////						public void onClick(DialogInterface dialog,int id) {
			////
			////							dialog.dismiss();
			////
			////						}
			////					})
			////					.setNegativeButton(getResources().getString(R.string.search_in_all),new DialogInterface.OnClickListener() {
			////						public void onClick(DialogInterface dialog,int id) {
			////
			////							dialog.dismiss();
			////						}
			////					})
			////					;
			////
			////					AlertDialog alertDialog3 = alertDialogBuilder3.create();
			////					alertDialog3.setCancelable(true);
			////					alertDialog3.show();
			////					
			//					
			//					
			//					return true;
			//				}
			//
			//
			//
			//				@Override
			//				public boolean onQueryTextChange(String newText) {
			//
			//
			//					return true;
			//				}
			//			});


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}

		if(item.getTitle().toString().equalsIgnoreCase("Search"))
		{
			Intent intent=new Intent(context,SearchActivity.class);
			intent.putExtra("isNewsCategory", true);
			startActivity(intent);
			overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
		}

		if(item.getTitle().toString().equalsIgnoreCase("Extra Settings"))
		{
			SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
			Editor 	editor=prefs.edit();
			if(session.isLoggedIn())
			{
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
		return true;
	}

	void getLocationFromPrefs()
	{
		SharedPreferences pref=context.getSharedPreferences("location",0);
		latitued=pref.getString("latitued", "not found");
		longitude=pref.getString("longitude", "not found");
		isLocationFound=pref.getBoolean("isLocationFound",false);

		Log.i("Location found","Location found "+isLocationFound+" "+latitued+","+longitude);
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		business_id=itemPosition+"";

		SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
		Editor editor=pref.edit();
		editor.putString("selected_dropdown", getDropDownObject(itemPosition,JSON_STRING));
		editor.putInt("dropdown_position", itemPosition);
		editor.commit();

		Log.i("business_id", "business_id commited "+getDropDownObject(itemPosition,JSON_STRING));

		//parse key and value to search
		parseKeyValue(getDropDownObject(itemPosition,JSON_STRING));
		clearArray();
		changeData();

		return false;
	}

	void parseKeyValue(String json)
	{
		try {
			Log.i("PARSE ", "PARSE KEY  "+json);
			JSONObject jsonobject=new JSONObject(json);
			key=jsonobject.getString("key");
			value=jsonobject.getString("value");
			locality=value;
			//IS_SHOPLOCAL=Boolean.parseBoolean(jsonobject.getString("isShopLocal"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void parseJson(String json)
	{
		try {

			Log.i("PARSE ", "PARSE  "+json);

			JSONObject jsonobject=new JSONObject(json);
			JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");
			for(int i=0;i<dropDownArray.length();i++)
			{
				JSONObject jsonObject=dropDownArray.getJSONObject(i);

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					//Shoplocal
					if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}else
				{
					//Inorbit
					if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	String getDropDownObject(int position,String json)
	{
		String dropdown="";
		if(dropdownItems.size()!=0)
		{

			Log.i("PARSE ", "PARSE getDropDownObject  "+json);
			//Log.i("BOOLEAN ", "BOOLEAN "+getResources().getBoolean(R.bool.isAppShopLocal)+" POSITION "+position);
			try {
				JSONObject jsonobject=new JSONObject(json);
				JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");

				/*	JSONObject jsonObject=dropDownArray.getJSONObject(position);
				dropdown=jsonObject.toString();

				Log.i("JSON", "JSON "+dropdown);*/

				for(int i=0;i<dropDownArray.length();i++)
				{
					JSONObject jsonObject=dropDownArray.getJSONObject(i);

					Log.i("PARSE ", "BOOLEAN PARSE getDropDownObject  "+jsonObject.toString()); 


					if(getResources().getBoolean(R.bool.isAppShopLocal))
					{
						//Shoplocal
						if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}else
					{
						//Inorbit
						if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}
				}

				Log.i("GET DROP DOWN ", "GET DROP DOWN "+dropdown);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();

			}


		}
		return dropdown;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.NewsFeedReceiver.NewsFeed#onNewsFeedResult(int, android.os.Bundle)
	 */
	@Override
	public void onNewsFeedResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		newsProgress.setVisibility(View.GONE);

		try
		{

			String search_status=resultData.getString("status");
			String message=resultData.getString("message");
			ArrayList<NewsFeedModel> newsFeed=resultData.getParcelableArrayList("posts");





			if(search_status.equalsIgnoreCase("true"))
			{
				TOTAL_PAGES=resultData.getString("total_page");
				TOTAL_RECORDS=resultData.getString("total_record");

				Log.i("News FEED SIZE ", "News FEED SIZE "+newsFeed.size());

				try
				{

					if(isRefreshing)
					{
						clearArray();
						isRefreshing=false;
						dbutil.deleteNewsFeedByArea(Integer.parseInt(value));
					}


					ArrayList<String> TEMP_NEWS_FEED_ID=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_NAME=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_AREA_ID=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_MOBNO1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_MOBNO2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_MOBNO3=new ArrayList<String>();


					ArrayList<String> TEMP_NEWS_FEED_TELLNO1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_TELLNO2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_TELLNO3=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_LATITUDE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_LONGITUDE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_DISTANCE=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_POST_ID=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_POST_TYPE=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_POST_TITLE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_DESCRIPTION=new ArrayList<String>();


					ArrayList<String> TEMP_NEWS_FEED_image_url1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_image_url2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_image_url3=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_thumb_url1=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_thumb_url2=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_thumb_url3=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_DATE=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_IS_OFFERED=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_OFFER_DATE_TIME=new ArrayList<String>();

					ArrayList<String> TEMP_NEWS_FEED_place_total_like=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_place_total_share=new ArrayList<String>();
					ArrayList<String> TEMP_NEWS_FEED_place_verified=new ArrayList<String>();


					TEMP_NEWS_FEED_ID.clear();
					TEMP_NEWS_FEED_NAME.clear();
					TEMP_NEWS_FEED_AREA_ID.clear();

					TEMP_NEWS_FEED_MOBNO1.clear();
					TEMP_NEWS_FEED_MOBNO2.clear();
					TEMP_NEWS_FEED_MOBNO3.clear();


					TEMP_NEWS_FEED_TELLNO1.clear();
					TEMP_NEWS_FEED_TELLNO2.clear();
					TEMP_NEWS_FEED_TELLNO3.clear();

					TEMP_NEWS_FEED_LATITUDE.clear();
					TEMP_NEWS_FEED_LONGITUDE.clear();
					TEMP_NEWS_FEED_DISTANCE.clear();

					TEMP_NEWS_FEED_POST_ID.clear();
					TEMP_NEWS_FEED_POST_TYPE.clear();

					TEMP_NEWS_FEED_POST_TITLE.clear();
					TEMP_NEWS_FEED_DESCRIPTION.clear();


					TEMP_NEWS_FEED_image_url1.clear();
					TEMP_NEWS_FEED_image_url2.clear();
					TEMP_NEWS_FEED_image_url3.clear();

					TEMP_NEWS_FEED_thumb_url1.clear();
					TEMP_NEWS_FEED_thumb_url2.clear();
					TEMP_NEWS_FEED_thumb_url3.clear();

					TEMP_NEWS_DATE.clear();
					TEMP_NEWS_IS_OFFERED.clear();
					TEMP_NEWS_OFFER_DATE_TIME.clear();

					TEMP_NEWS_FEED_place_total_like.clear();
					TEMP_NEWS_FEED_place_total_share.clear();

					TEMP_NEWS_FEED_place_verified.clear();


					for(int i=0;i<newsFeed.size();i++)
					{
						TEMP_NEWS_FEED_ID.add(newsFeed.get(i).getId());
						TEMP_NEWS_FEED_NAME.add(newsFeed.get(i).getName());
						TEMP_NEWS_FEED_AREA_ID.add(newsFeed.get(i).getArea_id());

						TEMP_NEWS_FEED_MOBNO1.add(newsFeed.get(i).getMob_no1());
						TEMP_NEWS_FEED_MOBNO2.add(newsFeed.get(i).getMob_no2());
						TEMP_NEWS_FEED_MOBNO3.add(newsFeed.get(i).getMob_no3());

						TEMP_NEWS_FEED_TELLNO1.add(newsFeed.get(i).getTel_no1());
						TEMP_NEWS_FEED_TELLNO2.add(newsFeed.get(i).getTel_no2());
						TEMP_NEWS_FEED_TELLNO3.add(newsFeed.get(i).getTel_no3());

						TEMP_NEWS_FEED_LATITUDE.add(newsFeed.get(i).getLatitude());
						TEMP_NEWS_FEED_LONGITUDE.add(newsFeed.get(i).getLongitude());
						TEMP_NEWS_FEED_DISTANCE.add(newsFeed.get(i).getDistance());

						TEMP_NEWS_FEED_POST_ID.add(newsFeed.get(i).getPost_id());
						TEMP_NEWS_FEED_POST_TYPE.add(newsFeed.get(i).getType());
						TEMP_NEWS_FEED_POST_TITLE.add(newsFeed.get(i).getTitle());

						TEMP_NEWS_FEED_DESCRIPTION.add(newsFeed.get(i).getDescription());

						TEMP_NEWS_FEED_image_url1.add(newsFeed.get(i).getImage_url());
						TEMP_NEWS_FEED_image_url2.add(newsFeed.get(i).getImage_url2());
						TEMP_NEWS_FEED_image_url3.add(newsFeed.get(i).getImage_url3());

						TEMP_NEWS_FEED_thumb_url1.add(newsFeed.get(i).getThumb_url());
						TEMP_NEWS_FEED_thumb_url2.add(newsFeed.get(i).getThumb_url2());
						TEMP_NEWS_FEED_thumb_url3.add(newsFeed.get(i).getThumb_url3());

						TEMP_NEWS_DATE.add(newsFeed.get(i).getDate());
						TEMP_NEWS_IS_OFFERED.add(newsFeed.get(i).getIs_offered());


						TEMP_NEWS_OFFER_DATE_TIME.add(newsFeed.get(i).getOffer_date_time());
						TEMP_NEWS_FEED_place_total_like.add(newsFeed.get(i).getTotal_like());
						TEMP_NEWS_FEED_place_total_share.add(newsFeed.get(i).getTotal_share());

						TEMP_NEWS_FEED_place_verified.add(newsFeed.get(i).getVerified());
					}

					for(int i=0;i<TEMP_NEWS_FEED_ID.size();i++)
					{
						NEWS_FEED_ID.add(TEMP_NEWS_FEED_ID.get(i) );
						NEWS_FEED_NAME.add(TEMP_NEWS_FEED_NAME.get(i));
						NEWS_FEED_AREA_ID.add(TEMP_NEWS_FEED_AREA_ID.get(i) );

						NEWS_FEED_MOBNO1.add(TEMP_NEWS_FEED_MOBNO1.get(i));
						NEWS_FEED_MOBNO2.add(TEMP_NEWS_FEED_MOBNO2.get(i));
						NEWS_FEED_MOBNO3.add(TEMP_NEWS_FEED_MOBNO3.get(i));

						NEWS_FEED_TELLNO1.add(TEMP_NEWS_FEED_TELLNO1.get(i));
						NEWS_FEED_TELLNO2.add(TEMP_NEWS_FEED_TELLNO2.get(i));
						NEWS_FEED_TELLNO3.add(TEMP_NEWS_FEED_TELLNO3.get(i));

						NEWS_FEED_LATITUDE.add(TEMP_NEWS_FEED_LATITUDE.get(i));
						NEWS_FEED_LONGITUDE.add(TEMP_NEWS_FEED_LONGITUDE.get(i));
						NEWS_FEED_DISTANCE.add(TEMP_NEWS_FEED_DISTANCE.get(i));

						NEWS_FEED_POST_ID.add(TEMP_NEWS_FEED_POST_ID.get(i));
						NEWS_FEED_POST_TYPE.add(TEMP_NEWS_FEED_POST_TYPE.get(i));
						NEWS_FEED_POST_TITLE.add(TEMP_NEWS_FEED_POST_TITLE.get(i));

						NEWS_FEED_DESCRIPTION.add(TEMP_NEWS_FEED_DESCRIPTION.get(i));

						NEWS_FEED_image_url1.add(TEMP_NEWS_FEED_image_url1.get(i));
						NEWS_FEED_image_url2.add(TEMP_NEWS_FEED_image_url2.get(i));
						NEWS_FEED_image_url3.add(TEMP_NEWS_FEED_image_url3.get(i));

						NEWS_FEED_thumb_url1.add(TEMP_NEWS_FEED_thumb_url1.get(i));
						NEWS_FEED_thumb_url2.add(TEMP_NEWS_FEED_thumb_url2.get(i));
						NEWS_FEED_thumb_url3.add(TEMP_NEWS_FEED_thumb_url3.get(i));

						NEWS_DATE.add(TEMP_NEWS_DATE.get(i));
						NEWS_IS_OFFERED.add(TEMP_NEWS_IS_OFFERED.get(i));


						NEWS_OFFER_DATE_TIME.add(TEMP_NEWS_OFFER_DATE_TIME.get(i));
						NEWS_FEED_place_total_like.add(TEMP_NEWS_FEED_place_total_like.get(i));
						NEWS_FEED_place_total_share.add(TEMP_NEWS_FEED_place_total_share.get(i));
					}




					page++;

					// Inserting values into database

					for(int i=0;i<newsFeed.size();i++)
					{
						//						if(!dbutil.isNewsFeedPostIdExists(Integer.parseInt(TEMP_NEWS_FEED_POST_ID.get(i))))
						//						{
						dbutil.open();
						dbutil.create_newsFeed(Integer.parseInt(TEMP_NEWS_FEED_ID.get(i)),Integer.parseInt(TEMP_NEWS_FEED_AREA_ID.get(i)),TEMP_NEWS_FEED_NAME.get(i), TEMP_NEWS_FEED_MOBNO1.get(i)
								, TEMP_NEWS_FEED_MOBNO2.get(i),TEMP_NEWS_FEED_MOBNO3.get(i),TEMP_NEWS_FEED_TELLNO1.get(i),TEMP_NEWS_FEED_TELLNO2.get(i),
								TEMP_NEWS_FEED_TELLNO3.get(i), TEMP_NEWS_FEED_LATITUDE.get(i), TEMP_NEWS_FEED_LONGITUDE.get(i), TEMP_NEWS_FEED_DISTANCE.get(i), Integer.parseInt(TEMP_NEWS_FEED_POST_ID.get(i)),
								TEMP_NEWS_FEED_POST_TYPE.get(i), TEMP_NEWS_FEED_POST_TITLE.get(i),TEMP_NEWS_FEED_DESCRIPTION.get(i), TEMP_NEWS_FEED_image_url1.get(i),TEMP_NEWS_FEED_image_url2.get(i),
								TEMP_NEWS_FEED_image_url3.get(i),TEMP_NEWS_FEED_thumb_url1.get(i),TEMP_NEWS_FEED_thumb_url2.get(i),TEMP_NEWS_FEED_thumb_url3.get(i),
								TEMP_NEWS_DATE.get(i), TEMP_NEWS_IS_OFFERED.get(i),TEMP_NEWS_OFFER_DATE_TIME.get(i), TEMP_NEWS_FEED_place_total_like.get(i),TEMP_NEWS_FEED_place_total_share.get(i),Integer.parseInt(TOTAL_PAGES),Integer.parseInt(TOTAL_RECORDS),page,TEMP_NEWS_FEED_place_verified.get(i));
						dbutil.close();
						//						}
					}


					dbutil.updateNewsFeedsCurrentPage(page,Integer.parseInt(TEMP_NEWS_FEED_AREA_ID.get(0)));
					dbutil.updateNewsFeedsTotalPage(Integer.parseInt(TOTAL_PAGES),Integer.parseInt(TEMP_NEWS_FEED_AREA_ID.get(0)));
					dbutil.updateNewsFeedsTotalRecord(Integer.parseInt(TOTAL_RECORDS),Integer.parseInt(TEMP_NEWS_FEED_AREA_ID.get(0)));

					datePref.setDateInPref();

					//Get the first visible position of listview
					//					final int old_pos = listNewsFeed.getRefreshableView().getFirstVisiblePosition();

					//					adapter =new NewsFeedAdapter(context, 0, 0,NEWS_FEED_POST_ID,NEWS_FEED_NAME,NEWS_FEED_POST_TITLE, NEWS_FEED_DESCRIPTION, NEWS_DATE, NEWS_IS_OFFERED, NEWS_FEED_image_url1,
					//							NEWS_FEED_place_total_like, NEWS_FEED_place_total_share);

					//					listNewsFeed.setAdapter(adapter);

					//Restore visible position
					//					listNewsFeed.post(new Runnable() {
					//
					//						@Override
					//						public void run() {
					//							// TODO Auto-generated method stub
					//							listNewsFeed.getRefreshableView().setSelection(old_pos);
					//						}
					//					});

					Log.i("TOTAL PAGES AND RECORDS  ","TOTAL CURRENT PAGE, PAGES AND RECORDS : PAGE "+page +" TOTAL PAGES "+TOTAL_PAGES+" TOTAL_RECORD "+TOTAL_RECORDS);

					txtNewsFeedCount.setText(NEWS_FEED_ID.size()+"/"+TOTAL_RECORDS);

					listNewsFeed.onRefreshComplete();

					adapter.notifyDataSetChanged();

					listNewsFeed.setOnItemClickListener(new OnItemClickListener() {


						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							// TODO Auto-generated method stub
							if(!isRefreshing)
							{
								EventTracker.logEvent("OfferStream_Details", false);

								Intent intent=new Intent(context, OffersBroadCastDetails.class);
								intent.putExtra("storeName", NEWS_FEED_NAME.get(position-1));
								intent.putExtra("title", NEWS_FEED_POST_TITLE.get(position-1));
								intent.putExtra("body", NEWS_FEED_DESCRIPTION.get(position-1));
								intent.putExtra("POST_ID", NEWS_FEED_POST_ID.get(position-1));
								intent.putExtra("PLACE_ID", NEWS_FEED_ID.get(position-1));
								intent.putExtra("fromCustomer", true);
								intent.putExtra("isFromStoreDetails", false);
								intent.putExtra("mob1", NEWS_FEED_MOBNO1.get(position-1));
								intent.putExtra("mob2", NEWS_FEED_MOBNO2.get(position-1));
								intent.putExtra("mob3", NEWS_FEED_MOBNO3.get(position-1));
								intent.putExtra("tel1", NEWS_FEED_TELLNO1.get(position-1));
								intent.putExtra("tel2", NEWS_FEED_TELLNO2.get(position-1));
								intent.putExtra("tel3", NEWS_FEED_TELLNO3.get(position-1));
								intent.putExtra("photo_source", NEWS_FEED_image_url1.get(position-1));
								//								if(POST_USER_LIKE.size()==0)
								//								{
								intent.putExtra("USER_LIKE","-1");
								//								}else
								//								{
								//									intent.putExtra("USER_LIKE",POST_USER_LIKE.get(position));
								//
								//								}

								context.startActivityForResult(intent, 5);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
							}
						}
					});

				}catch(Exception ex)
				{
					ex.printStackTrace();

				}


			}
			else
			{
				showToast(message);
				listNewsFeed.onRefreshComplete();
				clearArray();

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	class NewsFeedAdapter extends ArrayAdapter<String>
	{

		private int lastPosition = -1;

		Activity context;

		ArrayList<String> post_id;
		ArrayList<String> news_title;
		ArrayList<String> offer_title;
		ArrayList<String> offer_desc;

		ArrayList<String> offer_date;
		ArrayList<String> is_offer;

		ArrayList<String> photo_url1;
		ArrayList<String> photo_url2;
		ArrayList<String> photo_url3;
		
		ArrayList<String>mob1;
		ArrayList<String>mob2;
		ArrayList<String>mob3;
		
		ArrayList<String>tel1;
		ArrayList<String>tel2;
		ArrayList<String>tel3;
		
		ArrayList<String>valid_till;



		ArrayList<String> offer_total_like;
		ArrayList<String> offer_total_total_share;

		boolean [] isAnimEnd;   

		LayoutInflater inflator;

		Typeface tf;


		public NewsFeedAdapter(Activity context, int resource,
				int textViewResourceId,  ArrayList<String>post_id,ArrayList<String>news_title,ArrayList<String> offer_title,
				ArrayList<String> offer_desc,ArrayList<String> offer_date,
				ArrayList<String> is_offer,ArrayList<String> photo_url1,
				ArrayList<String> offer_total_like,ArrayList<String> offer_total_total_share,ArrayList<String>mob1,ArrayList<String>mob2,
				ArrayList<String>mob3,ArrayList<String>tel1,ArrayList<String>tel2,ArrayList<String>tel3,ArrayList<String>valid_till) {
			super(context, resource, textViewResourceId, offer_title);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();

			this.post_id=post_id;
			this.news_title=news_title;
			this.offer_title=offer_title;
			this.offer_desc=offer_desc;

			this.offer_date=offer_date;
			this.is_offer=is_offer;

			this.photo_url1=photo_url1;
			this.offer_total_like=offer_total_like;

			this.offer_total_total_share=offer_total_total_share;
			
			this.mob1=mob1;
			this.mob2=mob2;
			this.mob3=mob3;
			
			this.tel1=tel1;
			this.tel2=tel2;
			this.tel3=tel3;
			
			this.valid_till=valid_till;


			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_title.size()+" "+offer_desc.size()+" "+offer_date.size()+" "+is_offer.size()+" "+photo_url1.size());
			//			Log.i("SIZES OF NEWS FEED", "SIZES OF NEWS FEED "+offer_total_like.size()+" "+offer_total_total_share.size());

			tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");


		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return offer_title.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return offer_title.get(position);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_in);
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflator.inflate(R.layout.newsfeedrow,null);

				holder.newsFeedOffersDate=(TextView)convertView.findViewById(R.id.newsFeedOffersDate);
				//				holder.newsFeedOffersMonth=(TextView)convertView.findViewById(R.id.newsFeedOffersMonth);
				//
				//				holder.newsFeedviewOverLay=(View)convertView.findViewById(R.id.newsFeedviewOverLay);

				holder.newsFeedOffersText=(TextView)convertView.findViewById(R.id.newsFeedOffersText);
				holder.newsFeedOffersDesc=(TextView)convertView.findViewById(R.id.newsFeedOffersDesc);

				holder.newsFeedTotalLike=(TextView)convertView.findViewById(R.id.newsFeedTotalLike);
				holder.newsFeedTotalShare=(TextView)convertView.findViewById(R.id.newsFeedTotalShare);

				holder.imgNewsFeedLogo=(ImageView)convertView.findViewById(R.id.imgNewsFeedLogo);

				holder.newFeedband=(TextView)convertView.findViewById(R.id.newFeedband);

				holder.newsIsOffer=(ImageView)convertView.findViewById(R.id.newsIsOffer);

				holder.newsFeedStoreName=(TextView)convertView.findViewById(R.id.newsFeedStoreName);

				holder.newsFeedOffersText.setTypeface(tf);
				holder.newsFeedOffersDesc.setTypeface(tf);
				holder.newsFeedTotalLike.setTypeface(tf);

				holder.newsFeedTotalShare.setTypeface(tf);
				holder.newFeedband.setTypeface(tf);

				holder.newsFeedStoreName.setTypeface(tf);



				convertView.setTag(holder);
			}

			ViewHolder hold=(ViewHolder)convertView.getTag();



			try
			{
				hold.newsFeedStoreName.setText(news_title.get(position));
				hold.newsFeedOffersText.setText(offer_title.get(position));
				hold.newsFeedOffersDesc.setText(offer_desc.get(position));

				DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dt2=new SimpleDateFormat("MMM");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

				Date date_con = (Date) dt.parse(offer_date.get(position).toString());

				Date date=dateFormat.parse(offer_date.get(position).toString());
				DateFormat date_format=new SimpleDateFormat("hh:mm a");

				Calendar cal = Calendar.getInstance();
				cal.setTime(date_con);
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);

				Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+offer_date.get(position));
				hold.newsFeedOffersDate.setText(day+" "+dt2.format(date_con)+"   "+date_format.format(date));
				//				hold.newsFeedOffersMonth.setText(dt2.format(date_con)+"");


				// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
				// }
				// else
				// {
				// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
				// }
				hold.newsFeedTotalLike.setText(offer_total_like.get(position));


				hold.newsFeedTotalShare.setText(offer_total_total_share.get(position));

				hold.newsFeedTotalLike.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						//							showToast("Clicked On "+news_title.get(position));

						likePost(post_id.get(position));
					}
				});
				hold.newsFeedTotalShare.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						TEMP_POST_ID=post_id.get(position);

						/*
>>>>>>> af8ed9ee455d4b4c9b6e69502d8d9a2ff525d42a
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Share");
						alertDialogBuilder
						.setMessage("Share using")
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(true)
						.setPositiveButton("Other",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.dismiss();

								List<Intent> targetedShareIntents = new ArrayList<Intent>();
								Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
								sharingIntent.setType("text/plain");
								String shareBody = news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position);

								PackageManager pm = context.getPackageManager();
								List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

								for(final ResolveInfo app : activityList) {

									String packageName = app.activityInfo.packageName;
									Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
									targetedShareIntent.setType("text/plain");

									if(TextUtils.equals(packageName, "com.facebook.katana")){

									} else {
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, news_title.get(position) + " - " + "Offers");
										targetedShareIntent.setPackage(packageName);
										targetedShareIntents.add(targetedShareIntent);
									}

								}

								Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
								chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
								startActivityForResult(chooserIntent,6);

							}
						})

						.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.dismiss();

								if(photo_url1.get(position).toString().length()!=0)
								{
									String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");

									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(news_title.get(position) + " - " + "Offers")
									.setPicture(PHOTO_PARENT_URL+photo_source)
									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
								else
								{
									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(news_title.get(position) + " - " + "Offers")
									.setDescription(news_title.get(position) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(position) + "\n" + offer_desc.get(position))
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
							}
						})
						;

						AlertDialog alertDialog = alertDialogBuilder.create();

						alertDialog.show();*/


						showDialog(position);


					}
				});


				hold.newFeedband.setVisibility(View.GONE);
				hold.newsIsOffer.setVisibility(View.VISIBLE);


				hold.newsFeedStoreName.startAnimation(animation);
				hold.newsFeedOffersText.startAnimation(animation);
				hold.newsFeedOffersDesc.startAnimation(animation);
				hold.newsFeedOffersDate.startAnimation(animation);
				hold.newsFeedTotalLike.startAnimation(animation);
				hold.newsFeedTotalShare.startAnimation(animation);


				if(is_offer.get(position).toString().length()!=0 && is_offer.get(position).equalsIgnoreCase("1"))
				{
					hold.newsIsOffer.setVisibility(View.VISIBLE);

					//hold.newFeedband.setText("Specail Offer : "+is_offer.get(position));
					/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
					hold.txtStore.setTextColor(Color.WHITE);*/

				}
				else
				{
					//hold.txtBackOffers.setText("offers");
					hold.newFeedband.setVisibility(View.GONE);
					hold.newsIsOffer.setVisibility(View.INVISIBLE);
					/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
					hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			try
			{
				Log.i("PHOTO URL ","PHOTO URL"+photo_url1.get(position).toString());
				hold.imgNewsFeedLogo.setVisibility(View.VISIBLE);
				if(photo_url1.get(position).toString().length()==0)
				{
					//Log.i("LOGO", "LOGO "+logo.get(position));
					//hold.imgNewsFeedLogo.setVisibility(View.GONE);
					hold.imgNewsFeedLogo.setImageResource(R.drawable.ic_place_holder);
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_CENTER);
					//					imageLoaderList.DisplayImage("",hold.imgNewsFeedLogo);
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(255, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.INVISIBLE);
				}
				else
				{
					String photo_source=photo_url1.get(position).toString().replaceAll(" ", "%20");
					//					hold.newsFeedOffersDate.setTextColor(Color.argb(200, 255, 255, 255));
					//					hold.newsFeedviewOverLay.setVisibility(View.VISIBLE);
					/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgNewsFeedLogo);*/
					try{
						Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
						.placeholder(R.drawable.ic_place_holder)
						.error(R.drawable.ic_place_holder)
						.into(hold.imgNewsFeedLogo);
					}
					catch(IllegalArgumentException illegalArg){
						illegalArg.printStackTrace();
					}
					catch(Exception e){ 
						e.printStackTrace();
					}
					
					hold.imgNewsFeedLogo.setScaleType(ScaleType.FIT_XY);

					hold.imgNewsFeedLogo.startAnimation(animation);

				}

				//				Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
				//				convertView.startAnimation(animation);
				//				lastPosition = position;







			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



			return convertView;
		}

		void showDialog(final int tempPost){
			try
			{
				dialogShare.show();

				listViewShare.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {

						String app = appName.get(position);
						
						
						
						String shareBody= "";
						
						String validTill="";
						
						String contact="";
						
						String contact_lable="\nContact:";
						
						Log.i("Is Offer ","Is Offer valid_till "+valid_till.get(tempPost));
						
						if(is_offer.get(tempPost).toString().length()!=0 && is_offer.get(tempPost).equalsIgnoreCase("1"))
						{
							try
							{
								DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
								DateFormat dt2=new SimpleDateFormat("MMM");
								DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

								Date date_con = (Date) dt.parse(valid_till.get(tempPost).toString());

								Date date=dateFormat.parse(valid_till.get(tempPost).toString());
								DateFormat date_format=new SimpleDateFormat("hh:mm a");

								Calendar cal = Calendar.getInstance();
								cal.setTime(date_con);
								int year = cal.get(Calendar.YEAR);
								int month = cal.get(Calendar.MONTH);
								int day = cal.get(Calendar.DAY_OF_MONTH);
								
								validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
								validTill="";
							}
							/*validTill="\nValid Till:"+valid_till.get(tempPost);*/
						}
						else
						{
							validTill="";
						}
						
						if(mob1.get(tempPost).toString().length()>0)
						{
							contact="+"+mob1.get(tempPost).toString();
						} 
						if(mob2.get(tempPost).toString().length()>0)
						{
							contact+=" +"+mob2.get(tempPost).toString();
						}
						if(mob3.get(tempPost).toString().length()>0)
						{
							contact+=" +"+mob3.get(tempPost).toString();
						}
						if(tel1.get(tempPost).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPost).toString();
						}
						if(tel2.get(tempPost).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPost).toString();
						}
						if(tel3.get(tempPost).toString().length()>0)
						{
							contact+=" "+tel1.get(tempPost).toString();
						}
						
						if(contact.length()==0)
						{
							contact_lable="";
						}
						
						
						shareBody=news_title.get(tempPost) + " - " + "Offers" + " : " + "\n\n"+ offer_title.get(tempPost) + "\n" + offer_desc.get(tempPost)+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";
						
						if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

							if(photo_url1.get(tempPost).toString().length()!=0)
							{
								String photo_source=photo_url1.get(tempPost).toString().replaceAll(" ", "%20");

								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPost) + " - " + "Offers")
								.setPicture(PHOTO_PARENT_URL+photo_source)
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
							else
							{
								FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
								.setLink("http://shoplocal.co.in")
								.setName(news_title.get(tempPost) + " - " + "Offers")
								.setDescription(shareBody)
								.build();
								uiHelper.trackPendingDialogCall(shareDialog.present());
								dismissDialog();
							}
						}
						else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
							Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
						}

						else if(!app.equalsIgnoreCase("facebook")){
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setPackage(packageNames.get(position));
							i.setType("text/plain");
							i.putExtra(Intent.EXTRA_SUBJECT, news_title.get(tempPost));
							i.putExtra(Intent.EXTRA_TEXT, shareBody);
							startActivityForResult(i,6);
						}

						dismissDialog();

					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		void dismissDialog(){
			dialogShare.dismiss();
		}

	}

	static class ViewHolder
	{
		TextView newsFeedOffersDate;
		TextView newsFeedOffersMonth;

		TextView newsFeedOffersText;
		TextView newsFeedOffersDesc;

		TextView newsFeedTotalLike;
		TextView newsFeedTotalShare;

		ImageView imgNewsFeedLogo;

		View newsFeedviewOverLay;

		TextView newFeedband;

		ImageView newsIsOffer;

		TextView newsFeedStoreName;

	}

	void likePost(String POST_ID)
	{
		if(network.isNetworkAvailable())
		{
			if(session.isLoggedInCustomer())
			{
				Map<String,String> eventLike=new HashMap<String, String>();
				eventLike.put("Source", "OfferStream");
				eventLike.put("AreaName",dbutil.getActiveArea());
				EventTracker.logEvent("Offer_Like", eventLike);
				
				Intent intent=new Intent(context, FavouritePostService.class);
				intent.putExtra("favouritePost",mFav);
				intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("user_id", USER_ID);
				intent.putExtra("auth_id", AUTH_ID);
				intent.putExtra("post_id", POST_ID);
				context.startService(intent);
				newsProgress.setVisibility(View.VISIBLE);
			}
			else
			{
				loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
			}
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	private void callShareApi(String POST_ID) {
		// TODO Auto-generated method stub
		try{
			Map<String,String> eventShare=new HashMap<String, String>();
			eventShare.put("Source", "OfferStream");
			eventShare.put("AreaName",dbutil.getActiveArea());
			EventTracker.logEvent("Offer_Share", eventShare);
			
			Intent intent = new Intent(context, ShareService.class);
			intent.putExtra("shareService", shareServiceResult);
			intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("post_id", POST_ID);
			intent.putExtra("via", VIA);
			context.startService(intent);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}


	/* Anirudh next two methods for facebook login */
	public boolean login_facebook(){


		try
		{
			//pBarFb.setVisibility(View.VISIBLE);
			//showToast("Please Wait");

			//List<String> permissions = session.getPermissions();


			Session.openActiveSession(context, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();

						if (!isSubsetOf(PERMISSIONS, permissions)) {
							//pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									try{
										//Log.d("", "Fb responcse == "+response.toString());
										//Log.d("", "FbUser >> "+ user.toString());

										//fbUserid=user.getId();
										//fbAccessToken=session.getAccessToken();
										//Log.i("User Id", "FbName : "+fbAccessToken);
										//Log.i("User Id ", "FbId : "+fbUserid);

										//pBarFb.setVisibility(View.INVISIBLE);


										//loginCustomer();

									}catch(Exception ex){
										ex.printStackTrace();
									}


								}

								else{
									//EventTracker.logEvent("CSignup_LoginFailedFB", false);
									//pBarFb.setVisibility(View.INVISIBLE);
									//									Log.i("ELSE", "ELSE");
									//									showToast("wrong");
								}
							}


						});
						session.getAccessToken();
						Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();
					}

					if(session.isClosed()){
						//pBarFb.setVisibility(View.INVISIBLE);
						//EventTracker.logEvent("CSignup_LoginFailedFB", false);
						//Toast.makeText(context, "Closed", 0).show();
					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

		return true;

	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Discover");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==POST_LIKE)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	void clearArray()
	{
		NEWS_FEED_ID.clear();
		NEWS_FEED_NAME.clear();
		NEWS_FEED_AREA_ID.clear();

		NEWS_FEED_MOBNO1.clear();
		NEWS_FEED_MOBNO2.clear();
		NEWS_FEED_MOBNO3.clear();


		NEWS_FEED_TELLNO1.clear();
		NEWS_FEED_TELLNO2.clear();
		NEWS_FEED_TELLNO3.clear();

		NEWS_FEED_LATITUDE.clear();
		NEWS_FEED_LONGITUDE.clear();
		NEWS_FEED_DISTANCE.clear();

		NEWS_FEED_POST_ID.clear();
		NEWS_FEED_POST_TYPE.clear();

		NEWS_FEED_POST_TITLE.clear();
		NEWS_FEED_DESCRIPTION.clear();


		NEWS_FEED_image_url1.clear();
		NEWS_FEED_image_url2.clear();
		NEWS_FEED_image_url3.clear();

		NEWS_FEED_thumb_url1.clear();
		NEWS_FEED_thumb_url2.clear();
		NEWS_FEED_thumb_url3.clear();

		NEWS_DATE.clear();
		NEWS_IS_OFFERED.clear();
		NEWS_OFFER_DATE_TIME.clear();

		NEWS_FEED_place_total_like.clear();
		NEWS_FEED_place_total_share.clear();

		page=0;
		post_count=20;


	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), "Press again to exit!", Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}



	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
			if(session.isLoggedInCustomer())
			{
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
			}
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();
				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
		if(requestCode==6)
		{
			VIA="Android";
			callShareApi(TEMP_POST_ID);
		}

		try
		{
			uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
				@Override
				public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
					Log.e("Activity", String.format("Error: %s", error.toString()));
				}

				@Override
				public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
					Log.i("Activity", "Success!");
					VIA="Android-Facebook";
					callShareApi(TEMP_POST_ID);
				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}





	@Override
	public void postLikeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		newsProgress.setVisibility(View.GONE);
		String postLikestatus=resultData.getString("postLikestatus");
		String postLikemsg=resultData.getString("postLikemsg");
		if(postLikestatus.equalsIgnoreCase("true"))
		{
			showToast(postLikemsg);
			try
			{

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			showToast(postLikemsg);
			//			if(postLikemsg.startsWith(getResources().getString(R.string.invalidAuth)))
			//			{
			//				session.logoutCustomer();
			//			}
			String error_code=resultData.getString("error_code");
			if(error_code.equalsIgnoreCase("-221"))
			{
				session.logoutCustomer();
				createDrawer();
			}
			else if(postLikemsg.startsWith(getResources().getString(R.string.connectionTimedOut)))
			{

			}

		}
	}


	@Override
	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		String status = resultData.getString("share_status");

		String msg = resultData.getString("share_msg");



		try {

			if(status.equalsIgnoreCase("true")){

				showToast(msg);
			}
			else{

				showToast(msg);
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}



}


class SharedListViewAdapter extends ArrayAdapter<String>{

	Context c;
	LayoutInflater inflater;
	ArrayList<String> appName; ArrayList<String> packageName;
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	public SharedListViewAdapter(Context context, int resource, LayoutInflater inflater, ArrayList<String> appName, ArrayList<String> packageNames, ArrayList<Drawable> appIcon) {
		super(context, resource);
		c = context;
		this.inflater = inflater; 
		this.appName = appName;
		this.packageName = packageNames;
		this.appIcon = appIcon;
	}

	@Override
	public int getCount() {
		return appName.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		/*LayoutInflater inflater = (LayoutInflater) c
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/


		View v = inflater.inflate(R.layout.sharedialoglistviewlayout, null);

		TextView tv = (TextView) v.findViewById(R.id.textViewAppName);
		ImageView iv = (ImageView) v.findViewById(R.id.imageViewAppIcon);

		tv.setText(appName.get(position));
		iv.setImageDrawable(appIcon.get(position));
		//iv.setImageDrawable();

		return v;
	}
}
