package com.phonethics.shoplocal;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;


public class AboutWebView extends SherlockActivity {

	WebView aboutWeb;
	ActionBar actionBar;
	ProgressBar aboutWebProg;
	String url;
	String SocialURL;
	String webUrl;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_web_view);


		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		aboutWeb=(WebView)findViewById(R.id.aboutWeb);

		aboutWebProg=(ProgressBar)findViewById(R.id.aboutWebProg);
		

		Bundle b=getIntent().getExtras();
		if(b!=null)
		{
			url=b.getString("url");
			if(url.equalsIgnoreCase("SocialURL"))
			{
				SocialURL=b.getString("SocialURL");
				boolean isFb=b.getBoolean("isFb", false);
				
				if(isFb)
				{
					actionBar.setTitle("Facebook");
				}
				else
				{
					actionBar.setTitle("Twitter");
				}
			}
			else if(url.equalsIgnoreCase("Web"))
			{
				actionBar.setTitle("Web");
				webUrl=b.getString("Web");
			}

			if(url.equalsIgnoreCase("PrivacyPolicy"))
			{
				actionBar.setTitle("Privacy Policy");
				aboutWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));
			}
			if(url.equalsIgnoreCase("Terms"))
			{
				actionBar.setTitle("Terms & Conditions");
				aboutWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));
			}
			if(url.equalsIgnoreCase("Faq"))
			{
				actionBar.setTitle("FAQ");
				aboutWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.faqurl));
			}
			if(url.equalsIgnoreCase("SocialURL"))
			{
				Log.i("SocailUrl", "SocialURL "+SocialURL);
				if(!SocialURL.startsWith("http://") && !SocialURL.startsWith("https://"))
				{
					aboutWeb.loadUrl("http://"+SocialURL.replaceAll(" ", ""));
				}
				else
				{
					aboutWeb.loadUrl(SocialURL.replaceAll(" ", ""));
				}
				
			}
			if(url.equalsIgnoreCase("Web"))
			{
				if(!webUrl.startsWith("http://") && !webUrl.startsWith("https://"))
				{
					aboutWeb.loadUrl("http://"+webUrl.replaceAll(" ", ""));
				}
				else
				{
					aboutWeb.loadUrl(webUrl.replaceAll(" ", ""));
				}
				
			}
			
			aboutWeb.setWebViewClient(new MyWebViewClient());
			aboutWeb.requestFocus(View.FOCUS_DOWN);

			aboutWeb.getSettings().setJavaScriptEnabled(true);
			aboutWeb.getSettings().setPluginState(PluginState.ON);
		}


	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {


			view.loadUrl(url);
			return true;
		}

		@Override
		public void onLoadResource(WebView  view, String  url){

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.GONE);
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			aboutWebProg.setVisibility(View.GONE);
			super.onReceivedError(view, errorCode, description, failingUrl);
		}


	}    

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	void finishTo()
	{
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

}
