package com.phonethics.shoplocal;

public class MerchantDetails {

	
	 String	id					= "";
	 String	businessId 			= "";
	 String	store_name			= "";
	 String	building			= "";
	 String	street				= "";
	 String	landmark			= "";
	 String	area				= "";
	 String	pincode				= "";
	 String	city				= "";
	 String	state				= "";
	 String	country				= "";
	 String	latitude			= "";
	 String	longitude			= "";
	 String	tel_no1				= "";
	 String	tel_no2				= "";
	 String	mob_no1				= "";
	 String	mob_no2				= "";
	 String	fax_no1				= "";
	 String	fax_no2				= "";
	 String	toll_free_no1		= "";
	 String	toll_free_no2		= "";
	 String	photo				= "";
	 String fav					= "";
	 
	 
	
	public String getFav() {
		return fav;
	}
	public void setFav(String fav) {
		this.fav = fav;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getTel_no1() {
		return tel_no1;
	}
	public void setTel_no1(String tel_no1) {
		this.tel_no1 = tel_no1;
	}
	public String getTel_no2() {
		return tel_no2;
	}
	public void setTel_no2(String tel_no2) {
		this.tel_no2 = tel_no2;
	}
	public String getMob_no1() {
		return mob_no1;
	}
	public void setMob_no1(String mob_no1) {
		this.mob_no1 = mob_no1;
	}
	public String getMob_no2() {
		return mob_no2;
	}
	public void setMob_no2(String mob_no2) {
		this.mob_no2 = mob_no2;
	}
	public String getFax_no1() {
		return fax_no1;
	}
	public void setFax_no1(String fax_no1) {
		this.fax_no1 = fax_no1;
	}
	public String getFax_no2() {
		return fax_no2;
	}
	public void setFax_no2(String fax_no2) {
		this.fax_no2 = fax_no2;
	}
	public String getToll_free_no1() {
		return toll_free_no1;
	}
	public void setToll_free_no1(String toll_free_no1) {
		this.toll_free_no1 = toll_free_no1;
	}
	public String getToll_free_no2() {
		return toll_free_no2;
	}
	public void setToll_free_no2(String toll_free_no2) {
		this.toll_free_no2 = toll_free_no2;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
}
