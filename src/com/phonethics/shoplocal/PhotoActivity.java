package com.phonethics.shoplocal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.camera.CameraView;
import com.phonethics.camera.TouchImageView;

public class PhotoActivity extends SherlockActivity {

	//Button btnDoneCropping;

	/** Decalre ImageView objects */
	//ImageView getCroppedImage;
	//ImageView rotateLeft;
	ImageView rotateRight;

	//CropImageView cropView;

	/**************************FILE CROP***********************/
	int REQUEST_CODE_GALLERY;
	int REQUEST_CODE_TAKE_PICTURE;
	int REQUEST_CODE_CROP_IMAGE;

	int REQUEST_CODE_PAGER_GALLERY;
	int REQUEST_CODE_TAKE_PAGER_PICTURE;
	int REQUEST_CODE_CROP_PAGER_IMAGE;

	private  File      mFileTemp;

	Uri mImageCaptureUri;
	Uri mImagePagerCaptureUri;

	String FILE_PATH="";
	String FILE_NAME="";
	String FILE_TYPE="";

	Activity context;

	//Touch Image View

	TouchImageView touch;
	ImageView cropButton;


	int imageWidth, imageHeight;

	float[] screenDimen;
	float deviceWidth, deviceHeight;

	Bitmap bm;

	ActionBar actionBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo);

		try
		{

			context=this; 
			
			actionBar=getSupportActionBar();
			actionBar.setTitle("Crop Photo");

			rotateRight=(ImageView)findViewById(R.id.btnAPRotateRight);


			mFileTemp = new File(Environment.getExternalStorageDirectory(), "temp_photo.jpg");
			mImageCaptureUri = Uri.fromFile(mFileTemp);


			mImagePagerCaptureUri= Uri.fromFile(mFileTemp);

			FILE_PATH="/sdcard/temp_photo.jpg";


			//Touch ImageView 

			touch = (TouchImageView) findViewById(R.id.APimg);
			cropButton = (ImageView) findViewById(R.id.buttonFinishCropping);


			touch.maintainZoomAfterSetImage(true);

			Utils u = new Utils(context);

			screenDimen = u.calculateScreenDimen();
			deviceWidth = screenDimen[1]; deviceHeight = screenDimen[0];



			//touch.getLayoutParams().height = (int) deviceWidth;
			touch.getLayoutParams().height = (int) deviceWidth;
			touch.getLayoutParams().width = (int) deviceWidth;


			Bundle bundle=getIntent().getExtras();



			if(bundle!=null)
			{
				// For store gallery
				REQUEST_CODE_GALLERY=bundle.getInt("REQUEST_CODE_GALLERY", 0);
				REQUEST_CODE_TAKE_PICTURE=bundle.getInt("REQUEST_CODE_TAKE_PICTURE", 0);


				//For store logo
				REQUEST_CODE_PAGER_GALLERY=bundle.getInt("REQUEST_CODE_PAGER_GALLERY", 0);
				REQUEST_CODE_TAKE_PAGER_PICTURE=bundle.getInt("REQUEST_CODE_TAKE_PAGER_PICTURE", 0);

				/*	public static final int REQUEST_CODE_GALLERY      = 6;
				public static final int REQUEST_CODE_TAKE_PICTURE = 7;
				public static final int REQUEST_CODE_CROP_IMAGE   = 8;

				public static final int REQUEST_CODE_PAGER_GALLERY      = 9;
				public static final int REQUEST_CODE_TAKE_PAGER_PICTURE = 10;
				public static final int REQUEST_CODE_CROP_PAGER_IMAGE   = 11;*/

				if(REQUEST_CODE_TAKE_PAGER_PICTURE==10)
				{
					try {
						/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						mImagePagerCaptureUri = Uri.fromFile(mFileTemp);
						intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImagePagerCaptureUri);
						intent.putExtra("return-data", false);
						startActivityForResult(intent, REQUEST_CODE_TAKE_PAGER_PICTURE);*/
						Intent intent = new Intent(PhotoActivity.this, CameraView.class);
						mImagePagerCaptureUri = Uri.fromFile(mFileTemp);
						intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImagePagerCaptureUri);
						intent.putExtra("return-data", false);
						startActivityForResult(intent, REQUEST_CODE_TAKE_PAGER_PICTURE);
					} catch (ActivityNotFoundException e) {
						e.printStackTrace();
						Log.d("", "cannot take picture", e);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}

				if(REQUEST_CODE_TAKE_PICTURE==7)
				{
					try {
						Intent intent = new Intent(PhotoActivity.this, CameraView.class);
						mImagePagerCaptureUri = Uri.fromFile(mFileTemp);
						intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImagePagerCaptureUri);
						intent.putExtra("return-data", false);
						startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
					} catch (ActivityNotFoundException e) {
						e.printStackTrace();
						Log.d("", "cannot take picture", e);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}

				if(REQUEST_CODE_GALLERY==6 )
				{
					Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
					photoPickerIntent.setType("image/*");
					startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
				}

				if(REQUEST_CODE_PAGER_GALLERY==9)
				{
					Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
					photoPickerIntent.setType("image/*");
					startActivityForResult(photoPickerIntent, REQUEST_CODE_PAGER_GALLERY);
				}
			}

			//Cropping Listener

			cropButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					try
					{

						if(touch.getCurrentZoom() == 1.0){
							Toast.makeText(getApplicationContext(), "Please double tap on the image first", Toast.LENGTH_LONG).show();
						}
						else{

							/*	OutputStream fOut = null;
						Uri outputFileUri;*/
							touch.buildDrawingCache();
							/*bm = getBitmapFromView(touch);*/


							try
							{

								/*	public static final int REQUEST_CODE_GALLERY      = 6;
							public static final int REQUEST_CODE_TAKE_PICTURE = 7;
							public static final int REQUEST_CODE_CROP_IMAGE   = 8;

							public static final int REQUEST_CODE_PAGER_GALLERY      = 9;
							public static final int REQUEST_CODE_TAKE_PAGER_PICTURE = 10;
							public static final int REQUEST_CODE_CROP_PAGER_IMAGE   = 11;*/

								//getting image of particular resolution
								if(REQUEST_CODE_GALLERY==6)
								{
									bm = getBitmapFromView(touch,480);
								}
								if(REQUEST_CODE_TAKE_PICTURE==7)
								{
									bm = getBitmapFromView(touch,480);
								}

								if(REQUEST_CODE_PAGER_GALLERY==9)
								{
									bm = getBitmapFromView(touch,150);
								}
								if( REQUEST_CODE_TAKE_PAGER_PICTURE==10)
								{
									bm = getBitmapFromView(touch,150);
								}



								if(REQUEST_CODE_GALLERY==6 || REQUEST_CODE_PAGER_GALLERY==9)
								{
									String path = "/sdcard/";
									OutputStream fOut = null;
									File file = new File(path, FILE_NAME);
									fOut = new FileOutputStream(file);
									bm.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
									fOut.flush();
									fOut.close();
									MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
								}

								if(REQUEST_CODE_TAKE_PICTURE==7 || REQUEST_CODE_TAKE_PAGER_PICTURE==10)
								{
									String path = "/sdcard/";
									OutputStream fOut = null;
									File file = new File(path, FILE_NAME);
									fOut = new FileOutputStream(file);
									bm.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
									fOut.flush();
									fOut.close();

									MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
								}

								if(REQUEST_CODE_GALLERY==6)
								{
									setResultOfActivity(REQUEST_CODE_GALLERY, 480);
								}
								if(REQUEST_CODE_TAKE_PICTURE==7)
								{
									setResultOfActivity(REQUEST_CODE_TAKE_PICTURE, 480);
								}

								if(REQUEST_CODE_PAGER_GALLERY==9)
								{
									setResultOfActivity(REQUEST_CODE_PAGER_GALLERY, 150);
								}
								if( REQUEST_CODE_TAKE_PAGER_PICTURE==10)
								{
									setResultOfActivity(REQUEST_CODE_TAKE_PAGER_PICTURE, 150);
								}

							}catch(Exception ex)
							{
								ex.printStackTrace();
							}


							try {

								/*File root = Environment.getExternalStorageDirectory();
							File file = new File(root.getAbsolutePath()+"/saved_images");
							String filename="imagecrop.jpg";
							File f1=new File(file,filename);

							writeExternalToCache(bm, f1);*/
							} catch (Exception e) {
								Toast.makeText(getApplicationContext(), "Exception", Toast.LENGTH_LONG).show();
							}
						}
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}//click ends here
			});



			/*rotateLeft.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Log.i("Rotate", "Rotate Left");
					if(bm!=null)
					{
						bm=rotate(bm, -90);
						touch.setImageBitmap(bm);
					}
					cropView.rotateImage(-90);
				}
			});*/

			rotateRight.setOnClickListener(new android.view.View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Log.i("Rotate", "Rotate Right");
					if(bm!=null)
					{
						bm=rotate(bm, 90);
						touch.setImageBitmap(bm);
					}
				}
			});



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}//onCreate Ends Here

	public static Bitmap getBitmapFromView(View view,int size) {
		//Define a bitmap with the same size as the view
		Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
		//Bind a canvas to it
		Canvas canvas = new Canvas(returnedBitmap);
		//Get the view's background
		Drawable bgDrawable =view.getBackground();
		if (bgDrawable!=null) 
			//has background drawable, then draw it on the canvas
			bgDrawable.draw(canvas);
		else 
			//does not have background drawable, then draw white background on the canvas
			canvas.drawColor(Color.WHITE);
		// draw the view on the canvas
		view.draw(canvas);
		//return the bitmap

		returnedBitmap = Bitmap.createScaledBitmap(returnedBitmap, size, size, true);

		return returnedBitmap;
	}



	void setResultOfActivity(int resultCode,int dimension)
	{
		try
		{

			Intent intent=new Intent();
			intent.putExtra("byteArray", imageTobyteArray(FILE_PATH, dimension, dimension));
			//Toast.makeText(context, resultCode+" "+imageTobyteArray(FILE_PATH, dimension, dimension).length, Toast.LENGTH_SHORT).show();
			setResult(RESULT_OK, intent);
			finish();
			overridePendingTransition(0,R.anim.shrink_fade_out_center);
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}




	//Rotate Image

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		try
		{
			
			
			if(bm!=null)
			{
				bm=null;
			}
			
		

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static Bitmap rotate(Bitmap src, float degree) {
		// create new matrix
		Matrix matrix = new Matrix();
		// setup rotation degree
		matrix.postRotate(degree);

		// return new bitmap rotated using matrix
		return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
	}

	/*imageViewRotate.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View arg0) {

		introtateRight++;
		introtateRight %= 4; 

		Log.i("Rotate","Rotate "+(introtateRight*90));

		bitmapEffect = ImageEffects.rotate(bitmapEffect, 90);
		getCroppedImage = Bitmap.createBitmap(bitmapEffect);
		ivEffect.setImageBitmap(bitmapEffect);

		}
		});
	 */

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
	}

	void finishTo()
	{
		setResult(RESULT_CANCELED);
		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);

	}


	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finish();
		overridePendingTransition(0,R.anim.shrink_fade_out_center);
		return true;
	}

	//Convert image into byte array
	static 	byte[] imageTobyteArray(String path,int width,int height)
	{	byte[] b = null ;
	if(!path.equals("") || path.length()!=0)
	{

		try
		{

			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			options.inDither=true;//optional
			options.inPreferredConfig=Bitmap.Config.RGB_565;//optional

			Bitmap bm = BitmapFactory.decodeFile(path,options);

			options.inSampleSize = calculateInSampleSize(options, width, height);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;

			bm=BitmapFactory.decodeFile(path,options);


			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			bm.compress(Bitmap.CompressFormat.JPEG, 80, baos); //bm is the bitmap object   
			b= baos.toByteArray();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	return b;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		/*if (height > reqHeight || width > reqWidth) {

				// Calculate ratios of height and width to requested height and width
				final int heightRatio = Math.round((float) height / (float) reqHeight);
				final int widthRatio = Math.round((float) width / (float) reqWidth);

				// Choose the smallest ratio as inSampleSize value, this will guarantee
				// a final image with both dimensions larger than or equal to the
				// requested height and width.
				inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
			}*/

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}



	//Down scaling bitmap to get Thumbnail
	public  Bitmap getThumbnail(Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		InputStream input = context.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;//optional
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;//optional
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;//optional


		input = context.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);

		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode != RESULT_OK) {

			finishTo();

			return;
		}

		//Image For gallery grid
		if(requestCode==REQUEST_CODE_GALLERY)
		{
			try {
				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				try
				{
					bm=getThumbnail(data.getData(),(int)deviceWidth);

					int height = bm.getHeight();
					int width = bm.getWidth();

					setZoomToImageView(height,width);

					touch.setImageBitmap(getThumbnail(data.getData(),(int)deviceWidth));

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}


			} catch (Exception e) {

				Log.e("", "Error while creating temp file", e);
			}

		}
		//Image For gallery grid
		if(requestCode==REQUEST_CODE_TAKE_PICTURE)
		{
			FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";
			try
			{
				/*		bm=getThumbnail(mImagePagerCaptureUri, (int)deviceWidth);
				touch.setImageBitmap(getThumbnail(mImagePagerCaptureUri, (int)deviceWidth));*/

				bm=getThumbnail(mImagePagerCaptureUri, (int)deviceWidth);
				bm=getRotatedImage(FILE_PATH, bm);

				int height = bm.getHeight();
				int width = bm.getWidth();

				setZoomToImageView(height,width);

				touch.setImageBitmap(bm);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		//Image For Store Logo
		if(requestCode==REQUEST_CODE_PAGER_GALLERY)
		{
			try {

				FILE_TYPE="image/jpeg";
				FILE_NAME="temp_photo.jpg";
				FILE_PATH="/sdcard/temp_photo.jpg";

				bm=getThumbnail(data.getData(),(int)deviceWidth);

				int height = bm.getHeight();
				int width = bm.getWidth();

				setZoomToImageView(height,width);

				touch.setImageBitmap(getThumbnail(data.getData(),(int)deviceWidth));

			} catch (Exception e) {

				Log.e("", "Error while creating temp file", e);
			}
		}
		//Image For Store Logo
		if(requestCode==REQUEST_CODE_TAKE_PAGER_PICTURE)
		{
			FILE_TYPE="image/jpeg";
			FILE_NAME="temp_photo.jpg";
			FILE_PATH="/sdcard/temp_photo.jpg";

			try
			{
				bm=getThumbnail(mImagePagerCaptureUri, (int)deviceWidth);
				bm=getRotatedImage(FILE_PATH, bm);

				int height = bm.getHeight();
				int width = bm.getWidth();

				setZoomToImageView(height,width);

				touch.setImageBitmap(bm);

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}



	}

	void setZoomToImageView(int height,int width)
	{
		float zoom = 0.0f;

		if(height > width){
			zoom = (float) height/width;
		}
		else
			zoom = (float) width/height;


		touch.setMinZoom(zoom);

	}


	Bitmap getRotatedImage(String path,Bitmap bmp)
	{
		Matrix matrix=new Matrix();

		ExifInterface exif = null;
		try {
			exif = new ExifInterface(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String orientstring = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		int orientation = orientstring != null ? Integer.parseInt(orientstring) : ExifInterface.ORIENTATION_NORMAL;
		int rotateangle = 0;
		if(orientation == ExifInterface.ORIENTATION_ROTATE_90) 
			rotateangle = 90;
		if(orientation == ExifInterface.ORIENTATION_ROTATE_180) 
			rotateangle = 180;
		if(orientation == ExifInterface.ORIENTATION_ROTATE_270) 
			rotateangle = 270;

		//		touch.setScaleType(ScaleType.CENTER_CROP);   //required
		//		matrix.setRotate(rotateangle);

		touch.setImageMatrix(matrix);


		/*if(orientation == 0) 
		{
			exif.setAttribute(ExifInterface.TAG_ORIENTATION,  ""+ExifInterface.ORIENTATION_NORMAL);
		}
		if(orientation == ExifInterface.ORIENTATION_ROTATE_90) 
		{
			exif.setAttribute(ExifInterface.TAG_ORIENTATION,  ""+ExifInterface.ORIENTATION_ROTATE_90);
		}
		if(orientation == ExifInterface.ORIENTATION_ROTATE_180)
		{
			exif.setAttribute(ExifInterface.TAG_ORIENTATION,  ""+ExifInterface.ORIENTATION_ROTATE_180);
		}
		if(orientation == ExifInterface.ORIENTATION_ROTATE_270)
		{
			exif.setAttribute(ExifInterface.TAG_ORIENTATION,  ""+ExifInterface.ORIENTATION_ROTATE_270);
		}*/

		matrix.setRotate(rotateangle);

		Bitmap newBit = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);


		return newBit;

		/*captureImage.setImageBitmap(newBit);*/
	}

}
