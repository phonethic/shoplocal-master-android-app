package com.phonethics.shoplocal;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class ShowPlaceMessage extends SherlockActivity {

	Activity context;
	ActionBar actionBar;
	ArrayList<String>STOREID=new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_place_message);
		context=this;
		actionBar=getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
		actionBar.show();

		Bundle b=getIntent().getExtras();
		Button btnPost=(Button)findViewById(R.id.btnPost);
		Button btnSkip=(Button)findViewById(R.id.btnSkip);
		if(b!=null)
		{
			try
			{
				String storeid=b.getString("STOREID");
				STOREID.add(storeid);

				btnPost.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(context, CreateBroadCast.class);
						intent.putStringArrayListExtra("SELECTED_STORE_ID",STOREID);
						startActivity(intent);
						overridePendingTransition(R.anim.activity_push_up_in, R.anim.push_up_out);
						finish();
					}
				});

				btnSkip.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(context,MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Intent intent=new Intent(context,MerchantsHomeGrid.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
		return true;

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(context,MerchantsHomeGrid.class);
		startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);
	}



}
