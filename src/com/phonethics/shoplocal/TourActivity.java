package com.phonethics.shoplocal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.phonethics.networkcall.MerchantLoginReceiver;
import com.phonethics.networkcall.MerchantLoginReceiver.LoginReceiver;
import com.phonethics.networkcall.MerchantLoginService;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.BadTokenException;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TourActivity extends Activity implements LoginReceiver {

	TextView tourSubTitle;
	TextView tourSubTitle2;
	Button signUpCustomer;
	ImageView staticImage;
	TextView txtlater;
	TextView txtMerchantSignUp;
	Activity context;
	SessionManager session;

	Runnable runnable_anim;
	int count =1;

	LinearLayout signUpButtonLayout;

	ArrayList<String> subs=new ArrayList<String>();
	Animation fade;
	Animation fadeOut;

	ImageView staticLogo;

	Typeface tf;
	boolean runnableDone = false;
	String PREF_NAME="TOUR_PREF";
	String TOUR_KEY="TOUR_VALUE";

	Button signUpFb;

	NetworkCheck network;
	ProgressBar		pBarFb;
	private boolean pendingPublishReauthorization = false;
	private static final List<String> PERMISSIONS = Arrays.asList("email","user_birthday","user_location");

	String  userIdFB, accessTokenFB;
	String 			fbUserid="";
	String 			fbAccessToken="";

	static String LOGIN_PATH;
	static String LOGIN_URL;

	static String API_HEADER;
	static String API_VALUE;

	public MerchantLoginReceiver mLoginRecevier;
	String USER_ID,AUTH_ID;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour);

		try
		{
			context=this;
			session=new SessionManager(context);
			network=new NetworkCheck(context);
			fade=AnimationUtils.loadAnimation(context, R.anim.fade_in);
			fadeOut=AnimationUtils.loadAnimation(context, R.anim.fade_out);
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");


			tourSubTitle=(TextView)findViewById(R.id.tourSubTitle);
			tourSubTitle2=(TextView)findViewById(R.id.tourSubTitle2);
			signUpButtonLayout=(LinearLayout)findViewById(R.id.signUpButtonLayout);
			txtMerchantSignUp=(TextView)findViewById(R.id.txtMerchantSignUp);

			staticLogo=(ImageView)findViewById(R.id.staticLogo);
			staticImage=(ImageView)findViewById(R.id.staticImage);

			signUpCustomer=(Button)findViewById(R.id.signUpCustomer);

			txtlater=(TextView)findViewById(R.id.txtlater);

			signUpFb = (Button) findViewById(R.id.signUpFb);
			pBarFb =(ProgressBar)findViewById(R.id.pBarFb);

			signUpFb.setTypeface(tf);
			signUpCustomer.setTypeface(tf);

			signUpCustomer.setText("Sign up with shoplocal");

			fade.setAnimationListener(new SignUpLayout());
			fadeOut.setAnimationListener(new StaticImage());

			tourSubTitle.setTypeface(tf);
			tourSubTitle2.setTypeface(tf);
			txtlater.setTypeface(tf);
			signUpCustomer.setTypeface(tf);

			//			ValueAnimator colorAnim = ObjectAnimator.ofInt(tourSubTitle, "textColor", /*Red*/0xFFFF8080, /*Blue*/0xFF8080FF);
			//			colorAnim.setDuration(3000);
			//			colorAeventnim.setEvaluator(new ArgbEvaluator());
			//			colorAnim.setRepeatCount(ValueAnimator.INFINITE);
			//			colorAnim.setRepeatMode(ValueAnimator.REVERSE);
			//			colorAnim.start();

			subs.add(getResources().getString(R.string.subTitle1));
			subs.add(getResources().getString(R.string.subTitle2));
			subs.add(getResources().getString(R.string.subTitle3));
			subs.add(getResources().getString(R.string.subTitle4));
			subs.add(getResources().getString(R.string.subTitle5));
			subs.add(getResources().getString(R.string.subTitle6));
			subs.add(getResources().getString(R.string.subTitle7));
			subs.add(getResources().getString(R.string.subTitle8));
			subs.add(getResources().getString(R.string.subTitle9));
			subs.add(getResources().getString(R.string.subTitle10));
			subs.add(getResources().getString(R.string.subTitle11));
			subs.add(getResources().getString(R.string.subTitle12));
			subs.add(getResources().getString(R.string.subTitle22));
			//			subs.add(getResources().getString(R.string.subTitle13));
			//			subs.add(getResources().getString(R.string.subTitle14));
			//			subs.add(getResources().getString(R.string.subTitle15));
			//			subs.add(getResources().getString(R.string.subTitle16));
			//			subs.add(getResources().getString(R.string.subTitle17));
			//			subs.add(getResources().getString(R.string.subTitle18));
			//			subs.add(getResources().getString(R.string.subTitle19));
			//			subs.add(getResources().getString(R.string.subTitle20));
			//			subs.add(getResources().getString(R.string.subTitle21));
			//			//			subs.add(getResources().getString(R.string.subTitle5));

			//Defining URL's
			LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api);
			LOGIN_PATH=getResources().getString(R.string.login);

			//API KEY'S
			API_HEADER=getResources().getString(R.string.api_header);
			API_VALUE=getResources().getString(R.string.api_value);

			//Initializing Login Service Reciver
			mLoginRecevier=new MerchantLoginReceiver(new Handler());
			mLoginRecevier.setReceiver(this);


			signUpCustomer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					EventTracker.logEvent("Tour_SignUp", false);

					setTourPrefs();
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					intent.putExtra("facebookLogin", false);
					intent.putExtra("isFromTour", true);
					startActivityForResult(intent, 2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			});
			txtlater.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) { 
					EventTracker.logEvent("Tour_Later", false);

					setTourPrefs();
					Intent intent=new Intent(context,LocationActivity.class);
					startActivity(intent);;
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					finish();
				}
			});

			tourSubTitle2.setText(getResources().getString(R.string.subTitle1));

			//First text animation

			Animation fadeIn = new AlphaAnimation(0, 1);
			fadeIn.setInterpolator(new AccelerateInterpolator()); 
			fadeIn.setDuration(1000);
			tourSubTitle2.startAnimation(fadeIn);

			fadeIn.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					//tourSubTitle2.startAnimation(fadeOut);

					Animation fadeOut = new AlphaAnimation(1, 0);
					fadeOut.setInterpolator(new AccelerateInterpolator()); 
					fadeOut.setDuration(600);
					tourSubTitle2.startAnimation(fadeOut);

				}
			});

			startImageAnimation(tourSubTitle, tourSubTitle2, subs);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		txtMerchantSignUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				EventTracker.logEvent("Tour_Sell", false);

				setTourPrefs();
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});


		signUpFb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(network.isNetworkAvailable())
				{
					//login_facebook();
					EventTracker.logEvent("Tour_LoginFB", false);

					setTourPrefs();
					Intent intent = new Intent(context, LoginSignUpCustomer.class);
					intent.putExtra("facebookLogin", true);
					startActivityForResult(intent,2);
					//finish();
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}
		});

		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());

//		//facebook adv. event
//
//		AppEventsLogger.activateApp(getApplicationContext(), getResources().getString(R.string.fb_appid));
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}

	void setTourPrefs()
	{
		SharedPreferences prefs=context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putBoolean(TOUR_KEY, true);
		editor.commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			if(session.isLoggedInCustomer())
			{
				setTourPrefs();
				Intent intent=new Intent(context,LocationActivity.class);
				startActivity(intent);;
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();
			}


		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{
				EventTracker.logEvent("Tour_SellSuccess", false);

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}


		//Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);

	}

	void startImageAnimation(final TextView txt_1,
			final TextView txt_2,
			final ArrayList<String> subTitles){



		final Animation fade_in_anim=AnimationUtils.loadAnimation(context, R.anim.word_fade_in);

		final Animation fade_in_animLast=AnimationUtils.loadAnimation(context, R.anim.fade_in);
		final Animation fade_out_animFirst=AnimationUtils.loadAnimation(context, R.anim.fade_out);

		final Animation fade_out_anim=AnimationUtils.loadAnimation(context, R.anim.word_fade_out);

		final Handler handler_anim = new Handler();
		runnable_anim = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try{



					txt_2.setText(subTitles.get(count));

					txt_2.startAnimation(fade_in_anim);

					//					fade_in_anim.setFillAfter(true);

					fade_out_anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub

						}
					});

					fade_in_anim.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

							txt_1.startAnimation(fade_out_anim);

							Log.d("=====", "Shoplocal Animation start" +count);
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							Log.d("=====", "Shoplocal Animation end" +count);
							//							fade_out_anim.setFillAfter(true);
							txt_1.setText(subTitles.get(count));
							count++;
							if(count==subTitles.size()-1){
								//								count=0;
								//								showToast("Animation End");

								runnableDone = true;
								handler_anim.removeCallbacks(runnable_anim);

								staticImage.startAnimation(fadeOut);


								if(runnableDone){

									txt_2.setVisibility(View.GONE);



									Animation fadeOutManual = new AlphaAnimation(1, 0);
									//fadeOutManual.setInterpolator(new AccelerateInterpolator()); 
									fadeOutManual.setDuration(400);
									txt_1.startAnimation(fadeOutManual);

									fadeOutManual.setAnimationListener(new AnimationListener() {

										@Override
										public void onAnimationStart(Animation animation) {
											// TODO Auto-generated method stub

										}

										@Override
										public void onAnimationRepeat(Animation animation) {
											// TODO Auto-generated method stub

										}

										@Override
										public void onAnimationEnd(Animation animation) {
											// TODO Auto-generated method stub

											txt_1.setText("");
											txt_2.setVisibility(View.VISIBLE);
											txt_2.setText(subTitles.get(subTitles.size()-1));


											Animation fadeInManual = new AlphaAnimation(0, 1);

											fadeInManual.setDuration(800);
											txt_2.startAnimation(fadeInManual);

										}
									});

								}

							}
						}
					});

					handler_anim.postDelayed(runnable_anim, 300);
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		};
		handler_anim.postDelayed(runnable_anim,1600);



	}

	class StaticImage implements AnimationListener
	{

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			staticImage.setVisibility(View.GONE);
			signUpButtonLayout.setVisibility(View.VISIBLE);
			staticLogo.setVisibility(View.VISIBLE);
			txtMerchantSignUp.setVisibility(View.VISIBLE);
			txtMerchantSignUp.startAnimation(fade);
			staticLogo.startAnimation(fade);
			signUpButtonLayout.startAnimation(fade);
			//			fadeOut.setFillAfter(true);

			Animation fadeOutManual = new AlphaAnimation(1, 0);

			fadeOutManual.setDuration(800);
			fadeOutManual.setStartOffset(200);
			tourSubTitle2.startAnimation(fadeOutManual);
			fadeOutManual.setFillAfter(true);

			Log.d("LastText","LastText " + "Hi " + tourSubTitle.getText().toString());
			Log.d("LastText","LastText " + "Bye " + tourSubTitle2.getText().toString());
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub

		}

	}

	class SignUpLayout implements AnimationListener
	{

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub


		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub

		}

	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}


	public void login_facebook(){


		try
		{
			pBarFb.setVisibility(View.VISIBLE);
			//showToast("Please Wait");

			//List<String> permissions = session.getPermissions();


			Session.openActiveSession(context, true, new Session.StatusCallback() {

				@Override
				public void call(final Session session, SessionState state, Exception exception) {
					// TODO Auto-generated method stub
					if(session.isOpened())
					{
						showToast(getResources().getString(R.string.fbConnecting));
						List<String> permissions = session.getPermissions();

						if (!isSubsetOf(PERMISSIONS, permissions)) {
							pendingPublishReauthorization = true;
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, PERMISSIONS);
							session.requestNewPublishPermissions(newPermissionsRequest);
						}

						Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								if(user!=null)
								{

									try{
										//Log.d("", "Fb responcse == "+response.toString());
										//Log.d("", "FbUser >> "+ user.toString());
										fbUserid=user.getId();
										fbAccessToken=session.getAccessToken();
										Log.i("User Id", "FbName : "+fbAccessToken);
										Log.i("User Id ", "FbId : "+fbUserid);

										pBarFb.setVisibility(View.INVISIBLE);



										userIdFB = user.getId();

										accessTokenFB = session.getAccessToken();

										loginCustomer();

									}catch(Exception ex){
										ex.printStackTrace();
									}


								}

								else{
									EventTracker.logEvent("Tour_LoginFailedFB", false);
									pBarFb.setVisibility(View.INVISIBLE);
									//									Log.i("ELSE", "ELSE");
									showToast("wrong");
								}
							}


						});
						//session.getAccessToken();
						//Toast.makeText(context, "Session AccessToken : "+session.getAccessToken(), Toast.LENGTH_LONG).show();
					}

					if(session.isClosed()){
						EventTracker.logEvent("Tour_LoginFailedFB", false);
						//Toast.makeText(context, "Closed", 0).show();
					}

				}


			});}catch(NullPointerException npx)
			{
				npx.printStackTrace();
			}
		catch(BadTokenException bdx)
		{
			bdx.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	void loginCustomer()
	{

		if(network.isNetworkAvailable())
		{
			Intent intent=new Intent(context, MerchantLoginService.class);
			intent.putExtra("merchantLogin", mLoginRecevier);
			intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("facebook_user_id", fbUserid);
			intent.putExtra("facebook_access_token", fbAccessToken);
			intent.putExtra("isCustomer",true);
			intent.putExtra("fromFb",true);
			context.startService(intent);
			//progLoginCustomer.setVisibility(View.VISIBLE);

		}
		else
		{
			Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onReceiveLoginResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub


		// TODO Auto-generated method stub
		try
		{
			String auth_id=resultData.getString("login_authCode");
			String loginstatus=resultData.getString("loginstatus");
			String loginid=resultData.getString("loginid");

			Log.i("Data From Server-------", "Server -----loginstatus "+resultData.getString("loginstatus"));
			Log.i("Data From Server-------", "Server -----loginid "+loginid);
			Log.i("Data From Server-------", "Server -----loginauth_id "+auth_id);

			USER_ID=loginid;
			AUTH_ID=auth_id;
			//progLoginCustomer.setVisibility(View.INVISIBLE);
			if(loginstatus.equalsIgnoreCase("true"))
			{

				session.createLoginSessionCustomer(fbUserid, fbAccessToken, USER_ID, AUTH_ID,true);

				//				Intent intent=new Intent(this, NewsActivity.class);
				//				startActivity(intent);

				setTourPrefs();

				Intent intent=new Intent(this, LocationActivity.class);
				startActivity(intent);

				finish();

				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				EventTracker.logEvent("Tour_LoginSuccessFB", false);

			}
			else 
			{
				Toast.makeText(context,auth_id, Toast.LENGTH_SHORT).show();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}


}
