package com.phonethics.shoplocal;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.urbanairship.push.PushManager;

public class MerchantsHomeGrid extends SherlockActivity implements GetAllStore {

	GridView grid_home;
	ArrayList<String>gridItems;
	ArrayList<Integer>gridItemIcons;
	Activity context;
	ActionBar actionBar;

	SessionManager session;

	SharedPreferences prefs;
	Editor editor;

	ImageView newPost_Grid;
	ImageView viewPost_Grid;
	ImageView editStore_Grid;
	ImageView addStore_Grid;
	ImageView gallery_Grid;
	ImageView logout_Grid;
	ImageView report_Grid;
	ImageView facebook_Grid;
	ImageView twitter_Grid;
	ImageView sure_shop;

	TextView addstore_grid_lable;
	TextView editstore_grid_lable;
	TextView manage_grid_lable;

	TextView facebook_merchant_grid_lable;
	TextView twitter_merchant_grid_lable;
	TextView logout_merchant_grid_lable;

	TextView sure_merchant_grid_lable;





	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);


	Typeface tf;


	ArrayList<String> STOREID=new ArrayList<String>();
	ArrayList<String> STORE_NAME=new ArrayList<String>();
	ArrayList<String>PHOTO=new ArrayList<String>();
	ArrayList<String> STORE_DESC=new ArrayList<String>();

	DBUtil dbUtil;

	boolean isPlaceRefreshRequired=false;

	//All Stores
	GetAllStores_Of_Perticular_User_Receiver mAllStores;



	static String USER_ID="";
	static String AUTH_ID="";

	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	static String STORE_URL;
	static String SOTRES_PATH;

	static String API_HEADER;
	static String API_VALUE;

	NetworkCheck isnetConnected;
	
	//Fetch Dialog
	AlertDialog.Builder fetchDialogBuilder;
	AlertDialog fetchDialog;



	//Retry Dialog
	AlertDialog.Builder retryDialogBuilder;
	AlertDialog retryDialog;
	
	LocalNotification localNotification;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		/*		setContentView(R.layout.activity_merchants_home_grid);*/
		setContentView(R.layout.merchantgridstatic);
		actionBar=getSupportActionBar();

		actionBar.setTitle("Business Dashboard");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		context=this;
		
		localNotification=new LocalNotification(getApplicationContext());
		
		dbUtil=new DBUtil(context);
		
		EventTracker.startLocalyticsSession(context);

		//getting font from asset directory.
		tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");



		prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
		editor=prefs.edit();



		session=new SessionManager(context);

		isnetConnected=new NetworkCheck(context);

		//Store API
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		SOTRES_PATH=getResources().getString(R.string.allStores);

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//All Store Service
		mAllStores=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
		mAllStores.setReceiver(this);
		

		//Ui Dialogs

		fetchDialogBuilder = new AlertDialog.Builder(context);
		fetchDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));
		
		//Retry Dialog
		retryDialogBuilder = new AlertDialog.Builder(context);
		retryDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));
	



		try
		{
			HashMap<String,String>user_details=session.getUserDetails();
			USER_ID=user_details.get(KEY_USER_ID).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID).toString();


			SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
			isPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);

			Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+isPlaceRefreshRequired);
			
			fetchDialogBuilder
			.setMessage(getResources().getString(R.string.fetchDataMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false);
			
			fetchDialog = fetchDialogBuilder.create();




			


			retryDialogBuilder
			.setMessage(getResources().getString(R.string.retryDataMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Retry",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					/*	dialog.dismiss();*/
					try
					{
						loadAllStoreForUser();
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					/*	dialog.dismiss();*/
					try
					{
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			
			retryDialog = retryDialogBuilder.create();

			if(!isPlaceRefreshRequired)
			{
				STOREID=dbUtil.getAllPlacesIDs();
				STORE_NAME=dbUtil.getAllPlacesNames();
				PHOTO=dbUtil.getAllPlacesPhotoUrl();
				STORE_DESC=dbUtil.getAllPlacesDesc();
			}
			else
			{
				loadAllStoreForUser();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}




		/*grid_home=(GridView)findViewById(R.id.merchantHomeGrid);*/
		gridItems=new ArrayList<String>();
		gridItemIcons=new ArrayList<Integer>();

		gridItems.add("New Post");
		gridItems.add("View Posts");
		gridItems.add("Add Store");
		gridItems.add("Edit Store");
		gridItems.add("Manage Gallery");
		gridItems.add("All Store");


		gridItemIcons.add(R.drawable.new_post);
		gridItemIcons.add(R.drawable.view_post);
		gridItemIcons.add(R.drawable.add_store);
		gridItemIcons.add(R.drawable.edit_store);
		gridItemIcons.add(R.drawable.manage_gallery);
		gridItemIcons.add(R.drawable.logout);

		//ImageView 
		newPost_Grid=(ImageView)findViewById(R.id.newPost_Grid);
		viewPost_Grid=(ImageView)findViewById(R.id.viewPost_Grid);
		editStore_Grid=(ImageView)findViewById(R.id.editStore_Grid);
		addStore_Grid=(ImageView)findViewById(R.id.addStore_Grid);
		gallery_Grid=(ImageView)findViewById(R.id.gallery_Grid);
		logout_Grid=(ImageView)findViewById(R.id.logout_Grid);
		report_Grid = (ImageView)findViewById(R.id.report_Grid);
		facebook_Grid= (ImageView)findViewById(R.id.facebook_Grid);
		twitter_Grid= (ImageView)findViewById(R.id.twitter_Grid);
		sure_shop= (ImageView)findViewById(R.id.sure_shop);
		sure_shop.setVisibility(View.VISIBLE);

		addstore_grid_lable=(TextView)findViewById(R.id.addstore_grid_lable);
		editstore_grid_lable=(TextView)findViewById(R.id.editstore_grid_lable);
		manage_grid_lable=(TextView)findViewById(R.id.manage_grid_lable);

		facebook_merchant_grid_lable=(TextView)findViewById(R.id.facebook_merchant_grid_lable);
		twitter_merchant_grid_lable=(TextView)findViewById(R.id.twitter_merchant_grid_lable);
		logout_merchant_grid_lable=(TextView)findViewById(R.id.logout_merchant_grid_lable);

		logout_merchant_grid_lable.setText("Logout");
		
		sure_merchant_grid_lable=(TextView)findViewById(R.id.sure_merchant_grid_lable);

		addstore_grid_lable.setTypeface(tf);
		editstore_grid_lable.setTypeface(tf);
		manage_grid_lable.setTypeface(tf);

		facebook_merchant_grid_lable.setTypeface(tf);
		twitter_merchant_grid_lable.setTypeface(tf);
		logout_merchant_grid_lable.setTypeface(tf);


		addstore_grid_lable.setTextColor(Color.WHITE);
		editstore_grid_lable.setTextColor(Color.WHITE);
		manage_grid_lable.setTextColor(Color.WHITE);
		facebook_merchant_grid_lable.setTextColor(Color.WHITE);
		twitter_merchant_grid_lable.setTextColor(Color.WHITE);
		logout_merchant_grid_lable.setTextColor(Color.WHITE);

		


		if(session.isLoggedIn())
		{
			logout_Grid.setImageResource(R.drawable.logout);
			logout_merchant_grid_lable.setText(getResources().getString(R.string.logout_grid));
		}
		else
		{
			logout_Grid.setImageResource(R.drawable.login);
			logout_merchant_grid_lable.setText(getResources().getString(R.string.login_grid));
		}

		//		Temporarily Hidden
		animate(newPost_Grid).setDuration(200);
		newPost_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(newPost_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(newPost_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		//		Temporarily Hidden
		newPost_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Create Broadcast
				Intent intent=new Intent(context,PlaceChooser.class);
				intent.putExtra("choiceMode", 2);
				intent.putExtra("viewPost", 0);
				intent.putExtra("AddStore",0);
				intent.putExtra("manageGallery",0);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		//		Temporarily Hidden
		animate(viewPost_Grid).setDuration(200);
		viewPost_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(viewPost_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(viewPost_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		//		Temporarily Hidden
		viewPost_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//View Post
				Intent intent=new Intent(context,PlaceChooser.class);
				intent.putExtra("choiceMode", 1);
				intent.putExtra("viewPost",1);
				intent.putExtra("AddStore",0);
				intent.putExtra("manageGallery",0);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		/*if(STOREID.size()==0)
		{
			editStore_Grid.setVisibility(View.GONE);
			gallery_Grid.setVisibility(View.GONE);
			
		}*/
			animate(addStore_Grid).setDuration(200);
			addStore_Grid.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					// TODO Auto-generated method stub
					/*vibrate.vibrate(50);*/
					if(arg1.getAction()==MotionEvent.ACTION_DOWN)
					{
						animate(addStore_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
					}
					else if(arg1.getAction()==MotionEvent.ACTION_UP)
					{
						animate(addStore_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
					}
					return false;
				}
			});
			addStore_Grid.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Add Store
//					EventTracker.logEvent("Tab_AddStore",false );
					
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				}
			});



		animate(editStore_Grid).setDuration(200);
		editStore_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(editStore_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(editStore_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		editStore_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Edit Store

				//Add Store
				if(STOREID.size()==0)
				{
					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.noStoreMessage))
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							
							try
							{
								Intent intent=new Intent(context, EditStore.class);
								intent.putExtra("isNew", true);
								startActivity(intent);
								context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								context.finish();
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}

						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int arg1) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();
					
				}
				//EditStore
				else if(STOREID.size()==1)
				{
					EventTracker.logEvent("Tab_EditStore",false );	
					
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isSplitLogin",true);
					intent.putExtra("STORE_NAME", STORE_NAME.get(0));
					intent.putExtra("STORE_LOGO", PHOTO.get(0));
					intent.putExtra("STORE_ID", STOREID.get(0));
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					finish();
				}
				//Place Chooser
				else
				{
					EventTracker.logEvent("Tab_EditStore",false );	
					
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost",2);
					intent.putExtra("AddStore",0);
					intent.putExtra("manageGallery",0);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});

		animate(facebook_Grid).setDuration(200);
		facebook_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(facebook_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(facebook_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		facebook_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				EventTracker.logEvent("Tab_MerchantFacebook",false );	
				
				Intent intent=new Intent(context,FbTwitter.class);
				intent.putExtra("isMerchant", true);
				intent.putExtra("isFb", true);
				intent.putExtra("fb", "facebook");
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});


		animate(twitter_Grid).setDuration(200);
		twitter_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(twitter_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(twitter_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		twitter_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				EventTracker.logEvent("Tab_MerchantTwitter",false );	
				
				Intent intent=new Intent(context,FbTwitter.class);
				intent.putExtra("isMerchant", true);
				intent.putExtra("isFb", false);
				intent.putExtra("twitter", "twitter");
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});



		animate(gallery_Grid).setDuration(200);
		gallery_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(gallery_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(gallery_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		gallery_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Gellery i.e EditStore

				//Add Store
				if(STOREID.size()==0)
				{
					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.noStoreMessage))
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							
							try
							{
								Intent intent=new Intent(context, EditStore.class);
								intent.putExtra("isNew", true);
								startActivity(intent);
								context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								context.finish();
								
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}

						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int arg1) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();
					
				}
				//Gallery
				else if(STOREID.size()==1)
				{
					EventTracker.logEvent("Tab_ManageGallery",false );	
					
					Intent intent=new Intent(context,EditStore.class);
					intent.putExtra("manageGallery",1);
					intent.putExtra("STORE_NAME", STORE_NAME.get(0));
					intent.putExtra("STORE_LOGO", PHOTO.get(0));
					intent.putExtra("STORE_ID", STOREID.get(0));
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					finish();
				}
				//Place chooser
				else
				{
					EventTracker.logEvent("Tab_ManageGallery",false );	
					
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost",2);
					intent.putExtra("AddStore",0);
					intent.putExtra("manageGallery",1);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});

		animate(logout_Grid).setDuration(200);
		logout_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(logout_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(logout_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		logout_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//clear User type
				if(session.isLoggedIn())
				{
					AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
					alertDialogBuilder3.setTitle("Shoplocal");
					alertDialogBuilder3
					.setMessage(getResources().getString(R.string.logoutmessage))
					.setIcon(R.drawable.ic_launcher)
					.setCancelable(false)
					.setPositiveButton("Confirm",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
							editor.clear();
							//Logout user
							session.logoutUser();
							logout_Grid.setImageResource(R.drawable.login);


							String active_place;
							active_place=dbUtil.getActiveAreaID();
							//		Log.i("Active Area Count", "Active Area Count in Drawer");
							Log.i("Active Area Count", "Active Area Count "+active_place);
							
							localNotification.clearPlacePrefs();
							localNotification.clearPostPrefs();
							
							
							if(active_place.length()==0)
							{
								Intent intent=new Intent(context,LocationActivity.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
							}
							else
							{
								Intent intent=new Intent(context,NewsFeedActivity.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
							}



						}
					})
					.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

							dialog.dismiss();
						}
					})
					;

					AlertDialog alertDialog3 = alertDialogBuilder3.create();

					alertDialog3.show();

				}else
				{
					Intent intent=new Intent(context,SplitLoginSignUp.class);
					startActivity(intent);
					finish();

				}
				/*	finish();*/

			}
		});

		//		Temporarily Hidden 
		animate(report_Grid).setDuration(200);
		report_Grid.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub

				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(report_Grid).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(report_Grid).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		//		Temporarily Hidden
		report_Grid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(context,PlaceChooser.class);
				intent.putExtra("choiceMode", 1);
				intent.putExtra("report",1);
				intent.putExtra("viewPost",-1);
				intent.putExtra("AddStore",-1);
				intent.putExtra("manageGallery",-1);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});

		//		Temporarily Hidden
		animate(sure_shop).setDuration(200);
		sure_shop.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub

				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(sure_shop).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(sure_shop).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});
		//		Temporarily Hidden
		sure_shop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(STOREID.size()==0)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				else if(STOREID.size()==1)
				{
					Intent intent=new Intent(context,SureShop.class);
					intent.putExtra("SureShop",1);
					intent.putExtra("STORE_NAME", STORE_NAME.get(0));
					intent.putExtra("STORE_LOGO", PHOTO.get(0));
					intent.putExtra("STORE_ID", STOREID.get(0));
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					finish();
				}
				else
				{
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("report",0);
					intent.putExtra("sure_shop",1);
					intent.putExtra("viewPost",-1);
					intent.putExtra("AddStore",-1);
					intent.putExtra("manageGallery",-1);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		});


		/*grid_home.setAdapter(adapter);*/

		/*grid_home.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if(position==0)
				{
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 2);
					intent.putExtra("viewPost", 0);
					startActivity(intent);
					finish();
				}
				else if(position==1)
				{
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost",1);
					startActivity(intent);
					finish();
				}

				else if(position!=5)
				{
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost", 2);
					startActivity(intent);
					finish();
				}
				else if(position ==5)
				{
					session.logoutUser();
					finish();
				}

				switch (position) {
				case 0:
				{
					//Create Broadcast
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 2);
					intent.putExtra("viewPost", 0);
					intent.putExtra("AddStore",0);
					startActivity(intent);
					finish();
					break;
				}
				case 1:
				{
					//View Post
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost",1);
					intent.putExtra("AddStore",0);
					startActivity(intent);
					finish();
					break;
				}
				case 2:
				{
					//Add Store
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					finish();
					break;
				}
				case 3:
				{
					//Edit Store
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost",2);
					intent.putExtra("AddStore",0);
					startActivity(intent);
					finish();
					break;
				}
				case 4:
				{
					//Gellery i.e EditStore
					Intent intent=new Intent(context,PlaceChooser.class);
					intent.putExtra("choiceMode", 1);
					intent.putExtra("viewPost",2);
					intent.putExtra("AddStore",0);
					startActivity(intent);
					finish();
					break;
				}
				case 5:
				{
					//clear User type
					editor.clear();
					//Logout user
					session.logoutUser();
					finish();
					break;
				}
				default:
					break;
				}
			}

		});*/

	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		return true;
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		finishTo();
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*if(slide_menu.isMenuShowing())
		{
			slide_menu.toggle(true);
		}
		else
		{
		this.finish();
		}*/
		finishTo();



	}

	void finishTo()
	{
		Intent intent=new Intent(context,MerchantTalkHome.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

	void loadAllStoreForUser()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, GetAllStores_Of_Perticular_User.class);
			intent.putExtra("getAllStores",mAllStores);
			intent.putExtra("URL", STORE_URL+SOTRES_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			context.startService(intent);
			if(!fetchDialog.isShowing())
			{
				fetchDialog.show();
			}
		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
		/*relBroadCastProgress.setVisibility(ViewGroup.VISIBLE);*/
	}

	static class Sercv
	{
		GetAllStores_Of_Perticular_User_Receiver mrec;
		Activity context;
		public static void call(Activity context, GetAllStores_Of_Perticular_User_Receiver mrec)
		{
			Intent intent=new Intent(context, GetAllStores_Of_Perticular_User.class);
			intent.putExtra("getAllStores",mrec);
			intent.putExtra("URL", STORE_URL+SOTRES_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			context.startService(intent);
		}
	}

	@Override
	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		/*SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
		isPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);
		Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+isPlaceRefreshRequired);
		if(!isPlaceRefreshRequired)
		{
			STOREID=dbUtil.getAllPlacesIDs();
			STORE_NAME=dbUtil.getAllPlacesNames();
			PHOTO=dbUtil.getAllPlacesPhotoUrl();
			STORE_DESC=dbUtil.getAllPlacesDesc();
		}*/
		
		try
		{
			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS"); 
			fetchDialog.dismiss();
			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{
				

				SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
				isPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);
				Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+isPlaceRefreshRequired);
				if(!isPlaceRefreshRequired)
				{
					STOREID=dbUtil.getAllPlacesIDs();
					STORE_NAME=dbUtil.getAllPlacesNames();
					PHOTO=dbUtil.getAllPlacesPhotoUrl();
					STORE_DESC=dbUtil.getAllPlacesDesc();
				}
			}
			if(SEARCH_STATUS.equalsIgnoreCase("error"))
			{
				/*showToast("error");*/
				retryDialog.show();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}



}
