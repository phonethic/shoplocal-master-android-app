package com.phonethics.shoplocal;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;



public class SettingsActivity extends SherlockActivity implements OnClickListener {

	ActionBar actionBar;
	TextView selectDistanceLable;
	TextView selectDistance;
	TextView logoutLable;
	TextView logout;

	Activity context;

	Typeface tf;

	SessionManager session;

	Dialog distance_dialog;
	ListView distanceList;

	ArrayList<String> distances;

	ExpandableListView drawerList;


	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();


	private static long back_pressed;

	String drawer_item="";

	DrawerExpandibleAdapter drawerAdapter;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	LinearLayout logoutLayout;

	TextView clear;


	File file1, file2, file3, file4, file5;

	double total_length;

	DrawerClass drawerClass;

	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;
	
	private UiLifecycleHelper uiHelper;
	
	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;
	
	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;


	//User Name
	public static final String KEY_USER_NAME_CUSTOMER="mobileno_CUSTOMER";
	
	LocalNotification localNotification;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		context=this;
		
		localNotification=new LocalNotification(getApplicationContext());
		
		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);
		
		actionBar=getSupportActionBar();
		session=new SessionManager(context);
		distances=new ArrayList<String>();

		drawerClass=new DrawerClass(context);

		clear = (TextView) findViewById(R.id.clear);

		moreData=drawerClass.isShowMore();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			file1=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalGallery");
			file2=new File(android.os.Environment.getExternalStorageDirectory(),"/.hyperlocalcache");
			file3=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCustomerCache");
			file4=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalCache");
			file5=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalWelcomeGallery");

			//			int currentapiVersion = Build.VERSION.SDK_INT;
			//			if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH){
			//
			//
			//				total_length = file1.length()+file2.length()+file3.length()+file4.length()+file5.length();
			//
			//				total_length = total_length/1024;
			//
			//				clear.setText("Clear (File size " + total_length + "KB)");
			//
			//				Log.d(">GINGERBREAD",">GINGERBREAD " + total_length);
			//
			//			} else{

			double total_length_file1 = getFolderSize(file1);
			double total_length_file2 = getFolderSize(file2);
			double total_length_file3 = getFolderSize(file3);
			double total_length_file4 = getFolderSize(file4);
			double total_length_file5 = getFolderSize(file5);

			total_length = total_length_file1 + total_length_file2 + total_length_file3 + total_length_file4 + total_length_file5;

			total_length = total_length/(1024*1024);

			DecimalFormat df = new DecimalFormat("#.##");


			Log.d("GINGERBREAD","GINGERBREAD " + total_length);

			clear.setText("Clear (File size " + df.format(total_length) + "MB)");
			//}



			//Log.d("SizeOf","SizeOf " + total_length);
		}
		else
		{
			file1=context.getCacheDir();
			file2=context.getCacheDir();
			file3=context.getCacheDir();
			file4=context.getCacheDir();
			file5=context.getCacheDir();
		}



		distance_dialog=new Dialog(context);
		distance_dialog.setContentView(R.layout.distance_dialog);
		distance_dialog.setTitle("Select Distance");
		distance_dialog.setCancelable(true);
		distance_dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		distanceList=(ListView)distance_dialog.findViewById(R.id.distanceList);



		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
		actionBar.setTitle(getResources().getString(R.string.itemSettings));
		//		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		actionBar.setIcon(R.drawable.ic_drawer);

		selectDistanceLable=(TextView)findViewById(R.id.selectDistanceLable);
		selectDistance=(TextView)findViewById(R.id.selectDistance);
		logoutLable=(TextView)findViewById(R.id.logoutLable);
		logout=(TextView)findViewById(R.id.logout);
		logoutLayout=(LinearLayout)findViewById(R.id.logoutLayout);


		distances.add("25");
		distances.add("50");
		distances.add("75");
		distances.add("100");
		distances.add("Unknown");

		try
		{
			distanceList.setAdapter(new DistanceAdapter(context, 0, distances));
			if(!session.isLoggedInCustomer())
			{
				logoutLayout.setVisibility(View.GONE);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		selectDistanceLable.setTypeface(tf);
		selectDistance.setTypeface(tf);
		logoutLable.setTypeface(tf);
		logout.setTypeface(tf);

		logout.setOnClickListener(this);
		selectDistance.setOnClickListener(this);

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);





		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});

		//Creating Drawer
		createDrawer();

		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getResources().getString(R.string.itemSettings));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);


		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				DeleteCache();

			}
		});

		if(session.isLoggedInFacebook()){

			//Toast.makeText(context, "Facebook logged in", 0).show();
			logout.setText("Logout from facebook account");

		}
		else if(session.isLoggedInCustomer()){



			HashMap<String,String>user_details=session.getUserDetailsCustomer();
			String name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();


			//Toast.makeText(context, "Mobile logged in " + name, 0).show();

			logout.setText("Logout \t\t" + "(+" + name + ")");
		}

		EventTracker.startLocalyticsSession(getApplicationContext());
		
		
		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		
		
		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		
		
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
		        pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
		
		
		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}
				
				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
				
			}
			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
			
			
		}
		
		/** Anirudh- Custom SHare dialog */
		dialogShare = new Dialog(SettingsActivity.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));

	    
	}//onCreate Ends Here


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		uiHelper.onPause();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();
			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			EntryItem item = (EntryItem)items.get(position);
			drawer_item=item.getTitle();
			//			mDrawerLayout.closeDrawer(Gravity.LEFT);
			navigate();

		}
	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			//			Log.i("TRIM EVENT ", "TRIM EVENT "+drawer_item.replaceAll(" ", ""));
			EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();
			return false;
		}

	}

	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemChangeLocation));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,LocationActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{
			/*String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemShare));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, RequestTags.playStoreUrl);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			try {
				startActivity(Intent.createChooser(intent, "Select an action"));
			} catch (android.content.ActivityNotFoundException ex) {
				// (handle error)
			}*/
			
			
		
			showDialogTab();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
		dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{

				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				
				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}

	
	void showDialogTab(){
		dialogShare.show();
		
		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){
					
						FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(SettingsActivity.this)
						.setLink("http://shoplocal.co.in")
						.build();
						uiHelper.trackPendingDialogCall(shareDialog.present());
						dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}
				
				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}
				
				dismissDialog();
				
			}
		});
	}
	
	void dismissDialog(){
		dialogShare.dismiss();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		try
		{
			//			SearchView searchView = new SearchView(actionBar.getThemedContext());
			//			searchView.setQueryHint("Search");
			//			searchView.setIconified(true);
			//
			//			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "Search")
			//			.setIcon(R.drawable.abs__ic_search)
			//			.setActionView(searchView)
			//			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			//
			//			searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			//
			//				@Override
			//				public boolean onQueryTextSubmit(String newText) {
			//
			//					Intent intent=new Intent(context,SearchActivity.class);
			//					intent.putExtra("genericSearch", true);
			//					intent.putExtra("search_text", newText);
			//					startActivity(intent);
			//					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.fade_out);
			//					return true;
			//				}
			//
			//
			//
			//				@Override
			//				public boolean onQueryTextChange(String newText) {
			//
			//
			//					return true;
			//				}
			//			});

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}
		return true;
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(selectDistance.getId()==view.getId())
		{
			distance_dialog.show();
			distanceList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					// TODO Auto-generated method stub
					try
					{
						TextView dist= (TextView) view.findViewById(R.id.distance_text);
						String value = dist.getText().toString();
						Log.i("Distance Pref ", "Distance Pref  "+value);
						if(value.equalsIgnoreCase("Unknown"))
						{
							value="-1";
						}
						setPrefernces(value);
						distance_dialog.dismiss();

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});
		}
		if(logout.getId()==view.getId())
		{
			if(session.isLoggedInCustomer())
			{

				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle("Shoplocal");
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.logoutmessage))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Confirm",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						session.logoutCustomer();
						
						try
						{
							localNotification.checkPref();
							if(!session.isLoggedInCustomer())
							{
								localNotification.clearFavPrefs();
								localNotification.clearProfilePrefs();
								localNotification.clearAppLaunchPrefs();
								localNotification.clearLoginPrefs();
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
						
						logoutLayout.setVisibility(View.GONE);
						createDrawer();
					}
				})
				.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();

			}
		}
	}

	class DistanceAdapter extends ArrayAdapter<String>
	{
		Activity context;
		ArrayList<String> distances;
		LayoutInflater inflator;

		public DistanceAdapter(Activity context, int resource,
				ArrayList<String> distances) {
			super(context, resource, distances);
			// TODO Auto-generated constructor stub
			this.distances=distances;
			inflator=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return distances.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			try
			{
				if(convertView==null)
				{
					convertView=inflator.inflate(R.layout.distance_dialog_row,null);
					ViewHolder holder=new ViewHolder();
					holder.distance_text=(TextView)convertView.findViewById(R.id.distance_text);
					convertView.setTag(holder);
				}
				ViewHolder hold=(ViewHolder) convertView.getTag();
				hold.distance_text.setText(distances.get(position));
			}catch(Exception ex)
			{
				ex.printStackTrace();

			}
			return convertView;
		}


	}

	static class ViewHolder
	{
		TextView distance_text;
	}

	void setPrefernces(String value)
	{
		SharedPreferences prefs=getSharedPreferences("distance", MODE_PRIVATE);
		Editor editor=prefs.edit();
		editor.putString("distance", value);
		editor.commit();

		Log.i("Distance Pref","Distance Pref commited "+value);
	}

	//	@Override
	//	public void onBackPressed() {
	//		// TODO Auto-generated method stub
	//		if(distance_dialog.isShowing())
	//		{
	//			distance_dialog.dismiss();
	//		}
	//		finishTo();
	//	}

	void finishTo()
	{
		this.finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}	



	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	void DeleteCache()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage("Delete existing cache?")
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Clear",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				total_length = 0;

				clear.setText("Clear (File size " + total_length + "MB)");


				if(file1.exists())
				{
					DeleteRecursive(file1);
				}

				if(file2.exists())
				{
					DeleteRecursive(file2);
				}

				if(file3.exists())
				{
					DeleteRecursive(file3);
				}

				if(file4.exists())
				{
					DeleteRecursive(file4);
				}

				if(file5.exists())
				{
					DeleteRecursive(file5);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			createDrawer();
			if(session.isLoggedInCustomer())
			{
				logoutLayout.setVisibility(View.VISIBLE);
			}

			if(session.isLoggedInFacebook()){

			//	Toast.makeText(context, "Facebook logged in", 0).show();
				logout.setText("Logout from facebook account");

			}
			else if(session.isLoggedInCustomer()){



				//Login Details
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				String name=user_details.get(KEY_USER_NAME_CUSTOMER).toString();


				//Toast.makeText(context, "Mobile logged in " + name, 0).show();

				logout.setText("Logout \t\t" + "(+" + name + ")");
			}
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(distance_dialog.isShowing())
		{
			distance_dialog.dismiss();
		}
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}

	void DeleteRecursive(File file) {
		/*if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
			{

				Log.i("Delete:", "File "+child.getAbsolutePath());
				DeleteRecursive(child);
			}

		fileOrDirectory.delete();*/

		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					DeleteRecursive(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}

	public static double getFolderSize(File folderPath) {

		double totalSize = 0;

		if (folderPath == null) {
			return 0;
		}

		if (!folderPath.isDirectory()) {
			return 0;
		}

		File[] files = folderPath.listFiles();
		if(files != null){
			for (File file : files) {
				if (file.isFile()) {
					totalSize += file.length();
				} else if (file.isDirectory()) {
					totalSize += file.length();
					totalSize += getFolderSize(file);
				}
			}
		}
		return totalSize;
	}

}
