package com.phonethics.shoplocal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.shoplocal.R;
import com.phonethics.swipelistview.BaseSwipeListViewListener;
import com.phonethics.swipelistview.SwipeListView;
import com.phonethics.model.GetBroadCast;
import com.phonethics.networkcall.BroadCastApiReceiver;
import com.phonethics.networkcall.BroadCastApiReceiver.BroadCastApi;
import com.phonethics.networkcall.BroadCastService;
import com.squareup.picasso.Picasso;

public class ViewPost extends SherlockActivity implements BroadCastApi {

	SwipeListView viewPostsList;
	Activity context;
	NetworkCheck isnetConnected;
	ActionBar actionBar;

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> PLACE_ID=new ArrayList<String>();
	ArrayList<String> TITLE=new ArrayList<String>();
	ArrayList<String> BODY=new ArrayList<String>();
	ArrayList<String> TAGS=new ArrayList<String>();
	ArrayList<String> PHOTO_SOURCE=new ArrayList<String>();
	ArrayList<String> DATE=new ArrayList<String>();
	ArrayList<String> TYPE=new ArrayList<String>();	

	ArrayList<String>TOTAL_LIKE=new ArrayList<String>();

	ArrayList<String>TOTAL_VIEWS=new ArrayList<String>();
	ArrayList<String>TOTAL_SHARE=new ArrayList<String>();

	ArrayList<String> IMAGE_TITLE1=new ArrayList<String>();


	BroadCastApiReceiver mBroadCast;

	static String USER_ID="";
	static String AUTH_ID="";

	static String API_HEADER;
	static String API_VALUE;

	static String GET_BROADCAST_URL;
	static String STORE_URL;

	String STORE_ID;

	ProgressBar broadcastProgress;
/*	static ImageLoader imageLoaderList;*/

	Button btnViewPostLoadMore;

	static String PHOTO_PARENT_URL;

	String TOTAL_SEARCH_PAGES="";
	String TOTAL_SEARCH_RECORDS="";

	int post_count=20;
	int page=1;

	SessionManager session;
	
	String storeName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_post);
		context=this;

		actionBar=getSupportActionBar();
		actionBar.setTitle("View Posts");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		session=new SessionManager(context);

/*		imageLoaderList=new ImageLoader(context);*/

		isnetConnected=new NetworkCheck(context);

		viewPostsList=(SwipeListView)findViewById(R.id.viewPostsList);
		broadcastProgress=(ProgressBar)findViewById(R.id.broadcastProgress);
		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);

		//API KEY
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		GET_BROADCAST_URL=getResources().getString(R.string.broadcasts);


		mBroadCast=new BroadCastApiReceiver(new Handler());
		mBroadCast.setReceiver(this);

		btnViewPostLoadMore=(Button)findViewById(R.id.btnViewPostLoadMore);
		btnViewPostLoadMore.setVisibility(View.GONE);

		try{

			Bundle b=getIntent().getExtras();
			if(b!=null)
			{
				STORE_ID=b.getString("STORE_ID");
				storeName=b.getString("storeName");
				if(STORE_ID!="")
				{
					loadBroadCast();
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		btnViewPostLoadMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try{
					if(ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
					{
						btnViewPostLoadMore.setVisibility(View.GONE);

					}
					else if(page<Integer.parseInt(TOTAL_SEARCH_PAGES))
					{

						if(!broadcastProgress.isShown())
						{
							page++;
							loadBroadCast();
						}
						else
						{
							showToast("Please wait while loading");
						}
					}
					else
					{
						btnViewPostLoadMore.setVisibility(View.GONE);
					}

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		finishTo();
		return true;
	}

	void loadBroadCast()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, BroadCastService.class);
			intent.putExtra("broadcast",mBroadCast);
			intent.putExtra("URL", STORE_URL+GET_BROADCAST_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("store_id", STORE_ID);
			intent.putExtra("page", page);
			intent.putExtra("count", post_count);
			intent.putExtra("isMerchant", true);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			context.startService(intent);
			broadcastProgress.setVisibility(View.VISIBLE);
		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}
	void showToast(String message)
	{
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

	}
	@Override
	public void onReceiverBroadCasts(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		try
		{
			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");
			Log.i("Status ", "Status "+SEARCH_STATUS);
			broadcastProgress.setVisibility(View.GONE);
			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{
				viewPostsList.setVisibility(View.VISIBLE);
				ArrayList<GetBroadCast>getBroadcast=resultData.getParcelableArrayList("boradcast");
				/*		PLACE_ID.clear();
			ID.clear();
			TITLE.clear();
			BODY.clear();
			TAGS.clear();
			PHOTO_SOURCE.clear();
			DATE.clear();*/



				for(int i=0;i<getBroadcast.size();i++)
				{
					ID.add(getBroadcast.get(i).getId());
					PLACE_ID.add(getBroadcast.get(i).getPlace_id());
					TITLE.add(getBroadcast.get(i).getTitle());
					TYPE.add(getBroadcast.get(i).getType());
					BODY.add(getBroadcast.get(i).getDescription());
					TAGS.add(getBroadcast.get(i).getTags());
					DATE.add(getBroadcast.get(i).getDate());
					TOTAL_LIKE.add(getBroadcast.get(i).getTotal_like());

					TOTAL_VIEWS.add(getBroadcast.get(i).getUser_views());

					TOTAL_SHARE.add(getBroadcast.get(i).getTotal_share());
					
					PHOTO_SOURCE.add(getBroadcast.get(i).getImage_url1());
					
					IMAGE_TITLE1.add(getBroadcast.get(i).getImage_title1());

//					for(int j=0;j<getBroadcast.get(i).getSource().size();j++)
//					{
//						PHOTO_SOURCE.add(getBroadcast.get(i).getSource().get(j));
//					}
					TOTAL_SEARCH_PAGES=getBroadcast.get(i).getTotal_page();
					TOTAL_SEARCH_RECORDS=getBroadcast.get(i).getTotal_record();
				}

				Log.i("SIZE", "SIZE TITLE "+TITLE.size()+" DATE "+DATE.size()+" PHOTO_SOURCE "+PHOTO_SOURCE.size()+" TOTAL_SEARCH_PAGES "+TOTAL_SEARCH_PAGES+" TOTAL_SEARCH_RECORDS "+TOTAL_SEARCH_RECORDS+"TOTAL VIEWS "+TOTAL_VIEWS.size());
				for(int i=0;i<TOTAL_VIEWS.size();i++)
				{
					Log.i("TOTAL_VIEWS", "TOTAL_VIEWS "+TOTAL_VIEWS.get(i));
				}

				//Hide button if Total records are equal to Records fetched.
				if(Integer.parseInt(TOTAL_SEARCH_RECORDS)==ID.size())
				{
					btnViewPostLoadMore.setVisibility(View.GONE);
				}
				else
				{
					btnViewPostLoadMore.setVisibility(View.GONE);
				}


				SearchListAdapter adapter=new SearchListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, TITLE,PHOTO_SOURCE,DATE,TOTAL_LIKE, TOTAL_VIEWS, TOTAL_SHARE);

				viewPostsList.setAdapter(adapter);
				/*viewPostsList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context, OffersBroadCastDetails.class);
					intent.putExtra("title", TITLE.get(position));
					intent.putExtra("body", BODY.get(position));
					intent.putExtra("photo_source", PHOTO_SOURCE.get(position));

					startActivity(intent);
				}
			});*/

				viewPostsList.setSwipeListViewListener((new BaseSwipeListViewListener() {
					@Override
					public void onOpened(final int position, boolean toRight) {
						//Toast.makeText(context, "Right"+toRight, Toast.LENGTH_SHORT).show();
						/*swipeToRight=toRight;
					SWIPE_POSITION=position;*/
						/*		adapter.notifyDataSetChanged();*/
						try
						{
						Log.i("To Right", "To Right "+toRight);
						if(toRight)
						{
							AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
							alertDialog.setTitle("Re-use post");
							alertDialog.setMessage(getResources().getString(R.string.reusePostAlert));
							alertDialog.setPositiveButton("Yes",new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									ArrayList<String>SELECTED_STORE_ID=new ArrayList<String>();
									SELECTED_STORE_ID.add(PLACE_ID.get(position));
									dialog.cancel();
									Intent intent=new Intent(context, CreateBroadCast.class);
									intent.putExtra("POST_ID", ID.get(position));
									intent.putStringArrayListExtra("SELECTED_STORE_ID",SELECTED_STORE_ID);
									intent.putExtra("title", TITLE.get(position));
									intent.putExtra("description", BODY.get(position));
									intent.putExtra("photourl", PHOTO_SOURCE.get(position));
									intent.putExtra("tags", TAGS.get(position));
									intent.putExtra("caption", IMAGE_TITLE1.get(position));
									startActivity(intent);
									finish();
								}
							});
							alertDialog.setNegativeButton("No",new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});

							alertDialog.show();
						}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}

					@Override
					public void onClosed(int position, boolean fromRight) {
					}

					@Override
					public void onListChanged() {
					}

					@Override
					public void onMove(int position, float x) {
					}

					@Override
					public void onStartOpen(int position, int action, boolean right) {
						Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));

					}

					@Override
					public void onStartClose(int position, boolean right) {
						Log.d("swipe", String.format("onStartClose %d", position));
					}

					@Override
					public void onClickFrontView(int position) {
						Log.d("swipe", String.format("onClickFrontView %d", position));
						Intent intent=new Intent(context, OffersBroadCastDetails.class);
						intent.putExtra("PLACE_ID", STORE_ID);
						intent.putExtra("storeName", storeName);
						intent.putExtra("POST_ID", ID.get(position));
						intent.putExtra("title", TITLE.get(position));
						intent.putExtra("body", BODY.get(position));
						intent.putExtra("photo_source", PHOTO_SOURCE.get(position));

						startActivity(intent);
					}

					@Override
					public void onClickBackView(int position) {
						Log.d("swipe", String.format("onClickBackView %d", position));
					}

					@Override
					public void onDismiss(int[] reverseSortedPositions) {

						/*adapter.notifyDataSetChanged();*/
					}
				}));


			}
			if(SEARCH_STATUS.equalsIgnoreCase("false"))
			{
				Log.i("Status ", "Status INSIDE"+SEARCH_STATUS);
				String SEARCH_MESSAGE=resultData.getString("SEARCH_MESSAGE");
				showToast(SEARCH_MESSAGE);
				btnViewPostLoadMore.setVisibility(View.GONE);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-116"))
				{
					invalidAuthFinish();
				}
//				if(SEARCH_MESSAGE.equalsIgnoreCase("Invalid Authentication Code"))
//				{
//					session.logoutUser();
//					finishTo();
//				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}
	
	void invalidAuthFinish()
	{
		try
		{
			
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					try
					{
						session.logoutUser();
						Intent intent=new Intent(context, MerchantTalkHome.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();

			/*showToast(getResources().getString(R.string.invalidAuth)+ " Try logging in again ");*/
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	static class SearchListAdapter extends ArrayAdapter<String>
	{ArrayList<String> TITLE,distance,logo,photo,date,TOTAL_LIKE, TOTAL_VIEWS, TOTAL_SHARE;
	Activity context;
	LayoutInflater inflate;

	public SearchListAdapter(Activity context, int resource,
			int textViewResourceId, ArrayList<String> TITLE,ArrayList<String>photo,ArrayList<String>date,ArrayList<String>TOTAL_LIKE, ArrayList<String>TOTAL_VIEWS, ArrayList<String>TOTAL_SHARE) {
		super(context, resource, textViewResourceId, TITLE);
		// TODO Auto-generated constructor stub
		this.TITLE=TITLE;
		this.photo=photo;
		this.context=context;
		this.date=date;
		this.TOTAL_LIKE=TOTAL_LIKE;
		this.TOTAL_VIEWS = TOTAL_VIEWS;
		this.TOTAL_SHARE = TOTAL_SHARE;
		inflate=context.getLayoutInflater();

	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return TITLE.size();
	}
	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return TITLE.get(position);
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflate.inflate(R.layout.offers_broadcast_layout,null);

			holder.txtTitle=(TextView)convertView.findViewById(R.id.broadCastOffersText);
			holder.imgBroadcastLogo=(ImageView)convertView.findViewById(R.id.imgBroadcastLogo);
			holder.txtDate=(TextView)convertView.findViewById(R.id.broadCastOffersDate);
			holder.txtMonth=(TextView)convertView.findViewById(R.id.broadCastOffersMonth);
			holder.viewOverLay=(View)convertView.findViewById(R.id.viewOverLay);
			holder.txtBroadCastStoreTotalLike=(TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalLike);
			holder.txtBroadCastStoreTotalViews = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalViews);
			holder.txtBroadCastStoreTotalShare = (TextView)convertView.findViewById(R.id.txtBroadCastStoreTotalShare);

			convertView.setTag(holder);

		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		/*hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);*/

		hold.txtTitle.setText(TITLE.get(position));
		try
		{
			DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dt2=new SimpleDateFormat("MMM");
			Date date_con = (Date) dt.parse(date.get(position).toString());
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_con);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);

			Log.i("DATE", "DATE "+date_con+" DAY "+day+" YEAR "+year+" DATE OBJ"+date.get(position));
			hold.txtDate.setText(day+"");
			hold.txtMonth.setText(dt2.format(date_con)+"");

			// if(TOTAL_LIKE.get(position).equalsIgnoreCase("0"))
			// {
			// hold.txtBroadCastStoreTotalLike.setVisibility(View.GONE);
			// }
			// else
			// {
			// hold.txtBroadCastStoreTotalLike.setVisibility(View.VISIBLE);
			// }
			hold.txtBroadCastStoreTotalLike.setText(TOTAL_LIKE.get(position));
			hold.txtBroadCastStoreTotalViews.setText(TOTAL_VIEWS.get(position));
			
			Log.i("TOTAL_VIEWS ADAPTER", "TOTAL_VIEWS ADAPTER "+TOTAL_VIEWS.get(position));

			hold.txtBroadCastStoreTotalShare.setText(TOTAL_SHARE.get(position));

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			if(photo.get(position).toString().equalsIgnoreCase("notfound"))
			{
				//Log.i("LOGO", "LOGO "+logo.get(position));
				/*imageLoaderList.DisplayImage("",hold.imgBroadcastLogo);*/
				hold.txtDate.setTextColor(Color.argb(255, 255, 255, 255));
				hold.viewOverLay.setVisibility(View.INVISIBLE);
			}
			else
			{
				String photo_source=photo.get(position).toString().replaceAll(" ", "%20");
				hold.txtDate.setTextColor(Color.argb(200, 255, 255, 255));
				hold.viewOverLay.setVisibility(View.VISIBLE);
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo_source, hold.imgBroadcastLogo);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgBroadcastLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}

			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		return convertView;
	}
	}
	static class ViewHolder
	{

		TextView txtTitle,txtDate,txtMonth;
		ImageView imgBroadcastLogo;
		View viewOverLay;
		TextView txtBroadCastStoreTotalLike;
		TextView txtBroadCastStoreTotalViews;
		TextView txtBroadCastStoreTotalShare;


	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	void finishTo()
	{
		Intent intent=new Intent(context,MerchantTalkHome.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}



}
