package com.phonethics.shoplocal;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;
import com.phonethics.networkcall.SearchResultReceiver;
import com.phonethics.networkcall.SearchResultReceiver.SearchResult;
import com.phonethics.networkcall.SearchService;
import com.squareup.picasso.Picasso;
public class CategorySearch extends SherlockActivity  implements SearchResult, OnNavigationListener, Receiver {

	//Acitvity context
	Activity context;

	//Actionbar 
	ActionBar actionbar;

	//URLS
	String STORE_URL;
	String LOGIN_URL;
	String API_HEADER;
	String API_VALUE;
	String GET_SEARCH_URL;


	//Network 
	NetworkCheck isnetConnected;

	SearchResultReceiver searchReceiver;

	String PHOTO_PARENT_URL;

	//Session Manger
	SessionManager session;
	boolean isSplitLogin=false;

	//Listview
	PullToRefreshListView listCategSearchResult;
	TextView txtCategSearchCounter;
	TextView txtCategSearchCounterBack;
	ProgressBar searchCategListProg;
	Button btnCategSearchLoadMore;


	//Search
	ArrayList<String> Search_ID=new ArrayList<String>();
	ArrayList<String> Search_SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> Search_BUILDING=new ArrayList<String>();
	ArrayList<String> Search_STREET=new ArrayList<String>();
	ArrayList<String> Search_LANDMARK=new ArrayList<String>();
	ArrayList<String> Search_AREA=new ArrayList<String>();
	ArrayList<String> Search_CITY=new ArrayList<String>();
	ArrayList<String> Search_STATE=new ArrayList<String>();
	ArrayList<String> Search_COUNTRY=new ArrayList<String>();
	ArrayList<String> Search_MOBILE_NO=new ArrayList<String>();
	ArrayList<String> Search_PHOTO=new ArrayList<String>();
	ArrayList<String> Search_DISTANCE=new ArrayList<String>();
	ArrayList<String> Search_DESCRIPTION=new ArrayList<String>();

	ArrayList<String> Search_HAS_OFFER=new ArrayList<String>();
	ArrayList<String> Search_OFFER=new ArrayList<String>();

	String TOTAL_SEARCH_PAGES="";
	String TOTAL_SEARCH_RECORDS="";

	int search_post_count=20;
	int search_page=0;


	String offer="";
	String business_id="292";
	String category_id="";

	boolean isCheckOffers=false;
	boolean isCategory=true;

	int sposition=0;

	String locality="Lokhandwala";

	boolean isRefreshing=false;

	LocationResultReceiver mReceiver;

	String latitued="";
	String longitude="";

	boolean IS_SHOPLOCAL=false;

	ArrayList<String>dropdownItems=new ArrayList<String>();
	String JSON_STRING;
	String key="area_id";
	String value;

	int dropdown_position;

	boolean isLocationFound=false;

	//Database
	DBUtil dButil;

	SearchApiListAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_search);

		//Initializing context
		context=this;
		dButil=new DBUtil(context);

		JSON_STRING=getResources().getString(R.string.dropdownjson);

		mReceiver=new LocationResultReceiver(new Handler());
		mReceiver.setReceiver(this);
		business_id=getResources().getString(R.string.shoplocal_id);


		try
		{

			getLocationFromPrefs();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		isnetConnected=new NetworkCheck(context);
		/*imageLoaderList=new ImageLoader(context);*/


		//Action bar
		actionbar=getSupportActionBar();
		actionbar.setTitle(getString(R.string.actionBarTitle));
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.show();


		Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			category_id=bundle.getString("category_id");
			offer=bundle.getString("offer");
			isCategory=bundle.getBoolean("isCategory");
			if(!isCategory){
				isCheckOffers=true;
			}
		}


		//Registering Receiver
		searchReceiver=new SearchResultReceiver(new Handler());
		searchReceiver.setReceiver(this);

		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		/*STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.dummy_search);*/
		GET_SEARCH_URL=getResources().getString(R.string.searchapi);

		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		//Api Key
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);



		//Session Manager
		session=new SessionManager(getApplicationContext());
		isSplitLogin=session.isLoggedIn();

		listCategSearchResult=(PullToRefreshListView)findViewById(R.id.listCategSearchResult);
		txtCategSearchCounter=(TextView)findViewById(R.id.txtCategSearchCounter);
		//	txtCategSearchCounterBack=(TextView)findViewById(R.id.txtCategSearchCounterBack);
		searchCategListProg=(ProgressBar)findViewById(R.id.searchCategListProg);
		btnCategSearchLoadMore=(Button)findViewById(R.id.btnCategSearchLoadMore);
		btnCategSearchLoadMore.setVisibility(View.GONE);

		adapter=new SearchApiListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, Search_SOTRE_NAME, Search_PHOTO,Search_HAS_OFFER,Search_OFFER,Search_DESCRIPTION,Search_DISTANCE);
		listCategSearchResult.setAdapter(adapter);


		try
		{
			value=dButil.getActiveAreaID();
			if(isnetConnected.isNetworkAvailable())
			{
				searchApi();
			}
			else
			{
				MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
				listCategSearchResult.onRefreshComplete();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}



		//OnRefresh Listener
		listCategSearchResult.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				isRefreshing=true;
				try
				{
					search_page=0;
					if(isnetConnected.isNetworkAvailable())
					{

						if(!searchCategListProg.isShown())
						{
							searchApi();
						}
						else
						{
							MyToast.showToast(context,"Please wait while loading",2);
							listCategSearchResult.onRefreshComplete();
						}
					}
					else
					{
						MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
						listCategSearchResult.onRefreshComplete();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});


		listCategSearchResult.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				// TODO Auto-generated method stub
				if(isnetConnected.isNetworkAvailable())
				{

					if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
					{
						Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnCategSearchLoadMore.setVisibility(View.GONE);
					}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
					{
						if(!searchCategListProg.isShown())
						{

							//							search_page++;
							Log.i("Page", "Page "+search_page);
							searchApi();
							/*isSearchClicked=false;*/
						}
						else
						{
							MyToast.showToast(context, "Please wait while loading",2);
						}

					}
					else
					{
						Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnCategSearchLoadMore.setVisibility(View.GONE);
					}
				}
			}
		});

		btnCategSearchLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(isnetConnected.isNetworkAvailable())
				{

					if(Search_ID.size()==Integer.parseInt(TOTAL_SEARCH_RECORDS))
					{
						Log.i("ID SIZE","ID SIZE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnCategSearchLoadMore.setVisibility(View.GONE);
					}else if(search_page<Integer.parseInt(TOTAL_SEARCH_PAGES))
					{
						if(!searchCategListProg.isShown())
						{

							search_page++;
							Log.i("Page", "Page "+search_page);
							searchApi();
							/*isSearchClicked=false;*/
						}
						else
						{
							Toast.makeText(context, "Please wait while loading",Toast.LENGTH_SHORT).show();
						}

					}
					else
					{
						Log.i("ID SIZE","ID SIZE ELSE "+Search_ID.size()+" TOTAL SEARCHED RECORD: "+TOTAL_SEARCH_RECORDS);
						btnCategSearchLoadMore.setVisibility(View.GONE);
					}
				}
			}
		});
	}//onCreate ends here

	class CustomSpinnerAdapter extends ArrayAdapter<String> implements SpinnerAdapter 
	{

		ArrayList<String>name;
		Activity context;
		LayoutInflater layoutInflater;
		public CustomSpinnerAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> name) {
			super(context, resource, textViewResourceId, name);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.name=name;
			layoutInflater=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}


		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return name.get(position);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/* convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_menus, parent, false);
			}
			((TextView) convertView.findViewById(R.id.listText))
			.setText(getItem(position));
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				/*convertView = layoutInflater.inflate(
		                R.layout.sherlock_spinner_dropdown_item, parent, false);*/
				convertView = layoutInflater.inflate(
						R.layout.list_dropdown_item, parent, false);
			}
			((TextView) convertView.findViewById(R.id.txtSpinner))
			.setText(getItem(position));
			return convertView;
		}


	}




	//call search api
	void searchApi()
	{
		//		if(!IS_SHOPLOCAL)
		//		{
		//			Intent intent=new Intent(context, SearchService.class);
		//			intent.putExtra("searchapi",searchReceiver);
		//			/*intent.putExtra("URL", STORE_URL+GET_SEARCH_URL+search_post_count+"/page/"+search_page);*/
		//			intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
		//			intent.putExtra("api_header",API_HEADER);
		//			intent.putExtra("api_header_value", API_VALUE);
		//			intent.putExtra("search_post_count",search_post_count);
		//			intent.putExtra("search_page",search_page);
		//			intent.putExtra("place_id", business_id);
		//			intent.putExtra("offer", offer);
		//			intent.putExtra("category_id", category_id);
		//			intent.putExtra("isCheckOffers", isCheckOffers);
		//			intent.putExtra("isCategory", isCategory);
		//			intent.putExtra("key", key);
		//			intent.putExtra("value", value);
		//			intent.putExtra("isShopLocal",IS_SHOPLOCAL);
		//			context.startService(intent);
		//			searchCategListProg.setVisibility(View.VISIBLE);
		//		}
		//		else
		//		{

		Intent intent=new Intent(context, SearchService.class);
		intent.putExtra("searchapi",searchReceiver);
		/*intent.putExtra("URL", STORE_URL+GET_SEARCH_URL+search_post_count+"/page/"+search_page);*/
		intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		intent.putExtra("search_post_count",search_post_count);
		intent.putExtra("search_page",search_page+1);
		intent.putExtra("locality", locality);

		if(!isLocationFound)
		{
			latitued="0";
			longitude="0";
			intent.putExtra("isCheckLocation", false);
		}
		else
		{
			intent.putExtra("isCheckLocation", true);
		}
		intent.putExtra("latitude", latitued);
		intent.putExtra("longitude", longitude);

		intent.putExtra("offer", offer);
		intent.putExtra("category_id", category_id);
		intent.putExtra("isCheckOffers", isCheckOffers);
		intent.putExtra("isCategory", isCategory);
		intent.putExtra("key", key);
		intent.putExtra("value", value);
		intent.putExtra("isShopLocal",IS_SHOPLOCAL);
		context.startService(intent);
		searchCategListProg.setVisibility(View.VISIBLE);
		//		}
	}

	/*//call search api
		void searchApi()
		{
			Intent intent=new Intent(context, SearchService.class);
			intent.putExtra("searchapi",searchReceiver);
			intent.putExtra("URL", STORE_URL+GET_SEARCH_URL+search_post_count+"/page/"+search_page);
			intent.putExtra("URL", STORE_URL+GET_SEARCH_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("search_post_count",search_post_count);
			intent.putExtra("search_page",search_page);
			intent.putExtra("search", edtSearch.getText().toString());
					intent.putExtra("latitude", latitued);
				intent.putExtra("longitude", longitude);
				intent.putExtra("distance", "25");
			intent.putExtra("business_id", business_id);
			intent.putExtra("offer", offer);
			intent.putExtra("isCheckOffers", isCheckOffers);
				intent.putExtra("isCheckLocation", isCheckLocation);
				intent.putExtra("isCheckOffers", isCheckOffers);
			context.startService(intent);
			isGeneralSearch=true;
			searchListProg.setVisibility(View.VISIBLE);
		}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		this.finish();
		/*	overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);*/
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		return true;
	}


	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.SearchResultReceiver.SearchResult#onReciverSearchResult(int, android.os.Bundle)
	 */
	@Override
	public void onReciverSearchResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			searchCategListProg.setVisibility(View.GONE);
			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");

			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{


				final ArrayList<String> TempID=resultData.getStringArrayList("ID");
				final ArrayList<String> TempSOTRE_NAME=resultData.getStringArrayList("SOTRE_NAME");
				ArrayList<String> TempBUILDING=resultData.getStringArrayList("BUILDING");
				ArrayList<String> TempSTREET=resultData.getStringArrayList("STREET");
				ArrayList<String> TempLANDMARK=resultData.getStringArrayList("LANDMARK");
				final ArrayList<String> TempAREA=resultData.getStringArrayList("AREA");
				final ArrayList<String> TempCITY=resultData.getStringArrayList("CITY");
				final ArrayList<String> TempMOBILE_NO=resultData.getStringArrayList("MOBILE_NO");
				final ArrayList<String> TempPHOTO=resultData.getStringArrayList("PHOTO");
				ArrayList<String> TempDISTANCE=resultData.getStringArrayList("DISTANCE");
				ArrayList<String> TempDescription=resultData.getStringArrayList("DESCRIPTION");
				ArrayList<String> TempHasOffers=new ArrayList<String>();
				ArrayList<String> TempOffers=new ArrayList<String>();


				TempHasOffers.clear();
				TempOffers.clear();

				TempHasOffers=resultData.getStringArrayList("HAS_OFFERS");
				TempOffers=resultData.getStringArrayList("OFFERS");

				if(isRefreshing)
				{
					clearArray();
					isRefreshing=false;
				}
				TOTAL_SEARCH_PAGES=resultData.getString("TOTAL_PAGES");
				TOTAL_SEARCH_RECORDS=resultData.getString("TOTAL_RECORDS");

				search_page++;

				for(int index=0;index<TempID.size();index++)
				{
					Search_ID.add(TempID.get(index));
					Search_SOTRE_NAME.add(TempSOTRE_NAME.get(index));
					Search_BUILDING.add(TempBUILDING.get(index));
					Search_STREET.add(TempSTREET.get(index));
					Search_LANDMARK.add(TempLANDMARK.get(index));
					Search_AREA.add(TempAREA.get(index));
					Search_CITY.add(TempCITY.get(index));
					Search_MOBILE_NO.add(TempMOBILE_NO.get(index));
					Search_PHOTO.add(TempPHOTO.get(index));
					Search_DISTANCE.add(TempDISTANCE.get(index));
					/*TempDISTANCE.add("0");*/
					Search_DESCRIPTION.add(TempDescription.get(index));
					Search_HAS_OFFER.add(TempHasOffers.get(index));
					Search_OFFER.add(TempOffers.get(index));
				}

				//Get the first visible position of listview
				final int old_pos = listCategSearchResult.getRefreshableView().getFirstVisiblePosition();

				//			SearchApiListAdapter adapter=new SearchApiListAdapter(context, R.drawable.ic_launcher, R.drawable.ic_launcher, Search_SOTRE_NAME, Search_PHOTO,Search_HAS_OFFER,Search_OFFER,Search_DESCRIPTION,Search_DISTANCE);
				//			listCategSearchResult.setAdapter(adapter);

				adapter.notifyDataSetChanged();


				txtCategSearchCounter.setText(Search_ID.size()+"/"+TOTAL_SEARCH_RECORDS);


				if(Integer.parseInt(TOTAL_SEARCH_RECORDS)==Search_ID.size())
				{
					btnCategSearchLoadMore.setVisibility(View.GONE);
				}
				else
				{
					//btnCategSearchLoadMore.setVisibility(View.VISIBLE);
				}

				//List Refresh completed
				listCategSearchResult.onRefreshComplete();

				//Restore visible position
				//			listCategSearchResult.post(new Runnable() {
				//
				//				@Override
				//				public void run() {
				//					// TODO Auto-generated method stub
				//					listCategSearchResult.getRefreshableView().setSelection(old_pos);
				//				}
				//			});


				listCategSearchResult.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						if(!isRefreshing)
						{
							Intent intent=new Intent(context, StoreDetailsActivity.class);
							intent.putExtra("STORE_ID", Search_ID.get(position-1));
							intent.putExtra("STORE", Search_SOTRE_NAME.get(position-1));
							intent.putExtra("PHOTO", Search_PHOTO.get(position-1));
							intent.putExtra("AREA", Search_AREA.get(position-1));
							intent.putExtra("CITY", Search_CITY.get(position-1));
							intent.putExtra("MOBILE_NO", Search_MOBILE_NO.get(position-1));
							startActivity(intent);
						}
					}
				});
			}

			if(!SEARCH_STATUS.equalsIgnoreCase("true"))
			{
				String message="";
				message=resultData.getString("SEARCH_MESSAGE");
				listCategSearchResult.onRefreshComplete();
				if(message.equalsIgnoreCase("No records found"))
				{
					Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
					txtCategSearchCounter.setText("");
					btnCategSearchLoadMore.setVisibility(View.GONE);

				}
				else
				{
					Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
					txtCategSearchCounter.setText("");
					btnCategSearchLoadMore.setVisibility(View.GONE);
				}

				btnCategSearchLoadMore.setVisibility(View.GONE);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}





	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		this.finish();
		/*overridePendingTransition(R.anim.push_down_in, R.anim.activity_push_donw_out);*/
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}





	class SearchApiListAdapter extends ArrayAdapter<String>
	{
		ArrayList<String> store_name,distance,logo,has_offer,offers;
		ArrayList<String>Search_DESCRIPTION;
		Activity context;
		LayoutInflater inflate;
		String photo;
		Typeface tf;

		public SearchApiListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> store_name,ArrayList<String>logo,ArrayList<String>has_offers,ArrayList<String>offers,ArrayList<String>Search_DESCRIPTION,ArrayList<String>distance) {
			super(context, resource, textViewResourceId, store_name);
			// TODO Auto-generated constructor stub
			this.store_name=store_name;
			/*			this.distance=distance;*/
			this.logo=logo;
			this.Search_DESCRIPTION=Search_DESCRIPTION;
			this.context=context;
			this.offers=offers;
			this.has_offer=has_offers;
			this.distance=distance;
			inflate=context.getLayoutInflater();
			//getting font from asset directory.
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");

		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return store_name.size();
		}
		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return store_name.get(position);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflate.inflate(R.layout.layout_search_api,null);
				holder.imgStoreLogo=(ImageView)convertView.findViewById(R.id.imgsearchLogo);
				holder.txtStore=(TextView)convertView.findViewById(R.id.txtSearchStoreName);
				holder.txtDistance=(TextView)convertView.findViewById(R.id.txtSearchStoreDistance);
				holder.txtSearchStoreDesc=(TextView)convertView.findViewById(R.id.txtSearchStoreDesc);
				holder.band=(TextView)convertView.findViewById(R.id.band);
				holder.searchIsOffer=(ImageView)convertView.findViewById(R.id.searchIsOffer);
				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder)convertView.getTag();

			if(logo.get(position).toString().length()!=0)
			{
				//Log.i("LOGO", "LOGO "+logo.get(position));	
				photo=logo.get(position).replaceAll(" ", "%20");
				/*imageLoaderList.DisplayImage(PHOTO_PARENT_URL+photo, hold.imgStoreLogo);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(hold.imgStoreLogo);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}
			}
			else
			{
				hold.imgStoreLogo.setImageResource(R.drawable.ic_place_holder);
			}
			hold.txtStore.setText(store_name.get(position));
			hold.txtSearchStoreDesc.setText(Search_DESCRIPTION.get(position));

			hold.txtStore.setTypeface(tf);
			hold.txtSearchStoreDesc.setTypeface(tf);
			hold.txtDistance.setTypeface(tf);

			//			hold.band.setVisibility(View.VISIBLE);
			hold.searchIsOffer.setVisibility(View.VISIBLE);
			if(has_offer.get(position).toString().length()!=0 && has_offer.get(position).equalsIgnoreCase("1"))
			{
				/*	hold.txtBackOffers.setText(offers.get(position));*/
				/*	hold.band.setText("Special Offer : "+offers.get(position));*/
				hold.searchIsOffer.setVisibility(View.VISIBLE);
				/*				hold.searchFront.setBackgroundResource(R.drawable.shadow_effect_offers);
				hold.txtStore.setTextColor(Color.WHITE);*/
			}
			else
			{
				//				hold.txtBackOffers.setText("offers");
				/*hold.band.setVisibility(View.GONE);*/
				hold.searchIsOffer.setVisibility(View.GONE);
				/*	hold.searchFront.setBackgroundResource(R.drawable.shadow_effect);
				hold.txtStore.setTextColor(Color.parseColor("#51de04"));*/
			}
			/*if(distance.get(position)!=null)
				{
					if(Double.parseDouble(distance.get(position))!=-1)
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position)));
						hold.txtDistance.setText(""+formatted+" km.");
					}
				}*/

			if(distance.get(position)!=null)
			{
				/*if(Double.parseDouble(distance.get(position))!=-1)
				{
					DecimalFormat format = new DecimalFormat("##.##");
					String formatted = format.format(Double.parseDouble(distance.get(position)));
					hold.txtDistance.setText(""+formatted+" km.");
				}*/
				if(Double.parseDouble(distance.get(position))!=-1)
				{
					//	Log.i("Distance : ","Distance : "+distance.get(position));

					if(Double.parseDouble(distance.get(position))<1 && Double.parseDouble(distance.get(position))>0)
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position))*100);
						hold.txtDistance.setText(""+formatted+" m.");
					}
					else
					{
						DecimalFormat format = new DecimalFormat("##.##");
						String formatted = format.format(Double.parseDouble(distance.get(position)));
						if((Double.parseDouble(distance.get(position))*100)>8000)
						{
							hold.txtDistance.setVisibility(View.GONE);
						}
						else
						{
							hold.txtDistance.setVisibility(View.VISIBLE);
							hold.txtDistance.setText(""+formatted+" km.");
						}
						/*hold.txtDistance.setText(""+formatted+" km.");*/
					}

				}
			}

			return convertView;
		}



	}

	class ViewHolder
	{
		ImageView imgStoreLogo,searchIsOffer;
		TextView txtStore;
		TextView txtSearchStoreDesc;
		TextView txtDistance;
		TextView band;
	}


	void locationService()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			//calling location service.
			Intent intent=new Intent(context, LocationIntentService.class);
			intent.putExtra("receiverTag", mReceiver);
			startService(intent);
			/*ShopLocalMain.shopProgress.setVisibility(View.VISIBLE);*/
			searchCategListProg.setVisibility(View.VISIBLE);
		}else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void  showToast(String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		/*if(!IS_SHOPLOCAL)
		{

			SharedPreferences pref=context.getSharedPreferences("placeid", 0);
			Editor editor=pref.edit();
			if(itemPosition==0)
			{
				business_id=getResources().getString(R.string.malad_id);
				editor.putString("place_id", business_id);
				editor.commit();
			}
			else if(itemPosition==1)
			{
				business_id=getResources().getString(R.string.vashi_id);
				editor.putString("place_id", business_id);
				editor.commit();

			}
			else if(itemPosition==2)
			{
				business_id=getResources().getString(R.string.cyberabad_id);
				editor.putString("place_id", business_id);
				editor.commit();

			}
			else if(itemPosition==3)
			{
				business_id=getResources().getString(R.string.whitefield_id);
				editor.putString("place_id", business_id);
				editor.commit();
				Log.i("business_id", "business_id commited "+business_id);
			}
		}*/

		SharedPreferences pref=context.getSharedPreferences("dropdown", 0);
		Editor editor=pref.edit();
		editor.putString("selected_dropdown", getDropDownObject(itemPosition,JSON_STRING));
		editor.putInt("dropdown_position", itemPosition);
		editor.commit();

		Log.i("business_id", "business_id commited "+getDropDownObject(itemPosition,JSON_STRING));

		//parse key and value to search
		parseKeyValue(getDropDownObject(itemPosition,JSON_STRING));
		clearArray();
		if(IS_SHOPLOCAL)
		{
			if(isnetConnected.isNetworkAvailable())
			{
				/*if(latitued.equalsIgnoreCase("not found") && longitude.equalsIgnoreCase("not found"))
				{
				locationService();
				}else
				{
					searchApi();
				}*/
				searchApi();
			}
			else
			{
				MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
				listCategSearchResult.onRefreshComplete();
			}
		}
		else
		{

			if(isnetConnected.isNetworkAvailable())
			{
				searchApi();
			}
			else
			{
				MyToast.showToast(context,getResources().getString(R.string.noInternetConnection),1);
				listCategSearchResult.onRefreshComplete();
			}
		}

		return false;
	}

	void parseKeyValue(String json)
	{
		try {
			Log.i("PARSE ", "PARSE KEY  "+json);
			JSONObject jsonobject=new JSONObject(json);
			key=jsonobject.getString("key");
			value=jsonobject.getString("value");
			locality=value;
			IS_SHOPLOCAL=Boolean.parseBoolean(jsonobject.getString("isShopLocal"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void parseJson(String json)
	{
		try {

			Log.i("PARSE ", "PARSE  "+json);

			JSONObject jsonobject=new JSONObject(json);
			JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");
			for(int i=0;i<dropDownArray.length();i++)
			{
				JSONObject jsonObject=dropDownArray.getJSONObject(i);

				if(getResources().getBoolean(R.bool.isAppShopLocal))
				{
					//Shoplocal
					if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}else
				{
					//Inorbit
					if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")))
					{
						dropdownItems.add(jsonObject.getString("name"));
					}
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	String getDropDownObject(int position,String json)
	{
		String dropdown="";
		if(dropdownItems.size()!=0)
		{

			Log.i("PARSE ", "PARSE getDropDownObject  "+json);
			//Log.i("BOOLEAN ", "BOOLEAN "+getResources().getBoolean(R.bool.isAppShopLocal)+" POSITION "+position);
			try {
				JSONObject jsonobject=new JSONObject(json);
				JSONArray dropDownArray=jsonobject.getJSONArray("dropdowns");

				/*	JSONObject jsonObject=dropDownArray.getJSONObject(position);
				dropdown=jsonObject.toString();

				Log.i("JSON", "JSON "+dropdown);*/

				for(int i=0;i<dropDownArray.length();i++)
				{
					JSONObject jsonObject=dropDownArray.getJSONObject(i);

					Log.i("PARSE ", "BOOLEAN PARSE getDropDownObject  "+jsonObject.toString()); 


					if(getResources().getBoolean(R.bool.isAppShopLocal))
					{
						//Shoplocal
						if(Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}else
					{
						//Inorbit
						if(!Boolean.parseBoolean(jsonObject.getString("isShopLocal")) && position==i)
						{
							Log.i("BOOLEAN ", "BOOLEAN "+Boolean.parseBoolean(jsonObject.getString("isShopLocal")) +" POSITION "+position+" JSON "+jsonObject.toString());
							dropdown=jsonObject.toString();
						}
					}
				}

				Log.i("GET DROP DOWN ", "GET DROP DOWN "+dropdown);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();

			}


		}
		return dropdown;
	}

	void clearArray()
	{
		Search_ID.clear();
		Search_SOTRE_NAME.clear();
		Search_BUILDING.clear();
		Search_STREET.clear();
		Search_LANDMARK.clear();
		Search_AREA.clear();
		Search_CITY.clear();
		Search_MOBILE_NO.clear();
		Search_PHOTO.clear();
		Search_DISTANCE.clear();
		Search_DESCRIPTION.clear();
		Search_HAS_OFFER.clear();
		Search_OFFER.clear();

		search_page=0;
		search_post_count=20;





	}


	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		searchCategListProg.setVisibility(View.GONE);
		String status=resultData.getString("status");
		if(status.equalsIgnoreCase("set"))
		{
			try
			{
				latitued=resultData.getString("Lat");
				longitude=resultData.getString("Longi");


				if(latitued!=null || latitued.length()>0 && longitude!=null || longitude.length()>0)
				{

					if(!latitued.equalsIgnoreCase("not found") && !longitude.equalsIgnoreCase("not found"))
					{
						Log.i("LAT", "LATI "+latitued);
						Log.i("LONG", "LATI "+longitude);
						searchApi();
					}
					else
					{
						MyToast.showToast(context, "Please check your location service", 2);
					}

					/*onLocationChanged(location);*/
					//	showToast("Location");
				}
				Log.i("Location ", "LOCATION : "+latitued+" , "+longitude);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			searchCategListProg.setVisibility(View.GONE);
			showToast("Please check your location service");
		}
	}

	void getLocationFromPrefs()
	{
		SharedPreferences pref=context.getSharedPreferences("location",0);
		latitued=pref.getString("latitued", "not found");
		longitude=pref.getString("longitude", "not found");
		isLocationFound=pref.getBoolean("isLocationFound",false);
		Log.i("Location found","Location found "+isLocationFound+" "+latitued+","+longitude);
	}


}
