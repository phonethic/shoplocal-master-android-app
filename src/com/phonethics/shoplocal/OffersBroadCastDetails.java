package com.phonethics.shoplocal;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.model.GetBroadCast;
import com.phonethics.networkcall.BroadCastApiReceiver;
import com.phonethics.networkcall.BroadCastApiReceiver.BroadCastApi;
import com.phonethics.networkcall.BroadCastService;
import com.phonethics.networkcall.FavouritePlaceResultReceiver;
import com.phonethics.networkcall.FavouritePlaceResultReceiver.FavouriteInterface;
import com.phonethics.networkcall.FavouritePlaceService;
import com.phonethics.networkcall.FavouritePostResultReceiver;
import com.phonethics.networkcall.FavouritePostResultReceiver.FavouritePostInterface;
import com.phonethics.networkcall.FavouritePostService;
import com.phonethics.networkcall.ShareResultReceiver;
import com.phonethics.networkcall.ShareResultReceiver.ShareResultInterface;
import com.phonethics.networkcall.ShareService;
import com.phonethics.networkcall.UnFavouritePostReceiver;
import com.phonethics.networkcall.UnFavouritePostReceiver.UnfavouritePostInterface;
import com.phonethics.networkcall.UnFavouritePostService;
import com.squareup.picasso.Picasso;



public class OffersBroadCastDetails extends SherlockActivity implements FavouritePostInterface,UnfavouritePostInterface, BroadCastApi, ShareResultInterface, FavouriteInterface{

	ActionBar actionBar;
	TextView txtBroadCastTitle,txtBroadCastBody;
	ImageView imgStoreImage;
	ImageView imageSeprator;
	TableRow imageRow;
	/*	ImageLoader imgStoreLoader;*/
	ImageView sharePost;
	ImageView postFav;
	Activity context;
	String photo_source="";
	String url_source="";
	String photo_link="";

	String caption="";


	static String PHOTO_PARENT_URL;

	View shadow;

	boolean fromCustomer=false;
	boolean isSinglePostView=true;
	boolean fromNews=false;

	FavouritePostResultReceiver mFav;
	UnFavouritePostReceiver mUnfav;

	FavouritePlaceResultReceiver mFavourite;


	//Receiver
	BroadCastApiReceiver mBroadCast;

	static String BROADCAST_URL;
	static String GET_BROADCAST_URL;
	static String LIKE_API;

	NetworkCheck isnetConnected;

	static String API_HEADER;
	static String API_VALUE;


	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	//Password
	public static final String KEY_PASSWORD_CUSTOMER="password_CUSTOMER";

	SessionManager session;

	boolean isLoggedInCustomer=false;

	static String USER_ID;
	static String AUTH_ID;
	static String SHARE_URL;



	String POST_ID;
	String PLACE_ID;
	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	String USER_LIKE="-1";
	boolean getLike=false;
	boolean likePost=false;

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> TYPE=new ArrayList<String>();	
	ArrayList<String> URL_OFFERS=new ArrayList<String>();
	ArrayList<String> TITLE=new ArrayList<String>();
	ArrayList<String> BODY=new ArrayList<String>();


	ArrayList<String> VIDEO_URL=new ArrayList<String>();
	ArrayList<String> PHOTO_SOURCE=new ArrayList<String>();
	ArrayList<String> PHOTO_LINK=new ArrayList<String>();
	ArrayList<String> VIDEO_LINK=new ArrayList<String>();
	ArrayList<String> URL=new ArrayList<String>();
	ArrayList<String> DATE=new ArrayList<String>();
	ArrayList<String> USERLIKE=new ArrayList<String>();
	ArrayList<String>TOTAL_LIKE=new ArrayList<String>();

	ArrayList<String>TOTAL_VIEWS=new ArrayList<String>();
	ArrayList<String>TOTAL_SHARE=new ArrayList<String>();

	ArrayList<String> GALLERY_PHOTO_SOURCE=new ArrayList<String>();

	ArrayList<String> IMAGE_TITLE1=new ArrayList<String>();

	ArrayList<String> IS_OFFER=new ArrayList<String>();
	ArrayList<String> OFFER_DATE_TIME=new ArrayList<String>();

	TextView txtPostFav;
	TextView txtPostShare;
	TextView txtPostView;

	Typeface tf;

	ImageView vertiSeprator;
	Button shareButton;
	Button likeButton;
	Button addToShopLocal;

	ShareResultReceiver shareServiceResult;

	String VIA = "Android";
	LinearLayout merchantView;
	LinearLayout likeShareLayout;
	TextView shareTextButton;

	String storeName;

	boolean autoLike=false;

	Button byNowButton;

	String mob1="";
	String mob2="";
	String mob3="";

	String tel1="";
	String tel2="";
	String tel3="";

	String is_offer="";
	String valid_till="";

	ArrayList<String> callnumbers=new ArrayList<String>();

	//Call Dialog
	Dialog calldailog;
	ListView listCall;
	Button gotoStore;

	boolean isFromStoreDetails=false;

	static String GET_STORE_URL;
	int PLACE_LIKE=1;
	int POST_LIKE=2;

	View viewOverlay;

	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;
	List<Intent> targetedShareIntents = new ArrayList<Intent>();

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	/** Actionbar custom title */
	TextView tvActionBarTitle;

	/*<<<<<<< HEAD
	*//** Events param *//*
	Map<String, String> eventTrackingParamsLike = new HashMap<String, String>();
	Map<String, String> eventTrackingParamsShare = new HashMap<String, String>();
	Map<String, String> eventTrackingParamsCall = new HashMap<String, String>();
	Map<String, String> eventTrackingParamsCallDone = new HashMap<String, String>(); */
	
	Map<String,String> eventMap=new HashMap<String, String>();
	Map<String,String> eventMapStoreCall=new HashMap<String, String>();

	DBUtil dbUtil;
//>>>>>>> 7dc0d52c5613f93030a1113423333beb6f1a9a40

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offers_broad_cast_details);

		context=this;
		isnetConnected=new NetworkCheck(context);

		dbUtil=new DBUtil(context);

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		EventTracker.startLocalyticsSession(context);

		session=new SessionManager(context);

		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		mFavourite=new FavouritePlaceResultReceiver(new Handler());
		mFavourite.setReceiver(this);

		//Log.i("INVALID", "INVALID CODE "+getResources().getString(R.string.invalidAuth));

		BROADCAST_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.broadcast_api);
		LIKE_API=getResources().getString(R.string.like);
		GET_BROADCAST_URL=getResources().getString(R.string.broadcasts);

		GET_STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);

		SHARE_URL = "/" + getResources().getString(R.string.share);


		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		mFav=new FavouritePostResultReceiver(new Handler());
		mFav.setReceiver(this);

		mUnfav=new UnFavouritePostReceiver(new Handler());
		mUnfav.setReceiver(this);

		mBroadCast=new BroadCastApiReceiver(new Handler());
		mBroadCast.setReceiver(this);

		shareServiceResult = new ShareResultReceiver(new Handler());
		shareServiceResult.setReceiver(this);


		//Photo url
		PHOTO_PARENT_URL=getResources().getString(R.string.photo_url);

		/*		imgStoreLoader=new ImageLoader(context);*/

		/** Adjust title size with custom action bar layout */
		actionBar=getSupportActionBar();
		/*actionBar.setTitle("Shoplocal");
		actionBar.setDisplayHomeAsUpEnabled(true);*/
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		View actionBarView = getLayoutInflater().inflate(R.layout.actionbarlayoutforstoredetail, null);
		tvActionBarTitle = (TextView) actionBarView.findViewById(R.id.textViewActionBarTitle);
		actionBar.setCustomView(actionBarView);
		actionBar.show();




		merchantView=(LinearLayout)findViewById(R.id.merchantView);
		likeShareLayout=(LinearLayout)findViewById(R.id.likeShareLayout);

		calldailog=new Dialog(context);
		calldailog.setContentView(R.layout.calldialog);
		calldailog.setTitle("Contact");
		calldailog.setCancelable(true);
		calldailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		listCall=(ListView)calldailog.findViewById(R.id.listCall);
		gotoStore=(Button)findViewById(R.id.gotoStore);
		//		gotoStore.setVisibility(View.GONE);



		txtBroadCastTitle=(TextView)findViewById(R.id.txtBroadCastTitle);
		txtBroadCastBody=(TextView)findViewById(R.id.txtBroadCastBody);
		imageRow=(TableRow)findViewById(R.id.imageRow);

		imgStoreImage=(ImageView)findViewById(R.id.imgStoreImage);
		postFav=(ImageView)findViewById(R.id.postFav);
		sharePost=(ImageView)findViewById(R.id.sharePost);

		shareButton=(Button)findViewById(R.id.shareButton);
		likeButton=(Button)findViewById(R.id.likeButton);
		addToShopLocal=(Button)findViewById(R.id.addToShopLocal);

		txtPostFav=(TextView)findViewById(R.id.txtPostFav);
		txtPostShare=(TextView)findViewById(R.id.txtPostShare);
		txtPostView=(TextView)findViewById(R.id.txtPostView);
		shareTextButton=(TextView)findViewById(R.id.shareTextButton);
		vertiSeprator=(ImageView)findViewById(R.id.vertiSeprator);

		imageSeprator=(ImageView)findViewById(R.id.imageSeprator);

		viewOverlay=(View)findViewById(R.id.viewOverlay);

		byNowButton=(Button)findViewById(R.id.byNowButton);

		byNowButton.setText("Call");

		byNowButton.setTypeface(tf);
		gotoStore.setTypeface(tf);

		txtPostFav.setTypeface(tf);
		txtPostShare.setTypeface(tf);
		txtPostView.setTypeface(tf);

		txtPostFav.setTextColor(Color.parseColor("#c4c4c4"));
		txtPostShare.setTextColor(Color.parseColor("#c4c4c4"));
		txtPostView.setTextColor(Color.parseColor("#c4c4c4"));

		sharePost.setVisibility(View.VISIBLE);

		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
		isLoggedInCustomer=session.isLoggedInCustomer();

		try
		{
			Bundle b=getIntent().getExtras();

			if(b!=null)
			{
				txtBroadCastTitle.setText(b.getString("title"));
				txtBroadCastBody.setText(b.getString("body"));
				photo_source=b.getString("photo_source");
				url_source=b.getString("url_source");
				photo_link=b.getString("photo_link");
				fromCustomer=b.getBoolean("fromCustomer");
				fromNews=b.getBoolean("fromNews");
				POST_ID=b.getString("POST_ID");
				PLACE_ID=b.getString("PLACE_ID");
				USER_LIKE=b.getString("USER_LIKE");
				storeName=b.getString("storeName");
				isFromStoreDetails=b.getBoolean("isFromStoreDetails",false);
				Log.i("Intent Data", "Intent Data photo_source "+photo_source);
				Log.i("Intent Data", "Intent Data POST_ID "+POST_ID);
				Log.i("Intent Data", "Intent Data PLACE_ID "+PLACE_ID);
				Log.i("Intent Data", "Intent Data USER_LIKE"+USER_LIKE);
				Log.i("Intent Data", "Intent Data photo_source"+photo_source);
				Log.i("Intent Data", "Intent Data fromCustomer "+fromCustomer);

				try
				{
					//actionBar.setTitle(storeName);
					/** Set titles for action bar */
					tvActionBarTitle.setText(storeName);

					/** Check if actionbar title lines is > 1 and reduce text size */
					tvActionBarTitle.post(new Runnable() {

						@Override
						public void run() {

							int lineCount    = tvActionBarTitle.getLineCount();
							if(lineCount > 1){
								tvActionBarTitle.setTextSize(15.0f);
							}
						}
					});
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				Log.i("Intent Data", "Intent Data fromCustomer isFromStoreDetails "+isFromStoreDetails);

				if(fromCustomer && !fromNews)
				{
					mob1=b.getString("mob1");
					mob2=b.getString("mob2");
					mob3=b.getString("mob3");


					tel1=b.getString("tel1");
					tel2=b.getString("tel2");
					tel3=b.getString("tel3");

					if(mob1.length()>5)
					{
						callnumbers.add("+"+mob1);
					}
					if(mob2.length()>5)
					{
						callnumbers.add("+"+mob2);
					}
					if(mob3.length()>5)
					{
						callnumbers.add("+"+mob3);
					}


					if(tel1.length()>5)
					{
						callnumbers.add(tel1);
					}
					if(tel2.length()>5)
					{
						callnumbers.add(tel2);
					}
					if(tel3.length()>5)
					{
						callnumbers.add(tel3);
					}


					listCall.setAdapter(new CallListDialog(context, 0, callnumbers));


					listCall.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int position, long arg3) {
							try
							{
								eventMap.put("Source", "OfferDetails");
								eventMap.put("AreaName", dbUtil.getActiveArea());

								EventTracker.logEvent("StoreCallDone",eventMap);

								Intent call = new Intent(android.content.Intent.ACTION_DIAL);
								call.setData(Uri.parse("tel:"+callnumbers.get(position)));
								startActivity(call);
								overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								calldailog.dismiss();
							}catch(Exception ex)
							{
								ex.printStackTrace();
							}
						}
					});


				}

				if(photo_source.startsWith("notfound"))
				{
					Log.i("Intent Data", "Intent Data photo_source"+photo_source);

					imgStoreImage.setScaleType(ScaleType.FIT_CENTER);
				}

				if(USER_LIKE!=null && USER_LIKE.equalsIgnoreCase("0"))
				{
					//					postFav.setImageResource(R.drawable.postunfav);
				}
				else if(USER_LIKE!=null && USER_LIKE.equalsIgnoreCase("1"))
				{
					//					postFav.setImageResource(R.drawable.postfav);
				}

				if(POST_ID!=null && POST_ID.length()>0)
				{
					/*loadBroadCast();*/
					if(isnetConnected.isNetworkAvailable())
					{
						/*if(isLoggedInCustomer)
						{
							getLike=true;
							likePost=true;
						}
						else
						{
							getLike=false;
							likePost=false;
						}*/
						loadBroadCast();
					}
					else
					{
						MyToast.showToast(context, getString(R.string.noInternetConnection),1);
					}
				}



				animate(shareTextButton).setDuration(200);
				shareTextButton.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent arg1) {
						// TODO Auto-generated method stub

						if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
							animate(shareTextButton).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
						} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
							animate(shareTextButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}
						else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
						{
							animate(shareTextButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}



						return false;
					}
				});
				shareTextButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub


						//						//createShareIntent();
						//						Intent sendIntent = new Intent();
						//						sendIntent.setType("text/plain");
						//						sendIntent.setAction(Intent.ACTION_SEND);
						//						// sendIntent.putExtra(Intent.EXTRA_SUBJECT, txtBroadCastTitle.getText().toString());
						//
						//						sendIntent.putExtra(Intent.EXTRA_SUBJECT, storeName + " - " + "Offers");
						//						sendIntent.putExtra(Intent.EXTRA_TEXT,storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString());
						//						startActivityForResult(sendIntent,3);



						// TODO Auto-generated method stub
						//						TEMP_POST_ID=post_id.get(position);


						/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Share");
						alertDialogBuilder
						.setMessage("Share using")
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(true)
						.setPositiveButton("Other",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {

								dialog.dismiss();

								List<Intent> targetedShareIntents = new ArrayList<Intent>();
								Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
								sharingIntent.setType("text/plain");
								String shareBody =storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString();

								PackageManager pm = context.getPackageManager();
								List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

								for(final ResolveInfo app : activityList) {

									String packageName = app.activityInfo.packageName;
									Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
									targetedShareIntent.setType("text/plain");

									if(TextUtils.equals(packageName, "com.facebook.katana")){

									} else {
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,  storeName + " - " + "Offers");
										targetedShareIntent.setPackage(packageName);
										targetedShareIntents.add(targetedShareIntent);
									}

								}

								Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
								chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
								startActivityForResult(chooserIntent,3);

							}
						})
						.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.dismiss();

								if(photo_source.length()!=0)
								{
									String photo=photo_source.replaceAll(" ", "%20");

									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(storeName + " - " + "Offers")
									.setPicture(PHOTO_PARENT_URL+photo)
									.setDescription(storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString())
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
								else
								{
									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(storeName + " - " + "Offers")
									.setDescription(storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString())
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
							}
						})
						;

						AlertDialog alertDialog = alertDialogBuilder.create();

						alertDialog.show();*/



						showDialog();



					}


				});


				animate(shareButton).setDuration(200);
				shareButton.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent arg1) {
						// TODO Auto-generated method stub

						if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
							animate(shareButton).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
						} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
							animate(shareButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}
						else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
						{
							animate(shareButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}

						return false;
					}
				});

				animate(addToShopLocal).setDuration(200);
				addToShopLocal.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent arg1) {
						// TODO Auto-generated method stub

						if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
							animate(addToShopLocal).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
						} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
							animate(addToShopLocal).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}
						else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
						{
							animate(addToShopLocal).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}



						return false;
					}
				});
				addToShopLocal.setTypeface(tf);
				addToShopLocal.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(session.isLoggedInCustomer())
						{
							likePlace();
						}
						else
						{
							loginAlert(getResources().getString(R.string.placeFavAlert),PLACE_LIKE);
						}
					}
				});

				shareButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub



						//createShareIntent();
						//						Intent sendIntent = new Intent();
						//						sendIntent.setType("text/plain");
						//						sendIntent.setAction(Intent.ACTION_SEND);
						//						sendIntent.putExtra(Intent.EXTRA_SUBJECT, storeName + " - " + "Offers");
						//						sendIntent.putExtra(Intent.EXTRA_TEXT,storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString());
						//						startActivityForResult(sendIntent,3);


						/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
						alertDialogBuilder.setTitle("Share");
						alertDialogBuilder
						.setMessage("Share using")
						.setIcon(R.drawable.ic_launcher)
						.setCancelable(true)
						.setPositiveButton("Other",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
						 *//**Anirudh to change *//*
								dialog.dismiss();

								List<Intent> targetedShareIntents = new ArrayList<Intent>();
								Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
								sharingIntent.setType("text/plain");
								String shareBody =storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString();

								PackageManager pm = context.getPackageManager();
								List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);

								for(final ResolveInfo app : activityList) {

									String packageName = app.activityInfo.packageName;
									Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
									targetedShareIntent.setType("text/plain");

									if(TextUtils.equals(packageName, "com.facebook.katana")){

									} else {
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
										targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,  storeName + " - " + "Offers");
										targetedShareIntent.setPackage(packageName);
										targetedShareIntents.add(targetedShareIntent);
									}

								}

								Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share");
								chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
								startActivityForResult(chooserIntent,3);

							}
						})
						.setNegativeButton("Facebook",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.dismiss();

								if(photo_source.length()!=0)
								{
									String photo=photo_source.replaceAll(" ", "%20");

									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(storeName + " - " + "Offers")
									.setPicture(PHOTO_PARENT_URL+photo)
									.setDescription(storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString())
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
								else
								{
									FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
									.setLink("http://shoplocal.co.in")
									.setName(storeName + " - " + "Offers")
									.setDescription(storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString())
									.build();
									uiHelper.trackPendingDialogCall(shareDialog.present());
								}
							}
						})
						;

						AlertDialog alertDialog = alertDialogBuilder.create();

						alertDialog.show();*/


						showDialog();




					}


				});


				animate(byNowButton).setDuration(200);
				byNowButton.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent arg1) {
						// TODO Auto-generated method stub

						if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
							animate(byNowButton).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
						} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
							animate(byNowButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}
						else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
						{
							animate(byNowButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}

						return false;
					}
				});
				byNowButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						try
						{
							eventMapStoreCall.put("Source", "OfferDetails");
							eventMapStoreCall.put("AreaName", dbUtil.getActiveArea());
							EventTracker.logEvent("StoreCall",eventMapStoreCall);
							calldailog.show();
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}
				});

				animate(gotoStore).setDuration(200);
				gotoStore.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent arg1) {
						// TODO Auto-generated method stub

						if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
							animate(gotoStore).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
						} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
							animate(gotoStore).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}
						else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
						{
							animate(gotoStore).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
						}

						return false;
					}
				});
				gotoStore.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(calldailog.isShowing())
						{
							calldailog.dismiss();
						}

						EventTracker.logEvent("OfferDetails_GotoStore", false);

						Intent intent=new Intent(context, StoreDetailsActivity.class);
						intent.putExtra("STORE_ID", PLACE_ID);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}
				});



			}

			if(fromCustomer && !fromNews)
			{
				postFav.setVisibility(View.VISIBLE);
				likeButton.setVisibility(View.VISIBLE);
				shareTextButton.setVisibility(View.GONE);
				shareButton.setVisibility(View.VISIBLE);
				merchantView.setVisibility(View.VISIBLE);
				byNowButton.setVisibility(View.VISIBLE);
				vertiSeprator.setVisibility(View.GONE);
				txtPostView.setVisibility(View.GONE);
				txtPostFav.setVisibility(View.GONE);
				txtPostShare.setVisibility(View.GONE);
				merchantView.setVisibility(View.GONE);
				imageSeprator.setVisibility(View.GONE);
				if(!isFromStoreDetails)
				{
					gotoStore.setVisibility(View.VISIBLE);
				}
				else
				{
					gotoStore.setVisibility(View.GONE);
				}

			} else if(fromNews)
			{
				postFav.setVisibility(View.GONE);
				likeButton.setVisibility(View.GONE);
				shareTextButton.setVisibility(View.GONE);
				shareButton.setVisibility(View.GONE);
				merchantView.setVisibility(View.GONE);
				byNowButton.setVisibility(View.GONE);
				vertiSeprator.setVisibility(View.GONE);
				txtPostView.setVisibility(View.GONE);
				txtPostFav.setVisibility(View.GONE);
				txtPostShare.setVisibility(View.GONE);
				merchantView.setVisibility(View.GONE);
				gotoStore.setVisibility(View.GONE);
				addToShopLocal.setVisibility(View.GONE);
				viewOverlay.setVisibility(View.GONE);
				imageSeprator.setVisibility(View.GONE);
			}
			else
			{

				postFav.setVisibility(View.GONE);
				likeButton.setVisibility(View.GONE);
				merchantView.setVisibility(View.VISIBLE);
				shareButton.setVisibility(View.GONE);
				shareTextButton.setVisibility(View.VISIBLE);
				byNowButton.setVisibility(View.GONE);
				viewOverlay.setVisibility(View.GONE);
				addToShopLocal.setVisibility(View.GONE);

			}

			animate(likeButton).setDuration(200);
			likeButton.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					// TODO Auto-generated method stub

					if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
						animate(likeButton).setInterpolator(sDecelerator).scaleX(0.7f).scaleY(0.7f);
					} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
						animate(likeButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
					}
					else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
					{
						animate(likeButton).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
					}
					return false;
				}
			});
			likeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{


						Log.i("Liked","Clicked "+isLoggedInCustomer+" USER_LIKE "+USER_LIKE);
						//showToast("clicked");
						//showToast("Clicked");
						if(session.isLoggedInCustomer())
						{
							getLike=true;
							likePost=true;
							if(USER_LIKE.equalsIgnoreCase("0"))
							{
								Log.i("Liked","Liked");
								//	showToast("Like");
								likePost();
							}
							else if(USER_LIKE.equalsIgnoreCase("1"))
							{
								Log.i("Liked","Unliked");
								//			showToast("Unlike");
								unLikePost();
							}
							else if(USER_LIKE.equalsIgnoreCase("-1"))
							{
								autoLike=true;
								loadBroadCast();
							}
						}
						else
						{
							Log.i("Liked","Clicked else");
							getLike=false;
							likePost=false;

							//							showToast("Please login to mark post as favourite");
							loginAlert(getResources().getString(R.string.postFavAlert),POST_LIKE);
						}

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			});

			if(photo_source!=null && photo_source.length()!=0 && !photo_source.equalsIgnoreCase("notfound"))
			{
				photo_source=photo_source.replaceAll(" ", "%20");
				imgStoreImage.setScaleType(ScaleType.FIT_XY);
				imgStoreImage.setVisibility(View.VISIBLE);
				imageRow.setVisibility(View.VISIBLE);


				/*imgStoreLoader.DisplayImage(PHOTO_PARENT_URL+photo_source, imgStoreImage);*/
				try{
					Picasso.with(context).load(PHOTO_PARENT_URL+photo_source)
					.placeholder(R.drawable.ic_place_holder)
					.error(R.drawable.ic_place_holder)
					.into(imgStoreImage);
				}
				catch(IllegalArgumentException illegalArg){
					illegalArg.printStackTrace();
				}
				catch(Exception e){ 
					e.printStackTrace();
				}

				Log.i("PHTO ", "PHOTO ---------"+PHOTO_PARENT_URL+photo_source);



			}

			/*else if(url_source!=null && url_source.length()!=0)
			{
				url_source=url_source.replaceAll(" ", "%20");


				imgStoreImage.setVisibility(View.GONE);
				imgStoreLoader.DisplayImage(url_source, imgStoreImage);

				Log.i("PHTO ", "PHOTO ---------"+PHOTO_PARENT_URL+url_source);
			}
			else if(photo_link!=null && photo_link.length()!=0)
			{
				photo_link=photo_link.replaceAll(" ", "%20");

				imgStoreImage.setVisibility(View.GONE);

				imgStoreLoader.DisplayImage(photo_link, imgStoreImage);

				Log.i("PHTO ", "PHOTO ---------"+PHOTO_PARENT_URL+photo_link);
			}*/
			else
			{

				imgStoreImage.setVisibility(View.GONE);
				imgStoreImage.setScaleType(ScaleType.FIT_CENTER);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, storeName);


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}
			}

			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}


		/** CUstom share dialog */
		dialogShare = new Dialog(context);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));

	}

	void showDialog(){
		try
		{
			dialogShare.show();

			listViewShare.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {

					String app = appName.get(position);
					String shareBody= "";

					String validTill="";

					String contact="";

					String contact_lable="\nContact:";

					Log.i("is_offer", "is_offer valid "+is_offer+" valid till "+valid_till);

					if(is_offer.length()!=0 && is_offer.equalsIgnoreCase("1"))
					{
						try
						{
							DateFormat dt=new SimpleDateFormat("yyyy-MM-dd");
							DateFormat dt2=new SimpleDateFormat("MMM");
							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

							Date date_con = (Date) dt.parse(valid_till.toString());

							Date date=dateFormat.parse(valid_till.toString());
							DateFormat date_format=new SimpleDateFormat("hh:mm a");

							Calendar cal = Calendar.getInstance();
							cal.setTime(date_con);
							int year = cal.get(Calendar.YEAR);
							int month = cal.get(Calendar.MONTH);
							int day = cal.get(Calendar.DAY_OF_MONTH);

							validTill="\nValid Till:"+day+" "+dt2.format(date_con)+"   "+date_format.format(date);

						}catch(Exception ex)
						{
							ex.printStackTrace();
							validTill="";
						}
						/*validTill="\nValid Till:"+valid_till.get(tempPos);*/
					}
					else
					{
						validTill="";
					}

					try
					{
						if(fromCustomer || fromNews)
						{

							if(mob1.length()>0)
							{
								contact="+"+mob1;
							} 
							if(mob2.length()>0)
							{
								contact+=" +"+mob2;
							}
							if(mob3.length()>0)
							{
								contact+=" +"+mob3;
							}
							if(tel1.length()>0)
							{
								contact+=" "+tel1;
							}
							if(tel2.length()>0)
							{
								contact+=" "+tel1;
							}
							if(tel3.length()>0)
							{
								contact+=" "+tel1;
							}

							if(contact.length()==0)
							{
								contact_lable="";
							}
						}
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}
					shareBody=storeName + " - " + "Offers" + " : " + "\n\n"+ txtBroadCastTitle.getText().toString() + "\n" + txtBroadCastBody.getText().toString()+validTill+contact_lable+contact+"\n(Shared via: http://www.shoplocal.co.in )";



					if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

						dialogShare.dismiss();

						if(photo_source.length()!=0)
						{
							String photo=photo_source.replaceAll(" ", "%20");

							FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
							.setLink("http://shoplocal.co.in")
							.setName(storeName + " - " + "Offers")
							.setPicture(PHOTO_PARENT_URL+photo)
							.setDescription(shareBody)
							.build();
							uiHelper.trackPendingDialogCall(shareDialog.present());
						}
						else
						{
							FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(context)
							.setLink("http://shoplocal.co.in")
							.setName(storeName + " - " + "Offers")
							.setDescription(shareBody)
							.build();
							uiHelper.trackPendingDialogCall(shareDialog.present());
						}
					}
					else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
						Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
					}

					else if(!app.equalsIgnoreCase("facebook")){
						Intent i = new Intent(Intent.ACTION_SEND);
						i.setPackage(packageNames.get(position));
						i.setType("text/plain");
						i.putExtra(Intent.EXTRA_SUBJECT,storeName);
						i.putExtra(Intent.EXTRA_TEXT, shareBody);
						startActivityForResult(i, 3);
					}

					dismissDialog();


				}
			});
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void dismissDialog(){
		dialogShare.dismiss();
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}


	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
		uiHelper.onResume();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	void loginAlert(String msg,final int like)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Store Details");
		alertDialogBuilder
		.setMessage(msg)
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
				if(like==PLACE_LIKE)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,4);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
				if(like==POST_LIKE)
				{
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					context.startActivityForResult(intent,2);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	void likePlace()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			EventTracker.logEvent("OfferDetails_StoreFavourited", false);
			Intent intent=new Intent(context, FavouritePlaceService.class);
			intent.putExtra("favourite",mFavourite);
			intent.putExtra("URL", GET_STORE_URL+LIKE_API);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("place_id", PLACE_ID);
			context.startService(intent);

		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void loadBroadCast()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, BroadCastService.class);
			intent.putExtra("broadcast",mBroadCast);
			intent.putExtra("URL", BROADCAST_URL+GET_BROADCAST_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("store_id", PLACE_ID);
			intent.putExtra("isSinglePostView", isSinglePostView);
			intent.putExtra("isLoggedIn", isLoggedInCustomer);
			intent.putExtra("singlePost", true);
			if(isSinglePostView && isLoggedInCustomer)
			{
				intent.putExtra("user_id",USER_ID);
			}
			intent.putExtra("post_id", POST_ID);
			if(getLike)
			{
				intent.putExtra("getLike",getLike);
				//intent.putExtra("user_id",USER_ID);
			}
			context.startService(intent);


		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));

		}
	}

	void likePost()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Map<String,String> eventLike=new HashMap<String, String>();
			eventLike.put("Source", "OfferDetails");
			eventLike.put("AreaName",dbUtil.getActiveArea());
			EventTracker.logEvent("Offer_Like", eventLike);
			
			
			Intent intent=new Intent(context, FavouritePostService.class);
			intent.putExtra("favouritePost",mFav);
			intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("post_id", POST_ID);
			context.startService(intent);

		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	void unLikePost()
	{
		if(isnetConnected.isNetworkAvailable())
		{

			Intent intent=new Intent(context, UnFavouritePostService.class);
			intent.putExtra("unfavouritePost",mUnfav);
			intent.putExtra("URL", BROADCAST_URL+"/"+LIKE_API);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("post_id", POST_ID);
			context.startService(intent);

		}
		else
		{
			showToast(getResources().getString(R.string.noInternetConnection));
		}
	}

	public static Bitmap drawShadow(Bitmap bitmap, int leftRightThk, int bottomThk, int padTop) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		int newW = w - (leftRightThk * 2);
		int newH = h - (bottomThk + padTop);

		Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		Bitmap bmp = Bitmap.createBitmap(w, h, conf);
		Bitmap sbmp = Bitmap.createScaledBitmap(bitmap, newW, newH, false);

		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		Canvas c = new Canvas(bmp);

		// Left
		int leftMargin = (leftRightThk + 7)/2;
		Shader lshader = new LinearGradient(0, 0, leftMargin, 0, Color.TRANSPARENT, Color.BLACK, TileMode.CLAMP);
		paint.setShader(lshader);
		c.drawRect(0, padTop, leftMargin, newH, paint); 

		// Right
		Shader rshader = new LinearGradient(w - leftMargin, 0, w, 0, Color.BLACK, Color.TRANSPARENT, TileMode.CLAMP);
		paint.setShader(rshader);
		c.drawRect(newW, padTop, w, newH, paint);

		// Bottom
		Shader bshader = new LinearGradient(0, newH, 0, bitmap.getHeight(), Color.BLACK, Color.TRANSPARENT, TileMode.CLAMP);
		paint.setShader(bshader);
		c.drawRect(leftMargin -3, newH, newW + leftMargin + 3, bitmap.getHeight(), paint);
		c.drawBitmap(sbmp, leftRightThk, 0, null);

		return bmp;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		/*	if(item.getTitle().toString().equalsIgnoreCase("Detail's"))
		{

		}*/
		try
		{
			setResult();
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		/*imgStoreLoader.clearCache();*/
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}



	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.FavouritePostResultReceiver.FavouritePostInterface#postLikeReuslt(int, android.os.Bundle)
	 */
	@Override
	public void postLikeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String postLikestatus=resultData.getString("postLikestatus");
			String postLikemsg=resultData.getString("postLikemsg");
			if(postLikestatus.equalsIgnoreCase("true"))
			{
				showToast(postLikemsg);
				//			postFav.setImageResource(R.drawable.postfav);
				USER_LIKE="1";
				try
				{
					int post_like=Integer.parseInt(likeButton.getText().toString());
					likeButton.setText((post_like+1)+"");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			else if(postLikestatus.equalsIgnoreCase("false"))
			{
				showToast(postLikemsg);
				//			if(postLikemsg.startsWith(getResources().getString(R.string.invalidAuth)))
				//			{
				//				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				//				context.startActivityForResult(intent,2);
				//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				//			}
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}

			}
			else if(postLikestatus.equalsIgnoreCase("error"))
			{
				showToast(postLikemsg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.UnFavouritePostReceiver.UnfavouritePostInterface#unFavouritePostResult(int, android.os.Bundle)
	 */
	@Override
	public void unFavouritePostResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String unlikestatus=resultData.getString("unlikestatus");
			String unlikemsg=resultData.getString("unlikemsg");
			if(unlikestatus.equalsIgnoreCase("true"))
			{
				//showToast(unlikemsg);
				//			postFav.setImageResource(R.drawable.postunfav);
				USER_LIKE="0";
				try
				{
					int post_like=Integer.parseInt(likeButton.getText().toString());
					likeButton.setText((post_like-1)+"");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			else if(unlikestatus.equalsIgnoreCase("false"))
			{
				showToast(unlikemsg);
				//			if(unlikemsg.equalsIgnoreCase(getResources().getString(R.string.invalidAuth)))
				//			{
				//				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				//				context.startActivityForResult(intent,2);
				//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				//			}
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}
			}
			else if(unlikestatus.equalsIgnoreCase("error"))
			{
				showToast(unlikemsg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.BroadCastApiReceiver.BroadCastApi#onReceiverBroadCasts(int, android.os.Bundle)
	 */
	@Override
	public void onReceiverBroadCasts(int resultCode, Bundle resultData) {

		try
		{

			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS");
			Log.i("Status ", "Status "+SEARCH_STATUS);
			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{
				ArrayList<GetBroadCast>getBroadcast=resultData.getParcelableArrayList("boradcast");

				ID.clear();
				TITLE.clear();
				BODY.clear();
				TYPE.clear();
				VIDEO_URL.clear();
				PHOTO_LINK.clear();
				/*			SOURCE_URL.clear();*/
				URL.clear();
				PHOTO_SOURCE.clear();
				DATE.clear();
				USERLIKE.clear();

				IMAGE_TITLE1.clear();

				IS_OFFER.clear();
				OFFER_DATE_TIME.clear();

				//Removing duplicates from arrayList
				/*	HashSet<String>hs=new HashSet<String>(TempTypeArray);
		TYPE.clear();
		TYPE.addAll(hs);
				 */


				for(int i=0;i<getBroadcast.size();i++)
				{
					ID.add(getBroadcast.get(i).getId());
					TITLE.add(getBroadcast.get(i).getTitle());
					TYPE.add(getBroadcast.get(i).getType());
					BODY.add(getBroadcast.get(i).getDescription());
					DATE.add(getBroadcast.get(i).getDate());
					USERLIKE.add(getBroadcast.get(i).getUser_like());
					TOTAL_LIKE.add(getBroadcast.get(i).getTotal_like());

					TOTAL_VIEWS.add(getBroadcast.get(i).getUser_views());

					TOTAL_SHARE.add(getBroadcast.get(i).getTotal_share());

					PHOTO_SOURCE.add(getBroadcast.get(i).getImage_url1());

					IMAGE_TITLE1.add(getBroadcast.get(i).getImage_title1());

					IS_OFFER.add(getBroadcast.get(i).getIs_offer());
					OFFER_DATE_TIME.add(getBroadcast.get(i).getOffer_date_time());



					//				for(int j=0;j<getBroadcast.get(i).getSource().size();j++)
					//				{
					//					PHOTO_SOURCE.add(getBroadcast.get(i).getSource().get(j));
					//				}
				}
				for(int i=0;i<ID.size();i++)
				{
					if(ID.get(i).equalsIgnoreCase(POST_ID))
					{
						USER_LIKE=USERLIKE.get(i);
						txtPostFav.setText(TOTAL_LIKE.get(i));
						txtPostShare.setText(TOTAL_SHARE.get(i));
						txtPostView.setText(TOTAL_VIEWS.get(i));
						caption=IMAGE_TITLE1.get(i);

						shareButton.setText(TOTAL_SHARE.get(i));
						likeButton.setText(TOTAL_LIKE.get(i));

						is_offer=IS_OFFER.get(i);
						valid_till=OFFER_DATE_TIME.get(i);
					}
				}

				Log.i("OFFER DATE TIME ", "OFFER DATE TIME "+is_offer+" "+valid_till);

				imgStoreImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						EventTracker.logEvent("OfferDetails_ImageViewed", false);

						ArrayList<String>GALLERY_PHOTO_SOURCE=new ArrayList<String>();
						ArrayList<String>GALLERY_PHOTO_CAPTION=new ArrayList<String>();
						GALLERY_PHOTO_SOURCE.add("");
						GALLERY_PHOTO_CAPTION.add("");

						GALLERY_PHOTO_SOURCE.add(photo_source);
						GALLERY_PHOTO_CAPTION.add(caption);
						Intent intent=new Intent(context,StorePagerGallery.class);
						intent.putExtra("photo_source", GALLERY_PHOTO_SOURCE);
						intent.putExtra("isCaption",true);
						intent.putExtra("caption", GALLERY_PHOTO_CAPTION);
						intent.putExtra("position", 1);
						startActivity(intent);	
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				});

				/*if(likePost && USER_LIKE.equalsIgnoreCase("0"))
			{
				likePost();
				getLike=false;
				likePost=false;
			}
			else if(likePost && USER_LIKE.equalsIgnoreCase("1"))
			{
				unLikePost();
				likePost=false;
				getLike=false;
			}*/
				Log.i("Intent Data USER_LIKE ", "Intent Data USER_LIKE REc "+USER_LIKE);
				if( USER_LIKE.equalsIgnoreCase("0"))
				{
					if(autoLike)
					{
						likePost();
						autoLike=false;
					}
					getLike=false;
					likePost=false;
				}
				else if(USER_LIKE.equalsIgnoreCase("1"))
				{
					if(autoLike)
					{
						unLikePost();
						autoLike=false;
					}
					likePost=false;
					getLike=false;
				}
			}
			else
			{

			}
		}catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}



	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		autoLike=false;
		setResult();
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

	}



	void setResult()
	{

		Log.i("OFFER DETAILS ", "OFFER DETAILS ID "+ID.size()+" "+TITLE.size()+" "+TYPE.size()+" "+BODY.size()+" "+DATE.size()+" "+USERLIKE.size()+" "+PHOTO_SOURCE.size());

		Intent intent=new Intent();
		intent.putStringArrayListExtra("POSTID", ID);
		intent.putStringArrayListExtra("TITLE", TITLE);
		intent.putStringArrayListExtra("TYPE", TYPE);
		intent.putStringArrayListExtra("BODY", BODY);
		intent.putStringArrayListExtra("DATE", DATE);
		intent.putStringArrayListExtra("USERLIKE", USERLIKE);
		intent.putStringArrayListExtra("PHOTO_SOURCE", PHOTO_SOURCE);
		setResult(RESULT_OK, intent);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		try
		{
			if(requestCode==2)
			{

				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				isLoggedInCustomer=session.isLoggedInCustomer();

				if(isLoggedInCustomer)
				{
					getLike=true;
					likePost=true;
					/*		getPerticularStoreDetails()*/;
					autoLike=true;
					loadBroadCast();
				}
				else
				{
					/*showToast("Please login to mark place as favourite");
					Intent intent=new Intent(context,LoginSignUpCustomer.class);
					startActivityForResult(intent,2);*/
				}
			}
			if(requestCode == 3)
			{
				VIA="Android";
				Log.d("TEST","TEST IS CACELLED");
				callShareApi();


			}
			if(requestCode == 4)
			{
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();
				isLoggedInCustomer=session.isLoggedInCustomer();
			}

			try
			{
				uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
					@Override
					public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
						Log.e("Activity", String.format("Error: %s", error.toString()));
					}

					@Override
					public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
						Log.i("Activity", "Success!");
						VIA="Android-Facebook";
						callShareApi();
					}
				});
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



		}catch(NullPointerException ex)
		{
			ex.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	private void callShareApi() {
		// TODO Auto-generated method stub
		try
		{
			Map<String,String> eventShare=new HashMap<String, String>();
			eventShare.put("Source", "OfferDetails");
			eventShare.put("AreaName",dbUtil.getActiveArea());
			EventTracker.logEvent("Offer_Share", eventShare);
			
			Intent intent = new Intent(context, ShareService.class);
			intent.putExtra("shareService", shareServiceResult);
			intent.putExtra("URL",BROADCAST_URL + SHARE_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			intent.putExtra("post_id", POST_ID);
			intent.putExtra("via", VIA);
			context.startService(intent);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public void onReceiveShareStatus(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub

		String status = resultData.getString("share_status");

		String msg = resultData.getString("share_msg");



		try {

			if(status.equalsIgnoreCase("true")){

				showToast(msg);
			}
			else{

				showToast(msg);
			}

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}

	}

	class CallListDialog extends ArrayAdapter<String>
	{
		ArrayList<String> callNumbers;
		Activity context;
		LayoutInflater inflator;

		public CallListDialog(Activity context, int resource, ArrayList<String> callNumbers) {
			super(context, resource, callNumbers);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();
			this.callNumbers=callNumbers;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return callNumbers.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			try
			{
				if(convertView==null)
				{
					CallListViewHolder holder=new CallListViewHolder();
					convertView=inflator.inflate(R.layout.calllistlayout, null);
					holder.contactNo=(TextView)convertView.findViewById(R.id.contactNo);

					convertView.setTag(holder);
				}

				CallListViewHolder hold=(CallListViewHolder) convertView.getTag();

				hold.contactNo.setText(callNumbers.get(position));
				hold.contactNo.setTypeface(tf);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return convertView;
		}



	}

	static class CallListViewHolder
	{
		TextView contactNo;
	}

	@Override
	public void likeReuslt(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String likestatus=resultData.getString("likestatus");
			String likemsg=resultData.getString("likemsg");
			if(likestatus.equalsIgnoreCase("true"))
			{
				showToast(likemsg);
				//			searchFav.setImageResource(R.drawable.ic_heart_filled);
			}
			else  if(likestatus.equalsIgnoreCase("error"))
			{
				showToast(likemsg);
				String error_code=resultData.getString("error_code");
				if(error_code.equalsIgnoreCase("-221"))
				{
					session.logoutCustomer();
				}
				//			if(likemsg.equalsIgnoreCase(getResources().getString(R.string.invalidAuth)))
				//			{
				//				loginAlert(getResources().getString(R.string.placeFavAlert),PLACE_LIKE);
				//			}
			}
			else if(likestatus.equalsIgnoreCase("error"))
			{
				showToast(likemsg);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}







}
