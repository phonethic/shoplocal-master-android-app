package com.phonethics.shoplocal;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.networkcall.MerchantLoginReceiver;
import com.phonethics.networkcall.MerchantRegister;
import com.phonethics.networkcall.MerchantResultReceiver;
import com.phonethics.networkcall.MerchantLoginReceiver.LoginReceiver;
import com.phonethics.networkcall.MerchantLoginService;
import com.phonethics.networkcall.MerchantResultReceiver.MerchantRegisterInterface;
/*import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;*/

public class SplitLoginSignUp extends SherlockActivity implements OnClickListener,LoginReceiver, MerchantRegisterInterface{

	ActionBar actionBar;
	Activity context;


	Button btnLoginMerchant,btnSingupMerchant,btnLoginCustomer,btnSignUpCustomer;

	//To check Internet Connectivity.
	NetworkCheck isnetConnected;


	//Login Recevier
	public MerchantLoginReceiver mLoginRecevier;
	static String API_HEADER;
	static String API_VALUE;
	static String LOGIN_PATH;
	static String LOGIN_URL;

	String USER_ID,AUTH_ID;

	//Login Dialog
	Dialog dialog;
	EditText mobileno,password;
	ProgressBar progLogin;

	//Session Manger Class
	SessionManager session;
	//User Name
	public static final String KEY_USER_NAME="mobileno";

	//Password
	public static final String KEY_PASSWORD="password";

	//Login back
	ImageView imgLoginBack;

	/*ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;*/

	RelativeLayout merchantLogin;

	TextView forgotPassword;
	TextView signUpText;
	TextView txtMerchantPlaceHolder;
	TextView txtHeaderInfo;

	RelativeLayout infoText;

	int signUp=0;

	Typeface tf;

	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	String mCountryCode;

	TextView countryPicker;

	AlertDialog 	alert			 =	null;
	CountryAdapter	adapter;
	ArrayList<String> countryarr;
	ArrayList<String> countryarrSearch;
	ArrayList<String> countryCode;
	ArrayList<String> countryCodeSearch;
	String	JSON_ARR_STRING;

	ImageView loginMerchant_info;

	TextView loginTextBoxCondition;
	TextView loginTextPrivacy;


	Dialog dailog;

	Dialog privacy_dailog;


	WebView web;

	WebView privacyWeb;

	CheckBox chkBoxAccept;
	Button btnTermDone;
	Button btnTermCancel;

	Button buttonOk;

	String PAGER_STATE_PREF="PAGER_STATE_PREF";

	HashMap<String, String> user_details;

	boolean isAddStore=false;

	MerchantResultReceiver mRegister;
	String REGISTER_URL;
	String REGISTER_PATH;

	EditText edtDialogMerchantName;
	Dialog disclaimer;

	Button btnDesclaimer;
	TextView desclaimerText;
	TextView privacy;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_split_login_sign_up);
		//Creating Action Bar
		actionBar=getSupportActionBar();
		actionBar.setTitle("Merchant Login");
		//commented 
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.show();

		context=this;
		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");
		Bundle b=getIntent().getExtras();

		mRegister=new MerchantResultReceiver(new Handler());
		mRegister.setReceiver(this);

		REGISTER_URL=getResources().getString(R.string.server_url) + getResources().getString(R.string.merchant_api);
		REGISTER_PATH=getResources().getString(R.string.new_merchant);

		dailog=new Dialog(context);
		privacy_dailog=new Dialog(context);


		privacy_dailog.setContentView(R.layout.privacy_policy);
		privacy_dailog.setTitle("Privacy Policy");
		privacy_dailog.setCancelable(true);
		privacy_dailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		dailog.setContentView(R.layout.termsandcondition);
		dailog.setTitle("Terms & Conditions");
		dailog.setCancelable(false);
		dailog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		//disclaimer
		//Dialog
		disclaimer=new Dialog(context);
		disclaimer.setContentView(R.layout.disclaimer_dialog);
		disclaimer.setTitle("Why should I login?");
		disclaimer.setCancelable(true);
		disclaimer.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		btnDesclaimer=(Button)disclaimer.findViewById(R.id.btnDesclaimer);
		desclaimerText=(TextView)disclaimer.findViewById(R.id.desclaimerText);
		desclaimerText.setText(getResources().getString(R.string.mobile_desclaimer));
		privacy=(TextView)disclaimer.findViewById(R.id.privacy);
		privacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));

		web=(WebView)dailog.findViewById(R.id.tcWeb);
		web.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.tcURl));

		privacyWeb=(WebView)privacy_dailog.findViewById(R.id.privacyWeb);
		privacyWeb.loadUrl(getResources().getString(R.string.base_url)+getResources().getString(R.string.privacy_url));

		chkBoxAccept=(CheckBox)dailog.findViewById(R.id.chkBoxAccept);
		btnTermDone=(Button)dailog.findViewById(R.id.btnTermDone);
		btnTermCancel=(Button)dailog.findViewById(R.id.btnTermCancel);

		buttonOk=(Button)privacy_dailog.findViewById(R.id.buttonOk);



		countryPicker=(TextView)findViewById(R.id.countryPicker);

		forgotPassword=(TextView)findViewById(R.id.forgotPassword);
		signUpText=(TextView)findViewById(R.id.signUpText);
		txtMerchantPlaceHolder=(TextView)findViewById(R.id.txtMerchantPlaceHolder);

		txtHeaderInfo=(TextView)findViewById(R.id.txtHeaderInfo);

		loginMerchant_info=(ImageView)findViewById(R.id.loginMerchant_info);
		loginTextBoxCondition=(TextView)findViewById(R.id.loginTextBoxCondition);
		loginTextPrivacy=(TextView)findViewById(R.id.loginTextPrivacy);

		edtDialogMerchantName=(EditText)findViewById(R.id.edtDialogMerchantName);

		loginTextPrivacy.setText(Html.fromHtml("<u>Privacy Policy</u>"));
		loginTextBoxCondition.setText(Html.fromHtml("<u>Terms & Conditions</u>"));

		loginMerchant_info.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getResources().getString(R.string.mobile_desclaimer))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {

						dialog.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		});

		buttonOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				privacy_dailog.dismiss();
			}
		});


		loginTextPrivacy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				privacy_dailog.show();
			}
		});

		privacy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				disclaimer.dismiss();
				privacy_dailog.show();
			}
		});


		txtHeaderInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				disclaimer.show();
			}
		});

		btnDesclaimer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				disclaimer.dismiss();
			}
		});


		//chkBoxAccept.setChecked(true);
		btnTermDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chkBoxAccept.setChecked(true);
				if(chkBoxAccept.isChecked())
				{
					dailog.dismiss();
				}
				proceedWithoutNext();

			}
		});
		btnTermCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dailog.dismiss();
				chkBoxAccept.setChecked(false);
				AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
				alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
				alertDialogBuilder3
				.setMessage(getString(R.string.terms_and_condition_retry))
				.setIcon(R.drawable.ic_launcher)
				.setCancelable(false)
				.setPositiveButton("Try again",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog2,int id) {

						dailog.show();
					}
				})
				.setNegativeButton("Do not register",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog2,int id) {
						// TODO Auto-generated method stub
						dialog2.dismiss();
					}
				})
				;

				AlertDialog alertDialog3 = alertDialogBuilder3.create();

				alertDialog3.show();
			}
		});



		if(b!=null)
		{
			signUp=b.getInt("signUp");
		}

		try
		{
			JSON_ARR_STRING = getText();
			getCountryCode();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		dialog=new Dialog(context);
		dialog.setContentView(R.layout.logindialoge);
		dialog.setTitle("Login");



		//Network Check
		isnetConnected=new NetworkCheck(context);

		infoText=(RelativeLayout)findViewById(R.id.infoText);

		if(!getResources().getBoolean(R.bool.isAppShopLocal))
		{
			infoText.setVisibility(View.GONE);
		}

		//image loader
		//Image Loader
		/*imageLoader=ImageLoader.getInstance();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.hyperlocalcache");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}



		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();*/

		// Session Manager
		session = new SessionManager(getApplicationContext()); 



		//Initializing Login Service Reciver
		mLoginRecevier=new MerchantLoginReceiver(new Handler());
		mLoginRecevier.setReceiver(this);

		//Defining URL's
		LOGIN_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.merchant_api);
		LOGIN_PATH=getResources().getString(R.string.login);



		//API KEY'S
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);


		merchantLogin=(RelativeLayout)findViewById(R.id.merchantLogin);
		merchantLogin.bringToFront();

		btnLoginMerchant=(Button)findViewById(R.id.btnLoginMerchant);
		btnSingupMerchant=(Button)findViewById(R.id.btnSingupMerchant);
		btnSingupMerchant.setText("Proceed");
		/*		btnLoginCustomer=(Button)findViewById(R.id.btnLoginCustomer);


		btnSignUpCustomer=(Button)findViewById(R.id.btnSingupCustomer);*/


		/*signUpText.getBackground().setAlpha(130);
		forgotPassword.getBackground().setAlpha(130);*/

		signUpText.setTypeface(tf);
		forgotPassword.setTypeface(tf);
		txtMerchantPlaceHolder.setTypeface(tf);

		forgotPassword.setTextColor(Color.parseColor("#c4c4c4"));
		signUpText.setTextColor(Color.parseColor("#c4c4c4"));

		mobileno=(EditText)findViewById(R.id.edtDialogMobileNo);
		password=(EditText)findViewById(R.id.edtDialogPassword);
		progLogin=(ProgressBar)findViewById(R.id.progLogin);


		mobileno.setTypeface(tf);
		password.setTypeface(tf);

		edtDialogMerchantName.setTypeface(tf);
		edtDialogMerchantName.setHint("Owner/Manager's Name");

		try
		{
			forgotPassword.setText(Html.fromHtml("<u>"+getResources().getString(R.string.forgot_password)+"</u>"));
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		mobileno.setTextColor(Color.parseColor("#9c9c9c"));
		/*signUpText.setTextColor(Color.parseColor("#715b57"));*/
		password.setTextColor(Color.parseColor("#9c9c9c"));
		txtMerchantPlaceHolder.setTextColor(Color.parseColor("#715b57"));
		countryPicker.setTextColor(Color.parseColor("#9c9c9c"));

		edtDialogMerchantName.setTextColor(Color.parseColor("#9c9c9c"));

		mobileno.setSelection(mobileno.getText().length());

		if(!getResources().getBoolean(R.bool.isAppShopLocal))
		{
			btnSingupMerchant.setVisibility(View.GONE);
			forgotPassword.setVisibility(View.GONE);
		}


		animate(btnLoginMerchant).setDuration(200);
		btnLoginMerchant.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnLoginMerchant).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnLoginMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(btnLoginMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		animate(btnSingupMerchant).setDuration(200);
		btnSingupMerchant.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(btnSingupMerchant).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(btnSingupMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(btnSingupMerchant).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				return false;
			}
		});

		animate(forgotPassword).setDuration(200);
		forgotPassword.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				/*vibrate.vibrate(50);*/
				if(arg1.getAction()==MotionEvent.ACTION_DOWN)
				{
					animate(forgotPassword).setInterpolator(sDecelerator).scaleX(0.9f).scaleY(0.9f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_UP)
				{
					animate(forgotPassword).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}
				else if(arg1.getAction()==MotionEvent.ACTION_CANCEL)
				{
					animate(forgotPassword).setInterpolator(sOvershooter).scaleX(1f).scaleY(1f);
				}

				return false;
			}
		});

		countryPicker.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ParseJsonData(JSON_ARR_STRING);
				alert.show();
			}
		});


		//Adding Click Listeners
		btnLoginMerchant.setOnClickListener(this);
		/*	btnLoginCustomer.setOnClickListener(this);
		btnSignUpCustomer.setOnClickListener(this);*/
		btnSingupMerchant.setOnClickListener(this);
		forgotPassword.setOnClickListener(this);

		imgLoginBack=(ImageView)findViewById(R.id.imgloginBack);


		user_details=new HashMap<String, String>();
		user_details=session.getUserDetails();

		Log.i("MOBILE PREFS", "MOBILE PREFS MER"+user_details.get(KEY_USER_NAME).toString());
		if(user_details.get(KEY_USER_NAME)!="" && user_details.get(KEY_USER_NAME).toString().length()!=0)
		{
			//			if(user_details.get(KEY_USER_NAME).toString().toString().startsWith("91"))
			//			{
			//				mobileno.setText(user_details.get(KEY_USER_NAME).toString().substring(2));
			//			}
			Log.i("MOBILE PREFS", "MOBILE PREFS "+user_details.get(KEY_USER_NAME).toString().substring(mCountryCode.length()));

			Log.i("MOBILE PREFS", "MOBILE PREFS Name "+user_details.get(KEY_USER_NAME).toString());
			mobileno.setText(user_details.get(KEY_USER_NAME).toString().substring(mCountryCode.length()));
			//password.setText(user_details.get(KEY_PASSWORD).toString());
		}


		if(isnetConnected.isNetworkAvailable())
		{
			/*imgLoad.DisplayImage("http://www.hdwallpapersarena.com/wp-content/uploads/2012/07/nature-wallpaper-1366x768-009.jpg",imgLoginBack);*/
			/*imageLoader.displayImage("http://cooltwitterbackgrounds.com/wp-content/uploads/2011/11/City_Perspective_1920x1200_cool_twitter_backgrounds.jpg",imgLoginBack);*/
			//	imageLoader.displayImage("http://www.usapics.net/wp-content/uploads/2012/03/new-york-city-sunrise-new-york.jpg",imgLoginBack);
			/*imageLoader.displayImage("http://wallcapture.com/wp-content/uploads/2013/05/London-City-Twitter-Backgrounds-HD-Free.jpg",imgLoginBack);*/

		}

	}
	//onCreate Ends here


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}




	public void setText(String country, String code){

		try
		{

			countryPicker.setText(code);
			mCountryCode=code.substring(1);
			setPrefernces();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public String getText(){
		String fileString = "";
		try{
			InputStream is = context.getAssets().open("county_code.txt");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			fileString = new String(buffer,"UTF-8");
			Log.d("", "JsonString >> "+fileString);
		}catch(Exception ex){
			ex.printStackTrace();
		}




		return fileString;
	}


	public void ParseJsonData(String jsonString){

		/*countryModels 		= new ArrayList<CountryModel>();*/
		countryarr 			= new ArrayList<String>();
		countryCode 		= new ArrayList<String>();


		try{
			JSONArray jArr = new JSONArray(jsonString);
			for(int i=0;i<jArr.length();i++){
				CountryModel	countryModel = new CountryModel();
				JSONObject jObj = jArr.getJSONObject(i);
				countryModel.setcName(jObj.getString("name"));
				countryarr.add(jObj.getString("name"));
				countryModel.setcCode(jObj.getString("dial_code"));
				countryCode.add(jObj.getString("dial_code").replace(" ", ""));
				countryModel.setcShort(jObj.getString("code"));
				/*countryModels.add(countryModel);*/
			}

			countryarrSearch = countryarr;
			countryCodeSearch = countryCode;
			initDialog();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void initDialog(){

		try
		{
			AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle("Select County Code");
			alertDialog.setCancelable(true);
			View view = context.getLayoutInflater().inflate(R.layout.dialog_list, null);
			final ListView listView = (ListView) view.findViewById(R.id.list_county_code);
			final EditText editSearch = (EditText) view.findViewById(R.id.edit_search);

			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
						long arg3) {
					// TODO Auto-generated method stub
					//setText(countryModels.get(pos).getcName(), countryModels.get(pos).getcCode());
					setText(countryarrSearch.get(pos), countryCodeSearch.get(pos));

					alert.dismiss();

				}
			});
			adapter = new CountryAdapter(context, countryarr,countryCode);
			listView.setAdapter(adapter);
			editSearch.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub

					/*adapter.getFilter().filter(s);
					String newData = editSearch.getText().toString();
					Log.d("", "EditText >> "+newData);
					for(int i=0;i<countryarr.size();i++){
						if(countryarr.get(i).contains(newData)){
							countryarrSearch.add(countryarr.get(i));
						}
					}*/


					ArrayList<String> countryarrSearch_1 = new ArrayList<String>();
					ArrayList<String> countryCodeSearch_1 = new ArrayList<String>();

					int textLength 							 = editSearch.getText().toString().length();

					for(int i=0;i<countryarr.size();i++){
						if(textLength<= countryarr.get(i).length()){
							if(editSearch.getText().toString().equalsIgnoreCase((String)countryarr.get(i).subSequence(0, textLength))){
								countryarrSearch_1.add(countryarr.get(i));
								countryCodeSearch_1.add(countryCode.get(i));						
							}
						}
					}

					countryarrSearch = countryarrSearch_1;
					countryCodeSearch = countryCodeSearch_1;

					adapter = new CountryAdapter(context,  countryarrSearch,countryCodeSearch);
					adapter.notifyDataSetChanged();
					listView.setAdapter(adapter);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			alertDialog.setView(view);
			alert = alertDialog.create();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void setPrefernces()
	{
		SharedPreferences pref=context.getSharedPreferences("merchantCountryCode", 0);
		Editor editor=pref.edit();
		editor.putString("mCountryCode", mCountryCode);
		editor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		finishTo();
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==btnLoginMerchant.getId())
		{


			/*WindowManager.LayoutParams params = new WindowManager.LayoutParams();
			params.y = 100; 
			params.width=RelativeLayout.LayoutParams.WRAP_CONTENT;
			params.height=RelativeLayout.LayoutParams.WRAP_CONTENT;
			params.gravity = Gravity.TOP | Gravity.RIGHT;       
			dialog.getWindow().setAttributes(params);*/

			/*dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;


			dialog.show();
			mobileno=(EditText)dialog.findViewById(R.id.edtDialogMobileNo);
			password=(EditText)dialog.findViewById(R.id.edtDialogPassword);
			progLogin=(ProgressBar)dialog.findViewById(R.id.progLogin);
			Button btnLogin=(Button)dialog.findViewById(R.id.btnLoginDialogMerchant);
			Button btnCacel=(Button)dialog.findViewById(R.id.btnLoginDialogCalcel);*/

			/*if(session.isLoggedIn())
			{
				HashMap<String, String> user_details=new HashMap<String, String>();
				user_details=session.getUserDetails();
				mobileno.setText(user_details.get(KEY_USER_NAME).toString());
				//password.setText(user_details.get(KEY_PASSWORD).toString());

			}*/

			if(mobileno.getText().toString().length()==0)
			{
				Toast.makeText(context, getResources().getString(R.string.mobileAlert), Toast.LENGTH_SHORT).show();
			}
			else if(mobileno.getText().toString().length()<10)
			{
				Toast.makeText(context, "Please enter correct Mobile no.", Toast.LENGTH_SHORT).show();
			}
			else if(password.getText().toString().length()==0)
			{
				Toast.makeText(context, "Please enter password", Toast.LENGTH_SHORT).show();
			}
			else if(password.getText().toString().length()<6)
			{
				Toast.makeText(context, "Password must be atleast 6 character's long", Toast.LENGTH_SHORT).show();
			}
			else
			{
				if(isnetConnected.isNetworkAvailable())
				{
					if(!progLogin.isShown())
					{
						Intent intent=new Intent(context, MerchantLoginService.class);
						intent.putExtra("merchantLogin", mLoginRecevier);
						intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
						intent.putExtra("api_header",API_HEADER);
						intent.putExtra("api_header_value", API_VALUE);
						intent.putExtra("contact_no", mCountryCode+mobileno.getText().toString());
						intent.putExtra("password", password.getText().toString());
						context.startService(intent);
						progLogin.setVisibility(View.VISIBLE);
					}
					else
					{
						MyToast.showToast(context, "Please wait while loging in", 2);
					}
				}
				else
				{
					Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();
				}
			}

			/*btnLogin.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(mobileno.length()==0)
					{
						Toast.makeText(context, "Please enter Mobile no.", Toast.LENGTH_SHORT).show();
					}
					else if(mobileno.length()<10)
					{
						Toast.makeText(context, "Please enter correct Mobile no.", Toast.LENGTH_SHORT).show();
					}
					else if(password.length()==0)
					{
						Toast.makeText(context, "Please enter password", Toast.LENGTH_SHORT).show();
					}
					else if(password.length()<6)
					{
						Toast.makeText(context, "Please must be atleast 6 character's long", Toast.LENGTH_SHORT).show();
					}
					else
					{
						if(isnetConnected.isNetworkAvailable())
						{
							Intent intent=new Intent(context, MerchantLoginService.class);
							intent.putExtra("merchantLogin", mLoginRecevier);
							intent.putExtra("URL", LOGIN_URL+LOGIN_PATH);
							intent.putExtra("api_header",API_HEADER);
							intent.putExtra("api_header_value", API_VALUE);
							intent.putExtra("contact_no", mobileno.getText().toString());
							intent.putExtra("password", password.getText().toString());
							context.startService(intent);
							progLogin.setVisibility(View.VISIBLE);

						}
						else
						{
							Toast.makeText(context, "No internet connection.", Toast.LENGTH_SHORT).show();
						}
					}
				}
			});

			btnCacel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});*/


		}





		if(v.getId()==btnSingupMerchant.getId())
		{
			if(isnetConnected.isNetworkAvailable())
			{
				proceed();
			}
			else
			{
				Toast.makeText(context, getResources().getString(R.string.noInternetConnection), Toast.LENGTH_LONG).show();
			}


		}
		if(v.getId()==forgotPassword.getId())
		{

			/*Intent intent=new Intent(this, ShopLocalMain.class);
			intent.putExtra("ForgetPassword", 1);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);*/

		}
	}

	void proceed()
	{
		if(mobileno.getText().toString().length()==0)
		{
			Toast.makeText(context, getResources().getString(R.string.mobileAlert), Toast.LENGTH_SHORT).show();
		}
		else if(mobileno.getText().toString().length()<10)
		{
			Toast.makeText(context, getResources().getString(R.string.mobileLengthAlert), Toast.LENGTH_SHORT).show();
		}
		else
		{
			if(chkBoxAccept.isChecked())
			{
				user_details=session.getUserDetails();
				//				Log.i("Mobile NO ", "Mobile No "+user_details.get(KEY_USER_NAME).toString().substring(mCountryCode.length())+" == "+mobileno.getText().toString());
				//				if(mobileno.getText().toString().equalsIgnoreCase(user_details.get(KEY_USER_NAME).toString().substring(mCountryCode.length())))
				//				{
				//
				//				}
				//				else
				//				{
				//					clearPageState();
				//				}

//				EventTracker.logEvent("MSignUp_Proceed", false);

				//				Intent intent=new Intent(this, ShopLocalMain.class);
				//				intent.putExtra("countryCode", mCountryCode);
				//				intent.putExtra("mobileno", mobileno.getText().toString());
				//				startActivityForResult(intent, 4);
				//				//			finish();
				//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

				callRegistrationApi();
			}
			else
			{
				dailog.show();
			}

		}
	}

	void proceedWithoutNext()
	{
		if(mobileno.getText().toString().length()==0)
		{
			//		Toast.makeText(context, "Please enter Mobile no.", Toast.LENGTH_SHORT).show();
		}
		else if(mobileno.getText().toString().length()<10)
		{
			//		Toast.makeText(context, "Please enter correct Mobile no.", Toast.LENGTH_SHORT).show();
		}
		else
		{
			if(chkBoxAccept.isChecked())
			{
				//				user_details=session.getUserDetails();
				//				Log.i("Mobile NO ", "Mobile No "+user_details.get(KEY_USER_NAME).toString().substring(mCountryCode.length())+" == "+mobileno.getText().toString());
				//				if(mobileno.getText().toString().equalsIgnoreCase(user_details.get(KEY_USER_NAME).toString().substring(mCountryCode.length())))
				//				{
				//
				//				}
				//				else
				//				{
				////					clearPageState();
				//				}

			/*	EventTracker.logEvent("MSignUp_Proceed", false);*/

				callRegistrationApi();

				//				Intent intent=new Intent(this, ShopLocalMain.class);
				//				intent.putExtra("countryCode", mCountryCode);
				//				intent.putExtra("mobileno", mobileno.getText().toString());
				//				startActivityForResult(intent, 4);
				//				//			finish();
				//				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
			//			else
			//			{
			//				dailog.show();
			//			}

		}
	}

	void callRegistrationApi()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			if(!progLogin.isShown())
			{
				EventTracker.logEvent("MSignUp_Proceed", false);

				//Calling Register Api to Register Merchant.
				Intent intent=new Intent(context, MerchantRegister.class);
				intent.putExtra("merchantRegister", mRegister);
				intent.putExtra("URL", REGISTER_URL+REGISTER_PATH);
				intent.putExtra("api_header",API_HEADER);
				intent.putExtra("api_header_value", API_VALUE);
				intent.putExtra("isMerchant",true);
				intent.putExtra("merchant_name", edtDialogMerchantName.getText().toString());
				intent.putExtra("mobile_no",mCountryCode+mobileno.getText().toString());
				context.startService(intent);
				progLogin.setVisibility(View.VISIBLE);
				btnSingupMerchant.setEnabled(false);
			}
			else
			{
				/*showToast("Wait");*/
			}
		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
	}
	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
	void clearPageState()
	{
		try
		{

			SharedPreferences prefs=context.getSharedPreferences(PAGER_STATE_PREF, MODE_PRIVATE);
			Editor editor=prefs.edit();
			editor.clear();
			editor.clear();
			editor.commit();

			Log.i("CLeARED","CLEARED");

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Login Result
	@Override
	public void onReceiveLoginResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String auth_id=resultData.getString("login_authCode");
			String loginstatus=resultData.getString("loginstatus");
			String loginid=resultData.getString("loginid");

			Log.i("Data From Server-------", "Server -----loginstatus "+resultData.getString("loginstatus"));
			Log.i("Data From Server-------", "Server -----loginid "+loginid);
			Log.i("Data From Server-------", "Server -----loginauth_id "+auth_id);

			USER_ID=loginid;
			AUTH_ID=auth_id;
			progLogin.setVisibility(View.INVISIBLE);
			if(loginstatus.equalsIgnoreCase("true"))
			{

				session.createLoginSession(mCountryCode+mobileno.getText().toString(), password.getText().toString(), USER_ID, AUTH_ID);
				Intent intent=new Intent(this, PlaceChooser.class);
				intent.putExtra("isSplitLogin",true);
				intent.putExtra("choiceMode", 2);
				intent.putExtra("viewPost", 0);
				/*				intent.putExtra("USER_ID", USER_ID);
				intent.putExtra("AUTH_ID", AUTH_ID);*/
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
			else 
			{
				Toast.makeText(context,auth_id, Toast.LENGTH_SHORT).show();
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishTo();
	}

	void finishTo()
	{
		//		Intent intent=new Intent(context, NewsFeedActivity.class);
		//		startActivity(intent);
		//		this.finish();
		//		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

		Intent intent=new Intent();
		intent.putExtra("isAddStore", isAddStore);
		setResult(4, intent);
		this.finish();

	}

	void getCountryCode()
	{
		SharedPreferences pref=context.getSharedPreferences("merchantCountryCode",0);
		mCountryCode=pref.getString("mCountryCode", "91");
		countryPicker.setText("+"+mCountryCode);

		Log.i("Countrycode found","Countrycode found "+mCountryCode);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{
				isAddStore=data.getBooleanExtra("isAddStore",false);
				finishTo();
				overridePendingTransition(0,R.anim.shrink_fade_out_center);

			}
		}
	}


	/*
	 * (non-Javadoc)
	 * @see com.phonethics.networkcall.MerchantResultReceiver.MerchantRegister#onReceiverMerchantRegister(int, android.os.Bundle)
	 */
	@Override
	public void onReceiverMerchantRegister(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			btnSingupMerchant.setEnabled(true);
			
			progLogin.setVisibility(View.GONE);
			String passstatus=resultData.getString("passstatus");
			String passmsg=resultData.getString("passmsg");
			String error_code=resultData.getString("error_code");

			if(error_code.equalsIgnoreCase("-119")) //New user
			{
				EventTracker.logEvent("MSignUp_New", false);

				Log.i("Signup ", "MSignup success true");
			}
			else if(error_code.equalsIgnoreCase("-102")) //Returning user
			{
				EventTracker.logEvent("MSignUp_Return", false);

				Log.i("Signup ", "MSignup success false");
			}

			if(passstatus.equalsIgnoreCase("true"))
			{
				Intent intent=new Intent(this, SignupMerchant.class);
				intent.putExtra("server_message", passmsg);
				intent.putExtra("mobileno", mCountryCode+mobileno.getText().toString());
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				showToast(passmsg);
			}
		}catch(Exception  ex)
		{
			ex.printStackTrace();
		}
	}



}
