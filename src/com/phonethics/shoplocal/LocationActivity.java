package com.phonethics.shoplocal;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.phonethics.adapters.DrawerExpandibleAdapter;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.EntryItem;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.model.Item;
import com.phonethics.networkcall.GetAreaResultReceiver;
import com.phonethics.networkcall.GetAreaResultReceiver.GetArea;
import com.phonethics.networkcall.GetAreaService;
import com.phonethics.networkcall.LocationIntentService;
import com.phonethics.networkcall.LocationResultReceiver;
import com.phonethics.networkcall.LocationResultReceiver.Receiver;
import com.urbanairship.push.PushManager;



public class LocationActivity extends SherlockFragmentActivity implements GetArea, Receiver, OnItemClickListener, OnClickListener {

	DrawerExpandibleAdapter drawerAdapter;
	ExpandableListView drawerList;

	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();

	Activity context;
	Context aContext;

	SessionManager session;

	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	ActionBar actionBar;

	ArrayList<Item> items=new ArrayList<Item>();

	ArrayList<Integer>list_icons=new ArrayList<Integer>();


	private static long back_pressed;

	String drawer_item="";



	//Api Urls
	static String API_HEADER;
	static String API_VALUE;

	static String STORE_URL;
	static String AREA_URL;
	static String ISO_URL;

	String countryCode;

	GetAreaResultReceiver getAreaRec; 

	DBUtil dbutil;

	NetworkCheck network;

	SeekBar distanceBar;
	TextView textChooseLocation;
	TextView textDistanceValue;
	ProgressBar locationProgress;

	Dialog locationDialog;

	ListView locationList;

	ArrayList<String> allAreas=new ArrayList<String>();
	ArrayList<String> allPinCodes=new ArrayList<String>();
	ArrayList<String> allCities=new ArrayList<String>();
	ArrayList<String> allAreaIds=new ArrayList<String>();
	ArrayList<String> allLat=new ArrayList<String>();
	ArrayList<String> allLon=new ArrayList<String>();
	ArrayList<String> allCountry=new ArrayList<String>();
	ArrayList<String> allCountryCode=new ArrayList<String>();
	ArrayList<String> allIsoCode=new ArrayList<String>();
	ArrayList<String> allPlaceId=new ArrayList<String>();
	ArrayList<String> state = new ArrayList<String>();
	ArrayList<String> locality = new ArrayList<String>();
	ArrayList<String> published_status = new ArrayList<String>();

	long activeAreaRowCount;

	String AREA_DATE_TIME_STAMP="AREA_DATE_TIME_STAMP";
	String ACTIVE_AREA_PREF="ACTIVE_AREA";
	String ACTIVE_AREA_KEY="AREA_ID";

	SimpleDateFormat format;
	String date_time;
	String KEY="DATE_TIME_STAMP";
	long diffDays;

	Map<String, String> eventTrackingParam;


	//location
	public static final String SHARED_PREFERENCES_NAME = "save_lati_longi";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";

	LocationManager 	locationManager;
	Location 			location;
	String 				provider;

	double 				latitude,longitude, saved_lati, saved_longi;

	LatLng 				latLng;
	Dialog dialog;
	TextView textAcc ;

	boolean isGPSEnabled;
	boolean isNetworkEnabled;

	LocationResultReceiver mLocation;

	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	File cacheDir;

	ImageView locationMap;
	Button gotoWelcome;

	ArrayList<String> urls=new ArrayList<String>();

	static String IMAGE_URL="";

	Handler handler ;
	Runnable runnable;


	TextView startKm;
	TextView endKm;
	TextView defineYourRadious;

	Typeface tf;

	long update;
	int place_Status=1;

	AreaBroadCast areaBroadCast;

	DrawerClass drawerClass;


	View footerView;
	TextView list_item_footerMore;
	TextView list_item_footerLess;

	boolean moreData=false;

	private UiLifecycleHelper uiHelper;

	/* Custom dialog for share */
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	String shareText = "Hi, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself.";
	Dialog dialogShare;
	ListView listViewShare;

	/** Check if facebook is present in phone */
	boolean isFacebookPresent = false;

	LocalNotification localNotification;

	String activeAreaId="";
	String selectedAreaId="";

	//User Id
	public static final String KEY_USER_ID_CUSTOMER="user_id_CUSTOMER";

	//Auth Id
	public static final String KEY_AUTH_ID_CUSTOMER="auth_id_CUSTOMER";

	String USER_ID="";
	String AUTH_ID="";

	//Active Area URL
	String URL="";

	//Active Area
	//	ActiveArea activeAreaAsync;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);



		//		activeAreaAsync=new ActiveArea();

		context=this;

		localNotification=new LocalNotification(getApplicationContext());

		drawerClass=new DrawerClass(context);

		//facebook
		uiHelper = new UiLifecycleHelper(context, null);
		uiHelper.onCreate(savedInstanceState);

		mLocation=new LocationResultReceiver(new Handler());
		network=new NetworkCheck(context);
		//		handler = new Handler();
		tf=Typeface.createFromAsset(getAssets(), "fonts/GOTHIC_0.TTF");

		imageLoader=ImageLoader.getInstance();

		IMAGE_URL=getResources().getString(R.string.photo_url);
		eventTrackingParam = new HashMap<String, String>();

		moreData=drawerClass.isShowMore();

		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
		{
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.shoplocalWelcomeGallery");
		}
		else
		{
			cacheDir=context.getCacheDir();
		}

		if(!cacheDir.exists())
		{
			cacheDir.mkdirs();
		}else if(network.isNetworkAvailable())
		{

			/*DeleteRecursive(cacheDir);*/
		}

		config= new ImageLoaderConfiguration.Builder(context)

		.denyCacheImageMultipleSizesInMemory()
		.threadPoolSize(2)

		.discCache(new UnlimitedDiscCache(cacheDir))
		.enableLogging()
		.build();
		imageLoader.init(config);
		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()

		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();

		mLocation.setReceiver(this);

		//Session
		session=new SessionManager(context);

		// Defining url and path
		URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.user_api)+getResources().getString(R.string.register);


		dbutil=new DBUtil(context);
		format=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		locationDialog=new Dialog(context);
		locationDialog.setContentView(R.layout.layout_location_chooser_dialog);
		locationDialog.setCancelable(true);
		locationDialog.setTitle("Choose Location");
		locationDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

		locationList=(ListView)locationDialog.findViewById(R.id.locationList);

		actionBar=getSupportActionBar();

		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayUseLogoEnabled(true);
		actionBar.setTitle(getResources().getString(R.string.itemChangeLocation));

		mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);

		distanceBar=(SeekBar)findViewById(R.id.distanceBar);
		textChooseLocation=(TextView)findViewById(R.id.textChooseLocation);
		locationProgress=(ProgressBar)findViewById(R.id.locationProgress);
		textDistanceValue=(TextView)findViewById(R.id.textDistanceValue);

		startKm=(TextView)findViewById(R.id.startKm);
		endKm=(TextView)findViewById(R.id.endKm);
		defineYourRadious=(TextView)findViewById(R.id.defineYourRadious);

		textChooseLocation.setTextColor(Color.WHITE);
		textDistanceValue.setTextColor(Color.WHITE);

		textChooseLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.location_arrow,0);
		textChooseLocation.setCompoundDrawablePadding(15);

		startKm.setTypeface(tf);
		endKm.setTypeface(tf);
		defineYourRadious.setTypeface(tf);
		textChooseLocation.setTypeface(tf);
		textDistanceValue.setTypeface(tf);


		HashMap<String,String>user_details=session.getUserDetailsCustomer();
		USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
		AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();


		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");


		final PackageManager pm = getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);


		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) 
				pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);


		/** Anirudh facebook */
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();

				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
				}

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent)
			{
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}

		/** Anirudh- Custom dialog */
		dialogShare = new Dialog(LocationActivity.this);
		dialogShare.setContentView(R.layout.sharedialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(getApplicationContext(), 0, getLayoutInflater(), appName, packageNames, appIcon));


		locationMap=(ImageView)findViewById(R.id.locationMap);
		gotoWelcome=(Button)findViewById(R.id.gotoWelcome);

		// Getting LocationManager object from System Service SERVICE
		//		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		dialog = new Dialog(context);

		dialog.setContentView(R.layout.accuracy_dialog);
		textAcc = (TextView) dialog.findViewById(R.id.text_accuracy);
		dialog.setTitle("Fetching your position..");
		dialog.setCancelable(false);

		/*
		 * API KEY
		 */
		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//area service 
		getAreaRec = new GetAreaResultReceiver(new Handler());
		getAreaRec.setReciver(this);

		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		// Areas
		AREA_URL = getResources().getString(R.string.areas);
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		countryCode = tm.getSimCountryIso();

		//		ISO_URL = "?iso-code=" + countryCode;
		ISO_URL = "?iso-code=" + "in";
		Log.d("DeviceId","DeviceId " + countryCode);




		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		drawerList=(ExpandableListView)findViewById(R.id.drawerList);

		footerView = View.inflate(this, R.layout.drawer_item_footer, null);
		list_item_footerMore=(TextView)footerView.findViewById(R.id.list_item_footerMore);

		drawerList.addFooterView(footerView);

		if(moreData)
		{
			list_item_footerMore.setText("LESS");
		}
		else
		{
			list_item_footerMore.setText("MORE");
		}

		list_item_footerMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(list_item_footerMore.getText().toString().equalsIgnoreCase("More"))
				{
					moreData=true;
					list_item_footerMore.setText("LESS");
				}
				else
				{
					moreData=false;
					list_item_footerMore.setText("MORE");
				}
				createDrawer();

			}
		});

		mDrawerToggle=new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				R.string.drawer_open,  /* "open drawer" description for accessibility */
				R.string.drawer_close  /* "close drawer" description for accessibility */
				) {
			public void onDrawerClosed(View view) {
				actionBar.setTitle(getResources().getString(R.string.itemChangeLocation));
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				actionBar.setTitle(getResources().getString(R.string.actionBarTitle));
				//                getActionBar().setTitle(mDrawerTitle);
				//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);





		//Creating Drawer
		createDrawer();
		try
		{
			try
			{

				//get active Area Id
				if(dbutil.getAreaRowCount()!=0)
				{
					activeAreaId=dbutil.getActiveAreaID();
				}


				//Getting Date time stamp of last saved area
				SharedPreferences prefs=getSharedPreferences(AREA_DATE_TIME_STAMP, MODE_PRIVATE);

				date_time=prefs.getString(KEY, format.format(new Date()).toString());

				Date prevDate=format.parse(date_time);
				Date currentDate=format.parse(format.format(new Date()));

				//in milliseconds
				long diff = currentDate.getTime() - prevDate.getTime();

				//			long diffSeconds = diff / 1000 % 60;
				//			long diffMinutes = diff / (60 * 1000) % 60;
				//			long diffHours = diff / (60 * 60 * 1000) % 24;

				diffDays = diff / (24 * 60 * 60 * 1000);

				Log.i("DATE TIME DIFF", "DATE TIME DIFF STAMP"+date_time);

				Log.i("DATE TIME DIFF", "DATE TIME DIFF "+diffDays);



			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			//Date time Stamp is older than a day refresh Location
			if(diffDays>=1)
			{
				if(network.isNetworkAvailable())
				{
					mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
					actionBar.setHomeButtonEnabled(false);
					getAreas();	
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
					drawerModeToggle();
					//load Locally.
					setSelectedLocation();
					loadLocalAreas();
				}
			}
			else
			{

				//Toggle The Drawer
				drawerModeToggle();

				//If no records in table load from Internet
				if(dbutil.getAreaRowCount()==0)
				{
					if(network.isNetworkAvailable())
					{
						getAreas();	
					}
					else
					{
						showToast(getResources().getString(R.string.noInternetConnection));
					}
				}
				else
				{
					//load Locally.
					setSelectedLocation();
					clearArray();
					loadLocalAreas();
				}


			}

			for(int i=0;i<allAreaIds.size();i++)
			{
				Log.i("AREAS ", "AREA "+allAreaIds.get(i)+" "+allAreas.get(i));
			}
			textChooseLocation.setOnClickListener(this);
			locationList.setOnItemClickListener(this);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		distanceBar.incrementProgressBy(1);
		textDistanceValue.setText(distanceBar.getProgress() + " km." );
		distanceBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progresValue, boolean arg2) {
				// TODO Auto-generated method stub
				//				progresValue=progresValue/10;
				//				progresValue=progresValue*10;
				textDistanceValue.setText(progresValue + " km." );

			}
		});

		try
		{
			gotoWelcome.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					if(dbutil.getActiveAreaID().length()!=0)
					{
						mDrawerLayout.openDrawer(Gravity.LEFT);
					}
					else
					{
						showToast("Please Select Location First");
					}
					/*Intent intent=new Intent(context,SplashScreen.class);
					startActivity(intent);
					finish();
					overridePendingTransition(0,0);*/
				}
			});


			locationService();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		EventTracker.startLocalyticsSession(getApplicationContext());



	} //OnCreate Ends Here




	void DeleteRecursive(File file) {
		/*if (fileOrDirectory.isDirectory())
				for (File child : fileOrDirectory.listFiles())
				{

					Log.i("Delete:", "File "+child.getAbsolutePath());
					DeleteRecursive(child);
				}

			fileOrDirectory.delete();*/

		if(file.isDirectory()){

			//directory is empty, then delete it
			if(file.list().length==0){

				file.delete();
				System.out.println("Directory is deleted : " 
						+ file.getAbsolutePath());

			}else{

				//list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					//construct the file structure
					File fileDelete = new File(file, temp);

					//recursive delete
					DeleteRecursive(fileDelete);
				}

				//check the directory again, if empty then delete it
				if(file.list().length==0){
					file.delete();
					System.out.println("Directory is deleted : " 
							+ file.getAbsolutePath());
				}
			}

		}else{
			//if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}

	}



	void setImage(String latitude,String longitude)
	{
		String url="http://maps.googleapis.com/maps/api/staticmap?center="+latitude+","+longitude+"&zoom=16&size="+300+"x"+150+"&maptype=roadmap&markers=color:blue|label:S|"+latitude+","+longitude+"&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g";

		imageLoader.displayImage(url, locationMap);
	}




	//set Location
	void setSelectedLocation()
	{
		try
		{
			Log.i("Active Area", "Active "+dbutil.getActiveArea()+" Length "+dbutil.getActiveArea().length());
			if(dbutil.getActiveArea().length()!=0)
			{
				textChooseLocation.setText(dbutil.getActiveArea());
				//setImage(dbutil.getAreaLat(), dbutil.getAreaLong());

			}
			else
			{
				textChooseLocation.setText("Choose your location");
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}
	}

	//load Areas from DB
	void loadLocalAreas()
	{
		try
		{

			allAreas=dbutil.getAllPublishedAreas(place_Status);
			allPinCodes=dbutil.getAllPublishedAreaPin(place_Status);
			allCities=dbutil.getAllPublishedAreaCity(place_Status);
			allAreaIds=dbutil.getAllPublishedAreaIds(place_Status);
			allLat=dbutil.getAllPublishedAreaLat(place_Status);
			allLon=dbutil.getAllPublishedAreaLong(place_Status);
			allCountry=dbutil.getAllPublishedAreaCountries(place_Status);
			allCountryCode=dbutil.getAllPublishedAreaCountryCodes(place_Status);
			allIsoCode=dbutil.getAllPublishedAreaIsoCode(place_Status);
			allPlaceId=dbutil.getAllPublishedAreaPlaceId(place_Status);

			locationList.setAdapter(new LocationListAdapter(context, 0, 0, allAreas));

			Log.i("All areas ","All Areas "+dbutil.getAllAreas().size());
		}catch(Exception ex)
		{
			ex.printStackTrace();

		}

	}

	void clearArray()
	{
		allAreas.clear();
		allPinCodes.clear();
		allCities.clear();
		allAreaIds.clear();
		allLat.clear();
		allLon.clear();
		allCountry.clear();
		allCountryCode.clear();
		allIsoCode.clear();
		allPlaceId.clear();
	}

	class LocationListAdapter extends ArrayAdapter<String>
	{

		ArrayList<String> locations;
		Activity context;
		LayoutInflater inflator;

		public LocationListAdapter(Activity context, int resource,
				int textViewResourceId, ArrayList<String> locations) {
			super(context, resource, textViewResourceId, locations);
			// TODO Auto-generated constructor stub
			this.locations=locations;
			this.context=context;
			inflator=context.getLayoutInflater();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return locations.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflator.inflate(R.layout.layout_location_row, null);
				holder.txtLocationRow=(TextView)convertView.findViewById(R.id.txtLocationRow);
				convertView.setTag(holder);
			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			hold.txtLocationRow.setText(locations.get(position));
			return convertView;
		}






	}

	static class ViewHolder
	{
		TextView txtLocationRow;
	}


	void createDrawer()
	{
		//Creating Drawer Menu
		//items.add(new DrawerSearch());
		try
		{
			drawerMenu.clear();

			drawerClass.setShowMore(moreData);
			drawerMenu= drawerClass.getDrawerAdapter();

			drawerAdapter=new DrawerExpandibleAdapter(context, 0, drawerMenu);
			drawerList.setAdapter(drawerAdapter);

			drawerList.setOnChildClickListener(new ExpandedDrawerClickListener());

			drawerList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {
					// TODO Auto-generated method stub
					if(drawerMenu.get(groupPosition).getOpened_state()==1)
					{
						return true;
					}
					return false;
				}
			});

			for(int i=0;i<drawerMenu.size();i++)
			{
				if(drawerMenu.get(i).getOpened_state()==1)
				{
					drawerList.expandGroup(i);
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}


	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener
	{

		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			// TODO Auto-generated method stub
			drawer_item=drawerMenu.get(groupPosition).getItem_array().get(childPosition);
			//EventTracker.logEvent("Tab_"+drawer_item.replaceAll(" ", ""), true);
			navigate();
			return false;
		}

	}





	void navigate()
	{

		//Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemDiscover)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemDiscover));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,NewsFeedActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyShoplocalOffers)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyShoplocalOffers));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			Log.i("Event name",event);
			drawer_item="";
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MyShoplocal.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//Personalize

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogin)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemLogin));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			Intent intent=new Intent(context,LoginSignUpCustomer.class);
			startActivityForResult(intent, 2);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemMyProfile)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemMyProfile));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedInCustomer())
			{
				Intent intent=new Intent(context,CustomerProfile.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
			else
			{
				login();
			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemChangeLocation)))
		{
			//NOT ADDED ANALYICS HERE
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			mDrawerLayout.closeDrawer(Gravity.LEFT);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemSettings)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemSettings));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}

			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,SettingsActivity.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Business

		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAllStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAllStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,DefaultSearchList.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}

		if(drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFavouriteStores)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemFavouriteStores));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			//			if(session.isLoggedInCustomer())
			//			{
			Intent intent=new Intent(context,MerchantFavouriteStores.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			//			}
			//			else
			//			{
			//				login();
			//			}
		}

		//About Shoplocal
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemAboutShoplocal)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemAboutShoplocal));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			Intent intent=new Intent(context,AboutUs.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemShare)))
		{

			showDialog();


		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemContactUs)))
		{
			String event = EventTracker.getEventFromDrawer(getResources().getString(R.string.itemContactUs));
			if(!event.equals("")){
				EventTracker.logEvent(event, true);
			}
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle("Shoplocal");
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.contactusDrawerMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(true)
			.setPositiveButton("Send Feedback",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					//mDrawerLayout.closeDrawers();
					Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
							"mailto",getResources().getString(R.string.contact_email), null));
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, "no-subject");
					startActivity(Intent.createChooser(emailIntent, "Send email..."));
					dialog.dismiss();

				}
			});

			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemFacebook)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", true);
			intent.putExtra("fb", "facebook");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemTwitter)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			Intent intent=new Intent(context,FbTwitter.class);
			intent.putExtra("isMerchant", false);
			intent.putExtra("isFb", false);
			intent.putExtra("twitter", "twitter");
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		//Sell
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemLogintoBusiness)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{
				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}

			else
			{
				Intent intent=new Intent(context,SplitLoginSignUp.class);
				startActivityForResult(intent, 4);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}
		if( drawer_item.equalsIgnoreCase(getResources().getString(R.string.itemBusinessDashboard)))
		{
			drawer_item="";
			//mDrawerLayout.closeDrawers();
			if(session.isLoggedIn())
			{

				try
				{
					DBUtil dbutil =new DBUtil(context);
					long rowcount=dbutil.getMyPlaceCount();
					if(rowcount==0)
					{
						Log.i("DB ","DB Row count "+rowcount);
						SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
						Editor editor=prefs.edit();
						editor.putBoolean("isPlaceRefreshRequired", true);
						editor.commit();	
					}



				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				Intent intent=new Intent(context,MerchantTalkHome.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}


	}

	void showDialog(){

		dialogShare.show();

		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {

				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(LocationActivity.this)
					.setLink("http://shoplocal.co.in")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(getApplicationContext(), "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					startActivity(i);
				}

				dismissDialog();

			}
		});
	}



	void dismissDialog(){
		dialogShare.dismiss();
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}


	void login()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.loginDrawerMessage))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Login",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				drawer_item="";
				Intent intent=new Intent(context,LoginSignUpCustomer.class);
				startActivityForResult(intent, 2);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		})
		.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();
			}
		})
		;

		AlertDialog alertDialog3 = alertDialogBuilder3.create();

		alertDialog3.show();
	}

	void showLocalAlert()
	{
		AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
		alertDialogBuilder3.setTitle("Shoplocal");
		alertDialogBuilder3
		.setMessage(getResources().getString(R.string.localAlert))
		.setIcon(R.drawable.ic_launcher)
		.setCancelable(false)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				dialog.dismiss();

			}
		});
		AlertDialog alertDialog3 = alertDialogBuilder3.create();
		alertDialog3.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==2)
		{
			if(session.isLoggedInCustomer())
			{
				HashMap<String,String>user_details=session.getUserDetailsCustomer();
				USER_ID=user_details.get(KEY_USER_ID_CUSTOMER).toString();
				AUTH_ID=user_details.get(KEY_AUTH_ID_CUSTOMER).toString();

			}
			createDrawer();
		}
		if(requestCode==4)
		{
			if(session.isLoggedIn())
			{

				SharedPreferences	prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
				Editor 	editor=prefs.edit();
				editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.merchant));
				editor.commit();

				boolean isAddStore=data.getBooleanExtra("isAddStore", false);
				if(isAddStore)
				{
					Intent intent=new Intent(context, EditStore.class);
					intent.putExtra("isNew", true);
					startActivity(intent);
					context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					context.finish();
				}
				else
				{
					Intent intent=new Intent(context,MerchantTalkHome.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case android.R.id.home:
			mDrawerLayout.openDrawer(Gravity.LEFT);
			if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
			{
				mDrawerLayout.closeDrawers();
			}
		}


		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), getResources().getString(R.string.backpressMessage), Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}


	//Calling Area Service
	public void getAreas(){
		if(network.isNetworkAvailable())
		{
			Intent intent = new Intent(context, GetAreaService.class);
			intent.putExtra("getArea", getAreaRec);
			intent.putExtra("URL", STORE_URL+AREA_URL+ISO_URL);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			context.startService(intent);
			locationProgress.setVisibility(View.VISIBLE);
		}else
		{
			locationProgress.setVisibility(View.INVISIBLE);
		}
	}

	//Areas
	@Override
	public void onGetAreaReceive(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		locationProgress.setVisibility(View.INVISIBLE);

		try {
			String status = resultData.getString("area_status");
			if(status.equalsIgnoreCase("false") || status.equalsIgnoreCase("error"))
			{
				String message=resultData.getString("message");
				showToast(message);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	void drawerModeToggle()
	{
		try
		{
			Log.i("Area", "Current And Selected Area Id "+activeAreaId+" "+selectedAreaId);

			//if currently active id and user selected Id did not match then call Active Area Api
			Log.i("Area", "Current And Selected Area Id "+activeAreaId+" "+selectedAreaId);
			if(selectedAreaId.length()!=0)
			{
				if(session.isLoggedInCustomer())
				{
					new ActiveArea().execute(URL);
				}
			}

			String active_place;
			active_place=dbutil.getActiveAreaID();
			//		Log.i("Active Area Count", "Active Area Count in Drawer");
			Log.i("Active Area Count", "Active Area Count "+active_place);
			if(active_place.length()==0 && !countryCode.equalsIgnoreCase("in"))
			{
				showLocalAlert();
			}
			if(active_place.length()==0)
			{
				mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
				actionBar.setHomeButtonEnabled(false);
				gotoWelcome.setVisibility(View.GONE);
				locationMap.setVisibility(View.GONE);
			}
			else
			{

				actionBar.setIcon(R.drawable.ic_drawer);
				mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
				actionBar.setHomeButtonEnabled(true);
				gotoWelcome.setVisibility(View.VISIBLE);
				/*			locationMap.setVisibility(View.VISIBLE);*/
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Location Service
	void locationService()
	{
		try
		{
			if(network.isNetworkAvailable())
			{
				//calling location service.
				Intent intent=new Intent(context, LocationIntentService.class);
				intent.putExtra("receiverTag", mLocation);
				context.startService(intent);
				/*ShopLocalMain.shopProgress.setVisibility(View.VISIBLE);*/
				/*locationProgress.setVisibility(View.VISIBLE);*/
			}else
			{
				showToast(getResources().getString(R.string.noInternetConnection));
				/*	locationProgress.setVisibility(View.INVISIBLE);*/
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



	void setPref()
	{

		SharedPreferences prefs=getSharedPreferences("location", 0);
		Editor editor=prefs.edit();
		editor.putString("latitued", saved_lati+"");
		editor.putString("longitude", saved_longi+"");
		editor.putBoolean("isLocationFound",true);
		editor.commit();
		Log.i("LOCATION  ", "LOCATION COMMITED IN HOME GRID"+saved_lati+","+saved_longi);
	}

	void setPref(String lat,String longi)
	{

		SharedPreferences prefs=getSharedPreferences("location", 0);
		Editor editor=prefs.edit();
		editor.putString("latitued", lat);
		editor.putString("longitude", longi);
		editor.putBoolean("isLocationFound",true);
		editor.commit();
		Log.i("LOCATION  ", "LOCATION COMMITED IN HOME GRID"+lat+","+longi);
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		/*		locationProgress.setVisibility(View.INVISIBLE);*/

		String status=resultData.getString("status");
		String latitued;
		String longitude;
		if(status.equalsIgnoreCase("set"))
		{
			try
			{
				latitued=resultData.getString("Lat");
				longitude=resultData.getString("Longi");

				Log.i("LAT", "LATI "+latitued);
				Log.i("LONG", "LATI "+longitude);

				setPref(latitued, longitude);


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			showToast(getResources().getString(R.string.locationAlert));
			latitued="0";
			longitude="0";
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		try
		{
			//Setting Area Name from Selected Areas
			textChooseLocation.setText(allAreas.get(position));

			//Updating Status of Selected Area to 1
			update=dbutil.updatePlaceStatus(Integer.parseInt(allAreaIds.get(position)),1);

			//Putting Selected Area id in Preference so that it can be access across the app.
			SharedPreferences prefs=getSharedPreferences(ACTIVE_AREA_PREF,MODE_PRIVATE);
			Editor editor=prefs.edit();
			editor.putString(ACTIVE_AREA_KEY, dbutil.getActiveAreaID());
			editor.commit();

			//setImage(allLat.get(position), allLon.get(position));

			//Putting Active Area Name and Id in Analytics
			String areaName = allAreas.get(position);
			String areaId = allAreaIds.get(position);

			Log.i("AREA NAME", "AREA NAME "+areaName+" AREA ID "+areaId);
			eventTrackingParam.put("Area_Name",areaName);
			eventTrackingParam.put("Area_ID",areaId);
			EventTracker.logEvent("Location_Select", eventTrackingParam);

			//Saved selected Area id.
			selectedAreaId=allAreaIds.get(position);
			Log.i("Selected Area Id ", "Selected Area Id "+selectedAreaId);

			if(update==1)
			{
				//Changing visibility of Drawer 
				setAreaTag(selectedAreaId);
				createDrawer();
				drawerModeToggle();
			}
			locationDialog.dismiss();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void setAreaTag(String area)
	{
		Log.i("URBAN", "URBAN SET TAG");
		boolean isMisMatch=false;

		//Adding Tags

		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();

		ArrayList<String>tempIdList=new ArrayList<String>();

		String temp="";
		String replace="";
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();

			//Traversing Tags to find out if there are any tags existing with own_id

			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains own_
				if(temp.startsWith("area_"))
				{
					//Replace area_ with "" and compare it with ID
					replace=temp.replaceAll("area_", "");
					tempIdList.add(replace);
					Log.i("TAG", "Urban TAG ID "+replace);

					if(!area.equalsIgnoreCase(replace))
					{
						isMisMatch=true;
						break;
					}
				}
			}

			if(isMisMatch==false && tempIdList.size()==0)
			{
				isMisMatch=true;
			}


			if(isMisMatch==true)
			{

				//Clear area_ from Tag List
				for(int i=0;i<tempIdList.size();i++)
				{
					tags.remove("area_"+tempIdList.get(i));
				}


				tags.add("area_"+area);

				Log.i("TAG", "Urban TAG AFTER "+tags.toString());

				PushManager.shared().setTags(tags);
			}
	}




	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try
		{
			EventTracker.startLocalyticsSession(getApplicationContext());

			//Registering For broadcast
			areaBroadCast=new AreaBroadCast();
			IntentFilter filter = new IntentFilter(RequestTags.TagAreas);
			context.registerReceiver(areaBroadCast, filter);

		}catch(Exception ex)
		{
			ex.printStackTrace();

		}

		uiHelper.onResume();
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		uiHelper.onPause();
		super.onPause();
		try
		{
			if(areaBroadCast!=null)
			{
				//UnRegistering broadcast 
				context.unregisterReceiver(areaBroadCast);
				areaBroadCast=null;
			}
			/*			handler.removeCallbacks(runnable);*/
			/*		locationManager.removeUpdates(this);*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	//Broadcast reciver
	public class AreaBroadCast extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.i("Broadcast Called", "Broadcast called in Location");

			//Update Area List in L
			updateList();

		}

	}
	void updateList()
	{
		clearArray();
		loadLocalAreas();
		locationList.setAdapter(new LocationListAdapter(context, 0, 0, allAreas));

		textChooseLocation.setOnClickListener(this);
		locationList.setOnItemClickListener(this);

	}
	@Override
	protected void onStop() {

		EventTracker.endFlurrySession(getApplicationContext());
		super.onStop();


		try
		{
			if(areaBroadCast!=null)
			{
				context.unregisterReceiver(areaBroadCast);
				areaBroadCast=null;
			}
			/*			handler.removeCallbacks(runnable);*/
			//			locationManager.removeUpdates(this);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(allAreaIds.size()!=0)
		{
			locationDialog.show();
		}
	}


	//Active Area Async Task

	private class ActiveArea  extends AsyncTask<String, Void, String>
	{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String status = "";

			try
			{

				BufferedReader bufferedReader = null;


				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
				HttpPost httpPost=new HttpPost(params[0]);

				Log.i("", "Active Area URL "+params[0]);

				//Adding header.
				/*httpPost.addHeader("X-API-KEY", "Fool");*/
				httpPost.addHeader("X-HTTP-Method-Override", "PUT");
				httpPost.addHeader(API_HEADER, API_VALUE);

				JSONObject json = new JSONObject();

				json.put("user_id", USER_ID);
				json.put("auth_id", AUTH_ID);
				json.put("active_area", selectedAreaId);
				try
				{
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}


				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);


				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", "Active Area Response  "+stringBuffer.toString());
				status=stringBuffer.toString();


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return  status;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try
			{
				//				progUpload.setVisibility(View.GONE);

				String area_status;
				area_status=getStatus(result);
				if(area_status.equalsIgnoreCase("true"))
				{
					Log.i("Active Area", "Active Area Updated "+area_status);
				}
				else
				{

				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}



	}//Async Ends here




}




