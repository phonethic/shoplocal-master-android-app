/*
Copyright 2009-2011 Urban Airship Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE URBAN AIRSHIP INC ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL URBAN AIRSHIP INC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.phonethics.shoplocal;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;
import com.urbanairship.push.CustomPushNotificationBuilder;
import com.urbanairship.push.PushManager;

public class MyApplication extends Application {

	//public static String GOOGLE_MAPS_API_KEY = "ENTER YOUR GOOGLE MAPS API KEY HERE";
	String urbanAirshiptAppKey;
	String urbanAirshipAppSecret;
	String urbanAirshipGCMProjectId;



	SharedPreferences sharedVersionCode;


	@Override
	public void onCreate() {

		super.onCreate();

		AirshipConfigOptions options = AirshipConfigOptions.loadDefaultOptions(this);

		/*     urbanAirshiptAppKey = getResources().getString(R.string.urbanAirshipdevelopmentAppKey);
        urbanAirshipAppSecret = getResources().getString(R.string.urbanAirshipdevelopmentAppSecret);
		 */
		urbanAirshipGCMProjectId = getResources().getString(R.string.urbanAirshipGCMProjectId);
		options.inProduction = false;


		
		if(options.inProduction==false)
		{
			options.developmentAppKey 		= getResources().getString(R.string.urbanAirshipdevelopmentAppKey);
			options.developmentAppSecret 	= getResources().getString(R.string.urbanAirshipdevelopmentAppSecret);
		}
		else
		{
			options.productionAppKey		= getResources().getString(R.string.urbanAirshipProductionAppKey);
			options.productionAppSecret 	= getResources().getString(R.string.urbanAirshipProductionAppSecret);

		}

		options.gcmSender = urbanAirshipGCMProjectId;
		options.transport = "gcm";


		UAirship.takeOff(this, options);

		//        Logger.logLevel = Log.VERBOSE;

		//use CustomPushNotificationBuilder to specify a custom layout
		CustomPushNotificationBuilder nb = new CustomPushNotificationBuilder();

		nb.statusBarIconDrawableId = R.drawable.ic_launcher;//custom status bar icon

		nb.layout = R.layout.notification;
		nb.layoutIconDrawableId = R.drawable.ic_launcher;//custom layout icon
		nb.layoutIconId = R.id.icon;
		nb.layoutSubjectId = R.id.subject;
		nb.layoutMessageId = R.id.message;

		PushManager.enablePush();
		PushManager.shared().setNotificationBuilder(nb);
		PushManager.shared().setIntentReceiver(IntentReceiver.class);
	}
}