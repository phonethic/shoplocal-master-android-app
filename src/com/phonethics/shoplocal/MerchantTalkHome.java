package com.phonethics.shoplocal;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.phonethics.localnotification.LocalNotification;
import com.phonethics.model.DatePreferencesClass;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver;
import com.phonethics.networkcall.GetAllStores_Of_Perticular_User_Receiver.GetAllStore;
import com.phonethics.networkcall.GetAreaResultReceiver;
import com.phonethics.networkcall.GetAreaResultReceiver.GetArea;
import com.phonethics.networkcall.GetAreaService;
import com.urbanairship.push.PushManager;

public class MerchantTalkHome extends SherlockFragmentActivity implements GetAllStore, GetArea, OnClickListener {

	private static long back_pressed;

	ActionBar actionBar;

	Activity context;

	ArrayList<String>pages;


	//Animating Button
	DecelerateInterpolator sDecelerator = new DecelerateInterpolator();
	OvershootInterpolator sOvershooter = new OvershootInterpolator(10f);

	SharedPreferences prefs;
	Editor editor;

	DisplayMetrics metrics=new DisplayMetrics();

	ArrayList<String> STOREID=new ArrayList<String>();
	ArrayList<String> STORE_NAME=new ArrayList<String>();
	ArrayList<String>PHOTO=new ArrayList<String>();
	ArrayList<String> STORE_DESC=new ArrayList<String>();

	DBUtil dbUtil;

	boolean isPlaceRefreshRequired=false;

	//All Stores
	GetAllStores_Of_Perticular_User_Receiver mAllStores;



	static String USER_ID="";
	static String AUTH_ID="";

	//User Id
	public static final String KEY_USER_ID="user_id";

	//Auth Id
	public static final String KEY_AUTH_ID="auth_id";

	static String STORE_URL;
	static String SOTRES_PATH;

	static String API_HEADER;
	static String API_VALUE;

	NetworkCheck isnetConnected;


	SessionManager session;



	GetAreaResultReceiver getAreaRec; 
	static String AREA_URL;
	static String ISO_URL;
	String countryCode;

	//Fetch Dialog
	AlertDialog.Builder fetchDialogBuilder;
	AlertDialog fetchDialog;



	//Retry Dialog
	AlertDialog.Builder retryDialogBuilder;
	AlertDialog retryDialog;

	LocalNotification localNotification;

	ValidateCredentials validateCredentials;

	String VALIDATE_CREED_PREF="VALIDATE_CREED_PREF";
	String VALIDATE_CREED_DATE="VALIDATE_CREED_DATE";

	DatePreferencesClass datePref;
	ListView merchantPagerList;


	ImageView reportIcon;
	TextView txtReportBig;

	ImageView postIcon;
	TextView txtTalkNowBig;

	ImageView viewIcon;
	TextView txtViewPostBig;


	TextView txtMoreBig;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_City_custom);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merchant_talk_home);

		context=this;
		localNotification=new LocalNotification(getApplicationContext());
		pages=new ArrayList<String>();
		dbUtil=new DBUtil(context);
		session=new SessionManager(context);
		isnetConnected=new NetworkCheck(context);


		validateCredentials=new ValidateCredentials();

		//Initializing Date Pref Class
		datePref=new DatePreferencesClass(context, VALIDATE_CREED_PREF, VALIDATE_CREED_DATE);
		merchantPagerList=(ListView)findViewById(R.id.merchantPagerList);


		//

		reportIcon=(ImageView)findViewById(R.id.reportIcon);
		txtReportBig=(TextView)findViewById(R.id.txtReportBig);

		postIcon=(ImageView)findViewById(R.id.postIcon);
		txtTalkNowBig=(TextView)findViewById(R.id.txtTalkNowBig);

		viewIcon=(ImageView)findViewById(R.id.viewIcon);
		txtViewPostBig=(TextView)findViewById(R.id.txtViewPostBig);

		txtMoreBig=(TextView)findViewById(R.id.txtMoreBig);

		//Getting Display Densities
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		actionBar=getSupportActionBar();
		actionBar.setTitle("Business Dashboard");

		prefs=getSharedPreferences(getResources().getString(R.string.userprefs), 0);
		editor=prefs.edit();

		//Store API
		STORE_URL=getResources().getString(R.string.server_url)+getResources().getString(R.string.storeapi);
		SOTRES_PATH=getResources().getString(R.string.allStores);

		API_HEADER=getResources().getString(R.string.api_header);
		API_VALUE=getResources().getString(R.string.api_value);

		//All Store Service
		mAllStores=new GetAllStores_Of_Perticular_User_Receiver(new Handler());
		mAllStores.setReceiver(this);

		getAreaRec = new GetAreaResultReceiver(new Handler());
		getAreaRec.setReciver(this);

		// Areas
		AREA_URL = getResources().getString(R.string.areas);
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		countryCode = tm.getSimCountryIso();

		//		ISO_URL = "?iso-code=" + countryCode;
		ISO_URL = "?iso-code=" + "in";


		//Ui Dialogs

		fetchDialogBuilder = new AlertDialog.Builder(context);
		fetchDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));


		//Retry Dialog
		retryDialogBuilder = new AlertDialog.Builder(context);
		retryDialogBuilder.setTitle(getResources().getString(R.string.actionBarTitle));

		/*fetchDialog.show();*/

		try
		{


			datePref.checkDateDiff();

			//Date time Stamp is older than a day refresh Data
			/*	if(datePref.getDiffDays()>=1)
			{*/


			//Checking whether Validate api is getting called for first time only. If not then check if it is called and 
			//date time differnce is more than a day.
			if(datePref.isPrefDatePresent()==false || (datePref.isPrefDatePresent()==true && Math.abs(datePref.getDiffDays())>=1))
			{
				//Execute Validate Credentials 
				validateCredentials.execute(getResources().getString(R.string.server_url)+getResources().getString(R.string.merchant_api)+getResources().getString(R.string.validate_login));
			}
			/*}*/

			HashMap<String,String>user_details=session.getUserDetails();
			USER_ID=user_details.get(KEY_USER_ID).toString();
			AUTH_ID=user_details.get(KEY_AUTH_ID).toString();


			SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
			isPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);

			Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+isPlaceRefreshRequired);

			if(!session.isLoggedIn())
			{
				try
				{
					String active_place;
					active_place=dbUtil.getActiveAreaID();
					//		Log.i("Active Area Count", "Active Area Count in Drawer");
					Log.i("Active Area Count", "Active Area Count "+active_place);
					if(active_place.length()==0)
					{

						Intent intent=new Intent(context,LocationActivity.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
					else
					{
						Intent intent=new Intent(context,NewsFeedActivity.class);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

			}

			fetchDialogBuilder
			.setMessage(getResources().getString(R.string.fetchDataMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false);

			fetchDialog = fetchDialogBuilder.create();







			retryDialogBuilder
			.setMessage(getResources().getString(R.string.retryDataMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Retry",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					/*	dialog.dismiss();*/
					try
					{
						loadAllStoreForUser();
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			})
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					/*	dialog.dismiss();*/
					try
					{
						dialog.dismiss();
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});

			retryDialog = retryDialogBuilder.create();

			if(!isPlaceRefreshRequired)
			{
				STOREID=dbUtil.getAllPlacesIDs();
				STORE_NAME=dbUtil.getAllPlacesNames();
				PHOTO=dbUtil.getAllPlacesPhotoUrl();
				STORE_DESC=dbUtil.getAllPlacesDesc();
			}
			else
			{
				loadAllStoreForUser();
			}

			if(dbUtil.getAreaRowCount()==0)
			{
				if(isnetConnected.isNetworkAvailable())
				{
					getAreas();	
				}
				else
				{
					showToast(getResources().getString(R.string.noInternetConnection));
				}
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		pages.add("Report");
		pages.add("Talk");
		pages.add("View Post");
		pages.add("More");

		try
		{
			if(hasOwnTag()==false)
			{
				setTag(STOREID);
			}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		reportIcon.setOnClickListener(this);
		txtReportBig.setOnClickListener(this);

		postIcon.setOnClickListener(this);
		txtTalkNowBig.setOnClickListener(this);

		viewIcon.setOnClickListener(this);
		txtViewPostBig.setOnClickListener(this);

		txtMoreBig.setOnClickListener(this);


	}//onCreate Ends Here

	private class TalkNowList extends ArrayAdapter<String>
	{
		ArrayList<String> list;
		LayoutInflater inflator;
		Activity context;
		Typeface tf;
		public TalkNowList(Activity context, int resource,
				int textViewResourceId, ArrayList<String> list) {
			super(context, resource, textViewResourceId, list);
			// TODO Auto-generated constructor stub
			this.context=context;
			inflator=context.getLayoutInflater();
			this.list=list;
			tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				convertView=(View)inflator.inflate(R.layout.talknowscreen, null);
				ViewHolder holder=new ViewHolder();
				holder.talkNowText=(TextView)convertView.findViewById(R.id.txtTalkNow);
				convertView.setTag(holder);
			}
			ViewHolder hold=(ViewHolder)convertView.getTag();
			if(position==0)
			{
				hold.talkNowText.setBackgroundColor(Color.parseColor("#4Dbacc9b"));
			}
			if(position==1)
			{
				hold.talkNowText.setBackgroundColor(Color.parseColor("#4Dcc817a"));
				hold.talkNowText.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.new_post_big,0,0,0 );
			}
			if(position==2)
			{
				hold.talkNowText.setBackgroundColor(Color.parseColor("#4Dbacc9b"));
				hold.talkNowText.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.view_post_big,0,0,0 );
			}
			if(position==3)
			{

				hold.talkNowText.setBackgroundColor(Color.parseColor("#4Dcc817a"));
				hold.talkNowText.setCompoundDrawablesWithIntrinsicBounds(
						0,0,0,0 );

			}
			hold.talkNowText.setTypeface(tf);
			hold.talkNowText.setText(list.get(position));

			return convertView;
		}




	}

	static class ViewHolder{
		TextView talkNowText;
	}


	@Override
	protected void onStart() {
		super.onStart();
		EventTracker.startFlurrySession(getApplicationContext());
	}

	@Override
	protected void onStop() {
		EventTracker.endFlurrySession(getApplicationContext());	
		super.onStop();
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventTracker.startLocalyticsSession(getApplicationContext());
	}

	@Override
	protected void onPause() {
		EventTracker.endLocalyticsSession(getApplicationContext());
		super.onPause();
	}



	public void getAreas(){

		Intent intent = new Intent(context, GetAreaService.class);
		intent.putExtra("getArea", getAreaRec);
		intent.putExtra("URL", STORE_URL+AREA_URL+ISO_URL);
		intent.putExtra("api_header",API_HEADER);
		intent.putExtra("api_header_value", API_VALUE);
		context.startService(intent);

	}

	void loadAllStoreForUser()
	{
		if(isnetConnected.isNetworkAvailable())
		{
			Intent intent=new Intent(context, GetAllStores_Of_Perticular_User.class);
			intent.putExtra("getAllStores",mAllStores);
			intent.putExtra("URL", STORE_URL+SOTRES_PATH);
			intent.putExtra("api_header",API_HEADER);
			intent.putExtra("api_header_value", API_VALUE);
			intent.putExtra("user_id", USER_ID);
			intent.putExtra("auth_id", AUTH_ID);
			context.startService(intent);
			if(!fetchDialog.isShowing())
			{
				fetchDialog.show();
			}

		}else
		{
			showToast(context.getResources().getString(R.string.noInternetConnection));
		}
		/*relBroadCastProgress.setVisibility(ViewGroup.VISIBLE);*/
	}





	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuItem extra=menu.add("Extra Settings").setIcon(R.drawable.switch_view);
		extra.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		try
		{

			editor.putString(getResources().getString(R.string.usertype),getResources().getString(R.string.customer));
			editor.commit();

			String active_place;
			active_place=dbUtil.getActiveAreaID();
			//		Log.i("Active Area Count", "Active Area Count in Drawer");
			Log.i("Active Area Count", "Active Area Count "+active_place);
			if(active_place.length()==0)
			{
				Intent intent=new Intent(context,LocationActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
			else
			{
				Intent intent=new Intent(context,NewsFeedActivity.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return true;

	}



	@Override
	public void onReceiveUsersAllStore(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		try
		{
			String SEARCH_STATUS=resultData.getString("SEARCH_STATUS"); 
			fetchDialog.dismiss();
			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{


				SharedPreferences prefs=context.getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
				isPlaceRefreshRequired=prefs.getBoolean("isPlaceRefreshRequired", false);
				Log.i("isPlaceRefreshRequired", "isPlaceRefreshRequired "+isPlaceRefreshRequired);
				if(!isPlaceRefreshRequired)
				{
					STOREID=dbUtil.getAllPlacesIDs();
					STORE_NAME=dbUtil.getAllPlacesNames();
					PHOTO=dbUtil.getAllPlacesPhotoUrl();
					STORE_DESC=dbUtil.getAllPlacesDesc();
				}
			}
			if(SEARCH_STATUS.equalsIgnoreCase("error"))
			{
				/*showToast("error");*/
				retryDialog.show();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	void showToast(String text)
	{
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(back_pressed+2000 >System.currentTimeMillis())
		{
			localNotification.alarm();
			finish();
		}
		else
		{
			Toast.makeText(getBaseContext(), "Press again to exit!", Toast.LENGTH_SHORT).show();
			back_pressed=System.currentTimeMillis();
		}
	}

	void alarm()
	{
		try
		{
			localNotification.checkPref();

			if(session.isLoggedIn())
			{
				if(localNotification.placeAlarm()==true && localNotification.isPlaceAlarmFired()==false)
				{
					//Remind Merchant for incomplete place details
					localNotification.setLocalNotification(7, 0,Integer.parseInt(getString(R.string.Edit_Store_alert)), false);
				}
				else
				{
					localNotification.cancelAlarm(Integer.parseInt(getString(R.string.Edit_Store_alert)),getApplicationContext());
				}

				if(localNotification.postAlarm()==false && localNotification.isPostAlarmFired()==false)
				{
					//Remind Merchant for posting offer 
					localNotification.setLocalNotification(3, 0,Integer.parseInt(getString(R.string.Talk_Now_alert)), false);
				}
				else
				{
					localNotification.cancelAlarm(Integer.parseInt(getString(R.string.Talk_Now_alert)),getApplicationContext());
				}
			}
			if(session.isLoggedInCustomer())
			{
				if(localNotification.favPostAlarm()==false && localNotification.isFavAlarmFired()==false)
				{
					//Remind Customer that he has not marked any store favourite
					localNotification.setLocalNotification(3, 0,Integer.parseInt(getString(R.string.Favourite_alert)), false);
				}
				else
				{
					localNotification.cancelAlarm(Integer.parseInt(getString(R.string.Favourite_alert)),getApplicationContext());
				}


				if(localNotification.isProfileUpdated()==false && localNotification.isProfilAlarmFired()==false)
				{
					//Customer has not updated his profile
					localNotification.setLocalNotification(7, 0, Integer.parseInt(getString(R.string.Customer_Profile_alert)), false);
				}
				else
				{
					localNotification.cancelAlarm(Integer.parseInt(getString(R.string.Customer_Profile_alert)),getApplicationContext());
				}
			}

			if(localNotification.isAppLaunchedFired()==false)
			{
				//App Not launched
				localNotification.setLocalNotification(4, 0, Integer.parseInt(getString(R.string.App_Launch)), false);
			}


			if(!session.isLoggedInCustomer())
			{
				if(localNotification.isLoginAlarmFired()==false)
				{
					//Customer launched the app but not signed in
					localNotification.setLocalNotification(7, 0, Integer.parseInt(getString(R.string.Login_alert)), false);
				}
			}
			else
			{
				localNotification.cancelAlarm(Integer.parseInt(getString(R.string.Login_alert)),getApplicationContext());
			}

			//Clearing Merchant Prefs if Merchant is Logged out 
			if(!session.isLoggedIn())
			{
				localNotification.clearPlacePrefs();
				localNotification.clearPostPrefs();
			}

			//Clearing Customer Prefs if Customer is Logged out 
			if(!session.isLoggedInCustomer())
			{
				localNotification.clearFavPrefs();
				localNotification.clearProfilePrefs();

			}


			//Remind on Every Friday to Merchant to post offers
			/*localNotification.setWeeklyReminder(Integer.parseInt(getString(R.string.TalkNow_Rem_alert)), 4);*/

			//Remind on Every Friday to customer to check offers
			/*localNotification.setWeeklyReminder(Integer.parseInt(getString(R.string.Offers_Stream_alert)), 6);*/

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public void onGetAreaReceive(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub



		try {

			String status = resultData.getString("area_status");

			if(status.equalsIgnoreCase("true")){

			}else if(status.equalsIgnoreCase("error"))
			{
				String message=resultData.getString("message");
				showToast(message);
			}
			else
			{
				String message=resultData.getString("message");
				showToast(message);
			}


		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}


	//Validate Credential

	private class ValidateCredentials extends AsyncTask<String, Void, String>
	{

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			//			progUpload.setVisibility(View.GONE);
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//			progUpload.setVisibility(View.VISIBLE);
		}



		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String status = "";

			try
			{

				PackageInfo pinfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);

				BufferedReader bufferedReader = null;


				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

				//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient(httpParams);

				//Setting URL to post data.
				/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
				HttpPost httpPost=new HttpPost(params[0]);

				Log.i("", "Service Response URL "+params[0]);

				//Adding header.
				/*httpPost.addHeader("X-API-KEY", "Fool");*/
				httpPost.addHeader("X-HTTP-Method-Override", "PUT");
				httpPost.addHeader(API_HEADER, API_VALUE);

				JSONObject json = new JSONObject();

				json.put("user_id", USER_ID);
				json.put("auth_id", AUTH_ID);
				try
				{
					json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}


				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);


				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
				status=stringBuffer.toString();


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return  status;
		}



		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try
			{
				//				progUpload.setVisibility(View.GONE);

				String store_status;
				String store_msg = "";
				String store_data="";
				store_status=getStatus(result);
				if(store_status.equalsIgnoreCase("true"))
				{
					store_msg=getMessage(result);
					/*		store_data=getData(result);*/



					try
					{

						//Setting Current Date to Pref
						datePref.setDateInPref();

						//Tagging Merchant
						Set<String> tags = new HashSet<String>(); 
						tags=PushManager.shared().getTags();
						tags.add("merchant");
						PushManager.shared().setTags(tags);

						//Tracking Event
						EventTracker.logEvent("Merchant_ValidateLogin", false);


					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

					/*		showToast(store_msg);*/
					//					finishOnDelete();

				}
				else
				{
					store_msg=getMessage(result);
					String error_code=getErrorCode(result);
					showToast(store_msg);
					if(error_code.equalsIgnoreCase("-116"))
					{
						session.logoutUser();
						invalidAuthFinish();
					}
				}
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}



		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("success");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		String getErrorCode(String status)
		{
			String error_code="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				error_code=jsonobject.getString("code");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				error_code="-1";
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				error_code="-1";
			}
			return error_code;
		}

		String getMessage(String status)
		{
			String message="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				message=jsonobject.getString("message");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return message;
		}

		String getData(String status)
		{
			String data="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject getDataObject=jsonobject.getJSONObject("data");
				data=getDataObject.getString("place_id");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return data;
		}


	}


	void invalidAuthFinish()
	{
		try
		{

			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.invalidCredential))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {

					dialog.dismiss();
					try
					{
						try
						{
							String active_place;
							active_place=dbUtil.getActiveAreaID();
							//		Log.i("Active Area Count", "Active Area Count in Drawer");
							Log.i("Active Area Count", "Active Area Count "+active_place);
							if(active_place.length()==0)
							{

								Intent intent=new Intent(context,LocationActivity.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
							}
							else
							{
								Intent intent=new Intent(context,NewsFeedActivity.class);
								startActivity(intent);
								finish();
								overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
							}
						}catch(Exception ex)
						{
							ex.printStackTrace();
						}
					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();

			/*showToast(getResources().getString(R.string.invalidAuth)+ " Try logging in again ");*/

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {

		try
		{
			if(v.getId()==reportIcon.getId() || v.getId()==txtReportBig.getId())
			{
				reports();
			}

			if(v.getId()==postIcon.getId() || v.getId()==txtTalkNowBig.getId())
			{
				talkNow();
			}

			if(v.getId()==viewIcon.getId() || v.getId()==txtViewPostBig.getId())
			{
				viewPost();
			}

			if(v.getId()==txtMoreBig.getId())
			{

				Intent intent=new Intent(context,MerchantsHomeGrid.class);
				EventTracker.logEvent("Tab_MerchantMoreButton", true);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	void reports()
	{

		//Reports Tab


		// TODO Auto-generated method stub

		if(STOREID.size()==0)
		{
			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.noStoreMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {


					try
					{
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						context.finish();

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int arg1) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();



		}
		else if(STOREID.size()==1)
		{
			EventTracker.logEvent("Tab_Report", false);

			Intent intent=new Intent(context,Reports.class);
			intent.putExtra("STORE_NAME", STORE_NAME.get(0));
			intent.putExtra("STORE_LOGO", PHOTO.get(0));
			intent.putExtra("STORE_ID", STOREID.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			Intent intent=new Intent(context,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("report",1);
			intent.putExtra("viewPost",-1);
			intent.putExtra("AddStore",-1);
			intent.putExtra("manageGallery",-1);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}




	}
	void talkNow()
	{

		//Talk Now


		// TODO Auto-generated method stub
		if(STOREID.size()==0)
		{
			/*	Intent intent=new Intent(context, EditStore.class);
			intent.putExtra("isNew", true);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();*/

			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.noStoreMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {


					try
					{
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						context.finish();

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int arg1) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();


		}
		else if(STOREID.size()==1)
		{
			EventTracker.logEvent("Tab_TalkNow", false);

			Intent intent=new Intent(context, CreateBroadCast.class);
			intent.putStringArrayListExtra("SELECTED_STORE_ID",STOREID);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			Intent intent=new Intent(context,PlaceChooser.class);
			intent.putExtra("choiceMode", 2);
			intent.putExtra("viewPost", 0);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}



	}
	void viewPost()
	{

		//View Post


		// TODO Auto-generated method stub
		if(STOREID.size()==0)
		{
			/*Intent intent=new Intent(context, EditStore.class);
			intent.putExtra("isNew", true);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();*/

			AlertDialog.Builder alertDialogBuilder3 = new AlertDialog.Builder(context);
			alertDialogBuilder3.setTitle(getResources().getString(R.string.actionBarTitle));
			alertDialogBuilder3
			.setMessage(getResources().getString(R.string.noStoreMessage))
			.setIcon(R.drawable.ic_launcher)
			.setCancelable(false)
			.setPositiveButton("Add Store",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {


					try
					{
						Intent intent=new Intent(context, EditStore.class);
						intent.putExtra("isNew", true);
						startActivity(intent);
						context.overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						context.finish();

					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int arg1) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			AlertDialog alertDialog3 = alertDialogBuilder3.create();

			alertDialog3.show();


		}
		else if(STOREID.size()==1)
		{
			EventTracker.logEvent("Tab_ViewPost", false);

			Intent intent=new Intent(context, ViewPost.class);
			intent.putExtra("storeName", STORE_NAME.get(0));
			intent.putExtra("STORE_ID", STOREID.get(0));
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			finish();
		}
		else
		{
			Intent intent=new Intent(context,PlaceChooser.class);
			intent.putExtra("choiceMode", 1);
			intent.putExtra("viewPost",1);
			intent.putExtra("AddStore",0);
			intent.putExtra("manageGallery",0);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}



	}


	void setTag(ArrayList<String>ID)
	{
		try
		{
			boolean isMisMatch=false;
			//Adding Tags

			Set<String> tags = new HashSet<String>(); 
			tags=PushManager.shared().getTags();

			ArrayList<String>tempIdList=new ArrayList<String>();

			String temp="";
			String replace="";
			if(tags.size()!=0)
			{
				//Getting All Tags
				Iterator<String> iterator=tags.iterator();

				//Traversing Tags to find out if there are any tags existing with own_id

				while(iterator.hasNext())
				{
					temp=iterator.next();

					Log.i("TAG", "Urban TAG PRINT "+temp);
					//If tag contains own_
					if(temp.startsWith("own_"))
					{
						//Replace own_ with "" and compare it with ID
						replace=temp.replaceAll("own_", "");
						tempIdList.add(replace);
						Log.i("TAG", "Urban TAG ID "+replace);

						//If Merchants Places ID arrayList does not contain ID which is already in tag list .
						//Set insMisMatch to true
						if(ID.contains(replace)==false)
						{
							isMisMatch=true;
							break;
						}
					}
				}

				if(isMisMatch==false)
				{
					//Check if Tag list has the Merchant Place ID's or not 
					for(int i=0;i<ID.size();i++)
					{
						if(tempIdList.contains(ID.get(i))==false)
						{
							isMisMatch=true;
							break;
						}
					}
				}

				Log.i("ID", "UAIR ID "+ID.toString());
				Log.i("ID", "UAIR ID TAG "+tempIdList.toString()+" "+isMisMatch);



				if(isMisMatch==true)
				{

					//Clear own_tag from Tag List
					for(int i=0;i<tempIdList.size();i++)
					{
						tags.remove("own_"+tempIdList.get(i));
					}

					//Add all ID's of Merchant Places to Tag list.
					for(int i=0;i<ID.size();i++)
					{
						tags.add("own_"+ID.get(i));
					}

					Log.i("TAG", "Urban TAG AFTER Merchant "+tags.toString());

					//Set Tags to Urban Airship
					PushManager.shared().setTags(tags);
				}



			}
		}catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	boolean hasOwnTag()
	{

		boolean hasTag=false;
		//Adding Tags
		Set<String> tags = new HashSet<String>(); 
		tags=PushManager.shared().getTags();
		String temp="";
		if(tags.size()!=0)
		{
			//Getting All Tags
			Iterator<String> iterator=tags.iterator();
			//Traversing Tags to find out if there are any tags existing with own_id
			while(iterator.hasNext())
			{
				temp=iterator.next();

				Log.i("TAG", "Urban TAG PRINT "+temp);
				//If tag contains own_
				if(temp.startsWith("own_"))
				{
					hasTag=true;
					break;
				}

			}

		}


		return hasTag;
	}

}
