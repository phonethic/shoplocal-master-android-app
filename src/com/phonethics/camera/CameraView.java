package com.phonethics.camera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.Utils;



@SuppressLint("NewApi")
public class CameraView extends Activity implements SurfaceHolder.Callback {


	SurfaceView surfaceView;

	final int REQ_CODE_CROPPEIMG = 3;

	int windowwidth, windowheight;


	/** SurfaceView */
	SurfaceHolder surfaceHolder;

	float orien1, orien2, orien3;

	/** Callbacks */
	PictureCallback rawCallback;
	ShutterCallback shutterCallback;
	PictureCallback jpegCallback;
	PreviewCallback previewCallback;

	ImageView ivCropper;

	ImageView ivCaptureImage;
	//ImageView imageViewChooseFromGallery;

	ImageView imageViewCancelCamera;
	TextView textViewTitle;
	ImageView imageViewFlash;

	ActionBar ab;

	/** Camera */
	Camera camera;

	long time;

	int orientation;

	float ivX, ivY;

	float dpi = 0.0f;

	int imageWidth, imageHeight;

	float[] screenDimen;
	float deviceWidth, deviceHeight;

	int camWidth, camHeight;

	boolean isFlashAvailable = false;
	/** flash MODES
	 * 0=off 1=on 2=auto */
	int flashMode=0;

	android.widget.RelativeLayout.LayoutParams layoutParams1;

	OrientationEventListener myOrientationEventListener;

	int iOrientation;

	int REQUEST_CODE_GALLERY = 1;

	//public static Bitmap bitmap;

	CameraImageSave cameraImageSave;

	int OSVersion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cameraview);

		initViews();


		Typeface type = Typeface.createFromAsset(getAssets(),"GOTHIC_0.TTF"); 
		textViewTitle.setTypeface(type);


		ivCropper.getLayoutParams().height = windowwidth;


		surfaceHolder.addCallback(this);
		if(OSVersion < 14){
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		surfaceHolder.setKeepScreenOn(true);


		Utils u = new Utils(CameraView.this);
		dpi = u.getScreenDPI();


		screenDimen = u.calculateScreenDimen();
		deviceWidth = screenDimen[1]; deviceHeight = screenDimen[0];

		calculateRealCameraDimen();

		surfaceView.getLayoutParams().height = camWidth;
		surfaceView.getLayoutParams().width = camHeight;

		Log.i("Screen dimen", "width: "+deviceWidth + " height: "+deviceHeight);

		ivCaptureImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(getApplicationContext(), ""+iOrientation, Toast.LENGTH_LONG).show();
				ivCaptureImage.setEnabled(false);
				imageViewCancelCamera.setEnabled(false);
				captureCamera();
			}
		});


		/** Check flash availabilty */
		isFlashAvailable = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
		if(isFlashAvailable){
			imageViewFlash.setVisibility(View.VISIBLE);
		}
		else
			imageViewFlash.setVisibility(View.GONE);

		/** Camera / shutter callbacks */
		rawCallback=new PictureCallback() {

			@Override
			public void onPictureTaken(byte[] data, Camera camera) {

			}
		};

		shutterCallback=new ShutterCallback() {

			@Override
			public void onShutter() {
			}
		};

		jpegCallback = new PictureCallback() {
			public void onPictureTaken(final byte[] data, Camera camera) {
				try {
					class AClass extends AsyncTask<Void, Void, Void>{

						ProgressDialog pd;
						Intent i = null;

						@Override
						protected void onPreExecute() {
							super.onPreExecute();
							i = new Intent(CameraView.this, ZoomCroppingActivity.class);
							i.putExtra("orientation", iOrientation); 
							
							pd = new ProgressDialog(CameraView.this);
							pd.setCancelable(false);
							pd.setTitle("Please Wait");
							pd.setMessage("Processing Image");
							pd.show();
						}

						@Override
						protected Void doInBackground(Void... params) {
							try{
								Bitmap bitmap=BitmapFactory.decodeByteArray(data, 0, data.length);

								bitmap = cropImage(bitmap);
								cameraImageSave.saveBitmapToFile(bitmap);

								bitmap = null;
							}
							catch(Exception e){

							}
							return null;
						}

						@Override
						protected void onPostExecute(Void result) {
							super.onPostExecute(result);
							try{
								//Intent i = new Intent(CameraView.this, ZoomCroppingActivity.class);
								//i.putExtra("orientation", iOrientation);  
								i.putExtra("comingfrom", "camera");
								pd.dismiss();
								startActivityForResult(i, REQ_CODE_CROPPEIMG);
							}
							catch(Exception e) { }
						}

					}

					new AClass().execute(null, null, null);


				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		imageViewFlash.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(isFlashAvailable && camera!=null){

					try{
						if(flashMode == 0){
							Parameters p = camera.getParameters();
							p.setFlashMode(Parameters.FLASH_MODE_ON);
							camera.setParameters(p);
							flashMode = 1;
							imageViewFlash.setImageResource(R.drawable.flashon);
						}

						else if(flashMode == 1){
							Parameters p = camera.getParameters();
							p.setFlashMode(Parameters.FLASH_MODE_AUTO);
							camera.setParameters(p);
							flashMode = 2;
							imageViewFlash.setImageResource(R.drawable.flashauto);
						}

						else if(flashMode == 2){
							Parameters p = camera.getParameters();
							p.setFlashMode(Parameters.FLASH_MODE_OFF);
							camera.setParameters(p);
							flashMode = 0;
							imageViewFlash.setImageResource(R.drawable.flashoff);
						}
					}
					catch(Exception ex){
						showToast("Looks like there's a problem setting the flash");
					}
				}

			}
		});


		/** Cancel camera view */
		imageViewCancelCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Intent i = new Intent();
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		
		myOrientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL)
		{

			@Override
			public void onOrientationChanged(int iAngle) {
				final int iLookup[] = {0,   0, 0,90,90, 90,90, 90,  90, 180, 180, 180, 180, 180, 180, 270, 270, 270, 270, 270, 270, 0, 0, 0}; // 15-degree increments
				//final int iLookup[] = {0,  90, 180, 270};
				if (iAngle != ORIENTATION_UNKNOWN)
				{
					int iNewOrientation = iLookup[iAngle / 15];
					if (iOrientation != iNewOrientation)
					{
						iOrientation = iNewOrientation;
						Log.i("Orientation value","Orientation value: "+iOrientation);
					}
				}
			}
		};
		
		// To display if orientation detection will work and enable it
		if (myOrientationEventListener.canDetectOrientation())
		{
			myOrientationEventListener.enable();
		}
		else
		{
			Toast.makeText(this, "Can't Detect Orientation. Your image may not be oriented correctly.", Toast.LENGTH_LONG).show();
		}
	
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(ivCaptureImage.isEnabled() == false || imageViewCancelCamera.isEnabled() == false){
			ivCaptureImage.setEnabled(true);
			imageViewCancelCamera.setEnabled(true);
		}
	}
	
	

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		//myOrientationEventListener.disable();
	}


	public void onDestroy()
	{
		super.onDestroy();

	}


	private void initViews(){

		//iOrientation = -1;
		surfaceView=(SurfaceView) findViewById(R.id.surfaceView);
		ivCropper=(ImageView) findViewById(R.id.ivCropper);
		ivCaptureImage = (ImageView) findViewById(R.id.imageViewCaptureImage);
		imageViewCancelCamera = (ImageView) findViewById(R.id.imageViewCancelCamera);
		imageViewFlash = (ImageView) findViewById(R.id.imageViewFlash);
		textViewTitle = (TextView) findViewById(R.id.textViewTitle);

		camera = Camera.open();

		cameraImageSave = new CameraImageSave();

		windowwidth = getWindowManager().getDefaultDisplay().getWidth();
		windowheight = getWindowManager().getDefaultDisplay().getHeight();

		Log.i("Screen dimen", "width: "+windowwidth + " height: "+windowheight);

		OSVersion = android.os.Build.VERSION.SDK_INT;

		surfaceHolder=surfaceView.getHolder();
	}



	Bitmap cropImage(Bitmap bitmap){

		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		int left = 0;

		if(width > height){
			left = (width - height)/2;
		}
		else {
			left = (height - width)/2;
		}
		Log.i("Left value","Left value = "+left+" width = "+width+" height = "+height);
		Bitmap newBitmap=Bitmap.createBitmap(bitmap,left,0, height, height);
		bitmap = null;

		return newBitmap;
	}


	//Down scaling bitmap to get Thumbnail
	public  Bitmap getThumbnail(Uri uri,int THUMBNAIL) throws FileNotFoundException, IOException{
		InputStream input = CameraView.this.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.RGB_565;
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();

		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL) ? (originalSize / THUMBNAIL) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;
		bitmapOptions.inPreferredConfig=Bitmap.Config.RGB_565;

		input = CameraView.this.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);


		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 2;
		else return k;
	}

	public Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	} 




	void calculateRealCameraDimen(){

		/** Get highest image resolution of device */
		float[] imgRes = getImageSize();
		//int[] imgRes = getPreviewSizeForCamera();

		camWidth = (int) ((deviceWidth * imgRes[1]) / imgRes[0]);
		camHeight = (int) ((deviceHeight * imgRes[0]) / imgRes[1]);
	}


	float[] getImageSize(){
		float[] imgRes = new float[2];

		List<Camera.Size> sizes = camera.getParameters().getSupportedPictureSizes();

		/** Get closest to 1000 */	
		int closestTo1000 = 0;
		int w1=0,h1=0;
		for(int i=0; i<sizes.size(); i++){
			int res;
			
			res = Math.abs(2000-sizes.get(i).height);
			Log.i("Image Size",""+sizes.get(i).height+" X "+sizes.get(i).width+" Difference: "+res);
			if(i == 0) {
				closestTo1000 = res;
				h1 = sizes.get(i).height;
				w1 = sizes.get(i).width; 
			}
			if(res < closestTo1000){
				h1 = sizes.get(i).height;
				w1 = sizes.get(i).width;
				closestTo1000 = res;
			}
		}

		imgRes[0] = h1/2; imgRes[1] = w1/2;

		return imgRes;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		startCamera();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		stopCamera();
	}


	/** Methods for start, stop and capture camera */
	void startCamera(){
		try{
			if(camera == null)
				camera=Camera.open();
		}
		catch(Exception e) { }


		Camera.Size size = getOptimalPreviewSize(camera.getParameters().getSupportedPreviewSizes(), surfaceView.getWidth(), surfaceView.getHeight());
		try{
			Parameters p = camera.getParameters();
			p.setPreviewSize(size.width, size.height);
			camera.setParameters(p);
			/*surfaceView.getLayoutParams().height = size.height;
			surfaceView.getLayoutParams().width = size.height;*/
		}
		catch(Exception e){ }

		camera.setDisplayOrientation(90);

		try{
			camera.setPreviewDisplay(surfaceHolder);
			camera.startPreview();
		}
		catch(Exception e){ }

	}

	void stopCamera(){
		camera.stopPreview();
		camera.release();
		camera=null;
	}

	void captureCamera(){
		camera.takePicture(shutterCallback, rawCallback, jpegCallback);
	}

	/* Method to get optimal preview size */
	void showToast(String msg){
		Toast.makeText(CameraView.this, msg, Toast.LENGTH_LONG).show();
	}


	int[] getPreviewSizeForCamera(){

		/** 0 is height 1 is width */
		int[] sizes = new int[2];

		List<Camera.Size> camSizes = camera.getParameters().getSupportedPreviewSizes();

		/** Get closest to 1000 */	
		int closestTo1000 = 0;
		int w1=0,h1=0;
		for(int i=0; i<camSizes.size(); i++){
			int res;

			res = Math.abs(1000-camSizes.get(i).width);
			Log.i("Image Size",""+camSizes.get(i).height+" X "+camSizes.get(i).width+" Difference: "+res);
			if(i == 0) {
				closestTo1000 = res;
				h1 = camSizes.get(i).height;
				w1 = camSizes.get(i).width; 
			}
			if(res < closestTo1000){
				h1 = camSizes.get(i).height;
				w1 = camSizes.get(i).width;
				closestTo1000 = res;
			}
		}

		sizes[0] = h1; sizes[1] = w1;


		return sizes;
	}

	private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio=(double)h / w;

		if (sizes == null) return null;

		Camera.Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Camera.Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Camera.Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}


	Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
		Camera.Size result=null;
		float dr = Float.MAX_VALUE;
		float ratio = (float)width/(float)height;

		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
			float r = (float)size.width/(float)size.height;
			if( Math.abs(r - ratio) < dr && size.width <= width && size.height <= height ) {
				dr = Math.abs(r - ratio);
				result = size;
			}
		}

		return result;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == REQ_CODE_CROPPEIMG && resultCode == RESULT_OK){
			Intent i = new Intent();

			setResult(RESULT_OK, i);
			finish();
		}


	}


	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}
}

