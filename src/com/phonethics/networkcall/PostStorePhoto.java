package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class PostStorePhoto extends IntentService {

	ResultReceiver uploadphoto;
	public PostStorePhoto(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public PostStorePhoto()
	{
		super("postPhoto");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		uploadphoto=intent.getParcelableExtra("postPhoto");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String USER_ID=intent.getStringExtra("user_id");
		String AUTH_ID=intent.getStringExtra("auth_id");
		String STORE_ID=intent.getStringExtra("store_id");

		String FILE_NAME=intent.getStringExtra("filename");
		String FILE_TYPE=intent.getStringExtra("filetype");
		String USER_FILE=intent.getStringExtra("userfile");
		
		String title=intent.getStringExtra("title");

		BufferedReader bufferedReader = null;
		String status = "";

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =  30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);
		//Setting URL to post data.
		/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/business_api/business");*/

		HttpPost httpPost=new HttpPost(URL);

		//Adding header.
		/*httpPost.addHeader("X-API-KEY", "Fool");*/
		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);

		try
		{
			Log.i("Response ", "Response POST URL "+URL);
			Log.i("Response ", "Response POST USER_ID "+USER_ID);
			Log.i("Response ", "Response POST AUTH_ID "+AUTH_ID);
			Log.i("Response ", "Response POST STORE_ID "+STORE_ID);
			Log.i("Response ", "Response POST FILE_NAME "+FILE_NAME);
			Log.i("Response ", "Response POST title "+title);
			Log.i("Response ", "Response POST FILE_TYPE "+FILE_TYPE);
			Log.i("Response ", "Response POST USER_FILE "+USER_FILE);



			JSONObject json = new JSONObject();
			json.put("place_id", STORE_ID);
			//json.put("filename", FILE_NAME);
			json.put("filetype", FILE_TYPE);
			json.put("userfile", USER_FILE);
			json.put("title", title);
			json.put("user_id", USER_ID);
			json.put("auth_id", AUTH_ID	);


			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response "+stringBuffer.toString());
			status=stringBuffer.toString();

			try
			{
				String broadcast_status,message,data;
				broadcast_status=getStatus(status);
				message=getMessage(status);
				
				Bundle b=new Bundle();
				b.putString("broadcast_image_status", broadcast_status);
				b.putString("broadcast_message",message);
				if(broadcast_status.equalsIgnoreCase("false"))
				{
					message=getMessage(status);
					data=getData(status);
					b.putString("broadcast_message",message);
					b.putString("data",data);
					b.putString("error_code", getErrorCode(status));
				}
				uploadphoto.send(0, b);
			}
			catch(Exception ex)
			{
				
				ex.printStackTrace();
				String broadcast_status,message,data = null;
				Bundle b=new Bundle();
				b.putString("broadcast_image_status", "error");
				message=getResources().getString(R.string.connectionTimedOut);
				b.putString("broadcast_message","Something went wrong");
				b.putString("error_code", "-1");
				b.putString("data",data);
				uploadphoto.send(0, b);
				ex.printStackTrace();
			}

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			String broadcast_status,message,data = null;
			Bundle b=new Bundle();
			b.putString("broadcast_image_status", "error");
			message=getResources().getString(R.string.connectionTimedOut);
			b.putString("broadcast_message",message);
			b.putString("data",data);
			b.putString("error_code", "-1");
			uploadphoto.send(0, b);
		}catch(SocketTimeoutException s)
		{
			String broadcast_status,message,data = null;
			Bundle b=new Bundle();
			b.putString("broadcast_image_status", "error");
			message=getResources().getString(R.string.connectionTimedOut);
			b.putString("broadcast_message",message);
			b.putString("error_code", "-1");
			b.putString("data",data);
			uploadphoto.send(0, b);

			s.printStackTrace();
		}
		catch(Exception ex)
		{
			String broadcast_status,message,data = null;
			Bundle b=new Bundle();
			b.putString("broadcast_image_status", "error");
			message=getResources().getString(R.string.connectionTimedOut);
			b.putString("broadcast_message","Something went wrong");
			b.putString("error_code", "-1");
			b.putString("data",data);
			uploadphoto.send(0, b);
			ex.printStackTrace();
		}



	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}


	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String message="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			message=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return message;
	}
	
	String getData(String status)
	{
		String data="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			JSONObject getDataObject=jsonobject.getJSONObject("data");
			data=getDataObject.getString("place_id");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return data;
	}

}
