package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;



import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetPeopleCountService extends IntentService{

	ResultReceiver getPeopleCount;
	
	BufferedReader bufferedReader = null;
	String status = "";
	
	String profile_status;
	
	String count;
	
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	public GetPeopleCountService(String name) {
		super(name);
		// TODO Auto-generated constructor stub


	}

	public GetPeopleCountService() {

		super("getPeopleCount");

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		getPeopleCount = intent.getParcelableExtra("getPeopleCount");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		String store_id=intent.getStringExtra("store_id");

		nameValuePairs.add(new BasicNameValuePair("place_id", store_id));
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;

		Log.i("GETPEOPLE COUNT", "GETPEOPLE COUNT URL "+URL);
		Log.i("GETPEOPLE COUNT", "GETPEOPLE COUNT USER_ID "+user_id);
		Log.i("GETPEOPLE COUNT", "GETPEOPLE COUNT AUTH_ID "+auth_id);
		try {

			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);

			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);
			
			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);
			
			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
			httpGet.addHeader("merchant_id", user_id);
			httpGet.addHeader("auth_id", auth_id);
			
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response "+stringBuffer.toString());
			status=stringBuffer.toString();
			
			try {
			

				JSONObject jsonobject=new JSONObject(status);
				
				profile_status = jsonobject.getString("success");
				
				if(profile_status.equalsIgnoreCase("true")){
					
					JSONObject peopleCount = jsonobject.getJSONObject("data");
					count = peopleCount.getString("customer_count");
					
					Log.d("COUNT","COUNT" + count);
					Bundle b= new Bundle();
					b.putString("prof_status", profile_status);
					b.putString("totalpeoplecount", count);
					getPeopleCount.send(0, b);
				}
				else
				{
					Bundle b= new Bundle();
					b.putString("prof_status", "false");
					b.putString("totalpeoplecount", count);
					getPeopleCount.send(0, b);
				}

				
			} catch (Exception e) {
				// TODO: handle exception
				
				e.printStackTrace();
				Bundle b=new Bundle();
				b.putString("prof_msg", getResources().getString(R.string.exception));
				b.putString("prof_status", "error");
				b.putString("totalpeoplecount", count);
			}
			
			
			
			

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("prof_status", "error");
			b.putString("totalpeoplecount", count);
			
			getPeopleCount.send(0, b);

		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("prof_status", "error");
			b.putString("totalpeoplecount", count);

			getPeopleCount.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.exception));
			b.putString("prof_status", "error");
			b.putString("totalpeoplecount", count);
			
			getPeopleCount.send(0, b);
			
		}

	}

}
