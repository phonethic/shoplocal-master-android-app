package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetPlaceCategoryReceiver extends ResultReceiver {

	GetCategory categ;
	public GetPlaceCategoryReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(categ!=null)
		{
			categ.onReceiveStoreCategories(resultCode, resultData);
		}
	}

	public interface GetCategory
	{
		public void onReceiveStoreCategories(int resultCode, Bundle resultData);
	}
	public void setReceiver(GetCategory categ)
	{
		this.categ=categ;
	}
}
