package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.model.GetBroadCast;
import com.phonethics.model.GetSpecialDates;
import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetSpecialDatesService extends IntentService{

	ResultReceiver getSpecialDates;
	
	BufferedReader bufferedReader = null;
	String status = "";
	String getSpecialDateStatus;
	
	ArrayList<String> calenderDate = new ArrayList<String>();
	ArrayList<String> type = new ArrayList<String>();
	ArrayList<String> count = new ArrayList<String>();
	
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	
	ArrayList<GetSpecialDates> getAllSpecialDates=new ArrayList<GetSpecialDates>();

	public GetSpecialDatesService(){

		super("GetAreaService");
	}

	public GetSpecialDatesService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		getSpecialDates = intent.getParcelableExtra("getSpecialDates");
		
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		String place_id=intent.getStringExtra("place_id");
		
		nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
		
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;
		
		try {
			
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
			httpGet.addHeader("user_id",user_id);
			httpGet.addHeader("auth_id",auth_id);

			Log.i("URL", "URL Special: "+URL);
			

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service SpecialDate: "+stringBuffer.toString());
			status=stringBuffer.toString();
			
			JSONObject jsonobject=new JSONObject(status);
			
			getSpecialDateStatus = jsonobject.getString("success");
			
			try {
				
				if(getSpecialDateStatus.equalsIgnoreCase("true")){
				

					JSONArray data = jsonobject.getJSONArray("data");
					
					for(int i=0;i<data.length();i++){
						

						JSONObject tempObj = data.getJSONObject(i);
						Log.d("DATES","DATES" + tempObj.getString("date"));
						calenderDate.add(data.getJSONObject(i).getString("date"));
						
						GetSpecialDates setData = new GetSpecialDates();
						
						setData.setCalenderdate(data.getJSONObject(i).getString("date"));
						
//						JSONArray dateType = index.getJSONArray("date_type");
//						
//						for(int m =0;m<dateType.length();m++){
//							
//							Log.d("TYPE","TYPE" + dateType.getJSONObject(i).getString("type"));
//							Log.d("COUNT","COUNT" + dateType.getJSONObject(i).getString("count"));
//						}
						
						JSONArray dateType = tempObj.getJSONArray("date_type");
						
						for(int m=0; m<dateType.length(); m++){
							
							JSONObject tempObjDateType = dateType.getJSONObject(m);
							
							Log.d("TYPE","TYPE == " + tempObjDateType.getString("type"));
							Log.d("COUNT","COUNT == " + tempObjDateType.getString("count"));
							
//							type.add(tempObjDateType.getString("type"));
//							count.add(tempObjDateType.getString("count"));
							
							setData.setType(tempObjDateType.getString("type"));
							setData.setCount(tempObjDateType.getString("count"));
							
							
						}
						
						
						
						getAllSpecialDates.add(setData);
						
					}
					
				}
				else if(getSpecialDateStatus.equalsIgnoreCase("false"))
				{
					
				}
				
				Bundle b = new Bundle();
				b.putString("getSpecialDateStatus", getSpecialDateStatus);
				b.putParcelableArrayList("getAllSpecialDates",getAllSpecialDates);
				//b.putStringArrayList("calenderDates", calenderDate);
//				b.putStringArrayList("allTypes", type);
//				b.putStringArrayList("allCount", count);
				getSpecialDates.send(0, b);
				
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			
			
			Bundle b=new Bundle();
			b.putString("area_status", "false");
			b.putString("message", getResources().getString(R.string.connectionTimedOut));
			getSpecialDates.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			
			Bundle b=new Bundle();
			b.putString("area_status", "false");
			b.putString("message", getResources().getString(R.string.connectionTimedOut));
			getSpecialDates.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
		}
	}

}
