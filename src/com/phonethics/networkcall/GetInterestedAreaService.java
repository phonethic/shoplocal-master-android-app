package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetInterestedAreaService extends IntentService{
	
	ResultReceiver getInterestedAreas;
	
	String getInterestedAreaStatus;
	
	String dataMSg;
	
	ArrayList<String> area_ids = new ArrayList<String>();;
	
	public GetInterestedAreaService() {
		super("GetInterestedAreas");
		// TODO Auto-generated constructor stub
		
		
	}

	public GetInterestedAreaService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		
		getInterestedAreas = intent.getParcelableExtra("getInterestedAreas");
		
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		BufferedReader bufferedReader = null;
		String status = "";
		
		
		try {
			
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
			httpGet.addHeader("user_id",user_id);
			httpGet.addHeader("auth_id",auth_id);

			Log.i("URL", "URL Date : "+URL);
			Log.i("URL", "URL Date : "+user_id);
			Log.i("URL", "URL Date : "+auth_id);

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Interested Areas: "+stringBuffer.toString());
			status=stringBuffer.toString();
			
			JSONObject jsonobject=new JSONObject(status);
			
			getInterestedAreaStatus = jsonobject.getString("success");
			
			
			
			if(getInterestedAreaStatus.equalsIgnoreCase("true")){
				
				JSONObject data = jsonobject.getJSONObject("data");
				
				JSONArray areas = data.getJSONArray("areas");
				
				for(int i=0;i<areas.length();i++){
					
					area_ids.add(areas.getJSONObject(i).getString("area_id"));
				}
			}
			else{
				
				dataMSg = jsonobject.getString("message");
			}
			
			Bundle b = new Bundle();
			b.putString("status", getInterestedAreaStatus);
			b.putStringArrayList("allAreaIds", area_ids);
			b.putString("prof_msg", dataMSg);
			getInterestedAreas.send(0, b);
			
		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
		
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("status", "false");
			getInterestedAreas.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("status", "false");
			b.putString("prof_msg", "Connection Timed out");
		
			getInterestedAreas.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("prof_msg", "Try again");
			b.putString("status", "false");
			getInterestedAreas.send(0, b);
		}
	}

}
