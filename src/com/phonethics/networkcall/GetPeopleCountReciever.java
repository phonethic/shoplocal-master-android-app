package com.phonethics.networkcall;

import com.phonethics.networkcall.AddCustomerProfileReceiver.CustomerProfileReceiver;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetPeopleCountReciever extends ResultReceiver {
	
	
	PeopleCountReceiver countRec;

	public GetPeopleCountReciever(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
		
		
	}
	
	
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		
		if(countRec!=null){
			
			countRec.onReceiveCountApi(resultCode, resultData);
		}
	}



	public interface PeopleCountReceiver
	{
		public void onReceiveCountApi(int resultCode, Bundle resultData);
	}

	public void setReceiver(PeopleCountReceiver countRec)
	{
		this.countRec = countRec;
	}
}
