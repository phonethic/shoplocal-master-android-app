package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetPerticularBusinessDetailService extends IntentService{

	ResultReceiver getBusiness;
	String BUSINESS_ID;
	String BUSINESS_USER_ID;
	String BUSINESS_TYPE_ID;
	String YEAR_OF_ESTABLISHMENT;
	String BUSINESS_NAME;
	String DESCRIPTION;
	String LOGO;
	String WEBSITE;
	
	public GetPerticularBusinessDetailService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetPerticularBusinessDetailService()
	{
		super("GetPerticularBusinessDetailServiceTag");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		getBusiness=intent.getParcelableExtra("getBusiness");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		String business_id=intent.getStringExtra("business_id");

		BufferedReader bufferedReader = null;
		String status = "";

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient();

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);
		httpGet.addHeader("business_id",business_id);

		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response "+stringBuffer.toString());
			status=stringBuffer.toString();

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		/*
		 * Parsing JSON DATA
		 */
		try
		{
			JSONObject jsonobject=new JSONObject(status);

			JSONObject getJson=jsonobject.getJSONObject("status");

			for(int i=0;i<getJson.length();i++)
			{
				BUSINESS_ID=getJson.getString("id");
				BUSINESS_USER_ID=getJson.getString("business_user_id");
				BUSINESS_NAME=getJson.getString("business_name");
				DESCRIPTION=getJson.getString("description");
				BUSINESS_TYPE_ID=getJson.getString("business_type_id");
				YEAR_OF_ESTABLISHMENT=getJson.getString("year_of_establish");
				WEBSITE=getJson.getString("website");
				LOGO=getJson.getString("logo");
			}

			Bundle b=new Bundle();
			b.putString("BUSINESS_ID", BUSINESS_ID);
			b.putString("BUSINESS_NAME", BUSINESS_NAME);
			b.putString("BUSINESS_USER_ID", BUSINESS_USER_ID);
			b.putString("DESCRIPTION", DESCRIPTION);
			b.putString("BUSINESS_TYPE_ID", BUSINESS_TYPE_ID);
			b.putString("YEAR_OF_ESTABLISHMENT", YEAR_OF_ESTABLISHMENT);
			b.putString("WEBSITE", WEBSITE);
			b.putString("LOGO", LOGO);
			getBusiness.send(0, b);

		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

}
