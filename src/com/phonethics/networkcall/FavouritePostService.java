package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class FavouritePostService extends IntentService {

	ResultReceiver favPost;
	public FavouritePostService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public FavouritePostService()
	{
		super("favouritePost");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		favPost=intent.getParcelableExtra("favouritePost");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		
		String post_id=intent.getStringExtra("post_id");
		String auth_id=intent.getStringExtra("auth_id");
		String user_id=intent.getStringExtra("user_id");

		BufferedReader bufferedReader = null;
		String status="";

		HttpParams httpParams = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to post data.
		HttpPost httpPost=new HttpPost(URL);
		//Adding header.

		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);
		
		try
		{
			JSONObject json = new JSONObject();
			/*				json.put("verification_code", verification_code);*/

			/*				json.put("confirm_password", confirm_password);*/
			/*				json.put("user_id", user_id);*/
			json.put("user_id", user_id);
			json.put("post_id", post_id);
			json.put("auth_id", auth_id);
			Log.i("Response", "Response POST LIKE "+""+URL+"");
			Log.i("Response", "Response POST LIKE  "+""+user_id+"");
			Log.i("Response", "Response POST LIKE  "+""+auth_id+"");
			Log.i("Response", "Response POST LIKE  "+""+post_id+"");

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response POST LIKE   "+stringBuffer.toString());
			status=stringBuffer.toString();

			
			String likestatus=getStatus(status);
			String likemsg=getMessage(status);
	
			if(likestatus.equalsIgnoreCase("true"))
			{
			Bundle b=new Bundle();
			b.putString("postLikestatus", likestatus);
			b.putString("postLikemsg", likemsg);
			favPost.send(0, b);
			}
			else if(likestatus.equalsIgnoreCase("false"))
			{
				Bundle b=new Bundle();
				b.putString("postLikestatus", likestatus);
				b.putString("postLikemsg", likemsg);
				b.putString("error_code", getErrorCode(status));
				favPost.send(0, b);
				
			}

		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();

			String likestatus=getStatus(status);


			Bundle b=new Bundle();
			b.putString("postLikestatus", likestatus);
			b.putString("postLikemsg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			favPost.send(0, b);
		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			String likestatus=getStatus(status);


			Bundle b=new Bundle();
			b.putString("postLikestatus", "error");
			b.putString("postLikemsg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			favPost.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			String likestatus=getStatus(status);
			String likemsg=getMessage(status);

			Bundle b=new Bundle();
			b.putString("postLikestatus", "error");
			b.putString("postLikemsg",getResources().getString(R.string.exception));
			b.putString("error_code", "-1");
			favPost.send(0, b);
		}

	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}

	
	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String usersmsg="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			usersmsg=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usersmsg;
	}



}
