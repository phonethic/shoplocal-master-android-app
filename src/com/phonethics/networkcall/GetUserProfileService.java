package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.localnotification.LocalNotification;
import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetUserProfileService extends IntentService {

	ResultReceiver getUserProfile;
	String profile_status;
	String profile_data;

	String ID;
	String NAME;
	String MOBILE;
	String EMAIL;
	String DOB;
	String GENDER;
	String CITY;
	String STATE;
	String IMAGE_URL; 
	String FACEBOOK_USER_ID="";
	String FACEBOOK_ACCESSTOKEN="";

	ArrayList<String> fethedDates;

	ArrayList<String> fethedDateIds;

	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	LocalNotification localNotification;

	public GetUserProfileService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetUserProfileService()
	{
		super("getUserProfile");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		getUserProfile=intent.getParcelableExtra("getUserProfile");

		localNotification=new LocalNotification(getApplicationContext());

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");

		BufferedReader bufferedReader = null;
		String status = "";

		nameValuePairs.add(new BasicNameValuePair("user_id", user_id));
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;

		fethedDates = new ArrayList<String>();
		fethedDateIds = new ArrayList<String>();

		try
		{
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
			httpGet.addHeader("user_id",user_id);
			httpGet.addHeader("auth_id",auth_id);

			Log.i("URL", "URL : "+URL);
			Log.i("URL", "URL : "+user_id);
			Log.i("URL", "URL : "+auth_id);

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response IS PROFILE EXISTS Details: "+stringBuffer.toString());
			status=stringBuffer.toString();

			JSONObject jsonobject=new JSONObject(status);

			profile_status=jsonobject.getString("success");
			profile_data=jsonobject.getString("message");
			if(profile_status.equalsIgnoreCase("true"))
			{
				JSONObject user_profile_data=jsonobject.getJSONObject("data");
				ID=user_profile_data.getString("id");
				NAME=user_profile_data.getString("name");
				MOBILE=user_profile_data.getString("mobile");
				EMAIL=user_profile_data.getString("email");
				//	DOB=user_profile_data.getString("dob");
				GENDER=user_profile_data.getString("gender");
				CITY=user_profile_data.getString("city");
				STATE=user_profile_data.getString("state");
				IMAGE_URL=user_profile_data.getString("image_url");

				FACEBOOK_USER_ID = user_profile_data.getString("facebook_user_id");

				Log.d("IMAGEURL","IMAGEURL" + IMAGE_URL);

				try {

					//JSONArray dates = new JSONArray("dates");
					JSONArray dates = null;
					String  	datesStr = "";
					try{
						dates = user_profile_data.getJSONArray("dates");
					}catch(Exception ex){
						ex.printStackTrace();
					}

					if(dates == null){

						datesStr = user_profile_data.getString("dates");
					}
					else{


						for(int i=0;i<dates.length();i++){

							JSONObject datesData = dates.getJSONObject(i);

							fethedDateIds.add(datesData.getString("date_category"));
							fethedDates.add(datesData.getString("date"));

							Log.d("FETCHING DATES","FETCHING DATES" +  datesData.getString("date_category"));

						}
					}

					if(fethedDateIds.size()>0)
					{
						localNotification.checkPref();
						localNotification.setProfilePref(true, localNotification.isProfilAlarmFired());
					}
					else
					{
						localNotification.checkPref();
						localNotification.setProfilePref(false, localNotification.isProfilAlarmFired());
					}

				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();

					Bundle b=new Bundle();
					b.putString("prof_msg", "Try again");
					b.putString("prof_status", "false");
					b.putString("ID", ID);
					b.putString("NAME", NAME);
					b.putString("MOBILE", MOBILE);
					b.putString("EMAIL", EMAIL);
					//b.putString("DOB", DOB);
					b.putString("GENDER", GENDER);
					b.putString("CITY", CITY);
					b.putString("STATE",STATE);
					b.putString("IMAGE_URL",IMAGE_URL);
					b.putString("FACEBOOK_USER_ID", FACEBOOK_USER_ID);

					b.putStringArrayList("categoryList", fethedDateIds);
					b.putStringArrayList("datesList", fethedDates);
					b.putString("error_code","-1");
					getUserProfile.send(0, b);
				}






				/*try
				{
					FACEBOOK_USER_ID=user_profile_data.getString("images");
					FACEBOOK_ACCESSTOKEN=user_profile_data.getString("images");
				}catch(Exception ex)
				{
					ex.printStackTrace();
					Log.i("Log"," Error");

				}
				if(FACEBOOK_USER_ID!=null)
				{
					Log.i("found","found");

				}
				else
				{
					Log.i("Nofound","notfound");
					get.setSource("notfound");
				}*/


				Bundle b=new Bundle();
				b.putString("prof_status", profile_status);
				b.putString("prof_msg", profile_data);
				b.putString("ID", ID);
				b.putString("NAME", NAME);
				b.putString("MOBILE", MOBILE);
				b.putString("EMAIL", EMAIL);
				//b.putString("DOB", DOB);
				b.putString("GENDER", GENDER);
				b.putString("CITY", CITY);
				b.putString("STATE",STATE);
				b.putString("IMAGE_URL",IMAGE_URL);
				b.putString("FACEBOOK_USER_ID", FACEBOOK_USER_ID);

				b.putStringArrayList("categoryList", fethedDateIds);
				b.putStringArrayList("datesList", fethedDates);

				getUserProfile.send(0, b);

			}
			else if(profile_status.equalsIgnoreCase("false"))
			{
				Bundle b=new Bundle();
				b.putString("prof_status", profile_status);
				b.putString("prof_msg", profile_data);
				b.putString("error_code",getErrorCode(status));

				getUserProfile.send(0, b);
			}





		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("prof_status", "error");
			b.putString("error_code","-1");
			getUserProfile.send(0, b);

		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("prof_msg", "Connection Timed out");
			b.putString("prof_status", "error");
			b.putString("error_code","-1");
			getUserProfile.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.exception));
			b.putString("prof_status", "error");
			b.putString("error_code","-1");
			getUserProfile.send(0, b);
		}

	}

	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}

}
