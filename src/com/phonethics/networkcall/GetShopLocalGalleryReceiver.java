package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetShopLocalGalleryReceiver extends ResultReceiver {

	ShopLocalGallery rec;
	public GetShopLocalGalleryReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onReceiveShopLocalGallery(resultCode, resultData);
		}
	}

	public interface ShopLocalGallery
	{
		public void onReceiveShopLocalGallery(int resultCode, Bundle resultData);
	}
	public void setReceiver(ShopLocalGallery rec)
	{
		this.rec=rec;
	}
	

}
