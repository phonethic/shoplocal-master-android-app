package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetUsersFavPlacesService extends IntentService {

	ResultReceiver userfav;
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> STORE_NAME=new ArrayList<String>();	
	ArrayList<String> BUILDING=new ArrayList<String>();
	ArrayList<String> STREET=new ArrayList<String>();
	ArrayList<String> LANDMARK=new ArrayList<String>();
	ArrayList<String> AREA_ID=new ArrayList<String>();
	ArrayList<String> AREA=new ArrayList<String>();
	ArrayList<String> CITY=new ArrayList<String>();
	ArrayList<String> STATE=new ArrayList<String>();
	ArrayList<String> COUNTRY=new ArrayList<String>();
	
	ArrayList<String> MOBILE_NO=new ArrayList<String>();
	ArrayList<String> MOBILE_NO2=new ArrayList<String>();
	ArrayList<String> MOBILE_NO3=new ArrayList<String>();
	
	ArrayList<String> TEL_NO=new ArrayList<String>();
	ArrayList<String> TEL_NO2=new ArrayList<String>();
	ArrayList<String> TEL_NO3=new ArrayList<String>();
	
	ArrayList<String> PHOTO=new ArrayList<String>();
	ArrayList<String> DISTANCE=new ArrayList<String>();

	ArrayList<String> DESCRIPTION=new ArrayList<String>();

	ArrayList<String> HAS_OFFERS=new ArrayList<String>();
	ArrayList<String> OFFERS=new ArrayList<String>();

	ArrayList<String> PLACE_PARENT=new ArrayList<String>();
	ArrayList<String> TOTAL_LIKE=new ArrayList<String>();
	ArrayList<String> VERIFIED=new ArrayList<String>();

	ArrayList<String> EMAIL=new ArrayList<String>();
	ArrayList<String> WEBSITE=new ArrayList<String>();

	String LIKE_STATUS="";
	String LIKE_MESSAGE="";
	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";


	public GetUsersFavPlacesService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetUsersFavPlacesService()
	{
		super("GetUsersFavPlaces");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		userfav=intent.getParcelableExtra("userfav");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String search_post_count=intent.getIntExtra("search_post_count",0)+"";
		String search_page=intent.getIntExtra("search_page",0)+"";
		boolean isPlaceIdSearch=intent.getBooleanExtra("isPlaceId", false);


		String place_id=intent.getStringExtra("place_id");
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");



		if(isPlaceIdSearch)
		{
			nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
		}
		nameValuePairs.add(new BasicNameValuePair("page", search_page));
		nameValuePairs.add(new BasicNameValuePair("count", search_post_count));


		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;
		
		Log.i("URL","FAV PLACE URL "+URL);
		Log.i("URL","FAV PLACE user_id "+user_id);
		Log.i("URL","FAV PLACE auth_id "+auth_id);

		BufferedReader bufferedReader = null;
		String status = "";

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id", user_id);
		httpGet.addHeader("auth_id", auth_id);
		try
		{

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response "+stringBuffer.toString());

			status=stringBuffer.toString();

			JSONObject jsonobject=new JSONObject(status);
			LIKE_STATUS=jsonobject.getString("success");
			if(LIKE_STATUS.equalsIgnoreCase("true"))
			{
				Log.i("Status ", "Status "+LIKE_STATUS);
				JSONObject getMsg=jsonobject.getJSONObject("data");

				JSONArray getRecord=getMsg.getJSONArray("record");
				TOTAL_PAGES=getMsg.getString("total_page");
				TOTAL_RECORDS=getMsg.getString("total_record");
				Log.i("Status ", "Status TOTAL_PAGES "+TOTAL_PAGES);
				Log.i("Status ", "Status TOTAL_RECORDS "+TOTAL_RECORDS);

				for(int i=0;i<getRecord.length();i++)
				{
					JSONObject records=getRecord.getJSONObject(i);
					ID.add(records.getString("id"));
					PLACE_PARENT.add(records.getString("place_parent"));
					STORE_NAME.add(records.getString("name"));
					DESCRIPTION.add(records.getString("description"));
					BUILDING.add(records.getString("building"));
					STREET.add(records.getString("street"));
					LANDMARK.add(records.getString("landmark"));
					AREA_ID.add(records.getString("area_id"));
					AREA.add(records.getString("area"));
					CITY.add(records.getString("city"));
					
					MOBILE_NO.add(records.getString("mob_no1"));
					MOBILE_NO2.add(records.getString("mob_no2"));
					MOBILE_NO3.add(records.getString("mob_no3"));
					
					TEL_NO.add(records.getString("tel_no1"));
					TEL_NO2.add(records.getString("tel_no2"));
					TEL_NO3.add(records.getString("tel_no3"));
					
//					MOBILE_NO2.add("");
//					MOBILE_NO3.add("");
//					
//					TEL_NO.add("");
//					TEL_NO2.add("");
//					TEL_NO3.add("");
//					
					
					
					PHOTO.add(records.getString("image_url"));
					EMAIL.add(records.getString("email"));
					WEBSITE.add(records.getString("website"));
					TOTAL_LIKE.add(records.getString("total_like"));
					VERIFIED.add(records.getString("verified"));

				}

				Log.i("FAV", "FAV API "+"SIZE "+ID.size()+" "+PLACE_PARENT.size()+" "+STORE_NAME.size()+" "+DESCRIPTION.size()+" "+BUILDING.size()+" "+STREET.size()
						+" "+LANDMARK.size()+" "+AREA.size()+" "+CITY.size()+" "+MOBILE_NO.size()+" "+PHOTO.size()+" "+EMAIL.size()+" "+WEBSITE.size()+" "+TOTAL_LIKE.size());
				Bundle b=new Bundle();
				b.putString("LIKE_MESSAGE", LIKE_MESSAGE);
				b.putString("LIKE_STATUS", LIKE_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("PLACE_PARENT", PLACE_PARENT);
				b.putStringArrayList("STORE_NAME", STORE_NAME);
				b.putStringArrayList("DESCRIPTION", DESCRIPTION);
				b.putStringArrayList("BUILDING", BUILDING);
				b.putStringArrayList("STREET", STREET);
				b.putStringArrayList("LANDMARK", LANDMARK);
				b.putStringArrayList("AREA_ID", AREA_ID);
				b.putStringArrayList("AREA", AREA);
				b.putStringArrayList("CITY", CITY);
				
				b.putStringArrayList("MOBILE_NO", MOBILE_NO);
				b.putStringArrayList("MOBILE_NO2", MOBILE_NO2);
				b.putStringArrayList("MOBILE_NO3", MOBILE_NO3);
				
				b.putStringArrayList("TEL_NO", TEL_NO);
				b.putStringArrayList("TEL_NO2", TEL_NO2);
				b.putStringArrayList("TEL_NO3", TEL_NO3);
				
				b.putStringArrayList("PHOTO", PHOTO);
				b.putStringArrayList("EMAIL", EMAIL);
				b.putStringArrayList("WEBSITE", WEBSITE);
				b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
				b.putStringArrayList("VERIFIED", VERIFIED);
				b.putString("TOTAL_PAGES", TOTAL_PAGES);
				b.putString("TOTAL_RECORDS", TOTAL_RECORDS);
				userfav.send(0, b);
			}
			else if(LIKE_STATUS.equalsIgnoreCase("false"))
			{
				LIKE_MESSAGE=jsonobject.getString("message");
				String error_code=jsonobject.getString("code");
				Bundle b=new Bundle();
				Log.i("LIKE IN SERVER ", "LIKE_MESSAGE SERVER "+LIKE_MESSAGE);
				b.putString("LIKE_MESSAGE", LIKE_MESSAGE);
				b.putString("LIKE_STATUS", "false");
				b.putString("error_code",error_code);
				
				userfav.send(0, b);
			}

		}catch(ConnectTimeoutException c)
		{
			Bundle b=new Bundle();
			b.putString("LIKE_MESSAGE", getResources().getString(R.string.connectionTimedOut));
			b.putString("LIKE_STATUS", "error");
			b.putString("error_code", "-1");
			userfav.send(0, b);
		}catch(SocketTimeoutException sqt)
		{
			Bundle b=new Bundle();
			b.putString("LIKE_MESSAGE", getResources().getString(R.string.connectionTimedOut));
			b.putString("LIKE_STATUS", "error");
			b.putString("error_code", "-1");
			userfav.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("LIKE_MESSAGE", "Please try again afeter some time");
			b.putString("LIKE_STATUS", "error");
			b.putString("error_code", "-1");
			userfav.send(0, b);
		}



	}

}
