package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class UpdateStoreDetailsReceiver extends ResultReceiver {

	UpdateStoreReceiver rec;
	
	public UpdateStoreDetailsReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.updateStoreReceiver(resultCode, resultData);
		}
	}
	
	public interface UpdateStoreReceiver
	{
		public void updateStoreReceiver(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(UpdateStoreReceiver rec)
	{
		this.rec=rec;
	}
	

}
