package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetPerticularBusinessDetailReceiver extends ResultReceiver{

	BusinessDetail bdetail;
	public GetPerticularBusinessDetailReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(bdetail!=null)
		{
			bdetail.onReceiveBusinessDetail(resultCode, resultData);
		}
	}
	
	public interface BusinessDetail
	{
		public void onReceiveBusinessDetail(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(BusinessDetail receiver)
	{
		this.bdetail=receiver;
	}

}
