package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetPerticularStoreOperationService extends IntentService{

	ResultReceiver getOperation;
	ArrayList<String> TIME1_OPEN=new ArrayList<String>();
	ArrayList<String>  TIME1_CLOSE=new ArrayList<String>();
	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> STORE_ID=new ArrayList<String>();
	ArrayList<String>  PLACE_STATUS=new ArrayList<String>();
	ArrayList<String>  DAY=new ArrayList<String>();
	String DUAL_TIME_STATUS;
	String TIME2_OPEN;
	String TIME2_CLOSE;
	String operation_Status;
	public GetPerticularStoreOperationService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetPerticularStoreOperationService()
	{
		super("GetPerticularStoreOperationServiceTag");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		getOperation=intent.getParcelableExtra("getOperation");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");

		String store_id=intent.getStringExtra("store_id");

		BufferedReader bufferedReader = null;
		String status = "";

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient();

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);
		httpGet.addHeader("store_id",store_id);

		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Operation : "+stringBuffer.toString());
			status=stringBuffer.toString();

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		/*
		 * Parsing JSON DATA
		 */
		try
		{
			JSONObject jsonobject=new JSONObject(status);
			JSONArray getMessage=jsonobject.getJSONArray("message");
			operation_Status=jsonobject.getString("status");
			if(operation_Status.equalsIgnoreCase("set"))
			{
				for(int i=0;i<getMessage.length();i++)
				{
					JSONObject getData=getMessage.getJSONObject(i);
					ID.add(getData.getString("id"));
					STORE_ID.add(getData.getString("store_id"));
					DAY.add(getData.getString("day"));
					PLACE_STATUS.add(getData.getString("is_closed"));
					TIME1_OPEN.add(getData.getString("open_time"));
					TIME1_CLOSE.add(getData.getString("close_time"));
			
				}
				
			}
			Bundle b=new Bundle();
			b.putString("operation_Status", operation_Status);
			b.putStringArrayList("ID", ID);
			b.putStringArrayList("STORE_ID", STORE_ID);
			b.putStringArrayList("DAY", DAY);
			b.putStringArrayList("PLACE_STATUS", PLACE_STATUS);
			b.putStringArrayList("TIME1_OPEN", TIME1_OPEN);
			b.putStringArrayList("TIME1_CLOSE", TIME1_CLOSE);
		

			getOperation.send(0, b);
		}catch(JSONException js)
		{
			js.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
