package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.phonethics.shoplocal.R;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetPlaceCategoryService extends IntentService {

	ResultReceiver category;
	String category_status;
	String category_msg;
	ArrayList<String>CATEGORY_NAME=new ArrayList<String>();
	ArrayList<String>CATEGORY_ID=new ArrayList<String>();
	public GetPlaceCategoryService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetPlaceCategoryService()
	{
		super("GetCategory");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		category=intent.getParcelableExtra("category");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		String value=intent.getStringExtra("value");
		boolean IsAppend=intent.getBooleanExtra("IsAppend", false);

		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");


		//			URL+="?for="+value;


		BufferedReader bufferedReader = null;
		String status = "";


		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =  30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);



		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		/*		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);*/

		Log.i("URL", "URL : "+URL);
		Log.i("URL", "URL : "+user_id);
		Log.i("URL", "URL : "+auth_id);

		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Category Service Response Category Details: "+stringBuffer.toString());
			status=stringBuffer.toString();

			/*
			 * Parsing JSON DATA
			 */
			try
			{
				JSONObject jsonobject=new JSONObject(status);
				JSONArray getMessage=jsonobject.getJSONArray("data");
				category_status=jsonobject.getString("success");
				if(category_status.equalsIgnoreCase("true"))
				{
					for(int i=0;i<getMessage.length();i++)
					{
						JSONObject getData=getMessage.getJSONObject(i);
						CATEGORY_ID.add(getData.getString("id"));
						CATEGORY_NAME.add(getData.getString("label"));


					}
					Bundle b=new Bundle();
					b.putString("category_status", category_status);
					b.putStringArrayList("CATEGORY_ID", CATEGORY_ID);
					b.putStringArrayList("CATEGORY_NAME", CATEGORY_NAME);

					category.send(0, b);


				}
				else
				{
					category_msg=jsonobject.getString("message");
					Bundle b=new Bundle();
					b.putString("category_status", category_status);
					b.putString("category_msg", category_msg);
					b.putStringArrayList("CATEGORY_ID", CATEGORY_ID);
					b.putStringArrayList("CATEGORY_NAME", CATEGORY_NAME);

					category.send(0, b);

				}
			}catch(JSONException js)
			{
				Bundle b=new Bundle();
				b.putString("category_status", "error");
				b.putString("category_msg", getResources().getString(R.string.exception));
				category.send(0, b);


				js.printStackTrace();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Bundle b=new Bundle();
				b.putString("category_status", "error");
				b.putString("category_msg", getResources().getString(R.string.exception));
				category.send(0, b);
			}


		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Bundle b=new Bundle();
			b.putString("category_status", "error");
			b.putString("category_msg",getResources().getString(R.string.connectionTimedOut));
			category.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Bundle b=new Bundle();
			b.putString("category_status", "error");
			b.putString("category_msg",getResources().getString(R.string.connectionTimedOut));
			category.send(0, b);
		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("category_status", "error");
			b.putString("category_msg",getResources().getString(R.string.exception));
			category.send(0, b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("category_status", "error");
			b.putString("category_msg",getResources().getString(R.string.exception));
			category.send(0, b);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("category_status", "error");
			b.putString("category_msg",getResources().getString(R.string.exception));
			category.send(0, b);
		}


	}


}
