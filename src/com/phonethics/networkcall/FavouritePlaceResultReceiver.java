package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class FavouritePlaceResultReceiver extends ResultReceiver {

	FavouriteInterface rec;
	public FavouritePlaceResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.likeReuslt(resultCode, resultData);
		}
		
	}
	
	public interface FavouriteInterface
	{
		public void likeReuslt(int resultCode, Bundle resultData); 
	}
	
	public void setReceiver(FavouriteInterface rec)
	{
		this.rec=rec;
	}

}
