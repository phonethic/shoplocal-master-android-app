package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

public class RegisterMerchentService  extends IntentService {

	ResultReceiver rec;
	public RegisterMerchentService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public RegisterMerchentService()
	{
		super("RegisterMerchantTag");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		rec=intent.getParcelableExtra("regMerchant");
		
		
		
		
		String contact_no = intent.getStringExtra("contact_no");
		String email_id = intent.getStringExtra("email_id");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		/*
		 * Test
		 */
		BufferedReader bufferedReader = null;
		 String status="";
		 
		//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient();
			//Setting URL to post data.
			/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/register/user");*/
			HttpPost httpPost=new HttpPost(URL);
			//Adding header.
			/*httpPost.addHeader("X-API-KEY", "Fool");*/
			httpPost.addHeader(API_HEADER, API_HEADER_VALUE);



			try
			{
				JSONObject json = new JSONObject();
				json.put("mobile", contact_no);
				json.put("email", email_id);

				Log.i("Response", "Response "+"\""+contact_no+"\"");
				Log.i("Response", "Response "+"\""+email_id+"\"");

				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);
				/*
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs.add(new BasicNameValuePair("mobile", contact_not));
					nameValuePairs.add(new BasicNameValuePair("email", email_id));


					UrlEncodedFormEntity urlen=new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);


					httpPost.setEntity(urlen);*/




				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", " Response "+stringBuffer.toString());
				status=stringBuffer.toString();

				/*Log.i("JSON", "Response id"+getId(status));*/

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			String user_id=getId(status);
			String status_user=getStatus(status);
		/*	Log.i("JSON", "Response Status"+user_id);*/
			Bundle b=new Bundle();
			b.putString("user_id", user_id);
			b.putString("status", status_user);
			rec.send(0, b);
		 
		
		/*new CreateMerchant(contact_no,email_id).execute("Register");*/


	}
	
	String getStatus(String status)
	{
/*			Toast.makeText(getApplicationContext(), "Called Status", Toast.LENGTH_SHORT).show();*/
/*		Log.i("JSON", "USERSTATUS");*/
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("status");
/*			Log.i("JSON", "USERSTATUS"+userstatus);*/
		/*	Toast.makeText(getApplicationContext(), " Status"+userstatus, Toast.LENGTH_SHORT).show();*/
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}

String getId(String status)
{
	String userid="";
	try {
		JSONObject jsonobject=new JSONObject(status);
		userid=jsonobject.getString("message");

	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	return userid;
}

	//Register Merchant on server.
	class CreateMerchant extends AsyncTask<String, Integer, String>
	{
		BufferedReader bufferedReader = null;
		private String status;
		String contact_not;
		String email_id;
		CreateMerchant(String contact_not,String email_id)
		{
			this.contact_not=contact_not;
			this.email_id=email_id;
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient();
			//Setting URL to post data.
			HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/register/user");
			//Adding header.
			httpPost.addHeader("X-API-KEY", "Fool");



			try
			{
				JSONObject json = new JSONObject();
				json.put("mobile", contact_not);
				json.put("email", email_id);

				Log.i("Response", "Response "+"\""+contact_not+"\"");
				Log.i("Response", "Response "+"\""+email_id+"\"");

				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);
				/*
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs.add(new BasicNameValuePair("mobile", contact_not));
					nameValuePairs.add(new BasicNameValuePair("email", email_id));


					UrlEncodedFormEntity urlen=new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);


					httpPost.setEntity(urlen);*/




				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", " Response "+stringBuffer.toString());
				status=stringBuffer.toString();

				/*Log.i("JSON", "Response id"+getId(status));*/

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			String user_id=getId(status);
			String status_user=getStatus(status);
			Log.i("JSON", "Response Status"+user_id);
			Bundle b=new Bundle();
			b.putString("user_id", user_id);
			b.putString("status", status_user);
			rec.send(0, b);

		}
		String getStatus(String status)
		{
/*			Toast.makeText(getApplicationContext(), "Called Status", Toast.LENGTH_SHORT).show();*/
			Log.i("JSON", "USERSTATUS");
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("status");
				Log.i("JSON", "USERSTATUS"+userstatus);
				Toast.makeText(getApplicationContext(), " Status"+userstatus, Toast.LENGTH_SHORT).show();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}
	
	String getId(String status)
	{
		String userid="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userid=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userid;
	}






}

}
