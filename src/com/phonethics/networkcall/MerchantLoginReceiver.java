package com.phonethics.networkcall;





import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MerchantLoginReceiver extends ResultReceiver{

	private LoginReceiver mReceiver;
	public MerchantLoginReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(mReceiver!=null)
		{
			mReceiver.onReceiveLoginResult(resultCode, resultData);
		}
	}
	
	public interface LoginReceiver
	{
		public void onReceiveLoginResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(LoginReceiver receiver)
	{
		mReceiver=receiver;
	}
	

}
