package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AddImageBroadCastReceiver extends ResultReceiver{

	AddImageBroadCastResult result;
	public AddImageBroadCastReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(result!=null)
		{
			result.onReceiveAddImageBroadCastResult(resultCode, resultData);
		}
	}
	public interface AddImageBroadCastResult 
	{
		public void onReceiveAddImageBroadCastResult(int resultCode, Bundle resultData);
	}
	public void setReceiver(AddImageBroadCastResult rec)
	{
		result=rec;
	}

}
