package com.phonethics.networkcall;

import com.phonethics.networkcall.AddCustomerProfileReceiver.CustomerProfileReceiver;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class DeleteGalleryImagesResultReceiver extends ResultReceiver{

	DeleteImageReceiver rec;
	
	public DeleteGalleryImagesResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onDeleteImage(resultCode, resultData);
		}
	}
	
	public interface DeleteImageReceiver
	{
		public void onDeleteImage(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(DeleteImageReceiver rec)
	{
		this.rec=rec;
	}

}
