package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetAllStores_Of_Perticular_User_Receiver extends ResultReceiver {

	GetAllStore all_store;
	public GetAllStores_Of_Perticular_User_Receiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(all_store!=null)
		{
			all_store.onReceiveUsersAllStore(resultCode, resultData);
		}
	}

	public interface GetAllStore
	{
		public void onReceiveUsersAllStore(int resultCode, Bundle resultData);
	}
	public void setReceiver(GetAllStore rec)
	{
		all_store=rec;
	}
}
