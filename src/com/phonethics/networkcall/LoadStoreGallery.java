package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class LoadStoreGallery extends IntentService{

	ResultReceiver loadGall;

	String SEARCH_STATUS,MESSAGE;

	ArrayList<String>ID=new ArrayList<String>();
	ArrayList<String>STORE_ID=new ArrayList<String>();
	ArrayList<String>TITLE=new ArrayList<String>();
	ArrayList<String>IMAGE_DATE=new ArrayList<String>();
	ArrayList<String>SOURCE=new ArrayList<String>();
	ArrayList<String>thumb_url=new ArrayList<String>();
	public LoadStoreGallery(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public LoadStoreGallery()
	{
		super("LoadGallery");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		loadGall=intent.getParcelableExtra("loadgall");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		/*
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");*/
		String store_id=intent.getStringExtra("store_id");

		BufferedReader bufferedReader = null;
		String status = "";

		URL+=store_id;

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =  30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		/*		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);*/
		/*		httpGet.addHeader("store_id",store_id);*/

		Log.i("RESPONSE", "RESPONSE Gallery URL "+URL);
		/*		Log.i("RESPONSE", "RESPONSE Gallery user_id "+user_id);
		Log.i("RESPONSE", "RESPONSE Gallery auth_id "+auth_id);*/
		Log.i("RESPONSE", "RESPONSE Gallery store_id "+store_id);


		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store Details Gallery : "+stringBuffer.toString());
			status=stringBuffer.toString();


			/*
			 * Parsing JSON DATA
			 */
			try
			{
				JSONObject jsonobject=new JSONObject(status);

				/*		STREET=jsonobject.getString("street");
				LANDMARK=jsonobject.getString("landmark");
				AREA=jsonobject.getString("area");
				CITY=jsonobject.getString("city");
				PINCODE=jsonobject.getString("pincode");
				SOTRE_NAME=jsonobject.getString("store_name");
				MOBILE_NO=jsonobject.getString("mob_no1");
				PHOTO=jsonobject.getString("photo");*/

				SEARCH_STATUS=jsonobject.getString("success");

				if(SEARCH_STATUS.equalsIgnoreCase("true"))
				{
					JSONArray getMsg=jsonobject.getJSONArray("data");
					//					MESSAGE=jsonobject.getString("message");

					for(int i=0;i<getMsg.length();i++)
					{
						JSONObject record=getMsg.getJSONObject(i);
						ID.add(record.getString("id"));
						STORE_ID.add(record.getString("place_id"));
						TITLE.add(record.getString("title"));	
						SOURCE.add(record.getString("image_url"));
						IMAGE_DATE.add(record.getString("image_date"));
						thumb_url.add(record.getString("thumb_url"));
					}


					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", SEARCH_STATUS);
					b.putString("MESSAGE", MESSAGE);
					b.putStringArrayList("ID", ID);
					b.putStringArrayList("STORE_ID", STORE_ID);
					b.putStringArrayList("TITLE", TITLE);
					b.putStringArrayList("SOURCE", SOURCE);
					b.putStringArrayList("IMAGE_DATE", IMAGE_DATE);
					b.putStringArrayList("THUMB_URL", thumb_url);
					loadGall.send(0, b);

				}
				else
				{

					try
					{
						MESSAGE=jsonobject.getString("message");
					}catch(Exception ex)
					{
						MESSAGE=getResources().getString(R.string.exceptionMessage);
						ex.printStackTrace();

					}

					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", "false");
					b.putString("MESSAGE", MESSAGE);
					b.putStringArrayList("ID", ID);
					b.putStringArrayList("STORE_ID", STORE_ID);
					b.putStringArrayList("TITLE", TITLE);
					b.putStringArrayList("SOURCE", SOURCE);
					b.putStringArrayList("IMAGE_DATE", IMAGE_DATE);
					b.putStringArrayList("THUMB_URL", thumb_url);
					loadGall.send(0, b);

				}

			}catch(JSONException js)
			{
				js.printStackTrace();

				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "error");
				b.putString("MESSAGE", getResources().getString(R.string.exception));
				loadGall.send(0, b);

			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("MESSAGE", getResources().getString(R.string.connectionTimedOut));
			loadGall.send(0, b);

		}catch(SocketTimeoutException s)
		{
			s.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("MESSAGE",  getResources().getString(R.string.connectionTimedOut));
			loadGall.send(0, b);

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("MESSAGE",  getResources().getString(R.string.exception));
			loadGall.send(0, b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("MESSAGE",  getResources().getString(R.string.exception));
			loadGall.send(0, b);
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("MESSAGE",  getResources().getString(R.string.exception));
			loadGall.send(0, b);
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.i("GALLERY ", "GALLERY SERVICE STOPED");
		super.onDestroy();
	}



}
