package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;



public class LogoutReceiver extends ResultReceiver {

	private Logout logout;
	public LogoutReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(logout!=null)
		{
			logout.onLogout(resultCode, resultData);
		}
	}

	public interface Logout
	{
		public void onLogout(int resultCode, Bundle resultData);
	}
	public void setReceiver(Logout rec)
	{
		logout=rec;
	}
}
