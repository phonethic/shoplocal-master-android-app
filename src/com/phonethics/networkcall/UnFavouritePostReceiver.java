package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class UnFavouritePostReceiver extends ResultReceiver{

	UnfavouritePostInterface rec;
	public UnFavouritePostReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.unFavouritePostResult(resultCode, resultData);
		}
	}

	public interface UnfavouritePostInterface
	{
		public void unFavouritePostResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(UnfavouritePostInterface rec)
	{
		this.rec=rec;
	}
	
}
