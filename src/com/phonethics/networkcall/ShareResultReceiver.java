package com.phonethics.networkcall;

import com.phonethics.networkcall.GetUserProfileResultReceiver.GetUserProfileInterface;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class ShareResultReceiver extends ResultReceiver{
	
	ShareResultInterface share;

	public ShareResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}
	
	public interface ShareResultInterface
	{
		

		public void onReceiveShareStatus(int resultCode, Bundle resultData);
	}	
	
	public void setReceiver(ShareResultInterface share)
	{
		this.share=share;
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(share!=null)
		{
			share.onReceiveShareStatus(resultCode, resultData);
			
		}
	}

}
