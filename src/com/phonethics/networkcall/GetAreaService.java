package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.shoplocal.DBUtil;
import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.RequestTags;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetAreaService extends IntentService {

	ResultReceiver getArea;

	BufferedReader bufferedReader = null;
	String status = "";

	String getAreaStatus;

	String latitude;
	String longitude;

	ArrayList<String> areaId = new ArrayList<String>();
	ArrayList<String> sublocality = new ArrayList<String>();
	ArrayList<String> pinCodeList = new ArrayList<String>();
	ArrayList<String> cityList = new ArrayList<String>();
	ArrayList<String> countryList = new ArrayList<String>();
	ArrayList<String> countryCodeList = new ArrayList<String>();
	ArrayList<String> latitudeList = new ArrayList<String>();
	ArrayList<String> longitudeList = new ArrayList<String>();
	ArrayList<String> iso_code = new ArrayList<String>();
	ArrayList<String> shop_place_id = new ArrayList<String>();
	ArrayList<String> published_status = new ArrayList<String>();


	ArrayList<String> state = new ArrayList<String>();
	ArrayList<String> locality = new ArrayList<String>();
	DBUtil dbutil;

	String KEY="DATE_TIME_STAMP";
	String AREA_DATE_TIME_STAMP="AREA_DATE_TIME_STAMP";
	SimpleDateFormat format;
	String ACTIVE_AREA_PREF="ACTIVE_AREA";
	String ACTIVE_AREA_KEY="AREA_ID";
	
	public GetAreaService() {
		super("GetAreaService");
		// TODO Auto-generated constructor stub


	}


	public GetAreaService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		getArea = intent.getParcelableExtra("getArea");
		
		dbutil=new DBUtil(getApplicationContext());
		format=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		try {


			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
			/*	httpGet.addHeader("user_id",user_id);
			httpGet.addHeader("auth_id",auth_id);*/

			Log.i("URL", "URL Date : "+URL);


			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Area: "+stringBuffer.toString());
			status=stringBuffer.toString();

			JSONObject jsonobject=new JSONObject(status);

			getAreaStatus=jsonobject.getString("success");
			if(getAreaStatus.equalsIgnoreCase("true"))
			{

				JSONObject data = jsonobject.getJSONObject("data");

				JSONArray areas = data.getJSONArray("areas");

				for(int i=0;i<areas.length();i++){


					JSONObject objects = areas.getJSONObject(i);

					areaId.add(objects.getString("id"));
					sublocality.add(objects.getString("sublocality"));
					pinCodeList.add(objects.getString("pincode"));
					cityList.add(objects.getString("city"));
					countryList.add(objects.getString("country"));
					countryCodeList.add(objects.getString("country_code"));

					state.add(objects.getString("state"));
					locality.add(objects.getString("locality"));
					published_status.add(objects.getString("published_status"));


					//JSONObject geoPoints = objects.getJSONObject("geolocation");

					//latitude =  geoPoints.getString("latitude");
					latitudeList.add(objects.getString("latitude"));

					//longitude = geoPoints.getString("longitude");
					longitudeList.add(objects.getString("longitude"));

					iso_code.add(objects.getString("iso_code"));
					shop_place_id.add(objects.getString("shoplocal_place_id"));


					Log.d("LAT","LAT" +  iso_code);
					Log.d("LON","LON" +  longitudeList);

				}
				
//				cv.put(area_id, id);
//				cv.put(area_name, name);
//				cv.put(area_latitude, latitude);
//				cv.put(area_longitude, longitude);
//				cv.put(area_pincode, pincode);
//				cv.put(area_city, city);
//				cv.put(area_country, country);
//				cv.put(area_country_code, countryCode);
//				cv.put(area_iso_code, isoCode);
//				cv.put(area_placeID, placeId);
//				cv.put(area_locality, locality);
//				cv.put(area_state, state);
//				cv.put(area_active, area_active);
//				cv.put(area_published_status, published_status);

				dbutil.deleteAreasTable();
				dbutil.open();
				for(int i=0;i<areaId.size();i++){

//					Log.d("ALLIDS","ALLIDS" + allLat.get(i));
					long chk = dbutil.createArea(Integer.parseInt(areaId.get(i)), sublocality.get(i), latitudeList.get(i), longitudeList.get(i), pinCodeList.get(i), cityList.get(i), countryList.get(i), countryCodeList.get(i), iso_code.get(i), shop_place_id.get(i),locality.get(i),state.get(i),0,Integer.parseInt(published_status.get(i)));
					//Toast.makeText(context, " " + chk, 0).show();
				}

				dbutil.close();
				
				//Putting Date time stamp
				SharedPreferences prefs=getSharedPreferences(AREA_DATE_TIME_STAMP,MODE_PRIVATE);
				Editor editor=prefs.edit();
				editor.putString(KEY,format.format(new Date()));
				editor.commit();
				
				//Setting Active Area if any.
				try
				{
					SharedPreferences activeAreaPrefs=getSharedPreferences(ACTIVE_AREA_PREF,MODE_PRIVATE);
					dbutil.updatePlaceStatus(Integer.parseInt(activeAreaPrefs.getString(ACTIVE_AREA_KEY, "-1")),1);
					Log.i("ACTIVE AREA PREF","ACTIVE AREA PREF "+activeAreaPrefs.getString(ACTIVE_AREA_KEY, "-1"));

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				Intent intent_code=new Intent();
				intent_code.setAction(RequestTags.TagAreas);
				getApplicationContext().sendBroadcast(intent_code);


			}
			else if(getAreaStatus.equalsIgnoreCase("false"))
			{

			}


			Bundle b=new Bundle();
			b.putString("area_status", getAreaStatus);
			b.putStringArrayList("allIds", areaId);
			b.putStringArrayList("allAreas", sublocality);
			b.putStringArrayList("allPinCodes", pinCodeList);
			b.putStringArrayList("allCities", cityList);
			b.putStringArrayList("allCountries", countryList);
			b.putStringArrayList("allCountryCodes", countryCodeList);
			b.putStringArrayList("allLatitudes", latitudeList);
			b.putStringArrayList("allLongitudes", longitudeList);
			b.putStringArrayList("allIso_code", iso_code);
			b.putStringArrayList("allShoplocal_place_id", shop_place_id);
			b.putStringArrayList("locality", locality);
			b.putStringArrayList("state", state);
			b.putStringArrayList("published_status", published_status);
			getArea.send(0, b);

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();

			b.putString("area_status", "error");
			b.putString("message", getResources().getString(R.string.connectionTimedOut));

			getArea.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("area_status", "error");
			b.putString("message", getResources().getString(R.string.connectionTimedOut));
			getArea.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception
			Bundle b=new Bundle();
			b.putString("area_status", "error");
			b.putString("message",getResources().getString(R.string.exception));
			getArea.send(0, b);
			e.printStackTrace();
		}

	}

}
