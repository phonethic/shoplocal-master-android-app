package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class OffersReceiver extends ResultReceiver{

	Offers offers;
	public OffersReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(offers!=null)
		{
			offers.onReceiverOffers(resultCode, resultData);
		}
	}
	
	public interface Offers
	{
		void onReceiverOffers(int resultCode, Bundle resultData);
	}
	public void setReceiver(Offers rec)
	{
		offers=rec;
	}

}
