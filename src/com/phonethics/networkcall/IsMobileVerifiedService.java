package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class IsMobileVerifiedService extends IntentService {

	ResultReceiver isMobileVerified;
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	public IsMobileVerifiedService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public IsMobileVerifiedService() {
		super("isMobileVerified");
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		isMobileVerified=intent.getParcelableExtra("isMobileVerified");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String mobile_no=intent.getStringExtra("mobile_no");

		nameValuePairs.add(new BasicNameValuePair("mobile", mobile_no));
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;

		BufferedReader bufferedReader = null;
		String status="";

		HttpParams httpParams = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);





		//Setting URL to post data.
		HttpGet httpGet=new HttpGet(URL);
		//Adding header.

		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);



		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			
			
			Log.i("Response : ", "ISMOBILEVERIFIED URL "+URL);
			
			Log.i("Response : ", "ISMOBILEVERIFIED Response "+stringBuffer.toString());

			status=stringBuffer.toString();

			String verify_status=getStatus(status);
			String verify_msg=getMessage(status);

			Bundle b=new Bundle();
			b.putString("verify_status", verify_status);
			b.putString("verify_msg", verify_msg);
			isMobileVerified.send(0, b);


		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();


			Bundle b=new Bundle();
			String verify_status=getStatus(status);


			b.putString("verify_status", verify_status);
			b.putString("verify_msg", getResources().getString(R.string.connectionTimedOut));
			isMobileVerified.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			String verify_status=getStatus(status);

			Bundle b=new Bundle();
			b.putString("verify_status", verify_status);
			b.putString("verify_msg", getResources().getString(R.string.connectionTimedOut));
			isMobileVerified.send(0, b);
		}catch(Exception ex)
		{
			ex.printStackTrace();

			String verify_status=getStatus(status);
			String verify_msg=getMessage(status);

			Bundle b=new Bundle();
			b.putString("verify_status", verify_status);
			b.putString("verify_msg", verify_msg);
			isMobileVerified.send(0, b);
		}
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String usersmsg="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			usersmsg=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usersmsg;
	}

}
