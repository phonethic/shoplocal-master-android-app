package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class FavouritePostResultReceiver extends ResultReceiver {

	FavouritePostInterface rec;
	public FavouritePostResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.postLikeReuslt(resultCode, resultData);
		}
		
	}
	
	public interface FavouritePostInterface
	{
		public void postLikeReuslt(int resultCode, Bundle resultData); 
	}
	
	public void setReceiver(FavouritePostInterface rec)
	{
		this.rec=rec;
	}

}
