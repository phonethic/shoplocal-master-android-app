package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetPerticularStoreDetailService extends IntentService{

	ResultReceiver getStore;
	String ID;
	String BUSINESS_ID;
	String STORE_NAME;	
	String STORE_PARENT_ID;
	String BUILDING;
	String STREET;
	String LANDMARK;
	String AREA;
	String AREA_ID;
	String PINCODE;
	String CITY;
	String STATE;
	String COUNTRY;
	String LATITUDE;
	String LONGITUDE;

	String TELNO1;
	String TELNO2;
	String TELNO3;

	String MOBILE_NO;
	String MOBNO2;
	String MOBNO3;

	String FAX1;
	String FAX2;
	String TOLLFREE1;	
	String TOLLFREE2;
	String PHOTO;
	String store_status;
	String DESCRIPTION;
	String EMAIL;
	String WEBSITE;
	String USER_LIKE="-1";
	String TOTAL_VIEWS="-1";
	String TOTAL_LIKES="-1";
	String TOTAL_SHARE="0";
	String VERIFIED="";
	String PlACE_STATUS="";
	String Fb;
	String Twitter;

	ArrayList<String> CATEGORY_ID=new ArrayList<String>();
	ArrayList<String>  PLACE_TIMING_STATUS=new ArrayList<String>();
	ArrayList<String>  DAY=new ArrayList<String>();
	ArrayList<String> TIME1_OPEN=new ArrayList<String>();
	ArrayList<String>  TIME1_CLOSE=new ArrayList<String>();

	String STATUS="";
	String MSG="";

	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();




	public GetPerticularStoreDetailService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetPerticularStoreDetailService()
	{
		super("GetPerticularStoreDetailServiceTag");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		getStore=intent.getParcelableExtra("getStore");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		String business_id=intent.getStringExtra("business_id");
		String place_id=intent.getStringExtra("store_id");
		boolean getUserLike=intent.getBooleanExtra("getLike", false);
		boolean isMerchant=intent.getBooleanExtra("isMerchant", false);

		nameValuePairs.add(new BasicNameValuePair("place_id", place_id));
		if(getUserLike)
		{
			if(user_id!=null && user_id.length()!=0)
			{
				nameValuePairs.add(new BasicNameValuePair("customer_id", user_id));
			}
		}

		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;

		BufferedReader bufferedReader = null;
		String status = "";

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);
		/*		httpGet.addHeader("business_id",business_id);*/
		/*		httpGet.addHeader("store_id",store_id);*/

		Log.i("Response: ", "Response Store Details URL "+URL);
		Log.i("Response: ", "Response Store Details USER_ID "+user_id);
		Log.i("Response: ", "Response Store Details auth_id "+auth_id);
		Log.i("Response: ", "Response Store Details store_id "+place_id);

		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store Details: "+stringBuffer.toString());
			status=stringBuffer.toString();

			/*
			 * Parsing JSON DATA
			 */
			try
			{
				JSONObject jsonobject=new JSONObject(status);


				STATUS=jsonobject.getString("success");
				store_status=jsonobject.getString("success");
				if(STATUS.equalsIgnoreCase("true"))
				{
					JSONArray getMessageArr=jsonobject.getJSONArray("data");

					/*JSONObject getPlace=getMessage.getJSONObject("place");*/


					for(int i=0;i<getMessageArr.length();i++)
					{
						JSONObject getMessage=getMessageArr.getJSONObject(i);
						JSONArray getCategoryId=getMessage.getJSONArray("categories");
						JSONArray getTime=getMessage.getJSONArray("time");

						/*store_status=jsonobject.getString("status");*/
						ID=getMessage.getString("id");

						STORE_NAME=getMessage.getString("name");
						STORE_PARENT_ID=getMessage.getString("place_parent");
						BUILDING=getMessage.getString("building");
						STREET=getMessage.getString("street");
						LANDMARK=getMessage.getString("landmark");
						AREA_ID=getMessage.getString("area_id");
						AREA=getMessage.getString("area");
						PINCODE=getMessage.getString("pincode");
						CITY=getMessage.getString("city");
						STATE=getMessage.getString("state");
						COUNTRY=getMessage.getString("country");
						LATITUDE=getMessage.getString("latitude");
						LONGITUDE=getMessage.getString("longitude");

						TELNO1=getMessage.getString("tel_no1");
						TELNO2=getMessage.getString("tel_no2");
						TELNO3=getMessage.getString("tel_no3");

						MOBILE_NO=getMessage.getString("mob_no1");
						MOBNO2=getMessage.getString("mob_no2");
						MOBNO3=getMessage.getString("mob_no3");
						/*FAX1=getMessage.getString("fax_no1");
						FAX2=getMessage.getString("fax_no2");
						TOLLFREE1=getMessage.getString("toll_free_no1");	
						TOLLFREE2=getMessage.getString("toll_free_no2");*/
						PHOTO=getMessage.getString("image_url");
						DESCRIPTION=getMessage.getString("description");
						EMAIL=getMessage.getString("email");
						WEBSITE=getMessage.getString("website");
						Fb=getMessage.getString("facebook_url");
						Twitter=getMessage.getString("twitter_url");
						if(isMerchant)
						{
							TOTAL_VIEWS=getMessage.getString("total_view");
						}
						TOTAL_LIKES=getMessage.getString("total_like");
						TOTAL_SHARE=getMessage.getString("total_share");
						VERIFIED=getMessage.getString("verified");

						if(getUserLike)
						{
							USER_LIKE=getMessage.getString("user_like");
						}
						if(isMerchant)
						{
							PlACE_STATUS=getMessage.getString("place_status");
						}

						//Fetching Categories.
						for(int index=0;index<getCategoryId.length();index++)
						{
							CATEGORY_ID.add(getCategoryId.getString(index));
						}
						for(int index=0;index<getTime.length();index++)
						{
							JSONObject getData=getTime.getJSONObject(index);
							Log.i("DAY ", "DAY L "+getData.getString("day"));
							DAY.add(getData.getString("day"));
							PLACE_TIMING_STATUS.add(getData.getString("is_closed"));
							TIME1_OPEN.add(getData.getString("open_time"));
							TIME1_CLOSE.add(getData.getString("close_time"));
						}



					}

					Bundle b=new Bundle();
					b.putString("store_status",store_status);
					b.putString("ID", ID);
					/*			b.putString("BUSINESS_ID", BUSINESS_ID);*/
					b.putString("SOTRE_NAME", STORE_NAME);
					b.putString("STORE_PARENT_ID", STORE_PARENT_ID);
					b.putString("BUILDING", BUILDING);
					b.putString("STREET", STREET);
					b.putString("LANDMARK", LANDMARK);
					b.putString("AREA_ID", AREA_ID);
					b.putString("AREA", AREA);
					b.putString("PINCODE", PINCODE);
					b.putString("CITY", CITY);
					b.putString("STATE", STATE);
					b.putString("COUNTRY", COUNTRY);
					b.putString("LATITUDE", LATITUDE);
					b.putString("LONGITUDE", LONGITUDE);

					b.putString("TELNO1", TELNO1);
					b.putString("TELNO2", TELNO2);
					b.putString("TELNO3", TELNO3);

					b.putString("MOBILE_NO", MOBILE_NO);
					b.putString("MOBNO2", MOBNO2);
					b.putString("MOBNO3", MOBNO3);

					b.putString("FAX1", FAX1);
					b.putString("FAX2", FAX2);
					b.putString("TOLLFREE1", TOLLFREE1);
					b.putString("TOLLFREE2", TOLLFREE2);
					b.putString("PHOTO", PHOTO);
					b.putString("DESCRIPTION", DESCRIPTION);
					b.putString("EMAIL", EMAIL);
					b.putString("WEBSITE", WEBSITE);
					b.putString("FACEBOOK",Fb);
					b.putString("TWITTER",Twitter);
					b.putString("TOTAL_LIKES", TOTAL_LIKES);
					b.putString("TOTAL_VIEWS", TOTAL_VIEWS);
					b.putString("TOTAL_SHARE", TOTAL_SHARE);
	

					if(getUserLike)
					{
						b.putString("USER_LIKE", USER_LIKE);
					}
					b.putStringArrayList("CATEGORY_ID", CATEGORY_ID);
					b.putStringArrayList("DAY", DAY);
					b.putStringArrayList("PLACE_STATUS", PLACE_TIMING_STATUS);
					b.putStringArrayList("TIME1_OPEN", TIME1_OPEN);
					b.putStringArrayList("TIME1_CLOSE", TIME1_CLOSE);
					b.putString("STATUS",PlACE_STATUS);


					getStore.send(0, b);

					for(int i=0;i<DAY.size();i++)
					{
						Log.i("DAY", "DAY S  : "+DAY.get(i));
					}


				}else if(STATUS.equalsIgnoreCase("false"))
				{
					MSG=jsonobject.getString("message");

					Bundle b=new Bundle();
					b.putString("store_status","false");
					b.putString("MSG",MSG);
					b.putString("error_code", getErrorCode(status));
					getStore.send(0, b);
				}




			}
			catch(JSONException js)
			{
				js.printStackTrace();
				Bundle b=new Bundle();
				b.putString("store_status","error");
				b.putString("error_code", "-1");
				b.putString("MSG",getResources().getString(R.string.exception));
				getStore.send(0, b);
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Bundle b=new Bundle();
				b.putString("store_status","error");
				b.putString("error_code", "-1");
				b.putString("MSG",getResources().getString(R.string.exception));
				getStore.send(0, b);
			}


		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Bundle b=new Bundle();
			b.putString("store_status","error");
			b.putString("MSG",getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			getStore.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			Bundle b=new Bundle();
			b.putString("store_status","error");
			b.putString("MSG",getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			getStore.send(0, b);
		}

		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("store_status","error");
			b.putString("MSG",getResources().getString(R.string.exceptionMessage));
			b.putString("error_code", "-1");
			getStore.send(0, b);
		}




	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}

}
