package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MerchantResultReceiver extends ResultReceiver {

	MerchantRegisterInterface merchant;
	public MerchantResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(merchant!=null)
		{
			merchant.onReceiverMerchantRegister(resultCode, resultData);
		}
	}
	
	//Interface to implement.
	public interface MerchantRegisterInterface
	{
		public void onReceiverMerchantRegister(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(MerchantRegisterInterface merchant)
	{
		this.merchant=merchant;
	}

}
