package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetUserProfileResultReceiver extends ResultReceiver {

	GetUserProfileInterface rec;
	public GetUserProfileResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onReceiveProfileStatus(resultCode, resultData);
			
		}
	}

	public interface GetUserProfileInterface
	{
		public void onReceiveProfileStatus(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(GetUserProfileInterface rec)
	{
		this.rec=rec;
	}
}
