package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetUsersFavPlaceReceiver extends ResultReceiver {

	ReceiveUserLikedPlaces rec;
	public GetUsersFavPlaceReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onReceiveUserLikedPlaces(resultCode, resultData);
		}
	}
	
	public interface ReceiveUserLikedPlaces
	{
		public void onReceiveUserLikedPlaces(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(ReceiveUserLikedPlaces rec)
	{
		this.rec=rec;
	}

}
