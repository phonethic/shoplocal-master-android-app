package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class UnFavouritePlaceService extends IntentService {

	ResultReceiver unfav;

	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	public UnFavouritePlaceService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public UnFavouritePlaceService() {
		super("unFavourite");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		unfav=intent.getParcelableExtra("unfavourite");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String place_id=intent.getStringExtra("place_id");
		String auth_id=intent.getStringExtra("auth_id");
		String user_id=intent.getStringExtra("user_id");

		nameValuePairs.add(new BasicNameValuePair("place_id", place_id));

		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;

		BufferedReader bufferedReader = null;
		String status="";

		HttpParams httpParams = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to post data.
		HttpDelete httpDelete=new HttpDelete(URL);
		//Adding header.

		httpDelete.addHeader(API_HEADER, API_HEADER_VALUE);
		httpDelete.addHeader("user_id", user_id);
		httpDelete.addHeader("auth_id", auth_id);

		try
		{

			Log.i("Response", "Response UNLIKE "+""+URL+"");
			Log.i("Response", "Response UNLIKE  "+""+user_id+"");
			Log.i("Response", "Response UNLIKE  "+""+auth_id+"");
			Log.i("Response", "Response UNLIKE  "+""+place_id+"");



			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpDelete);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response UNLIKE   "+stringBuffer.toString());
			status=stringBuffer.toString();


			String unlike_status=getStatus(status);
			String unlike_msg=getMessage(status);

			if(unlike_status.equalsIgnoreCase("true"))
			{
				Bundle b=new Bundle();
				b.putString("unlikestatus", unlike_status);
				b.putString("unlikemsg", unlike_msg);
				unfav.send(0, b);
			}
			else if(unlike_status.equalsIgnoreCase("false"))
			{
				Bundle b=new Bundle();
				b.putString("unlikestatus", unlike_status);
				b.putString("unlikemsg", unlike_msg);
				b.putString("error_code", getErrorCode(status));
				unfav.send(0, b);
			}

		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();

			String unlike_status=getStatus(status);
			Bundle b=new Bundle();
			b.putString("unlikestatus", "error");
			b.putString("unlikemsg", getResources().getString(R.string.connectionTimedOut));
			unfav.send(0, b);
		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			String unlike_status=getStatus(status);


			Bundle b=new Bundle();
			b.putString("unlikestatus", "error");
			b.putString("unlikemsg", getResources().getString(R.string.connectionTimedOut));
			unfav.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			String unlike_status=getStatus(status);
			String unlike_msg=getMessage(status);

			Bundle b=new Bundle();
			b.putString("unlikestatus", "error");
			b.putString("unlikemsg", getResources().getString(R.string.exception));
			unfav.send(0, b);
		}

	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String usersmsg="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			usersmsg=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usersmsg;
	}

}
