package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class SearchResultReceiver extends ResultReceiver{

	SearchResult search ;
	public SearchResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(search!=null)
		{
			search.onReciverSearchResult(resultCode, resultData);
		}
	}
	
	public interface SearchResult
	{
		public void onReciverSearchResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(SearchResult rec)
	{
		this.search=rec;
	}

}
