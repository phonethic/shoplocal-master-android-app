package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetMobileVerifcationCodeReceiver extends ResultReceiver {

	GetMobileVerification rec;
	public GetMobileVerifcationCodeReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.getVerficationResult(resultCode, resultData);
		}
	}

	public interface GetMobileVerification
	{
		public void getVerficationResult(int resultCode, Bundle resultData);
	}
	public void setReceiver(GetMobileVerification rec)
	{
		this.rec=rec;
	}
}
