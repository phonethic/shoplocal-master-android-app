package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class MerchantPasswordService  extends IntentService  {

	ResultReceiver passresult;
	public MerchantPasswordService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	//Empty Constructor needed.
	public MerchantPasswordService()
	{
		super("SetMerchantPassword");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		passresult=intent.getParcelableExtra("setPassword");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String verification_code=intent.getStringExtra("verification_code");
		String password=intent.getStringExtra("password");
		String confirm_password=intent.getStringExtra("confirm_password");
		String user_id=intent.getStringExtra("user_id");
		String mobile_no=intent.getStringExtra("mobile_no");
		boolean isForgotPassword=intent.getBooleanExtra("isForgotPassword", false);

		
		BufferedReader bufferedReader = null;
		String status="";

		HttpParams httpParams = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to post data.
		/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/register/verification/format/json");*/
		HttpPost httpPost=new HttpPost(URL);
		//Adding header.
		/*httpPost.addHeader("X-API-KEY", "Fool");*/
		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);
		httpPost.addHeader("X-HTTP-Method-Override", "PUT");

		try
		{
			JSONObject json = new JSONObject();
			/*				json.put("verification_code", verification_code);*/

			/*				json.put("confirm_password", confirm_password);*/
			/*				json.put("user_id", user_id);*/
			json.put("mobile", mobile_no);
			json.put("password", password);
			if(isForgotPassword==true)
			{
				json.put("verification_code", verification_code);
			}

			Log.i("Response", "SETPASSWORD Response URL "+""+URL+"");

			Log.i("Response", "SETPASSWORD Response PASSWORD "+""+password+"");

			Log.i("Response", "SETPASSWORD Response MOBILE NO "+""+mobile_no+"");
			Log.i("Response", "SETPASSWORD Response verification"+""+verification_code+"");

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);

			/*				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("mobile", contact_no));
				nameValuePairs.add(new BasicNameValuePair("email", contact_no));


				UrlEncodedFormEntity urlen=new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);


				httpPost.setEntity(urlen);*/




			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", "SETPASSWORD Response "+stringBuffer.toString());
			status=stringBuffer.toString();

			
			
			String password_status=getStatus(status);
			String password_msg=getMessage(status);

			Bundle b=new Bundle();
			b.putString("passstatus", password_status);
			b.putString("passmsg", password_msg);
			b.putString("error_code", getErrorCode(status));
			passresult.send(0, b);

		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();
			
			String password_status=getStatus(status);
			String password_msg=getMessage(status);
			
			Bundle b=new Bundle();
			b.putString("passstatus", password_status);
			b.putString("passmsg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			passresult.send(0, b);
		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			
			String password_status=getStatus(status);
			String password_msg=getMessage(status);
			
			Bundle b=new Bundle();
			b.putString("passstatus", password_status);
			b.putString("passmsg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			passresult.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("passstatus", "false");
			b.putString("passmsg", "Please try again");
			b.putString("error_code", "-1");
			passresult.send(0, b);
		}






		/*new SetMerchantPassword(verification_code, password, confirm_password, user_id, mobile_no).execute("Set Password");*/

	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String usersmsg="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			usersmsg=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usersmsg;
	}





}
