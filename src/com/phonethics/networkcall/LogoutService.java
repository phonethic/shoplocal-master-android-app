package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class LogoutService extends IntentService{

	ResultReceiver logout;
	public LogoutService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public LogoutService()
	{
		super("Logout");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		logout=intent.getParcelableExtra("logout");
		
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		BufferedReader bufferedReader=null;
		String status="";

		
		//Creating HttpClient.
				HttpClient httpClient=new DefaultHttpClient();

				//Setting URL to Get data.
				HttpGet httpGet=new HttpGet(URL);

				//Adding header.
				httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
				httpGet.addHeader("auth_id", auth_id);
				httpGet.addHeader("user_id",user_id);

				try
				{
					HttpResponse response=httpClient.execute(httpGet);

					bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					StringBuffer stringBuffer=new StringBuffer("");
					String line="";
					String LineSeparator=System.getProperty("line.separator");

					while((line=bufferedReader.readLine())!=null)
					{
						stringBuffer.append(line+LineSeparator);
					}
					bufferedReader.close();
					Log.i("Response : ", "Service Response "+stringBuffer.toString());
					status=stringBuffer.toString();

				}
				catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				
				
				String logout_status=getStatus(status);
				Bundle b=new Bundle();
				b.putString("logout_status", logout_status);
		
				Log.i("logout_status", "logout_status"+logout_status);
				logout.send(0, b);

	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("status");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}
}
