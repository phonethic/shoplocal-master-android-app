package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class BusinessOperationService extends IntentService {

	ResultReceiver business_operation;
	public BusinessOperationService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public BusinessOperationService()
	{
		super("BusinessOperation");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		business_operation=intent.getParcelableExtra("business_operation");
		
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		
		String userfile=intent.getStringExtra("userfile");
		String filename=intent.getStringExtra("filename");
		String filetype=intent.getStringExtra("filetype");
		String displaytime=intent.getStringExtra("displaytime");
		String starttime=intent.getStringExtra("starttime");
		String closetime=intent.getStringExtra("closetime");
		String store_id=intent.getStringExtra("store_id");
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
/*		String business_id=intent.getStringExtra("business_id");*/

		boolean isPut=intent.getBooleanExtra("isput", false);
		boolean isSunOpen=intent.getBooleanExtra("isSunOpen",false);
		boolean isMonOpen=intent.getBooleanExtra("isMonOpen",false);
		boolean isTueOpen=intent.getBooleanExtra("isTueOpen",false);
		boolean isWedOpen=intent.getBooleanExtra("isWedOpen",false);
		boolean isThuOpen=intent.getBooleanExtra("isThuOpen",false);
		boolean isFriOpen=intent.getBooleanExtra("isFriOpen",false);
		boolean isSatOpen=intent.getBooleanExtra("isSatOpen",false);
		
		Log.i("Response","Response Operation URL"+URL);
		Log.i("Response","Response Operation isput"+isPut);
		Log.i("Response","Response Operation store_id"+store_id);
		Log.i("Response","Response Operation isSunOpen"+isSunOpen);
		Log.i("Response","Response Operation isMonOpen"+isMonOpen);
		Log.i("Response","Response Operation isTueOpen"+isTueOpen);
		Log.i("Response","Response Operation isWedOpen"+isWedOpen);
		Log.i("Response","Response Operation isThuOpen"+isThuOpen);
		Log.i("Response","Response Operation isFriOpen"+isFriOpen);
		Log.i("Response","Response Operation isSatOpen"+isSatOpen);
		Log.i("Response","Response Operation StartTime"+starttime);
		Log.i("Response","Response Operation closetime"+closetime);
		
		


		BufferedReader bufferedReader = null;
		String status = "";
		
		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =  30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);



		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);
		//Setting URL to post data.
		/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store_operation");*/
		HttpPost httpPost=new HttpPost(URL);
		//Adding header.
		/*httpPost.addHeader("X-API-KEY", "Fool");*/
		
		httpPost.addHeader("X-HTTP-Method-Override", "PUT");
		
		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);


		try
		{
			JSONObject json = new JSONObject();
			json.put("place_id",store_id);
			
			/*json.put("userfile",userfile);
			json.put("filename",filename);
			json.put("filetype",filetype);
			json.put("display_time",displaytime);*/

			if(isMonOpen==true)
			{
			
			
				json.put("mon_time1_status","0");
			}
			else
			{ 

				json.put("mon_time1_status","1");
			}

			if(isTueOpen==true)
			{
				
				json.put("tue_time1_status","0");
			}
			else
			{
		
				json.put("tue_time1_status","1");
			}

			if(isWedOpen==true)
			{
				
				json.put("wed_time1_status","0");
			}
			else
			{

				json.put("wed_time1_status","1");
			}

			if(isThuOpen==true)
			{

				json.put("thu_time1_status","0");
			}
			else
			{
	
				json.put("thu_time1_status","1");
			}

			if(isFriOpen==true)
			{

				json.put("fri_time1_status","0");
			}else
			{
	
				json.put("fri_time1_status","1");
			}
			if(isSatOpen==true)
			{
				
				json.put("sat_time1_status","0");
			}
			else
			{


				json.put("sat_time1_status","1");
			}

			if(isSunOpen==true)
			{
	
				json.put("sun_time1_status","0");
			}			
			else
			{

				json.put("sun_time1_status","1");
			}
			
			if(starttime==null)
			{
				starttime="";
			}
			if(closetime==null)
			{
				closetime="";
			}

			json.put("monday","monday");
			
			json.put("mon_time1_open",starttime);
			json.put("mon_time1_close",closetime);
			
			json.put("tuesday","tuesday");
			json.put("tue_time1_open",starttime);
			json.put("tue_time1_close",closetime);
			
			json.put("wednesday","wednesday");
			json.put("wed_time1_open",starttime);
			json.put("wed_time1_close",closetime);
			
			json.put("thursday","thursday");
			json.put("thu_time1_open",starttime);
			json.put("thu_time1_close",closetime);
			
			json.put("friday","friday");
			json.put("fri_time1_open",starttime);
			json.put("fri_time1_close",closetime);
			
			json.put("saturday","saturday");
			json.put("sat_time1_open",starttime);
			json.put("sat_time1_close",closetime);
			
			json.put("sunday","sunday");
			json.put("sun_time1_open",starttime);
			json.put("sun_time1_close",closetime);
			

			json.put("user_id", user_id);
			json.put("auth_id", auth_id);
/*			json.put("business_id", business_id);*/


			/*	Log.i("Response", "Response business_name"+""+business_name+"");
			Log.i("Response", "Response description "+""+description+"");
			Log.i("Response", "Response year_of_establish"+""+year_of_establish+"");
			Log.i("Response", "Response website"+""+website+"");
			Log.i("Response", "Response userfile"+""+userfile+"");
			Log.i("Response", "Response filename"+""+filename+"");

			Log.i("Response", "Response filetype"+""+filetype+"");
			Log.i("Response", "Response user_id"+""+user_id+"");

			Log.i("Response", "Response auth_id"+""+auth_id+"");
			Log.i("Response", "Response business_id"+""+business_id+"");*/


			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);

			Log.i("Response", "Response Operation Json "+json.toString());
			/*List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("mobile", contact_no));
			nameValuePairs.add(new BasicNameValuePair("email", contact_no));


			UrlEncodedFormEntity urlen=new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);


			httpPost.setEntity(urlen);*/




			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response operation "+stringBuffer.toString());
		
			status=stringBuffer.toString();

			writeToFile(json.toString());

			String store_operation;
			String store_msg;

			store_operation=getStatus(status);

			
			if(store_operation.equalsIgnoreCase("true"))
			{
			Bundle b=new Bundle();
			b.putString("store_operation", store_operation);
			business_operation.send(0, b);
			}
			else
			{
				store_msg=getMessage(status);
				Bundle b=new Bundle();
				b.putString("store_operation", store_operation);
				b.putString("store_msg", store_msg);
				b.putString("error_code", getErrorCode(status));
				b.putString("error_code", "-1");
				business_operation.send(0, b);
			}

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			
			Bundle b=new Bundle();
			b.putString("store_operation","error");
			b.putString("store_msg",  getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			business_operation.send(0, b);
		}
		catch(SocketTimeoutException s)
		{
			s.printStackTrace();
			Bundle b=new Bundle();
			b.putString("store_operation","error");
			b.putString("store_msg",  getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			business_operation.send(0, b);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("store_operation","error");
			b.putString("store_msg",  "Something went wrong");
			b.putString("error_code", "-1");
			business_operation.send(0, b);
		}



	}
	
	private void writeToFile(String data) {
	    /*try {
	    	FileOutputStream foutput=new FileOutputStream(file)

	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("json.txt", Context.MODE_WORLD_WRITEABLE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } */
		try {
            File myFile = new File("/sdcard/jsonoperation.txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.close();
            
      
        } 
        catch (Exception e) 
        {
        	 Log.e("Exception", "File write failed: " + e.toString());
        }
	}
	
	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}
	String getMessage(String status)
	{
		String msg="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			msg=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return msg;
	}
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}
	
	String getData(String status)
	{
		String data="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			JSONObject getDataObject=jsonobject.getJSONObject("data");
			data=getDataObject.getString("place_id");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return data;
	}

}
