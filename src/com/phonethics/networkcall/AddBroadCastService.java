package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class AddBroadCastService extends IntentService{

	ResultReceiver addBroadCast;
	public AddBroadCastService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public AddBroadCastService()
	{
		super("AddBroadCast");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		addBroadCast=intent.getParcelableExtra("addBroadCast");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		boolean isOffer=intent.getBooleanExtra("isOffer",false);
		boolean isImageBroadCast=intent.getBooleanExtra("isImageBroadCast", false);
		String USER_ID=intent.getStringExtra("user_id");
		String AUTH_ID=intent.getStringExtra("auth_id");
		ArrayList<String> STORE_ID=intent.getStringArrayListExtra("store_id");
		String TITLE=intent.getStringExtra("title");
		String BODY=intent.getStringExtra("body");
		String IMAGE_URL=intent.getStringExtra("url");
		String TAGS=intent.getStringExtra("tags");
		String TYPE=intent.getStringExtra("type");
		String STATE=intent.getStringExtra("state");
		String PUBLISHED=intent.getStringExtra("published");
		String FORMAT=intent.getStringExtra("format");
		String IS_OFFERED=intent.getStringExtra("is_offered");
		String OFFER_DATE=intent.getStringExtra("offer_date_time");
		String OFFER_TIME=intent.getStringExtra("offer_time");

		String FILE_NAME=intent.getStringExtra("filename");
		String FILE_TYPE=intent.getStringExtra("filetype");
		String USER_FILE=intent.getStringExtra("userfile");
		
		String image_title=intent.getStringExtra("image_title");

		BufferedReader bufferedReader = null;
		String status = "";

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);
		//Setting URL to post data.
		/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/business_api/business");*/

		HttpPost httpPost=new HttpPost(URL);

		//Adding header.
		/*httpPost.addHeader("X-API-KEY", "Fool");*/
		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);




		try
		{
			Log.i("Response ", "Response URL "+URL);
			Log.i("Response ", "Response USER_ID "+USER_ID);
			Log.i("Response ", "Response AUTH_ID "+AUTH_ID);
			Log.i("Response ", "Response STORE_ID "+STORE_ID);
			Log.i("Response ", "Response TITLE "+TITLE);
			Log.i("Response ", "Response BODY "+BODY);
			Log.i("Response ", "Response TAGS "+TAGS);
			Log.i("Response ", "Response TYPE "+TYPE);
			Log.i("Response ", "Response STATE "+STATE);
			Log.i("Response ", "Response IS_OFFERED "+IS_OFFERED);
			Log.i("Response ", "Response OFFER_DATE "+OFFER_DATE);
			Log.i("Response ", "Response OFFER_TIME "+OFFER_TIME);



			/*JSONObject jsonPost = new JSONObject();*/

			JSONObject json = new JSONObject();

			//Array of Place Id
			JSONArray jsonArray=new JSONArray(STORE_ID);


			//place id array
			json.put("place_id", jsonArray);

			json.put("title", TITLE);

			json.put("description", BODY);
			json.put("url", IMAGE_URL);
			json.put("tags", TAGS);
			json.put("type", TYPE);
			json.put("state", STATE);

			json.put("format", FORMAT);
			if(IS_OFFERED.length()==0)
			{
				json.put("is_offered", "0");
			}
			else
			{
				json.put("is_offered","1");
			}
			json.put("offer_date_time", OFFER_DATE+OFFER_TIME);
			/*			json.put("offer_time", OFFER_TIME);*/
			/*json.put("offer_time", OFFER_TIME);*/


			json.put("user_id", USER_ID);
			json.put("auth_id", AUTH_ID	);

			//image object array
			if(isImageBroadCast)
			{
				//File Array
				JSONArray fileArray=new JSONArray();
				JSONObject fileobject = new JSONObject();

			//	fileobject.put("filename", FILE_NAME);
				fileobject.put("filetype", FILE_TYPE);
				fileobject.put("userfile", USER_FILE);
				fileobject.put("title", image_title);

				fileArray.put(fileobject);

				json.put("file",fileArray);
			}


			/*jsonPost.put("post", json);*/

			Log.i("JSON", json.toString());

			writeToFile(json.toString());

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response "+stringBuffer.toString());

			status=stringBuffer.toString();

			try
			{
				String broadcast_status;
				broadcast_status=getStatus(status);

				if(broadcast_status.equalsIgnoreCase("true"))
				{
					String broadcast_msg=getMessage(status);
					Bundle b=new Bundle();
					b.putString("broadcast_status", broadcast_status);
					b.putString("broadcast_msg", broadcast_msg);
					addBroadCast.send(0, b);
				}
				else if(broadcast_status.equalsIgnoreCase("false"))
				{
					String broadcast_msg=getMessage(status);
					Bundle b=new Bundle();
					b.putString("broadcast_status", "false");
					b.putString("broadcast_msg", broadcast_msg);
					b.putString("error_code", getErrorCode(status));
					addBroadCast.send(0, b);
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				ex.printStackTrace();
				Bundle b=new Bundle();
				b.putString("broadcast_status", "error");
				b.putString("broadcast_msg", "Something went wrong");
				b.putString("error_code", "-1");
				addBroadCast.send(0, b);
			}

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "error");
			b.putString("broadcast_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			addBroadCast.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "error");
			b.putString("broadcast_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			addBroadCast.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "error");
			b.putString("broadcast_msg", "Something went wrong");
			b.putString("error_code", "-1");
			addBroadCast.send(0, b);
		}



	}

	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}


	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "false");
			b.putString("broadcast_msg","Something went wrong");
			addBroadCast.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "false");
			b.putString("broadcast_msg", "Something went wrong");
			addBroadCast.send(0, b);
		}
		return userstatus;
	}
	String getMessage(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("message");

		} catch (JSONException e) {
			// JSON Exception handled
			e.printStackTrace();
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "false");
			b.putString("broadcast_msg","Something went wrong");
			addBroadCast.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("broadcast_status", "false");
			b.putString("broadcast_msg", "Something went wrong");
			addBroadCast.send(0, b);
		}
		return userstatus;
	}


	private void writeToFile(String data) {
		try {
			File myFile = new File("/sdcard/create.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

}
