package com.phonethics.networkcall;

import com.phonethics.networkcall.GetSpecialDatesResultReceiver.GetSpecialDate;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetInterestedAreaResultReciever extends ResultReceiver {
	
	GetInterestedAreas result;

	public GetInterestedAreaResultReciever(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		
		if(result!=null){
			
			result.onGetInterestedAreas(resultCode, resultData);
		}
	}
	
	public interface GetInterestedAreas{
		
		public void onGetInterestedAreas(int resultCode, Bundle resultData);
	}
	
	public void setReciver(GetInterestedAreas result)
	{
		this.result=result;
	}
}
