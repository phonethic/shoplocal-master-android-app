package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class CheckPasswordReceiver extends ResultReceiver{

	CheckPass checkPass;
	public CheckPasswordReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(checkPass!=null)
		{
			checkPass.onCheckPassReceive(resultCode, resultData);
		}
	}
	
	public interface CheckPass
	{
		public void onCheckPassReceive(int resultCode, Bundle resultData);
	}
	
	public void setReciver(CheckPass res)
	{
		checkPass=res;
	}

}
