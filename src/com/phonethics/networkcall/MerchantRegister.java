package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class MerchantRegister extends IntentService {

	ResultReceiver merchant;
	public MerchantRegister(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public MerchantRegister() {
		super("merchantRegister");
		// TODO Auto-generated constructor stub
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		merchant=intent.getParcelableExtra("merchantRegister");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String mobile_no=intent.getStringExtra("mobile_no");
		boolean isMerchant=intent.getBooleanExtra("isMerchant", false);
		String merchant_name=intent.getStringExtra("merchant_name");

		BufferedReader bufferedReader = null;
		String status="";

		HttpParams httpParams = new BasicHttpParams();
		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to post data.
		HttpPost httpPost=new HttpPost(URL);
		//Adding header.

		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);

		try
		{
			PackageInfo pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
		
//			Log.i("pinfoName","pinfoCode name"+pinfo.versionName+" MODEL "+android.os.Build.MODEL);
			
			
			JSONObject json = new JSONObject();
			/*				json.put("verification_code", verification_code);*/

			/*				json.put("confirm_password", confirm_password);*/
			/*				json.put("user_id", user_id);*/
			json.put("mobile", mobile_no);
			if(isMerchant)
			{
				Log.i("pinfoCode","pinfoCode"+pinfo.versionCode+" "+merchant_name);
				json.put("name", merchant_name);
			}
			try
			{
			json.put("register_from", "android v"+pinfo.versionName+"("+pinfo.versionCode+") "+android.os.Build.MODEL+" "+android.os.Build.VERSION.SDK_INT);
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			Log.i("Response", "Response "+""+URL+"");
			Log.i("Response", "Response "+""+mobile_no+"");

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response "+stringBuffer.toString());
			status=stringBuffer.toString();



			String merchant_status=getStatus(status);
			String merchant_msg=getMessage(status);
			String action_call="";
			
			if(merchant_status.equalsIgnoreCase("true"))
			{
//				action_call=getNumber(status);
			}
			else
			{
				
			}

			Bundle b=new Bundle();
			b.putString("passstatus", merchant_status);
			b.putString("passmsg", merchant_msg);
			b.putString("action_call", action_call);
			b.putString("error_code", getErrorCode(status));
			
			merchant.send(0, b);

		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();

			String merchant_status="false";
			String merchant_msg=getResources().getString(R.string.connectionTimedOut);

			Bundle b=new Bundle();
			b.putString("passstatus", merchant_status);
			b.putString("passmsg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			merchant.send(0, b);
		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			String merchant_status="false";
			String merchant_msg=getResources().getString(R.string.connectionTimedOut);

			Bundle b=new Bundle();
			b.putString("passstatus", merchant_status);
			b.putString("passmsg", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			merchant.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			String merchant_status="false";
			String merchant_msg="Something Went wrong";

			Bundle b=new Bundle();
			b.putString("passstatus", merchant_status);
			b.putString("passmsg", merchant_msg);
			b.putString("error_code", "-1");
			merchant.send(0, b);
		}
	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String usersmsg="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			usersmsg=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usersmsg;
	}

	String getNumber(String status)
	{
		JSONObject usersmsg;
		String action_Call="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			usersmsg=jsonobject.getJSONObject("data");
			action_Call=usersmsg.getString("action_call");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return action_Call;
	}
}
