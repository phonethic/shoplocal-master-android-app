package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class RegisterMerchentReceiver extends ResultReceiver{

	private RegisterReceiver mReceiver;
	
	public RegisterMerchentReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
		
	}
	
	
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(mReceiver!=null)
		{
			mReceiver.onReceiveRegisterResult(resultCode, resultData);
		}
	}



	public interface RegisterReceiver
	{
		public void onReceiveRegisterResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(RegisterReceiver receiver)
	{
		mReceiver=receiver;
	}

}
