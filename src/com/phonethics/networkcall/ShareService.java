package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class ShareService extends IntentService {
	
	ResultReceiver shareService;
	
	BufferedReader bufferedReader = null;
	String status = "";
	
	String profile_status;

	public ShareService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public ShareService(){
		
		super("shareService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		shareService = intent.getParcelableExtra("shareService");
		
		String URL = intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		String post_id = intent.getStringExtra("post_id");
		String via = intent.getStringExtra("via");
		

		
		Log.i("URL", "URL : "+URL);
		Log.i("URL", "URL : "+user_id);
		Log.i("URL", "URL : "+auth_id);
		
		try {
			
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);

			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);
			
			//Setting URL to Get data.
			HttpPost httpPost=new HttpPost(URL);
			
			//Adding header.
			httpPost.addHeader(API_HEADER, API_HEADER_VALUE);

			
			JSONObject json = new JSONObject();
			
			

/*			json.put("user_id", user_id);
			json.put("auth_id", auth_id	);*/
			json.put("post_id", post_id	);
			json.put("via", via	);

			StringEntity se = new StringEntity( json.toString());  

			
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);
			
			HttpResponse response=httpClient.execute(httpPost);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");
			
			

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response "+stringBuffer.toString());
			status=stringBuffer.toString();
			
			
			try {
				
				JSONObject jsonobject = new JSONObject(status);
				
				String share_status = jsonobject.getString("success");
				String share_msg  =  jsonobject.getString("message"); 
				
				Bundle b=new Bundle();
				b.putString("share_status", share_status);
				b.putString("share_msg", share_msg);
				shareService.send(0, b);
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("share_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("share_status", "false");
			
			
			shareService.send(0, b);

		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("share_msg", "Connection Timed out");
			b.putString("share_status", "false");
			

			shareService.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			
			Bundle b=new Bundle();
			b.putString("share_msg", "Try again");
			b.putString("share_status", "false");
			
			
			shareService.send(0, b);
			
		}
		

		
	}

}
