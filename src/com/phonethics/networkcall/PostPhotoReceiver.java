package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class PostPhotoReceiver extends ResultReceiver {

	photoreceiver photo;
	public PostPhotoReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(photo!=null)
		{
			photo.onReceivePhoto(resultCode, resultData);
		}
	}


	public interface photoreceiver
	{
		public void onReceivePhoto (int resultCode, Bundle resultData);
	}
	
	public void setReceiver(photoreceiver rec)
	{
		photo=rec;
	}
	
	
}
