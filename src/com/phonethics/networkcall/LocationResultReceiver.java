package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class LocationResultReceiver extends ResultReceiver {

	private Receiver mReceiver;
	
	public LocationResultReceiver(Handler handler) {	
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(mReceiver!=null)
		{
			mReceiver.onReceiveResult(resultCode, resultData);
		}
	}
	
	public interface Receiver
	{
		public void onReceiveResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(Receiver receiver)
	{
		mReceiver=receiver;
	}

}
