package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetAllStoresOfPaticularBusinessService extends IntentService {

	ResultReceiver getAllStore;
	String SEARCH_STATUS="";
	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> BUSINESS_ID=new ArrayList<String>();
	ArrayList<String> SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> BUILDING=new ArrayList<String>();
	ArrayList<String> STREET=new ArrayList<String>();
	ArrayList<String> LANDMARK=new ArrayList<String>();
	ArrayList<String> AREA=new ArrayList<String>();
	ArrayList<String> PINCODE=new ArrayList<String>();
	ArrayList<String> CITY=new ArrayList<String>();
	ArrayList<String> STATE=new ArrayList<String>();
	ArrayList<String> COUNTRY=new ArrayList<String>();
	ArrayList<String> LATITUDE=new ArrayList<String>();
	ArrayList<String> LONGITUDE=new ArrayList<String>();
	ArrayList<String> TELNO1=new ArrayList<String>();
	ArrayList<String> TELNO2=new ArrayList<String>();
	ArrayList<String> MOBILE_NO=new ArrayList<String>();
	ArrayList<String> MOBNO2=new ArrayList<String>();
	ArrayList<String> FAX1=new ArrayList<String>();
	ArrayList<String> FAX2=new ArrayList<String>();
	ArrayList<String> TOLLFREE1=new ArrayList<String>();	
	ArrayList<String> TOLLFREE2=new ArrayList<String>();
	ArrayList<String> PHOTO=new ArrayList<String>();
	public GetAllStoresOfPaticularBusinessService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetAllStoresOfPaticularBusinessService()
	{
		super("GetAllStores");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		getAllStore=intent.getParcelableExtra("getAllStores");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		String business_id=intent.getStringExtra("business_id");

		BufferedReader bufferedReader = null;
		String status = "";

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient();
		

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);
		
		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);
		httpGet.addHeader("business_id",business_id);


		Log.i("URL", "URL : "+URL);
		Log.i("URL", "URL : "+user_id);
		Log.i("URL", "URL : "+auth_id);
		Log.i("URL", "URL : "+business_id);



		try
		{
			HttpResponse response=httpClient.execute(httpGet);
			
			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store Details: "+stringBuffer.toString());
			status=stringBuffer.toString();

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}



		/*
		 * Parsing JSON DATA
		 */
		try
		{
			JSONObject jsonobject=new JSONObject(status);

			/*		STREET=jsonobject.getString("street");
			LANDMARK=jsonobject.getString("landmark");
			AREA=jsonobject.getString("area");
			CITY=jsonobject.getString("city");
			PINCODE=jsonobject.getString("pincode");
			SOTRE_NAME=jsonobject.getString("store_name");
			MOBILE_NO=jsonobject.getString("mob_no1");
			PHOTO=jsonobject.getString("photo");*/

			SEARCH_STATUS=jsonobject.getString("status");
			if(SEARCH_STATUS.equalsIgnoreCase("set"))
			{
				JSONArray getMsg=jsonobject.getJSONArray("message");

				for(int i=0;i<getMsg.length();i++)
				{
				JSONObject record=getMsg.getJSONObject(i);
				ID.add(record.getString("id"));
				BUSINESS_ID.add(record.getString("business_id"));
				SOTRE_NAME.add(record.getString("store_name"));	
				BUILDING.add(record.getString("building"));
				STREET.add(record.getString("street"));
				LANDMARK.add(record.getString("landmark"));
				AREA.add(record.getString("area"));
				PINCODE.add(record.getString("pincode"));
				CITY.add(record.getString("city"));
				STATE.add(record.getString("state"));
				COUNTRY.add(record.getString("country"));
				LATITUDE.add(record.getString("latitude"));
				LONGITUDE.add(record.getString("longitude"));
				TELNO1.add(record.getString("tel_no1"));
				TELNO2.add(record.getString("tel_no2"));
				MOBILE_NO.add(record.getString("mob_no1"));
				MOBNO2.add(record.getString("mob_no2"));
				FAX1.add(record.getString("fax_no1"));
				FAX2.add(record.getString("fax_no2"));
				TOLLFREE1.add(record.getString("toll_free_no1"));	
				TOLLFREE2.add(record.getString("toll_free_no2"));
				PHOTO.add(record.getString("photo"));
				}


				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", SEARCH_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("BUSINESS_ID", BUSINESS_ID);
				b.putStringArrayList("STORE_NAME", SOTRE_NAME);
				b.putStringArrayList("BUILDING", BUILDING);
				b.putStringArrayList("STREET", STREET);
				b.putStringArrayList("LANDMARK", LANDMARK);
				b.putStringArrayList("AREA", AREA);
				b.putStringArrayList("PINCODE", PINCODE);
				b.putStringArrayList("CITY", CITY);
				b.putStringArrayList("STATE", STATE);
				b.putStringArrayList("COUNTRY", COUNTRY);
				b.putStringArrayList("LATITUDE", LATITUDE);
				b.putStringArrayList("LONGITUDE", LONGITUDE);
				b.putStringArrayList("TELNO1", TELNO1);
				b.putStringArrayList("TELNO2", TELNO2);
				b.putStringArrayList("MOBILE_NO", MOBILE_NO);
				b.putStringArrayList("MOBNO2", MOBNO2);
				b.putStringArrayList("FAX1", FAX1);
				b.putStringArrayList("FAX2", FAX2);
				b.putStringArrayList("TOLLFREE1", TOLLFREE1);
				b.putStringArrayList("TOLLFREE2", TOLLFREE2);
				b.putStringArrayList("PHOTO", PHOTO);


				getAllStore.send(0, b);

			}
			else
			{
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", SEARCH_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("BUSINESS_ID", BUSINESS_ID);
				b.putStringArrayList("STORE_NAME", SOTRE_NAME);
				b.putStringArrayList("BUILDING", BUILDING);
				b.putStringArrayList("STREET", STREET);
				b.putStringArrayList("LANDMARK", LANDMARK);
				b.putStringArrayList("AREA", AREA);
				b.putStringArrayList("PINCODE", PINCODE);
				b.putStringArrayList("CITY", CITY);
				b.putStringArrayList("STATE", STATE);
				b.putStringArrayList("COUNTRY", COUNTRY);
				b.putStringArrayList("LATITUDE", LATITUDE);
				b.putStringArrayList("LONGITUDE", LONGITUDE);
				b.putStringArrayList("TELNO1", TELNO1);
				b.putStringArrayList("TELNO2", TELNO2);
				b.putStringArrayList("MOBILE_NO", MOBILE_NO);
				b.putStringArrayList("MOBNO2", MOBNO2);
				b.putStringArrayList("FAX1", FAX1);
				b.putStringArrayList("FAX2", FAX2);
				b.putStringArrayList("TOLLFREE1", TOLLFREE1);
				b.putStringArrayList("TOLLFREE2", TOLLFREE2);
				b.putStringArrayList("PHOTO", PHOTO);


				getAllStore.send(0, b);

			}

		}catch(JSONException js)
		{
			js.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}




	}



}
