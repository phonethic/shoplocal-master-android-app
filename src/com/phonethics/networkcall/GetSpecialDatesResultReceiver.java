package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetSpecialDatesResultReceiver  extends ResultReceiver{
	
	GetSpecialDate result;

	
	public GetSpecialDatesResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		
		if(result!=null){
			
			result.onGetSpecialDates(resultCode, resultData);
		}
	}

	
	public interface GetSpecialDate{
		
		public void onGetSpecialDates(int resultCode, Bundle resultData);
	}
	
	public void setReciver(GetSpecialDate result)
	{
		this.result=result;
	}

}
