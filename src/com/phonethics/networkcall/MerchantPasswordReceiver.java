package com.phonethics.networkcall;





import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MerchantPasswordReceiver extends ResultReceiver{

	private SetPasswordReceiver mReceiver;
	
	public MerchantPasswordReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(mReceiver!=null)
		{
			mReceiver.onReceivePasswordResult(resultCode, resultData);
		}
	}
	public interface SetPasswordReceiver
	{
		public void onReceivePasswordResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(SetPasswordReceiver receiver)
	{
		mReceiver=receiver;
	}

	
}
