package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetPerticularStoreDetailReceiver extends ResultReceiver{

	StoresDetail store_detail;
	public GetPerticularStoreDetailReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(store_detail!=null)
		{
			store_detail.onReceiveStoreDetail(resultCode, resultData);
		}
	}
	
	public interface StoresDetail
	{
		public void onReceiveStoreDetail(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(StoresDetail receiver)
	{
		this.store_detail=receiver;
	}

}
