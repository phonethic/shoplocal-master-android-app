package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.model.GetBroadCast;
import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class BroadCastService extends IntentService {

	ResultReceiver broadcast;
	String SEARCH_STATUS="";
	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> TYPE=new ArrayList<String>();	
	ArrayList<String> URL_OFFERS=new ArrayList<String>();
	ArrayList<String> TITLE=new ArrayList<String>();
	ArrayList<String> BODY=new ArrayList<String>();
	ArrayList<String> DATE=new ArrayList<String>();

	ArrayList<String> PHOTO_LINK=new ArrayList<String>();
	ArrayList<String> VIDEO_LINK=new ArrayList<String>();
	ArrayList<String> EMBED=new ArrayList<String>();
	ArrayList<String> VIDEODATA=new ArrayList<String>();
	ArrayList<String> SOURCE=new ArrayList<String>();
	ArrayList<String> TYPE_URL=new ArrayList<String>();

	ArrayList<String> SOURCE_URL=new ArrayList<String>();
	ArrayList<String>USER_LIKE=new ArrayList<String>();
	ArrayList<String>TOTAL_LIKE=new ArrayList<String>();

	ArrayList<GetBroadCast> getAllBroadCast=new ArrayList<GetBroadCast>();

	JSONObject jsonobject=null;

	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	public BroadCastService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public BroadCastService()
	{
		super("broadcast");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		broadcast=intent.getParcelableExtra("broadcast");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		String STORE_ID=intent.getStringExtra("store_id");
		String post_id=intent.getStringExtra("post_id");
		String page=intent.getIntExtra("page",1)+"";
		String count=intent.getIntExtra("count",20)+"";
		String user_id=intent.getStringExtra("user_id");
		boolean getLike=intent.getBooleanExtra("getLike", false);
		boolean singlePost=intent.getBooleanExtra("singlePost", false);
		BufferedReader bufferedReader = null;
		String status = "";

		//Adding parameters to Url
		if(!singlePost)
		{
			nameValuePairs.add(new BasicNameValuePair("place_id", STORE_ID));
			nameValuePairs.add(new BasicNameValuePair("page", page));
			nameValuePairs.add(new BasicNameValuePair("count", count));
		}
		else
		{
			nameValuePairs.add(new BasicNameValuePair("post_id", post_id));
		}

		if(getLike)
		{
			nameValuePairs.add(new BasicNameValuePair("user_id", user_id));
		}

		//Making url
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");

		//Appending paramters to Url
		URL+=paramString;
		Log.i("Search", "Search Ui "+URL);
		Log.i("Search", "Search STORE_ID  "+STORE_ID);

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);

		/*		httpGet.addHeader("store_id", STORE_ID);*/


		/*httpGet.addHeader("search",search);*/
		/*	if(IS_LOCATION_CHECKED)
		{
			httpGet.addHeader("latitude",latitude);
			httpGet.addHeader("longitude",longitude);
			httpGet.addHeader("distance",distance);
		}
		Log.i("SEARCH", "SEARCH ISLOCATION CHECKED"+IS_LOCATION_CHECKED);
		Log.i("SEARCH", "SEARCH search"+search);
		Log.i("SEARCH", "SEARCH latitude"+latitude);
		Log.i("SEARCH", "SEARCH longitude"+longitude);
		Log.i("SEARCH", "SEARCH distance"+distance);
		 */
		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Broadcast "+stringBuffer.toString());
			status=stringBuffer.toString();

			/*
			 * Parsing JSON DATA
			 */

			try
			{
				jsonobject=new JSONObject(status);

				SEARCH_STATUS=jsonobject.getString("success");


				if(SEARCH_STATUS.equalsIgnoreCase("true"))
				{
					Log.i("Status ", "Status "+SEARCH_STATUS);
					JSONObject getMsg=jsonobject.getJSONObject("data");

					if(!singlePost)
					{
						JSONArray getRecord=getMsg.getJSONArray("record");

						//Log.i("Status ", "Status "+SEARCH_STATUS);
						TOTAL_PAGES=getMsg.getString("total_page");
						TOTAL_RECORDS=getMsg.getString("total_record");
						Log.i("Status ", "Status TOTAL_PAGES "+TOTAL_PAGES);
						Log.i("Status ", "Status TOTAL_RECORDS "+TOTAL_RECORDS);


						for(int i=0;i<getRecord.length();i++)
						{
							JSONObject records=getRecord.getJSONObject(i);
							JSONArray source=null;

							GetBroadCast get=new GetBroadCast();

							get.setId(records.getString("id"));
							get.setDate(records.getString("date"));
							get.setDescription(records.getString("description"));
							get.setTitle(records.getString("title"));
							get.setType(records.getString("type"));
							get.setPlace_id(records.getString("place_id"));
							get.setTags("tags");
							get.setTotal_page(TOTAL_PAGES);
							get.setTotal_record(TOTAL_RECORDS);
							get.setTotal_like(records.getString("total_like"));
							get.setUser_views(records.getString("total_view"));
							get.setTotal_share(records.getString("total_share"));

							get.setImage_url1(records.getString("image_url1"));
							get.setImage_url2(records.getString("image_url2"));
							get.setImage_url3(records.getString("image_url3"));

							get.setThumb_url1(records.getString("thumb_url1"));
							get.setThumb_url2(records.getString("thumb_url2"));
							get.setThumb_url3(records.getString("thumb_url3"));

							get.setImage_title1(records.getString("image_title1"));
							get.setImage_title2(records.getString("image_title2"));
							get.setImage_title3(records.getString("image_title3"));
							


							if(getLike)
							{
								get.setUser_like(records.getString("user_like"));
							}else
							{
								get.setUser_like("-1");
							}


							//						try
							//						{
							//							source=records.getJSONArray("images");
							//						}catch(Exception ex)
							//						{
							//							ex.printStackTrace();
							//							Log.i("Log"," Error");
							//
							//						}
							//						if(source!=null)
							//						{
							//							Log.i("found","found");
							//							for(int j=0;j<source.length();j++)
							//							{
							//								get.setSource(source.getString(j));
							//							}
							//						}
							//						else
							//						{
							//							Log.i("Nofound","notfound");
							//							get.setSource("notfound");
							//						}

							getAllBroadCast.add(get);
							/*					ID.add(records.getString("id"));
						TYPE.add(records.getString("type"));
						TITLE.add(records.getString("title"));
						DATE.add(records.getString("date"));
						BODY.add("description");

						if(records.has("source"))
						{
							JSONArray source=records.getJSONArray("source");
							for(int j=0;j<source.length();j++)
							{
								SOURCE_URL.add(source.getString(j));
							}
						}*/




							/*if(records.getString("type").equalsIgnoreCase("Photo"))
						{
							PHOTO_LINK.add(records.getString("link"));
							SOURCE.add(records.getString("source"));

							TYPE_URL.add("");
							BODY.add("");
							VIDEO_LINK.add("");
							EMBED.add("");
							VIDEODATA.add("");


						}
						if(records.getString("type").equalsIgnoreCase("Url"))
						{
							TYPE_URL.add(records.getString("url"));
							BODY.add(records.getString("body"));

							VIDEO_LINK.add("");
							EMBED.add("");
							VIDEODATA.add("");
							PHOTO_LINK.add("");
							SOURCE.add("");


						}
						if(records.getString("type").equalsIgnoreCase("Video"))
						{
							VIDEO_LINK.add(records.getString("link"));
							EMBED.add(records.getString("embed"));
							VIDEODATA.add(records.getString("video_data"));

							PHOTO_LINK.add("");
							SOURCE.add("");
							TYPE_URL.add("");
							BODY.add("");

						}
						if(records.getString("type").equalsIgnoreCase("Text"))
						{
							BODY.add(records.getString("body"));

							PHOTO_LINK.add("");
							SOURCE.add("");
							TYPE_URL.add("");
							VIDEO_LINK.add("");
							EMBED.add("");
							VIDEODATA.add("");

						}*/


						}

					}else
					{
					
						JSONArray source=null;

						GetBroadCast get=new GetBroadCast();

						get.setId(getMsg.getString("id"));
						get.setDate(getMsg.getString("date"));
						get.setDescription(getMsg.getString("description"));
						get.setTitle(getMsg.getString("title"));
						get.setType(getMsg.getString("type"));
						get.setPlace_id(getMsg.getString("place_id"));
						get.setTags("tags");
						get.setTotal_page(TOTAL_PAGES);
						get.setTotal_record(TOTAL_RECORDS);
						get.setTotal_like(getMsg.getString("total_like"));
						get.setUser_views(getMsg.getString("total_view"));
						get.setTotal_share(getMsg.getString("total_share"));

						get.setImage_url1(getMsg.getString("image_url1"));
						get.setImage_url2(getMsg.getString("image_url2"));
						get.setImage_url3(getMsg.getString("image_url3"));

						get.setThumb_url1(getMsg.getString("thumb_url1"));
						get.setThumb_url2(getMsg.getString("thumb_url2"));
						get.setThumb_url3(getMsg.getString("thumb_url3"));

						get.setImage_title1(getMsg.getString("image_title1"));
						get.setImage_title2(getMsg.getString("image_title2"));
						get.setImage_title3(getMsg.getString("image_title3"));
						
						get.setIs_offer(getMsg.getString("is_offered"));
						get.setOffer_date_time(getMsg.getString("offer_date_time"));

						if(getLike)
						{
							get.setUser_like(getMsg.getString("user_like"));
						}else
						{
							get.setUser_like("-1");
						}
						
						getAllBroadCast.add(get);
						
					}
					for(int i=0;i<ID.size();i++)
					{
						Log.i("", "--------------------------------------------------");
						Log.i("STATUS", "STATUS : "+"ID "+ID.get(i)+" STORE NAME "+TITLE.get(i));


					}


					String SEARCH_MESSAGE="";

					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", SEARCH_STATUS);
					b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
					b.putParcelableArrayList("boradcast",getAllBroadCast);

					/*b.putStringArrayList("ID", ID);
					b.putStringArrayList("TYPE", TYPE);
					b.putStringArrayList("TITLE", TITLE);
					b.putStringArrayList("BODY", BODY);
					b.putStringArrayList("PHOTO_LINK", PHOTO_LINK);
					b.putStringArrayList("SOURCE_URL", SOURCE_URL);
					b.putStringArrayList("VIDEODATA", VIDEODATA);
					b.putStringArrayList("SOURCE", SOURCE);
					b.putStringArrayList("TYPE_URL", TYPE_URL);
					b.putStringArrayList("EMBED", EMBED);
					b.putStringArrayList("DATE", DATE);*/

					broadcast.send(0, b);


				}else if(SEARCH_STATUS.equalsIgnoreCase("false"))
				{
					Log.i("Status ", "Status "+SEARCH_STATUS);
					String SEARCH_MESSAGE=jsonobject.getString("message");
					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", SEARCH_STATUS);
					b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
					b.putParcelableArrayList("boradcast",getAllBroadCast);
					b.putString("error_code", getErrorCode(status));
					/*b.putStringArrayList("ID", ID);
					b.putStringArrayList("TYPE", TYPE);
					b.putStringArrayList("TITLE", TITLE);
					b.putStringArrayList("BODY", BODY);
					b.putStringArrayList("PHOTO_LINK", PHOTO_LINK);
					b.putStringArrayList("VIDEO_LINK", VIDEO_LINK);
					b.putStringArrayList("VIDEODATA", VIDEODATA);
					b.putStringArrayList("SOURCE", SOURCE);
					b.putStringArrayList("TYPE_URL", TYPE_URL);
					b.putStringArrayList("EMBED", EMBED);
					b.putStringArrayList("DATE", DATE);
					 */

					broadcast.send(0, b);
					/*b.putString("SEARCH_STATUS", SEARCH_STATUS);
					b.putStringArrayList("ID", ID);
					b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
					b.putStringArrayList("BUILDING", BUILDING);
					b.putStringArrayList("STREET", STREET);
					b.putStringArrayList("LANDMARK", LANDMARK);
					b.putStringArrayList("AREA", AREA);
					b.putStringArrayList("CITY", CITY);
					b.putStringArrayList("MOBILE_NO", MOBILE_NO);
					b.putStringArrayList("PHOTO", PHOTO);
					b.putStringArrayList("DISTANCE", DISTANCE);
					getSearchResults.send(0, b);*/
				}




			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				String SEARCH_MESSAGE=jsonobject.getString("message");
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
				b.putParcelableArrayList("boradcast",getAllBroadCast);
				/*b.putStringArrayList("ID", ID);
				b.putStringArrayList("TYPE", TYPE);
				b.putStringArrayList("TITLE", TITLE);
				b.putStringArrayList("BODY", BODY);
				b.putStringArrayList("PHOTO_LINK", PHOTO_LINK);
				b.putStringArrayList("VIDEO_LINK", VIDEO_LINK);
				b.putStringArrayList("VIDEODATA", VIDEODATA);
				b.putStringArrayList("SOURCE", SOURCE);
				b.putStringArrayList("TYPE_URL", TYPE_URL);
				b.putStringArrayList("EMBED", EMBED);
				b.putStringArrayList("DATE", DATE);
				 */

				broadcast.send(0, b);

			}


		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			try {
				/*	String SEARCH_MESSAGE=jsonobject.getString("message");*/
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", getResources().getString(R.string.connectionTimedOut));
				b.putParcelableArrayList("boradcast",getAllBroadCast);
				broadcast.send(0, b);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			try {
				/*String SEARCH_MESSAGE=jsonobject.getString("message");*/
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", getResources().getString(R.string.connectionTimedOut));
				b.putParcelableArrayList("boradcast",getAllBroadCast);
				broadcast.send(0, b);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			try {
				/*String SEARCH_MESSAGE=jsonobject.getString("message");*/
				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", "false");
				b.putString("SEARCH_MESSAGE", "Something went wrong");
				b.putParcelableArrayList("boradcast",getAllBroadCast);
				broadcast.send(0, b);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}




	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}
}
