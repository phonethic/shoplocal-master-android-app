package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class StoreOwnerDropDownService extends IntentService {

	ResultReceiver mDropDown;
	String dropdown_status;
	String dropdown_msg;
	ArrayList<String> STORE_ID=new ArrayList<String>();
	ArrayList<String> STORE_NAME=new ArrayList<String>();
	public StoreOwnerDropDownService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public StoreOwnerDropDownService()
	{
		super("GetStoreOwnerDropDown");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		mDropDown=intent.getParcelableExtra("storedrop");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		boolean IsAppend=intent.getBooleanExtra("IsAppend", false);

		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		if(IsAppend)
		{
			URL+="?type=parent";
		}

		BufferedReader bufferedReader = null;
		String status = "";
		
		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =  30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =   30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);
		
		Log.i("URL", "URL : "+URL);
		Log.i("URL", "URL : "+user_id);
		Log.i("URL", "URL : "+auth_id);
		
		try
		{
			HttpResponse response=httpClient.execute(httpGet);
			
			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store Details: "+stringBuffer.toString());
			status=stringBuffer.toString();


			/*
			 * Parsing JSON DATA
			 */
			try
			{
				JSONObject jsonobject=new JSONObject(status);
			
				dropdown_status=jsonobject.getString("success");
				if(dropdown_status.equalsIgnoreCase("true"))
				{
					JSONArray getMessage=jsonobject.getJSONArray("data");
					for(int i=0;i<getMessage.length();i++)
					{
						JSONObject getData=getMessage.getJSONObject(i);
						STORE_ID.add(getData.getString("id"));
						STORE_NAME.add(getData.getString("name"));
						
				
					}
					
					Bundle b=new Bundle();
					b.putString("dropdown_status", dropdown_status);
					b.putStringArrayList("STORE_ID", STORE_ID);
					b.putStringArrayList("STORE_NAME", STORE_NAME);
					mDropDown.send(0, b);
				}
				else
				{
					dropdown_msg=jsonobject.getString("message");
					Bundle b=new Bundle();
					b.putString("dropdown_status", dropdown_status);
					b.putString("dropdown_msg", dropdown_msg);
					b.putStringArrayList("STORE_ID", STORE_ID);
					b.putStringArrayList("STORE_NAME", STORE_NAME);
					mDropDown.send(0, b);
				}
			
				
		
			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Bundle b=new Bundle();
			b.putString("dropdown_status", "false");
			b.putString("dropdown_msg", getResources().getString(R.string.connectionTimedOut));
			b.putStringArrayList("STORE_ID", STORE_ID);
			b.putStringArrayList("STORE_NAME", STORE_NAME);
			mDropDown.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Bundle b=new Bundle();
			b.putString("dropdown_status", "false");
			b.putString("dropdown_msg", getResources().getString(R.string.connectionTimedOut));
			b.putStringArrayList("STORE_ID", STORE_ID);
			b.putStringArrayList("STORE_NAME", STORE_NAME);
			mDropDown.send(0, b);
		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		



	}

}
