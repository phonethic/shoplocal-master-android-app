package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MyShopLocalReceiver extends ResultReceiver{
	MyShop rec;
	public MyShopLocalReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.myShopLocalFeeds(resultCode, resultData);
		}
	}

	public interface MyShop
	{
		public void myShopLocalFeeds(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(MyShop rec)
	{
		this.rec=rec;
	}
}
