package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.phonethics.shoplocal.R;

public class DeleteGalleryImagesService extends IntentService{

	ResultReceiver deleteImages;
	
	
	String deleteStatus;
	
	String message;

	public DeleteGalleryImagesService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public DeleteGalleryImagesService(){
		super("DeleteGalleryImagesService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		deleteImages = intent.getParcelableExtra("deleteImages");

		String URL=intent.getStringExtra("URL");
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");

		BufferedReader bufferedReader = null;
		String status = "";

		try {

			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to post data.
			/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
			HttpDelete httpDelete=new HttpDelete(URL);

			//Adding header.
			httpDelete.addHeader("X-API-KEY", "Fool");

			httpDelete.addHeader("X-HTTP-Method-Override", "DELETE");

			httpDelete.addHeader("user_id", user_id);
			httpDelete.addHeader("auth_id", auth_id);

			Log.i("URL", "URL Date : "+URL);
			Log.i("URL", "URL Date : "+user_id);
			Log.i("URL", "URL Date : "+auth_id);

			HttpResponse response=httpClient.execute(httpDelete);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Delete Gallery: "+stringBuffer.toString());
			status=stringBuffer.toString();

			JSONObject jsonobject=new JSONObject(status);

			deleteStatus = jsonobject.getString("success");
			Bundle b = new Bundle();
			if(deleteStatus.equalsIgnoreCase("true")){
				
				message = jsonobject.getString("message");
			}
			else{
				
				message = jsonobject.getString("message");
				b.putString("error_code", getErrorCode(status));
			}
			

			b.putString("status", deleteStatus);
			b.putString("message", message);
			deleteImages.send(0, b);

		} catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("message", getResources().getString(R.string.connectionTimedOut));
			b.putString("status", "error");
			b.putString("error_code", "-1");
			deleteImages.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message", getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			deleteImages.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("message", getResources().getString(R.string.exception));
			b.putString("status", "error");
			b.putString("error_code", "-1");
			deleteImages.send(0, b);
		}
	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}

}
