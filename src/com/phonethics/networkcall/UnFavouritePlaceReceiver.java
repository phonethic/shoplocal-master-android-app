package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class UnFavouritePlaceReceiver extends ResultReceiver{

	UnfavouriteInterface rec;
	public UnFavouritePlaceReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.unFavouriteResult(resultCode, resultData);
		}
	}

	public interface UnfavouriteInterface
	{
		public void unFavouriteResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(UnfavouriteInterface rec)
	{
		this.rec=rec;
	}
	
}
