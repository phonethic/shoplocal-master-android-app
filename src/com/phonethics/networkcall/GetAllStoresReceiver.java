package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetAllStoresReceiver extends ResultReceiver {

	AllStore all_store;
	public GetAllStoresReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(all_store!=null)
		{
			all_store.onReceiveAllStore(resultCode, resultData);
		}
	}

	public interface AllStore
	{
		public void onReceiveAllStore(int resultCode, Bundle resultData);
	}
	public void setReceiver(AllStore rec)
	{
		all_store=rec;
	}
}
