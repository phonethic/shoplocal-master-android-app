package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class NewsFeedReceiver extends ResultReceiver{

	NewsFeed newsfeed;
	public NewsFeedReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(newsfeed!=null)
		{
			newsfeed.onNewsFeedResult(resultCode, resultData);
		}
	}
	
	public interface NewsFeed
	{
		public void onNewsFeedResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(NewsFeed rec)
	{
		this.newsfeed=rec;
	}

}
