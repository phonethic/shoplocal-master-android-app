package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AddCustomerProfileReceiver extends ResultReceiver {

	CustomerProfileReceiver rec;
	public AddCustomerProfileReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onReceiveCustomerProfile(resultCode, resultData);
		}
	}

	public interface CustomerProfileReceiver
	{
		public void onReceiveCustomerProfile(int resultCode, Bundle resultData);
	}

	public void setReceiver(CustomerProfileReceiver rec)
	{
		this.rec=rec;
	}
}
