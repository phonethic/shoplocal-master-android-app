package com.phonethics.networkcall;

import com.phonethics.networkcall.GetDateCategoryReceiver.GetDate;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetAreaResultReceiver extends ResultReceiver {

	GetArea result;

	public GetAreaResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}




	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		super.onReceiveResult(resultCode, resultData);

		if(result!=null)
		{
			result.onGetAreaReceive(resultCode, resultData);
		}
	}


	public interface GetArea{

		public void onGetAreaReceive(int resultCode, Bundle resultData);
	}


	public void setReciver(GetArea result)
	{
		this.result=result;
	}

}
