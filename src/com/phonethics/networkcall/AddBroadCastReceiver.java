package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AddBroadCastReceiver extends ResultReceiver{

	AddBroadCastResult result;
	public AddBroadCastReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(result!=null)
		{
			result.onReceiveAddBroadCastResult(resultCode, resultData);
		}
	}
	public interface AddBroadCastResult 
	{
		public void onReceiveAddBroadCastResult(int resultCode, Bundle resultData);
	}
	public void setReceiver(AddBroadCastResult rec)
	{
		result=rec;
	}

}
