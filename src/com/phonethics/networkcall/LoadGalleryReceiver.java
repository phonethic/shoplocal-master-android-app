package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class LoadGalleryReceiver extends ResultReceiver {

	LoadGallery gallery;
	public LoadGalleryReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(gallery!=null)
		{
			gallery.onReceiveGalleryResult(resultCode, resultData);
		}
	}

	public interface LoadGallery
	{
		public void onReceiveGalleryResult(int resultCode, Bundle resultData);
		
	}
	
	public void setReceiver(LoadGallery load)
	{
		gallery=load;
	}
}
