package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.model.NewsFeedModel;
import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

public class MyShopLocalService extends IntentService {

	ResultReceiver myShopLocal;
	
	ArrayList<NewsFeedModel> myshop=new ArrayList<NewsFeedModel>();


	String total_page="";
	String total_record="";

	JSONObject jsonobject=null;

	String status="";
	String message="";
	
	String place_status="";
	
	BufferedReader bufferedReader = null;
	
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


	
	public MyShopLocalService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	

	public MyShopLocalService() {
		super("MyShoplocal");
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		myShopLocal=intent.getParcelableExtra("myShopLocal");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		String post_count=intent.getIntExtra("count",-1)+"";
		String page=intent.getIntExtra("page",1)+"";
		
		nameValuePairs.add(new BasicNameValuePair("page", page));
		nameValuePairs.add(new BasicNameValuePair("count", post_count));
		
		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+="?"+paramString;

		Log.i("Response : ", "My Shoplocal URL "+URL);
		
		post_count="50";
		try
		{
			
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);
			httpGet.addHeader("user_id", user_id);
			httpGet.addHeader("auth_id", auth_id);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "My Shoplocal Response "+stringBuffer.toString());

			status=stringBuffer.toString();

			jsonobject=new JSONObject(status);

			place_status=jsonobject.getString("success");
			
			
			if(place_status.equalsIgnoreCase("true"))
			{
				try
				{
					JSONObject getData=jsonobject.getJSONObject("data");

					JSONArray getRecord=getData.getJSONArray("record");

					total_page=getData.getString("total_page");
					total_record=getData.getString("total_record");

					for(int i=0;i<getRecord.length();i++)
					{
						JSONObject records=getRecord.getJSONObject(i);
						NewsFeedModel news=new NewsFeedModel();

						news.setId(records.getString("id"));
						news.setArea_id(records.getString("area_id"));
						news.setName(records.getString("name"));

						news.setLatitude(records.getString("latitude"));
						news.setLongitude(records.getString("longitude"));
						news.setDistance(records.getString("distance"));

						news.setMob_no1(records.getString("mob_no1"));
						news.setMob_no2(records.getString("mob_no2"));
						news.setMob_no3(records.getString("mob_no3"));

						news.setTel_no1(records.getString("tel_no1"));
						news.setTel_no2(records.getString("tel_no2"));
						news.setTel_no3(records.getString("tel_no3"));

						news.setTitle(records.getString("title"));

						news.setType(records.getString("type"));
						news.setPost_id(records.getString("post_id"));
						news.setDescription(records.getString("description"));

						news.setImage_url(records.getString("image_url1"));
						news.setImage_url2(records.getString("image_url2"));
						news.setImage_url3(records.getString("image_url3"));

						news.setThumb_url(records.getString("thumb_url1"));
						news.setThumb_url2(records.getString("thumb_url2"));
						news.setThumb_url3(records.getString("thumb_url3"));

						news.setDate(records.getString("date"));
						news.setIs_offered(records.getString("is_offered"));

						news.setOffer_date_time(records.getString("offer_date_time"));

						news.setTotal_like(records.getString("total_like"));
						news.setTotal_share(records.getString("total_share"));
						
						news.setVerified(records.getString("verified"));

						myshop.add(news);

						
					}
					Log.i("My Shoplocal SIZE ", "My Shoplocal SIZE SERVICE "+myshop.size()+"Total_page : "+total_page+" Total Record "+total_record);


					
					Bundle b=new Bundle();
					b.putString("status", place_status);
					b.putString("message", message);
					b.putString("total_page", total_page);
					b.putString("total_record", total_record);
					b.putParcelableArrayList("posts",myshop);
					myShopLocal.send(0, b);
					

				}catch(Exception ex)
				{
					ex.printStackTrace();

					Bundle b=new Bundle();
					b.putString("status", "error");
					b.putString("message", "Something went wrong");
					b.putString("error_code", "-1");
					myShopLocal.send(0, b);
				}

			}
			else
			{
				message=jsonobject.getString("message");
				String error_code=jsonobject.getString("code");
				
				Bundle b=new Bundle();
				b.putString("status", place_status);
				b.putString("message", message);
				b.putString("error_code", error_code);

				myShopLocal.send(0, b);

			}
			



			
		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message",  getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			myShopLocal.send(0, b);
		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();

			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message",  getResources().getString(R.string.connectionTimedOut));
			b.putString("error_code", "-1");
			myShopLocal.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message",  getResources().getString(R.string.exception));
			b.putString("error_code", "-1");
			myShopLocal.send(0, b);
		}
	}
	
	

}
