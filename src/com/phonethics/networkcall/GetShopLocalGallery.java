package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.phonethics.shoplocal.R;

public class GetShopLocalGallery extends IntentService {

	ResultReceiver getShopLocalGallery;
	String GALLERY_STATUS="";
	String GALLERY_MESSAGE="";
	ArrayList<String>ID=new ArrayList<String>();
	ArrayList<String>SOURCE=new ArrayList<String>();
	public GetShopLocalGallery(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public GetShopLocalGallery() {
		super("getShopLocalGallery");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		getShopLocalGallery=intent.getParcelableExtra("getShopLocalGallery");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		
		BufferedReader bufferedReader = null;
		String status = "";
		

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection =  30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		Log.i("RESPONSE", "RESPONSE Gallery URL "+URL);
		
		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store Details Gallery : "+stringBuffer.toString());
			status=stringBuffer.toString();
			

			/*
			 * Parsing JSON DATA
			 */
			try
			{
				JSONObject jsonobject=new JSONObject(status);

				/*		STREET=jsonobject.getString("street");
				LANDMARK=jsonobject.getString("landmark");
				AREA=jsonobject.getString("area");
				CITY=jsonobject.getString("city");
				PINCODE=jsonobject.getString("pincode");
				SOTRE_NAME=jsonobject.getString("store_name");
				MOBILE_NO=jsonobject.getString("mob_no1");
				PHOTO=jsonobject.getString("photo");*/

				GALLERY_STATUS=jsonobject.getString("success");

				if(GALLERY_STATUS.equalsIgnoreCase("true"))
				{
					JSONObject data=jsonobject.getJSONObject("data");
					JSONArray gallery=data.getJSONArray("Gallery");

					for(int i=0;i<gallery.length();i++)
					{
					/*JSONObject record=getMsg.getJSONObject(i);*/
				
					SOURCE.add(gallery.getString(i));

					
					}


					Bundle b=new Bundle();
					b.putString("GALLERY_STATUS", GALLERY_STATUS);
					b.putString("GALLERY_MESSAGE", GALLERY_MESSAGE);
					b.putStringArrayList("SOURCE", SOURCE);
					getShopLocalGallery.send(0, b);

				}
				else
				{
					
					Bundle b=new Bundle();
					b.putString("GALLERY_STATUS", GALLERY_STATUS);
					b.putString("GALLERY_MESSAGE", GALLERY_MESSAGE);
					b.putStringArrayList("SOURCE", SOURCE);
					getShopLocalGallery.send(0, b);


				}

			}catch(JSONException js)
			{
				js.printStackTrace();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Bundle b=new Bundle();
			b.putString("GALLERY_STATUS", GALLERY_STATUS);
			b.putString("GALLERY_MESSAGE", getResources().getString(R.string.connectionTimedOut));
			b.putStringArrayList("SOURCE", SOURCE);
			getShopLocalGallery.send(0, b);

		}catch(SocketTimeoutException s)
		{
			s.printStackTrace();
			Bundle b=new Bundle();
			b.putString("GALLERY_STATUS", GALLERY_STATUS);
			b.putString("GALLERY_MESSAGE", getResources().getString(R.string.connectionTimedOut));
			b.putStringArrayList("SOURCE", SOURCE);
			getShopLocalGallery.send(0, b);

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("GALLERY_STATUS", GALLERY_STATUS);
			b.putString("GALLERY_MESSAGE", getResources().getString(R.string.exceptionMessage));
			b.putStringArrayList("SOURCE", SOURCE);
			getShopLocalGallery.send(0, b);
		}
		

	}

}
