package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class PlaceShareResultReceiver extends ResultReceiver{
	
	PlaceShareResultInterface share;

	public PlaceShareResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}
	
	public interface PlaceShareResultInterface
	{
		

		public void placeShareStatus(int resultCode, Bundle resultData);
	}	
	
	public void setReceiver(PlaceShareResultInterface share)
	{
		this.share=share;
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(share!=null)
		{
			share.placeShareStatus(resultCode, resultData);
			
		}
	}

}
