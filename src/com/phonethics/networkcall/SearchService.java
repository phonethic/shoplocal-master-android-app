package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import com.phonethics.shoplocal.R;

public class SearchService extends IntentService {
	ResultReceiver getSearchResults;

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> BUILDING=new ArrayList<String>();
	ArrayList<String> STREET=new ArrayList<String>();
	ArrayList<String> LANDMARK=new ArrayList<String>();
	ArrayList<String> AREA_ID=new ArrayList<String>();
	ArrayList<String> AREA=new ArrayList<String>();
	ArrayList<String> CITY=new ArrayList<String>();
	ArrayList<String> STATE=new ArrayList<String>();
	ArrayList<String> COUNTRY=new ArrayList<String>();
	ArrayList<String> MOBILE_NO=new ArrayList<String>();
	ArrayList<String> MOBILE_NO2=new ArrayList<String>();
	ArrayList<String> MOBILE_NO3=new ArrayList<String>();

	ArrayList<String> TEL_NO=new ArrayList<String>();
	ArrayList<String> TEL_NO2=new ArrayList<String>();
	ArrayList<String> TEL_NO3=new ArrayList<String>();

	ArrayList<String> PHOTO=new ArrayList<String>();
	ArrayList<String> DISTANCE=new ArrayList<String>();

	ArrayList<String> DESCRIPTION=new ArrayList<String>();

	ArrayList<String> HAS_OFFERS=new ArrayList<String>();
	ArrayList<String> OFFERS=new ArrayList<String>();

	ArrayList<String> PLACE_PARENT=new ArrayList<String>();
	ArrayList<String> VERIFIED=new ArrayList<String>();
	ArrayList<String> TOTAL_LIKE=new ArrayList<String>();

	ArrayList<String> EMAIL=new ArrayList<String>();
	ArrayList<String> WEBSITE=new ArrayList<String>();

	String SEARCH_STATUS="";
	String SEARCH_MESSAGE="";
	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	String place_id;

	public SearchService(String name) {
		super(name);
	}
	public SearchService()
	{
		super("SearchApi");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		getSearchResults=intent.getParcelableExtra("searchapi");



		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		boolean IS_LOCATION_CHECKED=intent.getBooleanExtra("isCheckLocation",false);
		boolean IS_OFFERS_CHECKED=intent.getBooleanExtra("isCheckOffers",false);
		boolean IS_CATEGORY=intent.getBooleanExtra("isCategory",false);

		String search=intent.getStringExtra("search");
		String search_post_count=intent.getIntExtra("search_post_count",0)+"";
		String search_page=intent.getIntExtra("search_page",0)+"";
		String category_id=intent.getStringExtra("category_id");
		String latitude=intent.getStringExtra("latitude");
		String longitude=intent.getStringExtra("longitude");
		String distance=intent.getStringExtra("distance");
		String offer=intent.getStringExtra("offer");

		String key=intent.getStringExtra("key");
		String value=intent.getStringExtra("value");

		search_post_count="50";


		BufferedReader bufferedReader = null;
		String status = "";
		distance=getResources().getString(R.string.distance);

		try
		{

			/*SharedPreferences prefs=getSharedPreferences("distance", MODE_PRIVATE);
		distance=prefs.getString("distance", "-1");
		Log.i("DISTANCE PREFS","DISTANCE PREFS "+distance);*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		Log.i("Search Service", "Search Service started");
		if(IS_CATEGORY)
		{
			/*URL+=makeUrl("place_id", place_id, false)+makeUrl("category_id", category_id, false)+makeUrl("page", search_page, false)
					+makeUrl("count", search_post_count, true);*/
			/*nameValuePairs.add(new BasicNameValuePair("place_id", place_id));*/

			nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
			nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
			//nameValuePairs.add(new BasicNameValuePair("locality", locality));
			nameValuePairs.add(new BasicNameValuePair("distance", distance));


			/*nameValuePairs.add(new BasicNameValuePair("place_id", place_id));*/

			//place_id ,292
			//locality lokhandwala

			nameValuePairs.add(new BasicNameValuePair(key, value));

			nameValuePairs.add(new BasicNameValuePair("category_id", category_id));
			nameValuePairs.add(new BasicNameValuePair("page", search_page));
			nameValuePairs.add(new BasicNameValuePair("count", search_post_count));

			String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
			URL+=paramString;
		}
		else
		{

			if(IS_LOCATION_CHECKED && IS_OFFERS_CHECKED)
			{
				nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
				nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
				nameValuePairs.add(new BasicNameValuePair("distance", distance));
				nameValuePairs.add(new BasicNameValuePair(key, value));
				nameValuePairs.add(new BasicNameValuePair("search", search));
				nameValuePairs.add(new BasicNameValuePair("offer", offer));
				nameValuePairs.add(new BasicNameValuePair("page", search_page));
				nameValuePairs.add(new BasicNameValuePair("count", search_post_count));
				String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
				URL+=paramString;
			}
			//text+location
			else if(IS_LOCATION_CHECKED)
			{
				nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
				nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
				nameValuePairs.add(new BasicNameValuePair("distance", distance));
				nameValuePairs.add(new BasicNameValuePair(key, value));
				nameValuePairs.add(new BasicNameValuePair("search", search));
				nameValuePairs.add(new BasicNameValuePair("page", search_page));
				nameValuePairs.add(new BasicNameValuePair("count", search_post_count));
				String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
				URL+=paramString;
			}
			//text+offers
			else if(IS_OFFERS_CHECKED)
			{
				nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
				nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
				nameValuePairs.add(new BasicNameValuePair("distance", distance));

				nameValuePairs.add(new BasicNameValuePair(key, value));
				nameValuePairs.add(new BasicNameValuePair("search", search));
				nameValuePairs.add(new BasicNameValuePair("offer", offer));
				nameValuePairs.add(new BasicNameValuePair("page", search_page));
				nameValuePairs.add(new BasicNameValuePair("count", search_post_count));

				String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
				URL+=paramString;
			}
			//only place id
			else
			{
				nameValuePairs.add(new BasicNameValuePair("search", search));
				nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
				nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
				nameValuePairs.add(new BasicNameValuePair("distance", distance));
				nameValuePairs.add(new BasicNameValuePair(key, value));
				nameValuePairs.add(new BasicNameValuePair("page", search_page));
				nameValuePairs.add(new BasicNameValuePair("count", search_post_count));

				String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
				URL+=paramString;
			}
		}

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection = 30000;

		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);


		Log.i("SEARCH", "SEARCH Service URL"+URL);
		Log.i("SEARCH", "SEARCH IS_CATEGORY "+IS_CATEGORY);
		Log.i("SEARCH", "SEARCH category_id "+category_id);
		Log.i("SEARCH", "SEARCH IS_OFFERS_CHECKED "+IS_OFFERS_CHECKED);
		Log.i("SEARCH", "SEARCH offer "+offer);
		Log.i("SEARCH", "SEARCH ISLOCATION CHECKED"+IS_LOCATION_CHECKED);
		Log.i("SEARCH", "SEARCH search"+search);
		Log.i("SEARCH", "SEARCH latitude"+latitude);
		Log.i("SEARCH", "SEARCH longitude"+longitude);
		Log.i("SEARCH", "SEARCH distance"+distance);

		try
		{

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response "+stringBuffer.toString());

			status=stringBuffer.toString();

			/*
			 * Parsing JSON DATA
			 */
			try
			{
				if(status.length()>0)
				{
					JSONObject jsonobject=new JSONObject(status);

					SEARCH_STATUS=jsonobject.getString("success");


					if(SEARCH_STATUS.equalsIgnoreCase("true"))
					{
						Log.i("Status ", "Status "+SEARCH_STATUS);
						JSONObject getMsg=jsonobject.getJSONObject("data");

						JSONArray getRecord=getMsg.getJSONArray("record");

						//Log.i("Status ", "Status "+SEARCH_STATUS);
						TOTAL_PAGES=getMsg.getString("total_page");
						TOTAL_RECORDS=getMsg.getString("total_record");
						Log.i("Status ", "Status TOTAL_PAGES "+TOTAL_PAGES);
						Log.i("Status ", "Status TOTAL_RECORDS "+TOTAL_RECORDS);
						if(!IS_OFFERS_CHECKED)
						{
							for(int i=0;i<getRecord.length();i++)
							{
								JSONObject records=getRecord.getJSONObject(i);
								ID.add(records.getString("id"));
								SOTRE_NAME.add(records.getString("name"));
								if(IS_LOCATION_CHECKED)
								{
									STREET.add(records.getString("street"));
									BUILDING.add(records.getString("building"));
									LANDMARK.add(records.getString("landmark"));
									try
									{
										if(records.has("distance"))
										{
											DISTANCE.add(records.getString("distance"));
										}
										else
										{
											DISTANCE.add("0");
										}
									}catch(Exception ex)
									{
										ex.printStackTrace();
										DISTANCE.add("0");
									}
								}
								else 
								{
									DISTANCE.add("0");
								}
								DESCRIPTION.add(records.getString("description"));
								BUILDING.add(records.getString("building"));
								STREET.add(records.getString("street"));
								LANDMARK.add(records.getString("landmark"));
								AREA_ID.add(records.getString("area_id"));

								HAS_OFFERS.add(records.getString("has_offer"));
								OFFERS.add(records.getString("title"));
								AREA.add(records.getString("area"));
								CITY.add(records.getString("city"));
								MOBILE_NO.add(records.getString("mob_no1"));
								MOBILE_NO2.add(records.getString("mob_no2"));
								MOBILE_NO3.add(records.getString("mob_no3"));


								TEL_NO.add(records.getString("tel_no1"));
								TEL_NO2.add(records.getString("tel_no2"));
								TEL_NO3.add(records.getString("tel_no3"));


								PHOTO.add(records.getString("image_url"));
								PLACE_PARENT.add(records.getString("place_parent"));
								VERIFIED.add(records.getString("verified"));
								TOTAL_LIKE.add(records.getString("total_like"));
								EMAIL.add(records.getString("email"));
								WEBSITE.add(records.getString("website"));
							}
						}else
						{
							for(int i=0;i<getRecord.length();i++)
							{
								/*JSONArray recordsArray=getRecord.getJSONArray(i);
							for(int j=0;j<recordsArray.length();j++)
							{*/
								JSONObject records=getRecord.getJSONObject(i);
								ID.add(records.getString("id"));	
								SOTRE_NAME.add(records.getString("name"));
								if(IS_LOCATION_CHECKED)
								{
									/*STREET.add(records.getString("street"));
									BUILDING.add(records.getString("building"));
									LANDMARK.add(records.getString("landmark"));*/
									try
									{
										if(records.has("distance"))
										{
											DISTANCE.add(records.getString("distance"));
										}
										else 
										{
											DISTANCE.add("0");
										}
									}catch(Exception ex)
									{
										ex.printStackTrace();
										DISTANCE.add("0");
									}
								}
								else 
								{
									DISTANCE.add("0");
								}
								DESCRIPTION.add(records.getString("description"));
								STREET.add(records.getString("street"));
								BUILDING.add(records.getString("building"));
								LANDMARK.add(records.getString("landmark"));
								AREA_ID.add(records.getString("area_id"));

								HAS_OFFERS.add(records.getString("has_offer"));
								OFFERS.add(records.getString("title"));
								AREA.add(records.getString("area"));
								CITY.add(records.getString("city"));
								MOBILE_NO.add(records.getString("mob_no1"));
								MOBILE_NO2.add(records.getString("mob_no2"));
								MOBILE_NO3.add(records.getString("mob_no3"));


								TEL_NO.add(records.getString("tel_no1"));
								TEL_NO2.add(records.getString("tel_no2"));
								TEL_NO3.add(records.getString("tel_no3"));


								PHOTO.add(records.getString("image_url"));
								PLACE_PARENT.add(records.getString("place_parent"));
								VERIFIED.add(records.getString("verified"));
								TOTAL_LIKE.add(records.getString("total_like"));
								EMAIL.add(records.getString("email"));
								WEBSITE.add(records.getString("website"));
								/*}*/

							}

						}

						for(int i=0;i<ID.size();i++)
						{
							Log.i("", "--------------------------------------------------"+MOBILE_NO3.get(i));
							//	Log.i("STATUS", "STATUS : "+"ID "+ID.get(i)+" STORE NAME "+SOTRE_NAME.get(i)+" DESC "+DESCRIPTION.get(i));


						}

						Bundle b=new Bundle();
						b.putString("SEARCH_STATUS", SEARCH_STATUS);
						b.putStringArrayList("ID", ID);
						b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
						b.putStringArrayList("BUILDING", BUILDING);
						b.putStringArrayList("STREET", STREET);
						b.putStringArrayList("LANDMARK", LANDMARK);
						b.putStringArrayList("AREA_ID", AREA_ID);
						b.putStringArrayList("AREA", AREA);
						b.putStringArrayList("CITY", CITY);
						b.putStringArrayList("MOBILE_NO", MOBILE_NO);
						b.putStringArrayList("MOBILE_NO2", MOBILE_NO2);
						b.putStringArrayList("MOBILE_NO3", MOBILE_NO3);

						b.putStringArrayList("TEL_NO", TEL_NO);
						b.putStringArrayList("TEL_NO2", TEL_NO2);
						b.putStringArrayList("TEL_NO3", TEL_NO3);


						b.putStringArrayList("PHOTO", PHOTO);
						b.putStringArrayList("DISTANCE", DISTANCE);
						b.putStringArrayList("HAS_OFFERS", HAS_OFFERS);
						b.putStringArrayList("OFFERS", OFFERS);
						b.putString("TOTAL_PAGES", TOTAL_PAGES);
						b.putString("TOTAL_RECORDS", TOTAL_RECORDS);
						b.putStringArrayList("DESCRIPTION", DESCRIPTION);
						b.putStringArrayList("PLACE_PARENT", PLACE_PARENT);
						b.putStringArrayList("WEBSITE", WEBSITE);
						b.putStringArrayList("EMAIL", EMAIL);
						b.putStringArrayList("VERIFIED", VERIFIED);
						b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
						getSearchResults.send(0, b);
					}else if(SEARCH_STATUS.equalsIgnoreCase("false"))
					{
						Log.i("Status ", "Status "+SEARCH_STATUS);
						SEARCH_MESSAGE=jsonobject.getString("message");
						Bundle b=new Bundle();
						b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
						b.putString("SEARCH_STATUS", SEARCH_STATUS);
						b.putStringArrayList("ID", ID);
						b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
						b.putStringArrayList("BUILDING", BUILDING);
						b.putStringArrayList("STREET", STREET);
						b.putStringArrayList("LANDMARK", LANDMARK);
						b.putStringArrayList("AREA_ID", AREA_ID);
						b.putStringArrayList("AREA", AREA);
						b.putStringArrayList("CITY", CITY);
						b.putStringArrayList("MOBILE_NO", MOBILE_NO);
						b.putStringArrayList("MOBILE_NO2", MOBILE_NO2);
						b.putStringArrayList("MOBILE_NO3", MOBILE_NO3);

						b.putStringArrayList("TEL_NO", TEL_NO);
						b.putStringArrayList("TEL_NO2", TEL_NO2);
						b.putStringArrayList("TEL_NO3", TEL_NO3);

						b.putStringArrayList("PHOTO", PHOTO);
						b.putStringArrayList("DISTANCE", DISTANCE);
						b.putStringArrayList("HAS_OFFERS", HAS_OFFERS);
						b.putStringArrayList("OFFERS", OFFERS);
						b.putString("TOTAL_PAGES", TOTAL_PAGES);
						b.putString("TOTAL_RECORDS", TOTAL_RECORDS);
						b.putStringArrayList("DESCRIPTION", DESCRIPTION);
						b.putStringArrayList("PLACE_PARENT", PLACE_PARENT);
						b.putStringArrayList("VERIFIED", VERIFIED);
						b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
						b.putStringArrayList("WEBSITE", WEBSITE);
						b.putStringArrayList("EMAIL", EMAIL);
						getSearchResults.send(0, b);
					}
				}



			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				Bundle b=new Bundle();
				b.putString("SEARCH_MESSAGE",getResources().getString(R.string.exceptionMessage));
				b.putString("SEARCH_STATUS", "false");
				getSearchResults.send(0, b);
			}

		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Log.i("Status ", "Status "+SEARCH_STATUS);

			Bundle b=new Bundle();
			b.putString("SEARCH_MESSAGE",getResources().getString(R.string.connectionTimedOut));
			b.putString("SEARCH_STATUS", "false");
			b.putStringArrayList("ID", ID);
			b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
			b.putStringArrayList("BUILDING", BUILDING);
			b.putStringArrayList("STREET", STREET);
			b.putStringArrayList("LANDMARK", LANDMARK);
			b.putStringArrayList("AREA_ID", AREA_ID);
			b.putStringArrayList("AREA", AREA);
			b.putStringArrayList("CITY", CITY);
			b.putStringArrayList("MOBILE_NO", MOBILE_NO);
			b.putStringArrayList("MOBILE_NO2", MOBILE_NO2);
			b.putStringArrayList("MOBILE_NO3", MOBILE_NO3);

			b.putStringArrayList("TEL_NO", TEL_NO);
			b.putStringArrayList("TEL_NO2", TEL_NO2);
			b.putStringArrayList("TEL_NO3", TEL_NO3);

			b.putStringArrayList("PHOTO", PHOTO);
			b.putStringArrayList("DISTANCE", DISTANCE);
			b.putStringArrayList("HAS_OFFERS", HAS_OFFERS);
			b.putStringArrayList("OFFERS", OFFERS);
			b.putString("TOTAL_PAGES", TOTAL_PAGES);
			b.putString("TOTAL_RECORDS", TOTAL_RECORDS);
			b.putStringArrayList("DESCRIPTION", DESCRIPTION);
			b.putStringArrayList("PLACE_PARENT", PLACE_PARENT);
			b.putStringArrayList("VERIFIED", VERIFIED);
			b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
			b.putStringArrayList("WEBSITE", WEBSITE);
			b.putStringArrayList("EMAIL", EMAIL);
			getSearchResults.send(0, b);
		}
		catch(SocketTimeoutException sqt)
		{
			sqt.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Log.i("Status ", "Status "+SEARCH_STATUS);

			Bundle b=new Bundle();
			b.putString("SEARCH_MESSAGE", getResources().getString(R.string.connectionTimedOut));
			b.putString("SEARCH_STATUS", "false");
			b.putStringArrayList("ID", ID);
			b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
			b.putStringArrayList("BUILDING", BUILDING);
			b.putStringArrayList("STREET", STREET);
			b.putStringArrayList("LANDMARK", LANDMARK);
			b.putStringArrayList("AREA_ID", AREA_ID);
			b.putStringArrayList("AREA", AREA);
			b.putStringArrayList("CITY", CITY);
			b.putStringArrayList("MOBILE_NO", MOBILE_NO);
			b.putStringArrayList("MOBILE_NO2", MOBILE_NO2);
			b.putStringArrayList("MOBILE_NO3", MOBILE_NO3);

			b.putStringArrayList("TEL_NO", TEL_NO);
			b.putStringArrayList("TEL_NO2", TEL_NO2);
			b.putStringArrayList("TEL_NO3", TEL_NO3);

			b.putStringArrayList("PHOTO", PHOTO);
			b.putStringArrayList("DISTANCE", DISTANCE);
			b.putStringArrayList("HAS_OFFERS", HAS_OFFERS);
			b.putStringArrayList("OFFERS", OFFERS);
			b.putString("TOTAL_PAGES", TOTAL_PAGES);
			b.putString("TOTAL_RECORDS", TOTAL_RECORDS);
			b.putStringArrayList("DESCRIPTION", DESCRIPTION);
			b.putStringArrayList("PLACE_PARENT", PLACE_PARENT);
			b.putStringArrayList("VERIFIED", VERIFIED);
			b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
			b.putStringArrayList("WEBSITE", WEBSITE);
			b.putStringArrayList("EMAIL", EMAIL);
			getSearchResults.send(0, b);

		}
		catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_MESSAGE", "Something went wrong");
			b.putString("SEARCH_STATUS", "false");
			b.putStringArrayList("ID", ID);
			b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
			b.putStringArrayList("BUILDING", BUILDING);
			b.putStringArrayList("STREET", STREET);
			b.putStringArrayList("LANDMARK", LANDMARK);
			b.putStringArrayList("AREA_ID", AREA_ID);
			b.putStringArrayList("AREA", AREA);
			b.putStringArrayList("CITY", CITY);
			b.putStringArrayList("MOBILE_NO", MOBILE_NO);
			b.putStringArrayList("MOBILE_NO2", MOBILE_NO2);
			b.putStringArrayList("MOBILE_NO3", MOBILE_NO3);

			b.putStringArrayList("TEL_NO", TEL_NO);
			b.putStringArrayList("TEL_NO2", TEL_NO2);
			b.putStringArrayList("TEL_NO3", TEL_NO3);

			b.putStringArrayList("PHOTO", PHOTO);
			b.putStringArrayList("DISTANCE", DISTANCE);
			b.putStringArrayList("HAS_OFFERS", HAS_OFFERS);
			b.putStringArrayList("OFFERS", OFFERS);
			b.putString("TOTAL_PAGES", TOTAL_PAGES);
			b.putString("TOTAL_RECORDS", TOTAL_RECORDS);
			b.putStringArrayList("DESCRIPTION", DESCRIPTION);
			b.putStringArrayList("PLACE_PARENT", PLACE_PARENT);
			b.putStringArrayList("VERIFIED", VERIFIED);
			b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
			b.putStringArrayList("WEBSITE", WEBSITE);
			b.putStringArrayList("EMAIL", EMAIL);
			getSearchResults.send(0, b);

		}

		Log.i("Called", "Called Service");





	}//end of onHandleIntent

	String makeUrl(String parameter,String value,boolean isEnd)
	{
		String Url ;
		if(!isEnd)
		{
			Url=parameter+"="+value+"&";
		}
		else
		{
			Url=parameter+"="+value;
		}

		return Url;
	}

}
