package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class VerifyMobileTelcoResultReceiver extends ResultReceiver {

	VerifyMobileTelcoService receiver;
	public VerifyMobileTelcoResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(receiver!=null)
		{
			receiver.onReceiveMobileVerification(resultCode, resultData);
		}
	}
	
	public interface VerifyMobileTelcoService
	{
		public void onReceiveMobileVerification(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(VerifyMobileTelcoService receiver)
	{
		this.receiver=receiver;
	}
}
