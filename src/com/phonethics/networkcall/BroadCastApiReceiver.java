package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class BroadCastApiReceiver extends ResultReceiver{

	BroadCastApi broadcast_api;
	public BroadCastApiReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(broadcast_api!=null)
		{
			broadcast_api.onReceiverBroadCasts(resultCode, resultData);
		}
	}
	
	public interface BroadCastApi
	{
		void onReceiverBroadCasts(int resultCode, Bundle resultData);
	}
	public void setReceiver(BroadCastApi rec)
	{
		broadcast_api=rec;
	}

}
