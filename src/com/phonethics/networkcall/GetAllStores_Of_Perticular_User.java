package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.localnotification.LocalNotification;
import com.phonethics.shoplocal.DBUtil;
import com.phonethics.shoplocal.MerchantTalkHome;
import com.phonethics.shoplocal.R;
import com.urbanairship.push.PushManager;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class GetAllStores_Of_Perticular_User extends IntentService {

	ResultReceiver store_getall;
	String SEARCH_STATUS="";
	String SEARCH_MESSAGE="";

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> SOTRE_NAME=new ArrayList<String>();	
	ArrayList<String> STORE_DESC=new ArrayList<String>();	
	ArrayList<String> STORE_PARENT=new ArrayList<String>();	
	ArrayList<String> BUILDING=new ArrayList<String>();
	ArrayList<String> STREET=new ArrayList<String>();
	ArrayList<String> LANDMARK=new ArrayList<String>();
	ArrayList<String> AREA=new ArrayList<String>();
	ArrayList<String> PINCODE=new ArrayList<String>();
	ArrayList<String> CITY=new ArrayList<String>();
	ArrayList<String> STATE=new ArrayList<String>();
	ArrayList<String> COUNTRY=new ArrayList<String>();
	ArrayList<String> LATITUDE=new ArrayList<String>();
	ArrayList<String> LONGITUDE=new ArrayList<String>();

	ArrayList<String> TELNO1=new ArrayList<String>();
	ArrayList<String> TELNO2=new ArrayList<String>();
	ArrayList<String> TELNO3=new ArrayList<String>();

	ArrayList<String> MOBILE_NO=new ArrayList<String>();
	ArrayList<String> MOBNO2=new ArrayList<String>();
	ArrayList<String> MOBNO3=new ArrayList<String>();

	ArrayList<String> PHOTO=new ArrayList<String>();


	ArrayList<String> EMAIL=new ArrayList<String>();	
	ArrayList<String> WEBSITE=new ArrayList<String>();	
	ArrayList<String> FACEBOOK=new ArrayList<String>();	
	ArrayList<String> TWITTER=new ArrayList<String>();

	ArrayList<String> PLACE_STATUS=new ArrayList<String>();
	ArrayList<String> PUBLISHED=new ArrayList<String>();

	ArrayList<String> TOTAL_LIKE=new ArrayList<String>();	
	ArrayList<String> TOTAL_SHARE=new ArrayList<String>();	
	ArrayList<String> TOTAL_VIEW=new ArrayList<String>();



	/*	ArrayList<String> FAX1=new ArrayList<String>();
	ArrayList<String> FAX2=new ArrayList<String>();
	ArrayList<String> TOLLFREE1=new ArrayList<String>();	
	ArrayList<String> TOLLFREE2=new ArrayList<String>();*/

	DBUtil dbutil;

	LocalNotification localNotification;

	boolean isMisMatch=false;

	public GetAllStores_Of_Perticular_User(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public GetAllStores_Of_Perticular_User()
	{
		super("GetAllStores_Of_Perticular_User");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		localNotification=new LocalNotification(getApplicationContext());

		dbutil=new DBUtil(getApplicationContext());
		store_getall=intent.getParcelableExtra("getAllStores");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");

		BufferedReader bufferedReader = null;
		String status = "";

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket =  30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);


		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		httpGet.addHeader("user_id",user_id);
		httpGet.addHeader("auth_id",auth_id);

		Log.i("URL", "URL : "+URL);
		Log.i("URL", "URL : "+user_id);
		Log.i("URL", "URL : "+auth_id);



		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store Details: "+stringBuffer.toString());
			status=stringBuffer.toString();


			/*
			 * Parsing JSON DATA
			 */

			JSONObject jsonobject=new JSONObject(status);



			/*		STREET=jsonobject.getString("street");
				LANDMARK=jsonobject.getString("landmark");
				AREA=jsonobject.getString("area");
				CITY=jsonobject.getString("city");
				PINCODE=jsonobject.getString("pincode");
				SOTRE_NAME=jsonobject.getString("store_name");
				MOBILE_NO=jsonobject.getString("mob_no1");
				PHOTO=jsonobject.getString("photo");*/

			SEARCH_STATUS=jsonobject.getString("success");
			if(SEARCH_STATUS.equalsIgnoreCase("true"))
			{
				JSONArray getMsg=jsonobject.getJSONArray("data");
				/*JSONArray getCategoryId=getMsg.getJSONArray("categories");*/

				for(int i=0;i<getMsg.length();i++)
				{
					JSONObject record=getMsg.getJSONObject(i);
					ID.add(record.getString("id"));
					/*				BUSINESS_ID.add(record.getString("business_id"));*/
					SOTRE_NAME.add(record.getString("name"));	
					STORE_PARENT.add(record.getString("place_parent"));
					/*			SOTRE_CATEGORY.add(record.getString(name))*/
					STORE_DESC.add(record.getString("description"));
					BUILDING.add(record.getString("building"));
					STREET.add(record.getString("street"));
					LANDMARK.add(record.getString("landmark"));
					AREA.add(record.getString("area"));
					PINCODE.add(record.getString("pincode"));
					CITY.add(record.getString("city"));
					STATE.add(record.getString("state"));
					COUNTRY.add(record.getString("country"));
					LATITUDE.add(record.getString("latitude"));
					LONGITUDE.add(record.getString("longitude"));

					TELNO1.add(record.getString("tel_no1"));
					TELNO2.add(record.getString("tel_no2"));
					TELNO3.add(record.getString("tel_no3"));

					MOBILE_NO.add(record.getString("mob_no1"));
					MOBNO2.add(record.getString("mob_no2"));
					MOBNO3.add(record.getString("mob_no3"));


					//					FAX1.add(record.getString("fax_no1"));
					//					FAX2.add(record.getString("fax_no2"));
					//					TOLLFREE1.add(record.getString("toll_free_no1"));	
					//					TOLLFREE2.add(record.getString("toll_free_no2"));
					PHOTO.add(record.getString("image_url"));


					EMAIL.add(record.getString("email"));
					WEBSITE.add(record.getString("website"));
					FACEBOOK.add(record.getString("facebook_url"));
					TWITTER.add(record.getString("twitter_url"));

					PLACE_STATUS.add(record.getString("place_status"));
					PUBLISHED.add(record.getString("published"));

					TOTAL_LIKE.add(record.getString("total_like"));
					TOTAL_SHARE.add(record.getString("total_share"));
					TOTAL_VIEW.add(record.getString("total_view"));





				}

				//Writing Data to database
				try
				{
					dbutil.deletePlacesTable();
					dbutil.open();

					for(int i=0;i<ID.size();i++)
					{
						Log.i("", "PLACE CHOOSER "+ID.size()+" ID "+ID.get(i)+" STORE NAME "+SOTRE_NAME.get(i));
						dbutil.create_place_chooser(Integer.parseInt(ID.get(i)), Integer.parseInt(STORE_PARENT.get(i)), SOTRE_NAME.get(i), STORE_DESC.get(i), BUILDING.get(i),
								STREET.get(i), LANDMARK.get(i), AREA.get(i), PINCODE.get(i),
								CITY.get(i), STATE.get(i), COUNTRY.get(i), LATITUDE.get(i),
								LONGITUDE.get(i), TELNO1.get(i), TELNO2.get(i), TELNO3.get(i),
								MOBILE_NO.get(i), MOBNO2.get(i), MOBNO3.get(i), PHOTO.get(i), 
								EMAIL.get(i), WEBSITE.get(i), FACEBOOK.get(i), TWITTER.get(i),
								PLACE_STATUS.get(i), PUBLISHED.get(i),TOTAL_LIKE.get(i),TOTAL_SHARE.get(i),TOTAL_VIEW.get(i));
					}

					dbutil.close();

					//Checking For Empty content
					//And Setting Alarm Based on the empty place details.
					try
					{
						localNotification.checkPref();

						for(int i=0;i<ID.size();i++)
						{
							if(LONGITUDE.get(i).toString().length()==0 || LATITUDE.get(i).toString().length()==0 || PHOTO.get(i).length()==0 || STORE_DESC.get(i).length()==0)
							{

								Log.i("PREF ID ","PREF ID NAME "+ID.get(i)+" "+SOTRE_NAME.get(i));
								localNotification.setPlacePref(ID.get(i),true, localNotification.isPlaceAlarmFired());

								break;
							}
							else
							{
								localNotification.setPlacePref(ID.get(i),false, localNotification.isPlaceAlarmFired());
							}
						}

						//Adding Tags

						Set<String> tags = new HashSet<String>(); 
						tags=PushManager.shared().getTags();

						ArrayList<String>tempIdList=new ArrayList<String>();

						String temp="";
						String replace="";
						if(tags.size()!=0)
						{
							//Getting All Tags
							Iterator<String> iterator=tags.iterator();

							//Traversing Tags to find out if there are any tags existing with own_id
							
							while(iterator.hasNext())
							{
								temp=iterator.next();

								Log.i("TAG", "Urban TAG PRINT "+temp);
								//If tag contains own_
								if(temp.startsWith("own_"))
								{
									//Replace own_ with "" and compare it with ID
									replace=temp.replaceAll("own_", "");
									tempIdList.add(replace);
									Log.i("TAG", "Urban TAG ID "+replace);

									//If Merchants Places ID arrayList does not contain ID which is already in tag list .
									//Set insMisMatch to true
									if(ID.contains(replace)==false)
									{
										isMisMatch=true;
										break;
									}
								}
							}

							if(isMisMatch==false)
							{
								//Check if Tag list has the Merchant Place ID's or not 
								for(int i=0;i<ID.size();i++)
								{
									if(tempIdList.contains(ID.get(i))==false)
									{
										isMisMatch=true;
										break;
									}
								}
							}

							Log.i("ID", "UAIR ID "+ID.toString());
							Log.i("ID", "UAIR ID TAG "+tempIdList.toString()+" "+isMisMatch);



							if(isMisMatch==true)
							{

								//Clear own_tag from Tag List
								for(int i=0;i<tempIdList.size();i++)
								{
									tags.remove("own_"+tempIdList.get(i));
								}

								//Add all ID's of Merchant Places to Tag list.
								for(int i=0;i<ID.size();i++)
								{
									tags.add("own_"+ID.get(i));
								}

								Log.i("TAG", "Urban TAG AFTER "+tags.toString());

								//Set Tags to Urban Airship
								PushManager.shared().setTags(tags);
							}



						}


					}catch(Exception ex)
					{
						ex.printStackTrace();
					}

					SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", false);
					editor.commit();


					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", SEARCH_STATUS);
					b.putStringArrayList("ID", ID);

					b.putStringArrayList("STORE_NAME", SOTRE_NAME);
					b.putStringArrayList("STORE_DESC", STORE_DESC);
					b.putStringArrayList("STORE_PARENT", STORE_PARENT);
					b.putStringArrayList("BUILDING", BUILDING);
					b.putStringArrayList("STREET", STREET);
					b.putStringArrayList("LANDMARK", LANDMARK);
					b.putStringArrayList("AREA", AREA);
					b.putStringArrayList("PINCODE", PINCODE);
					b.putStringArrayList("CITY", CITY);
					b.putStringArrayList("STATE", STATE);
					b.putStringArrayList("COUNTRY", COUNTRY);
					b.putStringArrayList("LATITUDE", LATITUDE);
					b.putStringArrayList("LONGITUDE", LONGITUDE);

					b.putStringArrayList("TELNO1", TELNO1);
					b.putStringArrayList("TELNO2", TELNO2);
					b.putStringArrayList("TELNO3", TELNO3);

					b.putStringArrayList("MOBILE_NO", MOBILE_NO);
					b.putStringArrayList("MOBNO2", MOBNO2);
					b.putStringArrayList("MOBNO3", MOBNO3);

					b.putStringArrayList("EMAIL", EMAIL);
					b.putStringArrayList("WEBSITE", WEBSITE);
					b.putStringArrayList("FACEBOOK", FACEBOOK);

					b.putStringArrayList("TWITTER", TWITTER);
					b.putStringArrayList("PLACE_STATUS", PLACE_STATUS);
					b.putStringArrayList("PUBLISHED", PUBLISHED);

					b.putStringArrayList("TOTAL_LIKE", TOTAL_LIKE);
					b.putStringArrayList("TOTAL_VIEW", TOTAL_VIEW);
					b.putStringArrayList("TOTAL_SHARE", TOTAL_SHARE);
					//					b.putStringArrayList("FAX1", FAX1);
					//					b.putStringArrayList("FAX2", FAX2);
					//					b.putStringArrayList("TOLLFREE1", TOLLFREE1);
					//					b.putStringArrayList("TOLLFREE2", TOLLFREE2);
					b.putStringArrayList("PHOTO", PHOTO);


					store_getall.send(0, b);

				}
				catch(NumberFormatException nb)
				{
					nb.printStackTrace();
					SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();

					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", "error");
					b.putString("SEARCH_MESSAGE",getResources().getString(R.string.exceptionMessage));
					store_getall.send(0, b);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();

					SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", true);
					editor.commit();

					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", "error");
					b.putString("SEARCH_MESSAGE",getResources().getString(R.string.exceptionMessage));
					store_getall.send(0, b);
				}





			}
			else
			{
				try
				{
					dbutil.deletePlacesTable();

					SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
					Editor editor=prefs.edit();
					editor.putBoolean("isPlaceRefreshRequired", false);
					editor.commit();

					String error_code=jsonobject.getString("code");

					SEARCH_MESSAGE=jsonobject.getString("message");
					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", SEARCH_STATUS);
					b.putString("SEARCH_MESSAGE", SEARCH_MESSAGE);
					b.putString("error_code", error_code);
					store_getall.send(0, b);
				}catch(Exception ex)
				{
					ex.printStackTrace();
					Bundle b=new Bundle();
					b.putString("SEARCH_STATUS", "error");
					b.putString("SEARCH_MESSAGE",getResources().getString(R.string.exceptionMessage));
					store_getall.send(0, b);
				}
			}



		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();

			SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
			Editor editor=prefs.edit();
			editor.putBoolean("isPlaceRefreshRequired", true);

			Log.i("Connection Exception", "Connection Timed out Exception");

			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("SEARCH_MESSAGE",getResources().getString(R.string.connectionTimedOut));

			store_getall.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();


			SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
			Editor editor=prefs.edit();
			editor.putBoolean("isPlaceRefreshRequired", true);

			Log.i("Connection Exception", "Connection Timed out Exception");

			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("SEARCH_MESSAGE",getResources().getString(R.string.connectionTimedOut));
			store_getall.send(0, b);
		}
		catch(Exception ex)
		{

			SharedPreferences prefs=getSharedPreferences("PLACE_PREFS", MODE_PRIVATE);
			Editor editor=prefs.edit();
			editor.putBoolean("isPlaceRefreshRequired", true);


			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("SEARCH_STATUS", "error");
			b.putString("SEARCH_MESSAGE","Please try again");
			store_getall.send(0, b);
		}







	}

}
