package com.phonethics.networkcall;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.ResultReceiver;
import android.util.Log;

public class LocationIntentService extends IntentService {

	String latitude="0.0";
	String longitude="0.0";
	String address="";
	String locality="";
	String extra="";
	String city="";
	String pincode="";
	ResultReceiver rec;

	public LocationIntentService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public LocationIntentService()
	{
		super("LocationTag");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		rec=intent.getParcelableExtra("receiverTag");

		// Acquire a reference to the system Location Manager
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


		/*		Criteria criteria = new Criteria();
		String provider = locationManager.getBestProvider(criteria, false);*/
		/*		location = locationManager.getLastKnownLocation(provider);*/


		Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		/*		Location location = locationManager.getLastKnownLocation(provider);*/

		Log.i("Location ", "Location service started");

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				// Called when a new location is found by the network location provider.
				//setting location 
				/*	txtLatLong.setText(location.getLatitude()+" , "+location.getLongitude());
							txtLatLong.startAnimation(anim);*/

				latitude=location.getLatitude()+"";
				longitude=location.getLongitude()+"";

				//getting Address 
//				Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
				try {
//					List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//					if(addresses != null) {
//						Address returnedAddress = addresses.get(0);
//						locality=addresses.get(0).getThoroughfare();
//						pincode=addresses.get(0).getPostalCode();
//
//						StringBuilder strReturnedAddress = new StringBuilder("");
//						for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
//							strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
//						}
//
//
//
//
//						address=strReturnedAddress.toString();
//
//						Log.i("Map: ","Info" +" Address "+address+"\n");
//
//						Log.i("Map: ","Info" +" Country Name "+addresses.get(0).getCountryName());
//						Log.i("Map: ","Info" +" Postal Code "+addresses.get(0).getPostalCode());
//						Log.i("Map: ","Info " +" CountryCode "+addresses.get(0).getCountryCode());
//						Log.i("Map: ","Info " +" FeatureName "+addresses.get(0).getFeatureName());
//						Log.i("Map: ","Info " +" Locality "+addresses.get(0).getLocality());
//						Log.i("Map: ","Info " +" SubLocality "+addresses.get(0).getSubLocality());
//						Log.i("Map: ","Info " +" AdminArea "+addresses.get(0).getAdminArea());
//						Log.i("Map: ","Info" +" SubAdminArea "+addresses.get(0).getSubAdminArea());
//						Log.i("Map: ","Info" +" SubThoroughfare "+addresses.get(0).getSubThoroughfare());
//						Log.i("Map: ","Info" +" Thoroughfare "+addresses.get(0).getThoroughfare());
//						Log.i("Map: ","Info" +" Premises "+addresses.get(0).getPremises());
//
//						city=addresses.get(0).getLocality();	
//
//						/*	Log.i("Map: ","Info" +"\nPostal Code"+addresses.get(0).getPostalCode());*/
//						/*Log.i("Map: ","Info" +"\nCountryCode"+addresses.get(0).getCountryCode());
//											Log.i("Map: ","Info" +"\nFeatureName"+addresses.get(0).getFeatureName());
//											Log.i("Map: ","Info" +"\nLocality"+addresses.get(0).getLocality());
//											Log.i("Map: ","Info" +"\nSubLocality"+addresses.get(0).getSubLocality());
//											Log.i("Map: ","Info" +"\nAdminArea"+addresses.get(0).getAdminArea());
//
//											Log.i("Map: ","Info" +"\nSubAdminArea"+addresses.get(0).getSubAdminArea());
//											Log.i("Map: ","Info" +"\nSubThoroughfare"+addresses.get(0).getSubThoroughfare());*/
//
//
//						Log.i("Map: ","Location Service"+latitude+","+longitude);
//					}
//					else{
//
//						Log.i("Map: ","Location Service"+latitude+","+longitude);
//
//						address="No Address returned!";
//					}

					Bundle b=new Bundle();

					if(pincode==null)
					{
						pincode="0";
					}
					if(city==null)
					{
						city="not found";
					}
					if(locality==null)
					{
						locality="not found";
					}
					if(address==null)
					{
						address="No address found";
					}
					if(latitude==null || latitude.length()==0)
					{
						latitude="0.0";
					}
					if(longitude==null  || longitude.length()==0)
					{
						longitude="0.0";
					}
					
					Log.i("Map: ","Location Service"+latitude+","+longitude);
					
					b.putString("status", "set");
					b.putString("Lat", latitude);
					b.putString("Longi", longitude);
					b.putString("Addr", address);
					b.putString("locality", locality);
					b.putString("city", city);
					b.putString("pincode", pincode);

					rec.send(0, b);

				} catch(Exception ex)
				{
					ex.printStackTrace();

					Bundle b=new Bundle();

					if(pincode==null)
					{
						pincode="0";
					}
					if(city==null)
					{
						city="not found";
					}
					if(locality==null)
					{
						locality="not found";
					}
					if(address==null)
					{
						address="No address found";
					}
					if(latitude==null || latitude.length()==0)
					{
						latitude="0.0";
					}
					if(longitude==null  || longitude.length()==0)
					{
						longitude="0.0";
					}
					b.putString("status", "notset");
					b.putString("Lat", latitude);
					b.putString("Longi", longitude);
					b.putString("Addr", address);
					b.putString("locality", locality);
					b.putString("city", city);
					b.putString("pincode", pincode);

					rec.send(0, b);

					Log.i("Map: ","Location Service"+latitude+","+longitude);


				}
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {}

			public void onProviderEnabled(String provider) {}

			public void onProviderDisabled(String provider) {
				Log.i("Map: ","Location Service Provider disabled"+latitude+","+longitude);

				Bundle b=new Bundle();

				if(pincode==null)
				{
					pincode="0";
				}
				if(city==null)
				{
					city="not found";
				}
				if(locality==null)
				{
					locality="not found";
				}
				if(address==null)
				{
					address="No address found";
				}
				if(latitude==null || latitude.length()==0)
				{
					latitude="0.0";
				}
				if(longitude==null  || longitude.length()==0)
				{
					longitude="0.0";
				}
				b.putString("status", "notset");
				b.putString("Lat", latitude);
				b.putString("Longi", longitude);
				b.putString("Addr", address);
				b.putString("locality", locality);
				b.putString("city", city);
				b.putString("pincode", pincode);

				rec.send(0, b);

				Log.i("Map: ","Location Service"+latitude+","+longitude);

			}
		};

		if(location!=null){
			locationListener.onLocationChanged(location);
		}
		else
		{
			Bundle b=new Bundle();

			if(pincode==null)
			{
				pincode="0";
			}
			if(city==null)
			{
				city="not found";
			}
			if(locality==null)
			{
				locality="not found";
			}
			if(address==null)
			{
				address="No address found";
			}
			if(latitude==null || latitude.length()==0)
			{
				latitude="0.0";
			}
			if(longitude==null  || longitude.length()==0)
			{
				longitude="0.0";
			}
			b.putString("status", "notset");
			b.putString("Lat", latitude);
			b.putString("Longi", longitude);
			b.putString("Addr", address);
			b.putString("locality", locality);
			b.putString("city", city);
			b.putString("pincode", pincode);

			rec.send(0, b);

			Log.i("Map: ","Location Service"+latitude+","+longitude);
		}


	}

}
