package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.model.NewsFeedModel;
import com.phonethics.shoplocal.DBUtil;
import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class NewsFeedService extends IntentService {

	ResultReceiver newsFeed;
	String distance="-1";

	BufferedReader bufferedReader = null;
	String status = "";

	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	DBUtil dbUtil;

	//	ArrayList<String> id=new ArrayList<String>();
	//	ArrayList<String> name=new ArrayList<String>();
	//	ArrayList<String> title=new ArrayList<String>();
	//
	//	
	//	ArrayList<String> tel_no1=new ArrayList<String>();
	//	ArrayList<String> tel_no2=new ArrayList<String>();
	//	ArrayList<String> tel_no3=new ArrayList<String>();
	//	
	//	ArrayList<String> mob_no1=new ArrayList<String>();
	//	ArrayList<String> mob_no2=new ArrayList<String>();
	//	ArrayList<String> mob_no3=new ArrayList<String>();
	//	
	//	ArrayList<String> type=new ArrayList<String>();
	//	ArrayList<String> description=new ArrayList<String>();
	//
	//	ArrayList<String> image_url=new ArrayList<String>();
	//	ArrayList<String> image_url2=new ArrayList<String>();
	//	ArrayList<String> image_url3=new ArrayList<String>();
	//	
	//	ArrayList<String> thumb_url=new ArrayList<String>();
	//	ArrayList<String> thumb_url2=new ArrayList<String>();
	//	ArrayList<String> thumb_url3=new ArrayList<String>();
	//	
	//	ArrayList<String> date=new ArrayList<String>();
	//	ArrayList<String> is_offered=new ArrayList<String>();
	//	
	//	ArrayList<String> offer_date_time=new ArrayList<String>();
	//	ArrayList<String> total_like=new ArrayList<String>();
	//	ArrayList<String> total_share=new ArrayList<String>();
	//	


	ArrayList<NewsFeedModel> news_feed=new ArrayList<NewsFeedModel>();


	String total_page="";
	String total_record="";

	JSONObject jsonobject=null;

	String search_status="";
	String message="";

	public NewsFeedService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public NewsFeedService() {
		super("News Feed");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

	//	dbUtil=new DBUtil(getApplicationContext());

		newsFeed=intent.getParcelableExtra("newsFeed");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String post_count=intent.getIntExtra("count",-1)+"";
		String page=intent.getIntExtra("page",1)+"";

		String area_id=intent.getStringExtra("area_id");
		String category_id=intent.getStringExtra("category_id");
		String offer=intent.getStringExtra("offer");
		String search=intent.getStringExtra("search");
		
		boolean isSearch=intent.getBooleanExtra("isSearch",true);
		boolean isCategory=intent.getBooleanExtra("isCategory",false);
		boolean isCheckOffers=intent.getBooleanExtra("isCheckOffers",false);

		Log.i("Response : ", "News Feed URL page"+page);
		
		post_count="50";
		
//		post_count="5";

		
		if(isSearch)
		{
			nameValuePairs.add(new BasicNameValuePair("search", search));
		}
		nameValuePairs.add(new BasicNameValuePair("area_id", area_id));
		if(isCategory)
		{
			nameValuePairs.add(new BasicNameValuePair("category_id", category_id));
		}
		if(isCheckOffers)
		{
			nameValuePairs.add(new BasicNameValuePair("offer", offer));
		}
		nameValuePairs.add(new BasicNameValuePair("page", page));
		nameValuePairs.add(new BasicNameValuePair("count", post_count));

		String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
		URL+=paramString;

		Log.i("Response : ", "News Feed URL "+URL);

		try
		{
			SharedPreferences prefs=getSharedPreferences("distance", MODE_PRIVATE);
			distance=prefs.getString("distance", "-1");
			Log.i("DISTANCE PREFS","DISTANCE PREFS "+distance);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		try
		{
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "News Feed Response "+stringBuffer.toString());

			status=stringBuffer.toString();

			jsonobject=new JSONObject(status);

			search_status=jsonobject.getString("success");

			if(search_status.equalsIgnoreCase("true"))
			{
				try
				{
					JSONObject getData=jsonobject.getJSONObject("data");

					JSONArray getRecord=getData.getJSONArray("record");

					total_page=getData.getString("total_page");
					total_record=getData.getString("total_record");

					for(int i=0;i<getRecord.length();i++)
					{
						JSONObject records=getRecord.getJSONObject(i);
						NewsFeedModel news=new NewsFeedModel();

						news.setId(records.getString("id"));
						news.setArea_id(records.getString("area_id"));
						news.setName(records.getString("name"));

						news.setLatitude(records.getString("latitude"));
						news.setLongitude(records.getString("longitude"));
						news.setDistance(records.getString("distance"));

						news.setMob_no1(records.getString("mob_no1"));
						news.setMob_no2(records.getString("mob_no2"));
						news.setMob_no3(records.getString("mob_no3"));

						news.setTel_no1(records.getString("tel_no1"));
						news.setTel_no2(records.getString("tel_no2"));
						news.setTel_no3(records.getString("tel_no3"));

						news.setTitle(records.getString("title"));

						news.setType(records.getString("type"));
						news.setPost_id(records.getString("post_id"));
						news.setDescription(records.getString("description"));

						news.setImage_url(records.getString("image_url1"));
						news.setImage_url2(records.getString("image_url2"));
						news.setImage_url3(records.getString("image_url3"));

						news.setThumb_url(records.getString("thumb_url1"));
						news.setThumb_url2(records.getString("thumb_url2"));
						news.setThumb_url3(records.getString("thumb_url3"));

						news.setDate(records.getString("date"));
						news.setIs_offered(records.getString("is_offered"));

						news.setOffer_date_time(records.getString("offer_date_time"));

						news.setTotal_like(records.getString("total_like"));
						news.setTotal_share(records.getString("total_share"));
						
						news.setVerified(records.getString("verified"));

						news_feed.add(news);

						//						Log.i("News Feed ", "News Feed TITLE : "+" TITLE "+records.getString("title"));

					}
					Log.i("News FEED SIZE ", "News FEED SIZE SERVICE "+news_feed.size()+"Total_page : "+total_page+" Total Record "+total_record);

//					try
//					{
//						// Inserting values into database
//						dbUtil.open();
//						for(int i=0;i<news_feed.size();i++)
//						{
//							dbUtil.create_newsFeed(Integer.parseInt(news_feed.get(i).getId()),Integer.parseInt(news_feed.get(i).getArea_id()), news_feed.get(i).getName(), news_feed.get(i).getMob_no1()
//									, news_feed.get(i).getMob_no2(), news_feed.get(i).getMob_no3(), news_feed.get(i).getTel_no1(), news_feed.get(i).getTel_no2(),
//									news_feed.get(i).getTel_no3(), news_feed.get(i).getLatitude(), news_feed.get(i).getLongitude(), news_feed.get(i).getDistance(), Integer.parseInt(news_feed.get(i).getPost_id()),
//									news_feed.get(i).getType(), news_feed.get(i).getTitle(), news_feed.get(i).getDescription(), news_feed.get(i).getImage_url(),news_feed.get(i).getImage_url2(),
//									news_feed.get(i).getImage_url3(),news_feed.get(i).getThumb_url(),news_feed.get(i).getThumb_url2(),news_feed.get(i).getThumb_url3(),
//									news_feed.get(i).getDate(), news_feed.get(i).getIs_offered(),news_feed.get(i).getOffer_date_time(), news_feed.get(i).getTotal_like(), news_feed.get(i).getTotal_share(),total_page,total_record);
//						}
//						dbUtil.close();
//						
//						Bundle b=new Bundle();
//						b.putString("status", search_status);
//						b.putString("message", message);
//						b.putString("total_page", total_page);
//						b.putString("total_record", total_record);
//						b.putParcelableArrayList("posts",news_feed);
//						newsFeed.send(0, b);
//
//					}catch(Exception ex)
//					{
//						ex.printStackTrace();
//						Bundle b=new Bundle();
//						b.putString("status", "false");
//						b.putString("message", "Something went wrong");
//						b.putString("total_page", total_page);
//						b.putString("total_record", total_record);
//						b.putParcelableArrayList("posts",news_feed);
//						newsFeed.send(0, b);
//					}
					
					Bundle b=new Bundle();
					b.putString("status", search_status);
					b.putString("message", message);
					b.putString("total_page", total_page);
					b.putString("total_record", total_record);
					b.putParcelableArrayList("posts",news_feed);
					newsFeed.send(0, b);
					

				}catch(Exception ex)
				{
					ex.printStackTrace();

					Bundle b=new Bundle();
					b.putString("status", "error");
					b.putString("message",  getResources().getString(R.string.exception));
					b.putParcelableArrayList("posts",news_feed);
					b.putString("total_page", total_page);
					b.putString("total_record", total_record);
					newsFeed.send(0, b);
				}

			}
			else
			{
				message=jsonobject.getString("message");
				Bundle b=new Bundle();
				b.putString("status", search_status);
				b.putString("message", message);
				b.putString("error_code", getErrorCode(status));
				b.putParcelableArrayList("posts",news_feed);
				b.putString("total_page", total_page);
				b.putString("total_record", total_record);
				newsFeed.send(0, b);

			}

		}catch(SocketTimeoutException st)
		{
			st.printStackTrace();

			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message",  getResources().getString(R.string.connectionTimedOut));
			b.putParcelableArrayList("posts",news_feed);
			b.putString("total_page", total_page);
			b.putString("total_record", total_record);
			newsFeed.send(0, b);
		}catch(ConnectTimeoutException ct)
		{
			ct.printStackTrace();

			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message",  getResources().getString(R.string.connectionTimedOut));
			b.putParcelableArrayList("posts",news_feed);
			b.putString("total_page", total_page);
			b.putString("total_record", total_record);
			newsFeed.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

			Bundle b=new Bundle();
			b.putString("status", "error");
			b.putString("message",  getResources().getString(R.string.exception));
			b.putParcelableArrayList("posts",news_feed);
			b.putString("total_page", total_page);
			b.putString("total_record", total_record);
			newsFeed.send(0, b);
		}

	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return error_code;
	}

}
