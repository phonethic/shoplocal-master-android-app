package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class StoreDropDownReceiver extends ResultReceiver{
	
	StoreDropDown rec;
	
	public StoreDropDownReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onReceiveStoreDropDown(resultCode, resultData);
		}
	}
	
	public interface StoreDropDown
	{
		public void onReceiveStoreDropDown(int resultCode, Bundle resultData);
	}
	
	public void setStoreDropDownReceiver(StoreDropDown rec)
	{
		this.rec=rec;
	}

}
