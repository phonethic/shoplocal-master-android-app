package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class OffersService extends IntentService {

	ResultReceiver offers;
	String SEARCH_STATUS="";
	String TOTAL_PAGES="";
	String TOTAL_RECORDS="";

	ArrayList<String> ID=new ArrayList<String>();
	ArrayList<String> TYPE=new ArrayList<String>();	
	ArrayList<String> URL_OFFERS=new ArrayList<String>();
	ArrayList<String> TITLE=new ArrayList<String>();
	ArrayList<String> BODY=new ArrayList<String>();
	


	public OffersService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public OffersService()
	{
		super("offers");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		offers=intent.getParcelableExtra("offers");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");
		String STORE_ID=intent.getStringExtra("store_id");

		BufferedReader bufferedReader = null;
		String status = "";

		Log.i("Search", "Search Ui "+URL);
		Log.i("Search", "Search STORE_ID  "+STORE_ID);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient();

		//Setting URL to Get data.
		HttpGet httpGet=new HttpGet(URL);

		//Adding header.
		httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		
		httpGet.addHeader("store_id", STORE_ID);
		/*httpGet.addHeader("search",search);*/
		/*	if(IS_LOCATION_CHECKED)
		{
			httpGet.addHeader("latitude",latitude);
			httpGet.addHeader("longitude",longitude);
			httpGet.addHeader("distance",distance);
		}
		Log.i("SEARCH", "SEARCH ISLOCATION CHECKED"+IS_LOCATION_CHECKED);
		Log.i("SEARCH", "SEARCH search"+search);
		Log.i("SEARCH", "SEARCH latitude"+latitude);
		Log.i("SEARCH", "SEARCH longitude"+longitude);
		Log.i("SEARCH", "SEARCH distance"+distance);
		 */
		try
		{
			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response "+stringBuffer.toString());
			status=stringBuffer.toString();

		}
		catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


		/*
		 * Parsing JSON DATA
		 */
		try
		{
			JSONObject jsonobject=new JSONObject(status);

			SEARCH_STATUS=jsonobject.getString("status");


			if(SEARCH_STATUS.equalsIgnoreCase("set"))
			{
				Log.i("Status ", "Status "+SEARCH_STATUS);
				JSONObject getMsg=jsonobject.getJSONObject("message");

				JSONArray getRecord=getMsg.getJSONArray("record");

				//Log.i("Status ", "Status "+SEARCH_STATUS);
				TOTAL_PAGES=getMsg.getString("total_page");
				TOTAL_RECORDS=getMsg.getString("total_record");
				Log.i("Status ", "Status TOTAL_PAGES "+TOTAL_PAGES);
				Log.i("Status ", "Status TOTAL_RECORDS "+TOTAL_RECORDS);

				for(int i=0;i<getRecord.length();i++)
				{
					JSONObject records=getRecord.getJSONObject(i);
					ID.add(records.getString("id"));
					TYPE.add(records.getString("type"));
					TITLE.add(records.getString("title"));
					BODY.add(records.getString("body"));
					
				}

				for(int i=0;i<ID.size();i++)
				{
					Log.i("", "--------------------------------------------------");
					Log.i("STATUS", "STATUS : "+"ID "+ID.get(i)+" STORE NAME "+TITLE.get(i));


				}

				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", SEARCH_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("TYPE", TYPE);
				b.putStringArrayList("TITLE", TITLE);
				b.putStringArrayList("BODY", BODY);
				
				offers.send(0, b);
				/*b.putString("SEARCH_STATUS", SEARCH_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
				b.putStringArrayList("BUILDING", BUILDING);
				b.putStringArrayList("STREET", STREET);
				b.putStringArrayList("LANDMARK", LANDMARK);
				b.putStringArrayList("AREA", AREA);
				b.putStringArrayList("CITY", CITY);
				b.putStringArrayList("MOBILE_NO", MOBILE_NO);
				b.putStringArrayList("PHOTO", PHOTO);
				b.putStringArrayList("DISTANCE", DISTANCE);
				getSearchResults.send(0, b);*/
			}else if(SEARCH_STATUS.equalsIgnoreCase("notset"))
			{
				Log.i("Status ", "Status "+SEARCH_STATUS);

				Bundle b=new Bundle();
				b.putString("SEARCH_STATUS", SEARCH_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("TYPE", TYPE);
				b.putStringArrayList("TITLE", TITLE);
				b.putStringArrayList("BODY", BODY);
				b.putStringArrayList("URL_OFFERS", URL_OFFERS);
				offers.send(0, b);
				/*b.putString("SEARCH_STATUS", SEARCH_STATUS);
				b.putStringArrayList("ID", ID);
				b.putStringArrayList("SOTRE_NAME", SOTRE_NAME);
				b.putStringArrayList("BUILDING", BUILDING);
				b.putStringArrayList("STREET", STREET);
				b.putStringArrayList("LANDMARK", LANDMARK);
				b.putStringArrayList("AREA", AREA);
				b.putStringArrayList("CITY", CITY);
				b.putStringArrayList("MOBILE_NO", MOBILE_NO);
				b.putStringArrayList("PHOTO", PHOTO);
				b.putStringArrayList("DISTANCE", DISTANCE);
				getSearchResults.send(0, b);*/
			}




		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}
}
