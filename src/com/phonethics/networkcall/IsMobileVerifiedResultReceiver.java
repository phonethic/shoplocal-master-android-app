package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class IsMobileVerifiedResultReceiver extends ResultReceiver {

	IsMobileVerifiedServiceInterface receiver;
	public IsMobileVerifiedResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(receiver!=null)
		{
			receiver.onReceiveMobileVerifyResult(resultCode, resultData);
		}
	}

	public interface IsMobileVerifiedServiceInterface
	{
		public void onReceiveMobileVerifyResult(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(IsMobileVerifiedServiceInterface receiver)
	{
		this.receiver=receiver;
	}
}
