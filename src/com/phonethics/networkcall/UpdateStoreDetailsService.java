package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;



public class UpdateStoreDetailsService extends IntentService {

	ResultReceiver updateStore;
	public UpdateStoreDetailsService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public UpdateStoreDetailsService()
	{
		super("UpdateStoreDetails");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		updateStore=intent.getParcelableExtra("updateStore");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String store_name=intent.getStringExtra("store_name");
		String store_parent=intent.getStringExtra("place_parent");
		ArrayList<String> category=intent.getStringArrayListExtra("category");
		String store_desc=intent.getStringExtra("description");
		String building=intent.getStringExtra("building");
		String street=intent.getStringExtra("street");
		String landmark=intent.getStringExtra("landmark");
		String area=intent.getStringExtra("area");
		String pincode=intent.getStringExtra("pincode");
		String city=intent.getStringExtra("city");
		String latitude=intent.getStringExtra("latitude");
		String longitude=intent.getStringExtra("longitude");
		
		String landline=intent.getStringExtra("landline");
		String landline2=intent.getStringExtra("landline2");
		String landline3=intent.getStringExtra("landline3");
		
		
		
		String mobile=intent.getStringExtra("mobile");
		String mobile2=intent.getStringExtra("mobile2");
		String mobile3=intent.getStringExtra("mobile3");
		
		
		String fax=intent.getStringExtra("fax");
		String tollfree=intent.getStringExtra("tollfree");
		String store_email=intent.getStringExtra("store_email");
		String store_web=intent.getStringExtra("store_Web");
		String store_fb=intent.getStringExtra("store_fb");
		String store_twitter=intent.getStringExtra("store_twitter");

		String userfile=intent.getStringExtra("userfile");
		String filename=intent.getStringExtra("filename");
		String filetype=intent.getStringExtra("filetype");
		/*String business_id=intent.getStringExtra("business_id");*/
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		String store_id=intent.getStringExtra("place_id");
		String gallery_image=intent.getStringExtra("gallery_image");
		String place_status=intent.getStringExtra("place_status");
		
		int removeStoreLogo=intent.getIntExtra("removeStoreLogo", 0);
		
		boolean isUpdating=intent.getBooleanExtra("isUpdating",false);
		boolean isShopLocalIcon=intent.getBooleanExtra("isShopLocalIcon",false);
		
		String area_id=intent.getStringExtra("area_id");


		BufferedReader bufferedReader = null;
		String status = "";





		try
		{
			/*DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);*/


			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to post data.
			/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
			HttpPost httpPost=new HttpPost(URL);

			//Adding header.
			/*httpPost.addHeader("X-API-KEY", "Fool");*/
			if(isUpdating)
			{
				httpPost.addHeader("X-HTTP-Method-Override", "PUT");
			}
			httpPost.addHeader(API_HEADER, API_HEADER_VALUE);

			Log.i("URL", "URL "+URL);


			JSONArray jsonArray=new JSONArray(category);
			JSONObject json = new JSONObject();
			
			
			json.put("name", store_name);
			json.put("place_parent", store_parent);
			json.put("category", jsonArray);
			json.put("description", store_desc);
			json.put("building", building);
			json.put("street", street);

			json.put("landmark", landmark);
			json.put("area", area);
			json.put("area_id", area_id);

			/*ArrayList<String> s=new ArrayList<String>();

			JSONArray jsonArray=new JSONArray(s);

			jsonArray.toString();*/

			json.put("pincode", pincode);
			json.put("city", city);

			json.put("user_id", user_id	);
			json.put("auth_id", auth_id);
			
			json.put("place_status", place_status);

			if(isUpdating)
			{
				json.put("place_id", store_id);
			}
			else
			{
				json.put("place_id", "");				
			}

				json.put("latitude", latitude);
				json.put("longitude", longitude	);
			
			json.put("landline", landline);
			json.put("landline2", landline2);
			json.put("landline3", landline3);
			
			if(mobile!=null)
			{
				json.put("mobile", mobile);
			}
			else 
			{
				mobile="";
				json.put("mobile", mobile);
			}
			
			if(mobile2!=null)
			{
				json.put("mobile2", mobile2);
			}
			else 
			{
				mobile2="";
				json.put("mobile2", mobile2);
			}
			
			if(mobile3!=null)
			{
				json.put("mobile3", mobile3);
			}
			else 
			{
				mobile3="";
				json.put("mobile3", mobile3);
			}
			
			
/*			json.put("fax", fax	);
			json.put("tollfree",tollfree);
*/			
			json.put("email",store_email);
			json.put("website",store_web);
			json.put("facebook_url",store_fb);
			json.put("twitter_url",store_twitter);

			if(userfile!=null && isShopLocalIcon==false)
			{
				//json.put("filename",filename);
				json.put("filetype",filetype);
				if(removeStoreLogo!=1)
				{
				json.put("userfile",userfile);
				}
			}
			if(isShopLocalIcon==true)
			{
				if(removeStoreLogo!=1)
				{
				json.put("gallery_image",gallery_image);
				}
			}
			if(removeStoreLogo==1)
			{
				json.put("userfile","");
			}
			/*		json.put("business_id", business_id);*/

			/*Log.i("Response", "Response JSON Update Store"+""+json.toString()+"");*/
			//System.out.println("Response JSON Update Store"+""+json.toString()+"");

			Log.i("Response", "Response PLACE store_id"+""+store_id+"");
			Log.i("Response", "Response PLACE store_name"+""+store_name+"");
			Log.i("Response", "Response PLACE store_parent "+""+store_parent+"");
			Log.i("Response", "Response PLACE category"+""+jsonArray.toString()+"");
			Log.i("Response", "Response PLACE description"+""+store_desc+"");
			Log.i("Response", "Response PLACE building"+""+building+"");
			Log.i("Response", "Response PLACE street"+""+street+"");

			Log.i("Response", "Response PLACE landmark"+""+landmark+"");
			Log.i("Response", "Response PLACE area"+""+area+"");

			Log.i("Response", "Response PLACE pincode"+""+pincode+"");
			Log.i("Response", "Response PLACE city"+""+city+"");

			Log.i("Response", "Response PLACE user_id"+""+user_id+"");
			Log.i("Response", "Response PLACE auth_id"+""+auth_id+"");


			Log.i("Response", "Response PLACE store_id"+""+store_id+"");
			Log.i("Response", "Response PLACE latitude"+""+latitude+"");


			Log.i("Response", "Response PLACE longitude"+""+longitude+"");
			Log.i("Response", "Response PLACE landline"+""+landline+"");

			
			Log.i("Response", "Response PLACE landline"+""+landline+"");
			Log.i("Response", "Response PLACE landline2"+""+landline2+"");
			Log.i("Response", "Response PLACE landline3"+""+landline3+"");

			Log.i("Response", "Response PLACE mobile"+""+mobile+"");
			Log.i("Response", "Response PLACE mobile2"+""+mobile2+"");
			Log.i("Response", "Response PLACE mobile3"+""+mobile3+"");
			
//			Log.i("Response", "Response PLACE fax"+""+fax+"");


//			Log.i("Response", "Response PLACE tollfree"+""+tollfree+"");
			Log.i("Response", "Response PLACE email"+""+store_email+"");
			Log.i("Response", "Response PLACE website"+""+store_web+"");

			if(userfile!=null)
			{

				Log.i("Response", "Response PLACE filename"+""+filename+"");

				Log.i("Response", "Response PLACE filetype"+""+filetype+"");
				Log.i("Response", "Response PLACE userfile"+""+userfile+"");
			}



			/*	Log.i("URL", "URL "+""+URL+"");*/

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
			status=stringBuffer.toString();

			String store_status;
			String store_msg = "";
			String store_data="";
			store_status=getStatus(status);
			if(store_status.equalsIgnoreCase("true"))
			{
				store_msg=getMessage(status);
				store_data=getData(status);
				Bundle b=new Bundle();
				b.putString("store_data", store_data);
				b.putString("store_msg", store_msg);
				b.putString("store_status", store_status);
				updateStore.send(0, b);
			}
			else
			{
				store_msg=getMessage(status);
				Bundle b=new Bundle();
				b.putString("store_data", store_data);
				b.putString("store_msg", store_msg);
				b.putString("store_status", store_status);
				b.putString("error_code", getErrorCode(status));
				updateStore.send(0, b);
			}
			writeToFile(json.toString());

			


			//writeToFile(json.toString());
			/*			Log.i("FILE ","FILE : "+readFromFile());*/

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			String store_data="";
			b.putString("store_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("store_data", store_data);
			b.putString("store_status", "error");
			b.putString("error_code", "-1");
			updateStore.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			String store_data="";
			b.putString("store_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("store_data", store_data);
			b.putString("store_status", "error");
			b.putString("error_code", "-1");
			updateStore.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			String store_data="";
			b.putString("store_msg", getResources().getString(R.string.exceptionMessage));
			b.putString("store_data", store_data);
			b.putString("store_status", "error");
			b.putString("error_code", "-1");
			updateStore.send(0, b);
		}


	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}

	private void writeToFile(String data) {
		/*try {
	    	FileOutputStream foutput=new FileOutputStream(file)

	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("json.txt", Context.MODE_WORLD_WRITEABLE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } */
		try {
			File myFile = new File("/sdcard/json.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}


	/*	private String readFromFile() {

	    String ret = "";

	    try {
	        InputStream inputStream = openFileInput("/sdcard/json.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}*/


	String getMessage(String status)
	{
		String message="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			message=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return message;
	}

	String getData(String status)
	{
		String data="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			JSONObject getDataObject=jsonobject.getJSONObject("data");
			data=getDataObject.getString("place_id");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return data;
	}

}
