package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class AddImageBroadCastService extends IntentService{

	ResultReceiver addImageBroadCast;
	public AddImageBroadCastService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public AddImageBroadCastService()
	{
		super("AddImageBroadCast");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		addImageBroadCast=intent.getParcelableExtra("addImageBroadCast");
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		boolean isOffer=intent.getBooleanExtra("isOffer",false);
		String USER_ID=intent.getStringExtra("user_id");
		String AUTH_ID=intent.getStringExtra("auth_id");
		ArrayList<String> STORE_ID=intent.getStringArrayListExtra("store_id");
		String TITLE=intent.getStringExtra("title");
		String IMAGE_URL=intent.getStringExtra("url");
		String TAGS=intent.getStringExtra("tags");
		String TYPE=intent.getStringExtra("type");
		String STATE=intent.getStringExtra("state");
		String FORMAT=intent.getStringExtra("format");
	/*	String DATA=intent.getStringExtra("data");*/
		String IS_OFFERED=intent.getStringExtra("is_offered");
		String OFFER_DATE=intent.getStringExtra("offer_date_time");
		String OFFER_TIME=intent.getStringExtra("offer_time");
		
		String FILE_NAME=intent.getStringExtra("filename");
		String FILE_TYPE=intent.getStringExtra("filetype");
		String USER_FILE=intent.getStringExtra("userfile");

		BufferedReader bufferedReader = null;
		String status = "";

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient();
		//Setting URL to post data.
		/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/business_api/business");*/

		HttpPost httpPost=new HttpPost(URL);

		//Adding header.
		/*httpPost.addHeader("X-API-KEY", "Fool");*/
		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);


		
		
		try
		{
			Log.i("Response ", "Response URL "+URL);
			Log.i("Response ", "Response USER_ID "+USER_ID);
			Log.i("Response ", "Response AUTH_ID "+AUTH_ID);
			Log.i("Response ", "Response STORE_ID "+STORE_ID);
			Log.i("Response ", "Response TITLE "+TITLE);
			Log.i("Response ", "Response IMAGE_URL "+IMAGE_URL);
			Log.i("Response ", "Response TAGS "+TAGS);
			Log.i("Response ", "Response TYPE "+TYPE);
			Log.i("Response ", "Response STATE "+STATE);
			Log.i("Response ", "Response IS_OFFERED "+IS_OFFERED);
			Log.i("Response ", "Response OFFER_DATE "+OFFER_DATE);
			Log.i("Response ", "Response OFFER_TIME "+OFFER_TIME);
			Log.i("Response ", "Response FILE_NAME "+FILE_NAME);
			Log.i("Response ", "Response FILE_TYPE "+FILE_TYPE);
			Log.i("Response ", "Response USER_FILE "+USER_FILE);	
			
			
			
			JSONObject json = new JSONObject();
			json.put("store_id", STORE_ID);
			json.put("title", TITLE);

			json.put("url", IMAGE_URL);
			if(!IMAGE_URL.equals(""))
			{
				json.put("body", "see the website");
				Log.i("URL", "URL POST " +"see the website");
			}
			json.put("filename", FILE_NAME);
			json.put("filetype", FILE_TYPE);
			json.put("userfile", USER_FILE);
			
			
			
			json.put("tags", TAGS);
			json.put("type", TYPE);
			json.put("state", STATE);

			json.put("format", FORMAT);
/*			json.put("data", DATA);*/
			json.put("is_offered", IS_OFFERED);

			json.put("offer_date_time", OFFER_DATE+OFFER_TIME);
/*			json.put("offer_time", OFFER_TIME);*/
			/*json.put("offer_time", OFFER_TIME);*/


			json.put("user_id", USER_ID);
			json.put("auth_id", AUTH_ID	);
			





			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response "+stringBuffer.toString());
			status=stringBuffer.toString();


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		try
		{
			String broadcast_status;
			broadcast_status=getStatus(status);
			Bundle b=new Bundle();
			
			String id=getId(status);

			b.putString("broadcast_image_status", broadcast_status);
			b.putString("broadcast_image_id", id);
			addImageBroadCast.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("status");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}
	
	String getId(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}


}
