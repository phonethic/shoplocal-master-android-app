package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;




import com.phonethics.shoplocal.DBUtil;
import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class AddCustomerProfile extends IntentService {

	ResultReceiver profile;
	DBUtil dbUtil;
	public AddCustomerProfile(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public AddCustomerProfile(){
		super("addCustomerProfile");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		dbUtil=new DBUtil(getApplicationContext());
		profile=intent.getParcelableExtra("addCustomerProfile");

		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String name=intent.getStringExtra("name");
		String email=intent.getStringExtra("email");
		String dob=intent.getStringExtra("dob");
		String gender=intent.getStringExtra("gender");

		String city=intent.getStringExtra("city");
		String state=intent.getStringExtra("state");

		String filename=intent.getStringExtra("filename");
		String filetype=intent.getStringExtra("filetype");
		String userfile=intent.getStringExtra("userfile");

		String image_url=intent.getStringExtra("image_url");
		String facebook_user_id=intent.getStringExtra("facebook_user_id");
		String facebook_access_token=intent.getStringExtra("facebook_access_token");

		ArrayList<String> allIds = intent.getStringArrayListExtra("allIds");
		ArrayList<String>  allDates = intent.getStringArrayListExtra("allDates");

		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");

		String type=intent.getStringExtra("type");
		
		String active_area="";
		active_area=dbUtil.getActiveAreaID();
		
		


		BufferedReader bufferedReader = null;
		String status = "";


		try
		{
			/*DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);*/


			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to post data.
			/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
			HttpPost httpPost=new HttpPost(URL);

			//Adding header.
			/*httpPost.addHeader("X-API-KEY", "Fool");*/

			httpPost.addHeader("X-HTTP-Method-Override", "PUT");

			httpPost.addHeader(API_HEADER, API_HEADER_VALUE);

			Log.i("URL", "URL "+URL);
			Log.i("URL ", "URL NAME : "+name);
			Log.i("URL ", "URL EMAIL : "+email);
			Log.i("URL ", "URL dob : "+dob);
			Log.i("URL ", "URL gender : "+gender);
			Log.i("URL ", "URL filename : "+filename);
			Log.i("URL ", "URL filetype : "+filetype);
			Log.i("URL ", "URL userfile : "+userfile);




			JSONObject json = new JSONObject();
			//			json.put("name", name);
			//
			//			json.put("email",email);
			//			json.put("dob", dob);
			//			json.put("gender", gender);
			//			
			//			json.put("city", city);
			//			json.put("state", state);
			//
			//			json.put("filename",filename);
			//			json.put("filetype",filetype);
			//			json.put("userfile",userfile);
			//			
			//			json.put("user_id", user_id	);
			//			json.put("auth_id", auth_id);

			if(type.equalsIgnoreCase("facebook")){

				json.put("name", name);

				json.put("email",email);
				//json.put("dob", dob);
				json.put("gender", gender);

				json.put("city", city);
				json.put("state", state);

				json.put("image_url",image_url);
				json.put("facebook_user_id",facebook_user_id);
				json.put("facebook_access_token",facebook_access_token);

				json.put("user_id", user_id	);
				json.put("auth_id", auth_id);
				
				//active Area
				if(active_area.length()!=0)
				{
					json.put("active_area",active_area);
				}
						
				Log.d("DATES SIZE","DATES SIZE" + allDates.size());

				if(allDates.size()!=0 && allIds.size()!=0){

					JSONArray dates = new JSONArray();

					for(int i=0;i<allDates.size();i++){

						JSONObject obj = new JSONObject();

						obj.put("date_type", allIds.get(i));
						obj.put("date", allDates.get(i));

						dates.put(obj);
					}

					json.put("dates", dates);
				}
				else{

					json.put("dates", "");
				}


			}

			else{

				json.put("name", name);

				json.put("email",email);
				//json.put("dob", dob);
				json.put("gender", gender);

				json.put("city", city);
				json.put("state", state);

				if(filename!=null && userfile!=null)
				{
					json.put("filename",filename);
					json.put("filetype",filetype);
					json.put("userfile",userfile);
				}

				json.put("user_id", user_id	);
				json.put("auth_id", auth_id);

				if(allDates.size()!=0  && allIds.size()!=0){

					JSONArray dates = new JSONArray();

					for(int i=0;i<allDates.size();i++){

						JSONObject obj = new JSONObject();

						obj.put("date_type", allIds.get(i));
						obj.put("date", allDates.get(i));

						dates.put(obj);
					}

					json.put("dates", dates);
				}
				else{

					json.put("dates", "");
				}


			}


			/*	Log.i("URL", "URL "+""+URL+"");*/

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();

			Log.i("Response : ", "Service Response JSON "+json.toString());
			Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
			status=stringBuffer.toString();

			String prof_status;
			String prof_msg = "";
			prof_status=getStatus(status);
			Bundle b=new Bundle();
			if(prof_status.equalsIgnoreCase("success"))
			{
				prof_msg=getMessage(status);
			}
			else
			{
				prof_msg=getMessage(status);
				b.putString("error_code", getErrorCode(status));
			}


		
			b.putString("prof_msg", prof_msg);
			b.putString("prof_status", prof_status);
			profile.send(0, b);


			writeToFile(json.toString());


			/*			Log.i("FILE ","FILE : "+readFromFile());*/

		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("prof_status", "error");
			b.putString("error_code", "-1");
			profile.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("prof_status", "error");
			b.putString("error_code", "-1");
			profile.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("prof_msg",getResources().getString(R.string.exception));
			b.putString("prof_status", "error");
			b.putString("error_code", "-1");
			profile.send(0, b);
		}

	}
	
	String getErrorCode(String status)
	{
		String error_code="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			error_code=jsonobject.getString("code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error_code="-1";
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			error_code="-1";
		}
		return error_code;
	}



	private void writeToFile(String data) {
		/*try {
	    	FileOutputStream foutput=new FileOutputStream(file)

	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("json.txt", Context.MODE_WORLD_WRITEABLE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } */
		try {
			File myFile = new File("/sdcard/jsonProfile.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}

	String getMessage(String status)
	{
		String userid="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userid=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userid;
	}


}
