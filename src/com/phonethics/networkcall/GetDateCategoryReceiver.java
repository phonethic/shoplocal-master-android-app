package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetDateCategoryReceiver extends ResultReceiver {

	GetDate rec;
	public GetDateCategoryReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(rec!=null)
		{
			rec.onGetDateReceive(resultCode, resultData);
		}
	}
	
	public interface GetDate{
		
		public void onGetDateReceive(int resultCode, Bundle resultData);
	}

	public void setReciver(GetDate rec)
	{
		this.rec=rec;
	}

	
}
