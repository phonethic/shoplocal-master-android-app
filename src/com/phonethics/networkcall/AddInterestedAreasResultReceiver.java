package com.phonethics.networkcall;

import com.phonethics.networkcall.AddCustomerProfileReceiver.CustomerProfileReceiver;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AddInterestedAreasResultReceiver extends ResultReceiver {
	
	AddInterest result;

	public AddInterestedAreasResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(result!=null)
		{
			result.onReceiveInterestedAreas(resultCode, resultData);
		}
	}

	
	public interface AddInterest
	{
		public void onReceiveInterestedAreas(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(AddInterest result)
	{
		this.result = result;
	}
	
}
