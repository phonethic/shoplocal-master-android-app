package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

public class AddInterestedAreaService extends IntentService{
	
	ResultReceiver addInterestedAreas;
	
	public AddInterestedAreaService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public AddInterestedAreaService(){
		super("addCustomerProfile");
	}

	

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		addInterestedAreas = intent.getParcelableExtra("addInterestedAreas");
		
		String URL=intent.getStringExtra("URL");
		
		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		ArrayList<String> areaIds = intent.getStringArrayListExtra("interested_areaids");
		ArrayList<String>  areaNames = intent.getStringArrayListExtra("interested_areanames");


		BufferedReader bufferedReader = null;
		String status = "";

		try {
			
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to post data.
			/*	HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/store_api/store");*/
			HttpPost httpPost=new HttpPost(URL);

			//Adding header.
			httpPost.addHeader("X-API-KEY", "Fool");

//			httpPost.addHeader("X-HTTP-Method-Override", "PUT");
			
			Log.i("URL", "URL "+URL);
			
			JSONObject json = new JSONObject();
			
			json.put("user_id", user_id);
			json.put("auth_id", auth_id);
			
			
		
			JSONArray areas = new JSONArray();
			
			for(int i=0;i<areaIds.size();i++){
				
					areas.put(areaIds.get(i));
			}
			
			json.put("areas", areas);
			
			
			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);


			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();

			Log.i("Response : ", "Service Response JSON "+json.toString());
			Log.i("Response : ", "Service Response Store "+stringBuffer.toString());
			status=stringBuffer.toString();

			String prof_status;
			String prof_msg = "";
			prof_status=getStatus(status);
			if(prof_status.equalsIgnoreCase("true"))
			{
				prof_msg=getMessage(status);
			}


			Bundle b=new Bundle();
			b.putString("prof_msg", prof_msg);
			b.putString("prof_status", prof_status);
			addInterestedAreas.send(0, b);


			writeToFile(json.toString());
			
		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("prof_status", "notset");
			addInterestedAreas.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("prof_msg", "Connection Timed out");
			b.putString("prof_status", "notset");
			addInterestedAreas.send(0, b);
		}
		catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			Bundle b=new Bundle();
			b.putString("prof_msg", "Try again");
			b.putString("prof_status", "notset");
			addInterestedAreas.send(0, b);
		}
	}

	private void writeToFile(String data) {
		/*try {
	    	FileOutputStream foutput=new FileOutputStream(file)

	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("json.txt", Context.MODE_WORLD_WRITEABLE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } */
		try {
			File myFile = new File("/sdcard/jsonArea.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
			myOutWriter.append(data);
			myOutWriter.close();
			fOut.close();


		} 
		catch (Exception e) 
		{
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}
	
	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}
	
	String getMessage(String status)
	{
		String userid="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userid=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userid;
	}


}
