package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

public class GetDateCategory extends IntentService{
	
	ResultReceiver getDateCategory;
	
	String getDateStatus, getDateData;
	
	ArrayList<String> dataId= new ArrayList<String>();
	ArrayList<String> dataName= new ArrayList<String>();
	
	public GetDateCategory() {
		super("GetDateCategory");
		// TODO Auto-generated constructor stub
		
		
	}


	public GetDateCategory(String name) {
		super(name);
		// TODO Auto-generated constructor stub
		
		
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		getDateCategory = intent.getParcelableExtra("getDateCategory");
		
		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");


		String user_id=intent.getStringExtra("user_id");
		String auth_id=intent.getStringExtra("auth_id");
		
		BufferedReader bufferedReader = null;
		String status = "";
		
		try
		{
			HttpParams httpParams = new BasicHttpParams();

			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 30000;
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient(httpParams);

			//Setting URL to Get data.
			HttpGet httpGet=new HttpGet(URL);

			//Adding header.
			httpGet.addHeader(API_HEADER, API_HEADER_VALUE);
		/*	httpGet.addHeader("user_id",user_id);
			httpGet.addHeader("auth_id",auth_id);*/

			Log.i("URL", "URL Date : "+URL);
			Log.i("URL", "URL Date : "+user_id);
			Log.i("URL", "URL Date : "+auth_id);

			HttpResponse response=httpClient.execute(httpGet);

			bufferedReader=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer=new StringBuffer("");
			String line="";
			String LineSeparator=System.getProperty("line.separator");

			while((line=bufferedReader.readLine())!=null)
			{
				stringBuffer.append(line+LineSeparator);
			}
			bufferedReader.close();
			Log.i("Response : ", "Service Date: "+stringBuffer.toString());
			status=stringBuffer.toString();
			
			JSONObject jsonobject=new JSONObject(status);

			getDateStatus=jsonobject.getString("success");
			if(getDateStatus.equalsIgnoreCase("true"))
			{
				
				
				 JSONObject user_profile_data=jsonobject.getJSONObject("data");
				 
				 JSONArray dateCategories = user_profile_data.getJSONArray("date_categories");
				 Log.d("ids"," Date: "  +dateCategories.length());
				 for(int i=0;i<dateCategories.length();i++){
					 
					 JSONObject obj = dateCategories.getJSONObject(i);
					 
					 dataId.add(obj.getString("id"));
					 dataName.add(obj.getString("name"));
					 
					 Log.d("ids"," Date: "  + obj.getString("id"));
					 
				 }
				 
//				 ID=user_profile_data.getString("id");
//				 NAME=user_profile_data.getString("name");
//				 MOBILE=user_profile_data.getString("mobile");
//				 EMAIL=user_profile_data.getString("email");
//				 DOB=user_profile_data.getString("dob");
//				 GENDER=user_profile_data.getString("gender");
//				 CITY=user_profile_data.getString("city");
//				 STATE=user_profile_data.getString("state");
//				 IMAGE_URL=user_profile_data.getString("image_url");
				
			}
			else if(getDateStatus.equalsIgnoreCase("false"))
			{
				getDateData=jsonobject.getString("message");
			}
					
			Bundle b=new Bundle();
			b.putString("date_status", getDateStatus);
			//b.putString("prof_sg", getDateData);
			
			b.putStringArrayList("dataIds", dataId);
			b.putStringArrayList("dataName", dataName);
			
			getDateCategory.send(0, b);


		}catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("Socket Time out", "Socket Time out exception occured");
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
			b.putString("date_status", "error");
			getDateCategory.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("Socket Time out", getResources().getString(R.string.connectionTimedOut));
			Bundle b=new Bundle();
			b.putString("date_status", "error");
			b.putString("prof_msg", getResources().getString(R.string.connectionTimedOut));
		
			getDateCategory.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bundle b=new Bundle();
			b.putString("prof_msg", getResources().getString(R.string.exception));
			b.putString("date_status", "error");
			getDateCategory.send(0, b);
		}
		
		
	}

	
}
