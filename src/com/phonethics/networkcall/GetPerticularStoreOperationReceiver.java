package com.phonethics.networkcall;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class GetPerticularStoreOperationReceiver extends ResultReceiver{

	StoreOperation store_operation;
	public GetPerticularStoreOperationReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(store_operation!=null)
		{
			store_operation.onReceiveStoreOperation(resultCode, resultData);
		}
	}
	
	public interface StoreOperation
	{
		public void onReceiveStoreOperation(int resultCode, Bundle resultData);
	}
	
	public void setReceiver(StoreOperation receiver)
	{
		this.store_operation=receiver;
	}

}
