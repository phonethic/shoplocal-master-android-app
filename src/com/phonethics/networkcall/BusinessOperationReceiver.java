package com.phonethics.networkcall;



import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class BusinessOperationReceiver extends ResultReceiver {

	private OperationReceiver bReceiver;
	public BusinessOperationReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		if(bReceiver!=null)
		{
			bReceiver.onReciveOperationResult(resultCode, resultData);
		}
	}
	
	public interface OperationReceiver
	{
		public void onReciveOperationResult(int reulstCode,Bundle resultData);
		
	}
	
	public void setReceiver(OperationReceiver receiver)
	{
		bReceiver=receiver;
	}

}
