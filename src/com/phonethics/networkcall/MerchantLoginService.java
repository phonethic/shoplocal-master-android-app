package com.phonethics.networkcall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.phonethics.shoplocal.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class MerchantLoginService extends IntentService{

	ResultReceiver LoginResult;
	public MerchantLoginService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	public MerchantLoginService()
	{
		super("MerchantLoginTag");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		LoginResult=intent.getParcelableExtra("merchantLogin");


		String URL=intent.getStringExtra("URL");
		String API_HEADER=intent.getStringExtra("api_header");
		String API_HEADER_VALUE=intent.getStringExtra("api_header_value");

		String contact_no=intent.getStringExtra("contact_no");
		String password=intent.getStringExtra("password");
		boolean isCustomer=intent.getBooleanExtra("isCustomer", false);
		
		String fbUserid=intent.getStringExtra("facebook_user_id");
		String fbAccessToken=intent.getStringExtra("facebook_access_token");
		boolean fromFb = intent.getBooleanExtra("fromFb",false);
		/*
		 * Test
		 */

		BufferedReader bufferedReader = null;
		String status="";

		HttpParams httpParams = new BasicHttpParams();

		int timeoutConnection = 30000;
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 30000;
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

		//Creating HttpClient.
		HttpClient httpClient=new DefaultHttpClient(httpParams);
		//Setting URL to post data.
		/*HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/login/validate_credentials");*/
		HttpPost httpPost=new HttpPost(URL);
		//Adding header.
		/*httpPost.addHeader("X-API-KEY", "Fool");*/
		httpPost.addHeader(API_HEADER, API_HEADER_VALUE);
		/*if(isCustomer)
		{
			httpPost.addHeader("X-HTTP-Method-Override", "PUT");
		}
*/

		try
		{
			JSONObject json = new JSONObject();
			
			if(fromFb){
				
				json.put("facebook_user_id", 	fbUserid);
				json.put("facebook_access_token", fbAccessToken);

			}
			else{
			
				json.put("mobile", contact_no);
				json.put("password", password);

			}
			
			Log.i("Response", "Response Login Service "+ URL);
			Log.i("Response", "Response Login Service "+"\""+contact_no+"\"");
			Log.i("Response", "Response Login Service "+"\""+password+"\"");

			StringEntity se = new StringEntity( json.toString());  

			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);

			/*List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("mobile", contact_no));
				nameValuePairs.add(new BasicNameValuePair("email", contact_no));


				UrlEncodedFormEntity urlen=new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);


				httpPost.setEntity(urlen);*/




			// Execute HTTP Post Request
			HttpResponse response = httpClient.execute(httpPost);
			/* Log.i("Response ", " : "+response.toString());*/
			bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			StringBuffer stringBuffer = new StringBuffer("");
			String line = "";
			String LineSeparator = System.getProperty("line.separator");
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line + LineSeparator); 

			}
			bufferedReader.close();
			Log.i("Response : ", " Response Login Service "+stringBuffer.toString());
			status=stringBuffer.toString();

			String login_status=getStatus(status);
			String login_id="";
			String login_authCode = "";
			if(login_status.equalsIgnoreCase("true"))
			{
				login_id=getId(status);
				login_authCode=getAuthCode(status);
				Bundle b=new Bundle();
				b.putString("loginid", login_id);
				b.putString("loginstatus", login_status);
				b.putString("login_authCode", login_authCode);
				LoginResult.send(0, b);
			}
			else if(login_status.equalsIgnoreCase("false"))
			{
				/*	login_id=getId(status);*/
				login_authCode=getMessage(status);
				Bundle b=new Bundle();
				b.putString("loginid", login_id);
				b.putString("loginstatus", login_status);
				b.putString("login_authCode", login_authCode);
				LoginResult.send(0, b);
			}

		}
		catch(ConnectTimeoutException c)
		{
			c.printStackTrace();
			Log.i("SOcket", "Socket exception");

			String login_status="false";
			String login_id="";
			String login_authCode = getResources().getString(R.string.connectionTimedOut);

			Bundle b=new Bundle();
			b.putString("loginid", login_id);
			b.putString("loginstatus", login_status);
			b.putString("login_authCode", login_authCode);
			LoginResult.send(0, b);
		}
		catch(SocketTimeoutException st)
		{
			st.printStackTrace();
			Log.i("SOcket", "Socket exception");

			String login_status="false";
			String login_id="";
			String login_authCode = getResources().getString(R.string.connectionTimedOut);

			Bundle b=new Bundle();
			b.putString("loginid", login_id);
			b.putString("loginstatus", login_status);
			b.putString("login_authCode", login_authCode);
			LoginResult.send(0, b);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			String login_status="false";
			String login_id="";
			String login_authCode = getResources().getString(R.string.connectionTimedOut);

			Bundle b=new Bundle();
			b.putString("loginid", login_id);
			b.putString("loginstatus", login_status);
			b.putString("login_authCode", login_authCode);
			LoginResult.send(0, b);
		}

		


		/*new LoginMerchant(contact_no, password).execute("Login Merchant");*/

	}


	String getStatus(String status)
	{
		String userstatus="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			userstatus=jsonobject.getString("success");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userstatus;
	}

	String getId(String status)
	{
		String userid="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			JSONObject jmsg=jsonobject.getJSONObject("data");
			userid=jmsg.getString("id");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userid;
	}

	String getAuthCode(String status)
	{
		String authCode="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			JSONObject jmsg=jsonobject.getJSONObject("data");
			authCode=jmsg.getString("auth_code");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return authCode;
	}
	String getMessage(String status)
	{
		String authCode="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			authCode=jsonobject.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return authCode;
	}
	
	String getData(String status)
	{
		String authCode="";
		try {
			JSONObject jsonobject=new JSONObject(status);
			authCode=jsonobject.getString("data");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return authCode;
	}



	//Loging in Merchent to the System.
	class LoginMerchant extends AsyncTask<String, Integer, String>
	{
		BufferedReader bufferedReader = null;
		private String status;
		String contact_no;
		String password;

		public LoginMerchant(String contact_no,String password) {
			// TODO Auto-generated constructor stub
			this.contact_no=contact_no;
			this.password=password;
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub


			//Creating HttpClient.
			HttpClient httpClient=new DefaultHttpClient();
			//Setting URL to post data.
			HttpPost httpPost=new HttpPost("http://192.168.254.37/hyperlocal/api/login/validate_credentials");
			//Adding header.
			httpPost.addHeader("X-API-KEY", "Fool");


			try
			{
				JSONObject json = new JSONObject();
				json.put("mobile", contact_no);
				json.put("password", password);

				Log.i("Response", "Response "+"\""+contact_no+"\"");
				Log.i("Response", "Response "+"\""+password+"\"");

				StringEntity se = new StringEntity( json.toString());  

				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpPost.setEntity(se);

				/*List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs.add(new BasicNameValuePair("mobile", contact_no));
					nameValuePairs.add(new BasicNameValuePair("email", contact_no));


					UrlEncodedFormEntity urlen=new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8);


					httpPost.setEntity(urlen);*/




				// Execute HTTP Post Request
				HttpResponse response = httpClient.execute(httpPost);
				/* Log.i("Response ", " : "+response.toString());*/
				bufferedReader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				StringBuffer stringBuffer = new StringBuffer("");
				String line = "";
				String LineSeparator = System.getProperty("line.separator");
				while ((line = bufferedReader.readLine()) != null) {
					stringBuffer.append(line + LineSeparator); 

				}
				bufferedReader.close();
				Log.i("Response : ", " Response "+stringBuffer.toString());
				status=stringBuffer.toString();


			}catch(Exception ex)
			{
				ex.printStackTrace();
			}



			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			String login_status=getStatus(status);
			String login_id=getId(status);
			String login_authCode=getAuthCode(status);

			Bundle b=new Bundle();
			b.putString("loginid", login_id);
			b.putString("loginstatus", login_status);
			b.putString("login_authCode", login_authCode);
			LoginResult.send(0, b);
		}


		String getStatus(String status)
		{
			String userstatus="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				userstatus=jsonobject.getString("status");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userstatus;
		}

		String getId(String status)
		{
			String userid="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject jmsg=jsonobject.getJSONObject("message");
				userid=jmsg.getString("id");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return userid;
		}

		String getAuthCode(String status)
		{
			String authCode="";
			try {
				JSONObject jsonobject=new JSONObject(status);
				JSONObject jmsg=jsonobject.getJSONObject("message");
				authCode=jmsg.getString("auth_code");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			return authCode;
		}

	}


}
