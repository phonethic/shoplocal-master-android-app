package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonethics.model.EntryItem;
import com.phonethics.model.Item;
import com.phonethics.model.SectionItem;
import com.phonethics.shoplocal.R;

public class DrawerAdapter extends ArrayAdapter<Item>
{
	ArrayList<Item> item;
	Activity context;
	LayoutInflater inflator;
	Typeface tf;
	public DrawerAdapter(Activity context, int resource, ArrayList<Item> item) {
		super(context, resource, item);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.item=item;
		inflator=context.getLayoutInflater();
		tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return item.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		try
		{
			final Item i = item.get(position);
			if(i!=null)
			{
				if(i.ItemType()==1)
				{
					convertView=inflator.inflate(R.layout.drawer_search_layout,null);
					final EditText editDrawerSearch=(EditText)convertView.findViewById(R.id.editDrawerSearch);
					editDrawerSearch.setTypeface(tf);
				}
				else if(i.ItemType()==2)
				{
					SectionItem si=(SectionItem)i;
					convertView=inflator.inflate(R.layout.drawer_item_section,null);

					convertView.setOnClickListener(null);
					convertView.setOnLongClickListener(null);
					convertView.setLongClickable(false);

					final TextView sectionView=(TextView)convertView.findViewById(R.id.list_item_section_text);
					sectionView.setTypeface(tf);
					sectionView.setText(si.getTitle());
				}
				else if(i.ItemType()==3)
				{
					EntryItem ei=(EntryItem)i;
					convertView=inflator.inflate(R.layout.drawer_item_enty, null);
					final TextView title=(TextView)convertView.findViewById(R.id.list_item_title);
					final TextView badge=(TextView)convertView.findViewById(R.id.list_item_badge);
					final ImageView thumb=(ImageView)convertView.findViewById(R.id.list_item_thumb);
					
					title.setTypeface(tf);
					badge.setTypeface(tf);
					badge.setTextColor(Color.RED);
		

					if(title!=null)
					{
						title.setText(ei.getTitle());

					}
					if(thumb!=null)
					{
						thumb.setImageResource(ei.getDrawable_icon());
					}
					if(badge!=null)
					{
						badge.setText(ei.getBadge());

					}
				}
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		return convertView;
	}
	
	static class ViewHolder
	{
		
	}
}//Adapter ends here