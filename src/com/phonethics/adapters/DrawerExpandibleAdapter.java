package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.shoplocal.R;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerExpandibleAdapter extends BaseExpandableListAdapter {

	ArrayList<ExpandibleDrawer> item;
	Activity context;
	LayoutInflater inflator;
	Typeface tf;

	public DrawerExpandibleAdapter(Activity context, int resource, ArrayList<ExpandibleDrawer> item) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.item=item;
		inflator=context.getLayoutInflater();
		tf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean arg2, View convertView,
			ViewGroup arg4) {
		// TODO Auto-generated method stub
		try
		{
			if(convertView==null)
			{
				ChildViewHolder holder=new ChildViewHolder();
				convertView=inflator.inflate(R.layout.drawer_item_enty,null);
				holder.list_item_title=(TextView)convertView.findViewById(R.id.list_item_title);
				holder.list_item_badge=(TextView)convertView.findViewById(R.id.list_item_badge);
				holder.list_item_thumb=(ImageView)convertView.findViewById(R.id.list_item_thumb);
				
				convertView.setTag(holder);
			}
			
			ChildViewHolder hold=(ChildViewHolder)convertView.getTag();
			hold.list_item_title.setText(item.get(groupPosition).getItem_array().get(childPosition));
			hold.list_item_thumb.setImageResource(item.get(groupPosition).getItem_iconID().get(childPosition));
			
			hold.list_item_title.setTypeface(tf);
			hold.list_item_badge.setTypeface(tf);
			hold.list_item_title.setTextColor(Color.WHITE);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return convertView;
	}

	@Override
	public View getGroupView(int position, boolean arg1, View convertView, ViewGroup arg3) {
		// TODO Auto-generated method stub
		try
		{
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=inflator.inflate(R.layout.drawer_item_section,null);
				holder.list_item_section_text=(TextView)convertView.findViewById(R.id.list_item_section_text);
				holder.divider=(ImageView)convertView.findViewById(R.id.divider);
				holder.list_item_section_back=(View)convertView.findViewById(R.id.list_item_section_back);
				holder.list_item_section_back_other=(View)convertView.findViewById(R.id.list_item_section_back_other);
				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder) convertView.getTag();
			hold.divider.setVisibility(View.VISIBLE);
			hold.list_item_section_back.setVisibility(View.VISIBLE);
			hold.list_item_section_back_other.setVisibility(View.VISIBLE);
			if(item.get(position).getCustom_state()==1)
			{
				hold.list_item_section_text.setTextSize(16);
				hold.divider.setVisibility(View.GONE);
//				hold.list_item_section_back.setVisibility(View.VISIBLE);
				hold.list_item_section_back_other.setVisibility(View.GONE);
			}
			else
			{
				hold.list_item_section_text.setTextSize(20);
				hold.divider.setVisibility(View.VISIBLE);
				hold.list_item_section_back.setVisibility(View.GONE);
				hold.list_item_section_back_other.setVisibility(View.VISIBLE);
			}
			hold.list_item_section_text.setText(item.get(position).getHeader());
			hold.list_item_section_text.setTypeface(tf);
			hold.list_item_section_text.setTextColor(Color.WHITE);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return convertView;
	}


	class ViewHolder
	{
		TextView list_item_section_text;
		ImageView divider;
		View list_item_section_back;
		View list_item_section_back_other;
	}

	class ChildViewHolder
	{
		TextView list_item_title;
		ImageView list_item_thumb;
		TextView list_item_badge;
	}


	@Override
	public int getChildrenCount(int position) {
		// TODO Auto-generated method stub
		return item.get(position).getItem_array().size();
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return item.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}



	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

}
