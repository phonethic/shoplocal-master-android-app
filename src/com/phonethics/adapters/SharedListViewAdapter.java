package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.shoplocal.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SharedListViewAdapter extends ArrayAdapter<String>{

	Context c;
	LayoutInflater inflater;
	ArrayList<String> appName; ArrayList<String> packageName;
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	public SharedListViewAdapter(Context context, int resource, LayoutInflater inflater, ArrayList<String> appName, ArrayList<String> packageNames, ArrayList<Drawable> appIcon) {
		super(context, resource);
		c = context;
		this.inflater = inflater; 
		this.appName = appName;
		this.packageName = packageNames;
		this.appIcon = appIcon;
	}
	
	@Override
	public int getCount() {
		return appName.size();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		/*LayoutInflater inflater = (LayoutInflater) c
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
		
		
//		View v = inflater.inflate(R.layout.sharedialoglistviewlayout, null);
//		
//		TextView tv = (TextView) v.findViewById(R.id.textViewAppName);
//		ImageView iv = (ImageView) v.findViewById(R.id.imageViewAppIcon);
//		
//		tv.setText(appName.get(position));
//		iv.setImageDrawable(appIcon.get(position));
		if(convertView==null)
		{
			ViewHolder holder=new ViewHolder();
			convertView=inflater.inflate(R.layout.sharedialoglistviewlayout, null);
			holder.tv = (TextView) convertView.findViewById(R.id.textViewAppName);
			holder.iv = (ImageView) convertView.findViewById(R.id.imageViewAppIcon);
			convertView.setTag(holder);
		}
		ViewHolder hold=(ViewHolder)convertView.getTag();
		
		hold.tv.setText(appName.get(position));
		hold.iv.setImageDrawable(appIcon.get(position));
		
		//iv.setImageDrawable();
		
		return convertView;
	}
	
	static class ViewHolder
	{
		TextView tv;
		ImageView iv;
	}
}
