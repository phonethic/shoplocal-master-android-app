package com.phonethics.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class ExpandibleDrawer implements Parcelable {

	String header;
	String item;
	String icon_url;
	String badge;
	
	int icon;
	
	ArrayList<String> item_array=new ArrayList<String>();
	ArrayList<String> item_iconUrl=new ArrayList<String>();
	ArrayList<Integer> item_iconID=new ArrayList<Integer>();
	
	int opened_state;
	
	int custom_state;
	
	
	
	public int getCustom_state() {
		return custom_state;
	}

	public void setCustom_state(int custom_state) {
		this.custom_state = custom_state;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		item_iconID.add(icon);
	}

	public ArrayList<Integer> getItem_iconID() {
		return item_iconID;
	}

	public void setItem_iconID(ArrayList<Integer> item_iconID) {
		this.item_iconID = item_iconID;
	}

	public int getOpened_state() {
		return opened_state;
	}

	public void setOpened_state(int opened_state) {
		this.opened_state = opened_state;
	}

	public String getIcon_url() {
		return icon_url;
	}

	public void setIcon_url(String icon_url) {
		item_iconUrl.add(icon_url);
	}

	public ArrayList<String> getItem_iconUrl() {
		return item_iconUrl;
	}

	

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		item_array.add(item);
	}
	
	

	public ArrayList<String> getItem_array() {
		return item_array;
	}



	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

}
