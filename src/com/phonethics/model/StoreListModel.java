package com.phonethics.model;

import java.util.ArrayList;

public class StoreListModel {
	
	private String placename="";
	private String id="";
	private String placedesc="";
	public boolean selected;

	
	public StoreListModel()
	{
		
	}
	public StoreListModel(String placename,boolean selected)
	{
		this.placename=placename;
		this.selected=selected;
	}
	public StoreListModel(String placename,String placedesc,boolean selected)
	{
		this.placename=placename;
		this.selected=selected;
		this.placedesc=placedesc;
	}
	public String getPlacename() {
		return placename;
	}
	

	public String getPlacedesc() {
		return placedesc;
	}
	public void setPlacedesc(String placedesc) {
		this.placedesc = placedesc;
	}
	public void setPlacename(String placename) {
		this.placename = placename;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	

}
