package com.phonethics.model;



import android.os.Parcel;
import android.os.Parcelable;

public class NewsFeedModel implements Parcelable {

	private String id="";
	private String name="";
	private String title="";

	
	private String tel_no1="";
	private String tel_no2="";
	private String tel_no3="";
	
	private String mob_no1="";
	private String mob_no2="";
	private String mob_no3="";
	
	private String type="";
	private String post_id="";
	private String description="";

	private String image_url="";
	private String image_url2="";
	private String image_url3="";
	
	private String thumb_url="";
	private String thumb_url2="";
	private String thumb_url3="";
	
	private String date="";
	private String is_offered="";
	
	private String offer_date_time="";
	private String total_like="";
	private String total_share="";
	
	private String total_page="";
	private String total_record="";
	
	private String latitude="";
	private String longitude="";
	
	private String distance="";

	private String area_id="";
	
	private String verified="";
	
	
	
	
	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public String getArea_id() {
		return area_id;
	}

	public void setArea_id(String area_id) {
		this.area_id = area_id;
	}

	public String getPost_id() {
		return post_id;
	}

	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTel_no1() {
		return tel_no1;
	}

	public void setTel_no1(String tel_no1) {
		this.tel_no1 = tel_no1;
	}

	public String getTel_no2() {
		return tel_no2;
	}

	public void setTel_no2(String tel_no2) {
		this.tel_no2 = tel_no2;
	}

	public String getTel_no3() {
		return tel_no3;
	}

	public void setTel_no3(String tel_no3) {
		this.tel_no3 = tel_no3;
	}

	public String getMob_no1() {
		return mob_no1;
	}

	public void setMob_no1(String mob_no1) {
		this.mob_no1 = mob_no1;
	}

	public String getMob_no2() {
		return mob_no2;
	}

	public void setMob_no2(String mob_no2) {
		this.mob_no2 = mob_no2;
	}

	public String getMob_no3() {
		return mob_no3;
	}

	public void setMob_no3(String mob_no3) {
		this.mob_no3 = mob_no3;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getImage_url2() {
		return image_url2;
	}

	public void setImage_url2(String image_url2) {
		this.image_url2 = image_url2;
	}

	public String getImage_url3() {
		return image_url3;
	}

	public void setImage_url3(String image_url3) {
		this.image_url3 = image_url3;
	}

	public String getThumb_url() {
		return thumb_url;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getThumb_url2() {
		return thumb_url2;
	}

	public void setThumb_url2(String thumb_url2) {
		this.thumb_url2 = thumb_url2;
	}

	public String getThumb_url3() {
		return thumb_url3;
	}

	public void setThumb_url3(String thumb_url3) {
		this.thumb_url3 = thumb_url3;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getIs_offered() {
		return is_offered;
	}

	public void setIs_offered(String is_offered) {
		this.is_offered = is_offered;
	}

	public String getOffer_date_time() {
		return offer_date_time;
	}

	public void setOffer_date_time(String offer_date_time) {
		this.offer_date_time = offer_date_time;
	}

	public String getTotal_like() {
		return total_like;
	}

	public void setTotal_like(String total_like) {
		this.total_like = total_like;
	}

	public String getTotal_share() {
		return total_share;
	}

	public void setTotal_share(String total_share) {
		this.total_share = total_share;
	}

	public String getTotal_page() {
		return total_page;
	}

	public void setTotal_page(String total_page) {
		this.total_page = total_page;
	}

	public String getTotal_record() {
		return total_record;
	}

	public void setTotal_record(String total_record) {
		this.total_record = total_record;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

}
