package com.phonethics.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class DatePreferencesClass {

	Activity context;
	SharedPreferences prefs;
	Editor editor;

	String PREF_NAME;
	String PREF_KEY;

	long diffSeconds;
	long diffMinutes;
	long diffHours;

	long diffDays=0;

	SimpleDateFormat format;

	String date_time;

	public DatePreferencesClass(Activity context,String PREF_NAME,String PREF_KEY)
	{
		this.context = context;
		this.PREF_NAME=PREF_NAME;
		this.PREF_KEY=PREF_KEY;

		format=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		
	}


	public long getDiffSeconds() {
		return diffSeconds;
	}

	public long getDiffMinutes() {
		return diffMinutes;
	}

	public long getDiffHours() {
		return diffHours;
	}

	public long getDiffDays() {
		return diffDays;
	}

	public void checkDateDiff()
	{
		try
		{
			//Getting Date time stamp of last saved area
			SharedPreferences prefs=context.getSharedPreferences(PREF_NAME, 1);

			date_time=prefs.getString(PREF_KEY, format.format(new Date()).toString());

			Date prevDate=format.parse(date_time);
			Date currentDate=format.parse(format.format(new Date()));

			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();

			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;

			diffDays = diff / (24 * 60 * 60 * 1000);

			Log.i("DATE TIME DIFF", "DATE TIME DIFF STAMP"+date_time);

//			Log.i("DATE TIME DIFF", "DATE TIME DIFF "+diffDays);



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void setDateInPref()
	{
		SharedPreferences prefs=context.getSharedPreferences(PREF_NAME,1);
		Editor editor=prefs.edit();
		editor.putString(PREF_KEY,format.format(new Date()));
		editor.commit();
	}
	
	public boolean isPrefDatePresent()
	{
		SharedPreferences prefs=context.getSharedPreferences(PREF_NAME,1);
		String date=prefs.getString(PREF_KEY,"");
		if(date.length()==0)
		{
			return false;
		}
		return true;
	}




}
