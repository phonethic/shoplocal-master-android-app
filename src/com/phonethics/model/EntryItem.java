package com.phonethics.model;

public class EntryItem implements Item{

	private String title;
	private String badge;
	private int drawable_icon;

	public EntryItem(String title,String badge,int drawable_icon)
	{
		this.badge=badge;
		this.title=title;
		this.drawable_icon=drawable_icon;
	}
	
	public String getTitle() {
		return title;
	}
	
	
	

	public int getDrawable_icon() {
		return drawable_icon;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	@Override
	public Integer ItemType() {
		// TODO Auto-generated method stub
		return 3;
	}

	
	
	
}
