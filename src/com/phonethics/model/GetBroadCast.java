package com.phonethics.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class GetBroadCast implements Parcelable {

	private String total_page="";
	private String total_record="";
	private String id="";
	private String type="";
	private String tags="";
	private String title="";
	private String description="";
	private String url="";
	private String date="";
	private String place_id="";
	private String total_like="";
	private String user_like="";
	private String user_views = "";
	private String total_share = "";
	
	private String image_url1="";
	private String image_url2="";
	private String image_url3="";
	
	private String thumb_url1="";
	private String thumb_url2="";
	private String thumb_url3="";
	
	private String image_title1="";
	private String image_title2="";
	private String image_title3="";
	
	private String is_offer="";
	private String offer_date_time="";
	
	private ArrayList<String>source=new ArrayList<String>();

	
	
	public String getIs_offer() {
		return is_offer;
	}
	public void setIs_offer(String is_offer) {
		this.is_offer = is_offer;
	}
	public String getOffer_date_time() {
		return offer_date_time;
	}
	public void setOffer_date_time(String offer_date_time) {
		this.offer_date_time = offer_date_time;
	}
	public String getImage_title1() {
		return image_title1;
	}
	public void setImage_title1(String image_title1) {
		this.image_title1 = image_title1;
	}
	public String getImage_title2() {
		return image_title2;
	}
	public void setImage_title2(String image_title2) {
		this.image_title2 = image_title2;
	}
	public String getImage_title3() {
		return image_title3;
	}
	public void setImage_title3(String image_title3) {
		this.image_title3 = image_title3;
	}
	public String getImage_url1() {
		return image_url1;
	}
	public void setImage_url1(String image_url1) {
		this.image_url1 = image_url1;
	}
	public String getImage_url2() {
		return image_url2;
	}
	public void setImage_url2(String image_url2) {
		this.image_url2 = image_url2;
	}
	public String getImage_url3() {
		return image_url3;
	}
	public void setImage_url3(String image_url3) {
		this.image_url3 = image_url3;
	}
	public String getThumb_url1() {
		return thumb_url1;
	}
	public void setThumb_url1(String thumb_url1) {
		this.thumb_url1 = thumb_url1;
	}
	public String getThumb_url2() {
		return thumb_url2;
	}
	public void setThumb_url2(String thumb_url2) {
		this.thumb_url2 = thumb_url2;
	}
	public String getThumb_url3() {
		return thumb_url3;
	}
	public void setThumb_url3(String thumb_url3) {
		this.thumb_url3 = thumb_url3;
	}
	public String getPlace_id() {
		return place_id;
	}
	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}
	
	public String getTotal_share() {
		return total_share;
	}
	public void setTotal_share(String total_share) {
		this.total_share = total_share;
	}
	
	public String getUser_views() {
		return user_views;
	}
	public void setUser_views(String user_views) {
		this.user_views = user_views;
	}
	public String getTotal_like() {
		return total_like;
	}
	public void setTotal_like(String total_like) {
		this.total_like = total_like;
	}
	public String getUser_like() {
		return user_like;
	}
	public void setUser_like(String user_like) {
		this.user_like = user_like;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public void setSource(String isource)
	{
		source.add(isource);
	}
	public ArrayList<String> getSource()
	{
		return source;
	}
	public String getTotal_page() {
		return total_page;
	}

	public void setTotal_page(String total_page) {
		this.total_page = total_page;
	}

	public String getTotal_record() {
		return total_record;
	}

	public void setTotal_record(String total_record) {
		this.total_record = total_record;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	
	
}
