package com.phonethics.model;

public class Position {
	int  position;	
	public boolean ischeckedflag;

	private String placename="";
	private String id="";

	public Position(int name, boolean flag) {
		position = name;	   
		ischeckedflag = flag;
	}

	public String getPlacename() {
		return placename;
	}

	public void setPlacename(String placename) {
		this.placename = placename;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
