package com.phonethics.model;

public class Model {
	
	private String name;
	private String id;
	private boolean selected;
	
	public Model()
	{
		
	}
	public Model(String name,boolean selected)
	{
		this.name=name;
		this.selected=selected;
	}
	
	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	
}
