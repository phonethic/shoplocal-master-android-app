package com.phonethics.localnotification;



import java.util.Date;
import java.text.SimpleDateFormat;

import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.SessionManager;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class AlarmBroadCastReceiver extends BroadcastReceiver {


	long diffSeconds;
	long diffMinutes;
	long diffHours;

	long diffDays;

	String NOTIFICATION_ACTION="com.phonethics.localnotification.NOTIFICATION_ACTION";

	SessionManager session;

	int requestCode=0;

	LocalNotification localNotification;

	@Override
	public void onReceive(Context context, Intent intent2) {
		// TODO Auto-generated method stub
		try
		{
			session=new SessionManager(context);
			Bundle bundle = intent2.getExtras();

			localNotification=new LocalNotification(context);

			Log.i("Notification Broadcast", "Notification Broadcast fired on "+bundle.getString("dateCreated") +" "+bundle.getInt("requestCode"));

			if(bundle!=null)
			{
				requestCode=bundle.getInt("requestCode");
			}


			checkDateDiff(bundle.getString("dateCreated"));

			if(session.isLoggedIn()) //Merchant Logged in
			{

				if(requestCode==Integer.parseInt(context.getString(R.string.Edit_Store_alert)))
				{
					createNotification(context.getString(R.string.myplaceMessage),context);
				}

				if(requestCode==Integer.parseInt(context.getString(R.string.Talk_Now_alert)))
				{
					createNotification(context.getString(R.string.talkNowMessage),context);
				}

				//			if(requestCode==Integer.parseInt(context.getString(R.string.TalkNow_Rem_alert)))
				//			{
				//				createNotification(context.getString(R.string.talkNowReminder),context);
				//			}

			}
			else
			{
				if(session.isLoggedInCustomer()) //customer logged in
				{
					if(requestCode==Integer.parseInt(context.getString(R.string.Favourite_alert)))
					{
						createNotification(context.getString(R.string.customerFav),context);
					}

					if(requestCode==Integer.parseInt(context.getString(R.string.Customer_Profile_alert)))
					{
						createNotification(context.getString(R.string.customerProfile),context);
					}



				}else
				{
					if(requestCode==Integer.parseInt(context.getString(R.string.Login_alert)))
					{
						createNotification(context.getString(R.string.customerNotLoggedIn),context);
					}

				}

				if(requestCode==Integer.parseInt(context.getString(R.string.App_Launch)))
				{
					createNotification(context.getString(R.string.appNotLaunched),context);
				}

				//			if(requestCode==Integer.parseInt(context.getString(R.string.Offers_Stream_alert)))
				//			{
				//				createNotification(context.getString(R.string.customeOfferReminder),context);
				//			}
			}
			/*if(diffDays<17)
		{



			if(diffDays==16)
			{
				cancelAlarm(requestCode, context);
			}
		}
		else
		{
			cancelAlarm(requestCode, context);
		}*/
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void createNotification(String message,Context context)
	{
		try
		{
			/*Toast.makeText(context, "Created Notification", Toast.LENGTH_SHORT).show();*/

			RemoteViews myRemote=new RemoteViews(context.getPackageName(), R.layout.custom_notification);
			myRemote.setTextViewText(R.id.customText,message);

			//Notification Action
			Intent intent = new Intent(NOTIFICATION_ACTION);
			intent.putExtra("requestCode", requestCode);

			//Pending Intent 
			PendingIntent pIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

			//Notification Builder
			NotificationCompat.Builder mBuilder=new NotificationCompat.Builder(context);
			mBuilder.setContentTitle("Shoplocal");
			mBuilder.setContentText("Place Details ");
			mBuilder.setAutoCancel(true);
			mBuilder.setSmallIcon(R.drawable.ic_launcher);
			mBuilder.setWhen(System.currentTimeMillis());
			mBuilder.setContent(myRemote);
			mBuilder.setContentIntent(pIntent);


			mBuilder.setSound(RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION));
			NotificationManager  notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(requestCode, mBuilder.build());

			//Setting Alarm Pref
			localNotification.setAlarmFired(requestCode, true);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	public void checkDateDiff(String alarmDate)
	{
		try
		{
			//Getting Date time stamp of last saved area




			/*DateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");*/
			SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");

			Date prevDate=format.parse(alarmDate);
			Date currentDate=format.parse(format.format(new Date()));

			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();

			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;

			diffDays = diff / (24 * 60 * 60 * 1000);

			Log.i("DATE TIME DIFF", "Notification Broadcast fired on DIFF DAYS "+diffDays);

			//			Log.i("DATE TIME DIFF", "DATE TIME DIFF "+diffDays);



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void cancelAlarm(int requestCode,Context context)
	{

		/*Log.i("DATE TIME DIFF", "Notification Broadcast fired Alarm Canceled ");*/

		Intent intent = new Intent();
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null)
		{
			/*Toast.makeText(context, "Alarm Canceled",Toast.LENGTH_SHORT).show();*/

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}

}
