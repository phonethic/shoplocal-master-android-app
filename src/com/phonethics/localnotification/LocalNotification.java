package com.phonethics.localnotification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.phonethics.shoplocal.R;
import com.phonethics.shoplocal.SessionManager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.Toast;

public class LocalNotification {

	Context context;
	DateFormat format;

	//Alarm Manager
	AlarmManager alarmManager;



	int alarmType=AlarmManager.RTC_WAKEUP;
	long repeatFor=0;
	long timeOrLengthOfWait=0;
	String ALARM_ACTION="com.phonethics.localnotification.ALARM_ACTION";

	//Preference Names

	String IS_ALARM_FIRED="IS_ALARM_FIRED";

	//PREF FOR EDIT STORE
	String EDIT_STORE_PREF="EDIT_STORE";
	String EDIT_STORE_KEY_ID="PLACE_ID";
	String EDIT_STORE_SETALARM="EDIT_STORE_SET_ALARM";

	//PREF FOR MERCHANT NOT POSTED STORE
	String POST_STORE_PREF="POST_STORE_PREF";
	String POST_PREF_DATE="POST_STORE_PREF_DATE";
	String POST_PREF_SETALARM="POST_PREF_SET_ALARM";

	//PREF FOR FAV
	String FAV_STORE_PREF="FAV_STORE_PREF";
	String FAV_SETALARM="HAS_FAV";


	//PREF FOR Customer Profile
	String PROFILE_PREF="PROFILE_PREF";
	String PROFILE_SETALARM="IS_UPDATED";

	//PREF FOR APP LAUNCH
	String APP_LAUNCH_PREF="APP_LAUNCH_PREF";

	//PREF FOR LOGIN
	String APP_LOGIN_PREF="APP_LOGIN_PREF";

	//PREF Boolean
	boolean isPlaceAlarmReq=false;


	boolean isPlaceAlarmFired=false;

	boolean isFavAlarmFired=false;

	boolean isPostAlarmFired=false;

	boolean isProfilAlarmFired=false;

	boolean hasFav=false;

	boolean isPosted=false;

	boolean isProfileUpdated=false;

	boolean isAppLaunchedFired=false;

	boolean isLoginAlarmFired=false;


	//Pref Vari
	String placeId="";
	String postDate="";
	
	SessionManager session;


	long diffSeconds;
	long diffMinutes;
	long diffHours;

	long diffDays;

	public LocalNotification(Context context)
	{
		this.context=context;
		format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
		alarmManager=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		session=new SessionManager(this.context);
	}


	public void setLocalNotification(int setFor,int days,int requestCode,boolean isRepeating)
	{
		try
		{

			

			//Current Date
			Date date=new Date();

			//Adding the days to current date
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, setFor);

			long timeOrLengthOfWait=c.getTimeInMillis();

			//Firing Broadcast with date created
			Intent intentToFire= new Intent(ALARM_ACTION);

			intentToFire.putExtra("requestCode",requestCode);
			intentToFire.putExtra("dateCreated",format.format(date).toString());

			//Creating pending intent with given request Code
			PendingIntent alarmIntent=PendingIntent.getBroadcast(context, requestCode, intentToFire,PendingIntent.FLAG_UPDATE_CURRENT);

			if(isRepeating)
			{
				//Repeating Alarm for given days
				repeatFor=AlarmManager.INTERVAL_DAY*days;

				//Setting alarm
				alarmManager.setInexactRepeating(alarmType, timeOrLengthOfWait, repeatFor, alarmIntent);
			}
			else
			{
				Log.i("Notification ", "Notification  "+setFor+" "+days+" "+requestCode+" "+isRepeating);
				
				alarmManager.set(alarmType, timeOrLengthOfWait, alarmIntent);
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void setWeeklyReminder(int requestCode,int hour)
	{
		try
		{
			//Current Date
			Date date=new Date();

			//Repeating Alarm for given days
			repeatFor=AlarmManager.INTERVAL_DAY*7;

			int day=Math.abs(Calendar.FRIDAY - (Calendar.DAY_OF_WEEK % 7));

			//Setting Alarm for particular Day
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, day);
			c.set(Calendar.HOUR, hour);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);

			long timeOrLengthOfWait=c.getTimeInMillis();

			Intent intentToFire= new Intent(ALARM_ACTION);
			intentToFire.putExtra("requestCode",requestCode);
			intentToFire.putExtra("dateCreated",format.format(date).toString());

			//Creating pending intent with given request Code
			PendingIntent alarmIntent=PendingIntent.getBroadcast(context, requestCode, intentToFire,PendingIntent.FLAG_UPDATE_CURRENT);

			//Setting alarm
			alarmManager.setInexactRepeating(alarmType, timeOrLengthOfWait, repeatFor, alarmIntent);
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void checkPref()
	{
		try
		{
			//Place Preference 
			SharedPreferences editStorePref=context.getSharedPreferences(EDIT_STORE_PREF, Context.MODE_PRIVATE);
			isPlaceAlarmReq=editStorePref.getBoolean(EDIT_STORE_SETALARM, false);
			placeId=editStorePref.getString(EDIT_STORE_KEY_ID, "");
			isPlaceAlarmFired=editStorePref.getBoolean(IS_ALARM_FIRED, false);

			//Post Preference
			SharedPreferences postStorePref=context.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			postDate=postStorePref.getString(POST_PREF_DATE, "");
			isPosted=postStorePref.getBoolean(POST_PREF_SETALARM, false);
			isPostAlarmFired=postStorePref.getBoolean(IS_ALARM_FIRED, false);

			/*if(postDate.length()!=0)
			{
				isPosted=true;
			}
			else
			{
				isPosted=false;
			}*/

			//Favourite Preference
			SharedPreferences favStorePref=context.getSharedPreferences(FAV_STORE_PREF, Context.MODE_PRIVATE);
			hasFav=favStorePref.getBoolean(FAV_SETALARM, false);
			isFavAlarmFired=favStorePref.getBoolean(IS_ALARM_FIRED, false);

			//Profile Preference
			SharedPreferences profPref=context.getSharedPreferences(PROFILE_PREF, Context.MODE_PRIVATE);
			isProfileUpdated=profPref.getBoolean(PROFILE_SETALARM, false);
			isProfilAlarmFired=profPref.getBoolean(IS_ALARM_FIRED, false);

			//App Launch Preference
			SharedPreferences appLaunch=context.getSharedPreferences(APP_LAUNCH_PREF, Context.MODE_PRIVATE);
			isAppLaunchedFired=appLaunch.getBoolean(IS_ALARM_FIRED, false);

			//App Login Preference
			SharedPreferences appLogin=context.getSharedPreferences(APP_LOGIN_PREF, Context.MODE_PRIVATE);
			isLoginAlarmFired=appLogin.getBoolean(IS_ALARM_FIRED, false);

			Log.i("Check Prefs", "Check prefs "+isPlaceAlarmReq+" "+isPosted+" "+hasFav+" "+isProfileUpdated);
			Log.i("Check Prefs", "Check prefs fired "+isPlaceAlarmFired+" "+isPostAlarmFired+" "+isFavAlarmFired+" "+isProfilAlarmFired+" "+isAppLaunchedFired+" "+isLoginAlarmFired);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public void clearPlacePrefs()
	{
		try
		{
			SharedPreferences editStorePref=context.getSharedPreferences(EDIT_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=editStorePref.edit();
			editor.remove(EDIT_STORE_KEY_ID);
			editor.remove(EDIT_STORE_SETALARM);
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}


	}

	public void clearPostPrefs()
	{
		try
		{
			SharedPreferences postStorePref=context.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=postStorePref.edit();
			editor.remove(POST_PREF_DATE);
			editor.remove(IS_ALARM_FIRED);
			editor.remove(POST_PREF_SETALARM);
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void clearFavPrefs()
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(FAV_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(FAV_SETALARM);
			editor.remove(IS_ALARM_FIRED);
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	
	public void clearProfilePrefs()
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(PROFILE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(PROFILE_SETALARM);
			editor.remove(IS_ALARM_FIRED);
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	public void clearAppLaunchPrefs()
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(APP_LAUNCH_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(IS_ALARM_FIRED);
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	
	public void clearLoginPrefs()
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(APP_LOGIN_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.remove(IS_ALARM_FIRED);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}



	//Place Preference
	public void setPlacePref(String Place_Id,boolean setAlarm,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences editStorePref=context.getSharedPreferences(EDIT_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=editStorePref.edit();
			editor.putString(EDIT_STORE_KEY_ID, Place_Id);
			editor.putBoolean(EDIT_STORE_SETALARM, setAlarm);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Set Post Preference
	public void setPostPref(String postDate,boolean isAlarmFired)
	{

		try
		{
			SharedPreferences postStorePref=context.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=postStorePref.edit();
			editor.putString(POST_PREF_DATE, postDate);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.putBoolean(POST_PREF_SETALARM, isPosted);	
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void setIsPosted(boolean isPosted)
	{
		try
		{
			SharedPreferences postStorePref=context.getSharedPreferences(POST_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=postStorePref.edit();
			editor.putBoolean(POST_PREF_SETALARM, isPosted);	
			editor.commit();

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	//Set Favourite Preference
	public void setFavPref(boolean setAlarm,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(FAV_STORE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(FAV_SETALARM, setAlarm);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Set Profile Preference
	public void setProfilePref(boolean setAlarm,boolean isAlarmFired)
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(PROFILE_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(PROFILE_SETALARM, setAlarm);
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Set App Launch Preference
	public void setAppLaunchPref(boolean setAlarm)
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(APP_LAUNCH_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(IS_ALARM_FIRED, setAlarm);
			editor.commit();


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}

	//Set Login Preference
	public void setLoginPref(boolean setAlarm)
	{
		try
		{
			SharedPreferences favStorePref=context.getSharedPreferences(APP_LOGIN_PREF, Context.MODE_PRIVATE);
			Editor editor=favStorePref.edit();
			editor.putBoolean(IS_ALARM_FIRED, setAlarm);
			editor.commit();


		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	
	
	// Set Alarm Fired Toggle
	
	void setAlarmFired(int requestCode,boolean isAlarmFired)
	{
		try
		{
			String PREF_NAME="";
			
			//Merchant
			if(requestCode==Integer.parseInt(context.getString(R.string.Edit_Store_alert)))
			{
				PREF_NAME=EDIT_STORE_PREF;
			}
			if(requestCode==Integer.parseInt(context.getString(R.string.Talk_Now_alert)))
			{
				PREF_NAME=POST_STORE_PREF;
			}
			
			//Customer
			if(requestCode==Integer.parseInt(context.getString(R.string.Favourite_alert)))
			{
				PREF_NAME=FAV_STORE_PREF;
			}
			if(requestCode==Integer.parseInt(context.getString(R.string.Customer_Profile_alert)))
			{
				PREF_NAME=PROFILE_PREF;
			}
			
			
			
			if(requestCode==Integer.parseInt(context.getString(R.string.Login_alert)))
			{
				PREF_NAME=APP_LOGIN_PREF;
			}
			if(requestCode==Integer.parseInt(context.getString(R.string.App_Launch)))
			{
				PREF_NAME=APP_LAUNCH_PREF;
			}
			
			SharedPreferences pref=context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
			Editor editor=pref.edit();
			editor.putBoolean(IS_ALARM_FIRED, isAlarmFired);
			editor.commit();
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	


	public void cancelAlarm(int requestCode,Context context)
	{

		Log.i("DATE TIME DIFF", "Notification Broadcast fired Alarm Canceled "+requestCode);

		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled ");
			
			//Toast.makeText(context, "Alarm Canceled on Back Press",Toast.LENGTH_SHORT).show();

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}


	//Canceling All Alarm
	public void caneclAllAlarm()
	{
		String NOTIFICATION_ACTION="com.phonethics. NOTIFICATION_ACTION";

		//Log.i("DATE TIME DIFF", "Alarm Canceled ");

	//	Toast.makeText(context, "Canceled Called", Toast.LENGTH_SHORT).show();

		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender1 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Edit_Store_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender1!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Edit_Store_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender1);
		}

		PendingIntent sender2 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Talk_Now_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender2!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Talk_Now_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender2);
		}

		/*PendingIntent sender3 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.TalkNow_Rem_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender3!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.TalkNow_Rem_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender3);
		}*/

		PendingIntent sender4 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Login_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender4!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Login_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender4);
		}


		PendingIntent sender5 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Favourite_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender5!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Favourite_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender5);
		}

		PendingIntent sender6 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Customer_Profile_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender6!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Customer_Profile_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender6);
		}

		/*PendingIntent sender7 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.Offers_Stream_alert)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender7!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.Offers_Stream_alert));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender7);
		}*/

		PendingIntent sender8 = PendingIntent.getBroadcast(context, Integer.parseInt(context.getString(R.string.App_Launch)), intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender8!=null)
		{
			Log.i("DATE TIME DIFF", "Alarm Canceled "+context.getString(R.string.App_Launch));

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender8);
		}

	}







	public boolean isAppLaunchedFired() {
		return isAppLaunchedFired;
	}


	public void setAppLaunchedFired(boolean isAppLaunchedFired) {
		this.isAppLaunchedFired = isAppLaunchedFired;
	}


	public boolean isLoginAlarmFired() {
		return isLoginAlarmFired;
	}


	public String getPlaceId() {
		return placeId;
	}

	public String getPostDate() {
		return postDate;
	}


	public boolean placeAlarm()
	{
		return isPlaceAlarmReq;
	}

	public boolean postAlarm()
	{
		return isPosted;
	}

	public boolean favPostAlarm()
	{
		return hasFav;
	}


	



	public boolean isPlaceAlarmFired() {
		return isPlaceAlarmFired;
	}



	public boolean isFavAlarmFired() {
		return isFavAlarmFired;
	}



	public boolean isPostAlarmFired() {
		return isPostAlarmFired;
	}



	public boolean isProfilAlarmFired() {
		return isProfilAlarmFired;
	}



	public boolean isProfileUpdated() {
		return isProfileUpdated;
	}
	
	
	//Setting Alarm
	public void alarm()
	{
		try
		{
			checkPref();

			if(session.isLoggedIn()) //Merchant
			{
				if( isPlaceAlarmFired()==false)
				{
					if( placeAlarm()==true )
					{
						//Remind Merchant for incomplete place details
						 setLocalNotification(7, 0,Integer.parseInt(context.getString(R.string.Edit_Store_alert)), false);
					}
					else
					{
						 cancelAlarm(Integer.parseInt(context.getString(R.string.Edit_Store_alert)),context);
					}
				}
				if( isPostAlarmFired()==false)
				{
					if(postAlarm()==false)
					{
						//Remind Merchant for posting offer 
						 setLocalNotification(3, 0,Integer.parseInt(context.getString(R.string.Talk_Now_alert)), false);
						 setIsPosted(true);
					}
					/*else
					{
						 cancelAlarm(Integer.parseInt(context.getString(R.string.Talk_Now_alert)),context);
					}*/
				}
			}
			if(session.isLoggedInCustomer())
			{
				if( isFavAlarmFired()==false)
				{
					if( favPostAlarm()==false)
					{
						//Remind Customer that he has not marked any store favourite
						 setLocalNotification(3, 0,Integer.parseInt(context.getString(R.string.Favourite_alert)), false);
					}
					else
					{
						 cancelAlarm(Integer.parseInt(context.getString(R.string.Favourite_alert)),context);
					}
				}

				if( isProfilAlarmFired()==false)
				{
					if( isProfileUpdated()==false )
					{
						//Customer has not updated his profile
						 setLocalNotification(7, 0, Integer.parseInt(context.getString(R.string.Customer_Profile_alert)), false);
					}
					else
					{
						 cancelAlarm(Integer.parseInt(context.getString(R.string.Customer_Profile_alert)),context);
					}
				}
				
				if( isLoginAlarmFired()==false)
				{
					 cancelAlarm(Integer.parseInt(context.getString(R.string.Login_alert)),context);
				}
			}
			else
			{
				if( isLoginAlarmFired()==false)
				{
					//Customer launched the app but not signed in
					 setLocalNotification(7, 0, Integer.parseInt(context.getString(R.string.Login_alert)), false);
				}
				
			}

			if( isAppLaunchedFired()==false)
			{
				//App Not launched
				 setLocalNotification(4, 0, Integer.parseInt(context.getString(R.string.App_Launch)), false);
			}

			//Clearing Merchant Prefs if Merchant is Logged out 
			if(!session.isLoggedIn())
			{
				 clearPlacePrefs();
				 clearPostPrefs();
			}

			//Clearing Customer Prefs if Customer is Logged out 
			if(!session.isLoggedInCustomer())
			{
				 clearFavPrefs();
				 clearProfilePrefs();

			}

//			if([INUserDefaultOperations isMerchantLoggedIn]) { //If merchant is loggedin
//				if(![INUserDefaultOperations isEditStoreNotificationAlreadySet])
//					[INUserDefaultOperations setEditStoreNotification]; //After 7 Days
//
//				if(![INUserDefaultOperations isTalkNowNotificationAlreadySet])
//					[INUserDefaultOperations setTalkNowNotification]; // After 3 Days from today
//
//			} else { // Reminders for Customers
//
//				if([INUserDefaultOperations isCustomerLoggedIn]) {
//					if(![INUserDefaultOperations isCustomerSpecialDatesNotificationAlreadySet])
//						[INUserDefaultOperations setCustomerSpecialDatesNotification]; // After 7 days
//
//					if(![INUserDefaultOperations isCustomerFavouriteNotificationAlreadySet])
//						[INUserDefaultOperations setCustomerFavouriteNotification]; // After 3 days
//				} else {
//					if(![INUserDefaultOperations isCustomerSignInNotificationAlreadySet])
//						[INUserDefaultOperations setCustomerSignInNotification]; // After 7 days
//				}
//				if(![INUserDefaultOperations isAppNotLaunchedNotificationAlreadySet])
//					[INUserDefaultOperations setAppNotLaunchedNotification]; // After 4 days
//			}


			//Remind on Every Friday to Merchant to post offers
			/* setWeeklyReminder(Integer.parseInt(getString(R.string.TalkNow_Rem_alert)), 4);*/

			//Remind on Every Friday to customer to check offers
			/* setWeeklyReminder(Integer.parseInt(getString(R.string.Offers_Stream_alert)), 6);*/

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	
	
	public void checkDateDiff(String alarmDate)
	{
		try
		{
			//Getting Date time stamp of last saved area




			/*DateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");*/
			SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");

			Date prevDate=format.parse(alarmDate);
			Date currentDate=format.parse(format.format(new Date()));

			//in milliseconds
			long diff = currentDate.getTime() - prevDate.getTime();

			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;

			diffDays = diff / (24 * 60 * 60 * 1000);

			Log.i("DATE TIME DIFF", "Notification Broadcast fired on DIFF DAYS "+diffDays);

			//			Log.i("DATE TIME DIFF", "DATE TIME DIFF "+diffDays);



		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}



}
