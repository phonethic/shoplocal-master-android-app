package com.phonethics.localnotification;

import java.util.List;

import com.phonethics.shoplocal.StartUpActivity;
import com.urbanairship.UAirship;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class NotificationReceiver extends BroadcastReceiver {

	int requestCode=0;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		//Toast.makeText(context, "Came In Notification", Toast.LENGTH_SHORT).show();
		/*if(!isNamedProcessRunning("com.phonethics.shoplocal", context))
		{
			Toast.makeText(context, "App is not running", Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(context, "App is  running", Toast.LENGTH_SHORT).show();
		}*/
		
		Bundle bundle = intent.getExtras();
		
		if(bundle!=null)
		{
			requestCode=bundle.getInt("requestCode");
			cancelAlarm(requestCode, context);
		}
		
		
		 Intent launch = new Intent(Intent.ACTION_MAIN);
		 launch.putExtra("requestCode", requestCode);
         launch.setClass(context, StartUpActivity.class);
         launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

         boolean isShoplocalRunning = isRunning(context);
         
         if(!isShoplocalRunning)
         	context.startActivity(launch);
	}
	
	public boolean isRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;   
//            else if (ctx.getPackageName().equalsIgnoreCase("com.phonethics.fragments"))
//            	return true;
//            else if (ctx.getPackageName().equalsIgnoreCase("com.phonethics.twitter"))
//            	return true;
        }

        return false;
    }

	boolean isNamedProcessRunning(String processName,Context context){
		if (processName == null) return false;

		ActivityManager manager = 
				(ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> processes = manager.getRunningAppProcesses();

		for (RunningAppProcessInfo process : processes)
		{
			if (processName.equals(process.processName))
			{
				return true;
			}
		}
		return false;
	}
	
	void cancelAlarm(int requestCode,Context context)
	{

		String ALARM_ACTION="com.phonethics.localnotification.ALARM_ACTION";
		
		//Log.i("DATE TIME DIFF", "Notification Broadcast fired Alarm Canceled ");
		

		Intent intentToFire= new Intent(ALARM_ACTION);
		intentToFire.putExtra("requestCode",0);
		intentToFire.putExtra("dateCreated","");
		PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intentToFire, PendingIntent.FLAG_UPDATE_CURRENT);
		if(sender!=null)
		{
			//Toast.makeText(context, "Alarm Canceled",Toast.LENGTH_SHORT).show();

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(sender);
		}
	}
	
	

}
